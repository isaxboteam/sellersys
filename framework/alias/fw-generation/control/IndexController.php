<?php
/**
 * Description of IndexController
 *
 * @author ypra
 */
class IndexController extends WebAliasController
{

    public function index()
    {
    }

    public function dbConfigs()
    {
        $env = Configs::value('app.run.environment');
        $conf = DBConfig::envConfigs($env);
        $this->set('env', $env);
        $this->set('conf', $conf);
    }

    public function classes()
    {
        $env = Configs::value('app.run.environment');
        $conf = DBConfig::envConfigs($env);
        $this->set('env', $env);
        $this->set('conf', $conf);
        $this->set('dsn', DBConfig::dsn($conf));
    }

    public function mapping()
    {   
        $reverseLog = $mappingLog = '';
        CodeGenerator::generateGenerationConfigs();
        CodeGenerator::includePhingAndPropel();
        if(isset($_POST['reverse'])){
            $reverseLog = CodeGenerator::generateSchema();
        }
        if(isset($_POST['mapping'])){
            $mappingLog = CodeGenerator::generateMapping();
        }
        /**
        // Bash generation way
        $result = '';
        $phpinfo = Configs::phpinfo();
        $PHP_COMMAND = $phpinfo['Environment']['PHP_COMMANDS'];
        $dirs = array('generator', 'mapping', 'gen');
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
            if(isset($_POST['reverse'])){
                $result.= system(CHOCALA_DIR.implode(DIRECTORY_SEPARATOR,$dirs).
                        ' '.$PHP_COMMAND.
                        ' reverse > '.MAPPING_DIR.'output'.DIRECTORY_SEPARATOR.
                        time().'-rev.log');
            }
            if(isset($_POST['mapping'])){
                $result.= system(CHOCALA_DIR.implode(DIRECTORY_SEPARATOR,$dirs).
                        ' '.$PHP_COMMAND.
                        ' > '.MAPPING_DIR.'output'.DIRECTORY_SEPARATOR.time().
                        '-gen.log');
            }        
        } else {
            echo "Linux Bash";
        }
        /**/
        $env = Configs::value('app.run.environment');
        $conf = DBConfig::envConfigs($env);
        $this->set('env', $env);
        $this->set('conf', $conf);
        $this->set('dsn', DBConfig::dsn($conf));
        $this->set('reverseLog', $reverseLog);
        $this->set('mappingLog', $mappingLog);
    }

    public function domains()
    {
        $mapedClasses = ClassMapHelper::mapDirRead(DOMAIN_DIR);
        ksort($mapedClasses);
        $this->set('mapedClasses', $mapedClasses);
    }

    public function domainClass()
    {
        $mapedClasses = ClassMapHelper::mapDirRead(DOMAIN_DIR);
        if(isset($mapedClasses[$this->id])){
            $mapedClass = $this->id;
            $mapedColumns = ClassMapHelper::columnsFrom($mapedClass);
            foreach ($mapedColumns as $mc){
                //print_r($mc);
            }
            $this->set('mapedClass', $mapedClass);
            $this->set('mapedColumns', $mapedColumns);
        }else{
            header('Location: '.WEB_ROOT.URI::toPage());
            exit();
        }
    }

    public function scaffolding()
    {
        $mapedClasses = ClassMapHelper::mapDirRead(DOMAIN_DIR);
        if(isset($mapedClasses[$this->id])){            
            $mapedClass = $this->id;
            $mapedColumns = ClassMapHelper::columnsFrom($mapedClass);
            $mapedColumsGen = array_filter($mapedColumns, function($obj){
                return isset($_REQUEST['_'.$obj->getName()]);
            });
            $hashColumns = array();
            foreach ($mapedColumsGen as $mcKey => $columnMap){
                $hashColumns[$columnMap->getName()] =
                        array('maped' => $columnMap);
                if($columnMap->isForeignKey()){
                    $hashColumns[$columnMap->getName()]['field'] = 
                            $_REQUEST['_field_'.$columnMap->getName()];
                }
            }
            $mapedHash = array('className' => $mapedClass,
                'mapedColumns' => $mapedColumns,
                'hashColumns' => $hashColumns);
            CodeGenerator::generateController($mapedHash);
            CodeGenerator::generateViews($mapedHash);
            
            //$this->renderAsJSON($mapedColumns);
            $this->set('mapedClass', $mapedClass);
            $this->set('views', CodeGenerator::$views);
            //$this->renderAsJSON($mapedColumns);
        }else{
            header('Location: '.WEB_ROOT.URI::toPage());
            exit();
        }
    }

}