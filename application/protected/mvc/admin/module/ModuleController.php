<?php
//Chocala::import('System.generation.CodeGenerator');
Chocala::import("Mvc.generics.AdminWebController");
/**
 * Description of ModuleController
 *
 * @author ypra
 */
class ModuleController extends AdminWebController
{

    public function index()
    {        
    }

    public function dataList()
    {
        $this->view->changeLayout('ajax');
        $modules = ChoModuleQuery::create()->orderByPosition()->find();
        //PageControl::allModules();
        $this->set('modules', $modules);
    }

    public function show()
    {
        $module = ChoModulePeer::retrieveByPK($this->id);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(ChoUriPeer::TITLE);
        $uris = $module->getChoUris($c);
        $this->set('module', $module);
        $this->set('uris', $uris);
    }

    public function create()
    {
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $module = new ChoModule();
        if(PageControl::canCreate()){
            //$module->fromArray(Req::all(), BasePeer::TYPE_STUDLYPHPNAME);
            $module->setUri(Req::_('uri'));
            $module->setName(Req::_('name'));
            $module->setAccess(Req::_('access'));
            $module->setDescription(Req::_('description'));
            $module->setPosition(ChoModuleQuery::create()->count()+1);
            $success = $module->validate();
            if($success){
                $module->save();
            }

        }
        $this->set('success', $success);
        $this->set('errors', $module->getErrorsMap());
        $this->renderAsJSON();
    }
    
    public function edit()
    {
        $module = ChoModuleQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()) {
            $this->set('module', $module);
        }
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $module = ChoModuleQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()) {
            //$rol->fromArray(Req::all(), BasePeer::TYPE_STUDLYPHPNAME);
            $module->setUri(Req::_('uri'));
            $module->setName(Req::_('name'));
            $module->setAccess(Req::_('access'));
            $module->setDescription(Req::_('description'));
            $success = $module->validate();
            if($success){
                $module->save();
            }
        }
        $this->set('success', $success);
        $this->set('errors', $module->getErrorsMap());
        $this->renderAsJSON();
    }
    

    public function ajaxUpdate()
    {
        $site = new Site();
        $modules = $site->allModules();
        $isProtected = false;
        if($this->id == ChocalaVars::MAIN_MODULE || $this->id ==
            ChocalaVars::ADMIN_MODULE){
            $isProtected = true;
        }
        $__Response = ':: Error ::';
        if(PageControl::canUpdate() && is_object($modules[$this->id]) &&
            Validation::isGet('newValue')){
            $field = URI::parameter(0);
            $newValue = Req::_('newValue');
            switch($field){
                case 'name':
                    if(Validation::isNotEmpty($newValue)){
                        $modules[$this->id]->setName($newValue);
                        $__Response = $newValue;
                    }
                    break;
                case 'access':
                    if($isProtected){
                        $__Response = $modules[$this->id]->getAccess();
                    }elseif(Validation::isNotEmpty($newValue)){
                        $modules[$this->id]->setAccess($newValue);
                        $__Response = $newValue;
                    }
                    break;
                case 'description':
                    $modules[$this->id]->setDescription($newValue);
                    $__Response = $newValue;
                    break;
                case 'main':
                    if($isProtected){
                        $__Response = '-----';
                    }elseif(isset($modules[$newValue])){
                        $modules[$this->id]->setModule($modules[$newValue]);
                        $__Response = $modules[$newValue]->getName();
                    }else{
                        $modules[$this->id]->withoutModule();
                        $__Response = '-----';
                    }
                    break;
                default:
                    break;
            }
            Site::saveToXML($modules);
        }
        $this->set('__Response', $__Response);
    }

    public function ajaxCombo()
    {
        $values = array();
        $value = null;
        $site = new Site();
        $modules = $site->allModules();
        $__Response = ':: Error ::';
        if(isset($modules[$this->id])){
            $field = URI::parameter(0);
            switch($field){
                case 'access':
                    $value = $modules[$this->id]->getAccess();
                    $values = PageConfigs::enumAccess('es');
                    break;
                case 'main':
                    $value = (is_object($modules[$this->id]->module())?
                        $modules[$this->id]->module()->URI(): '');
                    $values[''] = '- - - - -';
                    foreach($modules as $kMod => $mod){
                        if($kMod != $this->id){
                            $values[$kMod]=$mod->getName();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        $this->set('__ComboValues', $values);
        $this->set('__Value', $value);
    }

}