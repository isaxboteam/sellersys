<?php
Chocala::import("Mvc.generics.AdminWebController");
Chocala::import('Model.security.UserStatusEnum');
Chocala::import('Model.security.UserTypesEnum');
/**
 * Description of UserController
 *
 * @author ypra
 */
class UserController extends AdminWebController
{
    
    public function index()
    {
    }

    public function dataList()
    {
        $this->view->changeLayout('ajax');
        $users = ChoUserQuery::create()->paginate($page = 1, $maxPerPage = 10);
        $this->set('users', $users);
    }

    public function create()
    {
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $password = '123456';
        $user = new ChoUser();
        
        $user->setRolId(1);
        
        $user->setFirstName(ucfirst(Req::_('firstName')));
        $user->setSecondName(ucfirst(Req::_('secondName')));
        $user->setFirstLastname(ucfirst(Req::_('firstLastname')));
        $user->setSecondLastname(ucfirst(Req::_('secondLastname')));
        $user->setDni(Req::_('dni'));
        
        $user->setUsername(Req::_('username'));
        $user->setPassword($password);
        $user->setEmail(Req::_('email'));

        $user->setBirthday(Req::_asDate('birthday'));
        $user->setGender(Req::_('gender'));
        $user->setAddress(Req::_('address'));
        $user->setPrimaryPhone(Req::_('primaryPhone'));
        $user->setSecondaryPhone(Req::_('secundaryPhone'));
        $user->setCellPhone(Req::_('cellPhone'));

        $user->setType('INTERNAL');
        //$user->setRegisteredDate(date('c'));
        $user->setStatus(UserStatusEnum::CREATED);

        $success = $user->validate();
        if($success){
            $user->save();
        }
        $this->set('success', $success);
        $this->set('errors', $user->getErrorsMap());
        $this->set('userId', $user? $user->getId(): '');
        $this->renderAsJSON();
    }
    
    public function edit()
    {
        $user = ChoUserQuery::create()->findPk($this->id);
        $this->set('user', $user);
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $user = ChoUserQuery::create()->findPk($this->id);
        $user->setFirstName(ucfirst(Req::_('firstName')));
        $user->setSecondName(ucfirst(Req::_('secondName')));
        $user->setFirstLastname(ucfirst(Req::_('firstLastname')));
        $user->setSecondLastname(ucfirst(Req::_('secondLastname')));
        $user->setDni(Req::_('dni'));

        $user->setBirthday(Req::_asDate('birthday'));
        $user->setGender(Req::_('gender'));
        $user->setAddress(Req::_('address'));
        $user->setPrimaryPhone(Req::_('primaryPhone'));
        $user->setSecondaryPhone(Req::_('secundaryPhone'));
        $user->setCellPhone(Req::_('cellPhone'));

        $user->setType('INTERNAL');

        $success = $user->validate();
        if($success){
            $user->save();
        }
        $this->set('success', $success);
        $this->set('errors', $user->getErrorsMap());
        $this->set('userId', $user? $user->getId(): '');
        $this->renderAsJSON();
    }
    
    public function delete()
    {
        $rol = ChoRolPeer::retrieveByPK($this->id);
        $rol->delete();
        $this->redirectTo(array('action' => 'index'));
    }
    
    public function details()
    {
        $user = ChoUserQuery::create()->findPk($this->id);
        $this->set('user', $user);
    }
    
    public function rolAdd()
    {
        $user = ChoUserQuery::create()->findPk($this->id);
        /*
        $availableRols = ChoRolQuery::create()
                ->filterById(array_map(function($item){
                    return $item->getRolId();                    
                }, $user->inOrderRols()), Criteria::NOT_IN)
                ->find();
         */
        $availableRols = ChoRolQuery::create()
                //->filterByChoRolXUri($user->inOrderRols(), Criteria::NOT_IN)
                ->orderByName()->find();
        $this->set('user', $user);
        $this->set('availableRols', $availableRols);
        $this->view->changeLayout('ajax');
    }

    public function rolSave()
    {
        $userRol = new ChoUserXRol();
        //$rol->fromArray(Req::all(), BasePeer::TYPE_STUDLYPHPNAME);
        $userRol->setUserId(Req::_('userId'));
        $userRol->setRolId(Req::_('rolId'));
        $success = $userRol->validate();
        if($success){
            $userRol->save();
        }
        $this->set('success', $success);
        $this->set('errors', $userRol->getErrorsMap());
        $this->set('userRol', $userRol);
        $this->renderAsJSON();
    }

    public function rolDelete()
    {
        $user = ChoUserQuery::create()->findPk($this->id);
        $userRol = ChoUserXRolPeer::retrieveByPK($this->id, Req::_('rolId'));
        $userRol->delete();
        $this->redirectTo(array('action'=>'details', 'id'=>$user->getId()));
    }

    public function ajaxUpdate()
    {
        $site = new Site();
        $modules = $site->allModules();
        $isProtected = false;
        if($this->id == ChocalaVars::MAIN_MODULE || $this->id ==
            ChocalaVars::ADMIN_MODULE){
            $isProtected = true;
        }
        $__Response = ':: Error ::';
        if(PageControl::canUpdate() && is_object($modules[$this->id]) &&
            Validation::isGet('newValue')){
            $field = URI::parameter(0);
            $newValue = Req::_('newValue');
            switch($field){
                case 'name':
                    if(Validation::isNotEmpty($newValue)){
                        $modules[$this->id]->setName($newValue);
                        $__Response = $newValue;
                    }
                    break;
                case 'access':
                    if($isProtected){
                        $__Response = $modules[$this->id]->getAccess();
                    }elseif(Validation::isNotEmpty($newValue)){
                        $modules[$this->id]->setAccess($newValue);
                        $__Response = $newValue;
                    }
                    break;
                case 'description':
                    $modules[$this->id]->setDescription($newValue);
                    $__Response = $newValue;
                    break;
                case 'main':
                    if($isProtected){
                        $__Response = '-----';
                    }elseif(isset($modules[$newValue])){
                        $modules[$this->id]->setModule($modules[$newValue]);
                        $__Response = $modules[$newValue]->getName();
                    }else{
                        $modules[$this->id]->withoutModule();
                        $__Response = '-----';
                    }
                    break;
                default:
                    break;
            }
            Site::saveToXML($modules);
        }
        $this->set('__Response', $__Response);
    }

    public function ajaxCombo()
    {
        $values = array();
        $value = null;
        $site = new Site();
        $modules = $site->allModules();
        $__Response = ':: Error ::';
        if(isset($modules[$this->id])){
            $field = URI::parameter(0);
            switch($field){
                case 'access':
                    $value = $modules[$this->id]->getAccess();
                    $values = PageConfigs::enumAccess('es');
                    break;
                case 'main':
                    $value = (is_object($modules[$this->id]->module())?
                        $modules[$this->id]->module()->URI(): '');
                    $values[''] = '- - - - -';
                    foreach($modules as $kMod => $mod){
                        if($kMod != $this->id){
                            $values[$kMod]=$mod->getName();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        $this->set('__ComboValues', $values);
        $this->set('__Value', $value);
    }

}