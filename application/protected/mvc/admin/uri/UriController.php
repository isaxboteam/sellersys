<?php
Chocala::import("Mvc.generics.AdminWebController");
/**
 * Description of ModulesController
 *
 * @author ypra
 */
class UriController extends AdminWebController
{

    public function index()
    {
        $this->redirectTo(array('controller' => 'module'));
    }

    public function module()
    {
        $this->set('module', ChoModulePeer::retrieveByPK($this->id));
    }

    public function dataListFromModule()
    {
        $module = ChoModulePeer::retrieveByPK($this->id);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(ChoUriPeer::POSITION);
        $uris = $module->getChoUris($c);
        $this->set('module', $module);
        $this->set('uris', $uris);
        $this->view->changeLayout('ajax');
    }

    public function createFromModule()
    {
        $this->set('module', ChoModulePeer::retrieveByPK($this->id));
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $uri = new ChoModule();
        $uri = new ChoUri();
        $uri->fromArray(Req::all(), BasePeer::TYPE_STUDLYPHPNAME);
        if($uri->getChoModule()){
            $uri->setPosition($uri->getChoModule()->countChoUris()+1);
        }
        $success = $uri->validate();
        if($success){
            $uri->save();
        }
        $this->set('success', $success);
        $this->set('errors', $uri->getErrorsMap());
        $this->renderAsJSON();
    }

    public function edit()
    {
        $this->set('modules', ChoModuleQuery::create()->orderByName()->find());
        $this->set('uri', ChoUriQuery::create()->findPk($this->id));
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $uri = ChoUriQuery::create()->findPk($this->id);
        $uri->fromArray(Req::all(), BasePeer::TYPE_STUDLYPHPNAME);
        if($uri->getChoModule()){
            $uri->setPosition($uri->getChoModule()->countChoUris()+1);
        }
        $success = $uri->validate();
        if($success){
            $uri->save();
        }
        $this->set('success', $success);
        $this->set('errors', $uri->getErrorsMap());
        $this->renderAsJSON();
    }

    public function delete()
    {
        $uri = ChoUriQuery::create()->findPk($this->id);
        $module = $uri->getChoModule();
        try {
            $uri->delete();
        } catch (Exception $e) {
            $message = 'No se pudo eliminar el registro por que tiene información dependiente.';
        }
        $this->redirectTo(array('controller' => 'uri', 'action' => 'module',
            'id' => $module->getId()));
    }

    public function ajaxUpdate()
    {
        $site = new Site();
        $modules = $site->allModules();
        $isProtected = false;
        if($this->id == ChocalaVars::MAIN_MODULE || $this->id ==
            ChocalaVars::ADMIN_MODULE){
            $isProtected = true;
        }
        $__Response = ':: Error ::';
        if(PageControl::canUpdate() && is_object($modules[$this->id]) &&
            Validation::isGet('newValue')){
            $field = URI::parameter(0);
            $newValue = Req::_('newValue');
            switch($field){
                case 'name':
                    if(Validation::isNotEmpty($newValue)){
                        $modules[$this->id]->setName($newValue);
                        $__Response = $newValue;
                    }
                    break;
                case 'access':
                    if($isProtected){
                        $__Response = $modules[$this->id]->getAccess();
                    }elseif(Validation::isNotEmpty($newValue)){
                        $modules[$this->id]->setAccess($newValue);
                        $__Response = $newValue;
                    }
                    break;
                case 'description':
                    $modules[$this->id]->setDescription($newValue);
                    $__Response = $newValue;
                    break;
                case 'main':
                    if($isProtected){
                        $__Response = '-----';
                    }elseif(isset($modules[$newValue])){
                        $modules[$this->id]->setModule($modules[$newValue]);
                        $__Response = $modules[$newValue]->getName();
                    }else{
                        $modules[$this->id]->withoutModule();
                        $__Response = '-----';
                    }
                    break;
                default:
                    break;
            }
            Site::saveToXML($modules);
        }
        $this->set('__Response', $__Response);
    }

    public function ajaxCombo()
    {
        $values = array();
        $value = null;
        $site = new Site();
        $modules = $site->allModules();
        $__Response = ':: Error ::';
        if(isset($modules[$this->id])){
            $field = URI::parameter(0);
            switch($field){
                case 'access':
                    $value = $modules[$this->id]->getAccess();
                    $values = PageConfigs::enumAccess('es');
                    break;
                case 'main':
                    $value = (is_object($modules[$this->id]->module())?
                        $modules[$this->id]->module()->URI(): '');
                    $values[''] = '- - - - -';
                    foreach($modules as $kMod => $mod){
                        if($kMod != $this->id){
                            $values[$kMod]=$mod->getName();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        $this->set('__ComboValues', $values);
        $this->set('__Value', $value);
    }

    public function rolesAccess()
    {
        $uri = ChoUriPeer::retrieveByPK($this->id);
        $rolesUri = ChoRolXUriQuery::create()->filterByChoUri($uri)->find();
        $this->set('uri', $uri);
        $this->set('roles', ChoRolQuery::create()->orderByName()->find());
        $this->set('rolesUri', $rolesUri);
        $this->view->changeLayout('ajax');
    }

    public function changeMainPermission()
    {
        $rolUri = ChoRolXUriPeer::retrieveByPK(Req::_('rolId'),Req::_('uriId'));
        $permission = false;
        if($rolUri) {
            $rolUri->delete();
        } else {
            $rolUri = new ChoRolXUri();
            $rolUri->setRolId(Req::_('rolId'));
            $rolUri->setUriId(Req::_('uriId'));
            $rolUri->save();
            $permission = $rolUri;
        }
        $this->set('permission', is_object($permission));
        $this->renderAsJSON();
    }

    public function changeDetailPermission()
    {
        $rolUri = ChoRolXUriPeer::retrieveByPK(Req::_('rolId'),Req::_('uriId'));
        $permission = false;
        if($rolUri) {
            switch (Req::_('type')){
                case 'read';
                    $rolUri->setRead(ChocalaVars::
                            asBoolean($rolUri->getRead())? 'NO': 'SI');
                    $permission = $rolUri->getRead();
                    break;
                case 'create';
                    $rolUri->setCreate(ChocalaVars::
                            asBoolean($rolUri->getCreate())? 'NO': 'SI');
                    $permission = $rolUri->getCreate();
                    break;
                case 'update';
                    $rolUri->setUpdate(ChocalaVars::
                            asBoolean($rolUri->getUpdate())? 'NO': 'SI');
                    $permission = $rolUri->getUpdate();
                    break;
                case 'delete';
                    $rolUri->setDelete(ChocalaVars::
                            asBoolean($rolUri->getDelete())? 'NO': 'SI');
                    $permission = $rolUri->getDelete();
                    break;
            }
            $rolUri->save();
        }
        $this->set('permission', ChocalaVars::asBoolean($permission));
        $this->renderAsJSON();
    }

    public function functionalitiesList()
    {
        $this->module = GlobalVars::instance()->module();
        $this->template = 'page_functionalities';
        $site = new Site();
        $modules = $site->allModules();
        $__Response = ':: Error ::';
        $keys = explode('*', $this->id);
        $mPk = $keys[0];
        $pk = $keys[1];
        if(is_object($modules[$mPk])){
            $components = $modules[$mPk]->components();
            if(is_object($components[$pk])){
                $page = $components[$pk];
                $this->set("pagina", $page);
                $this->set("functionalities", Functionality::functionalitiesList());
            }
        }
    }

}