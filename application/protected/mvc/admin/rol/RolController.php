<?php
Chocala::import("Mvc.generics.AdminWebController");
/**
 * Description of ModulesController
 *
 * @author ypra
 */
class RolController extends AdminWebController
{

    public function index()
    {
        //$this->redirectTo(array('action'=>'dataList'));
    }

    public function dataList()
    {
        $this->view->changeLayout('ajax');
        $rols = ChoRolQuery::create()->orderByName()->find();
        $this->set('rols', $rols);
    }

    public function create()
    {
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $rol = new ChoRol();
        if(PageControl::canCreate()){
            //$rol->fromArray(Req::all(), BasePeer::TYPE_STUDLYPHPNAME);
            $rol->setCode(strtoupper(Req::_('code')));
            $rol->setName(Req::_('name'));
            $rol->setDescription(Req::_('description'));
            $success = $rol->validate();
            if($success){
                $rol->save();
            }
        }
        $this->set('success', $success);
        $this->set('errors', $rol->getErrorsMap());
        $this->renderAsJSON();
    }
    
    public function edit()
    {
        $rol = ChoRolQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()){
            $this->set('rol', $rol);
        }
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $rol = ChoRolQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()){
            //$rol->fromArray(Req::all(), BasePeer::TYPE_STUDLYPHPNAME);
            $rol->setCode(strtoupper(Req::_('code')));
            $rol->setName(Req::_('name'));
            $rol->setDescription(Req::_('description'));
            $success = $rol->validate();
            if($success){
                $rol->save();
            }
        }
        $this->set('success', $success);
        $this->set('errors', $rol->getErrorsMap());
        $this->renderAsJSON();
    }
    
    public function delete()
    {
        $rol = ChoRolPeer::retrieveByPK($this->id);
        if(PageControl::canDelete()){
            $rol->delete();
        }
        $this->redirectTo(array('action' => 'index'));
    }
    
    public function ajaxUpdate()
    {
        $site = new Site();
        $modules = $site->allModules();
        $isProtected = false;
        if($this->id == ChocalaVars::MAIN_MODULE || $this->id ==
            ChocalaVars::ADMIN_MODULE){
            $isProtected = true;
        }
        $__Response = ':: Error ::';
        if(PageControl::canUpdate() && is_object($modules[$this->id]) &&
            Validation::isGet('newValue')){
            $field = URI::parameter(0);
            $newValue = Req::_('newValue');
            switch($field){
                case 'name':
                    if(Validation::isNotEmpty($newValue)){
                        $modules[$this->id]->setName($newValue);
                        $__Response = $newValue;
                    }
                    break;
                case 'access':
                    if($isProtected){
                        $__Response = $modules[$this->id]->getAccess();
                    }elseif(Validation::isNotEmpty($newValue)){
                        $modules[$this->id]->setAccess($newValue);
                        $__Response = $newValue;
                    }
                    break;
                case 'description':
                    $modules[$this->id]->setDescription($newValue);
                    $__Response = $newValue;
                    break;
                case 'main':
                    if($isProtected){
                        $__Response = '-----';
                    }elseif(isset($modules[$newValue])){
                        $modules[$this->id]->setModule($modules[$newValue]);
                        $__Response = $modules[$newValue]->getName();
                    }else{
                        $modules[$this->id]->withoutModule();
                        $__Response = '-----';
                    }
                    break;
                default:
                    break;
            }
            Site::saveToXML($modules);
        }
        $this->set('__Response', $__Response);
    }

    public function ajaxCombo()
    {
        $values = array();
        $value = null;
        $site = new Site();
        $modules = $site->allModules();
        $__Response = ':: Error ::';
        if(isset($modules[$this->id])){
            $field = URI::parameter(0);
            switch($field){
                case 'access':
                    $value = $modules[$this->id]->getAccess();
                    $values = PageConfigs::enumAccess('es');
                    break;
                case 'main':
                    $value = (is_object($modules[$this->id]->module())?
                        $modules[$this->id]->module()->URI(): '');
                    $values[''] = '- - - - -';
                    foreach($modules as $kMod => $mod){
                        if($kMod != $this->id){
                            $values[$kMod]=$mod->getName();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        $this->set('__ComboValues', $values);
        $this->set('__Value', $value);
    }

}