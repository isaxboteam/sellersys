<?php
/**
 * Created by PhpStorm.
 * User: Yecid
 * Date: 11/30/2015
 * Time: 9:25 p.m.
 */
class ShoppingController extends WebController
{

    public function _init()
    {
        parent::_init();
        $this->view->changeLayout('showroom');
    }

    public function index()
    {
        $vars  = SelProductQuery::create();
    }

    public function category()
    {
        $category = SelCategoryQuery::create()->findPk($this->id);
        $mainCategory = SelCategoryQuery::create()->findPk($category->getMainCategoryId());
        $products = SelProductQuery::create()
                ->filterBySelCategory($category)
                ->orderByName()
            ->find();
        $this->set('category', $category);
        $this->set('mainCategory', $mainCategory);
        $this->set('products', $products);
    }

    public function product_details()
    {
        $product = SelProductQuery::create()->findPk($this->id);
        $this->set('product', $product);
        $this->view->changeLayout('ajax');
    }

    public function addToCart()
    {
        $this->view->changeLayout('ajax');
        $product = SelProductQuery::create()->findPk($this->id);
        SessionOrder::addProduct($product, Req::_('quantity'));
        $this->set('product', $product);
        $this->renderView('shopping.product_cart', 'main');
   }

    public function removeToCart()
    {
        $this->view->changeLayout('ajax');
        $product = SelProductQuery::create()->findPk($this->id);
        SessionOrder::removeProduct($product);
        $this->set('product', $product);
        $this->renderView('shopping.product_cart', 'main');
    }


    public function account()
    {
        $this->view->changeLayout('account');
        $this->verifyCustomer();
        $customer = SessionOrder::customer();
        $orders = $customer->orders();
        $this->set('customer', $customer);
        $this->set('orders', $orders);
    }

    public function verifyCustomer()
    {
        if(!SessionOrder::hasCustomer()){
            $this->redirectTo(array('uri' => 'main/index/login'));
        }
    }

    public function search()
    {
        $term = Req::_('term');
        $products = SelProductQuery::create()
                ->filterByName('%'.$term.'%', Criteria::LIKE)
                ->_or()
                ->filterByModel('%'.$term.'%', Criteria::LIKE)
                ->_or()
                ->filterByDescription('%'.$term.'%', Criteria::LIKE)
                ->_or()
                ->useSelCategoryQuery()
                    ->filterByName('%'.$term.'%', Criteria::LIKE)
                ->endUse()
            ->find();
        $this->set('products', $products);
        $this->set('term', $term);
    }

}