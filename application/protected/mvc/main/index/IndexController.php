<?php
/**
 * Description of IndexController
 *
 * @author ypra
 */
class IndexController extends WebController
{
    
    public function index()
    {

    }

    public function test()
    {
    }

    public function login()
    {
        $this->view->changeLayout('access');
    }

    public function access()
    {
        $customemrAccount = SelCustomerAccountQuery::create()
                ->filterByEmail(Req::_('email'))
                ->filterByPassword(Req::_('password'))
            ->findOne();
        if(is_object($customemrAccount)){
            SessionOrder::setCustomer($customemrAccount->customer());
            $this->redirectTo(array('uri' => 'main/shopping/account'));
        }else{
            $this->redirectTo(array('uri' => 'main/index/login'));
        }
    }

    public function createAccount()
    {
        $custommerAccount = new SelCustomerAccount();
        $custommerAccount->fromArray(Req::all(), BasePeer::TYPE_STUDLYPHPNAME);
        $custommer = new SelCustomer();
        $custommer->fromArray(Req::all(), BasePeer::TYPE_STUDLYPHPNAME);
        $custommer->setSelCustomerAccount($custommerAccount);

        $success = $custommerAccount->validate();
        if($success){
            $success = $custommer->validate();
            if($success){
                $custommerAccount->setLastAccess(date('Y-m-d'));
                $custommer->save();
                SessionOrder::setCustomer($custommer);
            }else{
                $this->set('errors', $custommer->getErrorsMap());
            }
        }else{
            $this->set('errors', $custommerAccount->getErrorsMap());
        }
        $this->set('success', $success);
        $this->renderAsJSON();
    }

    public function logout()
    {
        SessionOrder::setCustomer();
        $this->redirectTo(array('uri' => 'index.htm'));
    }

}