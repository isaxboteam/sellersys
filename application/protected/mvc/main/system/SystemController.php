<?php
Chocala::import('Mvc.generics.SellerPrivateController');
/**
 * Description of SystemController
 *
 * @author ypra
 */
class SystemController extends SellerPrivateController
{

    public function _init()
    {
        parent::_init();
        $this->view->changeLayout('black');
    }

    public function index()
    {
        $this->redirectTo(array('action' => 'access'));
    }

    public function access()
    {
        if(UserControl::isLoggedIn()){
            $this->redirectTo(array('action' => 'admin'));
        }
    }

    public function naccess()
    {

    }
    
    public function login()
    {
        if(UserControl::login(Req::_('user'), Req::_('password'))){
            if(UserControl::hasRol('SUPER') || UserControl::hasRol('CODE')){
                $this->redirectTo(array('uri' => 'main/system/admin?management=2'));
            }elseif(UserControl::hasRol('ADM-CEN')){
                $this->redirectTo(array('module' => 'main',
                    'controller' => 'adminCenter', 'action' => 'admin'));
            }else{
                $this->redirectTo(array('action' => 'admin'));
            }
        }else{
            $this->redirectTo(array('action' => 'access', 'params' => ['pass' => 'error']));
        }
    }

    public function mensual()
    {
        $availableMonths = [];
        $initDate = Configs::value('statistics.initDate')?: '1-1-'.date('y');
        $initialMonth = DateUtil::createFromFormat('d-m-Y', $initDate);
        $endMonth = new DateUtil('now');

        $monthOption = clone $initialMonth;
        do{
//            $availableMonths[$monthOption->format('m-Y')] = $monthOption->format('F/Y');
            $availableMonths[] = $monthOption->format('m-Y');
            $monthOption->add(new DateInterval('P1M'));
        }while($endMonth ->diff($monthOption)->invert > 0);
        $this->set("availableMonths", $availableMonths);

        $month = Req::has('month')? Req::_('month'): date('m-Y');
        $dBase = '1-'.$month;
        $dMonthIni = DateUtil::createFromFormat("d-m-Y", $dBase);
        $dMonthCurrent = clone $dMonthIni;
        $dMonthNext = clone $dMonthCurrent;
        $dMonthNext->modify("+1 day");
        $dMonthFin = clone $dMonthCurrent;
        $dMonthFin->modify("+1 month");
        $dMonthFin->modify("-1 day");

        $totalAcquisitionsPeriodo = 0;
        $totalSalesPeriodo = 0;
        do{
            $acquisitionsAux = 0;
            $salesAux = 0;
            if($dMonthNext->getTimestamp() <= time()){
                $dayAcquisitions = SelAcquisitionQuery::create()
                    ->filterByDate($dMonthCurrent, Criteria::GREATER_EQUAL)
                    ->filterByDate($dMonthNext, Criteria::LESS_THAN)
                    ->find();
                foreach ($dayAcquisitions as $dayAcquisition) {
                    $acquisitionsAux+= $dayAcquisition->totalPrice();
                }
                $daySales = SelSaleQuery::create()
                    ->filterByDate($dMonthCurrent, Criteria::GREATER_EQUAL)
                    ->filterByDate($dMonthNext, Criteria::LESS_THAN)
                    ->find();
                foreach ($daySales as $daySale) {
                    $salesAux+= $daySale->totalPrice();
                }
            }
            $monthAcquisitions[] = array(
                'day'   => $dMonthCurrent->format('d/M'),
                'sales' => $acquisitionsAux
            );
            $monthSales[] = array(
                'day'   => $dMonthCurrent->format('d/M'),
                'sales' => $salesAux
            );

            $totalAcquisitionsPeriodo+= $acquisitionsAux;
            $totalSalesPeriodo+= $salesAux;

            $dMonthCurrent->modify('+1 day');
            $dMonthNext->modify('+1 day');
        }while($dMonthCurrent <= $dMonthFin);

        $this->set('dMonthIni', $dMonthIni);
        $this->set('dMonthCurrent', $dMonthCurrent);
        $this->set('monthAcquisitions', $monthAcquisitions);
        $this->set('monthSales', $monthSales);
        $this->set('totalAcquisitionsPeriodo', $totalAcquisitionsPeriodo);
        $this->set('totalSalesPeriodo', $totalSalesPeriodo);
    }

    public function anual()
    {
        $availableYears = [];
        $initDate = Configs::value('statistics.initDate')?: '1-1-'.date('y');
        $initialYear = DateUtil::createFromFormat('d-m-Y', '1-1-'.DateUtil::createFromFormat('d-m-Y', $initDate)->format('Y'));
        $endYear = new DateUtil('now');

        $yearOption = clone $initialYear;
        do{
            $availableYears[] = $yearOption->format('Y');
            $yearOption->add(new DateInterval('P1Y'));
        }while($endYear->diff($yearOption)->invert > 0);
        $this->set("availableYears", $availableYears);

        $year = Req::has('year')? Req::_('year'): date('Y');
        $dBase = '1-1'.$year;
//        $dYearIni = DateUtil::createFromFormat("d-m-Y", $dBase);


        $year = Req::has('year')? Req::_('year'): date('Y');
        $dBase = '1-1-'.$year;
        $dYearIni = DateUtil::createFromFormat("d-m-Y", $dBase);


//        $dYearIni = clone $initialYear;
        $dYearCurrent = clone $dYearIni;
        $dYearNext = clone $dYearCurrent;
        $dYearNext->modify("+1 month");
        $dYearFin = clone $dYearCurrent;
        $dYearFin->modify("+1 year");
        $dYearFin->modify("-1 day");

        $totalAcquisitionsPeriodo = 0;
        $totalSalesPeriodo = 0;
        do{
            $acquisitionsAux = 0;
            $salesAux = 0;
            if($dYearCurrent->getTimestamp() <= time()){
                $monthAcquisitions = SelAcquisitionQuery::create()
                        ->filterByDate($dYearCurrent, Criteria::GREATER_EQUAL)
                        ->filterByDate($dYearNext, Criteria::LESS_THAN)
                    ->find();
                foreach ($monthAcquisitions as $monthAcquisition) {
                    $acquisitionsAux+= $monthAcquisition->totalPrice();
                }
                $monthSales = SelSaleQuery::create()
                        ->filterByDate($dYearCurrent, Criteria::GREATER_EQUAL)
                        ->filterByDate($dYearNext, Criteria::LESS_THAN)
                    ->find();
                foreach ($monthSales as $monthSale) {
                    $salesAux+= $monthSale->totalPrice();
                }
            }
            $yearAcquisitions[] = array(
                'month'   => $dYearCurrent->format('M/Y'),
                'sales' => $acquisitionsAux
            );
            $yearSales[] = array(
                'month'   => $dYearCurrent->format('M/Y'),
                'sales' => $salesAux
            );

            $totalAcquisitionsPeriodo+= $acquisitionsAux;
            $totalSalesPeriodo+= $salesAux;

            $dYearCurrent->modify('+1 month');
            $dYearNext->modify('+1 month');
        }while($dYearCurrent <= $dYearFin);

        $this->set('dYearIni', $dYearIni);
        $this->set('dYearCurrent', $dYearCurrent);
        $this->set('yearAcquisitions', $yearAcquisitions);
        $this->set('yearSales', $yearSales);
        $this->set('totalAcquisitionsPeriodo', $totalAcquisitionsPeriodo);
        $this->set('totalSalesPeriodo', $totalSalesPeriodo);
    }

    public function admin()
    {
        if(UserControl::isLoggedIn()){
            setlocale(LC_TIME, 'es_BO', 'Spanish_Spain');
            $type = Req::has('type')? Req::_('type'): 'month';
            $this->set('user', UserControl::user());
            if($type == 'month'){
                $this->mensual();
            }else{
                $this->anual();
            }

            $this->view->changeLayout('private');
//            $month = Req::has('month')? Req::_('month'): date('m');
            $month = date('m');
            $dBase = '1-'.$month.'-'.date('Y');
            $dBase2 = '2-'.$month.'-'.date('Y');
            $dIni = DateUtil::createFromFormat("d-m-Y", $dBase);
            $dFin = DateUtil::createFromFormat("d-m-Y", $dBase);
            $dFin->modify("+1 month");
            $totalCompras = 0;
            $comprasMes = SelAcquisitionQuery::create()
                    ->filterByDate($dIni, Criteria::GREATER_EQUAL)
                    ->filterByDate($dFin, Criteria::LESS_THAN)
                ->find();
            foreach($comprasMes as $acquisition){
                $totalCompras+= $acquisition->totalPrice();
            }

            $totalVentas = 0;
            $ventasMes = SelSaleQuery::create()
                    ->filterByDate($dIni, Criteria::GREATER_EQUAL)
                    ->filterByDate($dFin, Criteria::LESS_THAN)
                ->find();
            foreach($ventasMes as $sale){
                $totalVentas+= $sale->totalPrice();
            }

            $variacion = ($totalCompras>$totalVentas? -1: 1) * $totalVentas/$totalCompras;

            $dAux = DateUtil::createFromFormat("d-m-Y", $dBase);
            $dNext = DateUtil::createFromFormat("d-m-Y", $dBase2);
            $monthAcquisitions = array();
            $monthSales = array();
            while($dAux < $dFin){
                $acquisitionsAux = 0;
                $salesAux = 0;
                if($dAux->getTimestamp() <= time()){
                    $dayAcquisitions = SelAcquisitionQuery::create()
                            ->filterByDate($dAux, Criteria::GREATER_EQUAL)
                            ->filterByDate($dNext, Criteria::LESS_THAN)
                        ->find();
                    foreach ($dayAcquisitions as $dayAcquisition) {
                        $acquisitionsAux+= $dayAcquisition->totalPrice();
                    }
                    $daySales = SelSaleQuery::create()
                            ->filterByDate($dAux, Criteria::GREATER_EQUAL)
                            ->filterByDate($dNext, Criteria::LESS_THAN)
                        ->find();
                    foreach ($daySales as $daySale) {
                        $salesAux+= $daySale->totalPrice();
                    }
                }
                $monthAcquisitions[] = array(
                    'day'   => $dAux->format('d'),
                    'sales' => $acquisitionsAux
                );
                $monthSales[] = array(
                    'day'   => $dAux->format('d'),
                    'sales' => $salesAux
                );
                $dAux->modify('+1 day');
                $dNext->modify('+1 day');
            }
            $this->set('dIni', $dIni);
            $this->set('mes', $dIni->format('M/Y'));
            $this->set('totalCompras', $totalCompras);
            $this->set('totalVentas', $totalVentas);
            $this->set('variacion', $variacion);
            $this->set('type', $type);
        }else{
            $this->redirectTo(array('controller' => 'system', 'action' => 'naccess'));
        }
    }

    public function logout()
    {
        if(UserControl::logout()){
            $this->redirectTo(array('action' => 'index'));
        }
    }

    public function forgot()
    {
        $user = ChoUserQuery::create()->findOneByUsername(Req::_('sent'));
        $this->set('user', $user);
    }

    public function forgotPassword()
    {
        $user = ChoUserQuery::create()->findOneByUsername(Req::_('user'));
        if(is_object($user)){
            sleep(4);

            $this->redirectTo(array('action' => 'forgot', 'params' => ['sent' => $user->getUsername()]));
        }else{
            $this->redirectTo(array('action' => 'forgot', 'params' => ['user' => 'no']));
        }

    }

    public function bigButtons()
    {
        $this->view->changeLayout('ajax');
    }

}