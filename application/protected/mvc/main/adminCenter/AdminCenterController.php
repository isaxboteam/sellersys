<?php
Chocala::import('Mvc.generics.SellerPrivateController');
/**
 * Description of SystemController
 *
 * @author ypra
 */
class AdminCenterController extends SellerPrivateController
{

    public function index()
    {
        $this->redirectTo(array('action' => 'admin'));
    }

    public function admin()
    {
        if(UserControl::isLoggedIn()){
            $this->set('branches', $this->_allowedBranches());
            $this->set('user', UserControl::user());
            if($_GET['uri'] != ''){
                $this->set('uri', $_GET['uri']);
            }
        }else{
            $this->redirectTo(array('controller' => 'system', 'action' => 'naccess'));
        }
    }

    public function open()
    {
        $branch = SelBranchPeer::retrieveByPK($this->id);
        if(is_object($branch)) {
            Session::set('branch', $branch);
            if($_GET['uri']!=''){
                $this->redirectTo(array('uri' => $_GET['uri']));
            }else{
                $this->redirectTo(array('uri' => 'sales/sale'));
            }
        }else{
            $this->redirectTo(array('action'=>'admin'));
        }
    }

    public function branchPanel()
    {
        if(!is_object($this->sessionBranch)){
            $this->redirectTo(array('action'=>'admin'));
        }
        $this->set('branch', $this->sessionBranch);
    }

    public function profile()
    {
        if(UserControl::isLoggedIn()){
            $this->set('branches', $this->_allowedBranches());
            $this->set('user', UserControl::user());
            if($_GET['uri'] != ''){
                $this->set('uri', $_GET['uri']);
            }
        }else{
            $this->redirectTo(array('controller' => 'system', 'action' => 'naccess'));
        }
    }

    public function editProfile()
    {
        if(UserControl::isLoggedIn()){
            $this->view->changeLayout('ajax');
            $this->set('user', UserControl::user());
        }else{
            $this->redirectTo(array('controller' => 'system', 'action' => 'naccess'));
        }
    }

    public function editContrasena()
    {
        if(UserControl::isLoggedIn()){
            $this->view->changeLayout('ajax');
            $this->set('user', UserControl::user());
        }else{
            $this->redirectTo(array('controller' => 'system', 'action' => 'naccess'));
        }
    }

    public function updateProfile(){
        $restrictions = true;
        $success = false;
        $user = null;
        $errorsMap = array();
        if (UserControl::isLoggedIn()) {
            $user = UserControl::user();
            $data = Req::all();
            if(isset($data['Birthday'])){
                $data['Birthday'] = DateUtil::mysql($data['Birthday']);
            }
            if (is_object($user)) {
                $user->fromArray($data);
                $success = $user->validate();
                $errorsMap = $user->getErrorsMap();
//                echo $data['PasswordR']; exit();
                /*if(isset($data['Password']) && $data['Password']=='' && $data['PasswordR'] == $data['Password']){
                    unset($data['Password']);
                }else*/if(isset($data['Password']) && $data['Password']!='' && $data['Password'] != $data['PasswordR']){
                    array_push($errorsMap,array('field'=>'Password', 'message' => 'Las contraseñas deben ser iguales'));
                    $restrictions = false;
                }else if(isset($data['Password']) && strlen(trim($data['Password']))<6){
                    array_push($errorsMap,array('field'=>'Password', 'message' => 'La contraseña debe ser mayor a 6 caracteres'));
                    $restrictions = false;
                }
                if(isset($data['Username'])){
                    $successUserName = ChoUserQuery::create()->filterByUsername($data['Username'])->filterById($user->getId(),Criteria::NOT_EQUAL)->findOne();
                    if(is_object($successUserName)){
                        array_push($errorsMap,array('field'=>'username','message' => 'El nombre de usuario ya existe'));
                        $restrictions = false;
                    }
                }
                if(isset($data['Ext']) && $data['Ext']==''){
                    array_push($errorsMap,array('field'=>'Ext','message' => 'Tiene que seleccionar un valor para este campo'));
                    $restrictions = false;
                }
                if($success && $restrictions){
                    $user->save();
                }
                $this->set('errors', $errorsMap);
            }
        }
        $this->set('success', $success && $restrictions);
        $this->renderAsJSON();
    }

    public function loadImage()
    {
        $user = UserControl::user();
        $this->set('user', $user);
        $this->view->changeLayout('ajax');
    }

    public function uploadImage()
    {
        $user = UserControl::user();
        $imagenFile = new Image($_FILES['imagen']);
        $user->setPhotoFormat($_FILES['imagen']['type']);
        if($user->validate()){
            $user->save();
            $imagenFile->saveResizeMax(IMG_DIR.$user->imageLocation(), 256);
//            $imagenFile->saveAs(IMG_DIR.$user->imageLocation());
        }
        $this->redirectTo(array('uri'=>'main/adminCenter/profile/'));
    }



}