<?php
Chocala::import("Mvc.generics.SellerPrivateController");

class ProviderController extends SellerPrivateController
{

    public function index()
    {        
    }
    
    public function dataList()
    {
        $providers = SelProviderQuery::create()->orderByCompany()->find();
        $this->set('providers', $providers);
        $this->view->changeLayout('ajax');
    }

    public function create()
    {
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $provider = new SelProvider();
        $provider->fromArray(Req::all());
        $success = $provider->validate();
        if($success){
            $provider->save();
        }
        $this->set('success', $success);
        $this->set('errors', $provider->getErrorsMap());
        $this->renderAsJSON();
    }
    
    public function edit()
    {
        $provider = SelProviderQuery::create()->findPk($this->id);
        $this->set('provider', $provider);
        $this->view->changeLayout('ajax');
    }
   
    public function update()
    {
        $provider = SelProviderQuery::create()->findPk($this->id);
        $provider->fromArray(Req::all());
        $success = $provider->validate();
        if($success){
            $provider->save();
        }
        $this->set('success', $success);
        $this->set('errors', $provider->getErrorsMap());
        $this->renderAsJSON();
    }

    public function details()
    {
        $provider = SelProviderQuery::create()->findPk($this->id);
        $acquisitions = $provider->acquisitions();
        $this->set('acquisitions', $acquisitions);
        $this->set('provider', $provider);
        $this->view->changeLayout('ajax');
    } 
    
    public function delete()
    {
        $provider = SelProviderQuery::create()->findPk($this->id);
        $acquisitionProducts=$provider->getSelAcquisitions();
        if(sizeof($acquisitionProducts)==0){
            $provider->delete();
        }else{
            $this->set('errors', 'Existen adquisiciones de productos registrados con el proveedor'.$provider->getCompany());
        }
        $this->redirectTo(array('action' => 'index'));
    }

    public function acquisition()
    {
        $acquisition = SelAcquisitionPeer::retrieveByPK($this->id);
        $productAcquisitions = $acquisition->productAcquisitions();
        $this->set('acquisition', $acquisition);
        $this->set('productAcquisitions', $productAcquisitions);
        $this->set('provider', $acquisition->getSelProvider());
    }

    public function pdfReport()
    {
        $providers = SelProviderQuery::create()->orderByCompany()->find();
        ob_start();
        require_once("pdfReport.phtml");
        $content = ob_get_contents();
        define(PDF_PAGE_ORIENTATION, 'L');
        $this->printPdfHorizontal($content);
    }

}