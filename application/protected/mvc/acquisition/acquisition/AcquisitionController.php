<?php
Chocala::import("Mvc.generics.SellerPrivateController");

class AcquisitionController extends SellerPrivateController
{

    public function index()
    {        
    }

   public function dataList()
    {
        $acquisitionProducts = SelAcquisitionQuery::create()->orderByDate(Criteria::DESC)->find();
        $this->set('acquisitionProducts', $acquisitionProducts);
        $this->view->changeLayout('ajax');
    }
    
    public function create()
    {
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $acquisition = new SelAcquisition();
        $params = Req::all();
        $params['Date'] = Req::_asDate('Date');
        $params['Money'] = SelProductAcquisition::BS;
        $acquisition->fromArray($params);
        $success = $acquisition->validate();
        if($success){
            $acquisition->save();
            Session::set('acquisition', $acquisition);
        }
        $this->set('success', $success);
        $this->set('errors', $acquisition->getErrorsMap());
        $this->renderAsJSON();
    }

    public function createStep2()
    {
        $acquisition = Session::get('acquisition');
//        $acquisition = new SelAcquisition();
        $margenGanancia = 0;
        foreach($acquisition->getSelProductAcquisitions() as $productAcquisition){
            $margenGanancia+= ($productAcquisition->getSelProduct()->getBasePrice() - $productAcquisition->getPrice()) *
                $productAcquisition->getQuantity();
        }
        $this->set('margenGanancia', $margenGanancia);
        $this->set('acquisition', $acquisition);
    }

    public function saveProductAcquisition()
    {
        $acquisition = Session::get('acquisition');
        $productAcquisition = new SelProductAcquisition();
        $productAcquisition->setSelAcquisition($acquisition);
        $productAcquisition->fromArray(Req::all());
        $productAcquisition->setMoney(SelProductAcquisition::BS);
        $success = $productAcquisition->validate();
//        print_r($productAcquisition->getErrorsMap()); exit();
        if($success){
            $con = Propel::getConnection(SelProductAcquisitionPeer::DATABASE_NAME);
            $con->beginTransaction();
            for($i=1;$i<=$productAcquisition->getQuantity();$i++){
                $productItem = new SelProductItem();
                $productItem->setSelProductAcquisition($productAcquisition);
                $productItem->setCode($productAcquisition->getSelProduct()->getId().'-'.time().'-'.$i);
                $productItem->setCost($productAcquisition->getPrice());
                $productItem->setPrice($productAcquisition->getSelProduct()->getBasePrice());
                $productItem->setStatus(SelProductItem::IN_ORDER);
                $productItem->setSelBrand($productAcquisition->getSelProduct()->getSelBrand());
                $productItem->setWarrantySell($productAcquisition->getWarrantyProvider());
                $productAcquisition->addSelProductItem($productItem);
            }
            if($productAcquisition->validate()){
                $productAcquisition->save($con);
            }else{
                print_r($productAcquisition->getErrorsMap()); exit();
            }
            $this->set('errors', $productAcquisition->getErrorsMap());
            try{
                $success = $con->commit();
            }catch (Exception $e){
                $con->rollBack();
                $success = false;
            }
            $this->set('id', $acquisition->getId());
        }
        $this->set('success', $success);
        $this->renderAsJSON();
    }

    public function details()
    {
        Session::delete('acquistion');
        $acquisition = SelAcquisitionPeer::retrieveByPK($this->id);
        $productAcquisitions = $acquisition->productAcquisitions();
        $this->set('acquisition', $acquisition);
        $this->set('provider', $acquisition->getSelProvider());
        $this->set('productAcquisitions', $productAcquisitions);
    }

    public function edit()
    {
        $acquisition = SelAcquisitionQuery::create()->findPk($this->id);
        $providers = SelProviderQuery::create()
            ->orderByCompany()
            ->find();
        $this->set('acquisition', $acquisition);
        $this->set('providers', $providers);
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $acquisition = SelAcquisitionQuery::create()->findPk($this->id);
        $params = Req::all();
        $params['Date'] = Req::_asDate('Date');
        $params['Money'] = SelProductAcquisition::BS;
        $acquisition->fromArray($params);
        $success = $acquisition->validate();
        if($success){
            $acquisition->save();
        }
        $this->set('success', $success);
        $this->set('errors', $acquisition->getErrorsMap());
        $this->renderAsJSON();
    }

    public function delete()
    {
        $acquisicion = SelAcquisitionPeer::retrieveByPK($this->id);
        $acquisicion->delete();
        $this->redirectTo(array('action' => 'index'));
    }

    public function searchProveedor()
    {
        $term=Req::_('term');
        $providers=SelProviderQuery::create()
            ->where('SelProvider.Company LIKE ?', '%'.$term.'%')
            ->find();

        $data = array();
        foreach($providers as $provider){
            $data[] = array("id" => $provider->getId(), "name"=>$provider->getCompany());
        }
        $this->set("datos", $data);
        $this->renderAsJSON();
    }

    public function inputTable(){
        $acquisition=Session::get('acquisition');
        $brands = SelBrandQuery::create()
            ->orderByName()->find();
        $products = SelProductQuery::create()
            ->orderByName()->find();
        $i=Req::_('i');
        $this->set('moneys',SelProductAcquisition::enumMoneda());
        $this->set('i', $i);
        $this->set('brands', $brands);
        $this->set('products', $products);
        $this->set('acquisition', $acquisition);
        $this->view->changeLayout('ajax');
    }

    public function createProduct(){
        $acquisition=Session::get('acquisition');
        $brands = SelBrandQuery::create()
            ->orderByName()->find();
        $products = SelProductQuery::create()
            ->orderByName()->find();
        $i=Req::_('i');
        $this->set('moneys',SelProductAcquisition::enumMoneda());
        $this->set('i', $i);
        $this->set('brands', $brands);
        $this->set('products', $products);
        $this->set('acquisition', $acquisition);
        $this->view->changeLayout('ajax');
    }

    public function loadTable()
    {
        $acquisition=Session::get('acquisition');
        $productAcquisitions= $acquisition->productAcquisitions();
        $this->set('productAcquisitions', $productAcquisitions);
        $this->view->changeLayout('ajax');
    }

    public function loadTableProduct()
    {
        $acquisition=Session::get('acquisition');
        $productAcquisitions= $acquisition->productAcquisitions();
        $this->set('productAcquisitions', $productAcquisitions);
        $this->view->changeLayout('ajax');
    }

    public function searchProduct()
    {
        $term=Req::_('term');
        $products=SelProductQuery::create()
            ->where('SelProduct.Name LIKE ?', '%'.$term.'%')
            ->find();

        $data = array();
        foreach($products as $product){
            $data[] = array(
                "id" => $product->getId(),
                "name"=>$product->getName(),
                "basePrice"=>number_format($product->getBasePrice(), 2)
            );
        }
        $this->set("datos", $data);
        $this->renderAsJSON();
    }

    public function deleteProductAcquisition()
    {

    }

}