<?php
Chocala::import("Mvc.generics.SellerPrivateController");

class ProductAcquisitionController extends SellerPrivateController
{

    public function index()
    {        
    }

    public function dataList()
    {
        Session::delete('acquistion');
        $acquisition = SelAcquisitionPeer::retrieveByPK($this->id);
        $productAcquisitions = SelProductAcquisitionQuery::create()
                ->filterBySelAcquisition($acquisition)
                ->orderByQuantity()
            ->find();
        $this->set('acquisition', $acquisition);
        $this->set('productAcquisitions', $productAcquisitions);
    }
    
    public function create()
    {
        $acquisition=SelAcquisitionPeer::retrieveByPK($this->id);
        $brands = SelBrandQuery::create()
            ->orderByName()->find();
        $products = SelProductQuery::create()
            ->orderByName()->find();
        $this->set('moneys',SelProductAcquisition::enumMoneda());
        $this->set('brands', $brands);
        $this->set('products', $products);
        $this->set('acquisition', $acquisition);
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $productAcquisition = new SelProductAcquisition();
        $productAcquisition->setAquisitionId(Req::_('acquisitionId'));
        $productAcquisition->setProductId(Req::_('productId'));
        $productAcquisition->setQuantity(Req::_('quantity'));
        $productAcquisition->setPrice(Req::_('price'));
        $success = $productAcquisition->validate();
        if($success){
            $productAcquisition->save();
        }
        $this->set('success', $success);
        $this->set('errors', $productAcquisition->getErrorsMap());

        $con=Propel::getConnection(SelProductAcquisitionPeer::DATABASE_NAME);
        $con->beginTransaction();

        for($i=1;$i<=$productAcquisition->getQuantity();$i++){
            $productItem = new SelProductItem();
            $productItem->setProductAcquisitionId($productAcquisition->getId());
            $productItem->setBrandId(Req::_('brandId'));
            $productItem->setPrice($productAcquisition->getPrice());
            $productItem->setCost($productAcquisition->getPrice());
            $productItem->setStatus(SelProductItem::IN_ORDER);
//            $productItem->setMoney(Req::_('money'));
//            $productItem->setWarrantyProvider(Req::_('warrantyProvider'));
            $productItem->setWarrantySell(Req::_('warranty'));
            $productItem->save($con);
        }
        try{
            $con->commit();
        }catch (Exception $e){
            $con->rollBack();
        }


        $this->renderAsJSON();
    }

    public function edit()
    {
        $acquisitionProduct = SelProductAcquisitionQuery::create()->findPk($this->id);
        $providers = SelProviderQuery::create()
            ->orderByCompany()
            ->find();
        $this->set('productAcquisition', $acquisitionProduct);
        $this->set('providers', $providers);
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $acquisitionProduct = SelProductAcquisitionQuery::create()->findPk($this->id);
        $acquisitionProduct->setDate(Req::_('date'));
        $acquisitionProduct->setProviderId(Req::_('providerId'));
        $acquisitionProduct->setObservation(Req::_('observation'));
        $success = $acquisitionProduct->validate();
        if($success){
            $acquisitionProduct->save();
        }
        $this->set('success', $success);
        $this->set('errors', $acquisitionProduct->getErrorsMap());
        $this->renderAsJSON();
    }

    public function delete()
    {
        $productAcquisicion = SelProductAcquisitionPeer::retrieveByPK($this->id);
        $acquisicion = $productAcquisicion->getSelAcquisition();
        $productItems = $productAcquisicion->productItems();
        if(sizeof($productItems) == sizeof($productAcquisicion->notAssignedItems())){
            foreach($productItems as $productItem){
                $productItem->delete();
            }
            $productAcquisicion->delete();
        }
        //TODO: poner mensaje de que no se pudo eliminar porque existen asignaciones
        $this->redirectTo(array('uri' => 'acquisition/acquisition/details/'.$acquisicion->getId()));
    }
    
    public function detailsProduct()
    {
        $productAcquisition = SelProductAcquisitionPeer::retrieveByPK($this->id);
        $productItems = $productAcquisition->productItems();
        $this->set('productAcquisition', $productAcquisition);
        $this->set('productItems', $productItems);
        $this->view->changeLayout('ajax');
    }

    public function createAssingBranch()
    {
        $productAcquisition = SelProductAcquisitionPeer::retrieveByPK($this->id);
        $branchs = SelBranchQuery::create()->orderByNumber()->find();
        $this->set('productAcquisition',$productAcquisition);
        $this->set('branchs',$branchs);
        $this->view->changeLayout('ajax');
    }

    public function assingBranch()
    {
        $productAcquisition = SelProductAcquisitionPeer::retrieveByPK($this->id);
        $branch = SelBranchPeer::retrieveByPK(Req::_('branchId'));
        $cantidad = Req::_('cantidad');
        $con = Propel::getConnection(SelProductAcquisitionPeer::DATABASE_NAME);
        $con->beginTransaction();
        $productItems = $productAcquisition->notAssignedItems($cantidad);
        foreach($productItems as $productItem){
            $productItem->setBranchId($branch->getId());
            $productItem->setStatus(SelProductItem::STOCK);
//            $productItem->save($con);
        }
        try{
            $con->commit();
        }catch (Exception $e){
            $con->rollBack();
        }
        $this->redirectTo(array('uri' => 'acquisition/acquisition/details/'.
            $productAcquisition->getAquisitionId()));
    }

}