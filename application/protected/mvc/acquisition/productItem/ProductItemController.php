<?php

Chocala::import("Mvc.generics.SellerPrivateController");

class ProductItemController extends SellerPrivateController
{

    public function index()
    {        
    }

   public function dataList()
    {
        $productAcquisition = SelProductAcquisitionPeer::retrieveByPK($this->id);
        $productItems=SelProductItemQuery::create()
            ->filterBySelProductAcquisition($productAcquisition)
            ->find();
        $this->set('productAcquisition', $productAcquisition);
        $this->set('productItems', $productItems);
    }
    
    public function create()
    {
        $acquisitionProduct=SelProductAcquisitionPeer::retrieveByPK($this->id);
        $products = SelProductQuery::create()->find();
        $this->set('products',$products);
        $this->set('acquisitionProduct',$acquisitionProduct);
    }

    public function save()
    {
        $item = new SelProductItem();
        $item->setProductId(Req::_('productId'));
        $item->setPrice(Req::_('price'));
        $item->setCost(Req::_('cost'));
        $item->setMoney(Req::_('money'));
        $item->setColor(Reg::_('color'));
        $item->setWarrantyProvider(Req::_('warrantyProvider'));
        $item->setWarrantySell(Req::_('warrantySell'));
        $success = $item->validate();
        if($success){
            $item->save();
        }
        $this->set('success', $success);
        $this->set('errors', $item->getErrorsMap());
        $this->renderAsJSON();
    }
    
    public function edit()
    {
        $productItem = SelProductItemQuery::create()->findPk($this->id);
        $this->set('productItem', $productItem);
        $this->view->changeLayout('ajax');
    }
   
    public function update()
    {
        $item = SelProductItemQuery::create()->findPk($this->id);
        $item->setProductId(Req::_('productId'));
        $item->setPrice(Req::_('price'));
        $item->setCost(Req::_('cost'));
        $item->setMoney(Req::_('money'));
        $item->setColor(Reg::_('color'));
        $item->setWarrantyProvider(Req::_('warrantyProvider'));
        $item->setWarrantySell(Req::_('warrantySell'));
        $success = $item->validate();
        if($success){
            $item->save();
        }
        $this->set('success', $success);
        $this->set('errors', $item->getErrorsMap());
        $this->renderAsJSON();
    }

    public function show()
    {
        $item = SelProductItemQuery::create()->findPk($this->id);
        $this->set('item', $item);
        $this->view->changeLayout('ajax');
    } 
    
    public function delete()
    {
        $item = SelProductItemQuery::create()->findPk($this->id);
        $item->delete();
        $this->redirectTo(array('action' => 'index'));
    }

    public function target()
    {
        $dir = IMG_DIR.'barCodes'.DIRECTORY_SEPARATOR;
        $web = IMG_WEB.'barCodes/';
        $item = SelProductItemQuery::create()->findPk($this->id);
        $productName = $item->getSelProductAcquisition()->getSelProduct()->getName();
        $brandName = $item->getSelProductAcquisition()->getSelProduct()->getSelBrand()->getName();
        $categoryName = $item->getSelProductAcquisition()->getSelProduct()->getSelCategory()->getName();
        $item->getCode();
        $code = '125689365472365458';
        $code = $item->getCode();
        //new barCodeGenrator($code_number,0,'hello.gif');
        new BarCodeGenerator($code,1, $dir.$code.'.gif', 190, 100, true);
//        new BarCodeGenerator($code_number,0,IMG_DIR.'hello.gif', 190, 130, true);
        ob_start();
        require_once("barCode.phtml");
        $content = ob_get_contents();
        define(PDF_PAGE_ORIENTATION, 'L');
        $this->printPdf($content, 'A6');
        $this->render("Generado!");
    }

}