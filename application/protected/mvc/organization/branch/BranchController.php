<?php
Chocala::import("Mvc.generics.SellerPrivateController");

class BranchController extends SellerPrivateController
{

    public function index()
    {        
    }

   public function dataList()
    {
        $branchs = SelBranchQuery::create()->orderByName()->find();
        $this->set('branchs', $branchs);
        $this->view->changeLayout('ajax');
    }

    public function create()
    {
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $branch = new SelBranch();
        $branch->setCode(Req::_('code'));        
        $branch->setName(Req::_('name'));
        $branch->setAddress(Req::_('address'));
        $branch->setPhone(Req::_('phone'));
        $branch->setFax(Req::_('fax'));        
        $success = $branch->validate();
        if($success){
            $branch->save();
        }
        $this->set('success', $success);
        $this->set('errors', $branch->getErrorsMap());
        $this->renderAsJSON();
    }

    public function edit()
    {
        $branch = SelBranchQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()){
            $this->set('branch', $branch);
        }
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $branch = SelBranchQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()){
            $branch->setCode(Req::_('code'));
            $branch->setName(Req::_('name'));
            $branch->setAddress(Req::_('address'));
            $branch->setPhone(Req::_('phone'));
            $branch->setFax(Req::_('fax'));
            $success = $branch->validate();
            if($success){
                $branch->save();
            }
        }
        $this->set('success', $success);
        $this->set('errors', $branch->getErrorsMap());
        $this->renderAsJSON();
    }

    public function delete()
    {
        $branch = SelBranchPeer::retrieveByPK($this->id);
        /* if(PageControl::canDelete()){
             $branch->delete();
         }
         $this->redirectTo(array('action' => 'index'));*/
        try {
            $branch->delete();
        } catch (Exception $e) {
            echo "La entidad no pudo ser eleminida porque esta siendo referenciada";
        }
        $this->redirectTo(array('action' => 'index'));

    }

    public function details()
    {
        $branch = SelBranchPeer::retrieveByPK($this->id);
        $e = new Criteria();
        $c = new Criteria();
        $e->addAscendingOrderByColumn(SelBranchEmployeePeer::EMPLOYEE_ID);
        $c->addAscendingOrderByColumn(SelBranchCategoryPeer::CATEGORY_ID);
        $employees = $branch->getSelBranchEmployees($e);
        $categories = $branch->getSelBranchCategorys($c);
        $this->set('branch', $branch);
        $this->set('employees', $employees);
        $this->set('categories', $categories);

        $this->view->changeLayout('ajax');

    }

    public function createEmployee()
    {
        $branch = SelBranchQuery::create()->findPk($this->id);
        $employees = SelEmployeeQuery::create()
                ->filterByInitDate(date(), Criteria::LESS_EQUAL)
                ->filterByEndDate(date(), Criteria::GREATER_EQUAL)
                ->useChoUserQuery()
                    ->orderByFirstName()
                    ->orderBySecondName()
                    ->orderByFirstLastname()
                    ->orderBySecondLastname()
                ->endUse()
            ->find();
        $this->set('branch', $branch);
        $this->set('employees', $employees);
        $this->view->changeLayout('ajax');
    }

    public function saveBranchEmployee()
    {
        $success = false;
        $errors = array();
        if(Req::_('initDate') == "") {
            $errors = array("field" => "initDate",
                "message" => "* Este campo es requerido");
        }elseif(Req::_('endDate') == "") {
            $errors = array("field" => "endDate",
                "message" => "* Este campo es requerido");
        }else{
            $branchEmployee = new SelBranchEmployee();
            $branchEmployee->setBranchId(Req::_('branchId'));
            $branchEmployee->setEmployeeId(Req::_('employeeId'));
            $branchEmployee->setInitDate(Req::_asDate('initDate'));
            $branchEmployee->setEndDate(Req::_asDate('endDate'));
            $branchEmployee->setAdmin(true);
            $branchEmployee->setStatus('ACTIVE');
            $success = $branchEmployee->validate();
            if($success){
                $branchEmployee->save();
            }else{
                $errors = $branchEmployee->getErrorsMap();
            }
        }
        $this->set('success', $success);
        $this->set('errors', $errors);
        $this->renderAsJSON();
    }

    public function deleteEmployee()
    {
        $branchEmployee = SelBranchEmployeeQuery::create()->findPk($this->id);
        $branch = $branchEmployee->getSelBranch();
        $branchEmployee->delete();
        $this->redirectTo(array('uri' => 'organization/branch/'.$branch->getId()));
    }

    public function createCategory()
    {
        $branch = SelBranchQuery::create()->findPk($this->id);
        $category = SelCategoryQuery::create()
            ->orderByName()->find();
        $this->set('category', $category);
        $this->set('branch', $branch);
        $this->view->changeLayout('ajax');
    }

    public function searchEmployee()
    {
        $employee = SelEmployeeQuery::create()
                ->useChoUserQuery()
                    ->filterByFirstName("%".Req::_("term")."%")
                ->endUse()
            ->find();
        $data = array();
        foreach($employee as $employee){
            $data[] = array("id" => $employee->getId(), "nombreCompleto"=>$employee->getChoUser()->completeName());
        }
        $this->set("datos", $data);
        $this->renderAsJSON();
    }

    public function categorySave()
    {
        $success = false;
        $errors = array();
        $branchCategory = SelBranchCategoryQuery::create()
                ->filterByBranchId(Req::_('branchId'))
                ->filterByCategoryId(Req::_('categoryId'))
            ->findOne();
        if(is_object($branchCategory)) {
            $errors = array("field" => "categoryId",
                "message" => "* Esta categoria ya esta agregada a la sucursal");
        }else{
            $branchCategory = new SelBranchCategory();
            $branchCategory->setBranchId(Req::_('branchId'));
            $branchCategory->setCategoryId(Req::_('categoryId'));
            $branchCategory->setState(Req::_('state'));
            $branchCategory->setObservation(Req::_('observation'));
            $success = $branchCategory->validate();
            if($success){
                $branchCategory->save();
            }else{
                $errors = $branchCategory->getErrorsMap();
            }
        }
        $this->set('success', $success);
        $this->set('errors', $errors);
        $this->renderAsJSON();
    }

    public function deleteCategory()
    {
        $branchCategory = SelBranchCategoryQuery::create()->findPk($this->id);
        $branch = $branchCategory->getSelBranch();
        $branchCategory->delete();
        $this->redirectTo(array('uri' => 'organization/branch/'.$branch->getId()));
    }


    public function pdfReport()
    {
        $branchs = SelBranchQuery::create()->orderByName()->find();
        $this->set('branchs', $branchs);
        ob_start();
        require_once("pdfReport.phtml");
        $content = ob_get_contents();
        $this->printPdfVertical($content);
    }

}