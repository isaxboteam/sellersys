<?php
Chocala::import("Mvc.generics.SellerPrivateController");

class EmployeeController extends SellerPrivateController
{

    public function index()
    {  
    }

    public function imprimir()
    {

    }
        public function dataList()
    {
        $employees = SelEmployeeQuery::create()->orderByUserId()->find();
        $this->set('employees', $employees);
        $this->view->changeLayout('ajax');
    }
    
    public function create()
    {
//        $branchs = SelBranchQuery::create()->orderByName()->find();
//        $this->set('branchs', $branchs);
        $users = ChoUserQuery::create()->orderByFirstName()->find();
        $this->set('users', $users);
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $employee = new SelEmployee();
        $employee->setInitDate(Req::_asDate('initDate'));
        $employee->setEndDate(Req::_asDate('endDate'));
        $employee->setUserId(Req::_('userId'));
        $employee->setDetails( Req::_('details'));
        $success = $employee->validate();
        //echo $success; exit();
        if($success){
            $employee->save();
        }
        $this->set('success', $success);
        $this->set('errors', $employee->getErrorsMap());
        $this->set('userId', $employee? $employee->getId(): '');
        $this->renderAsJSON();
    }

    public function edit()
    {
        $employee = SelEmployeeQuery::create()->findPk($this->id);
        $this->set('employee', $employee);
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $employee = SelEmployeeQuery::create()->findPk($this->id);
        $employee->setInitDate(Req::_asDate('initDate'));
        $employee->setEndDate(Req::_asDate('endDate'));
//        $employee->setUserId(Req::_('userId'));
        $employee->setDetails(Req::_('details'));
        $success = $employee->validate();
        if($success){
            $employee->save();
        }
        $this->set('success', $success);
        $this->set('errors', $employee->getErrorsMap());
        $this->set('userId', $employee? $employee->getId(): '');
        $this->renderAsJSON();
    }

    public function delete()
    {
        $employee = SelEmployeeQuery::create()->findPk($this->id);
        $employee->delete();
        $this->redirectTo(array('action' => 'index'));
    }

    public function details()
    {

    }

    public function searchUsuario()
    {
        $users = ChoUserQuery::create()
            ->filterByFirstName("%".Req::_("term")."%")
            ->find();
        $data = array();
        foreach($users as $user){
            $data[] = array("id" => $user->getId(), "nombreCompleto"=>$user->completeName());
        }
        $this->set("datos", $data);
        //$this->set("datos", $users->toArray());
        $this->renderAsJSON();
    }

    public function asignarSucursal()
    {
        $employee = SelEmployeePeer::retrieveByPK($this->id);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(SelBranchEmployeePeer::ADMIN);
        $branchEmployees = $employee->getSelBranchEmployees($c);
        $this->set('employee', $employee);
        $this->set('branchEmployeesuris', $branchEmployees);
    }

    public function branchDataList()
    {
        $employee = SelEmployeeQuery::create()->findPk($this->id);
        $this ->set('enployee', $employee);
        $branchs = SelBranchQuery::create()
            ->orderByName()->find();
        $this->set('branchs', $branchs);
        $this->view->changeLayout('ajax');
    }

    public function saveBranchEmployee()
    {
        $branchEmployee = new SelBranchEmployee();
        $branchEmployee->setBranchId(Req::_('branchId'));
        $branchEmployee->setInitDate(Req::_('initDate'));
        $branchEmployee->setEndDate(Req::_('endDate'));
        $branchEmployee->setAdmin(Req::_('admin'));
        $branchEmployee->setEmployeeId(Req::_('employeeId'));
        if($branchEmployee->getSelBranch()){
            $branchEmployee->setPosition($branchEmployee->getSelBranch()->countSelBranchEmployees()+1);
        }
        if($branchEmployee->getSelEmployee()){
            $branchEmployee->setPosition($branchEmployee->getSelEmployee()->countSelBranchEmployees()+1);
        }
        $success = $branchEmployee->validate();
        if($success){
            $branchEmployee->save();
        }
        $this->set('success', $success);
        $this->set('errors', $branchEmployee->getErrorsMap());
        $this->renderAsJSON();
    }

    public function pdfReport()
    {
        $employees = SelEmployeeQuery::create()->orderByUserId()->find();
        $this->set('employees', $employees);
        ob_start();
        require_once("pdfReport.phtml");
        $content = ob_get_contents();
        $this->printPdfVertical($content);
    }

    public function printList()
    {
        ob_start();
        require_once("printList.phtml");
        $content = ob_get_contents();
        $this->printPdfVertical($content);
    }

}