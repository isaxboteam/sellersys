<?php
/**
 * Created by PhpStorm.
 * User: jcanqui
 * Date: 31/12/2014
 * Time: 02:42 PM
 */
Chocala::import("Mvc.generics.SellerBranchPrivateController");

class SaleController extends SellerBranchPrivateController
{

    public function index()
    {

    }

    public function dataList()
    {
        $branch = $this->sessionBranch;
        $employee = $this->_employee();
        $sales = SelSaleQuery::create()
                ->filterByBranchId($branch->getId())
                ->filterByStatus('DELETED',Criteria::NOT_EQUAL)
                ->orderByCode(Criteria::DESC)
            ->find();
        $this->set('employee', $employee);
        $this->set('branch', $branch);
        $this->set('sales', $sales);
        $this->view->changeLayout('ajax');
    }

    public function showSale()
    {
        $sale = SelSaleQuery::create()->findPk($this->id);
        /**@var $productSalesInstance SelProductSell*/
        $aProductSells = array();
        foreach($sale->getSelProductSells() as $productSalesInstance){
            $aProductItem = array(
                'IdPorduct' => $productSalesInstance->getProductId(),
                'namePorduct' => $productSalesInstance->getSelProduct()->getName(),
                'cantidad' => $productSalesInstance->getQuantity(),
                'precio' => $productSalesInstance->getPrice(),
                'precioTotal' => $productSalesInstance->totalPrice()
            );
            $aProductSells[$productSalesInstance->getProductId()]=$aProductItem;
        }
        $this->set('aProductSells', $aProductSells);
        $this->set('sale', $sale);
        $this->set('customer', $sale->getSelCustomer());
        $this->view->changeLayout('ajax');
    }

    public function createSale()
    {
        $employee = $this->_employee();
        $branch = $this->sessionBranch;
        if($this->isOpenedBranch){
            $sale = new SelSale();
            $sale->setDate(date('Y-m-d H:i:s'));
            $sale->setStatus(SelSale::OPEN);
            $sale->setSelBranch($branch);
            $sale->setSelEmployee($employee);
            Session::set('aProductSells', array());
            Session::set('sale', $sale);
            $this->set('sale', $sale);
        }
        $this->view->changeLayout('ajax');
    }

    public function tableSale()
    {
        $aProductSells = Session::get('aProductSells');
        $this->set('aProductSells', $aProductSells);
        $this->view->changeLayout('ajax');
    }

    public function preSaveProductItem()
    {
        $quantity = is_numeric(Req::_('cantidad'))? Req::_('cantidad'): 0;
        $product = SelProductQuery::create()->findPk(Req::_('productId'));
        $hasProduct = is_object($product);
        if($hasProduct){
            $countProductItem = $this->sessionBranch->countStockItems($product);
            if($countProductItem < $quantity){
                $quantity = $countProductItem;
            }
            if($quantity > 0){
                $aProductSells = Session::_('aProductSells');
                $aProductItem = array(
                    'product' => $product,
                    'idPorduct' => $product->getId(),
                    'name' => $product->getName(),
                    'price' => $product->getBasePrice(),
                    'quantity' => $quantity,
                    'totalPrice' => $quantity * $product->getBasePrice()
                );
                $aProductSells[$product->getId()] = $aProductItem;
                Session::set('aProductSells', $aProductSells);
            }
        }
        $this->set('cantidad',$quantity);
        $this->set('hasProduct', $hasProduct);
        $this->renderAsJSON();
    }

    public function saveSale()
    {
        $customer = SelCustomerQuery::create()->findPk(Req::_('customerId'));
        $success = false;
        $sale = Session::_('sale');
        $aProductSells = Session::_('aProductSells');
        $hasCustomer = is_object($customer);
        $countProductSells = sizeof($aProductSells);
        $countProducts = 0;
        foreach($aProductSells as $aProductSell){
            $product = SelProductQuery::create()->findPk($aProductSell['idPorduct']);
            $availabality = $this->sessionBranch->countStockItems($product);
            if($availabality >= $aProductSell['quantity']){
                $productItemList = $this->sessionBranch->stockItems($product, $aProductSell['quantity']);
                $productSell = new SelProductSell();
                $productSell->setSelProduct($product);
                $productSell->setPrice($aProductSell['price']);
                $productSell->setQuantity($aProductSell['quantity']);
                $productSell->setAddedDate(date('Y-m-d H:i:s'));
                foreach($productItemList as $productItem){
                    $productItem->setPrice($productSell->getPrice());
                    $productItem->setStatus(SelProductItem::SOLD);
                    $productSell->addSelProductItem($productItem);
                }
                $sale->addSelProductSell($productSell);
                $countProducts++;
            }else{
                $aProductSell['quantity'] = $availabality;
                $aProductSells[$aProductSell['idProduct']] = $aProductSell;
                $this->set('notAvailableProduct', $aProductSell);
                if($availabality == 0){
                    unset($aProductSells[$aProductSell['idProduct']]);
                }
            }
        }
        if($countProducts == $countProductSells){
            $sale->setSelCustomer($customer);
            $success = $sale->validate();
            if($success){
                $sale->save();
            } else {
                $this->set('errors', $sale->getValidationFailures());
            }
        }
        $this->set('hasCustomer', $hasCustomer);
        $this->set('success', $success);
        $this->renderAsJSON();
    }

    public function searchProduct()
    {
        $term=Req::_('term');
        $products=SelProductQuery::create()
            ->where('SelProduct.Name LIKE ?', '%'.$term.'%')
            ->find();
        $data = array();
        foreach($products as $product){
            $data[] = array("id" => $product->getId(), "name"=>$product->getName(), "nItem"=>SelProductItemQuery::byProductAndBranch($product->getId(),$this->sessionBranch->getId())->count());
        }
        $this->set("datos", $data);
        $this->renderAsJSON();
    }

    public function deleteItem()
    {
        $aProductSells = Session::get('aProductSells');
        unset($aProductSells[Req::_('productId')]);
        Session::set('aProductSells', $aProductSells);
        $this->renderAsJSON();
    }

    public function delete()
    {
        $sales = SelSalePeer::retrieveByPK($this->id);
        $sales->setStatus('DELETED');
        $sales->save();
        $this->redirectTo(array('action' => 'index'));
    }

    public function searchCustomer()
    {
        $term = '%'.Req::_('term').'%';
        $customers = SelCustomerQuery::create()
                ->filterByName($term, Criteria::LIKE)
                ->_or()
                ->filterByLastname($term, Criteria::LIKE)
                ->_or()
                ->filterByIdNumber($term, Criteria::LIKE)
            ->find();

        $data = array();
        foreach($customers as $customer){
            $data[] = array("id" => $customer->getId(), "name"=>$customer->completeName());
        }
        $this->set("datos", $data);
        $this->renderAsJSON();
    }

}