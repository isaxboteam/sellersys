<?php
Chocala::import("Mvc.generics.SellerPrivateController");

class CustomerController extends SellerPrivateController
{

    public function index()
    {        
    }

    public function dataList()
    {
        $customers = SelCustomerQuery::create()->orderByName()->find();
        $this->set('customers', $customers);
        $this->view->changeLayout('ajax');
    }

    public function create()
    {
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $customer = new SelCustomer();
        if(PageControl::canCreate()){
            $customer->setIdNumber(Req::_('idNumber'));
            $customer->setEmail(Req::_('email'));
            $customer->setName(Req::_('name'));
            $customer->setLastname(Req::_('lastname'));
            $customer->setAddress(Req::_('address'));
            $customer->setPhone(Req::_('phone'));
            $customer->setCellphone(Req::_('cellphone'));
            $success = $customer->validate();
            if($success){
                $customer->save();
            }
        }
        $this->set('success', $success);
        $this->set('errors', $customer->getErrorsMap());
        $this->renderAsJSON();
    }

    public function edit()
    {
        $customer = SelCustomerQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()){
            $this->set('customer', $customer);
        }
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $customer = SelCustomerQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()){
            $customer->setIdNumber(Req::_('idNumber'));
            $customer->setEmail(Req::_('email'));
            $customer->setName(Req::_('name'));
            $customer->setLastname(Req::_('lastname'));
            $customer->setAddress(Req::_('address'));
            $customer->setPhone(Req::_('phone'));
            $customer->setCellphone(Req::_('cellphone'));
            $success = $customer->validate();
            if($success){
                $customer->save();
            }
        }
        $this->set('success', $success);
        $this->set('errors', $customer->getErrorsMap());
        $this->renderAsJSON();
    }

    public function delete()
    {
        $customer = SelCustomerPeer::retrieveByPK($this->id);
        if(PageControl::canDelete()){
            $customer->delete();
        }
        $this->redirectTo(array('action' => 'index'));
    }

    public function pdfReport()
    {
        $customers = SelCustomerQuery::create()->orderByName()->find();
        ob_start();
        require_once("pdfReport.phtml");
        $content = ob_get_contents();
        define(PDF_PAGE_ORIENTATION, 'L');
        $this->printPdfHorizontal($content);
    }

}