<?php
/**
 * Created by PhpStorm.
 * User: jcanqui
 * Date: 31/12/2014
 * Time: 02:42 PM
 */
Chocala::import("Mvc.generics.SellerPrivateController");

class CatalogController extends SellerPrivateController
{

    public function index()
    {

    }

    public function dataList()
    {
        $branchs = $this->_allowedBranches();

        $categories=SelCategoryQuery::create()
            ->filterByMainCategoryId(null)
            ->find();

        $this->set('categories', $categories);
        $this->set('branchs', $branchs);
        $this->view->changeLayout('ajax');
    }

	public function dataListSubCategories(){
		$branchs = $this->_allowedBranches();
        $categories=SelCategoryQuery::create()
            ->filterByMainCategoryId($this->id)
            ->find();
        $category=SelCategoryPeer::retrieveByPK($this->id);
        $this->set('category', $category);
        $this->set('categories', $categories);
        $this->set('branchs', $branchs);
//        $this->view->changeLayout('ajax');
	}

	public function dataListProduct(){
        $category=SelCategoryPeer::retrieveByPK($this->id);
		$branchs=SelBranchQuery::create()
            ->orderByName()
            ->find();
        $products=SelProductQuery::create()
            ->filterByCategoryId($this->id)
            ->find();
        $this->set('category', $category);
        $this->set('products', $products);
        $this->set('branchs', $branchs);
        $this->view->changeLayout('ajax');
	}

    public function tableSucursal(){
        $productsItem=function($product){
            $branch=SelBranchPeer::retrieveByPK(Req::_('branchId'));
            return array("product"=>$product,"count"=>SelProductItemQuery::productItemByProduct($product,$branch)->count());
        };
        $products=SelProductQuery::create()
            ->orderByName()
            ->find();
        $masterProducts=array_map($productsItem,$products->getArrayCopy());
        $this->set('masterProducts', $masterProducts);
        $this->set('branch',SelBranchPeer::retrieveByPK(Req::_('branchId')));
        $this->view->changeLayout('ajax');
    }

    public function createAssingBranch(){
        $product=SelProductPeer::retrieveByPK($this->id);
        $productAcquisitions=SelProductAcquisitionQuery::create()
            ->filterByProductId($product->getId())
            ->useSelProductItemQuery()
                ->filterByStatus(SelProductItem::IN_ORDER)
            ->endUse()
            ->useSelAcquisitionQuery()
                ->orderByDate()
            ->endUse()
            ->setDistinct()
            ->find();
        $numProductcBranchLess=SelProductItemQuery::byProduct($product->getId())->count();
        $branchs=SelBranchQuery::create()->orderByName()->find();

        $this->set('productAcquisitions',$productAcquisitions);
        $this->set('numProductcBranchLess',$numProductcBranchLess);
        $this->set('branchs',$branchs);
        $this->set('product',$product);
        $this->view->changeLayout('ajax');
    }

    public function assingBranch(){

        $product=SelProductPeer::retrieveByPK($this->id);
        $cantidad=Req::_('cantidad');
        $branch= SelBranchPeer::retrieveByPK(Req::_('branchId'));

        $con=Propel::getConnection(SelProductPeer::DATABASE_NAME);
        $con->beginTransaction();

        $productItems=SelProductItemQuery::byProduct($product->getId())->find();
        $i=0;
        foreach($productItems as $productItem){
            if($i<$cantidad){
                $productItem->setBranchId($branch->getId());
                $productItem->setStatus(SelProductItem::STOCK);
                $productItem->save($con);
            }
            $i++;
        }
        try{
            $con->commit();
        }catch (Exception $e){
            $con->rollBack();
        }
        $this->redirectTo(array('action' => 'dataListProduct/'.$product->getCategoryId()));
    }

    public function saveAssingBranch(){
        $productAcquisition=SelProductAcquisitionPeer::retrieveByPK(Req::_('productAcquisitionId'));
        $product=SelProductPeer::retrieveByPK($productAcquisition->getProductId());

        $cantidad=Req::_('cantidad');
        $branch= SelBranchPeer::retrieveByPK(Req::_('branchId'));
        $con=Propel::getConnection(SelProductAcquisitionPeer::DATABASE_NAME);
        $con->beginTransaction();
        $productItems=SelProductItemQuery::create()
            ->filterByProductAcquisitionId($productAcquisition->getId())
            ->orderByBranchId()
            ->find();
        $i=0;
        foreach($productItems as $productItem){
            if($i<$cantidad){
                $productItem->setBranchId($branch->getId());
                $productItem->setStatus(SelProductItem::STOCK);
                $productItem->save($con);
            }
            $i++;
        }
        try{
            $con->commit();
        }catch (Exception $e){
            $con->rollBack();
        }

        $numProductcBranchLess=SelProductItemQuery::byProduct($product->getId())->count();
        $this->set('cantDisponible',$product->productItemByProductAcquisitionLess($productAcquisition));
        $this->set('cantDisponibleTotal',$numProductcBranchLess);
//        $data[] = array('cantDisponible' => $product->productItemByProductAcquisitionLess($productAcquisition));
//
//        foreach($users as $user){
//            $data[] = array("id" => $product->productItemByProductAcquisitionLess($productAcquisition), "nombreCompleto"=>$user->completeName());
//        }
//        $this->set("data", $data);
        $this->renderAsJSON();
    }

}