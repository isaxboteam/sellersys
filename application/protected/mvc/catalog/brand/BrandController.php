<?php

Chocala::import("Mvc.generics.SellerPrivateController");

class BrandController extends SellerPrivateController
{

    public function index()
    {        
    }

    public function dataList()
    {
        $brands = SelBrandQuery::create()->orderByName()->find();
        $this->set('brands', $brands);
        $this->view->changeLayout('ajax');
    }

    public function create()
    {
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $brand = new SelBrand();
        if(PageControl::canCreate()){
            //$brand->fromArray(Req::all(), BasePeer::TYPE_STUDLYPHPNAME);
            $brand->setCode(strtoupper(Req::_('code')));
            $brand->setName(Req::_('name'));
            $brand->setDescription(Req::_('description'));
            $success = $brand->validate();
            if($success){
                $brand->save();
            }
        }
        $this->set('success', $success);
        $this->set('errors', $brand->getErrorsMap());
        $this->renderAsJSON();
    }

    public function edit()
    {
        $brand = SelBrandQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()){
            $this->set('brand', $brand);
        }
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $brand = SelBrandQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()){
            //$brand->fromArray(Req::all(), BasePeer::TYPE_STUDLYPHPNAME);
            $brand->setCode(strtoupper(Req::_('code')));
            $brand->setName(Req::_('name'));
            $brand->setDescription(Req::_('description'));
            $success = $brand->validate();
            if($success){
                $brand->save();
            }
        }
        $this->set('success', $success);
        $this->set('errors', $brand->getErrorsMap());
        $this->renderAsJSON();
    }

    public function delete()
    {
        $brand = SelBrandPeer::retrieveByPK($this->id);
        if(PageControl::canDelete()){
            $brand->delete();
        }
        $this->redirectTo(array('action' => 'index'));
    }


    public function pdfReport()
    {
        $brands = SelBrandQuery::create()->orderByName()->find();
        $this->set('brands', $brands);
        ob_start();
        require_once("pdfReport.phtml");
        $content = ob_get_contents();
        $this->printPdfVertical($content);
    }

}