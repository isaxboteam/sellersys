<?php
Chocala::import("Mvc.generics.SellerPrivateController");

class ProductController extends SellerPrivateController
{

    public function index()
    {        
    }
    
    public function category()
    {
        $category = SelCategoryPeer::retrieveByPK($this->id);
        $products = SelProductQuery::create()->filterBySelCategory($category)->orderByName()->find();
        $this->set('products',$products );
        $this->set('category',$category);
    }

    public function categoryDataList()
    {
        $category = SelCategoryPeer::retrieveByPK($this->id);
        $products = $category->products();
        $this->set('category', $category);
        $this->set('products', $products);
        $this->view->changeLayout('ajax');
    }

    public function dataList(){
        $category = SelCategoryPeer::retrieveByPK($this->id);
        $products = $category->products();
        $this->set('category', $category);
        $this->set('products', $products);
    }
    
    public function create()
    {
        $category = SelCategoryPeer::retrieveByPK($this->id);
        $productGroups = $category->getSelProductGroups();
        $brands = SelBrandQuery::create()->orderByName()->find();
        $this->set('productGroups', $productGroups);
        $this->set('brands', $brands);
        $this->set('category', $category);
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $product = new SelProduct();
        $product->setProductGroupId(Req::_('productGroupId'));
        $product->setBrandId(Req::_('brandId'));
        $product->setCategoryId(Req::_('categoryId'));
        $product->setCode(Req::_('code'));
        $product->setName(Req::_('name'));
        $product->setBasePrice(Req::_('basePrice'));
        $product->setModel(Req::_('model'));
        $product->setDescription(Req::_('description'));
        $success = $product->validate();
        if($success){
            $product->save();
        }
        $this->set('success', $success);
        $this->set('errors', $product->getErrorsMap());
        $this->renderAsJSON();
    }

    public function edit()
    {
        $product = SelProductQuery::create()->findPk($this->id);
        $category = $product->getSelCategory();
        $productGroups = $category->getSelProductGroups();
        $brands = SelBrandQuery::create()->orderByName()->find();
        $this->set('productGroups', $productGroups);
        $this->set('brands', $brands);
        $this->set('category', $category);
        $this->set('product', $product);
        $this->view->changeLayout('ajax');
    }
   
    public function update()
    {
        $product = SelProductQuery::create()->findPk($this->id);
        $product->setProductGroupId(Req::_('productGroupId'));
        $product->setBrandId(Req::_('brandId'));
        $product->setCode(Req::_('code'));
        $product->setName(Req::_('name'));
        $product->setBasePrice(Req::_('basePrice'));
        $product->setModel(Req::_('model'));
        $product->setDescription(Req::_('description'));
        $success = $product->validate();
        if($success){
            $product->save();
        }
        $this->set('success', $success);
        $this->set('errors', $product->getErrorsMap());
        $this->redirectTo(array('action' => 'category/'.$product->getCategoryId()));
    }

    public function show()
    {
        $product = SelProductQuery::create()->findPk($this->id);
        $items = BaseSelProductItemQuery::create()
                ->useSelProductAcquisitionQuery()
                    ->filterBySelProduct($product)
                ->endUse()
            ->find();
        $this->set('product', $product);
        $this->set('items', $items);
    } 
    
    public function delete()
    {
        $product = SelProductQuery::create()->findPk($this->id);
        $idCategory=$product->getCategoryId();
        $product->delete();
        $this->redirectTo(array('action' => 'category/'.$idCategory));
    }
    
    public function subirImg()
    {
        $product = SelProductQuery::create()->findPk($this->id);
        $imagenFile = new Image($_FILES['imagen']);
        $productImage = new SelProductImage();
        $productImage->setName($_FILES['imagen']['name']);
        $productImage->setSize($_FILES['imagen']['size']);
        $productImage->setMimetype($_FILES['imagen']['type']);
        $productImage->setSelProduct($product);
        $productImage->generatePosition();
        $productImage->save();
        $imagenFile->saveAs(IMG_DIR.$productImage->imageLocation());
        $this->redirectTo(array('uri'=>'catalog/product/show/'.$product->getId()));
    }

    public function pdfReport()
    {
        $category = SelCategoryPeer::retrieveByPK($this->id);
        $products = SelProductQuery::create()->filterBySelCategory($category)->orderByName()->find();
        ob_start();
        require_once("pdfReport.phtml");
        $content = ob_get_contents();
        $this->printPdfVertical($content);
    }

}