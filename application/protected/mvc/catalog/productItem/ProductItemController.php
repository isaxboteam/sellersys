<?php
Chocala::import("Mvc.generics.SellerPrivateController");

class ProductItemController extends SellerPrivateController
{

    public function index()
    {        
    }

   public function dataList()
    {
        $product = SelProductPeer::retrieveByPK($this->id);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(SelProductItemPeer::PRODUCT_ID);
        $items = $product->getSelProductItems($c);
        $this->set('product', $product);
        $this->set('items', $items);
        $this->view->changeLayout('ajax');
    }
    
    public function create()
    {
        $acquisitionProduct=SelProductItemPeer::retrieveByPK($this->id);
        $products = SelProductQuery::create()->find();
        $this->set('products', $products);
        $this->set('acquisitionProduct', $acquisitionProduct);
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $item = new SelProductItem();
        $item->setProductId(Req::_('productId'));
        $item->setPrice(Req::_('price'));
        $item->setCost(Req::_('cost'));
        $item->setMoney(Req::_('money'));
        $item->setColor(Reg::_('color'));
        $item->setWarrantyProvider(Req::_('warrantyProvider'));
        $item->setWarrantySell(Req::_('warrantySell'));
        $success = $item->validate();
        if($success){
            $item->save();
        }
        $this->set('success', $success);
        $this->set('errors', $item->getErrorsMap());
        $this->renderAsJSON();
    }
    
    public function edit()
    {
        $item = SelProductItemQuery::create()->findPk($this->id);
        $this->set('item', $item);
        $this->view->changeLayout('ajax');
    }
   
    public function update()
    {
        $item = SelProductItemQuery::create()->findPk($this->id);
        $item->setProductId(Req::_('productId'));
        $item->setPrice(Req::_('price'));
        $item->setCost(Req::_('cost'));
        $item->setMoney(Req::_('money'));
        $item->setColor(Reg::_('color'));
        $item->setWarrantyProvider(Req::_('warrantyProvider'));
        $item->setWarrantySell(Req::_('warrantySell'));
        $success = $item->validate();
        if($success){
            $item->save();
        }
        $this->set('success', $success);
        $this->set('errors', $item->getErrorsMap());
        $this->renderAsJSON();
    }

    public function show()
    {
        $item = SelProductItemQuery::create()->findPk($this->id);
        $this->set('item', $item);
        $this->view->changeLayout('ajax');
    } 
    
    public function delete()
    {
        $item = SelProductItemQuery::create()->findPk($this->id);
        $item->delete();
        $this->redirectTo(array('action' => 'index'));
    }

}