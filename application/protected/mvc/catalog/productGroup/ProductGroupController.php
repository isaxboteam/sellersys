<?php
Chocala::import("Mvc.generics.SellerPrivateController");

class ProductGroupController extends SellerPrivateController
{

    public function category()
    {
        $category = SelCategoryPeer::retrieveByPK($this->id);
        $productGroups = SelProductGroupQuery::create()->filterBySelCategory($category)->orderByName()->find();
        $this->set('category',$category);
        $this->set('productGroups',$productGroups );
    }

    public function create()
    {
        $this->set('category', SelCategoryPeer::retrieveByPK($this->id));
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $productGroup = new SelProductGroup();
        $productGroup->setCategoryId(Req::_('categoryId'));
        $productGroup->setCode(Req::_('code'));
        $productGroup->setName(Req::_('name'));
        $productGroup->setDescription(Req::_('description'));
        $success = $productGroup->validate();
        if($success){
            $productGroup->save();
        }
        $this->set('success', $success);
        $this->set('errors', $productGroup->getErrorsMap());
        $this->renderAsJSON();
    }

    public function edit()
    {
        $productGroup = SelProductGroupQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()){
            $this->set('productGroup', $productGroup);
        }
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $productGroup = SelProductGroupQuery::create()->findPk($this->id);
        if(PageControl::canUpdate()){
            $productGroup->setCode(Req::_('code'));
            $productGroup->setName(Req::_('name'));
            $productGroup->setDescription(Req::_('description'));
            $success = $productGroup->validate();
            if($success){
                $productGroup->save();
            }
        }
        $this->set('success', $success);
        $this->set('errors', $productGroup->getErrorsMap());
        $this->renderAsJSON();
    }

    public function pdfReport()
    {
        $category = SelCategoryPeer::retrieveByPK($this->id);
        $productGroups = SelProductGroupQuery::create()->filterBySelCategory($category)->orderByName()->find();
        ob_start();
        require_once("pdfReport.phtml");
        $content = ob_get_contents();
        $this->printPdfVertical($content);
    }

}