<?php
Chocala::import("Mvc.generics.SellerPrivateController");

/**
 * User: Yecid
 * Date: 12/10/2015
 * Time: 10:46 a.m.
 */
class ProductImageController extends SellerPrivateController
{

    public function addImage()
    {
        $product = SelProductQuery::create()->findPk($this->id);
        $this->set('product', $product);
        $this->view->changeLayout('ajax');
    }

    public function uploadImage()
    {
        $product = SelProductQuery::create()->findPk($this->id);
        $imagenFile = new Image($_FILES['imagen']);
        $productImage = new SelProductImage();
        $productImage->setName(trim($_FILES['imagen']['name']));
        $productImage->setSize($_FILES['imagen']['size']);
        $productImage->setMimetype($_FILES['imagen']['type']);
        $productImage->setTitle(Req::_('title'));
        $productImage->setDescription(Req::_('description'));
        $productImage->setSelProductRelatedByProductId($product);
        $productImage->generatePosition();
        if($productImage->validate()){
            $productImage->save();
            $imagenFile->saveAs(IMG_DIR.$productImage->imageLocation());
            foreach(array_reverse(SelProductImage::uploadSizes()) as $size){
                $imagenFile->saveResizeMax(IMG_DIR.$productImage->imageLocation($size), $size);
            }
        }
        $this->redirectTo(array('uri'=>'catalog/product/show/'.$product->getId()));
    }

    public function edit()
    {
        $productImage = SelProductImageQuery::create()->findPk($this->id);
        $this->set('productImage', $productImage);
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $productImage = SelProductImageQuery::create()->findPk($this->id);
        $productImage->setTitle(Req::_('title'));
        $productImage->setDescription(Req::_('description'));
        $productImage->save();
        $this->redirectTo(array('uri' => 'catalog/product/show/'.$productImage->getProductId()));
    }

    public function makeMain()
    {
        $productImage = SelProductImageQuery::create()->findPk($this->id);
        $product = $productImage->getSelProductRelatedByProductId();
        $product->setMainImageId($productImage->getId());
        $product->save();
        $this->redirectTo(array('uri' => 'catalog/product/show/'.$productImage->getProductId()));
    }

    public function delete()
    {
        $productImage = SelProductImageQuery::create()->findPk($this->id);
        $product = $productImage->getSelProductRelatedByProductId();
        if($product->getMainImageId() == $productImage->getId()){
            $product->setMainImageId(null);
            $product->save();
        }
        $productImage->delete();
        $i = 1;
        $product->reload();
        foreach($product->images() as $productImage){
            $productImage->setPosition($i++);
            $productImage->save();
        }
        $this->redirectTo(array('uri' => 'catalog/product/show/'.$product->getId()));
    }

}