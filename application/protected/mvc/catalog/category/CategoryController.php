<?php
Chocala::import("Mvc.generics.SellerPrivateController");

class CategoryController extends SellerPrivateController
{

    public function index()
    {        
    }

    public function dataList()
    {
        $categories = SelCategoryQuery::principales()->find();
        $this->set('categories', $categories);
        $this->view->changeLayout('ajax');
    }

    public function subcategories()
    {
        $category = SelCategoryPeer::retrieveByPK($this->id);
        $this->set('category',$category);
        $this->view->changeLayout('ajax');
    }

    public function create()
    {
        $mainCategory = SelCategoryPeer::retrieveByPK($this->id);
        if(is_object($mainCategory)){
            $this->set('mainCategory',$mainCategory);
//        }else{
//            $mainCategories = SelCategoryQuery::create()->principales()->find();
//            $this->set('mainCategories',$mainCategories);
        }
        $this->view->changeLayout('ajax');
    }

    public function save()
    {
        $category = new SelCategory();
        $category->setMainCategoryId(Req::_('mainCategoryId'));
        $category->setCode(Req::_('code'));
        $category->setName(Req::_('name'));
        $category->setDescription(Req::_('description'));
        $success = $category->validate();
        if($success){
            $category->save();
        }
        $this->set('success', $success);
        $this->set('errors', $category->getErrorsMap());
        $this->renderAsJSON();
        //$this->redirectTo(array('action' => 'index'));
    }
    
    public function edit()
    {
        $category = SelCategoryQuery::create()->findPk($this->id);
        $this->set('category', $category);
        $this->view->changeLayout('ajax');
    }

    public function update()
    {
        $category = SelCategoryQuery::create()->findPk(Req::_('id'));
        $category->setCode(Req::_('code'));
        $category->setName(Req::_('name'));
//        $category->setMainAddress(Req::_('address'));
        $category->setDescription(Req::_('description'));
        $success = $category->validate();
        if($success){
            $category->save();
        }
        $this->set('success', $success);
        $this->set('errors', $category->getErrorsMap());
//        $this->renderAsJSON();
//        $this->view->changeLayout('ajax');
        $this->redirectTo(array('action' => 'index'));
    }

    public function show()
    {
        $category = SelCategoryQuery::create()->findPk($this->id);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(SelProductPeer::CODE);
        $product = $category->getSelProducts($c);
        $this->set('category', $category);
        $this->set('product', $product);
        $this->view->changeLayout('ajax');
    }
    
    public function delete()
    {
        $category = SelCategoryQuery::create()->findPk($this->id);
        $category->delete();
        $this->redirectTo(array('action' => 'index'));
    }

    public function dataListSubCategories()
    {
        $category = SelCategoryPeer::retrieveByPK($this->id);
        $products = $category->products();
        $this->set('category', $category);
        $this->set('products', $products);
    }

    public function pdfReport()
    {
        $categories = SelCategoryQuery::principales()->find();
        $this->set('categories', $categories);
        ob_start();
        require_once("pdfReport.phtml");
        $content = ob_get_contents();
        $this->printPdfVertical($content);
    }

    public function pdfReportSubcategories()
    {
        $category = SelCategoryPeer::retrieveByPK($this->id);
        $products = $category->products();
        $subcategories = $category->subcategories();
        ob_start();
        require_once("pdfReportSubcategories.phtml");
        $content = ob_get_contents();
        $this->printPdfVertical($content);
    }

}