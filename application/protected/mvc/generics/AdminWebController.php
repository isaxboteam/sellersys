<?php
Chocala::import("Model.security.PageControl");
Chocala::import("Model.security.UserControl");
/**
 * Description of AdminWebController
 *
 * @author ypra
 */
abstract class AdminWebController extends WebController
{

    public function _init()
    {
        //$__AG = new AjaxGrid();
        //$this->setVar('__AG', $__AG);
        $this->view->changeLayout('private');
        /*
        if(!PageControl::hasAccess()){
            $this->module = 'admin';
            $this->template = 'login';
        }
         */
    }

}