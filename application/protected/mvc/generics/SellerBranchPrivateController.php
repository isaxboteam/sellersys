<?php
include_once('SellerPrivateController.php');
/**
 * Description of SellerBranchPrivateController
 *
 * @author ypra
 */
abstract class SellerBranchPrivateController extends SellerPrivateController
{

    /**
     * @var SelBranch
     */
    protected $sessionBranch = null;

    /**
     * @var bool
     */
    protected $isAdmin = false;

    public function _init()
    {
        parent::_init();
        $this->sessionDate = new DateUtil('now');
        if(!is_object($this->sessionBranch)){
            $this->redirectTo(array('uri'=>'main/adminCenter/admin?uri='.URI::instance()->completeURI()));
        }
    }

}