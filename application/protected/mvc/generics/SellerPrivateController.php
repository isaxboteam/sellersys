<?php
include_once('AdminWebController.php');
/**
 * Description of SellerPrivateController
 *
 * @author ypra
 */
abstract class SellerPrivateController extends AdminWebController
{

    /**
     * @var DateUtil
     */
    protected $sessionDate = null;

    /**
     * @var array
     */
    protected static $adminRoles = array('SUPER', 'ADM');

    /**
     * @var ChoUser
     */
    protected $sessionUser = null;

    /**
     * @var SelBranch
     */
    protected $sessionBranch = null;

    /**
     * @var bool
     */
    protected $isAdmin = false;

    /**
     * @var bool
     */
    protected $isOpenedBranch = false;

    public function _init()
    {
        parent::_init();
        $this->sessionDate = new DateUtil('now');
        if(UserControl::isLoggedIn()){
            $this->sessionUser = UserControl::user();
            $this->sessionBranch = Session::get('branch');
            $this->isAdmin = $this->_isAdmin();
            $this->isOpenedBranch = is_object($this->sessionBranch);
        }
        $this->view->changeLayout('private');
        //$this->view->changeLayout('bootstrap');
    }

    /**
     * @return SelEmployee
     * @throws PropelException
     */
    public function _employee()
    {
        return SelEmployeeQuery::create()->filterByChoUser($this->sessionUser)->findOne();
    }

    /**
     * @return bool
     */
    public function _isAdmin()
    {
        if(is_object($this->sessionUser)){
            $rolCodes = array_map(function($rol){
                return $rol->getCode();
            }, $this->sessionUser->inOrderRols()->getArrayCopy());
            return sizeof(array_intersect($rolCodes, self::$adminRoles)) > 0;
        }
        return false;
    }

    /**
     * @param SelBranch $branch
     * @return null|SelBranchEmployee
     * @throws PropelException
     */
    public function _branchEmployee(SelBranch $branch)
    {
        if(is_object($branch) && is_object($this->sessionUser)){
            return SelBranchEmployeeQuery::createInDateEmployee($this->sessionDate, $this->_employee())
                ->filterBySelBranch($branch)
                ->findOne();
        }
        return null;
    }

    /**
     * @param SelBranch $branch
     * @return bool
     */
    public function _isBranchEmployee(SelBranch $branch)
    {
        if(is_object($branch) && is_object($this->sessionUser)){
            if($this->isAdmin){
                return true;
            }
            return is_object($this->_branchEmployee($branch));
        }
        return false;
    }

    /**
     * @param SelBranch $branch
     * @return bool
     */
    public function _isBranchAdmin(SelBranch $branch)
    {
        if(is_object($branch) && is_object($this->sessionUser)){
            if($this->isAdmin){
                return true;
            }
            $branchEmployee = $this->_branchEmployee($branch);
            return is_object($branchEmployee)? $branchEmployee->getAdmin(): false;
        }
        return false;
    }

    /**
     * @return array|mixed|PropelObjectCollection
     */
    public function _allowedBranches()
    {
        $allBranchs = SelBranchQuery::create()->orderByName()->find();
        foreach($allBranchs as $key => $branch){
            if(!$this->_isBranchAdmin($branch)){
                $allBranchs->remove($key);
            }
        }
        return $allBranchs;
    }

    public function printPdf($contents, $pageSize = null)
    {
        $pdf = new MyPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, ($pageSize?: 'LETTER'), true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("DUNAMIS");
        $pdf->SetTitle("SINDEL");
        $pdf->SetSubject("SINDEL");
        $pdf->SetKeywords("SINDEL");

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        ////$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(20, 25, 20, true);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        ///$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray($l);
        // set font*/
        $pdf->SetFont('helvetica', '', 10);
        // add a page
        $pdf->AddPage();
//        $contents = 'james lo hizo';
        $pdf->writeHTML($contents, true, false, false, false, '');
        $pdf->lastPage();
        $pdf->Output("imprimir.pdf", "I", "I");
        ob_clean();
    }

    public function printPdfHorizontal($contents, $pageSize = null)
    {
        $pdf = new MyPdfHorizontal(PDF_PAGE_ORIENTATION, PDF_UNIT, ($pageSize?: 'LETTER'), true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("DUNAMIS");
        $pdf->SetTitle("SINDEL");
        $pdf->SetSubject("SINDEL");
        $pdf->SetKeywords("SINDEL");

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        ////$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(20, 35, 20, true);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        ///$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray($l);
        // set font*/
        $pdf->SetFont('helvetica', '', 10);
        // add a page
        $pdf->AddPage();
//        $contents = 'james lo hizo';
        $pdf->writeHTML($contents, true, false, false, false, '');
        $pdf->lastPage();
        $pdf->Output("imprimir.pdf", "I", "I");
        ob_clean();
    }

    public function printPdfVertical($contents, $pageSize = null)
    {
        $pdf = new MyPdfVertical(PDF_PAGE_ORIENTATION, PDF_UNIT, ($pageSize?: 'LETTER'), true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("DUNAMIS");
        $pdf->SetTitle("SINDEL");
        $pdf->SetSubject("SINDEL");
        $pdf->SetKeywords("SINDEL");

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        ////$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(20, 35, 20, true);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        ///$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray($l);
        // set font*/
        $pdf->SetFont('helvetica', '', 10);
        // add a page
        $pdf->AddPage();
//        $contents = 'james lo hizo';
        $pdf->writeHTML($contents, true, false, false, false, '');
        $pdf->lastPage();
        $pdf->Output("imprimir.pdf", "I", "I");
        ob_clean();
    }

}