<?php
/**
 * This class is a helper that scope the variables for the bar renderization
 *
 * @author ypra
 */
class AppBarsHelper extends BarView
{

    public function myBar()
    {
        $this->setVar('myName', 'Yecid');
    }

    public function menu_bootstrap()
    {
        $userPages = UserControl::isLoggedIn()?
                PageControl::userPages(UserControl::user(), true): array();
        $this->setVar('userPages', $userPages);
        $this->setVar('page', PageControl::page());
    }

    public function categories_menu()
    {
        $categories = SelCategoryQuery::create()
                ->filterByMainCategoryId(null)
                ->orderByName()
            ->find();
        $this->setVar('categories', $categories);
    }

    public function categories_list_menu()
    {
        $categories = SelCategoryQuery::create()
                ->filterByMainCategoryId(null)
                ->orderByName()
            ->find();
        $this->setVar('categories', $categories);
    }

    public function home_products_new()
    {
        $products = SelProductQuery::create()
                ->filterByIsNew(true)
                ->orderByName(Criteria::ASC)
            ->find();
        $this->setVar('products', $products);
    }

    public function home_products_offers()
    {

        $products = SelProductQuery::create()
                ->filterByIsOffer(true)
                ->orderByName(Criteria::ASC)
            ->find();
        $this->setVar('products', $products);
    }

    public function pre_header()
    {

    }

}