
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- cho_module
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cho_module`;

CREATE TABLE `cho_module`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `URI` VARCHAR(30) NOT NULL,
    `NAME` VARCHAR(30) NOT NULL,
    `ACCESS` VARCHAR(20) NOT NULL,
    `POSITION` INTEGER NOT NULL,
    `DESCRIPTION` TEXT,
    PRIMARY KEY (`ID`),
    UNIQUE INDEX `UK_MODULE_NAME` (`NAME`),
    UNIQUE INDEX `NAME` (`NAME`),
    UNIQUE INDEX `URI` (`URI`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- cho_rol
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cho_rol`;

CREATE TABLE `cho_rol`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `CODE` VARCHAR(20) NOT NULL,
    `NAME` VARCHAR(50) NOT NULL,
    `DESCRIPTION` TEXT,
    PRIMARY KEY (`ID`),
    UNIQUE INDEX `UK_CODE` (`CODE`),
    UNIQUE INDEX `CODE` (`CODE`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- cho_rol_x_uri
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cho_rol_x_uri`;

CREATE TABLE `cho_rol_x_uri`
(
    `ROL_ID` INTEGER NOT NULL,
    `URI_ID` INTEGER NOT NULL,
    `READ` VARCHAR(3) DEFAULT 'SI' NOT NULL,
    `CREATE` VARCHAR(3) DEFAULT 'NO' NOT NULL,
    `UPDATE` VARCHAR(3) DEFAULT 'NO' NOT NULL,
    `DELETE` VARCHAR(3) DEFAULT 'NO' NOT NULL,
    PRIMARY KEY (`ROL_ID`,`URI_ID`),
    INDEX `ROL_ID` (`ROL_ID`),
    INDEX `URI_ID` (`URI_ID`),
    CONSTRAINT `cho_rol_x_uri_fk1`
        FOREIGN KEY (`ROL_ID`)
        REFERENCES `cho_rol` (`ID`),
    CONSTRAINT `cho_rol_x_uri_fk2`
        FOREIGN KEY (`URI_ID`)
        REFERENCES `cho_uri` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- cho_uri
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cho_uri`;

CREATE TABLE `cho_uri`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `MODULE_ID` INTEGER NOT NULL,
    `URI` VARCHAR(200) NOT NULL,
    `TITLE` VARCHAR(50) NOT NULL,
    `ACCESS` VARCHAR(20) NOT NULL,
    `TYPE` VARCHAR(20) NOT NULL,
    `POSITION` INTEGER NOT NULL,
    `DESCRIPTION` TEXT,
    PRIMARY KEY (`ID`),
    UNIQUE INDEX `UK_URI` (`URI`),
    INDEX `MODULE_ID` (`MODULE_ID`),
    CONSTRAINT `cho_uri_fk1`
        FOREIGN KEY (`MODULE_ID`)
        REFERENCES `cho_module` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- cho_user
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cho_user`;

CREATE TABLE `cho_user`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `ROL_ID` INTEGER NOT NULL,
    `FIRST_LASTNAME` VARCHAR(30) NOT NULL,
    `SECOND_LASTNAME` VARCHAR(30),
    `FIRST_NAME` VARCHAR(30) NOT NULL,
    `SECOND_NAME` VARCHAR(30),
    `USERNAME` VARCHAR(50) NOT NULL,
    `PASSWORD` VARCHAR(50) NOT NULL,
    `EMAIL` VARCHAR(80) NOT NULL,
    `DNI` VARCHAR(20),
    `GENDER` VARCHAR(10),
    `BIRTHDAY` DATE,
    `TYPE` VARCHAR(20),
    `STATUS` VARCHAR(20) NOT NULL,
    `COUNTRY` VARCHAR(30),
    `STATE` VARCHAR(30),
    `CITY` VARCHAR(50),
    `ADDRESS` TEXT,
    `ZIP` VARCHAR(10),
    `PRIMARY_PHONE` VARCHAR(20),
    `SECONDARY_PHONE` VARCHAR(20),
    `CELL_PHONE` VARCHAR(20),
    `PHOTO_FORMAT` VARCHAR(15),
    `REGISTERED_DATE` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `LAST_ACCESS` DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
    PRIMARY KEY (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- cho_user_admin
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cho_user_admin`;

CREATE TABLE `cho_user_admin`
(
    `ID` INTEGER NOT NULL,
    PRIMARY KEY (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- cho_user_x_rol
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cho_user_x_rol`;

CREATE TABLE `cho_user_x_rol`
(
    `USER_ID` INTEGER NOT NULL,
    `ROL_ID` INTEGER NOT NULL,
    PRIMARY KEY (`USER_ID`,`ROL_ID`),
    INDEX `ROL_ID` (`ROL_ID`),
    INDEX `USER_ID` (`USER_ID`),
    CONSTRAINT `cho_user_x_rol_fk1`
        FOREIGN KEY (`USER_ID`)
        REFERENCES `cho_user` (`ID`),
    CONSTRAINT `cho_user_x_rol_fk2`
        FOREIGN KEY (`ROL_ID`)
        REFERENCES `cho_rol` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_acquisition
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_acquisition`;

CREATE TABLE `sel_acquisition`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `DATE` DATE,
    `PROVIDER_ID` INTEGER,
    `OBSERVATION` TEXT,
    `WARRANTY_PROVIDER` VARCHAR(30),
    PRIMARY KEY (`ID`),
    INDEX `PROVIDER_ID` (`PROVIDER_ID`),
    CONSTRAINT `acquisition_product_fk1`
        FOREIGN KEY (`PROVIDER_ID`)
        REFERENCES `sel_provider` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_branch
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_branch`;

CREATE TABLE `sel_branch`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `NUMBER` INTEGER DEFAULT 0 NOT NULL,
    `STATUS` VARCHAR(15) DEFAULT 'OPEN' NOT NULL,
    `CODE` VARCHAR(20) NOT NULL,
    `NAME` VARCHAR(20) NOT NULL,
    `ADDRESS` VARCHAR(100) NOT NULL,
    `PHONE` VARCHAR(20),
    `FAX` VARCHAR(20),
    PRIMARY KEY (`ID`),
    UNIQUE INDEX `sel_branch_idx1` (`CODE`),
    UNIQUE INDEX `CODE` (`CODE`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_branch_category
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_branch_category`;

CREATE TABLE `sel_branch_category`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `BRANCH_ID` INTEGER NOT NULL,
    `CATEGORY_ID` INTEGER NOT NULL,
    `STATE` VARCHAR(15) NOT NULL,
    `OBSERVATION` TEXT,
    PRIMARY KEY (`ID`),
    UNIQUE INDEX `ID` (`ID`),
    INDEX `BRANCH_ID` (`BRANCH_ID`),
    INDEX `CATEGORY_ID` (`CATEGORY_ID`),
    CONSTRAINT `sel_branch_category_fk1`
        FOREIGN KEY (`BRANCH_ID`)
        REFERENCES `sel_branch` (`ID`),
    CONSTRAINT `sel_branch_category_fk2`
        FOREIGN KEY (`CATEGORY_ID`)
        REFERENCES `sel_category` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_branch_employee
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_branch_employee`;

CREATE TABLE `sel_branch_employee`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `BRANCH_ID` INTEGER NOT NULL,
    `EMPLOYEE_ID` INTEGER NOT NULL,
    `INIT_DATE` DATE NOT NULL,
    `END_DATE` DATE NOT NULL,
    `ADMIN` TINYINT(1) NOT NULL,
    `STATUS` VARCHAR(15),
    PRIMARY KEY (`ID`),
    INDEX `BRANCH_ID` (`BRANCH_ID`),
    INDEX `EMPLOYEE_ID` (`EMPLOYEE_ID`),
    CONSTRAINT `sel_branch_employee_fk1`
        FOREIGN KEY (`EMPLOYEE_ID`)
        REFERENCES `sel_employee` (`ID`),
    CONSTRAINT `sel_employee_branch`
        FOREIGN KEY (`BRANCH_ID`)
        REFERENCES `sel_branch` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_brand
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_brand`;

CREATE TABLE `sel_brand`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(100),
    `CODE` VARCHAR(20),
    `DESCRIPTION` VARCHAR(300),
    PRIMARY KEY (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_category
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_category`;

CREATE TABLE `sel_category`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `MAIN_CATEGORY_ID` INTEGER,
    `CODE` VARCHAR(20) NOT NULL,
    `NAME` VARCHAR(100) NOT NULL,
    `DESCRIPTION` TEXT,
    PRIMARY KEY (`ID`),
    INDEX `MAIN_CATEGORY_ID` (`MAIN_CATEGORY_ID`, `ID`),
    INDEX `MAIN_CATEGORY_ID_2` (`MAIN_CATEGORY_ID`),
    CONSTRAINT `sel_category_fk1`
        FOREIGN KEY (`MAIN_CATEGORY_ID`)
        REFERENCES `sel_category` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_customer
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_customer`;

CREATE TABLE `sel_customer`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `CUSTOMER_ACCOUNT_ID` INTEGER,
    `ID_NUMBER` VARCHAR(30) NOT NULL,
    `NAME` VARCHAR(100) NOT NULL,
    `LASTNAME` VARCHAR(100) NOT NULL,
    `EMAIL` VARCHAR(150) NOT NULL,
    `BIRTHDAY` DATE,
    `ADDRESS` VARCHAR(500),
    `PHONE` VARCHAR(30),
    `CELLPHONE` VARCHAR(30),
    `STATUS` VARCHAR(20),
    `CODE` VARCHAR(20),
    PRIMARY KEY (`ID`),
    UNIQUE INDEX `ID_NUMBER` (`ID_NUMBER`),
    UNIQUE INDEX `CUSTOMER_ACCOUNT_ID` (`CUSTOMER_ACCOUNT_ID`),
    INDEX `USER_ID` (`CUSTOMER_ACCOUNT_ID`),
    CONSTRAINT `sel_customer_fk1`
        FOREIGN KEY (`CUSTOMER_ACCOUNT_ID`)
        REFERENCES `sel_customer_account` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_customer_account
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_customer_account`;

CREATE TABLE `sel_customer_account`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `EMAIL` VARCHAR(100) NOT NULL,
    `PASSWORD` VARCHAR(200) NOT NULL,
    `LAST_ACCESS` DATETIME,
    `CURRENT_ACCESS` DATETIME,
    `CREATION_DATE` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_employee
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_employee`;

CREATE TABLE `sel_employee`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `USER_ID` INTEGER NOT NULL,
    `INIT_DATE` DATE NOT NULL,
    `END_DATE` DATE NOT NULL,
    `DETAILS` TEXT,
    PRIMARY KEY (`ID`),
    INDEX `USER_ID` (`USER_ID`),
    CONSTRAINT `sel_employee_fk1`
        FOREIGN KEY (`USER_ID`)
        REFERENCES `cho_user` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_order
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_order`;

CREATE TABLE `sel_order`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `CUSTOMER_ID` INTEGER NOT NULL,
    `SALES_ID` INTEGER,
    `STATUS` VARCHAR(20) NOT NULL,
    `SHIPPING_ADDRESS` TEXT NOT NULL,
    `PHONE` VARCHAR(30) NOT NULL,
    `CUSTOMMER_COMMENT` TEXT NOT NULL,
    `SALES_COMMENT` INTEGER,
    `CREATION_DATE` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `ORDER_DATE` DATETIME,
    `PAYMENT_DATE` DATETIME,
    `PAYMENT_CODE` VARCHAR(30),
    `PAYMENT_ENTITY` VARCHAR(200),
    `CHECK_DATE` DATETIME,
    `APPROVED_DATE` DATETIME,
    `SHIPPING_DATE` DATETIME,
    `DELIVERY_DATE` DATETIME,
    PRIMARY KEY (`ID`),
    INDEX `SALES_ID` (`SALES_ID`),
    INDEX `CUSTOMER_ID` (`CUSTOMER_ID`),
    CONSTRAINT `sel_order_fk1`
        FOREIGN KEY (`SALES_ID`)
        REFERENCES `sel_sale` (`ID`),
    CONSTRAINT `sel_order_fk2`
        FOREIGN KEY (`CUSTOMER_ID`)
        REFERENCES `sel_customer` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_person
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_person`;

CREATE TABLE `sel_person`
(
    `ID` INTEGER NOT NULL,
    `NAME` VARCHAR(20),
    `FIRST_LASTNAME` VARCHAR(20),
    `SECOND_LASTNAME` VARCHAR(20),
    `IDENTITY` INTEGER,
    `IDENTITY_EXT` VARCHAR(20),
    `GENDER` VARCHAR(20),
    `BIRTHDAY_DATE` DATE,
    PRIMARY KEY (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_product
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_product`;

CREATE TABLE `sel_product`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PRODUCT_GROUP_ID` INTEGER,
    `BRAND_ID` INTEGER NOT NULL,
    `CATEGORY_ID` INTEGER NOT NULL,
    `MAIN_IMAGE_ID` INTEGER,
    `CODE` VARCHAR(20) NOT NULL,
    `NAME` VARCHAR(100) NOT NULL,
    `BASE_PRICE` FLOAT(9,3) NOT NULL,
    `DESCRIPTION` TEXT,
    `MODEL` VARCHAR(100),
    `NUMBER_PARTS` INTEGER,
    `IS_MAIN` TINYINT(1) DEFAULT 0 NOT NULL,
    `IS_NEW` TINYINT(1) DEFAULT 0 NOT NULL,
    `IS_OFFER` TINYINT(1) DEFAULT 0 NOT NULL,
    PRIMARY KEY (`ID`),
    INDEX `CATEGORY_ID` (`CATEGORY_ID`),
    INDEX `PRODUCT_GROUP_ID` (`PRODUCT_GROUP_ID`),
    INDEX `BRAND_ID` (`BRAND_ID`),
    INDEX `MAIN_IMAGE_ID` (`MAIN_IMAGE_ID`),
    CONSTRAINT `sel_product_fk1`
        FOREIGN KEY (`CATEGORY_ID`)
        REFERENCES `sel_category` (`ID`),
    CONSTRAINT `sel_product_fk2`
        FOREIGN KEY (`PRODUCT_GROUP_ID`)
        REFERENCES `sel_product_group` (`ID`),
    CONSTRAINT `sel_product_fk3`
        FOREIGN KEY (`BRAND_ID`)
        REFERENCES `sel_brand` (`ID`),
    CONSTRAINT `sel_product_fk4`
        FOREIGN KEY (`MAIN_IMAGE_ID`)
        REFERENCES `sel_product_image` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_product_acquisition
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_product_acquisition`;

CREATE TABLE `sel_product_acquisition`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `AQUISITION_ID` INTEGER NOT NULL,
    `PRODUCT_ID` INTEGER NOT NULL,
    `QUANTITY` INTEGER NOT NULL,
    `PRICE` FLOAT(15,2) NOT NULL,
    `MONEY` VARCHAR(20),
    `WARRANTY_PROVIDER` VARCHAR(50),
    PRIMARY KEY (`ID`),
    INDEX `AQUISITION_ID` (`AQUISITION_ID`),
    INDEX `PRODUCT_ITEM_ID` (`PRODUCT_ID`),
    CONSTRAINT `sel_aquisition_product_fk1`
        FOREIGN KEY (`AQUISITION_ID`)
        REFERENCES `sel_acquisition` (`ID`),
    CONSTRAINT `sel_product_aquisition_fk1`
        FOREIGN KEY (`PRODUCT_ID`)
        REFERENCES `sel_product` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_product_group
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_product_group`;

CREATE TABLE `sel_product_group`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `CATEGORY_ID` INTEGER NOT NULL,
    `CODE` VARCHAR(20) NOT NULL,
    `NAME` VARCHAR(200) NOT NULL,
    `DESCRIPTION` TEXT,
    PRIMARY KEY (`ID`),
    INDEX `CATEGORY_ID` (`CATEGORY_ID`),
    CONSTRAINT `sel_product_group_fk1`
        FOREIGN KEY (`CATEGORY_ID`)
        REFERENCES `sel_category` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_product_image
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_product_image`;

CREATE TABLE `sel_product_image`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PRODUCT_ID` INTEGER NOT NULL,
    `NAME` VARCHAR(100) NOT NULL,
    `SIZE` INTEGER NOT NULL,
    `MIMETYPE` VARCHAR(30),
    `POSITION` INTEGER NOT NULL,
    `TITLE` VARCHAR(100) NOT NULL,
    `DESCRIPTION` TEXT,
    PRIMARY KEY (`ID`),
    INDEX `PRODUCT_ID` (`PRODUCT_ID`),
    CONSTRAINT `sel_product_image_fk1`
        FOREIGN KEY (`PRODUCT_ID`)
        REFERENCES `sel_product` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_product_item
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_product_item`;

CREATE TABLE `sel_product_item`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PRODUCT_ACQUISITION_ID` INTEGER NOT NULL,
    `BRAND_ID` INTEGER NOT NULL,
    `BRANCH_ID` INTEGER,
    `PRODUCT_DEPOSIT_ID` INTEGER,
    `PRODUCT_SELL_ID` INTEGER,
    `CODE` VARCHAR(20) NOT NULL,
    `STATUS` VARCHAR(15) NOT NULL,
    `COST` FLOAT(9,3) NOT NULL,
    `PRICE` FLOAT(9,3),
    `COLOR` VARCHAR(30),
    `WARRANTY_SELL` INTEGER,
    `OBSERVATION` TEXT,
    `REGISTER_DATE` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`ID`),
    INDEX `PRODUCT_ID` (`PRODUCT_ACQUISITION_ID`),
    INDEX `BRAND_ID` (`BRAND_ID`),
    INDEX `BRANCH_ID` (`BRANCH_ID`),
    INDEX `PRODUCT_SALES_ID` (`PRODUCT_SELL_ID`),
    CONSTRAINT `sel_product_item_fk1`
        FOREIGN KEY (`PRODUCT_ACQUISITION_ID`)
        REFERENCES `sel_product_acquisition` (`ID`),
    CONSTRAINT `sel_product_item_fk2`
        FOREIGN KEY (`BRANCH_ID`)
        REFERENCES `sel_branch` (`ID`),
    CONSTRAINT `sel_product_item_fk3`
        FOREIGN KEY (`BRAND_ID`)
        REFERENCES `sel_brand` (`ID`),
    CONSTRAINT `sel_product_item_fk4`
        FOREIGN KEY (`PRODUCT_SELL_ID`)
        REFERENCES `sel_product_sell` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_product_offer
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_product_offer`;

CREATE TABLE `sel_product_offer`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PRODUCT_ID` INTEGER NOT NULL,
    `TYPE` VARCHAR(20) NOT NULL,
    `INITIAL_DATE` DATE NOT NULL,
    `END_DATE` DATE NOT NULL,
    `POSiTION` INTEGER NOT NULL,
    PRIMARY KEY (`ID`),
    CONSTRAINT `product_offer_fk1`
        FOREIGN KEY (`ID`)
        REFERENCES `sel_product` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_product_order
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_product_order`;

CREATE TABLE `sel_product_order`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `ORDER_ID` INTEGER NOT NULL,
    `PRODUCT_ID` INTEGER NOT NULL,
    `PRODUCT_ACQUISITION_ID` INTEGER,
    `STATUS` VARCHAR(20) NOT NULL,
    `CODE` INTEGER NOT NULL,
    `PRICE` FLOAT(9,3) NOT NULL,
    `QUANTITY` INTEGER NOT NULL,
    `OBSERVATIONS` TEXT,
    PRIMARY KEY (`ID`),
    INDEX `ORDER_ID` (`ORDER_ID`),
    INDEX `PRODUCT_ID` (`PRODUCT_ID`),
    CONSTRAINT `sel_product_order_fk1`
        FOREIGN KEY (`ORDER_ID`)
        REFERENCES `sel_order` (`ID`),
    CONSTRAINT `sel_product_order_fk2`
        FOREIGN KEY (`PRODUCT_ID`)
        REFERENCES `sel_product` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_product_sell
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_product_sell`;

CREATE TABLE `sel_product_sell`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PRODUCT_ID` INTEGER NOT NULL,
    `SALE_ID` INTEGER NOT NULL,
    `QUANTITY` INTEGER NOT NULL,
    `PRICE` FLOAT(9,3) NOT NULL,
    `ADDED_DATE` DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
    PRIMARY KEY (`ID`),
    INDEX `SALE_ID` (`SALE_ID`),
    INDEX `PRODUCT_ID` (`PRODUCT_ID`),
    CONSTRAINT `fk_sel_product_sale_product`
        FOREIGN KEY (`PRODUCT_ID`)
        REFERENCES `sel_product` (`ID`),
    CONSTRAINT `sel_product_sale_sale`
        FOREIGN KEY (`SALE_ID`)
        REFERENCES `sel_sale` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_provider
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_provider`;

CREATE TABLE `sel_provider`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `PERSON_ID` INTEGER,
    `COMPANY` VARCHAR(100) NOT NULL,
    `ADDRESS` VARCHAR(200) NOT NULL,
    `PERSON` VARCHAR(100) NOT NULL,
    `PHONE` VARCHAR(20),
    `CELLPHONE` VARCHAR(20),
    `OBSERVATIONS` TEXT,
    PRIMARY KEY (`ID`),
    INDEX `PERSON_ID` (`PERSON_ID`),
    CONSTRAINT `sel_provider_fk1`
        FOREIGN KEY (`PERSON_ID`)
        REFERENCES `sel_person` (`ID`)
) ENGINE={InnoDB};

-- ---------------------------------------------------------------------
-- sel_sale
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sel_sale`;

CREATE TABLE `sel_sale`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `BRANCH_ID` INTEGER NOT NULL,
    `EMPLOYEE_ID` INTEGER NOT NULL,
    `CUSTOMER_ID` INTEGER NOT NULL,
    `DATE` DATETIME,
    `STATUS` VARCHAR(20),
    `CODE` VARCHAR(20) NOT NULL,
    `MONEY_CHANGE` INTEGER,
    `OBSERVATION` VARCHAR(300),
    PRIMARY KEY (`ID`),
    INDEX `CUSTOMER_ID` (`CUSTOMER_ID`),
    INDEX `BRANCH_ID` (`BRANCH_ID`),
    INDEX `EMPLOYEE_ID` (`EMPLOYEE_ID`),
    CONSTRAINT `sel_sale_fk1`
        FOREIGN KEY (`CUSTOMER_ID`)
        REFERENCES `sel_customer` (`ID`),
    CONSTRAINT `sel_sale_fk2`
        FOREIGN KEY (`BRANCH_ID`)
        REFERENCES `sel_branch` (`ID`),
    CONSTRAINT `sel_sale_fk3`
        FOREIGN KEY (`EMPLOYEE_ID`)
        REFERENCES `sel_employee` (`ID`)
) ENGINE={InnoDB};

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
