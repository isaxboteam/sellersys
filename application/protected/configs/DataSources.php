<?php
/**
 * Description of DataSources
 *
 * @author ypra
 */
class DataSources {
    
    static $development = array(
        'adapter'   =>  'mysql',
        'driver'    =>  'mysql',
        'datasource'=>  'system',
        'host'      =>  'localhost',
        'port'      =>  '3321',
        'dbname'    =>  'sellersys',
        'user'      =>  'root',
        'password'  =>  ''
    );
    
    static $test = array(
        'adapter'   =>  'mysql',
        'driver'    =>  'mysql',
        'datasource'=>  'system',
        'host'      =>  'mysql.tupe.ga',
        'port'      =>  '3306',
        'dbname'    =>  'sellersystem',
        'user'      =>  'sellersys',
        'password'  =>  'sellersys2016'
    );
    
    static $production = array(
        'adapter'   =>  'mysql',
        'driver'    =>  'mysql',
        'datasource'=>  'demo',
        'host'      =>  '',
        'port'      =>  '',
        'dbname'    =>  '',
        'user'      =>  '',
        'password'  =>  ''
    );
    
}