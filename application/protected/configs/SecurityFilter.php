<?php
Chocala::import('Model.security.SecurityRegistry');
/**
 * Description of SecurityFilter
 *
 * @author ypra
 */
class SecurityFilter extends ChocalaFilter
{

    private static $noControls = array('system'=>'*');
    
    public function beforeAction()
    {
        SecurityRegistry::instance();
        $toVerify = !array_key_exists($this->controllerName, self::$noControls);
        if($toVerify){
            if(is_array(self::$noControls[$this->controllerName])){
                $toVerify = !in_array($this->actionName,
                        self::$noControls[$this->controllerName]);
            } else {
                $toVerify = !self::$noControls[$this->controllerName] == '*';
            }
        }
        if($toVerify){
            $pageControl = PageControl::instance();
            if($pageControl->isRegistered() && !$pageControl->canRead()){
                $this->redirectTo(array('module' => 'main', 'controller' => 'system',
                    'action' => 'access'));
            }
        }

        //Lessc::compressList(PUBLIC_DIR.'css/styleLess.less', PUBLIC_DIR.'css/styleLess.css');
        //Lessc::ccompile(PUBLIC_DIR.'css/styleLess.less', PUBLIC_DIR.'css/styleLess.css');
        Lessc::ccompile(PUBLIC_DIR.'css/style.less', PUBLIC_DIR.'css/style.css');
    }

    public function afterAction()
    { 
    }

    public function afterView()
    {
    }

}