-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 09-01-2016 a las 12:15:46
-- Versión del servidor: 5.5.16-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sellersys`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_module`
--

CREATE TABLE IF NOT EXISTS `cho_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `URI` varchar(30) NOT NULL,
  `NAME` varchar(30) NOT NULL,
  `ACCESS` varchar(20) NOT NULL,
  `POSITION` int(11) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_MODULE_NAME` (`NAME`),
  UNIQUE KEY `NAME` (`NAME`),
  UNIQUE KEY `URI` (`URI`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `cho_module`
--

INSERT INTO `cho_module` (`ID`, `URI`, `NAME`, `ACCESS`, `POSITION`, `DESCRIPTION`) VALUES
(1, '', 'Portal', 'PUBLIC', 1, 'Portal p'),
(2, 'admin', 'Administracion', 'PRIVATE', 2, 'Administraci'),
(3, 'params', 'Pararametrós', 'PRIVATE', 3, 'Paramámetros'),
(4, 'catalog', 'Catálogo', 'PRIVATE', 5, 'Categoria'),
(5, 'inventory', 'Inventario', 'PRIVATE', 6, 'Adquisici'),
(6, 'organization', 'Organización', 'PRIVATE', 4, 'Modulo de organizaci'),
(7, 'acquisition', 'Compras', 'PRIVATE', 7, 'Compras para inventario'),
(8, 'sales', 'Ventas', 'PRIVATE', 8, 'Módulo de ventas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_rol`
--

CREATE TABLE IF NOT EXISTS `cho_rol` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(20) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_CODE` (`CODE`),
  UNIQUE KEY `CODE` (`CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cho_rol`
--

INSERT INTO `cho_rol` (`ID`, `CODE`, `NAME`, `DESCRIPTION`) VALUES
(1, 'SUPER', 'Superusuario', ''),
(2, 'YPRA', 'YES', 'hyhhkhjk'),
(3, 'ADM', 'Admin', 'Administrador'),
(4, 'USR', 'User', 'No description'),
(5, 'ADM-CEN', 'Administrador de Centro', 'Administrador de Centro Infantil');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_rol_x_uri`
--

CREATE TABLE IF NOT EXISTS `cho_rol_x_uri` (
  `ROL_ID` int(11) NOT NULL,
  `URI_ID` int(11) NOT NULL,
  `READ` varchar(3) NOT NULL DEFAULT 'SI',
  `CREATE` varchar(3) NOT NULL DEFAULT 'NO',
  `UPDATE` varchar(3) NOT NULL DEFAULT 'NO',
  `DELETE` varchar(3) NOT NULL DEFAULT 'NO',
  PRIMARY KEY (`ROL_ID`,`URI_ID`),
  KEY `ROL_ID` (`ROL_ID`),
  KEY `URI_ID` (`URI_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=431;

--
-- Volcado de datos para la tabla `cho_rol_x_uri`
--

INSERT INTO `cho_rol_x_uri` (`ROL_ID`, `URI_ID`, `READ`, `CREATE`, `UPDATE`, `DELETE`) VALUES
(1, 4, 'SI', 'SI', 'SI', 'SI'),
(1, 9, 'SI', 'SI', 'SI', 'SI'),
(1, 11, 'SI', 'SI', 'SI', 'SI'),
(1, 12, 'SI', 'SI', 'SI', 'SI'),
(1, 14, 'SI', 'SI', 'SI', 'SI'),
(1, 17, 'SI', 'SI', 'SI', 'SI'),
(1, 18, 'SI', 'SI', 'SI', 'SI'),
(1, 19, 'SI', 'SI', 'SI', 'SI'),
(1, 27, 'SI', 'SI', 'SI', 'SI'),
(1, 28, 'SI', 'SI', 'SI', 'SI'),
(1, 29, 'SI', 'SI', 'SI', 'SI'),
(1, 32, 'SI', 'SI', 'SI', 'SI'),
(1, 33, 'SI', 'SI', 'SI', 'SI'),
(1, 34, 'SI', 'SI', 'SI', 'SI'),
(1, 35, 'SI', 'SI', 'SI', 'SI'),
(1, 36, 'SI', 'SI', 'SI', 'SI'),
(2, 4, 'SI', 'NO', 'NO', 'NO'),
(2, 18, 'SI', 'SI', 'SI', 'SI'),
(3, 4, 'SI', 'SI', 'SI', 'NO'),
(3, 9, 'SI', 'SI', 'SI', 'SI'),
(3, 11, 'SI', 'SI', 'SI', 'SI'),
(3, 12, 'SI', 'SI', 'SI', 'SI'),
(3, 14, 'SI', 'SI', 'SI', 'SI'),
(3, 17, 'SI', 'SI', 'SI', 'SI'),
(3, 18, 'SI', 'SI', 'SI', 'SI'),
(3, 19, 'SI', 'SI', 'SI', 'SI'),
(3, 27, 'SI', 'SI', 'SI', 'SI'),
(3, 28, 'SI', 'SI', 'SI', 'SI'),
(3, 29, 'SI', 'SI', 'SI', 'SI'),
(3, 32, 'SI', 'SI', 'SI', 'SI'),
(3, 33, 'SI', 'SI', 'SI', 'SI'),
(3, 34, 'SI', 'SI', 'SI', 'SI'),
(3, 35, 'SI', 'SI', 'SI', 'SI'),
(3, 36, 'SI', 'SI', 'SI', 'SI'),
(4, 18, 'SI', 'SI', 'SI', 'SI'),
(5, 18, 'SI', 'SI', 'SI', 'SI'),
(5, 19, 'SI', 'SI', 'SI', 'SI'),
(5, 23, 'SI', 'SI', 'SI', 'SI'),
(5, 26, 'SI', 'SI', 'SI', 'SI'),
(5, 29, 'SI', 'SI', 'SI', 'SI'),
(5, 32, 'SI', 'SI', 'SI', 'SI'),
(5, 33, 'SI', 'SI', 'SI', 'SI'),
(5, 35, 'SI', 'SI', 'SI', 'SI'),
(5, 36, 'SI', 'SI', 'SI', 'SI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_uri`
--

CREATE TABLE IF NOT EXISTS `cho_uri` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` int(11) NOT NULL,
  `URI` varchar(200) NOT NULL,
  `TITLE` varchar(50) NOT NULL,
  `ACCESS` varchar(20) NOT NULL,
  `TYPE` varchar(20) NOT NULL,
  `POSITION` int(11) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_URI` (`URI`),
  KEY `MODULE_ID` (`MODULE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2340 AUTO_INCREMENT=37 ;

--
-- Volcado de datos para la tabla `cho_uri`
--

INSERT INTO `cho_uri` (`ID`, `MODULE_ID`, `URI`, `TITLE`, `ACCESS`, `TYPE`, `POSITION`, `DESCRIPTION`) VALUES
(1, 1, 'index/', 'Inicio', 'PUBLIC', 'MENU', 1, 'P'),
(2, 1, 'index/index/', 'Segundo Inicio', 'PUBLIC', 'EXTRA', 2, 'Segunda P'),
(4, 2, 'rol/', 'Roles', 'PRIVATE', 'MENU', 5, ''),
(9, 2, 'uri/', 'URIs', 'PRIVATE', 'SECTION', 5, ''),
(11, 2, 'module/', 'Módulos', 'PRIVATE', 'MENU', 5, ''),
(12, 2, 'user/', 'Usuarios', 'PRIVATE', 'MENU', 5, ''),
(14, 3, 'brand/', 'Marcas', 'PRIVATE', 'MENU', 1, ''),
(15, 3, 'trainingArea/', 'Otrin', 'PRIVATE', 'MENU', 2, ''),
(17, 4, 'category/', 'Categorías', 'PRIVATE', 'MENU', 1, ''),
(18, 4, 'products/', 'Productos', 'PRIVATE', 'SECTION', 3, ''),
(19, 7, 'acquisition/', 'Compra de Productos', 'PRIVATE', 'MENU', 3, ''),
(20, 6, 'categoryXBranch/', 'Categorías de Sucursal', 'PRIVATE', 'SECTION', 8, ''),
(23, 6, 'employee/', 'Empleados', 'PRIVATE', 'MENU', 4, ''),
(26, 6, 'branch/', 'Sucursales', 'PRIVATE', 'MENU', 8, ''),
(27, 4, 'productItems/', 'Items', 'PRIVATE', 'SECTION', 4, ''),
(28, 7, 'provider/', 'Proveedores', 'PRIVATE', 'MENU', 1, ''),
(29, 5, 'catalog/', 'Catálogo', 'PRIVATE', 'MENU', 2, 'Catalogo del inventario'),
(32, 7, 'productItem/', 'Item del producto', 'PRIVATE', 'SECTION', 5, ''),
(33, 7, 'productAcquisition/', 'Adquisicion del Producto', 'PRIVATE', 'SECTION', 4, ''),
(34, 4, 'productGroup/', 'Grupos de Producto', 'PRIVATE', 'SECTION', 2, ''),
(35, 8, 'sale/', 'Ventas', 'PRIVATE', 'MENU', 1, ''),
(36, 8, 'saleXItems/', 'Ventas por productos', 'PRIVATE', 'SECTION', 3, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user`
--

CREATE TABLE IF NOT EXISTS `cho_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROL_ID` int(11) NOT NULL,
  `FIRST_LASTNAME` varchar(30) NOT NULL,
  `SECOND_LASTNAME` varchar(30) DEFAULT NULL,
  `FIRST_NAME` varchar(30) NOT NULL,
  `SECOND_NAME` varchar(30) DEFAULT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `EMAIL` varchar(80) NOT NULL,
  `DNI` varchar(20) DEFAULT NULL,
  `GENDER` varchar(10) DEFAULT NULL,
  `BIRTHDAY` date DEFAULT NULL,
  `TYPE` varchar(20) DEFAULT NULL,
  `STATUS` varchar(20) NOT NULL,
  `COUNTRY` varchar(30) DEFAULT NULL,
  `STATE` varchar(30) DEFAULT NULL,
  `CITY` varchar(50) DEFAULT NULL,
  `ADDRESS` text,
  `ZIP` varchar(10) DEFAULT NULL,
  `PRIMARY_PHONE` varchar(20) DEFAULT NULL,
  `SECONDARY_PHONE` varchar(20) DEFAULT NULL,
  `CELL_PHONE` varchar(20) DEFAULT NULL,
  `PHOTO_FORMAT` varchar(15) DEFAULT NULL,
  `REGISTERED_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_ACCESS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=455 AUTO_INCREMENT=58 ;

--
-- Volcado de datos para la tabla `cho_user`
--

INSERT INTO `cho_user` (`ID`, `ROL_ID`, `FIRST_LASTNAME`, `SECOND_LASTNAME`, `FIRST_NAME`, `SECOND_NAME`, `USERNAME`, `PASSWORD`, `EMAIL`, `DNI`, `GENDER`, `BIRTHDAY`, `TYPE`, `STATUS`, `COUNTRY`, `STATE`, `CITY`, `ADDRESS`, `ZIP`, `PRIMARY_PHONE`, `SECONDARY_PHONE`, `CELL_PHONE`, `PHOTO_FORMAT`, `REGISTERED_DATE`, `LAST_ACCESS`) VALUES
(1, 1, 'Rodriguez', 'Aranda', 'Yecid', '', 'yecid', 'yecid123', 'yecid@serviciosycompras.com', '1234567', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 00:00:00', '2014-05-08 22:36:33'),
(2, 1, 'Ticona', 'Menchaca', 'Clever', '', 'clever', 'clever123', 'clever@serviciosycompras.com', '6787889', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 00:00:00', '2014-05-08 22:36:33'),
(3, 1, 'Santana', 'Diaz', 'Abel', '', 'abel', 'abel123', 'abel@serviciosycompras.com', '5789543', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 00:00:00', '2014-05-08 22:36:33'),
(4, 1, 'Colomina', '', 'Juan', '', 'juan', 'juan123', 'juan@serviciosycompras.com', '6879654', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 00:00:00', '2014-05-08 22:36:33'),
(55, 1, 'Perez', 'Perez', 'Juan', 'Maria', 'juanperez', 'juanperez', 'juan.perez@gmail.com', '123456', 'MALE', '2019-01-01', 'EXTERNAL', 'CREATED', NULL, NULL, NULL, 'Calle Las Retamas #921', NULL, '2810927', NULL, '77719202', NULL, '2014-08-24 10:27:00', '0000-00-00 00:00:00'),
(56, 1, 'Jgh', 'Jhghjg', 'JAIME', 'Lkh', 'ibrahim', '', 'miharbi@fdsf.com', '9078979', 'MALE', '2015-12-13', 'INTERNAL', 'CREATED', NULL, NULL, NULL, 'Av San Felipe #222', NULL, '5522555', NULL, '4564654', NULL, '2015-12-13 15:49:23', '0000-00-00 00:00:00'),
(57, 1, 'Jgh', 'Jhghjg', 'JAIME', 'Lkh', 'ibrahim', '', 'miharbi@fdsf.com', '9078979', 'MALE', '2015-12-13', 'INTERNAL', 'CREATED', NULL, NULL, NULL, 'Av San Felipe #222', NULL, '5522555', NULL, '4564654', NULL, '2015-12-13 15:49:41', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user_admin`
--

CREATE TABLE IF NOT EXISTS `cho_user_admin` (
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096;

--
-- Volcado de datos para la tabla `cho_user_admin`
--

INSERT INTO `cho_user_admin` (`ID`) VALUES
(1),
(2),
(3),
(4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user_x_rol`
--

CREATE TABLE IF NOT EXISTS `cho_user_x_rol` (
  `USER_ID` int(11) NOT NULL,
  `ROL_ID` int(11) NOT NULL,
  PRIMARY KEY (`USER_ID`,`ROL_ID`),
  KEY `ROL_ID` (`ROL_ID`),
  KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=3276;

--
-- Volcado de datos para la tabla `cho_user_x_rol`
--

INSERT INTO `cho_user_x_rol` (`USER_ID`, `ROL_ID`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(55, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_acquisition`
--

CREATE TABLE IF NOT EXISTS `sel_acquisition` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de compra',
  `DATE` date DEFAULT NULL COMMENT 'Fecha de compra',
  `PROVIDER_ID` int(11) DEFAULT NULL COMMENT 'ID de proveedor',
  `OBSERVATION` text COMMENT 'Observaciones de la compra',
  `WARRANTY_PROVIDER` varchar(30) DEFAULT NULL COMMENT 'Código de Garantia del proveedor',
  PRIMARY KEY (`ID`),
  KEY `PROVIDER_ID` (`PROVIDER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `sel_acquisition`
--

INSERT INTO `sel_acquisition` (`ID`, `DATE`, `PROVIDER_ID`, `OBSERVATION`, `WARRANTY_PROVIDER`) VALUES
(1, '2015-01-01', 17, '', '101010'),
(2, '2015-01-01', 14, '', '101010'),
(3, '2015-01-01', 15, '', '101010'),
(4, '2015-01-01', 15, '', '101010'),
(6, '2015-07-08', 16, 'Factura retenida para descargo', '33333333'),
(7, '2015-12-02', 15, 'Mantenimiento de cuenta', '1 mes'),
(8, '2015-12-01', 15, 'Pago postentrega', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_branch`
--

CREATE TABLE IF NOT EXISTS `sel_branch` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de sucursal',
  `NUMBER` int(11) NOT NULL COMMENT 'Numero de Sucursal',
  `STATUS` varchar(15) NOT NULL COMMENT 'Estado de atención',
  `CODE` varchar(20) NOT NULL COMMENT 'Código de sucursal',
  `NAME` varchar(20) NOT NULL COMMENT 'Nombre de sucursal',
  `ADDRESS` varchar(100) NOT NULL COMMENT 'Dirección de sucursal',
  `PHONE` varchar(20) DEFAULT NULL COMMENT 'Teléfono de atención de sucursal',
  `FAX` varchar(20) DEFAULT NULL COMMENT 'Fax de contacto',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `sel_branch_idx1` (`CODE`),
  UNIQUE KEY `CODE` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `sel_branch`
--

INSERT INTO `sel_branch` (`ID`, `NUMBER`, `STATUS`, `CODE`, `NAME`, `ADDRESS`, `PHONE`, `FAX`) VALUES
(0, 2, 'ACTIVO', '004', 'Sucursal B', 'Calle 6 de Octubre #1562', '52', '52'),
(1, 1, 'ACTIVO', '002', 'Sucursal A', 'Av 6 de agosto', '299777', '443434');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_branch_category`
--

CREATE TABLE IF NOT EXISTS `sel_branch_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de categoría de sucursal',
  `BRANCH_ID` int(11) NOT NULL COMMENT 'Id de sucursal',
  `CATEGORY_ID` int(11) NOT NULL COMMENT 'Id de categoría',
  `STATE` varchar(15) NOT NULL COMMENT 'Estado de categoría de sucursal',
  `OBSERVATION` text COMMENT 'Observacion de Categoría de Sucursal',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_branch_employee`
--

CREATE TABLE IF NOT EXISTS `sel_branch_employee` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de empleado en sucursal',
  `BRANCH_ID` int(11) NOT NULL COMMENT 'Id de sucursal',
  `EMPLOYEE_ID` int(11) NOT NULL COMMENT 'Id de empleado',
  `INIT_DATE` date NOT NULL COMMENT 'Fecha de Inicio de empleado en sucursal',
  `END_DATE` date NOT NULL COMMENT 'Fecha de fin de empleado en sucursal',
  `ADMIN` tinyint(1) NOT NULL COMMENT 'Indicador de administrador de sucursal',
  `STATUS` varchar(15) DEFAULT NULL COMMENT 'Estado de habiloiración de empleado en sucursal',
  PRIMARY KEY (`ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`),
  KEY `EMPLOYEE_ID` (`EMPLOYEE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `sel_branch_employee`
--

INSERT INTO `sel_branch_employee` (`ID`, `BRANCH_ID`, `EMPLOYEE_ID`, `INIT_DATE`, `END_DATE`, `ADMIN`, `STATUS`) VALUES
(1, 1, 3, '2014-12-01', '2015-07-30', 1, 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_brand`
--

CREATE TABLE IF NOT EXISTS `sel_brand` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `CODE` varchar(20) DEFAULT NULL,
  `DESCRIPTION` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 AUTO_INCREMENT=25 ;

--
-- Volcado de datos para la tabla `sel_brand`
--

INSERT INTO `sel_brand` (`ID`, `NAME`, `CODE`, `DESCRIPTION`) VALUES
(1, 'Hewlet Packard', 'HP', ''),
(2, 'L&G', 'LG', 'Ideas for life'),
(3, 'Sony', 'SNY', ''),
(4, 'Acer', 'ACR', ''),
(5, 'Dell', 'DLL', ''),
(6, 'Samsung', 'SMS', ''),
(7, 'Delux', 'DLX', ''),
(8, 'Haier', 'HAI', ''),
(9, 'Daewoo', 'DWO', ''),
(10, 'Toshiba', 'TSH', ''),
(11, 'Master G', 'MTG', ''),
(12, 'HTC', 'HTC', ''),
(13, 'Genius', 'GNS', ''),
(14, 'Lenovo', 'LNV', ''),
(15, 'Top Link', 'TPLK', ''),
(16, 'D-Link', 'DLNK', ''),
(17, 'Cisco', 'CSC', ''),
(18, 'Asus', 'ASS', ''),
(19, 'Epson', 'EPS', ''),
(20, 'Canon', 'CNN', ''),
(21, 'Panasonic', 'PNS', ''),
(22, 'Philips', 'PHL', ''),
(23, 'Apple', 'APPLE', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_category`
--

CREATE TABLE IF NOT EXISTS `sel_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MAIN_CATEGORY_ID` int(11) DEFAULT NULL,
  `CODE` varchar(20) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  KEY `MAIN_CATEGORY_ID` (`MAIN_CATEGORY_ID`,`ID`),
  KEY `MAIN_CATEGORY_ID_2` (`MAIN_CATEGORY_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1489 AUTO_INCREMENT=38 ;

--
-- Volcado de datos para la tabla `sel_category`
--

INSERT INTO `sel_category` (`ID`, `MAIN_CATEGORY_ID`, `CODE`, `NAME`, `DESCRIPTION`) VALUES
(15, NULL, 'CMPT', 'Computadoras y Tablets', ''),
(16, NULL, 'PTLL', 'Pantallas', ''),
(17, NULL, 'TLFR', 'Telefonia y Redes', ''),
(18, NULL, 'ENTR', 'Entretenimiento', ''),
(19, NULL, 'LNBL', 'Linea Blanca', ''),
(20, 15, 'PC', 'PCs de Escritorio', ''),
(21, 15, 'PRT', 'Portátiles', ''),
(22, 15, 'PRU', 'Ultraligeras', ''),
(23, 15, 'IMP', 'Impresoras', ''),
(24, 15, 'SFT', 'Software', ''),
(25, 15, 'TBL', 'Tabletas', ''),
(26, 16, 'MNT', 'Monitores', ''),
(28, 17, 'TLF', 'Teléfonos', ''),
(29, 17, 'MLT', 'Equipos Multilinea', ''),
(30, 17, 'ROU', 'Enrutadores', ''),
(31, 17, 'WFI', 'Equipos WiFi', ''),
(32, 18, 'CNS', 'Consolas', ''),
(33, 18, 'CSS', 'Accesorios de Juegos', ''),
(34, 16, 'MHD', 'Monitores HD +', ''),
(35, 19, 'ETF', 'Estufas', ''),
(36, 19, 'RFR', 'Refrigeradores', ''),
(37, 19, 'FTC', 'Fotocopiadoras', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_customer`
--

CREATE TABLE IF NOT EXISTS `sel_customer` (
  `ID` int(11) NOT NULL,
  `STATE` varchar(20) DEFAULT NULL,
  `CODE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_employee`
--

CREATE TABLE IF NOT EXISTS `sel_employee` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `INIT_DATE` date NOT NULL,
  `END_DATE` date NOT NULL,
  `DETAILS` text,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `sel_employee`
--

INSERT INTO `sel_employee` (`ID`, `USER_ID`, `INIT_DATE`, `END_DATE`, `DETAILS`) VALUES
(3, 55, '2014-12-13', '2015-02-07', 'SDFSDF');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_order_detail`
--

CREATE TABLE IF NOT EXISTS `sel_order_detail` (
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_order_product`
--

CREATE TABLE IF NOT EXISTS `sel_order_product` (
  `ID` int(11) NOT NULL,
  `DATE` date DEFAULT NULL,
  `STATE` varchar(20) DEFAULT NULL,
  `ORDER_DETAIL` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_person`
--

CREATE TABLE IF NOT EXISTS `sel_person` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `FIRST_LASTNAME` varchar(20) DEFAULT NULL,
  `SECOND_LASTNAME` varchar(20) DEFAULT NULL,
  `IDENTITY` int(11) DEFAULT NULL,
  `IDENTITY_EXT` varchar(20) DEFAULT NULL,
  `GENDER` varchar(20) DEFAULT NULL,
  `BIRTHDAY_DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product`
--

CREATE TABLE IF NOT EXISTS `sel_product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_GROUP_ID` int(11) NOT NULL,
  `BRAND_ID` int(11) NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `CODE` varchar(20) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `DESCRIPTION` text,
  `MODEL` varchar(100) DEFAULT NULL,
  `NUMBER_PARTS` int(11) DEFAULT NULL,
  `MAIN_IMAGE_ID` int(11) DEFAULT NULL,
  `IS_MAIN` tinyint(1) NOT NULL,
  `IS_OFFER` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`),
  KEY `PRODUCT_GROUP_ID` (`PRODUCT_GROUP_ID`),
  KEY `BRAND_ID` (`BRAND_ID`),
  KEY `MAIN_IMAGE_ID` (`MAIN_IMAGE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096 AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `sel_product`
--

INSERT INTO `sel_product` (`ID`, `PRODUCT_GROUP_ID`, `BRAND_ID`, `CATEGORY_ID`, `CODE`, `NAME`, `DESCRIPTION`, `MODEL`, `NUMBER_PARTS`, `MAIN_IMAGE_ID`, `IS_MAIN`, `IS_OFFER`) VALUES
(18, 1, 14, 21, 'LNXNR14', 'Laptop Lanix Neuron', NULL, 'Neuron', NULL, NULL, 0, 1),
(19, 1, 5, 21, 'LDINS14', 'Laptop Dell Inspiron', NULL, 'Inspiron', NULL, NULL, 0, 1),
(20, 2, 5, 21, 'LDINS15', 'Laptop Dell Inspiron', NULL, 'Inspiron', NULL, NULL, 0, 1),
(21, 1, 1, 21, 'HPPAV14', 'Notebook HP Pavilion 14"', NULL, 'Pavilion', NULL, NULL, 0, 1),
(22, 1, 1, 21, 'CHRMBE', 'Chromebook 14"', NULL, 'Chromebook', NULL, NULL, 0, 1),
(23, 1, 4, 21, 'ACUM', 'Laptop Acer UMAC 14', NULL, 'UMAC', NULL, 4, 0, 1),
(24, 3, 4, 21, 'HPBTS16', 'Notebook HP Beats 15.8"', NULL, 'ENVY', NULL, NULL, 0, 1),
(25, 10, 1, 23, 'HP 2555', 'hp laser', 'hp laser', '2555', NULL, 3, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_acquisition`
--

CREATE TABLE IF NOT EXISTS `sel_product_acquisition` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AQUISITION_ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `PRICE` float(15,2) NOT NULL,
  `MONEY` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `AQUISITION_ID` (`AQUISITION_ID`),
  KEY `PRODUCT_ITEM_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `sel_product_acquisition`
--

INSERT INTO `sel_product_acquisition` (`ID`, `AQUISITION_ID`, `PRODUCT_ID`, `QUANTITY`, `PRICE`, `MONEY`) VALUES
(1, 1, 18, 50, 3000.00, 'Bs'),
(2, 2, 20, 20, 1000.00, 'Bs'),
(3, 6, 25, 40, 500.00, 'Bs'),
(4, 7, 19, 30, 5800.00, 'Bs'),
(5, 7, 19, 30, 5800.00, 'Bs'),
(6, 7, 19, 30, 5800.00, 'Bs'),
(7, 7, 19, 30, 5800.00, 'Bs'),
(8, 7, 19, 30, 5800.00, 'Bs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_group`
--

CREATE TABLE IF NOT EXISTS `sel_product_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATEGORY_ID` int(11) NOT NULL,
  `CODE` varchar(20) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DESCRIPTION` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1820 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `sel_product_group`
--

INSERT INTO `sel_product_group` (`ID`, `CATEGORY_ID`, `CODE`, `NAME`, `DESCRIPTION`) VALUES
(1, 21, 'LPT14', 'Laptop de 14"', 0),
(2, 21, 'LPT15', 'Laptop de 15"', 0),
(3, 21, 'LPT16', 'Laptop de 16"', 0),
(4, 21, 'LPT17', 'Laptop de 17"', 0),
(5, 20, 'CR2D', 'Core 2 Duo', 0),
(6, 20, 'CCRI3', 'Core i3', 0),
(7, 20, 'CRi5', 'Core i5', 0),
(8, 20, 'CRi7', 'Core i7', 0),
(9, 20, 'DUCr', 'Dual Core', 0),
(10, 23, 'HP', 'Laser 2555', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_image`
--

CREATE TABLE IF NOT EXISTS `sel_product_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(40) DEFAULT NULL,
  `SIZE` int(11) DEFAULT NULL,
  `MIMETYPE` varchar(30) DEFAULT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `POSITION` int(11) DEFAULT NULL,
  `TITLE` varchar(100) DEFAULT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `sel_product_image`
--

INSERT INTO `sel_product_image` (`ID`, `NAME`, `SIZE`, `MIMETYPE`, `PRODUCT_ID`, `POSITION`, `TITLE`, `DESCRIPTION`) VALUES
(1, '1.jpg', 704903, 'image/jpeg', 25, NULL, NULL, NULL),
(2, '1.jpg', 704903, 'image/jpeg', 25, NULL, NULL, NULL),
(3, '1.jpg', 704903, 'image/jpeg', 25, NULL, NULL, NULL),
(4, '2.jpg', 533070, 'image/jpeg', 23, NULL, NULL, NULL),
(5, '', 0, '', 25, NULL, NULL, NULL),
(6, '', 0, '', 25, NULL, NULL, NULL),
(7, '', 0, '', 25, NULL, NULL, NULL),
(8, '', 0, '', 25, NULL, NULL, NULL),
(9, 'chofer_papamovil.jpg', NULL, 'image/jpeg', 23, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_item`
--

CREATE TABLE IF NOT EXISTS `sel_product_item` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ACQUISITION_ID` int(11) NOT NULL,
  `BRAND_ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) DEFAULT NULL,
  `PRODUCT_DEPOSIT_ID` int(11) DEFAULT NULL,
  `PRODUCT_SALES_ID` int(11) DEFAULT NULL,
  `CODE` varchar(20) NOT NULL,
  `STATUS` varchar(15) NOT NULL,
  `REGISTER_DATE` datetime DEFAULT NULL,
  `COST` int(11) DEFAULT NULL,
  `PRICE` double(15,3) DEFAULT NULL,
  `COLOR` int(11) DEFAULT NULL,
  `WARRANTY_SELL` int(11) DEFAULT NULL,
  `OBSERVATION` text,
  PRIMARY KEY (`ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ACQUISITION_ID`),
  KEY `BRAND_ID` (`BRAND_ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`),
  KEY `PRODUCT_SALES_ID` (`PRODUCT_SALES_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 AUTO_INCREMENT=261 ;

--
-- Volcado de datos para la tabla `sel_product_item`
--

INSERT INTO `sel_product_item` (`ID`, `PRODUCT_ACQUISITION_ID`, `BRAND_ID`, `BRANCH_ID`, `PRODUCT_DEPOSIT_ID`, `PRODUCT_SALES_ID`, `CODE`, `STATUS`, `REGISTER_DATE`, `COST`, `PRICE`, `COLOR`, `WARRANTY_SELL`, `OBSERVATION`) VALUES
(1, 1, 4, 0, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(2, 1, 4, 0, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(3, 1, 4, 0, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(4, 1, 4, 0, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(5, 1, 4, 0, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(6, 1, 4, 0, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(7, 1, 4, 0, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(8, 1, 4, 0, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(9, 1, 4, 0, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(10, 1, 4, 0, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(11, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(12, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(13, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(14, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(15, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(16, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(17, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(18, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(19, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(20, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(21, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(22, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(23, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(24, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(25, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(26, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(27, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(28, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(29, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(30, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(31, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(32, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(33, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(34, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(35, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(36, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(37, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(38, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(39, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(40, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(41, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(42, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(43, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(44, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(45, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(46, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(47, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(48, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(49, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(50, 1, 4, 1, NULL, NULL, '', 'STOCK', NULL, 3000, 3000.000, NULL, 101010, NULL),
(51, 2, 5, 1, NULL, NULL, '', 'SOLD', NULL, 1000, 1000.000, NULL, 101010, NULL),
(52, 2, 5, 1, NULL, NULL, '', 'SOLD', NULL, 1000, 1000.000, NULL, 101010, NULL),
(53, 2, 5, 1, NULL, NULL, '', 'SOLD', NULL, 1000, 1000.000, NULL, 101010, NULL),
(54, 2, 5, 1, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(55, 2, 5, 1, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(56, 2, 5, 0, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(57, 2, 5, 0, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(58, 2, 5, 0, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(59, 2, 5, 0, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(60, 2, 5, 0, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(61, 2, 5, 0, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(62, 2, 5, 0, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(63, 2, 5, 0, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(64, 2, 5, 1, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(65, 2, 5, 1, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(66, 2, 5, 1, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(67, 2, 5, 0, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(68, 2, 5, 0, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(69, 2, 5, 0, NULL, NULL, '', 'STOCK', NULL, 1000, 1000.000, NULL, 101010, NULL),
(70, 2, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 1000, 1000.000, NULL, 101010, NULL),
(71, 3, 4, 1, NULL, NULL, '', 'SOLD', NULL, 500, 500.000, NULL, 0, NULL),
(72, 3, 4, 1, NULL, NULL, '', 'STOCK', NULL, 500, 500.000, NULL, 0, NULL),
(73, 3, 4, 1, NULL, NULL, '', 'STOCK', NULL, 500, 500.000, NULL, 0, NULL),
(74, 3, 4, 1, NULL, NULL, '', 'STOCK', NULL, 500, 500.000, NULL, 0, NULL),
(75, 3, 4, 1, NULL, NULL, '', 'STOCK', NULL, 500, 500.000, NULL, 0, NULL),
(76, 3, 4, 1, NULL, NULL, '', 'STOCK', NULL, 500, 500.000, NULL, 0, NULL),
(77, 3, 4, 1, NULL, NULL, '', 'STOCK', NULL, 500, 500.000, NULL, 0, NULL),
(78, 3, 4, 1, NULL, NULL, '', 'STOCK', NULL, 500, 500.000, NULL, 0, NULL),
(79, 3, 4, 1, NULL, NULL, '', 'STOCK', NULL, 500, 500.000, NULL, 0, NULL),
(80, 3, 4, 1, NULL, NULL, '', 'STOCK', NULL, 500, 500.000, NULL, 0, NULL),
(81, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(82, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(83, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(84, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(85, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(86, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(87, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(88, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(89, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(90, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(91, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(92, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(93, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(94, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(95, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(96, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(97, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(98, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(99, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(100, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(101, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(102, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(103, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(104, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(105, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(106, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(107, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(108, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(109, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(110, 3, 4, NULL, NULL, NULL, '', 'INORDER', NULL, 500, 500.000, NULL, 0, NULL),
(111, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(112, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(113, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(114, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(115, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(116, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(117, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(118, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(119, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(120, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(121, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(122, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(123, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(124, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(125, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(126, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(127, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(128, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(129, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(130, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(131, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(132, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(133, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(134, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(135, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(136, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(137, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(138, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(139, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(140, 4, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(141, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(142, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(143, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(144, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(145, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(146, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(147, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(148, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(149, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(150, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(151, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(152, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(153, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(154, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(155, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(156, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(157, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(158, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(159, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(160, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(161, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(162, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(163, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(164, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(165, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(166, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(167, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(168, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(169, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(170, 5, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(171, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(172, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(173, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(174, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(175, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(176, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(177, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(178, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(179, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(180, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(181, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(182, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(183, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(184, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(185, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(186, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(187, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(188, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(189, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(190, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(191, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(192, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(193, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(194, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(195, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(196, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(197, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(198, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(199, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(200, 6, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(201, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(202, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(203, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(204, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(205, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(206, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(207, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(208, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(209, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(210, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(211, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(212, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(213, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(214, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(215, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(216, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(217, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(218, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(219, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(220, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(221, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(222, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(223, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(224, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(225, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(226, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(227, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(228, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(229, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(230, 7, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(231, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(232, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(233, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(234, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(235, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(236, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(237, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(238, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(239, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(240, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(241, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(242, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(243, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(244, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(245, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(246, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(247, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(248, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(249, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(250, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(251, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(252, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(253, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(254, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(255, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(256, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(257, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(258, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(259, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL),
(260, 8, 5, NULL, NULL, NULL, '', 'INORDER', NULL, 5800, 5800.000, NULL, 3, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_offer`
--

CREATE TABLE IF NOT EXISTS `sel_product_offer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `TYPE` varchar(20) NOT NULL,
  `INITIAL_DATE` date NOT NULL,
  `END_DATE` date NOT NULL,
  `POSiTION` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_sales`
--

CREATE TABLE IF NOT EXISTS `sel_product_sales` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRICE` float(9,3) DEFAULT NULL,
  `ADDED_DATE` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `SALE_ID` int(11) NOT NULL,
  `QUANTITY` int(11) DEFAULT NULL,
  `PRODUCT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SALE_ID` (`SALE_ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2048 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `sel_product_sales`
--

INSERT INTO `sel_product_sales` (`ID`, `PRICE`, `ADDED_DATE`, `SALE_ID`, `QUANTITY`, `PRODUCT_ID`) VALUES
(2, 1000.000, '0000-00-00 00:00:00', 35, 1, 20),
(3, 1000.000, '0000-00-00 00:00:00', 36, 1, 20),
(4, 1000.000, '0000-00-00 00:00:00', 37, 1, 20),
(5, 1000.000, '0000-00-00 00:00:00', 38, 1, 20),
(6, 1000.000, '0000-00-00 00:00:00', 39, 1, 20),
(7, 1000.000, '0000-00-00 00:00:00', 40, 1, 20),
(8, 1000.000, '0000-00-00 00:00:00', 41, 1, 20),
(9, 1000.000, '0000-00-00 00:00:00', 42, 1, 20),
(10, 500.000, '2015-07-12 20:26:48', 43, 1, 25),
(11, NULL, '2015-12-17 03:48:36', 44, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_provider`
--

CREATE TABLE IF NOT EXISTS `sel_provider` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPANY` varchar(100) DEFAULT NULL,
  `ADDRESS` varchar(100) DEFAULT NULL,
  `PERSON_ID` int(11) DEFAULT NULL,
  `TELEFONO` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PERSON_ID` (`PERSON_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2730 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `sel_provider`
--

INSERT INTO `sel_provider` (`ID`, `COMPANY`, `ADDRESS`, `PERSON_ID`, `TELEFONO`) VALUES
(14, 'P&C Importaciones', 'Av. Sargento Flores #982', NULL, '72459019'),
(15, 'Atlantis Ltda.', 'Av. Zofro Km 6 s/n ', NULL, '71126767'),
(16, 'Magnus Electronics', 'Av. 6 de Octubre #293', NULL, '52-89291'),
(17, 'Herbas Liendo Importaciones', 'Av. Tacno #902 esq. Murguia', NULL, ''),
(18, 'Herbas Liendo Importaciones', 'Av. Tacna #902 esq. Murguia', NULL, ''),
(19, 'Pascual Guerrero', 'calle Oblitas #982 Zona Norte', NULL, '52-43902');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_sales`
--

CREATE TABLE IF NOT EXISTS `sel_sales` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CUSTOMER_ID` int(11) DEFAULT NULL,
  `PRICE` float(9,2) DEFAULT NULL,
  `OBSERVACION` varchar(300) DEFAULT NULL,
  `DATE` datetime DEFAULT NULL,
  `STATUS` varchar(20) DEFAULT NULL,
  `BRANCH_ID` int(11) DEFAULT NULL,
  `EMPLOYEE_ID` int(11) DEFAULT NULL,
  `CODE` int(20) DEFAULT NULL,
  `MONEY_CHANGE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CUSTOMER_ID` (`CUSTOMER_ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`),
  KEY `EMPLOYEE_ID` (`EMPLOYEE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2048 AUTO_INCREMENT=45 ;

--
-- Volcado de datos para la tabla `sel_sales`
--

INSERT INTO `sel_sales` (`ID`, `CUSTOMER_ID`, `PRICE`, `OBSERVACION`, `DATE`, `STATUS`, `BRANCH_ID`, `EMPLOYEE_ID`, `CODE`, `MONEY_CHANGE`) VALUES
(35, NULL, 0.00, '', '2015-02-11 17:56:16', 'OPEN', 1, 3, 1, NULL),
(36, NULL, 0.00, '', '2015-02-11 17:57:36', 'OPEN', 1, 3, 2, NULL),
(37, NULL, 0.00, '', '2015-02-11 17:58:37', 'OPEN', 1, 3, 3, NULL),
(38, NULL, 0.00, '', '2015-02-11 17:59:10', 'OPEN', 1, 3, 4, NULL),
(39, NULL, 0.00, '', '2015-02-11 17:59:47', 'OPEN', 1, 3, 5, NULL),
(40, NULL, 0.00, '', '2015-02-11 18:02:28', 'OPEN', 1, 3, 6, NULL),
(41, NULL, 0.00, '', '2015-02-11 18:02:50', 'OPEN', 1, 3, 7, NULL),
(42, NULL, 0.00, '', '2015-02-11 18:04:06', 'OPEN', 1, 3, 8, NULL),
(43, NULL, 0.00, '', '2015-07-12 16:26:36', 'OPEN', 1, 3, 9, NULL),
(44, NULL, 0.00, NULL, '2015-12-16 23:48:10', 'OPEN', 1, NULL, 10, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_sale_x_items`
--

CREATE TABLE IF NOT EXISTS `sel_sale_x_items` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_SALE` int(11) DEFAULT NULL,
  `ID_PRODUCT_ITEM` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cho_rol_x_uri`
--
ALTER TABLE `cho_rol_x_uri`
  ADD CONSTRAINT `cho_rol_x_uri_fk1` FOREIGN KEY (`ROL_ID`) REFERENCES `cho_rol` (`ID`),
  ADD CONSTRAINT `cho_rol_x_uri_fk2` FOREIGN KEY (`URI_ID`) REFERENCES `cho_uri` (`ID`);

--
-- Filtros para la tabla `cho_uri`
--
ALTER TABLE `cho_uri`
  ADD CONSTRAINT `cho_uri_fk1` FOREIGN KEY (`MODULE_ID`) REFERENCES `cho_module` (`ID`);

--
-- Filtros para la tabla `cho_user_x_rol`
--
ALTER TABLE `cho_user_x_rol`
  ADD CONSTRAINT `cho_user_x_rol_fk1` FOREIGN KEY (`USER_ID`) REFERENCES `cho_user` (`ID`),
  ADD CONSTRAINT `cho_user_x_rol_fk2` FOREIGN KEY (`ROL_ID`) REFERENCES `cho_rol` (`ID`);

--
-- Filtros para la tabla `sel_acquisition`
--
ALTER TABLE `sel_acquisition`
  ADD CONSTRAINT `acquisition_product_fk1` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `sel_provider` (`ID`);

--
-- Filtros para la tabla `sel_branch_category`
--
ALTER TABLE `sel_branch_category`
  ADD CONSTRAINT `sel_branch_category_fk1` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`),
  ADD CONSTRAINT `sel_branch_category_fk2` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `sel_category` (`ID`);

--
-- Filtros para la tabla `sel_branch_employee`
--
ALTER TABLE `sel_branch_employee`
  ADD CONSTRAINT `sel_branch_employee_fk1` FOREIGN KEY (`EMPLOYEE_ID`) REFERENCES `sel_employee` (`ID`),
  ADD CONSTRAINT `sel_employee_branch` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`);

--
-- Filtros para la tabla `sel_category`
--
ALTER TABLE `sel_category`
  ADD CONSTRAINT `sel_category_fk1` FOREIGN KEY (`MAIN_CATEGORY_ID`) REFERENCES `sel_category` (`ID`);

--
-- Filtros para la tabla `sel_employee`
--
ALTER TABLE `sel_employee`
  ADD CONSTRAINT `sel_employee_fk1` FOREIGN KEY (`USER_ID`) REFERENCES `cho_user` (`ID`);

--
-- Filtros para la tabla `sel_product`
--
ALTER TABLE `sel_product`
  ADD CONSTRAINT `sel_product_fk1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `sel_category` (`ID`),
  ADD CONSTRAINT `sel_product_fk2` FOREIGN KEY (`PRODUCT_GROUP_ID`) REFERENCES `sel_product_group` (`ID`),
  ADD CONSTRAINT `sel_product_fk3` FOREIGN KEY (`BRAND_ID`) REFERENCES `sel_brand` (`ID`),
  ADD CONSTRAINT `sel_product_fk4` FOREIGN KEY (`MAIN_IMAGE_ID`) REFERENCES `sel_product_image` (`ID`);

--
-- Filtros para la tabla `sel_product_acquisition`
--
ALTER TABLE `sel_product_acquisition`
  ADD CONSTRAINT `sel_aquisition_product_fk1` FOREIGN KEY (`AQUISITION_ID`) REFERENCES `sel_acquisition` (`ID`),
  ADD CONSTRAINT `sel_product_aquisition_fk1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sel_product` (`ID`);

--
-- Filtros para la tabla `sel_product_group`
--
ALTER TABLE `sel_product_group`
  ADD CONSTRAINT `sel_product_group_fk1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `sel_category` (`ID`);

--
-- Filtros para la tabla `sel_product_image`
--
ALTER TABLE `sel_product_image`
  ADD CONSTRAINT `sel_product_image_fk1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sel_product` (`ID`);

--
-- Filtros para la tabla `sel_product_item`
--
ALTER TABLE `sel_product_item`
  ADD CONSTRAINT `sel_product_item_fk1` FOREIGN KEY (`PRODUCT_ACQUISITION_ID`) REFERENCES `sel_product_acquisition` (`ID`),
  ADD CONSTRAINT `sel_product_item_fk2` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`),
  ADD CONSTRAINT `sel_product_item_fk3` FOREIGN KEY (`BRAND_ID`) REFERENCES `sel_brand` (`ID`),
  ADD CONSTRAINT `sel_product_item_product_sale` FOREIGN KEY (`PRODUCT_SALES_ID`) REFERENCES `sel_product_sales` (`ID`);

--
-- Filtros para la tabla `sel_product_offer`
--
ALTER TABLE `sel_product_offer`
  ADD CONSTRAINT `product_offer_fk1` FOREIGN KEY (`ID`) REFERENCES `sel_product` (`ID`);

--
-- Filtros para la tabla `sel_product_sales`
--
ALTER TABLE `sel_product_sales`
  ADD CONSTRAINT `fk_sel_product_sale_product` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sel_product` (`ID`),
  ADD CONSTRAINT `sel_product_sale_sale` FOREIGN KEY (`SALE_ID`) REFERENCES `sel_sales` (`ID`);

--
-- Filtros para la tabla `sel_provider`
--
ALTER TABLE `sel_provider`
  ADD CONSTRAINT `sel_provider_fk1` FOREIGN KEY (`PERSON_ID`) REFERENCES `sel_person` (`ID`);

--
-- Filtros para la tabla `sel_sales`
--
ALTER TABLE `sel_sales`
  ADD CONSTRAINT `sel_sale_fk1` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sel_customer` (`ID`),
  ADD CONSTRAINT `sel_sale_fk2` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`),
  ADD CONSTRAINT `sel_sale_fk3` FOREIGN KEY (`EMPLOYEE_ID`) REFERENCES `sel_employee` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
