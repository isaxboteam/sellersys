-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 30-12-2014 a las 20:18:16
-- Versión del servidor: 5.5.16
-- Versión de PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sellersys`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_module`
--

CREATE TABLE IF NOT EXISTS `cho_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `URI` varchar(30) NOT NULL,
  `NAME` varchar(30) NOT NULL,
  `ACCESS` varchar(20) NOT NULL,
  `POSITION` int(11) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_MODULE_NAME` (`NAME`),
  UNIQUE KEY `NAME` (`NAME`),
  UNIQUE KEY `URI` (`URI`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `cho_module`
--

INSERT INTO `cho_module` (`ID`, `URI`, `NAME`, `ACCESS`, `POSITION`, `DESCRIPTION`) VALUES
(1, '', 'Portal', 'PUBLIC', 1, 'Portal p'),
(2, 'admin', 'Administracion', 'PRIVATE', 2, 'Administraci'),
(3, 'parametros', 'Pararametrós', 'PRIVATE', 3, 'Paramámetros'),
(4, 'catalogo', 'Categoria', 'PRIVATE', 4, 'Categoria'),
(5, 'acquisitionProduct', 'Adquisición', 'PRIVATE', 5, 'Adquisici'),
(6, 'organizacion', 'Organización', 'PRIVATE', 6, 'Modulo de organizaci');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_rol`
--

CREATE TABLE IF NOT EXISTS `cho_rol` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(20) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_CODE` (`CODE`),
  UNIQUE KEY `CODE` (`CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cho_rol`
--

INSERT INTO `cho_rol` (`ID`, `CODE`, `NAME`, `DESCRIPTION`) VALUES
(1, 'SUPER', 'Superusuario', ''),
(2, 'YPRA', 'YES', 'hyhhkhjk'),
(3, 'ADM', 'Admin', 'Administrador'),
(4, 'USR', 'User', 'No description'),
(5, 'ADM-CEN', 'Administrador de Centro', 'Administrador de Centro Infantil');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_rol_x_uri`
--

CREATE TABLE IF NOT EXISTS `cho_rol_x_uri` (
  `ROL_ID` int(11) NOT NULL,
  `URI_ID` int(11) NOT NULL,
  `READ` varchar(3) NOT NULL DEFAULT 'SI',
  `CREATE` varchar(3) NOT NULL DEFAULT 'NO',
  `UPDATE` varchar(3) NOT NULL DEFAULT 'NO',
  `DELETE` varchar(3) NOT NULL DEFAULT 'NO',
  PRIMARY KEY (`ROL_ID`,`URI_ID`),
  KEY `ROL_ID` (`ROL_ID`),
  KEY `URI_ID` (`URI_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=431;

--
-- Volcado de datos para la tabla `cho_rol_x_uri`
--

INSERT INTO `cho_rol_x_uri` (`ROL_ID`, `URI_ID`, `READ`, `CREATE`, `UPDATE`, `DELETE`) VALUES
(1, 4, 'SI', 'SI', 'SI', 'SI'),
(1, 9, 'SI', 'SI', 'SI', 'SI'),
(1, 11, 'SI', 'SI', 'SI', 'SI'),
(1, 12, 'SI', 'SI', 'SI', 'SI'),
(1, 13, 'SI', 'SI', 'SI', 'SI'),
(1, 14, 'SI', 'SI', 'SI', 'SI'),
(1, 15, 'SI', 'SI', 'SI', 'SI'),
(1, 16, 'SI', 'SI', 'SI', 'SI'),
(1, 17, 'SI', 'SI', 'SI', 'SI'),
(1, 18, 'SI', 'SI', 'SI', 'SI'),
(1, 19, 'SI', 'SI', 'SI', 'SI'),
(1, 27, 'SI', 'SI', 'SI', 'SI'),
(1, 28, 'SI', 'SI', 'SI', 'SI'),
(2, 4, 'SI', 'NO', 'NO', 'NO'),
(2, 18, 'SI', 'SI', 'SI', 'SI'),
(2, 19, 'SI', 'SI', 'SI', 'SI'),
(3, 4, 'SI', 'SI', 'SI', 'NO'),
(3, 9, 'SI', 'SI', 'SI', 'SI'),
(3, 11, 'SI', 'SI', 'SI', 'SI'),
(3, 12, 'SI', 'SI', 'SI', 'SI'),
(3, 13, 'SI', 'SI', 'SI', 'SI'),
(3, 14, 'SI', 'SI', 'SI', 'SI'),
(3, 15, 'SI', 'SI', 'SI', 'SI'),
(3, 16, 'SI', 'SI', 'SI', 'SI'),
(3, 17, 'SI', 'SI', 'SI', 'SI'),
(3, 18, 'SI', 'SI', 'SI', 'SI'),
(3, 19, 'SI', 'SI', 'SI', 'SI'),
(3, 27, 'SI', 'SI', 'SI', 'SI'),
(3, 28, 'SI', 'SI', 'SI', 'SI'),
(4, 18, 'SI', 'SI', 'SI', 'SI'),
(4, 19, 'SI', 'SI', 'SI', 'SI'),
(5, 18, 'SI', 'SI', 'SI', 'SI'),
(5, 19, 'SI', 'SI', 'SI', 'SI'),
(5, 23, 'SI', 'SI', 'SI', 'SI'),
(5, 26, 'SI', 'SI', 'SI', 'SI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_uri`
--

CREATE TABLE IF NOT EXISTS `cho_uri` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` int(11) NOT NULL,
  `URI` varchar(200) NOT NULL,
  `TITLE` varchar(50) NOT NULL,
  `ACCESS` varchar(20) NOT NULL,
  `TYPE` varchar(20) NOT NULL,
  `POSITION` int(11) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_URI` (`URI`),
  KEY `MODULE_ID` (`MODULE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2340 AUTO_INCREMENT=29 ;

--
-- Volcado de datos para la tabla `cho_uri`
--

INSERT INTO `cho_uri` (`ID`, `MODULE_ID`, `URI`, `TITLE`, `ACCESS`, `TYPE`, `POSITION`, `DESCRIPTION`) VALUES
(1, 1, 'index/', 'Inicio', 'PUBLIC', 'MENU', 1, 'P'),
(2, 1, 'index/index/', 'Segundo Inicio', 'PUBLIC', 'EXTRA', 2, 'Segunda P'),
(4, 2, 'rol/', 'Roles', 'PRIVATE', 'MENU', 5, ''),
(9, 2, 'uri/', 'URIs', 'PRIVATE', 'SECTION', 5, ''),
(11, 2, 'module/', 'M', 'PRIVATE', 'MENU', 5, ''),
(12, 2, 'user/', 'Usuarios', 'PRIVATE', 'MENU', 5, ''),
(13, 3, 'categoryType/', 'Categor', 'PRIVATE', 'MENU', 2, ''),
(14, 3, 'brand/', 'Marcas', 'PRIVATE', 'MENU', 5, ''),
(15, 3, 'trainingArea/', 'Areas de Formaci', 'PRIVATE', 'MENU', 4, ''),
(16, 3, 'kinship/', 'Parentezcos', 'PRIVATE', 'MENU', 4, ''),
(17, 4, 'category/', 'Categor', 'PRIVATE', 'MENU', 3, ''),
(18, 4, 'products/', 'Productos', 'PRIVATE', 'SECTION', 3, ''),
(19, 5, 'acquisitionProduct/', 'Compra de Productos', 'PRIVATE', 'MENU', 2, 'kllklljjggh'),
(20, 6, 'categoryXBranch/', 'Categorías de Sucursal', 'PRIVATE', 'SECTION', 8, ''),
(23, 6, 'employee/', 'Empleados', 'PRIVATE', 'MENU', 4, ''),
(26, 6, 'branch/', 'Sucursales', 'PRIVATE', 'MENU', 8, ''),
(27, 4, 'productItem/', 'Items', 'PRIVATE', 'SECTION', 4, ''),
(28, 5, 'provider/', 'Proveedores', 'PRIVATE', 'MENU', 2, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user`
--

CREATE TABLE IF NOT EXISTS `cho_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROL_ID` int(11) NOT NULL,
  `FIRST_LASTNAME` varchar(30) NOT NULL,
  `SECOND_LASTNAME` varchar(30) DEFAULT NULL,
  `FIRST_NAME` varchar(30) NOT NULL,
  `SECOND_NAME` varchar(30) DEFAULT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `EMAIL` varchar(80) NOT NULL,
  `DNI` varchar(20) DEFAULT NULL,
  `GENDER` varchar(10) DEFAULT NULL,
  `BIRTHDAY` date DEFAULT NULL,
  `TYPE` varchar(20) DEFAULT NULL,
  `STATUS` varchar(20) NOT NULL,
  `COUNTRY` varchar(30) DEFAULT NULL,
  `STATE` varchar(30) DEFAULT NULL,
  `CITY` varchar(50) DEFAULT NULL,
  `ADDRESS` text,
  `ZIP` varchar(10) DEFAULT NULL,
  `PRIMARY_PHONE` varchar(20) DEFAULT NULL,
  `SECONDARY_PHONE` varchar(20) DEFAULT NULL,
  `CELL_PHONE` varchar(20) DEFAULT NULL,
  `PHOTO_FORMAT` varchar(15) DEFAULT NULL,
  `REGISTERED_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_ACCESS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=455 AUTO_INCREMENT=56 ;

--
-- Volcado de datos para la tabla `cho_user`
--

INSERT INTO `cho_user` (`ID`, `ROL_ID`, `FIRST_LASTNAME`, `SECOND_LASTNAME`, `FIRST_NAME`, `SECOND_NAME`, `USERNAME`, `PASSWORD`, `EMAIL`, `DNI`, `GENDER`, `BIRTHDAY`, `TYPE`, `STATUS`, `COUNTRY`, `STATE`, `CITY`, `ADDRESS`, `ZIP`, `PRIMARY_PHONE`, `SECONDARY_PHONE`, `CELL_PHONE`, `PHOTO_FORMAT`, `REGISTERED_DATE`, `LAST_ACCESS`) VALUES
(1, 1, 'Rodriguez', 'Aranda', 'Yecid', '', 'yecid', 'yecid123', 'yecid@serviciosycompras.com', '1234567', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 00:00:00', '2014-05-08 22:36:33'),
(2, 1, 'Ticona', 'Menchaca', 'Clever', '', 'clever', 'clever123', 'clever@serviciosycompras.com', '6787889', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 00:00:00', '2014-05-08 22:36:33'),
(3, 1, 'Santana', 'Diaz', 'Abel', '', 'abel', 'abel123', 'abel@serviciosycompras.com', '5789543', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 00:00:00', '2014-05-08 22:36:33'),
(4, 1, 'Colomina', '', 'Juan', '', 'juan', 'juan123', 'juan@serviciosycompras.com', '6879654', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 00:00:00', '2014-05-08 22:36:33'),
(55, 1, 'Perez', 'Perez', 'Juan', 'Maria', 'juanperez', 'juanperez', 'juan.perez@gmail.com', '123456', 'MALE', '2019-01-01', 'EXTERNAL', 'CREATED', NULL, NULL, NULL, 'Calle Las Retamas #921', NULL, '2810927', NULL, '77719202', NULL, '2014-08-24 10:27:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user_admin`
--

CREATE TABLE IF NOT EXISTS `cho_user_admin` (
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096;

--
-- Volcado de datos para la tabla `cho_user_admin`
--

INSERT INTO `cho_user_admin` (`ID`) VALUES
(1),
(2),
(3),
(4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user_x_rol`
--

CREATE TABLE IF NOT EXISTS `cho_user_x_rol` (
  `USER_ID` int(11) NOT NULL,
  `ROL_ID` int(11) NOT NULL,
  PRIMARY KEY (`USER_ID`,`ROL_ID`),
  KEY `ROL_ID` (`ROL_ID`),
  KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=3276;

--
-- Volcado de datos para la tabla `cho_user_x_rol`
--

INSERT INTO `cho_user_x_rol` (`USER_ID`, `ROL_ID`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 5),
(55, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_acquisition`
--

CREATE TABLE IF NOT EXISTS `sel_acquisition` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` date DEFAULT NULL,
  `PROVIDER_ID` int(11) DEFAULT NULL,
  `OBSERVATION` text,
  PRIMARY KEY (`ID`),
  KEY `PROVIDER_ID` (`PROVIDER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `sel_acquisition`
--

INSERT INTO `sel_acquisition` (`ID`, `DATE`, `PROVIDER_ID`, `OBSERVATION`) VALUES
(2, '2014-10-20', 1, 'importaciones patito'),
(3, '2014-12-08', 11, 'me gusta mucho la cerveza');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_branch`
--

CREATE TABLE IF NOT EXISTS `sel_branch` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `ADDRESS` varchar(100) DEFAULT NULL,
  `TELEPHONE` int(20) DEFAULT NULL,
  `FAX` int(20) DEFAULT NULL,
  `CODE` varchar(20) DEFAULT NULL,
  `NUMBER` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_branch`
--

INSERT INTO `sel_branch` (`ID`, `NAME`, `ADDRESS`, `TELEPHONE`, `FAX`, `CODE`, `NUMBER`) VALUES
(0, 'Sucursal B', 'Av 20 de octubre, calle 1 ', 89898989, 54567800, '004', NULL),
(1, 'Sucursal A', 'Av 6 de agosto', 299777, 443434, '002', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_branch_employee`
--

CREATE TABLE IF NOT EXISTS `sel_branch_employee` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMPLOYEE_ID` int(11) DEFAULT NULL,
  `END_DATE` date DEFAULT NULL,
  `INIT_DATE` date DEFAULT NULL,
  `BRANCH_ID` int(11) DEFAULT NULL,
  `ADMIN` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`),
  KEY `EMPLOYEE_ID` (`EMPLOYEE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_brand`
--

CREATE TABLE IF NOT EXISTS `sel_brand` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `CODE` varchar(20) DEFAULT NULL,
  `OBSERVATION` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384;

--
-- Volcado de datos para la tabla `sel_brand`
--

INSERT INTO `sel_brand` (`ID`, `NAME`, `CODE`, `OBSERVATION`) VALUES
(0, 'brand', 'br', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_category`
--

CREATE TABLE IF NOT EXISTS `sel_category` (
  `NAME` varchar(20) NOT NULL,
  `CODE` varchar(20) NOT NULL,
  `MAIN_CATEGORY_ID` int(11) DEFAULT NULL,
  `DESCRIPTION` text,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`),
  KEY `MAIN_CATEGORY_ID` (`MAIN_CATEGORY_ID`,`ID`),
  KEY `MAIN_CATEGORY_ID_2` (`MAIN_CATEGORY_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1489 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `sel_category`
--

INSERT INTO `sel_category` (`NAME`, `CODE`, `MAIN_CATEGORY_ID`, `DESCRIPTION`, `ID`) VALUES
('Ropa', 'c1', NULL, 'ropas', 1),
('c2', 'cc2', NULL, 'fgfgfg', 2),
('c3', 'cc3', NULL, '', 3),
('c4', 'cc4', NULL, '', 4),
('c5', 'cc5', NULL, '', 5),
('c6', 'cc6', NULL, '', 7),
('pantalones', 'pat', 1, '', 8),
('MOuse', 'PRL', 1, '', 9),
('Parlantes', 'PRL', 1, '', 10),
('BAJOS', 'BJS', 1, '', 11),
('Ropa', 'rop-m', 1, 'Ropa de mujeres', 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_customer`
--

CREATE TABLE IF NOT EXISTS `sel_customer` (
  `ID` int(11) NOT NULL,
  `STATE` varchar(20) DEFAULT NULL,
  `CODE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_employee`
--

CREATE TABLE IF NOT EXISTS `sel_employee` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `INIT_DATE` date NOT NULL,
  `END_DATE` date NOT NULL,
  `DETAILS` text COLLATE utf8_bin,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_order_detail`
--

CREATE TABLE IF NOT EXISTS `sel_order_detail` (
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_order_product`
--

CREATE TABLE IF NOT EXISTS `sel_order_product` (
  `ID` int(11) NOT NULL,
  `DATE` date DEFAULT NULL,
  `STATE` varchar(20) DEFAULT NULL,
  `ORDER_DETAIL` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_person`
--

CREATE TABLE IF NOT EXISTS `sel_person` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `FIRST_LASTNAME` varchar(20) DEFAULT NULL,
  `SECOND_LASTNAME` varchar(20) DEFAULT NULL,
  `IDENTITY` int(11) DEFAULT NULL,
  `IDENTITY_EXT` varchar(20) DEFAULT NULL,
  `GENDER` varchar(20) DEFAULT NULL,
  `BIRTHDAY_DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product`
--

CREATE TABLE IF NOT EXISTS `sel_product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(20) DEFAULT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `DESCRIPTION` text,
  `CATEGORY_ID` int(11) NOT NULL,
  `MODEL` varchar(100) DEFAULT NULL,
  `NUMBER_PARTS` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `sel_product`
--

INSERT INTO `sel_product` (`ID`, `CODE`, `NAME`, `DESCRIPTION`, `CATEGORY_ID`, `MODEL`, `NUMBER_PARTS`) VALUES
(1, 'PC', 'Portatil', 'Portatil Samsumg', 3, NULL, NULL),
(2, 'sdfasdf', 'gdsddd', 'ffgggggggggggggggggg g', 1, NULL, NULL),
(3, 'codigolddfsdf', '1234', 'dfssssss', 8, NULL, NULL),
(5, 'blu', 'blusa', 'Blusa', 14, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_acquisition`
--

CREATE TABLE IF NOT EXISTS `sel_product_acquisition` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AQUISITION_ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `PRICE` float(15,2) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `AQUISITION_ID` (`AQUISITION_ID`),
  KEY `PRODUCT_ITEM_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `sel_product_acquisition`
--

INSERT INTO `sel_product_acquisition` (`ID`, `AQUISITION_ID`, `PRODUCT_ID`, `QUANTITY`, `PRICE`) VALUES
(1, 2, 3, 0, 0.00),
(2, 2, 5, 5, 78.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_detail_sell`
--

CREATE TABLE IF NOT EXISTS `sel_product_detail_sell` (
  `ID` int(11) NOT NULL,
  `PRODUCT_SELL_ID` int(11) DEFAULT NULL,
  `AMOUNT` int(11) DEFAULT NULL,
  `PRODUCTO_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PRODUCT_SELL_ID` (`PRODUCT_SELL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_image`
--

CREATE TABLE IF NOT EXISTS `sel_product_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(40) DEFAULT NULL,
  `SIZE` int(11) DEFAULT NULL,
  `LOCATION` varchar(100) DEFAULT NULL,
  `PRODUCT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_item`
--

CREATE TABLE IF NOT EXISTS `sel_product_item` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ACQUISITION_ID` int(11) DEFAULT NULL,
  `WARRANTY_SELL` int(11) DEFAULT NULL,
  `PRICE` double(15,3) DEFAULT NULL,
  `COST` int(11) DEFAULT NULL,
  `WARRANTY_PROVIDER` int(11) DEFAULT NULL,
  `MONEY` varchar(20) DEFAULT NULL,
  `BRAND_ID` int(11) DEFAULT NULL,
  `COLOR` int(11) DEFAULT NULL,
  `IMAGE` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ACQUISITION_ID`),
  KEY `BRAND_ID` (`BRAND_ID`),
  KEY `ID` (`ID`,`PRODUCT_ACQUISITION_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `sel_product_item`
--

INSERT INTO `sel_product_item` (`ID`, `PRODUCT_ACQUISITION_ID`, `WARRANTY_SELL`, `PRICE`, `COST`, `WARRANTY_PROVIDER`, `MONEY`, `BRAND_ID`, `COLOR`, `IMAGE`) VALUES
(5, 2, NULL, 78.000, 78, NULL, NULL, NULL, NULL, NULL),
(6, 2, NULL, 78.000, 78, NULL, NULL, NULL, NULL, NULL),
(7, 2, NULL, 78.000, 78, NULL, NULL, NULL, NULL, NULL),
(8, 2, NULL, 78.000, 78, NULL, NULL, NULL, NULL, NULL),
(9, 2, NULL, 78.000, 78, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_sell`
--

CREATE TABLE IF NOT EXISTS `sel_product_sell` (
  `ID` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) DEFAULT NULL,
  `DATE` date DEFAULT NULL,
  `BRANCH_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`),
  KEY `CUSTOMER_ID` (`CUSTOMER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_provider`
--

CREATE TABLE IF NOT EXISTS `sel_provider` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPANY` varchar(100) DEFAULT NULL,
  `ADDRESS` varchar(100) DEFAULT NULL,
  `PERSON_ID` int(11) DEFAULT NULL,
  `TELEFONO` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PERSON_ID` (`PERSON_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2730 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `sel_provider`
--

INSERT INTO `sel_provider` (`ID`, `COMPANY`, `ADDRESS`, `PERSON_ID`, `TELEFONO`) VALUES
(1, 'importadors DREF', 'Av. pensilvania #455', NULL, NULL),
(5, 'importadora', 'Av. pensilvania #455', NULL, NULL),
(10, 'YPFB', 'camacho', NULL, NULL),
(11, 'CBN', 'puente', NULL, NULL),
(12, 'LINE', 'USA', NULL, NULL),
(13, 'Streper', 'centro', NULL, '123456');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cho_rol_x_uri`
--
ALTER TABLE `cho_rol_x_uri`
  ADD CONSTRAINT `cho_rol_x_uri_fk1` FOREIGN KEY (`ROL_ID`) REFERENCES `cho_rol` (`ID`),
  ADD CONSTRAINT `cho_rol_x_uri_fk2` FOREIGN KEY (`URI_ID`) REFERENCES `cho_uri` (`ID`);

--
-- Filtros para la tabla `cho_uri`
--
ALTER TABLE `cho_uri`
  ADD CONSTRAINT `FK_URI_MODULE` FOREIGN KEY (`MODULE_ID`) REFERENCES `cho_module` (`ID`);

--
-- Filtros para la tabla `cho_user_admin`
--
ALTER TABLE `cho_user_admin`
  ADD CONSTRAINT `cho_user_adimn_fk` FOREIGN KEY (`ID`) REFERENCES `cho_user` (`ID`);

--
-- Filtros para la tabla `cho_user_x_rol`
--
ALTER TABLE `cho_user_x_rol`
  ADD CONSTRAINT `cho_user_x_rol_fk1` FOREIGN KEY (`USER_ID`) REFERENCES `cho_user` (`ID`),
  ADD CONSTRAINT `cho_user_x_rol_fk2` FOREIGN KEY (`ROL_ID`) REFERENCES `cho_rol` (`ID`);

--
-- Filtros para la tabla `sel_acquisition`
--
ALTER TABLE `sel_acquisition`
  ADD CONSTRAINT `acquisition_product_fk1` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `sel_provider` (`ID`);

--
-- Filtros para la tabla `sel_branch_employee`
--
ALTER TABLE `sel_branch_employee`
  ADD CONSTRAINT `sel_branch_employee_fk1` FOREIGN KEY (`EMPLOYEE_ID`) REFERENCES `sel_employee` (`ID`),
  ADD CONSTRAINT `sel_employee_branch` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`);

--
-- Filtros para la tabla `sel_category`
--
ALTER TABLE `sel_category`
  ADD CONSTRAINT `sel_category_fk1` FOREIGN KEY (`MAIN_CATEGORY_ID`) REFERENCES `sel_category` (`ID`);

--
-- Filtros para la tabla `sel_employee`
--
ALTER TABLE `sel_employee`
  ADD CONSTRAINT `sel_employee_fk1` FOREIGN KEY (`USER_ID`) REFERENCES `cho_user` (`ID`);

--
-- Filtros para la tabla `sel_product`
--
ALTER TABLE `sel_product`
  ADD CONSTRAINT `sel_product_fk1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `sel_category` (`ID`);

--
-- Filtros para la tabla `sel_product_acquisition`
--
ALTER TABLE `sel_product_acquisition`
  ADD CONSTRAINT `sel_product_aquisition_fk1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sel_product` (`ID`),
  ADD CONSTRAINT `sel_aquisition_product_fk1` FOREIGN KEY (`AQUISITION_ID`) REFERENCES `sel_acquisition` (`ID`);

--
-- Filtros para la tabla `sel_product_detail_sell`
--
ALTER TABLE `sel_product_detail_sell`
  ADD CONSTRAINT `sel_product_detail_sell_fk1` FOREIGN KEY (`PRODUCT_SELL_ID`) REFERENCES `sel_product_sell` (`ID`);

--
-- Filtros para la tabla `sel_product_image`
--
ALTER TABLE `sel_product_image`
  ADD CONSTRAINT `sel_product_image_fk1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sel_product` (`ID`);

--
-- Filtros para la tabla `sel_product_item`
--
ALTER TABLE `sel_product_item`
  ADD CONSTRAINT `sel_product_item_fk1` FOREIGN KEY (`PRODUCT_ACQUISITION_ID`) REFERENCES `sel_product_acquisition` (`ID`),
  ADD CONSTRAINT `sel_product_item_fk3` FOREIGN KEY (`BRAND_ID`) REFERENCES `sel_brand` (`ID`);

--
-- Filtros para la tabla `sel_product_sell`
--
ALTER TABLE `sel_product_sell`
  ADD CONSTRAINT `sel_product_sell_fk1` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`),
  ADD CONSTRAINT `sel_product_sell_fk2` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sel_customer` (`ID`);

--
-- Filtros para la tabla `sel_provider`
--
ALTER TABLE `sel_provider`
  ADD CONSTRAINT `sel_provider_fk1` FOREIGN KEY (`PERSON_ID`) REFERENCES `sel_person` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
