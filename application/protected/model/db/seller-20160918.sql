-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-09-2016 a las 23:48:05
-- Versión del servidor: 5.7.9
-- Versión de PHP: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sellersys`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_module`
--

DROP TABLE IF EXISTS `cho_module`;
CREATE TABLE IF NOT EXISTS `cho_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `URI` varchar(30) NOT NULL,
  `NAME` varchar(30) NOT NULL,
  `ACCESS` varchar(20) NOT NULL,
  `POSITION` int(11) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_MODULE_NAME` (`NAME`),
  UNIQUE KEY `NAME` (`NAME`),
  UNIQUE KEY `URI` (`URI`)
) ENGINE=InnoDB AUTO_INCREMENT=9 AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cho_module`
--

INSERT INTO `cho_module` (`ID`, `URI`, `NAME`, `ACCESS`, `POSITION`, `DESCRIPTION`) VALUES
(1, '', 'Portal', 'PUBLIC', 1, 'Portal público'),
(2, 'admin', 'Administracion', 'PRIVATE', 2, 'Administración General del Sistema'),
(3, 'params', 'Pararametrós', 'PRIVATE', 3, 'Paramámetros'),
(4, 'catalog', 'Catálogo', 'PRIVATE', 5, 'Categoria'),
(5, 'inventory', 'Inventario', 'PRIVATE', 6, 'Inventarios'),
(6, 'organization', 'Organización', 'PRIVATE', 4, 'Modulo de organización'),
(7, 'acquisition', 'Compras', 'PRIVATE', 7, 'Compras para inventario'),
(8, 'sales', 'Ventas', 'PRIVATE', 8, 'Módulo de ventas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_rol`
--

DROP TABLE IF EXISTS `cho_rol`;
CREATE TABLE IF NOT EXISTS `cho_rol` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(20) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_CODE` (`CODE`),
  UNIQUE KEY `CODE` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=8 AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cho_rol`
--

INSERT INTO `cho_rol` (`ID`, `CODE`, `NAME`, `DESCRIPTION`) VALUES
(1, 'SUPER', 'Superusuario', ''),
(2, 'VEN-SUC', 'Ventas de Sucursal', 'Rol asignado a Vendedores de Sucursal'),
(3, 'ADM', 'Admin', 'Administrador'),
(4, 'CONT', 'Contabilidad', 'Administrador Contable'),
(5, 'ADM-CEN', 'Administrador de Centro', 'Administrador de Centro Infantil'),
(6, 'INV', 'Inventarios', 'Administrador de Inventarios'),
(7, 'ADQ', 'Adquisiciones', 'Encargado de compras');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_rol_x_uri`
--

DROP TABLE IF EXISTS `cho_rol_x_uri`;
CREATE TABLE IF NOT EXISTS `cho_rol_x_uri` (
  `ROL_ID` int(11) NOT NULL,
  `URI_ID` int(11) NOT NULL,
  `READ` varchar(3) NOT NULL DEFAULT 'SI',
  `CREATE` varchar(3) NOT NULL DEFAULT 'NO',
  `UPDATE` varchar(3) NOT NULL DEFAULT 'NO',
  `DELETE` varchar(3) NOT NULL DEFAULT 'NO',
  PRIMARY KEY (`ROL_ID`,`URI_ID`),
  KEY `ROL_ID` (`ROL_ID`),
  KEY `URI_ID` (`URI_ID`)
) ENGINE=InnoDB AVG_ROW_LENGTH=431 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cho_rol_x_uri`
--

INSERT INTO `cho_rol_x_uri` (`ROL_ID`, `URI_ID`, `READ`, `CREATE`, `UPDATE`, `DELETE`) VALUES
(1, 4, 'SI', 'SI', 'SI', 'SI'),
(1, 9, 'SI', 'SI', 'SI', 'SI'),
(1, 11, 'SI', 'SI', 'SI', 'SI'),
(1, 12, 'SI', 'SI', 'SI', 'SI'),
(1, 14, 'SI', 'SI', 'SI', 'SI'),
(1, 17, 'SI', 'SI', 'SI', 'SI'),
(1, 18, 'SI', 'SI', 'SI', 'SI'),
(1, 19, 'SI', 'SI', 'SI', 'SI'),
(1, 20, 'SI', 'SI', 'SI', 'SI'),
(1, 23, 'SI', 'SI', 'SI', 'SI'),
(1, 26, 'SI', 'SI', 'SI', 'SI'),
(1, 27, 'SI', 'SI', 'SI', 'SI'),
(1, 28, 'SI', 'SI', 'SI', 'SI'),
(1, 29, 'SI', 'SI', 'SI', 'SI'),
(1, 32, 'SI', 'SI', 'SI', 'SI'),
(1, 33, 'SI', 'SI', 'SI', 'SI'),
(1, 34, 'SI', 'SI', 'SI', 'SI'),
(1, 35, 'SI', 'SI', 'SI', 'SI'),
(1, 36, 'SI', 'SI', 'SI', 'SI'),
(1, 37, 'SI', 'SI', 'SI', 'SI'),
(1, 38, 'SI', 'SI', 'SI', 'SI'),
(1, 39, 'SI', 'SI', 'SI', 'SI'),
(2, 4, 'SI', 'NO', 'NO', 'NO'),
(2, 18, 'SI', 'SI', 'SI', 'SI'),
(2, 35, 'SI', 'SI', 'SI', 'SI'),
(2, 36, 'SI', 'SI', 'SI', 'SI'),
(2, 37, 'SI', 'SI', 'SI', 'NO'),
(3, 4, 'SI', 'SI', 'SI', 'NO'),
(3, 9, 'SI', 'SI', 'SI', 'SI'),
(3, 11, 'SI', 'SI', 'SI', 'SI'),
(3, 12, 'SI', 'SI', 'SI', 'SI'),
(3, 14, 'SI', 'SI', 'SI', 'SI'),
(3, 17, 'SI', 'SI', 'SI', 'SI'),
(3, 18, 'SI', 'SI', 'SI', 'SI'),
(3, 19, 'SI', 'SI', 'SI', 'SI'),
(3, 20, 'SI', 'SI', 'SI', 'SI'),
(3, 23, 'SI', 'SI', 'SI', 'SI'),
(3, 26, 'SI', 'SI', 'SI', 'SI'),
(3, 27, 'SI', 'SI', 'SI', 'SI'),
(3, 28, 'SI', 'SI', 'SI', 'SI'),
(3, 29, 'SI', 'SI', 'SI', 'SI'),
(3, 32, 'SI', 'SI', 'SI', 'SI'),
(3, 33, 'SI', 'SI', 'SI', 'SI'),
(3, 34, 'SI', 'SI', 'SI', 'SI'),
(3, 35, 'SI', 'SI', 'SI', 'SI'),
(3, 36, 'SI', 'SI', 'SI', 'SI'),
(3, 37, 'SI', 'SI', 'SI', 'SI'),
(3, 38, 'SI', 'SI', 'SI', 'SI'),
(3, 39, 'SI', 'SI', 'SI', 'SI'),
(4, 18, 'SI', 'SI', 'SI', 'SI'),
(5, 18, 'SI', 'SI', 'SI', 'SI'),
(5, 20, 'SI', 'SI', 'SI', 'SI'),
(5, 23, 'SI', 'SI', 'SI', 'SI'),
(5, 26, 'SI', 'SI', 'SI', 'SI'),
(5, 29, 'SI', 'NO', 'NO', 'NO'),
(5, 35, 'SI', 'SI', 'SI', 'SI'),
(5, 36, 'SI', 'SI', 'SI', 'SI'),
(5, 37, 'SI', 'SI', 'SI', 'SI'),
(5, 39, 'SI', 'NO', 'NO', 'NO'),
(6, 29, 'SI', 'SI', 'SI', 'SI'),
(6, 38, 'SI', 'SI', 'SI', 'SI'),
(6, 39, 'SI', 'SI', 'SI', 'SI'),
(7, 19, 'SI', 'SI', 'SI', 'SI'),
(7, 28, 'SI', 'SI', 'SI', 'SI'),
(7, 32, 'SI', 'SI', 'SI', 'SI'),
(7, 33, 'SI', 'SI', 'SI', 'SI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_uri`
--

DROP TABLE IF EXISTS `cho_uri`;
CREATE TABLE IF NOT EXISTS `cho_uri` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` int(11) NOT NULL,
  `URI` varchar(200) NOT NULL,
  `TITLE` varchar(50) NOT NULL,
  `ACCESS` varchar(20) NOT NULL,
  `TYPE` varchar(20) NOT NULL,
  `POSITION` int(11) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_URI` (`URI`),
  KEY `MODULE_ID` (`MODULE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 AVG_ROW_LENGTH=2340 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cho_uri`
--

INSERT INTO `cho_uri` (`ID`, `MODULE_ID`, `URI`, `TITLE`, `ACCESS`, `TYPE`, `POSITION`, `DESCRIPTION`) VALUES
(1, 1, 'index/', 'Inicio', 'PUBLIC', 'MENU', 1, 'P'),
(2, 1, 'index/index/', 'Segundo Inicio', 'PUBLIC', 'EXTRA', 2, 'Segunda P'),
(4, 2, 'rol/', 'Roles', 'PRIVATE', 'MENU', 5, ''),
(9, 2, 'uri/', 'URIs', 'PRIVATE', 'SECTION', 5, ''),
(11, 2, 'module/', 'Módulos', 'PRIVATE', 'MENU', 5, ''),
(12, 2, 'user/', 'Usuarios', 'PRIVATE', 'MENU', 5, ''),
(14, 4, 'brand/', 'Marcas', 'PRIVATE', 'MENU', 1, ''),
(15, 4, 'trainingArea/', 'Otrin', 'PRIVATE', 'MENU', 2, ''),
(17, 4, 'category/', 'Categorías', 'PRIVATE', 'MENU', 3, ''),
(18, 4, 'products/', 'Productos', 'PRIVATE', 'SECTION', 5, ''),
(19, 7, 'acquisition/', 'Compra de Productos', 'PRIVATE', 'MENU', 4, ''),
(20, 6, 'categoryXBranch/', 'Categorías de Sucursal', 'PRIVATE', 'SECTION', 8, ''),
(23, 6, 'employee/', 'Empleados', 'PRIVATE', 'MENU', 4, ''),
(26, 6, 'branch/', 'Sucursales', 'PRIVATE', 'MENU', 8, ''),
(27, 4, 'productItems/', 'Items', 'PRIVATE', 'SECTION', 6, ''),
(28, 7, 'provider/', 'Proveedores', 'PRIVATE', 'MENU', 1, ''),
(29, 5, 'catalog/', 'Por Catálogo', 'PRIVATE', 'MENU', 2, 'Inventario por Catálogo'),
(32, 7, 'productItem/', 'Item del producto', 'PRIVATE', 'SECTION', 4, ''),
(33, 7, 'productAcquisition/', 'Adquisicion del Producto', 'PRIVATE', 'SECTION', 3, ''),
(34, 4, 'productGroup/', 'Grupos de Producto', 'PRIVATE', 'SECTION', 4, ''),
(35, 8, 'sale/', 'Ventas por Oficina', 'PRIVATE', 'MENU', 4, ''),
(36, 8, 'saleXItems/', 'Ventas por productos', 'PRIVATE', 'SECTION', 3, ''),
(37, 8, 'customer/', 'Clientes', 'PRIVATE', 'MENU', 1, 'Gestión de clientes'),
(38, 5, 'branchInventory/', 'Por Sucursal', 'PRIVATE', 'SECTION', 4, 'Inventario por Sucursal'),
(39, 5, 'inventory/', 'General', 'PRIVATE', 'SECTION', 4, 'Inventario General');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user`
--

DROP TABLE IF EXISTS `cho_user`;
CREATE TABLE IF NOT EXISTS `cho_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROL_ID` int(11) NOT NULL,
  `FIRST_LASTNAME` varchar(30) NOT NULL,
  `SECOND_LASTNAME` varchar(30) DEFAULT NULL,
  `FIRST_NAME` varchar(30) NOT NULL,
  `SECOND_NAME` varchar(30) DEFAULT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `EMAIL` varchar(80) NOT NULL,
  `DNI` varchar(20) DEFAULT NULL,
  `GENDER` varchar(10) DEFAULT NULL,
  `BIRTHDAY` date DEFAULT NULL,
  `TYPE` varchar(20) DEFAULT NULL,
  `STATUS` varchar(20) NOT NULL,
  `COUNTRY` varchar(30) DEFAULT NULL,
  `STATE` varchar(30) DEFAULT NULL,
  `CITY` varchar(50) DEFAULT NULL,
  `ADDRESS` text,
  `ZIP` varchar(10) DEFAULT NULL,
  `PRIMARY_PHONE` varchar(20) DEFAULT NULL,
  `SECONDARY_PHONE` varchar(20) DEFAULT NULL,
  `CELL_PHONE` varchar(20) DEFAULT NULL,
  `PHOTO_FORMAT` varchar(15) DEFAULT NULL,
  `REGISTERED_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_ACCESS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=61 AVG_ROW_LENGTH=455 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cho_user`
--

INSERT INTO `cho_user` (`ID`, `ROL_ID`, `FIRST_LASTNAME`, `SECOND_LASTNAME`, `FIRST_NAME`, `SECOND_NAME`, `USERNAME`, `PASSWORD`, `EMAIL`, `DNI`, `GENDER`, `BIRTHDAY`, `TYPE`, `STATUS`, `COUNTRY`, `STATE`, `CITY`, `ADDRESS`, `ZIP`, `PRIMARY_PHONE`, `SECONDARY_PHONE`, `CELL_PHONE`, `PHOTO_FORMAT`, `REGISTERED_DATE`, `LAST_ACCESS`) VALUES
(1, 1, 'ZEGARRA', 'SABA', 'IBRAHIM', 'GERARDO', 'ibrahim', 'miharbi', 'miharbi@gmail.com', '1234567', 'MALE', '2016-06-28', 'INTERNAL', 'ACTIVE', 'BOLIVIA', 'Oruro', 'Oruro', 'calle San felipe #175', NULL, NULL, NULL, NULL, NULL, '2010-01-01 00:00:00', '2014-05-08 22:36:33'),
(2, 1, 'Dunamis', 'System', 'Usuario', '', 'admin', 'admin', 'admin@dunamis.com', '4087895', NULL, NULL, 'INTERNO', 'ACTIVE', 'BOLIVIA', 'Oruro', 'Oruro', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 00:00:00', '2014-05-08 22:36:33'),
(55, 1, 'PARDO', 'CARVAJAL', 'JOSE', 'MARIO', 'juanperez', '123456', 'juan.perez@gmail.com', '6050403', 'MALE', '2016-01-09', 'INTERNAL', 'CREATED', 'BOLIVIA', 'Oruro', 'Oruro', 'Calle Las Retamas #921', NULL, '2810927', NULL, '77719202', NULL, '2014-08-24 10:27:00', '0000-00-00 00:00:00'),
(58, 1, 'CRUZ', 'CARRILLO', 'JAIME', 'NICOLAS', 'jaimico', '123456', 'jaime.cc@gmail.com', '6277899', 'MALE', '2016-01-09', 'INTERNAL', 'CREATED', NULL, NULL, NULL, 'Av. Dehene #289', NULL, '52-79856', NULL, '71865974', NULL, '2016-01-09 14:34:44', '0000-00-00 00:00:00'),
(59, 1, 'PEREDO', 'LAMAS', 'MARIO', 'EDUARDO', 'mperedo', '123456', 'marito.edu@gmail.com', '6209839 OR', 'MALE', '2016-06-28', 'INTERNAL', 'CREATED', NULL, NULL, NULL, 'calle Jaen #292', NULL, NULL, NULL, '71896542', NULL, '2016-06-28 01:14:12', '0000-00-00 00:00:00'),
(60, 1, 'HAYLLANI', 'CHOQUE', 'ANDRES', NULL, 'ahuayllani', '123456', 'andriway@hotmail.com', '6389289', 'MALE', '2016-06-28', 'INTERNAL', 'CREATED', NULL, NULL, NULL, 'Av. Dehene #782', NULL, '5273594', NULL, '67254810', NULL, '2016-06-28 01:30:13', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user_admin`
--

DROP TABLE IF EXISTS `cho_user_admin`;
CREATE TABLE IF NOT EXISTS `cho_user_admin` (
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cho_user_admin`
--

INSERT INTO `cho_user_admin` (`ID`) VALUES
(1),
(2),
(3),
(4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user_x_rol`
--

DROP TABLE IF EXISTS `cho_user_x_rol`;
CREATE TABLE IF NOT EXISTS `cho_user_x_rol` (
  `USER_ID` int(11) NOT NULL,
  `ROL_ID` int(11) NOT NULL,
  PRIMARY KEY (`USER_ID`,`ROL_ID`),
  KEY `ROL_ID` (`ROL_ID`),
  KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB AVG_ROW_LENGTH=3276 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cho_user_x_rol`
--

INSERT INTO `cho_user_x_rol` (`USER_ID`, `ROL_ID`) VALUES
(1, 1),
(1, 2),
(55, 2),
(58, 2),
(1, 3),
(1, 4),
(1, 5),
(55, 5),
(59, 6),
(60, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_acquisition`
--

DROP TABLE IF EXISTS `sel_acquisition`;
CREATE TABLE IF NOT EXISTS `sel_acquisition` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de compra',
  `DATE` date DEFAULT NULL COMMENT 'Fecha de compra',
  `PROVIDER_ID` int(11) DEFAULT NULL COMMENT 'ID de proveedor',
  `OBSERVATION` text COMMENT 'Observaciones de la compra',
  `WARRANTY_PROVIDER` varchar(30) DEFAULT NULL COMMENT 'Código de Garantia del proveedor',
  PRIMARY KEY (`ID`),
  KEY `PROVIDER_ID` (`PROVIDER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_acquisition`
--

INSERT INTO `sel_acquisition` (`ID`, `DATE`, `PROVIDER_ID`, `OBSERVATION`, `WARRANTY_PROVIDER`) VALUES
(1, '2016-07-05', 20, '', '6 meses'),
(2, '2016-07-08', 14, '', '24 meses'),
(3, '2016-08-11', 15, 'Sin observaciones', '36 meses'),
(4, '2016-08-09', 15, 'Observado en esto...', '8 meses');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_branch`
--

DROP TABLE IF EXISTS `sel_branch`;
CREATE TABLE IF NOT EXISTS `sel_branch` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de sucursal',
  `NUMBER` int(11) NOT NULL DEFAULT '0' COMMENT 'Numero de Sucursal',
  `STATUS` varchar(15) NOT NULL DEFAULT 'OPEN' COMMENT 'Estado de atención',
  `CODE` varchar(20) NOT NULL COMMENT 'Código de sucursal',
  `NAME` varchar(20) NOT NULL COMMENT 'Nombre de sucursal',
  `ADDRESS` varchar(100) NOT NULL COMMENT 'Dirección de sucursal',
  `PHONE` varchar(20) DEFAULT NULL COMMENT 'Teléfono de atención de sucursal',
  `FAX` varchar(20) DEFAULT NULL COMMENT 'Fax de contacto',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `sel_branch_idx1` (`CODE`),
  UNIQUE KEY `CODE` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=3 AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_branch`
--

INSERT INTO `sel_branch` (`ID`, `NUMBER`, `STATUS`, `CODE`, `NAME`, `ADDRESS`, `PHONE`, `FAX`) VALUES
(0, 2, 'ACTIVO', '002', 'Sucursal A', 'Calle 6 de Octubre #1562', '52-11452', '52-11452'),
(1, 1, 'ACTIVO', '001', 'Oficina Central', 'Calle Cochabamba #574', '52-11876', '52-11876'),
(2, 3, 'ACTIVO', '003', 'Sucursal B', 'Av. Dehene #2110', '52-78454', '52-78454');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_branch_category`
--

DROP TABLE IF EXISTS `sel_branch_category`;
CREATE TABLE IF NOT EXISTS `sel_branch_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de categoría de sucursal',
  `BRANCH_ID` int(11) NOT NULL COMMENT 'Id de sucursal',
  `CATEGORY_ID` int(11) NOT NULL COMMENT 'Id de categoría',
  `STATE` varchar(15) NOT NULL COMMENT 'Estado de categoría de sucursal',
  `OBSERVATION` text COMMENT 'Observacion de Categoría de Sucursal',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_branch_employee`
--

DROP TABLE IF EXISTS `sel_branch_employee`;
CREATE TABLE IF NOT EXISTS `sel_branch_employee` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de empleado en sucursal',
  `BRANCH_ID` int(11) NOT NULL COMMENT 'Id de sucursal',
  `EMPLOYEE_ID` int(11) NOT NULL COMMENT 'Id de empleado',
  `INIT_DATE` date NOT NULL COMMENT 'Fecha de Inicio de empleado en sucursal',
  `END_DATE` date NOT NULL COMMENT 'Fecha de fin de empleado en sucursal',
  `ADMIN` tinyint(1) NOT NULL COMMENT 'Indicador de administrador de sucursal',
  `STATUS` varchar(15) DEFAULT NULL COMMENT 'Estado de habiloiración de empleado en sucursal',
  PRIMARY KEY (`ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`),
  KEY `EMPLOYEE_ID` (`EMPLOYEE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_branch_employee`
--

INSERT INTO `sel_branch_employee` (`ID`, `BRANCH_ID`, `EMPLOYEE_ID`, `INIT_DATE`, `END_DATE`, `ADMIN`, `STATUS`) VALUES
(1, 1, 3, '2014-12-01', '2015-07-30', 1, 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_brand`
--

DROP TABLE IF EXISTS `sel_brand`;
CREATE TABLE IF NOT EXISTS `sel_brand` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `CODE` varchar(20) DEFAULT NULL,
  `DESCRIPTION` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_brand`
--

INSERT INTO `sel_brand` (`ID`, `NAME`, `CODE`, `DESCRIPTION`) VALUES
(1, 'Hewlet Packard', 'HP', ''),
(2, 'L&G', 'LG', 'Ideas for life'),
(3, 'Sony', 'SNY', ''),
(4, 'Acer', 'ACR', ''),
(5, 'Dell', 'DLL', ''),
(6, 'Samsung', 'SMS', ''),
(7, 'Delux', 'DLX', ''),
(8, 'Haier', 'HAI', ''),
(9, 'Daewoo', 'DWO', ''),
(10, 'Toshiba', 'TSH', ''),
(11, 'Master G', 'MTG', ''),
(12, 'HTC', 'HTC', ''),
(13, 'Genius', 'GNS', ''),
(14, 'Lenovo', 'LNV', ''),
(15, 'Top Link', 'TPLK', ''),
(16, 'D-Link', 'DLNK', ''),
(17, 'Cisco', 'CSC', ''),
(18, 'Asus', 'ASS', ''),
(19, 'Epson', 'EPS', ''),
(20, 'Canon', 'CNN', ''),
(21, 'Panasonic', 'PNS', ''),
(22, 'Philips', 'PHL', ''),
(23, 'Apple', 'APPLE', ''),
(24, 'Titan', 'TTN', 'Marca Titan');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_category`
--

DROP TABLE IF EXISTS `sel_category`;
CREATE TABLE IF NOT EXISTS `sel_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MAIN_CATEGORY_ID` int(11) DEFAULT NULL,
  `CODE` varchar(20) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  KEY `MAIN_CATEGORY_ID` (`MAIN_CATEGORY_ID`,`ID`),
  KEY `MAIN_CATEGORY_ID_2` (`MAIN_CATEGORY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 AVG_ROW_LENGTH=1489 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_category`
--

INSERT INTO `sel_category` (`ID`, `MAIN_CATEGORY_ID`, `CODE`, `NAME`, `DESCRIPTION`) VALUES
(15, NULL, 'CMPT', 'Computadoras y Tablets', ''),
(16, NULL, 'PTLL', 'Pantallas', ''),
(17, NULL, 'TLFR', 'Telefonía y Redes', ''),
(18, NULL, 'ENTR', 'Entretenimiento', ''),
(19, NULL, 'LNBL', 'Linea Blanca', ''),
(20, 15, 'PC', 'PCs de Escritorio', ''),
(21, 15, 'PRT', 'Portátiles', ''),
(22, 15, 'PRU', 'Ultraligeras', ''),
(23, 15, 'IMP', 'Impresoras', ''),
(24, 15, 'SFT', 'Software', ''),
(25, 15, 'TBL', 'Tabletas', ''),
(26, 16, 'MNT', 'Monitores', ''),
(28, 17, 'TLF', 'Teléfonos', ''),
(29, 17, 'MLT', 'Equipos Multilinea', ''),
(30, 17, 'ROU', 'Enrutadores', ''),
(31, 17, 'WFI', 'Equipos WiFi', ''),
(32, 18, 'CNS', 'Consolas', ''),
(33, 18, 'CSS', 'Accesorios de Juegos', ''),
(34, 16, 'MHD', 'Monitores HD +', ''),
(35, 19, 'ETF', 'Estufas', ''),
(36, 19, 'RFR', 'Refrigeradores', ''),
(37, 19, 'FTC', 'Fotocopiadoras', ''),
(38, 16, 'M4K', 'Monitores 4K', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_customer`
--

DROP TABLE IF EXISTS `sel_customer`;
CREATE TABLE IF NOT EXISTS `sel_customer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CUSTOMER_ACCOUNT_ID` int(11) DEFAULT NULL,
  `ID_NUMBER` varchar(30) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `LASTNAME` varchar(100) NOT NULL,
  `EMAIL` varchar(150) NOT NULL,
  `BIRTHDAY` date DEFAULT NULL,
  `ADDRESS` varchar(500) DEFAULT NULL,
  `PHONE` varchar(30) DEFAULT NULL,
  `CELLPHONE` varchar(30) DEFAULT NULL,
  `STATUS` varchar(20) DEFAULT NULL,
  `CODE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_NUMBER` (`ID_NUMBER`),
  UNIQUE KEY `CUSTOMER_ACCOUNT_ID` (`CUSTOMER_ACCOUNT_ID`),
  KEY `USER_ID` (`CUSTOMER_ACCOUNT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_customer`
--

INSERT INTO `sel_customer` (`ID`, `CUSTOMER_ACCOUNT_ID`, `ID_NUMBER`, `NAME`, `LASTNAME`, `EMAIL`, `BIRTHDAY`, `ADDRESS`, `PHONE`, `CELLPHONE`, `STATUS`, `CODE`) VALUES
(1, NULL, '5654566', 'FATIMA', 'GONZALES MONROY', 'miemail@email.com', NULL, 'Calle Tarija # 930 - Zona Sud', '52 64856', '71148566', NULL, NULL),
(2, NULL, '4908289', 'DAMIAN', 'TICONA ECHALAR', 'damian.te@gmail.com', NULL, 'Calle Colón #500 '' Zona Sud', '52 73546', '61187562', NULL, NULL),
(3, NULL, '7985520', 'JAVIER WILMER', 'LLANQUE LOPEZ', 'javi.tecno@hotmail.com', NULL, 'Calle Beni #34 '' Zona Norte', '52 41578', '78455660', NULL, NULL),
(4, NULL, '5641279', 'BRUNO DAVID', 'GALLEGOS DURAN', 'brunopeine@hotmail.com', NULL, 'Av. Sargento Flores esq. Potosí #758', '52 43612', '71856412', NULL, NULL),
(5, NULL, '4155641', 'CLAUDIA', 'OMONTE BELTRAN', 'claudinhatop@yahoo.com', NULL, 'Calle Washintong #301 - Zona Central', '52 22594', '76457820', NULL, NULL),
(6, NULL, '2057011', 'ERNESTO', 'QUINTANA SORIA', 'ernesto.quintana@gmail.com', NULL, 'Santa Barbara y Backovic #975', '52 11209', '66921803', NULL, NULL),
(7, NULL, '2158795', 'KARINA ORIANA', 'LEDEZMA CALLE', 'karina.led@gmail.com', NULL, 'Av. Dehene #920', '52 73652', '71184565', NULL, NULL),
(10, 3, '5456464', 'PAMELA', 'CHUQUIMIA', 'pamechuqui@gmail.com', NULL, 'av. villarroel #232', '52134234', '71145895', NULL, NULL),
(11, 4, '45454566', 'MICKY', 'JAMES', 'mickyjam@gmail.com', NULL, 'calle Peguot', '52-85452', '71232556', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_customer_account`
--

DROP TABLE IF EXISTS `sel_customer_account`;
CREATE TABLE IF NOT EXISTS `sel_customer_account` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(100) NOT NULL,
  `PASSWORD` varchar(200) NOT NULL,
  `LAST_ACCESS` timestamp NULL DEFAULT NULL,
  `CURRENT_ACCESS` timestamp NULL DEFAULT NULL,
  `CREATION_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Volcado de datos para la tabla `sel_customer_account`
--

INSERT INTO `sel_customer_account` (`ID`, `EMAIL`, `PASSWORD`, `LAST_ACCESS`, `CURRENT_ACCESS`, `CREATION_DATE`) VALUES
(3, 'pamechuqui@gmail.com', '123456', '2016-08-02 04:00:00', NULL, '2016-08-02 07:01:42'),
(4, 'mickyjam@gmail.com', '123456', '2016-08-03 04:00:00', NULL, '2016-08-03 05:42:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_employee`
--

DROP TABLE IF EXISTS `sel_employee`;
CREATE TABLE IF NOT EXISTS `sel_employee` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `INIT_DATE` date NOT NULL,
  `END_DATE` date NOT NULL,
  `DETAILS` text,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_employee`
--

INSERT INTO `sel_employee` (`ID`, `USER_ID`, `INIT_DATE`, `END_DATE`, `DETAILS`) VALUES
(3, 55, '2016-08-03', '2016-08-03', 'SDFSDF'),
(4, 55, '2016-01-09', '2016-01-09', NULL),
(5, 1, '2016-08-03', '2016-08-03', NULL),
(9, 59, '2016-06-28', '2016-06-28', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_order`
--

DROP TABLE IF EXISTS `sel_order`;
CREATE TABLE IF NOT EXISTS `sel_order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CUSTOMER_ID` int(11) NOT NULL,
  `SALES_ID` int(11) DEFAULT NULL,
  `STATUS` varchar(20) NOT NULL,
  `SHIPPING_ADDRESS` text NOT NULL,
  `PHONE` varchar(30) NOT NULL,
  `CUSTOMMER_COMMENT` text NOT NULL,
  `SALES_COMMENT` int(11) DEFAULT NULL,
  `CREATION_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ORDER_DATE` timestamp NULL DEFAULT NULL,
  `PAYMENT_DATE` timestamp NULL DEFAULT NULL,
  `PAYMENT_CODE` varchar(30) DEFAULT NULL,
  `PAYMENT_ENTITY` varchar(200) DEFAULT NULL,
  `CHECK_DATE` timestamp NULL DEFAULT NULL,
  `APPROVED_DATE` timestamp NULL DEFAULT NULL,
  `SHIPPING_DATE` timestamp NULL DEFAULT NULL,
  `DELIVERY_DATE` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SALES_ID` (`SALES_ID`),
  KEY `CUSTOMER_ID` (`CUSTOMER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_person`
--

DROP TABLE IF EXISTS `sel_person`;
CREATE TABLE IF NOT EXISTS `sel_person` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `FIRST_LASTNAME` varchar(20) DEFAULT NULL,
  `SECOND_LASTNAME` varchar(20) DEFAULT NULL,
  `IDENTITY` int(11) DEFAULT NULL,
  `IDENTITY_EXT` varchar(20) DEFAULT NULL,
  `GENDER` varchar(20) DEFAULT NULL,
  `BIRTHDAY_DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product`
--

DROP TABLE IF EXISTS `sel_product`;
CREATE TABLE IF NOT EXISTS `sel_product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_GROUP_ID` int(11) DEFAULT NULL,
  `BRAND_ID` int(11) NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `MAIN_IMAGE_ID` int(11) DEFAULT NULL,
  `CODE` varchar(20) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `BASE_PRICE` float(9,3) NOT NULL,
  `DESCRIPTION` text,
  `MODEL` varchar(100) DEFAULT NULL,
  `NUMBER_PARTS` int(11) DEFAULT NULL,
  `IS_MAIN` tinyint(1) NOT NULL DEFAULT '0',
  `IS_NEW` tinyint(1) NOT NULL DEFAULT '0',
  `IS_OFFER` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`),
  KEY `PRODUCT_GROUP_ID` (`PRODUCT_GROUP_ID`),
  KEY `BRAND_ID` (`BRAND_ID`),
  KEY `MAIN_IMAGE_ID` (`MAIN_IMAGE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=35 AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_product`
--

INSERT INTO `sel_product` (`ID`, `PRODUCT_GROUP_ID`, `BRAND_ID`, `CATEGORY_ID`, `MAIN_IMAGE_ID`, `CODE`, `NAME`, `BASE_PRICE`, `DESCRIPTION`, `MODEL`, `NUMBER_PARTS`, `IS_MAIN`, `IS_NEW`, `IS_OFFER`) VALUES
(18, 1, 14, 21, 11, 'LNXNR14', 'Laptop Lanix Neuron', 4350.000, NULL, 'Neuron', NULL, 0, 1, 1),
(19, 1, 5, 21, 8, 'LDINS14', 'Laptop Dell Inspiron', 5900.000, NULL, 'Inspiron', NULL, 0, 0, 1),
(20, 2, 5, 21, 9, 'LDINS15', 'Laptop Dell Inspiron', 6500.000, NULL, 'Inspiron', NULL, 0, 0, 1),
(21, 1, 1, 21, 17, 'HPPAV14', 'Notebook HP Pavilion 14"', 4600.000, NULL, 'Pavilion', NULL, 0, 1, 1),
(22, 1, 1, 21, 3, 'CHRMBE', 'Chromebook 14"', 4950.000, NULL, 'Chromebook', NULL, 0, 0, 1),
(23, 1, 4, 21, 6, 'ACUM', 'Laptop Acer UMAC 14', 5200.000, NULL, 'UMAC', NULL, 0, 0, 1),
(24, 3, 4, 21, 15, 'HPBTS16', 'Notebook HP Beats 15.8', 5800.000, NULL, 'ENVY', NULL, 0, 0, 1),
(25, 10, 1, 23, NULL, 'HP 2555', 'HP laser', 1850.000, 'HP laser', '2555', NULL, 0, 1, 1),
(26, 10, 19, 23, NULL, 'EPS-520', 'Epson Laser', 2680.000, NULL, '520', NULL, 0, 0, 0),
(27, 11, 20, 23, NULL, 'CAN-1200', 'Canon IP', 480.000, NULL, '1200', NULL, 0, 0, 0),
(32, NULL, 20, 37, NULL, 'CNN-825', 'Fotocopiadora Canon 825 ', 0.000, 'Fotocopiadora de alto rendimiento Canon 825', 'X10', NULL, 0, 1, 0),
(33, NULL, 19, 37, NULL, 'EPS-5278', 'Fotocopiadora Epson 5278', 0.000, 'Fotocopiadora de Alto Tráfico', 'EPS-5278', NULL, 0, 0, 0),
(34, 15, 10, 21, 24, 'XTERRA', 'Xterra Netbook', 1050.000, 'Computadora portatil reducida', '2001', NULL, 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_acquisition`
--

DROP TABLE IF EXISTS `sel_product_acquisition`;
CREATE TABLE IF NOT EXISTS `sel_product_acquisition` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AQUISITION_ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `PRICE` float(15,2) NOT NULL,
  `MONEY` varchar(20) DEFAULT NULL,
  `WARRANTY_PROVIDER` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `AQUISITION_ID` (`AQUISITION_ID`),
  KEY `PRODUCT_ITEM_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_product_acquisition`
--

INSERT INTO `sel_product_acquisition` (`ID`, `AQUISITION_ID`, `PRODUCT_ID`, `QUANTITY`, `PRICE`, `MONEY`, `WARRANTY_PROVIDER`) VALUES
(15, 1, 19, 10, 2122.00, 'Bs', NULL),
(16, 1, 23, 50, 2180.00, 'Bs', NULL),
(17, 1, 27, 30, 295.00, 'Bs', NULL),
(18, 2, 25, 80, 920.00, 'Bs', NULL),
(19, 2, 21, 50, 3800.00, 'Bs', NULL),
(20, 2, 22, 50, 3690.00, 'Bs', NULL),
(21, 2, 19, 20, 2750.00, 'Bs', NULL),
(22, 3, 27, 39, 395.00, 'Bs', NULL),
(23, 3, 24, 20, 4990.00, 'Bs', NULL),
(24, 4, 21, 10, 4100.00, 'Bs', NULL),
(25, 4, 23, 10, 4850.00, 'Bs', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_group`
--

DROP TABLE IF EXISTS `sel_product_group`;
CREATE TABLE IF NOT EXISTS `sel_product_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATEGORY_ID` int(11) NOT NULL,
  `CODE` varchar(20) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 AVG_ROW_LENGTH=1820 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_product_group`
--

INSERT INTO `sel_product_group` (`ID`, `CATEGORY_ID`, `CODE`, `NAME`, `DESCRIPTION`) VALUES
(1, 21, 'LPT14', 'Laptop de 14"', '0'),
(2, 21, 'LPT15', 'Laptop de 15"', '0'),
(3, 21, 'LPT16', 'Laptop de 16"', '0'),
(4, 21, 'LPT17', 'Laptop de 17"', '0'),
(5, 20, 'CR2D', 'Core 2 Duo', '0'),
(6, 20, 'CCRI3', 'Core i3', '0'),
(7, 20, 'CRi5', 'Core i5', '0'),
(8, 20, 'CRi7', 'Core i7', '0'),
(9, 20, 'DUCr', 'Dual Core', '0'),
(10, 23, 'IMP-LAS', 'Impresoras Laser', '*****'),
(11, 23, 'IMP-MULT', 'Impresoras Multifunción', 'Impresoras Multifunción'),
(12, 23, 'IMPR', 'Impresora', NULL),
(13, 36, 'FRZ', 'Freezers', 'Freezers congeladoras'),
(14, 36, 'HLD', 'Heladeras', 'Heladeras comerciales'),
(15, 21, 'NETBK', 'Netbooks', 'Computadoras portátiles pequeñas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_image`
--

DROP TABLE IF EXISTS `sel_product_image`;
CREATE TABLE IF NOT EXISTS `sel_product_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `SIZE` int(11) NOT NULL,
  `MIMETYPE` varchar(30) DEFAULT NULL,
  `POSITION` int(11) NOT NULL,
  `TITLE` varchar(100) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_product_image`
--

INSERT INTO `sel_product_image` (`ID`, `PRODUCT_ID`, `NAME`, `SIZE`, `MIMETYPE`, `POSITION`, `TITLE`, `DESCRIPTION`) VALUES
(3, 22, 'Acer-Chromebook-Laptop.jpg', 23319, 'image/jpeg', 1, 'Chromebook Xpert', NULL),
(5, 22, 'dsnew-laptop-drawer-6-2.jpg', 102232, 'image/jpeg', 2, 'ChromeBook roja', 'Varidad de colores que se adaptan a tu estilo'),
(6, 23, '1_37_384.jpg', 82321, 'image/jpeg', 1, 'UMAC ACER Super Speed', NULL),
(7, 23, 'dsnew-laptop-drawer-6-2.jpg', 102232, 'image/jpeg', 2, 'Stylus Online', NULL),
(8, 19, 'dell-inspiron-15r-laptop-notebook-back-to-school.png', 603800, 'image/png', 1, 'Del Inspiron XSpeed', NULL),
(9, 20, 'dsnew-laptop-drawer-6-2.jpg', 102232, 'image/jpeg', 1, 'Del Inspiron Smart', NULL),
(10, 18, 'feno4.jpg', 54459, 'image/jpeg', 1, 'Xtra High Speed', NULL),
(11, 18, 'feno4.jpg', 54459, 'image/jpeg', 3, 'Fenooo', NULL),
(12, 18, 'top-ten-laptop-brands.jpg', 115791, 'image/jpeg', 2, 'Top[ ten', NULL),
(13, 24, 'Gigabyte_P37X_gaming_laptop_800_thumb800.jpg', 25991, 'image/jpeg', 1, 'ENVY Slim Ultrabook', NULL),
(14, 24, 'top-ten-laptop-brands.jpg', 115791, 'image/jpeg', 2, 'tENVY', NULL),
(15, 24, 'HP255Laptop.jpg', 118879, 'image/jpeg', 3, 'Notebook HP 2371i', NULL),
(16, 24, 'laptop.jpg', 12457, 'image/jpeg', 4, 'Another HP Notebook', NULL),
(17, 21, 'P18320479.jpg', 32527, 'image/jpeg', 1, 'HP Pavillion', 'Super Laptop'),
(18, 21, 'Toughbook-cf-m34_4.jpg', 669819, 'image/jpeg', 2, 'Nodes', NULL),
(19, 21, 'lenovo-laptop-thinkpad-yoga.png', 104656, 'image/png', 3, 'HPnovo', NULL),
(20, 21, 'laptop_PNG5905.png', 731795, 'image/png', 4, 'Laptop HP', NULL),
(21, 23, 'laptops.jpg', 45340, 'image/jpeg', 3, 'Variedad de Colores', NULL),
(22, 18, 'lenovo-laptop-lenovo-yoga-13-orange-back.png', 121587, 'image/png', 4, '360 grados de giro', NULL),
(23, 22, 'laptop_PNG5905.png', 731795, 'image/png', 3, 'Laptop', NULL),
(24, 34, 'Toughbook-cf-m34_4.jpg', 669819, 'image/jpeg', 1, 'Xterra Netbook', NULL),
(25, 34, 'laptop-lenovo.jpg', 33639, 'image/jpeg', 2, 'Xterra lnvo', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_item`
--

DROP TABLE IF EXISTS `sel_product_item`;
CREATE TABLE IF NOT EXISTS `sel_product_item` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ACQUISITION_ID` int(11) NOT NULL,
  `BRAND_ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) DEFAULT NULL,
  `PRODUCT_DEPOSIT_ID` int(11) DEFAULT NULL,
  `PRODUCT_SELL_ID` int(11) DEFAULT NULL,
  `CODE` varchar(20) NOT NULL,
  `STATUS` varchar(15) NOT NULL,
  `COST` float(9,3) NOT NULL,
  `PRICE` float(9,3) DEFAULT NULL,
  `COLOR` varchar(30) DEFAULT NULL,
  `WARRANTY_SELL` int(11) DEFAULT NULL,
  `OBSERVATION` text,
  `REGISTER_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ACQUISITION_ID`),
  KEY `BRAND_ID` (`BRAND_ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`),
  KEY `PRODUCT_SALES_ID` (`PRODUCT_SELL_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=370 AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_product_item`
--

INSERT INTO `sel_product_item` (`ID`, `PRODUCT_ACQUISITION_ID`, `BRAND_ID`, `BRANCH_ID`, `PRODUCT_DEPOSIT_ID`, `PRODUCT_SELL_ID`, `CODE`, `STATUS`, `COST`, `PRICE`, `COLOR`, `WARRANTY_SELL`, `OBSERVATION`, `REGISTER_DATE`) VALUES
(1, 15, 5, 1, NULL, 3, '19-1469313096-1', 'SOLD', 2122.000, 5900.000, NULL, NULL, NULL, '2016-07-23 22:31:36'),
(2, 15, 5, 1, NULL, 3, '19-1469313096-2', 'SOLD', 2122.000, 5900.000, NULL, NULL, NULL, '2016-07-23 22:31:36'),
(3, 15, 5, 1, NULL, 3, '19-1469313096-3', 'SOLD', 2122.000, 5900.000, NULL, NULL, NULL, '2016-07-23 22:31:36'),
(4, 15, 5, 1, NULL, 3, '19-1469313096-4', 'SOLD', 2122.000, 5900.000, NULL, NULL, NULL, '2016-07-23 22:31:36'),
(5, 15, 5, 1, NULL, 3, '19-1469313096-5', 'SOLD', 2122.000, 5900.000, NULL, NULL, NULL, '2016-07-23 22:31:36'),
(6, 15, 5, 1, NULL, 3, '19-1469313096-6', 'SOLD', 2122.000, 5900.000, NULL, NULL, NULL, '2016-07-23 22:31:36'),
(7, 15, 5, 1, NULL, 3, '19-1469313096-7', 'SOLD', 2122.000, 5900.000, NULL, NULL, NULL, '2016-07-23 22:31:36'),
(8, 15, 5, 1, NULL, 3, '19-1469313096-8', 'SOLD', 2122.000, 5900.000, NULL, NULL, NULL, '2016-07-23 22:31:36'),
(9, 15, 5, 1, NULL, 3, '19-1469313096-9', 'SOLD', 2122.000, 5900.000, NULL, NULL, NULL, '2016-07-23 22:31:36'),
(10, 15, 5, 1, NULL, 3, '19-1469313096-10', 'SOLD', 2122.000, 5900.000, NULL, NULL, NULL, '2016-07-23 22:31:36'),
(11, 16, 4, 1, NULL, 10, '23-1469313250-1', 'SOLD', 2180.000, 5200.000, NULL, NULL, NULL, '2016-07-23 22:34:10'),
(12, 16, 4, 1, NULL, 10, '23-1469313250-2', 'SOLD', 2180.000, 5200.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(13, 16, 4, 1, NULL, NULL, '23-1469313250-3', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(14, 16, 4, 1, NULL, NULL, '23-1469313250-4', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(15, 16, 4, 1, NULL, NULL, '23-1469313250-5', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(16, 16, 4, 1, NULL, NULL, '23-1469313250-6', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(17, 16, 4, 1, NULL, NULL, '23-1469313250-7', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(18, 16, 4, 1, NULL, NULL, '23-1469313250-8', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(19, 16, 4, 1, NULL, NULL, '23-1469313250-9', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(20, 16, 4, 1, NULL, NULL, '23-1469313250-10', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(21, 16, 4, 1, NULL, NULL, '23-1469313250-11', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(22, 16, 4, 1, NULL, NULL, '23-1469313250-12', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(23, 16, 4, 1, NULL, NULL, '23-1469313250-13', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(24, 16, 4, 1, NULL, NULL, '23-1469313250-14', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(25, 16, 4, 1, NULL, NULL, '23-1469313250-15', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(26, 16, 4, 1, NULL, NULL, '23-1469313250-16', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(27, 16, 4, 1, NULL, NULL, '23-1469313250-17', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(28, 16, 4, 1, NULL, NULL, '23-1469313250-18', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(29, 16, 4, 1, NULL, NULL, '23-1469313250-19', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(30, 16, 4, 1, NULL, NULL, '23-1469313250-20', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(31, 16, 4, 0, NULL, NULL, '23-1469313250-21', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(32, 16, 4, 0, NULL, NULL, '23-1469313250-22', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(33, 16, 4, 0, NULL, NULL, '23-1469313250-23', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(34, 16, 4, 0, NULL, NULL, '23-1469313250-24', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(35, 16, 4, 0, NULL, NULL, '23-1469313250-25', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(36, 16, 4, 0, NULL, NULL, '23-1469313250-26', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(37, 16, 4, 0, NULL, NULL, '23-1469313250-27', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(38, 16, 4, 0, NULL, NULL, '23-1469313250-28', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(39, 16, 4, 0, NULL, NULL, '23-1469313250-29', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(40, 16, 4, 0, NULL, NULL, '23-1469313250-30', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(41, 16, 4, 0, NULL, NULL, '23-1469313250-31', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(42, 16, 4, 0, NULL, NULL, '23-1469313250-32', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(43, 16, 4, 0, NULL, NULL, '23-1469313250-33', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(44, 16, 4, 0, NULL, NULL, '23-1469313250-34', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(45, 16, 4, 0, NULL, NULL, '23-1469313250-35', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(46, 16, 4, 0, NULL, NULL, '23-1469313250-36', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(47, 16, 4, 0, NULL, NULL, '23-1469313250-37', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(48, 16, 4, 0, NULL, NULL, '23-1469313250-38', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(49, 16, 4, 0, NULL, NULL, '23-1469313250-39', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(50, 16, 4, 0, NULL, NULL, '23-1469313250-40', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(51, 16, 4, 0, NULL, NULL, '23-1469313250-41', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(52, 16, 4, 0, NULL, NULL, '23-1469313250-42', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(53, 16, 4, 0, NULL, NULL, '23-1469313250-43', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(54, 16, 4, 0, NULL, NULL, '23-1469313250-44', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(55, 16, 4, 0, NULL, NULL, '23-1469313250-45', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(56, 16, 4, 0, NULL, NULL, '23-1469313250-46', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(57, 16, 4, 0, NULL, NULL, '23-1469313250-47', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(58, 16, 4, 0, NULL, NULL, '23-1469313250-48', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(59, 16, 4, 0, NULL, NULL, '23-1469313250-49', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(60, 16, 4, 0, NULL, NULL, '23-1469313250-50', 'STOCK', 2180.000, 2180.000, NULL, NULL, NULL, '2016-07-23 22:34:11'),
(61, 17, 20, 1, NULL, 4, '27-1469313337-1', 'SOLD', 295.000, 480.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(62, 17, 20, 1, NULL, 4, '27-1469313337-2', 'SOLD', 295.000, 480.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(63, 17, 20, 1, NULL, 4, '27-1469313337-3', 'SOLD', 295.000, 480.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(64, 17, 20, 1, NULL, 4, '27-1469313337-4', 'SOLD', 295.000, 480.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(65, 17, 20, 1, NULL, 4, '27-1469313337-5', 'SOLD', 295.000, 480.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(66, 17, 20, 1, NULL, 6, '27-1469313337-6', 'SOLD', 295.000, 480.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(67, 17, 20, 1, NULL, 8, '27-1469313337-7', 'SOLD', 295.000, 480.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(68, 17, 20, 1, NULL, NULL, '27-1469313337-8', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(69, 17, 20, 1, NULL, NULL, '27-1469313337-9', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(70, 17, 20, 1, NULL, NULL, '27-1469313337-10', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(71, 17, 20, 0, NULL, NULL, '27-1469313337-11', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(72, 17, 20, 0, NULL, NULL, '27-1469313337-12', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(73, 17, 20, 0, NULL, NULL, '27-1469313337-13', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(74, 17, 20, 0, NULL, NULL, '27-1469313337-14', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(75, 17, 20, 0, NULL, NULL, '27-1469313337-15', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(76, 17, 20, 0, NULL, NULL, '27-1469313337-16', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(77, 17, 20, 0, NULL, NULL, '27-1469313337-17', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(78, 17, 20, 0, NULL, NULL, '27-1469313337-18', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(79, 17, 20, 0, NULL, NULL, '27-1469313337-19', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(80, 17, 20, 0, NULL, NULL, '27-1469313337-20', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(81, 17, 20, NULL, NULL, NULL, '27-1469313337-21', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(82, 17, 20, NULL, NULL, NULL, '27-1469313337-22', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(83, 17, 20, NULL, NULL, NULL, '27-1469313337-23', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(84, 17, 20, NULL, NULL, NULL, '27-1469313337-24', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(85, 17, 20, NULL, NULL, NULL, '27-1469313337-25', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(86, 17, 20, NULL, NULL, NULL, '27-1469313337-26', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(87, 17, 20, NULL, NULL, NULL, '27-1469313337-27', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(88, 17, 20, NULL, NULL, NULL, '27-1469313337-28', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(89, 17, 20, NULL, NULL, NULL, '27-1469313337-29', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(90, 17, 20, NULL, NULL, NULL, '27-1469313337-30', 'STOCK', 295.000, 295.000, NULL, NULL, NULL, '2016-07-23 22:35:38'),
(91, 18, 1, 1, NULL, 1, '25-1469352053-1', 'SOLD', 920.000, 1850.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(92, 18, 1, 1, NULL, 1, '25-1469352053-2', 'SOLD', 920.000, 1850.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(93, 18, 1, 1, NULL, 1, '25-1469352053-3', 'SOLD', 920.000, 1850.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(94, 18, 1, 1, NULL, 1, '25-1469352053-4', 'SOLD', 920.000, 1850.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(95, 18, 1, 1, NULL, 1, '25-1469352053-5', 'SOLD', 920.000, 1850.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(96, 18, 1, 1, NULL, NULL, '25-1469352053-6', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(97, 18, 1, 1, NULL, NULL, '25-1469352053-7', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(98, 18, 1, 1, NULL, NULL, '25-1469352053-8', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(99, 18, 1, 1, NULL, NULL, '25-1469352053-9', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(100, 18, 1, 1, NULL, NULL, '25-1469352053-10', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(101, 18, 1, 1, NULL, NULL, '25-1469352053-11', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(102, 18, 1, 1, NULL, NULL, '25-1469352053-12', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(103, 18, 1, 1, NULL, NULL, '25-1469352053-13', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(104, 18, 1, 1, NULL, NULL, '25-1469352053-14', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(105, 18, 1, 1, NULL, NULL, '25-1469352053-15', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(106, 18, 1, 1, NULL, NULL, '25-1469352053-16', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(107, 18, 1, 1, NULL, NULL, '25-1469352053-17', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(108, 18, 1, 1, NULL, NULL, '25-1469352053-18', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(109, 18, 1, 1, NULL, NULL, '25-1469352053-19', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(110, 18, 1, 1, NULL, NULL, '25-1469352053-20', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(111, 18, 1, 1, NULL, NULL, '25-1469352053-21', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(112, 18, 1, 1, NULL, NULL, '25-1469352053-22', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(113, 18, 1, 1, NULL, NULL, '25-1469352053-23', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(114, 18, 1, 1, NULL, NULL, '25-1469352053-24', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(115, 18, 1, 1, NULL, NULL, '25-1469352053-25', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(116, 18, 1, 1, NULL, NULL, '25-1469352053-26', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(117, 18, 1, 1, NULL, NULL, '25-1469352053-27', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(118, 18, 1, 1, NULL, NULL, '25-1469352053-28', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(119, 18, 1, 1, NULL, NULL, '25-1469352053-29', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(120, 18, 1, 1, NULL, NULL, '25-1469352053-30', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(121, 18, 1, 1, NULL, NULL, '25-1469352053-31', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(122, 18, 1, 1, NULL, NULL, '25-1469352053-32', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(123, 18, 1, 1, NULL, NULL, '25-1469352053-33', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(124, 18, 1, 1, NULL, NULL, '25-1469352053-34', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(125, 18, 1, 1, NULL, NULL, '25-1469352053-35', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(126, 18, 1, 1, NULL, NULL, '25-1469352053-36', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(127, 18, 1, 1, NULL, NULL, '25-1469352053-37', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(128, 18, 1, 1, NULL, NULL, '25-1469352053-38', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(129, 18, 1, 1, NULL, NULL, '25-1469352053-39', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(130, 18, 1, 1, NULL, NULL, '25-1469352053-40', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(131, 18, 1, 0, NULL, NULL, '25-1469352053-41', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(132, 18, 1, 0, NULL, NULL, '25-1469352053-42', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(133, 18, 1, 0, NULL, NULL, '25-1469352053-43', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(134, 18, 1, 0, NULL, NULL, '25-1469352053-44', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(135, 18, 1, 0, NULL, NULL, '25-1469352053-45', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(136, 18, 1, 0, NULL, NULL, '25-1469352053-46', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(137, 18, 1, 0, NULL, NULL, '25-1469352053-47', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(138, 18, 1, 0, NULL, NULL, '25-1469352053-48', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(139, 18, 1, 0, NULL, NULL, '25-1469352053-49', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(140, 18, 1, 0, NULL, NULL, '25-1469352053-50', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(141, 18, 1, 0, NULL, NULL, '25-1469352053-51', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(142, 18, 1, 0, NULL, NULL, '25-1469352053-52', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(143, 18, 1, 0, NULL, NULL, '25-1469352053-53', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(144, 18, 1, 0, NULL, NULL, '25-1469352053-54', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(145, 18, 1, 0, NULL, NULL, '25-1469352053-55', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(146, 18, 1, 0, NULL, NULL, '25-1469352053-56', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(147, 18, 1, 0, NULL, NULL, '25-1469352053-57', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(148, 18, 1, 0, NULL, NULL, '25-1469352053-58', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(149, 18, 1, 0, NULL, NULL, '25-1469352053-59', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(150, 18, 1, 0, NULL, NULL, '25-1469352053-60', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(151, 18, 1, 0, NULL, NULL, '25-1469352053-61', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(152, 18, 1, 0, NULL, NULL, '25-1469352053-62', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(153, 18, 1, 0, NULL, NULL, '25-1469352053-63', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(154, 18, 1, 0, NULL, NULL, '25-1469352053-64', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(155, 18, 1, 0, NULL, NULL, '25-1469352053-65', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(156, 18, 1, 0, NULL, NULL, '25-1469352053-66', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(157, 18, 1, 0, NULL, NULL, '25-1469352053-67', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(158, 18, 1, 0, NULL, NULL, '25-1469352053-68', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(159, 18, 1, 0, NULL, NULL, '25-1469352053-69', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(160, 18, 1, 0, NULL, NULL, '25-1469352053-70', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(161, 18, 1, 0, NULL, NULL, '25-1469352053-71', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(162, 18, 1, 0, NULL, NULL, '25-1469352053-72', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(163, 18, 1, 0, NULL, NULL, '25-1469352053-73', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(164, 18, 1, 0, NULL, NULL, '25-1469352053-74', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(165, 18, 1, 0, NULL, NULL, '25-1469352053-75', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(166, 18, 1, 0, NULL, NULL, '25-1469352053-76', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(167, 18, 1, 0, NULL, NULL, '25-1469352053-77', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(168, 18, 1, 0, NULL, NULL, '25-1469352053-78', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(169, 18, 1, 0, NULL, NULL, '25-1469352053-79', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(170, 18, 1, 0, NULL, NULL, '25-1469352053-80', 'STOCK', 920.000, 920.000, NULL, NULL, NULL, '2016-07-24 09:20:55'),
(171, 19, 1, 1, NULL, NULL, '21-1469352087-1', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(172, 19, 1, 1, NULL, NULL, '21-1469352087-2', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(173, 19, 1, 1, NULL, NULL, '21-1469352087-3', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(174, 19, 1, 1, NULL, NULL, '21-1469352087-4', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(175, 19, 1, 1, NULL, NULL, '21-1469352087-5', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(176, 19, 1, 1, NULL, NULL, '21-1469352087-6', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(177, 19, 1, 1, NULL, NULL, '21-1469352087-7', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(178, 19, 1, 1, NULL, NULL, '21-1469352087-8', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(179, 19, 1, 1, NULL, NULL, '21-1469352087-9', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(180, 19, 1, 1, NULL, NULL, '21-1469352087-10', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(181, 19, 1, 1, NULL, NULL, '21-1469352087-11', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(182, 19, 1, 1, NULL, NULL, '21-1469352087-12', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(183, 19, 1, 1, NULL, NULL, '21-1469352087-13', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(184, 19, 1, 1, NULL, NULL, '21-1469352087-14', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(185, 19, 1, 1, NULL, NULL, '21-1469352087-15', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(186, 19, 1, 1, NULL, NULL, '21-1469352087-16', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(187, 19, 1, 1, NULL, NULL, '21-1469352087-17', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(188, 19, 1, 1, NULL, NULL, '21-1469352087-18', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(189, 19, 1, 1, NULL, NULL, '21-1469352087-19', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(190, 19, 1, 1, NULL, NULL, '21-1469352087-20', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(191, 19, 1, 0, NULL, NULL, '21-1469352087-21', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(192, 19, 1, 0, NULL, NULL, '21-1469352087-22', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(193, 19, 1, 0, NULL, NULL, '21-1469352087-23', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(194, 19, 1, 0, NULL, NULL, '21-1469352087-24', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(195, 19, 1, 0, NULL, NULL, '21-1469352087-25', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(196, 19, 1, 0, NULL, NULL, '21-1469352087-26', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(197, 19, 1, 0, NULL, NULL, '21-1469352087-27', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(198, 19, 1, 0, NULL, NULL, '21-1469352087-28', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(199, 19, 1, 0, NULL, NULL, '21-1469352087-29', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(200, 19, 1, 0, NULL, NULL, '21-1469352087-30', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(201, 19, 1, 0, NULL, NULL, '21-1469352087-31', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(202, 19, 1, 0, NULL, NULL, '21-1469352087-32', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(203, 19, 1, 0, NULL, NULL, '21-1469352087-33', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(204, 19, 1, 0, NULL, NULL, '21-1469352087-34', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(205, 19, 1, 0, NULL, NULL, '21-1469352087-35', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(206, 19, 1, 0, NULL, NULL, '21-1469352087-36', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(207, 19, 1, 0, NULL, NULL, '21-1469352087-37', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(208, 19, 1, 0, NULL, NULL, '21-1469352087-38', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(209, 19, 1, 0, NULL, NULL, '21-1469352087-39', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(210, 19, 1, 0, NULL, NULL, '21-1469352087-40', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(211, 19, 1, 0, NULL, NULL, '21-1469352087-41', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(212, 19, 1, 0, NULL, NULL, '21-1469352087-42', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(213, 19, 1, 0, NULL, NULL, '21-1469352087-43', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(214, 19, 1, 0, NULL, NULL, '21-1469352087-44', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(215, 19, 1, 0, NULL, NULL, '21-1469352087-45', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(216, 19, 1, 0, NULL, NULL, '21-1469352087-46', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(217, 19, 1, 0, NULL, NULL, '21-1469352087-47', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(218, 19, 1, 0, NULL, NULL, '21-1469352087-48', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:27'),
(219, 19, 1, 0, NULL, NULL, '21-1469352087-49', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:28'),
(220, 19, 1, 0, NULL, NULL, '21-1469352087-50', 'STOCK', 3800.000, 3800.000, NULL, NULL, NULL, '2016-07-24 09:21:28'),
(221, 20, 1, 1, NULL, 2, '22-1469352116-1', 'SOLD', 3690.000, 4950.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(222, 20, 1, 1, NULL, 2, '22-1469352116-2', 'SOLD', 3690.000, 4950.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(223, 20, 1, 1, NULL, 2, '22-1469352116-3', 'SOLD', 3690.000, 4950.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(224, 20, 1, 1, NULL, 2, '22-1469352116-4', 'SOLD', 3690.000, 4950.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(225, 20, 1, 1, NULL, 2, '22-1469352116-5', 'SOLD', 3690.000, 4950.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(226, 20, 1, 1, NULL, 5, '22-1469352116-6', 'SOLD', 3690.000, 4950.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(227, 20, 1, 1, NULL, NULL, '22-1469352116-7', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(228, 20, 1, 1, NULL, NULL, '22-1469352116-8', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(229, 20, 1, 1, NULL, NULL, '22-1469352116-9', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(230, 20, 1, 1, NULL, NULL, '22-1469352116-10', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(231, 20, 1, 1, NULL, NULL, '22-1469352116-11', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(232, 20, 1, 1, NULL, NULL, '22-1469352116-12', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(233, 20, 1, 1, NULL, NULL, '22-1469352116-13', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(234, 20, 1, 1, NULL, NULL, '22-1469352116-14', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(235, 20, 1, 1, NULL, NULL, '22-1469352116-15', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(236, 20, 1, 1, NULL, NULL, '22-1469352116-16', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(237, 20, 1, 1, NULL, NULL, '22-1469352116-17', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(238, 20, 1, 1, NULL, NULL, '22-1469352116-18', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(239, 20, 1, 1, NULL, NULL, '22-1469352116-19', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(240, 20, 1, 1, NULL, NULL, '22-1469352116-20', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(241, 20, 1, 1, NULL, NULL, '22-1469352116-21', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(242, 20, 1, 1, NULL, NULL, '22-1469352116-22', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(243, 20, 1, 1, NULL, NULL, '22-1469352116-23', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(244, 20, 1, 1, NULL, NULL, '22-1469352116-24', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(245, 20, 1, 1, NULL, NULL, '22-1469352116-25', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(246, 20, 1, 1, NULL, NULL, '22-1469352116-26', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(247, 20, 1, 1, NULL, NULL, '22-1469352116-27', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(248, 20, 1, 1, NULL, NULL, '22-1469352116-28', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(249, 20, 1, 1, NULL, NULL, '22-1469352116-29', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(250, 20, 1, 1, NULL, NULL, '22-1469352116-30', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(251, 20, 1, 1, NULL, NULL, '22-1469352116-31', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(252, 20, 1, 1, NULL, NULL, '22-1469352116-32', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(253, 20, 1, 1, NULL, NULL, '22-1469352116-33', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(254, 20, 1, 1, NULL, NULL, '22-1469352116-34', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(255, 20, 1, 1, NULL, NULL, '22-1469352116-35', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(256, 20, 1, 1, NULL, NULL, '22-1469352116-36', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(257, 20, 1, 1, NULL, NULL, '22-1469352116-37', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(258, 20, 1, 1, NULL, NULL, '22-1469352116-38', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(259, 20, 1, 1, NULL, NULL, '22-1469352116-39', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(260, 20, 1, 1, NULL, NULL, '22-1469352116-40', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(261, 20, 1, 0, NULL, NULL, '22-1469352116-41', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(262, 20, 1, 0, NULL, NULL, '22-1469352116-42', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(263, 20, 1, 0, NULL, NULL, '22-1469352116-43', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(264, 20, 1, 0, NULL, NULL, '22-1469352116-44', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(265, 20, 1, 0, NULL, NULL, '22-1469352116-45', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(266, 20, 1, 0, NULL, NULL, '22-1469352116-46', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(267, 20, 1, 0, NULL, NULL, '22-1469352116-47', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(268, 20, 1, 0, NULL, NULL, '22-1469352116-48', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(269, 20, 1, 0, NULL, NULL, '22-1469352116-49', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(270, 20, 1, 0, NULL, NULL, '22-1469352116-50', 'STOCK', 3690.000, 3690.000, NULL, NULL, NULL, '2016-07-24 09:21:56'),
(271, 21, 5, 1, NULL, 7, '19-1469352156-1', 'SOLD', 2750.000, 5900.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(272, 21, 5, 1, NULL, 7, '19-1469352156-2', 'SOLD', 2750.000, 5900.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(273, 21, 5, 1, NULL, 11, '19-1469352156-3', 'SOLD', 2750.000, 5900.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(274, 21, 5, 1, NULL, NULL, '19-1469352156-4', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(275, 21, 5, 1, NULL, NULL, '19-1469352156-5', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(276, 21, 5, 1, NULL, NULL, '19-1469352156-6', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(277, 21, 5, 1, NULL, NULL, '19-1469352156-7', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(278, 21, 5, 1, NULL, NULL, '19-1469352156-8', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(279, 21, 5, 1, NULL, NULL, '19-1469352156-9', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(280, 21, 5, 1, NULL, NULL, '19-1469352156-10', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(281, 21, 5, 1, NULL, NULL, '19-1469352156-11', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(282, 21, 5, 1, NULL, NULL, '19-1469352157-12', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(283, 21, 5, 1, NULL, NULL, '19-1469352157-13', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(284, 21, 5, 1, NULL, NULL, '19-1469352157-14', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(285, 21, 5, 1, NULL, NULL, '19-1469352157-15', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(286, 21, 5, 1, NULL, NULL, '19-1469352157-16', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(287, 21, 5, 1, NULL, NULL, '19-1469352157-17', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(288, 21, 5, 1, NULL, NULL, '19-1469352157-18', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(289, 21, 5, 1, NULL, NULL, '19-1469352157-19', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(290, 21, 5, 1, NULL, NULL, '19-1469352157-20', 'STOCK', 2750.000, 2750.000, NULL, NULL, NULL, '2016-07-24 09:22:37'),
(291, 22, 20, 1, NULL, NULL, '27-1469354732-1', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(292, 22, 20, 1, NULL, NULL, '27-1469354732-2', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(293, 22, 20, 1, NULL, NULL, '27-1469354732-3', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(294, 22, 20, 1, NULL, NULL, '27-1469354732-4', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(295, 22, 20, 1, NULL, NULL, '27-1469354732-5', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(296, 22, 20, 1, NULL, NULL, '27-1469354732-6', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(297, 22, 20, 1, NULL, NULL, '27-1469354732-7', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(298, 22, 20, 1, NULL, NULL, '27-1469354732-8', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(299, 22, 20, 1, NULL, NULL, '27-1469354732-9', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(300, 22, 20, 1, NULL, NULL, '27-1469354732-10', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(301, 22, 20, 1, NULL, NULL, '27-1469354732-11', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(302, 22, 20, 1, NULL, NULL, '27-1469354732-12', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(303, 22, 20, 1, NULL, NULL, '27-1469354732-13', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(304, 22, 20, 1, NULL, NULL, '27-1469354732-14', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(305, 22, 20, 1, NULL, NULL, '27-1469354732-15', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(306, 22, 20, 1, NULL, NULL, '27-1469354732-16', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(307, 22, 20, 1, NULL, NULL, '27-1469354732-17', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(308, 22, 20, 1, NULL, NULL, '27-1469354732-18', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(309, 22, 20, 1, NULL, NULL, '27-1469354732-19', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(310, 22, 20, 0, NULL, NULL, '27-1469354732-20', 'STOCK', 395.000, 480.000, NULL, NULL, NULL, '2016-07-24 10:05:32'),
(311, 25, 4, NULL, NULL, NULL, '23-1469388888-10', 'INORDER', 4850.000, 5200.000, NULL, NULL, NULL, '2016-07-24 19:34:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_offer`
--

DROP TABLE IF EXISTS `sel_product_offer`;
CREATE TABLE IF NOT EXISTS `sel_product_offer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `TYPE` varchar(20) NOT NULL,
  `INITIAL_DATE` date NOT NULL,
  `END_DATE` date NOT NULL,
  `POSiTION` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_order`
--

DROP TABLE IF EXISTS `sel_product_order`;
CREATE TABLE IF NOT EXISTS `sel_product_order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `PRODUCT_ACQUISITION_ID` int(11) DEFAULT NULL,
  `STATUS` varchar(20) NOT NULL,
  `CODE` int(11) NOT NULL,
  `PRICE` float(9,3) NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `OBSERVATIONS` text,
  PRIMARY KEY (`ID`),
  KEY `ORDER_ID` (`ORDER_ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_sell`
--

DROP TABLE IF EXISTS `sel_product_sell`;
CREATE TABLE IF NOT EXISTS `sel_product_sell` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `SALE_ID` int(11) NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `PRICE` float(9,3) NOT NULL,
  `ADDED_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`),
  KEY `SALE_ID` (`SALE_ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 AVG_ROW_LENGTH=2048 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_product_sell`
--

INSERT INTO `sel_product_sell` (`ID`, `PRODUCT_ID`, `SALE_ID`, `QUANTITY`, `PRICE`, `ADDED_DATE`) VALUES
(1, 25, 49, 5, 1850.000, '2016-07-24 13:35:15'),
(2, 22, 49, 5, 4950.000, '2016-07-24 13:35:15'),
(3, 19, 49, 10, 5900.000, '2016-07-24 13:35:15'),
(4, 27, 49, 5, 480.000, '2016-07-24 13:35:15'),
(5, 22, 50, 1, 4950.000, '2016-07-24 13:51:15'),
(6, 27, 50, 1, 480.000, '2016-07-24 13:51:15'),
(7, 19, 51, 2, 5900.000, '2016-07-24 13:52:00'),
(8, 27, 52, 1, 480.000, '2016-07-24 14:10:22'),
(9, 24, 52, 1, 5450.000, '2016-07-24 14:10:22'),
(10, 23, 53, 2, 5200.000, '2016-07-25 00:03:56'),
(11, 19, 53, 1, 5900.000, '2016-07-25 00:03:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_provider`
--

DROP TABLE IF EXISTS `sel_provider`;
CREATE TABLE IF NOT EXISTS `sel_provider` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERSON_ID` int(11) DEFAULT NULL,
  `COMPANY` varchar(100) NOT NULL,
  `ADDRESS` varchar(200) NOT NULL,
  `PERSON` varchar(100) NOT NULL,
  `PHONE` varchar(20) DEFAULT NULL,
  `CELLPHONE` varchar(20) DEFAULT NULL,
  `OBSERVATIONS` text,
  PRIMARY KEY (`ID`),
  KEY `PERSON_ID` (`PERSON_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 AVG_ROW_LENGTH=2730 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_provider`
--

INSERT INTO `sel_provider` (`ID`, `PERSON_ID`, `COMPANY`, `ADDRESS`, `PERSON`, `PHONE`, `CELLPHONE`, `OBSERVATIONS`) VALUES
(14, NULL, 'P&C Importaciones', 'Av. Sargento Flores #982', 'Juan Magan', '72459019', NULL, NULL),
(15, NULL, 'Atlantis Ltda.', 'Av. Zofro Km 6 s/n ', 'Martin Calle', '71126767', '71152255', 'DE MARTIN'),
(16, NULL, 'Magnus Electronics', 'Av. 6 de Octubre #293', 'Jesusa Benavente Diaz', '52-89291', NULL, NULL),
(17, NULL, 'Herbas Liendo Importaciones', 'Av. Tacno #902 esq. Murguia', 'Katy Herbas Liendo', '', NULL, NULL),
(18, NULL, 'Import Export Oruro', 'C. Soria Galvarro esq. Murguia #590', 'Ismael Cruz Barrios', '52-53610', NULL, NULL),
(19, NULL, 'Pascual Guerrero', 'calle Oblitas #982 Zona Norte', 'Pascual Guerrero Choque', '52-43902', NULL, NULL),
(20, NULL, 'GeneSis', 'calle Backovick #297', 'Martin Argandoña V.', '52-87595', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_sale`
--

DROP TABLE IF EXISTS `sel_sale`;
CREATE TABLE IF NOT EXISTS `sel_sale` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BRANCH_ID` int(11) NOT NULL,
  `EMPLOYEE_ID` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) NOT NULL,
  `DATE` datetime DEFAULT NULL,
  `STATUS` varchar(20) DEFAULT NULL,
  `CODE` varchar(20) NOT NULL,
  `MONEY_CHANGE` int(11) DEFAULT NULL,
  `OBSERVATION` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CUSTOMER_ID` (`CUSTOMER_ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`),
  KEY `EMPLOYEE_ID` (`EMPLOYEE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=54 AVG_ROW_LENGTH=2048 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sel_sale`
--

INSERT INTO `sel_sale` (`ID`, `BRANCH_ID`, `EMPLOYEE_ID`, `CUSTOMER_ID`, `DATE`, `STATUS`, `CODE`, `MONEY_CHANGE`, `OBSERVATION`) VALUES
(49, 1, 5, 2, '2016-07-11 21:33:10', 'OPEN', '1', NULL, NULL),
(50, 1, 5, 1, '2016-07-13 09:50:13', 'OPEN', '2', NULL, NULL),
(51, 1, 5, 3, '2016-07-14 09:51:29', 'OPEN', '3', NULL, NULL),
(52, 1, 5, 5, '2016-07-15 10:09:47', 'OPEN', '4', NULL, NULL),
(53, 1, 5, 6, '2016-07-20 19:45:23', 'OPEN', '5', NULL, NULL);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cho_rol_x_uri`
--
ALTER TABLE `cho_rol_x_uri`
  ADD CONSTRAINT `cho_rol_x_uri_fk1` FOREIGN KEY (`ROL_ID`) REFERENCES `cho_rol` (`ID`),
  ADD CONSTRAINT `cho_rol_x_uri_fk2` FOREIGN KEY (`URI_ID`) REFERENCES `cho_uri` (`ID`);

--
-- Filtros para la tabla `cho_uri`
--
ALTER TABLE `cho_uri`
  ADD CONSTRAINT `cho_uri_fk1` FOREIGN KEY (`MODULE_ID`) REFERENCES `cho_module` (`ID`);

--
-- Filtros para la tabla `cho_user_x_rol`
--
ALTER TABLE `cho_user_x_rol`
  ADD CONSTRAINT `cho_user_x_rol_fk1` FOREIGN KEY (`USER_ID`) REFERENCES `cho_user` (`ID`),
  ADD CONSTRAINT `cho_user_x_rol_fk2` FOREIGN KEY (`ROL_ID`) REFERENCES `cho_rol` (`ID`);

--
-- Filtros para la tabla `sel_acquisition`
--
ALTER TABLE `sel_acquisition`
  ADD CONSTRAINT `acquisition_product_fk1` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `sel_provider` (`ID`);

--
-- Filtros para la tabla `sel_branch_category`
--
ALTER TABLE `sel_branch_category`
  ADD CONSTRAINT `sel_branch_category_fk1` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`),
  ADD CONSTRAINT `sel_branch_category_fk2` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `sel_category` (`ID`);

--
-- Filtros para la tabla `sel_branch_employee`
--
ALTER TABLE `sel_branch_employee`
  ADD CONSTRAINT `sel_branch_employee_fk1` FOREIGN KEY (`EMPLOYEE_ID`) REFERENCES `sel_employee` (`ID`),
  ADD CONSTRAINT `sel_employee_branch` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`);

--
-- Filtros para la tabla `sel_category`
--
ALTER TABLE `sel_category`
  ADD CONSTRAINT `sel_category_fk1` FOREIGN KEY (`MAIN_CATEGORY_ID`) REFERENCES `sel_category` (`ID`);

--
-- Filtros para la tabla `sel_customer`
--
ALTER TABLE `sel_customer`
  ADD CONSTRAINT `sel_customer_fk1` FOREIGN KEY (`CUSTOMER_ACCOUNT_ID`) REFERENCES `sel_customer_account` (`ID`);

--
-- Filtros para la tabla `sel_employee`
--
ALTER TABLE `sel_employee`
  ADD CONSTRAINT `sel_employee_fk1` FOREIGN KEY (`USER_ID`) REFERENCES `cho_user` (`ID`);

--
-- Filtros para la tabla `sel_order`
--
ALTER TABLE `sel_order`
  ADD CONSTRAINT `sel_order_fk1` FOREIGN KEY (`SALES_ID`) REFERENCES `sel_sale` (`ID`),
  ADD CONSTRAINT `sel_order_fk2` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sel_customer` (`ID`);

--
-- Filtros para la tabla `sel_product`
--
ALTER TABLE `sel_product`
  ADD CONSTRAINT `sel_product_fk1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `sel_category` (`ID`),
  ADD CONSTRAINT `sel_product_fk2` FOREIGN KEY (`PRODUCT_GROUP_ID`) REFERENCES `sel_product_group` (`ID`),
  ADD CONSTRAINT `sel_product_fk3` FOREIGN KEY (`BRAND_ID`) REFERENCES `sel_brand` (`ID`),
  ADD CONSTRAINT `sel_product_fk4` FOREIGN KEY (`MAIN_IMAGE_ID`) REFERENCES `sel_product_image` (`ID`);

--
-- Filtros para la tabla `sel_product_acquisition`
--
ALTER TABLE `sel_product_acquisition`
  ADD CONSTRAINT `sel_aquisition_product_fk1` FOREIGN KEY (`AQUISITION_ID`) REFERENCES `sel_acquisition` (`ID`),
  ADD CONSTRAINT `sel_product_aquisition_fk1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sel_product` (`ID`);

--
-- Filtros para la tabla `sel_product_group`
--
ALTER TABLE `sel_product_group`
  ADD CONSTRAINT `sel_product_group_fk1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `sel_category` (`ID`);

--
-- Filtros para la tabla `sel_product_image`
--
ALTER TABLE `sel_product_image`
  ADD CONSTRAINT `sel_product_image_fk1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sel_product` (`ID`);

--
-- Filtros para la tabla `sel_product_item`
--
ALTER TABLE `sel_product_item`
  ADD CONSTRAINT `sel_product_item_fk1` FOREIGN KEY (`PRODUCT_ACQUISITION_ID`) REFERENCES `sel_product_acquisition` (`ID`),
  ADD CONSTRAINT `sel_product_item_fk2` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`),
  ADD CONSTRAINT `sel_product_item_fk3` FOREIGN KEY (`BRAND_ID`) REFERENCES `sel_brand` (`ID`),
  ADD CONSTRAINT `sel_product_item_fk4` FOREIGN KEY (`PRODUCT_SELL_ID`) REFERENCES `sel_product_sell` (`ID`);

--
-- Filtros para la tabla `sel_product_offer`
--
ALTER TABLE `sel_product_offer`
  ADD CONSTRAINT `product_offer_fk1` FOREIGN KEY (`ID`) REFERENCES `sel_product` (`ID`);

--
-- Filtros para la tabla `sel_product_order`
--
ALTER TABLE `sel_product_order`
  ADD CONSTRAINT `sel_product_order_fk1` FOREIGN KEY (`ORDER_ID`) REFERENCES `sel_order` (`ID`),
  ADD CONSTRAINT `sel_product_order_fk2` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sel_product` (`ID`);

--
-- Filtros para la tabla `sel_product_sell`
--
ALTER TABLE `sel_product_sell`
  ADD CONSTRAINT `fk_sel_product_sale_product` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sel_product` (`ID`),
  ADD CONSTRAINT `sel_product_sale_sale` FOREIGN KEY (`SALE_ID`) REFERENCES `sel_sale` (`ID`);

--
-- Filtros para la tabla `sel_provider`
--
ALTER TABLE `sel_provider`
  ADD CONSTRAINT `sel_provider_fk1` FOREIGN KEY (`PERSON_ID`) REFERENCES `sel_person` (`ID`);

--
-- Filtros para la tabla `sel_sale`
--
ALTER TABLE `sel_sale`
  ADD CONSTRAINT `sel_sale_fk1` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sel_customer` (`ID`),
  ADD CONSTRAINT `sel_sale_fk2` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`),
  ADD CONSTRAINT `sel_sale_fk3` FOREIGN KEY (`EMPLOYEE_ID`) REFERENCES `sel_employee` (`ID`);
