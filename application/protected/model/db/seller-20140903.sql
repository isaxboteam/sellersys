-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 03-09-2014 a las 15:14:13
-- Versión del servidor: 5.5.16
-- Versión de PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `seller`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acquisition_product`
--

CREATE TABLE IF NOT EXISTS `acquisition_product` (
  `ID` int(11) NOT NULL,
  `DATE` int(11) DEFAULT NULL,
  `PROVIDER_ID` int(11) DEFAULT NULL,
  `OBSERVATION` text,
  PRIMARY KEY (`ID`),
  KEY `PROVIDER_ID` (`PROVIDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_module`
--

CREATE TABLE IF NOT EXISTS `cho_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `URI` varchar(30) NOT NULL,
  `NAME` varchar(30) NOT NULL,
  `ACCESS` varchar(20) NOT NULL,
  `POSITION` int(11) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_MODULE_NAME` (`NAME`),
  UNIQUE KEY `NAME` (`NAME`),
  UNIQUE KEY `URI` (`URI`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `cho_module`
--

INSERT INTO `cho_module` (`ID`, `URI`, `NAME`, `ACCESS`, `POSITION`, `DESCRIPTION`) VALUES
(1, '', 'Portal', 'PUBLIC', 1, 'Portal público'),
(2, 'admin', 'Administración', 'PRIVATE', 2, 'Administración del Sistema'),
(3, 'parametros', 'Parámetros', 'PRIVATE', 3, 'Parámetros generales del sistema'),
(4, 'clientes', 'Clientes', 'PRIVATE', 4, 'Módulo de clientes'),
(5, 'administracionCliente', 'Mi Centro infantil', 'PRIVATE', 5, 'Administración del Centro Infantil'),
(6, 'organizacion', 'Organización', 'PRIVATE', 6, 'Modulo de organización de Centro Infantil');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_rol`
--

CREATE TABLE IF NOT EXISTS `cho_rol` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(20) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_CODE` (`CODE`),
  UNIQUE KEY `CODE` (`CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cho_rol`
--

INSERT INTO `cho_rol` (`ID`, `CODE`, `NAME`, `DESCRIPTION`) VALUES
(1, 'SUPER', 'Superusuario', ''),
(2, 'YPRA', 'YES', 'hyhhkhjk'),
(3, 'ADM', 'Admin', 'Administrador'),
(4, 'USR', 'User', 'No description'),
(5, 'ADM-CEN', 'Administrador de Centro', 'Administrador de Centro Infantil');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_rol_x_uri`
--

CREATE TABLE IF NOT EXISTS `cho_rol_x_uri` (
  `ROL_ID` int(11) NOT NULL,
  `URI_ID` int(11) NOT NULL,
  `READ` varchar(3) NOT NULL DEFAULT 'SI',
  `CREATE` varchar(3) NOT NULL DEFAULT 'NO',
  `UPDATE` varchar(3) NOT NULL DEFAULT 'NO',
  `DELETE` varchar(3) NOT NULL DEFAULT 'NO',
  PRIMARY KEY (`ROL_ID`,`URI_ID`),
  KEY `ROL_ID` (`ROL_ID`),
  KEY `URI_ID` (`URI_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cho_rol_x_uri`
--

INSERT INTO `cho_rol_x_uri` (`ROL_ID`, `URI_ID`, `READ`, `CREATE`, `UPDATE`, `DELETE`) VALUES
(1, 4, 'SI', 'SI', 'SI', 'SI'),
(1, 9, 'SI', 'SI', 'SI', 'SI'),
(1, 11, 'SI', 'SI', 'SI', 'SI'),
(1, 12, 'SI', 'SI', 'SI', 'SI'),
(1, 13, 'SI', 'SI', 'SI', 'SI'),
(1, 14, 'SI', 'SI', 'SI', 'SI'),
(1, 15, 'SI', 'SI', 'SI', 'SI'),
(1, 16, 'SI', 'SI', 'SI', 'SI'),
(1, 17, 'SI', 'SI', 'SI', 'SI'),
(2, 4, 'SI', 'NO', 'NO', 'NO'),
(3, 4, 'SI', 'SI', 'SI', 'NO'),
(3, 9, 'SI', 'SI', 'SI', 'SI'),
(3, 11, 'SI', 'SI', 'SI', 'SI'),
(3, 12, 'SI', 'SI', 'SI', 'SI'),
(3, 13, 'SI', 'SI', 'SI', 'SI'),
(3, 14, 'SI', 'SI', 'SI', 'SI'),
(3, 15, 'SI', 'SI', 'SI', 'SI'),
(3, 16, 'SI', 'SI', 'SI', 'SI'),
(3, 17, 'SI', 'SI', 'SI', 'SI'),
(5, 19, 'SI', 'SI', 'SI', 'SI'),
(5, 20, 'SI', 'SI', 'SI', 'SI'),
(5, 21, 'SI', 'SI', 'SI', 'SI'),
(5, 22, 'SI', 'SI', 'SI', 'SI'),
(5, 23, 'SI', 'SI', 'SI', 'SI'),
(5, 24, 'SI', 'SI', 'SI', 'SI'),
(5, 25, 'SI', 'SI', 'SI', 'SI'),
(5, 26, 'SI', 'SI', 'SI', 'SI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_uri`
--

CREATE TABLE IF NOT EXISTS `cho_uri` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` int(11) NOT NULL,
  `URI` varchar(200) NOT NULL,
  `TITLE` varchar(50) NOT NULL,
  `ACCESS` varchar(20) NOT NULL,
  `TYPE` varchar(20) NOT NULL,
  `POSITION` int(11) NOT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_URI` (`URI`),
  KEY `MODULE_ID` (`MODULE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2340 AUTO_INCREMENT=27 ;

--
-- Volcado de datos para la tabla `cho_uri`
--

INSERT INTO `cho_uri` (`ID`, `MODULE_ID`, `URI`, `TITLE`, `ACCESS`, `TYPE`, `POSITION`, `DESCRIPTION`) VALUES
(1, 1, 'index/', 'Inicio', 'PUBLIC', 'MENU', 1, 'Página Principal'),
(2, 1, 'index/index/', 'Segundo Inicio', 'PUBLIC', 'EXTRA', 2, 'Segunda Página'),
(3, 1, 'azeta/zeteandoMasmelos', 'MasMelos', 'PUBLIC', 'EXTRA', 3, 'Masmeleada'),
(4, 2, 'rol/', 'Roles', 'PRIVATE', 'MENU', 5, ''),
(9, 2, 'uri/', 'URIs', 'PRIVATE', 'SECTION', 5, ''),
(11, 2, 'module/', 'Módulos', 'PRIVATE', 'MENU', 5, ''),
(12, 2, 'user/', 'Usuarios', 'PRIVATE', 'MENU', 5, ''),
(13, 3, 'activityType/', 'Tipos de Actividades', 'PRIVATE', 'MENU', 2, ''),
(14, 3, 'teacherType/', 'Tipos de Educadores', 'PRIVATE', 'MENU', 2, ''),
(15, 3, 'trainingArea/', 'Areas de Formación', 'PRIVATE', 'MENU', 4, ''),
(16, 3, 'kinship/', 'Parentezcos', 'PRIVATE', 'MENU', 4, ''),
(17, 4, 'nurserySchoolAdmin/', 'Centros Infantiles', 'PRIVATE', 'MENU', 3, ''),
(18, 4, 'nurseryLocationAdmin/', 'Ubicaciones de Centros Infanti', 'PRIVATE', 'SECTION', 3, ''),
(19, 5, 'nurseryAdmin/', 'Centro Infantil', 'PRIVATE', 'MENU', 1, ''),
(20, 6, 'degree/', 'Grados', 'PRIVATE', 'MENU', 7, ''),
(21, 6, 'classroom', 'Aula', 'PRIVATE', 'SECTION', 7, ''),
(22, 6, 'preschoolTeacher/', 'Maestros', 'PRIVATE', 'MENU', 7, ''),
(23, 6, 'child/', 'Niños', 'PRIVATE', 'SECTION', 7, ''),
(24, 6, 'kindred/', 'Parientes', 'PRIVATE', 'SECTION', 7, ''),
(25, 6, 'nurseryLocation/', 'Instalaciones', 'PRIVATE', 'SECTION', 8, ''),
(26, 6, 'nurserySubsidiary/', 'Subsidiarias', 'PRIVATE', 'MENU', 7, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user`
--

CREATE TABLE IF NOT EXISTS `cho_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROL_ID` int(11) NOT NULL,
  `FIRST_LASTNAME` varchar(30) NOT NULL,
  `SECOND_LASTNAME` varchar(30) DEFAULT NULL,
  `FIRST_NAME` varchar(30) NOT NULL,
  `SECOND_NAME` varchar(30) DEFAULT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `EMAIL` varchar(80) NOT NULL,
  `DNI` varchar(20) DEFAULT NULL,
  `GENDER` varchar(10) DEFAULT NULL,
  `BIRTHDAY` date DEFAULT NULL,
  `TYPE` varchar(20) DEFAULT NULL,
  `STATUS` varchar(20) NOT NULL,
  `COUNTRY` varchar(30) DEFAULT NULL,
  `STATE` varchar(30) DEFAULT NULL,
  `CITY` varchar(50) DEFAULT NULL,
  `ADDRESS` text,
  `ZIP` varchar(10) DEFAULT NULL,
  `PRIMARY_PHONE` varchar(20) DEFAULT NULL,
  `SECONDARY_PHONE` varchar(20) DEFAULT NULL,
  `CELL_PHONE` varchar(20) DEFAULT NULL,
  `PHOTO_FORMAT` varchar(15) DEFAULT NULL,
  `REGISTERED_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_ACCESS` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=455 AUTO_INCREMENT=56 ;

--
-- Volcado de datos para la tabla `cho_user`
--

INSERT INTO `cho_user` (`ID`, `ROL_ID`, `FIRST_LASTNAME`, `SECOND_LASTNAME`, `FIRST_NAME`, `SECOND_NAME`, `USERNAME`, `PASSWORD`, `EMAIL`, `DNI`, `GENDER`, `BIRTHDAY`, `TYPE`, `STATUS`, `COUNTRY`, `STATE`, `CITY`, `ADDRESS`, `ZIP`, `PRIMARY_PHONE`, `SECONDARY_PHONE`, `CELL_PHONE`, `PHOTO_FORMAT`, `REGISTERED_DATE`, `LAST_ACCESS`) VALUES
(1, 1, 'Rodriguez', 'Aranda', 'Yecid', '', 'yecid', 'yecid123', 'yecid@serviciosycompras.com', '1234567', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPAÑA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 04:00:00', '2014-05-09 03:36:33'),
(2, 1, 'Ticona', 'Menchaca', 'Clever', '', 'clever', 'clever123', 'clever@serviciosycompras.com', '6787889', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPAÑA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 04:00:00', '2014-05-09 03:36:33'),
(3, 1, 'Santana', 'Diaz', 'Abel', '', 'abel', 'abel123', 'abel@serviciosycompras.com', '5789543', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPAÑA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 04:00:00', '2014-05-09 03:36:33'),
(4, 1, 'Colomina', '', 'Juan', '', 'juan', 'juan123', 'juan@serviciosycompras.com', '6879654', NULL, NULL, 'INTERNO', 'ACTIVE', 'ESPAÑA', 'Las Palmas', 'Las Palmas de Gran Canaria', NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-01 04:00:00', '2014-05-09 03:36:33'),
(55, 1, 'Perez', 'Perez', 'Juan', 'Maria', 'juanperez', 'juanperez', 'juan.perez@gmail.com', '123456', 'MALE', '2019-01-01', 'EXTERNAL', 'CREATED', NULL, NULL, NULL, 'Calle Las Retamas #921', NULL, '2810927', NULL, '77719202', NULL, '2014-08-24 15:27:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user_admin`
--

CREATE TABLE IF NOT EXISTS `cho_user_admin` (
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=4096;

--
-- Volcado de datos para la tabla `cho_user_admin`
--

INSERT INTO `cho_user_admin` (`ID`) VALUES
(1),
(2),
(3),
(4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cho_user_x_rol`
--

CREATE TABLE IF NOT EXISTS `cho_user_x_rol` (
  `USER_ID` int(11) NOT NULL,
  `ROL_ID` int(11) NOT NULL,
  PRIMARY KEY (`USER_ID`,`ROL_ID`),
  KEY `ROL_ID` (`ROL_ID`),
  KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cho_user_x_rol`
--

INSERT INTO `cho_user_x_rol` (`USER_ID`, `ROL_ID`) VALUES
(1, 1),
(1, 2),
(1, 3),
(55, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_branch`
--

CREATE TABLE IF NOT EXISTS `sel_branch` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `ADDRESS` varchar(100) DEFAULT NULL,
  `TELEPHONE` int(20) DEFAULT NULL,
  `FAX` int(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_category`
--

CREATE TABLE IF NOT EXISTS `sel_category` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_customer`
--

CREATE TABLE IF NOT EXISTS `sel_customer` (
  `ID` int(11) NOT NULL,
  `PERSON_ID` int(11) DEFAULT NULL,
  `STATE` varchar(20) DEFAULT NULL,
  `CODE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PERSON_ID` (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_order_detail`
--

CREATE TABLE IF NOT EXISTS `sel_order_detail` (
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_order_product`
--

CREATE TABLE IF NOT EXISTS `sel_order_product` (
  `ID` int(11) NOT NULL,
  `DATE` date DEFAULT NULL,
  `STATE` varchar(20) DEFAULT NULL,
  `ORDER_DETAIL` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_person`
--

CREATE TABLE IF NOT EXISTS `sel_person` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `FIRST_LASTNAME` varchar(20) DEFAULT NULL,
  `SECOND_LASTNAME` varchar(20) DEFAULT NULL,
  `IDENTITY` int(11) DEFAULT NULL,
  `IDENTITY_EXT` varchar(20) DEFAULT NULL,
  `GENDER` varchar(20) DEFAULT NULL,
  `BIRTHDAY_DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product`
--

CREATE TABLE IF NOT EXISTS `sel_product` (
  `ID` int(11) NOT NULL,
  `CODE` varchar(20) DEFAULT NULL,
  `NAME` varchar(20) DEFAULT NULL,
  `DESCRIPTION` text,
  `BRAND` varchar(100) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `MODEL` varchar(100) DEFAULT NULL,
  `COLOR` varchar(50) DEFAULT NULL,
  `NRO_PARTS` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_detail_sell`
--

CREATE TABLE IF NOT EXISTS `sel_product_detail_sell` (
  `ID` int(11) NOT NULL,
  `PRODUCT_SELL_ID` int(11) DEFAULT NULL,
  `AMOUNT` int(11) DEFAULT NULL,
  `PRODUCTO_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PRODUCT_SELL_ID` (`PRODUCT_SELL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_item`
--

CREATE TABLE IF NOT EXISTS `sel_product_item` (
  `ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) DEFAULT NULL,
  `WARRANTY_SALES` int(11) DEFAULT NULL,
  `PRODUCT_COST_SALES` double(15,3) DEFAULT NULL,
  `PRODUCT_COST_ACQUISITION` int(11) DEFAULT NULL,
  `WARRANTY_PROVIDER` int(11) DEFAULT NULL,
  `MONEY_KIND` varchar(20) DEFAULT NULL,
  `ACQUISITION_PRODUCT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ACQUISITION_PRODUCT_ID` (`ACQUISITION_PRODUCT_ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_item_x_branch`
--

CREATE TABLE IF NOT EXISTS `sel_product_item_x_branch` (
  `PRODUCT_ITEM_ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  PRIMARY KEY (`PRODUCT_ITEM_ID`,`BRANCH_ID`),
  KEY `PRODUCT_ITEM_ID` (`PRODUCT_ITEM_ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_product_sell`
--

CREATE TABLE IF NOT EXISTS `sel_product_sell` (
  `ID` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) DEFAULT NULL,
  `DATE` date DEFAULT NULL,
  `BRANCH_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BRANCH_ID` (`BRANCH_ID`),
  KEY `CUSTOMER_ID` (`CUSTOMER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sel_provider`
--

CREATE TABLE IF NOT EXISTS `sel_provider` (
  `ID` int(11) NOT NULL,
  `COMPANY` varchar(100) DEFAULT NULL,
  `ADDRESS` varchar(100) DEFAULT NULL,
  `PERSON_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PERSON_ID` (`PERSON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acquisition_product`
--
ALTER TABLE `acquisition_product`
  ADD CONSTRAINT `acquisition_product_fk1` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `sel_provider` (`ID`);

--
-- Filtros para la tabla `cho_rol_x_uri`
--
ALTER TABLE `cho_rol_x_uri`
  ADD CONSTRAINT `cho_rol_x_uri_fk1` FOREIGN KEY (`ROL_ID`) REFERENCES `cho_rol` (`ID`),
  ADD CONSTRAINT `cho_rol_x_uri_fk2` FOREIGN KEY (`URI_ID`) REFERENCES `cho_uri` (`ID`);

--
-- Filtros para la tabla `cho_uri`
--
ALTER TABLE `cho_uri`
  ADD CONSTRAINT `FK_URI_MODULE` FOREIGN KEY (`MODULE_ID`) REFERENCES `cho_module` (`ID`);

--
-- Filtros para la tabla `cho_user_admin`
--
ALTER TABLE `cho_user_admin`
  ADD CONSTRAINT `cho_user_adimn_fk` FOREIGN KEY (`ID`) REFERENCES `cho_user` (`ID`);

--
-- Filtros para la tabla `cho_user_x_rol`
--
ALTER TABLE `cho_user_x_rol`
  ADD CONSTRAINT `cho_user_x_rol_fk1` FOREIGN KEY (`USER_ID`) REFERENCES `cho_user` (`ID`),
  ADD CONSTRAINT `cho_user_x_rol_fk2` FOREIGN KEY (`ROL_ID`) REFERENCES `cho_rol` (`ID`);

--
-- Filtros para la tabla `sel_customer`
--
ALTER TABLE `sel_customer`
  ADD CONSTRAINT `sel_customer_fk1` FOREIGN KEY (`PERSON_ID`) REFERENCES `sel_person` (`ID`);

--
-- Filtros para la tabla `sel_product`
--
ALTER TABLE `sel_product`
  ADD CONSTRAINT `sel_product_fk1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `sel_category` (`ID`);

--
-- Filtros para la tabla `sel_product_detail_sell`
--
ALTER TABLE `sel_product_detail_sell`
  ADD CONSTRAINT `sel_product_detail_sell_fk1` FOREIGN KEY (`PRODUCT_SELL_ID`) REFERENCES `sel_product_sell` (`ID`);

--
-- Filtros para la tabla `sel_product_item`
--
ALTER TABLE `sel_product_item`
  ADD CONSTRAINT `sel_product_item_fk2` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sel_product` (`ID`),
  ADD CONSTRAINT `sel_product_item_fk1` FOREIGN KEY (`ACQUISITION_PRODUCT_ID`) REFERENCES `acquisition_product` (`ID`);

--
-- Filtros para la tabla `sel_product_item_x_branch`
--
ALTER TABLE `sel_product_item_x_branch`
  ADD CONSTRAINT `sel_product_item_x_branch_fk2` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`),
  ADD CONSTRAINT `sel_product_item_x_branch_fk1` FOREIGN KEY (`PRODUCT_ITEM_ID`) REFERENCES `sel_product_item` (`ID`);

--
-- Filtros para la tabla `sel_product_sell`
--
ALTER TABLE `sel_product_sell`
  ADD CONSTRAINT `sel_product_sell_fk2` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sel_customer` (`ID`),
  ADD CONSTRAINT `sel_product_sell_fk1` FOREIGN KEY (`BRANCH_ID`) REFERENCES `sel_branch` (`ID`);

--
-- Filtros para la tabla `sel_provider`
--
ALTER TABLE `sel_provider`
  ADD CONSTRAINT `sel_provider_fk1` FOREIGN KEY (`PERSON_ID`) REFERENCES `sel_person` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
