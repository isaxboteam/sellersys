<?php

/**
 * Created by PhpStorm.
 * User: Yecid
 * Date: 7/17/2016
 * Time: 3:36 p.m.
 */
class SessionOrder
{

    /**
     * @var SessionOrder
     */
    private static $instance = null;

    /**
     * @var SelCustomer
     */
    private $customer = null;

    /**
     * @var SelOrder
     */
    private $order = null;

    /**
     * @var array
     */
    private $products = array();

    /**+
     * @param $order
     * @return SessionOrder
     */
    public static function instance($order)
    {
        self::$instance = Session::_('order');
        if(!is_object(static::$instance)){
            static::$instance = new SessionOrder();
        }
        return static::$instance;
    }

    /**
     * @param SelCustomer|null $customer
     */
    static function setCustomer(SelCustomer $customer = null){
        self::instance()->customer = $customer;
        Session::set('order', self::instance());
    }

    /**
     * @param SelOrder $order
     */
    static function setOrder(SelOrder $order){
        self::instance()->order = $order;
        Session::set('order', self::instance());
    }

    /**
     * @param SelProduct $product
     * @param $quantity
     */
    static function addProduct(SelProduct $product, $quantity){
        $productCart = array(
            'product'   => $product ,
            'price'     => $product->getBasePrice(),
            'quantity'  => $quantity
        );
        self::instance()->products[$product->getId()] = $productCart;
        Session::set('order', self::instance());
    }

    /**
     * @param SelProduct $product
     * @param $quantity
     */
    static function removeProduct(SelProduct $product, $quantity){
        unset(self::instance()->products[$product->getId()]);
        Session::set('order', self::instance());
    }

    /**
     *
     */
    static function resetProducts(){
        self::instance()->products = array();
        Session::set('order', self::instance());
    }

    /**
     * @return SelCustomer
     */
    static function customer(){
        return self::instance()->customer;
    }

    static function hasCustomer(){
        return is_object(self::instance()->customer());
    }

    /**
     * @return SelOrder
     */
    static function order(){
        return self::instance()->order;
    }

    /**
     * @return array
     */
    static function products(){
        return self::instance()->products;
    }

    /**
     * @param SelProduct $product
     * @return array
     */
    static function productInCart(SelProduct $product){
        return self::instance()->products[$product->getId()];
    }

    /**
     * @return bool
     */
    static function hasProducts(){
        return sizeof(self::instance()->products)>0;
    }

    /**
     * @param SelProduct $product
     * @return bool
     */
    static function hasProduct(SelProduct $product){
        return isset(self::instance()->products[$product->getId()]);
    }

        /**
     * @return int
     */
    static function totalItems(){
        $totalItems = 0;
        foreach (self::instance()->products as $productCart) {
            $totalItems+= $productCart['quantity'];
        }
        return $totalItems;
    }

    /**
     * @return float
     */
    static function totalPrice(){
        $totalPrice = 0.0;
        foreach (self::instance()->products as $productCart) {
            $totalPrice+= $productCart['price'] * $productCart['quantity'];
        }
        return $totalPrice;
    }

}