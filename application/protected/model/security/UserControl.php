<?php
/**
 * UserControlClass (Singleton Registered)
 * SINGLETON Pattern (SINGLETON REFACTORIZED)
 *
 * @author ypra
 */
class UserControl implements ISingleton
{

    /**
     *
     * @var ChoUser
     */
    private static $choUser = null;

    /**
     *
     * @var array
     */
    private static $choRols = null;

    /**
     *
     * @return ChoUser
     */
    public static function user()
    {
        return self::$choUser;
    }

    /**
     *
     * @return array
     */
    public static function rols()
    {
        return self::$choRols;
    }

    /**
     * 
     * @return UserControl
     */
    public static function instance()
    {
        return SecurityRegistry::instance()->userControl();
    }

    /**
     * 
     * @return string
     */
    public static function sessionVar()
    {
        return Configs::value('app.code').'_USER';
    }

    public function __construct()
    {
        $sessionVar = self::sessionVar();
        $sessionUser = Session::has($sessionVar)?
                unserialize(Session::get($sessionVar)): null;
        if(is_object($sessionUser)){
            self::$choUser = $sessionUser;
            self::$choUser->reload();
            self::$choRols = ChoRolQuery::create()
                    ->useChoUserXRolQuery()
                        ->filterByChoUser(self::$choUser)
                    ->endUse()
                ->find();
            define("ID_USUARIO", self::$choUser->getId());
        }
    }

    /**
     * Crypt a string by a encryption method
     * @param string $string
     * @return string
     */
    public static function crypt($string)
    {
        /* TODO implements a encryption method*/
        $hash = $string;
        return $hash;
    }

    /**
     * Decrypt a string by a reverse encryption method
     * @param string $hash
     * @return string 
     */
    public static function decrypt($hash)
    {
        /* TODO implements a decryption method*/
        $string = $hash;
        return $string;
    }

    /**
     * Authenticate a user by login and password for signed in
     * @param string $username
     * @param string $password
     * @return boolean
     */
    public static function login($username, $password)
    {
        $choUser = ChoUserQuery::create()->findOneByUsername($username);
        if(is_object($choUser)){
            //$role = new Role($choUser->getRolId());
            if($choUser->getPassword() == self::crypt($password)){
                //&& $role->isActive()){
                Session::set(self::sessionVar(), serialize($choUser));
                SecurityRegistry::instance()->updateRegistry('user', new UserControl());
                return true;
            }
        }
        return false;
    }

    /**
     * Signed out a user
     * @return boolean
     */
    public static function logout()
    {
        $sessionVar = self::sessionVar();
        if(Session::has($sessionVar)){
            Session::delete($sessionVar);
            return true;
        }
        return false;
    }

    /**
     * Verify the login of a user of the system
     * @return bool
     */
    public static function loginVerify()
    {
        if(is_object(self::$choUser)){
            return true;
        }elseif(isset($_POST['username']) && isset($_POST['password'])
            && $_POST['username']!=""){
            self::login($_POST['username'], $_POST['password']);
            header('Location: '.$_SERVER['HTTP_REFERER']);
            exit();
        }elseif($_REQUEST['logout']!=""){
            self::logout();
            header('Location: '.WEB_ROOT);
            exit();
        }else{
            if(realpath($_SERVER['SCRIPT_FILENAME']) != PUBLIC_DIR."index.php"){
                header('Location: '.WEB_ROOT);
                return false;
            }
        }
    }

    /**
     * 
     * @return boolean
     */
    public static function isLoggedIn()
    {
        return is_object(UserControl::user());
    }

    /**
     * @param $rolCode
     * @return bool
     */
    public static function hasRol($rolCode)
    {
        return sizeof(array_filter(self::$choRols->getArrayCopy(), function($obj) use($rolCode){
            return $obj->getCode() == $rolCode;
        }))>0;
    }

}