<?php
require_once('Page.php');
require_once('PageConfigs.php');
require_once('UserControl.php');
//Chocala::import('Model.framework.page.Module');
//Chocala::import('Model.framework.page.Page');
//Chocala::import('Model.framework.page.Functionality');
//Chocala::import('Model.framework.page.PageConfigs');
/**
 * PageControl class (Singleton Registered)
 * SINGLETON Pattern (SINGLETON REFACTORIZED)
 *
 * @author ypra
 */
class PageControl implements ISingleton
{

    /**
     *
     * @var array
     */
    private static $modules = array();

    /**
     *
     * @var Page
     */
    private static $page = null;

    /**
     *
     * @var array
     */
    private static $functionalities = array();

    /**
     *
     * @var string
     */
    private static $backURI = '';

    /**
     *
     * @var bool
     */
    private static $canAdd = false;

    /**
     *
     * @var bool
     */
    private static $canBack = false;

    /**
     *
     * @var bool
     */
    private static $canFilter = false;

    /**
     *
     * @var bool
     */
    private static $canPaginate = false;

    /**
     *
     * @var bool
     */
    private static $canRefresh = false;

    /**
     *
     * @return Page
     */
    public static function page()
    {
        return self::$page;
    }

    /**
     *
     * @return string
     */
    public static function backURI()
    {
        return self::$backURI;
    }

    /**
     *
     * @param string $backURI
     * @return void
     */
    public static function setBackURI($backURI)
    {
        self::$backURI = $backURI;
    }

    /**
     *
     * @return bool
     */
    public static function isRegistered()
    {
        return !is_null(self::$page->URI());
    }

    /**
     *
     * @return bool
     */
    public static function canAdd()
    {
        return (self::$canAdd && self::$page->autCreate());
    }

    /**
     *
     * @return void
     */
    public static function noAdd()
    {
        self::$canAdd = false;
    }

    /**
     *
     * @return bool
     */
    public static function canBack()
    {
        return (self::$canBack && !empty(self::$backURI));
    }

    /**
     *
     * @return void
     */
    public static function noBack()
    {
        self::$canBack = false;
    }

    /**
     *
     * @return bool
     */
    public static function canFilter()
    {
        return self::$canFilter;
    }

    /**
     *
     * @return void
     */
    public static function noFilter()
    {
        self::$canFilter = false;
    }

    /**
     *
     * @return bool
     */
    public static function canPaginate()
    {
        return self::$canPaginate;
    }

    /**
     *
     * @return void
     */
    public static function noPaginate()
    {
        self::$canPaginate = false;
    }

    /**
     *
     * @return bool
     */
    public static function canRefresh()
    {
        return self::$canRefresh;
    }

    /**
     *
     * @return void
     */
    public static function noRefresh()
    {
        self::$canRefresh = false;
    }

    /**
     *
     * @return PageControl
     */
    public static function instance()
    {
        return SecurityRegistry::instance()->pageControl();
    }

    public function __construct()
    {        
        $uris = URI::subsequentURIs();
        self::$page = Page::createFrom($uris, UserControl::user());        
        /*
        self::$functionalities = Functionality::functionalitiesList();
        $__functionalities = self::$page->functionalities();
        if(isset($__functionalities[1])
            && $__functionalities[1]->isActive()){
                self::$canAdd = true;
        }
        if(isset($__functionalities[2])
            && $__functionalities[2]->isActive()){
            self::$canRefresh = true;
        }
        if(isset($__functionalities[5])
            && $__functionalities[5]->isActive()){
            self::$canBack = true;
        }
         */
    }

    /**
     * 
     * @param ChoUser $user
     * @param boolean $all
     * @return array
     */
    public static function userPages(ChoUser $user, $onlyMenu = false)
    {
        $rols = $user->inOrderRols();
        $modulesHash = array();
        $uris = ChoUriQuery::create()
                ->distinct()
                ->_if($onlyMenu)
                    ->filterByType(PageConfigs::TYPE_MENU)
                ->_endif()
                ->useChoRolXUriQuery()
                    ->filterByChoRol($rols, Criteria::IN)
                ->endUse()
                ->useChoModuleQuery()
                    ->orderByPosition()
                ->endUse()
                ->orderByPosition()
            ->find();
        foreach ($uris as $uri){
            $module = $uri->getChoModule();
            if(!isset($modulesHash[$module->getId()])){
                $modulesHash[$module->getId()]['module'] = $module;
                $modulesHash[$module->getId()]['uris'] = array();
            }
            array_push($modulesHash[$module->getId()]['uris'], $uri);
        }
        return $modulesHash;        
    }

    /**
     * Return a determinate main module
     * @param string $section
     * @return array
     */
    public static function section($section="all")
    {
        if($section=="all"){
            return self::$modules;
        }else{
            return self::$modules[$section];
        }
    }

    /**
     * Returns all modules that exist in the system.
     * @return array
     */
    public static function allModules()
    {
        $choModules = ChoModuleQuery::create()->orderByPosition()->find();
        $choUris = ChoUriQuery::create()->orderByPosition()->find();
        $uris = $choUris->getData();
        $modules = array();
        foreach ($choModules as $choModule){
            $modules[$choModule->getUri()] = array('module' => $choModule,
                'uris' => array_filter($uris, function($obj) use ($choModule){
                        return $obj->getChoModule()->equals($choModule);
                    })
                );
        }
        return $modules;
    }

    /**
     * Verify
     * @return bool
     */
    public static function hasAccess()
    {
        if(self::page()->getAccess() == PageConfigs::ACCESS_PUBLIC){
            return true;
        }elseif(UserControl::loginVerify()){
            return true;
        }else{
            return false;
        }
    }

    /**
     *
     * @return bool
     */
    public static function canCreate()
    {
        return self::$page->autCreate();
    }

    /**
     *
     * @return bool
     */
    public static function canUpdate()
    {
        return self::$page->autUpdate();
    }

    /**
     *
     * @return bool
     */
    public static function canDelete()
    {
        return self::$page->autDelete();
    }

    /**
     *
     * @return bool
     */
    public static function canRead()
    {
        return self::$page->autRead();
    }

    /**
     * @return void
     */
    public static function noFunctionalityBar()
    {
        self::$canAdd = false;
        self::$canBack = false;
        self::$canFilter = false;
        self::$canPaginate = false;
        self::$canRefresh = false;
    }


    /**
     *
     * @return User
     */
    public static function enabledToUser()
    {
        //TODO implements the  preview class charge
        PageControl::instance();
        $user = null;
        if(UserControl::loginVerify()){
            $user = UserControl::user();
            if(is_object($user)){
                if(PageControl::instance()->enabledPage()){
                    PageControl::instance();
                }
            }
        }
        if(ControlPagina::instancia()->accedePagina()){
            ControlPagina::instancia()->inicializarPagina();
        }else{
            ControlPagina::instancia()->setLibreria(new Libreria());
            $vistaRestingida= new Vista(LAYOUT_PRIVADO);
            $vistaRestingida->set("user", UserControl::user());
            $vistaRestingida->display("accesoRestringido");
            exit();
        }
        return $user;
    }

}