<?php
/**
 * Description of UserStatusEnum
 *
 * @author ypra
 */
abstract class UserStatusEnum
{

    /** User with actived account */
    const ACTIVED = 'ACTIVED';

    /** User with blocked account */
    const BLOCKED = 'BLOQUED';

    /** User with closed account */
    const CLOSED = 'CLOSED';

    /** User with created account */
    const CREATED = 'CREATED';

    const ES_ACTIVED = 'ACTIVO';
    const ES_BLOCKED = 'BLOQUEADO';
    const ES_CLOSED = 'CERRADO';
    const ES_CREATED = 'CREADO';

    /**
     * Return a user status complete list
     * @return array
     */
    public static function enum($lang=null)
    {
        switch($lang){
            case 'ES':
                $userStatus = array(
                    UserStatusEnum::ACTIVED => UserStatusEnum::ES_ACTIVED,
                    UserStatusEnum::BLOCKED => UserStatusEnum::ES_BLOCKED,
                    UserStatusEnum::CLOSED => UserStatusEnum::ES_CLOSED,
                    UserStatusEnum::CREATED => UserStatusEnum::ES_CREATED);
                break;
            default:
                $userStatus = array(
                    UserStatusEnum::ACTIVED => UserStatusEnum::ACTIVED,
                    UserStatusEnum::BLOCKED => UserStatusEnum::BLOCKED,
                    UserStatusEnum::CLOSED => UserStatusEnum::CLOSED,
                    UserStatusEnum::CREATED => UserStatusEnum::CREATED);
                break;
        }
        return $userStatus;
    }

    /**
     * Return the name title for the language as $lang parameter
     * @param string $status
     * @param string $lang
     * @return string
     */
    public static function statusFrom($status, $lang=null)
    {
        $statuses = self::enum($lang);
        return $statuses[$status];
    }

}