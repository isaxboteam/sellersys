<?php
/**
 * Description of PageConfigs
 *
 * @author ypra
 */
class PageConfigs
{
    
    const ACCESS_PRIVATE = "PRIVATE";

    const ACCESS_PROTECTED = "PROTECTED";

    const ACCESS_PUBLIC = "PUBLIC";

    const ACCESS_PRIVATE_ES = "PRIVADO";

    const ACCESS_PROTECTED_ES = "PROTEGIDO";

    const ACCESS_PUBLIC_ES = "PUBLICO";

    const NO_PAGE = 'noAction';

    const NO_PAGE_TITLE = 'Direccion no encontrada';

    const TYPE_MENU = "MENU";

    const TYPE_SECTION = "SECTION";

    public static function enumAccess($lang = '')
    {
        switch(strtolower($lang)){
            case 'es':
                $pageAccess = array(
                    PageConfigs::ACCESS_PRIVATE => PageConfigs::ACCESS_PRIVATE_ES,
                    PageConfigs::ACCESS_PROTECTED => PageConfigs::ACCESS_PROTECTED_ES,
                    PageConfigs::ACCESS_PUBLIC => PageConfigs::ACCESS_PUBLIC_ES);
                break;
            default:
                $pageAccess = array(
                    PageConfigs::ACCESS_PRIVATE => PageConfigs::ACCESS_PRIVATE,
                    PageConfigs::ACCESS_PROTECTED => PageConfigs::ACCESS_PROTECTED,
                    PageConfigs::ACCESS_PUBLIC => PageConfigs::ACCESS_PUBLIC);
                break;
        }
        return $pageAccess;
    }

    public static function label($value, $lang = '')
    {
        $map = self::enumAccess($lang);
        return isset($map[$value])? $map[$value]: $value;
    }

    public static function enumTypes()
    {
        $pageTypes = array(
            PageConfigs::TYPE_MENU => PageConfigs::TYPE_MENU,
            PageConfigs::TYPE_SECTION => PageConfigs::TYPE_SECTION);
        return $pageTypes;
    }

}