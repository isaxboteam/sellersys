<?php
/**
 * Description of UserStatusEnum
 *
 * @author ypra
 */
abstract class UserTypesEnum
{

    /** User with access only to external privileges */
    const EXTERNAL = 'EXTERNAL';

    /** User with access to internal privileges */
    const INTERNAL = 'INTERNAL';

    const ES_EXTERNAL = 'EXTERNO';
    const ES_INTERNAL= 'INTERNO';

    /**
     * Returns a user types complete list
     * @return array
     */
    public static function enum()
    {
        /* TODO implements a get by language translation */
        switch($lang){
            case 'ES':
                $userTypes = array(
                    UserTypesEnum::EXTERNAL => UserTypesEnum::ES_EXTERNAL,
                    UserTypesEnum::INTERNAL => UserTypesEnum::ES_INTERNAL
                );
                break;
            default:
                $userTypes = array(
                    UserTypesEnum::EXTERNAL => UserTypesEnum::EXTERNAL,
                    UserTypesEnum::INTERNAL => UserTypesEnum::INTERNAL
                );
                break;
        }
        return $userTypes;
    }

    /**
     * Return the name title for the language as $lang parameter
     * @param string $status
     * @param string $lang
     * @return string
     */
    public static function statusFrom($status, $lang=null)
    {
        $statuses = self::enum($lang);
        return $statuses[$status];
    }

}