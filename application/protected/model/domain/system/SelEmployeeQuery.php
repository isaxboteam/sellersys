<?php
/**
 * Skeleton subclass for performing query and update operations on the 'sel_employee' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class SelEmployeeQuery extends BaseSelEmployeeQuery
{

    public static function createInDate($date)
    {
        return self::create()
            ->filterByInitDate($date, Criteria::LESS_EQUAL)
            ->filterByEndDate($date, Criteria::GREATER_THAN);
    }

    public static function createInDateEmployee($date, $employee)
    {
        return self::createInDate($date)->filterBySelEmployee($employee);
    }

    public static function createInDateBranch($date, $branch)
    {
        return self::createInDate($date)->filterBySelBranch($branch);
    }

}