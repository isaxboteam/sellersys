<?php



/**
 * This class defines the structure of the 'sel_product_item' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelProductItemTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelProductItemTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_product_item');
        $this->setPhpName('SelProductItem');
        $this->setClassname('SelProductItem');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('PRODUCT_ACQUISITION_ID', 'ProductAcquisitionId', 'INTEGER', 'sel_product_acquisition', 'ID', true, null, null);
        $this->addForeignKey('BRAND_ID', 'BrandId', 'INTEGER', 'sel_brand', 'ID', true, null, null);
        $this->addForeignKey('BRANCH_ID', 'BranchId', 'INTEGER', 'sel_branch', 'ID', false, null, null);
        $this->addColumn('PRODUCT_DEPOSIT_ID', 'ProductDepositId', 'INTEGER', false, null, null);
        $this->addForeignKey('PRODUCT_SELL_ID', 'ProductSellId', 'INTEGER', 'sel_product_sell', 'ID', false, null, null);
        $this->addColumn('CODE', 'Code', 'VARCHAR', true, 20, null);
        $this->addColumn('STATUS', 'Status', 'VARCHAR', true, 15, null);
        $this->addColumn('COST', 'Cost', 'FLOAT', true, 9, null);
        $this->addColumn('PRICE', 'Price', 'FLOAT', false, 9, null);
        $this->addColumn('COLOR', 'Color', 'VARCHAR', false, 30, null);
        $this->addColumn('WARRANTY_SELL', 'WarrantySell', 'INTEGER', false, null, null);
        $this->addColumn('OBSERVATION', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('REGISTER_DATE', 'RegisterDate', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelProductAcquisition', 'SelProductAcquisition', RelationMap::MANY_TO_ONE, array('PRODUCT_ACQUISITION_ID' => 'ID', ), null, null);
        $this->addRelation('SelBranch', 'SelBranch', RelationMap::MANY_TO_ONE, array('BRANCH_ID' => 'ID', ), null, null);
        $this->addRelation('SelBrand', 'SelBrand', RelationMap::MANY_TO_ONE, array('BRAND_ID' => 'ID', ), null, null);
        $this->addRelation('SelProductSell', 'SelProductSell', RelationMap::MANY_TO_ONE, array('PRODUCT_SELL_ID' => 'ID', ), null, null);
    } // buildRelations()

} // SelProductItemTableMap
