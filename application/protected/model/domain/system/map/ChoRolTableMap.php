<?php



/**
 * This class defines the structure of the 'cho_rol' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class ChoRolTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.ChoRolTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cho_rol');
        $this->setPhpName('ChoRol');
        $this->setClassname('ChoRol');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('CODE', 'Code', 'VARCHAR', true, 20, null);
        $this->addColumn('NAME', 'Name', 'VARCHAR', true, 50, null);
        $this->addColumn('DESCRIPTION', 'Description', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ChoRolXUri', 'ChoRolXUri', RelationMap::ONE_TO_MANY, array('ID' => 'ROL_ID', ), null, null, 'ChoRolXUris');
        $this->addRelation('ChoUserXRol', 'ChoUserXRol', RelationMap::ONE_TO_MANY, array('ID' => 'ROL_ID', ), null, null, 'ChoUserXRols');
    } // buildRelations()

} // ChoRolTableMap
