<?php



/**
 * This class defines the structure of the 'sel_provider' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelProviderTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelProviderTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_provider');
        $this->setPhpName('SelProvider');
        $this->setClassname('SelProvider');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('PERSON_ID', 'PersonId', 'INTEGER', 'sel_person', 'ID', false, null, null);
        $this->addColumn('COMPANY', 'Company', 'VARCHAR', true, 100, null);
        $this->addColumn('ADDRESS', 'Address', 'VARCHAR', true, 200, null);
        $this->addColumn('PERSON', 'Person', 'VARCHAR', true, 100, null);
        $this->addColumn('PHONE', 'Phone', 'VARCHAR', false, 20, null);
        $this->addColumn('CELLPHONE', 'Cellphone', 'VARCHAR', false, 20, null);
        $this->addColumn('OBSERVATIONS', 'Observations', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelPerson', 'SelPerson', RelationMap::MANY_TO_ONE, array('PERSON_ID' => 'ID', ), null, null);
        $this->addRelation('SelAcquisition', 'SelAcquisition', RelationMap::ONE_TO_MANY, array('ID' => 'PROVIDER_ID', ), null, null, 'SelAcquisitions');
    } // buildRelations()

} // SelProviderTableMap
