<?php



/**
 * This class defines the structure of the 'sel_order' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelOrderTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelOrderTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_order');
        $this->setPhpName('SelOrder');
        $this->setClassname('SelOrder');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('CUSTOMER_ID', 'CustomerId', 'INTEGER', 'sel_customer', 'ID', true, null, null);
        $this->addForeignKey('SALES_ID', 'SalesId', 'INTEGER', 'sel_sale', 'ID', false, null, null);
        $this->addColumn('STATUS', 'Status', 'VARCHAR', true, 20, null);
        $this->addColumn('SHIPPING_ADDRESS', 'ShippingAddress', 'LONGVARCHAR', true, null, null);
        $this->addColumn('PHONE', 'Phone', 'VARCHAR', true, 30, null);
        $this->addColumn('CUSTOMMER_COMMENT', 'CustommerComment', 'LONGVARCHAR', true, null, null);
        $this->addColumn('SALES_COMMENT', 'SalesComment', 'INTEGER', false, null, null);
        $this->addColumn('CREATION_DATE', 'CreationDate', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('ORDER_DATE', 'OrderDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('PAYMENT_DATE', 'PaymentDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('PAYMENT_CODE', 'PaymentCode', 'VARCHAR', false, 30, null);
        $this->addColumn('PAYMENT_ENTITY', 'PaymentEntity', 'VARCHAR', false, 200, null);
        $this->addColumn('CHECK_DATE', 'CheckDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('APPROVED_DATE', 'ApprovedDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('SHIPPING_DATE', 'ShippingDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('DELIVERY_DATE', 'DeliveryDate', 'TIMESTAMP', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelSale', 'SelSale', RelationMap::MANY_TO_ONE, array('SALES_ID' => 'ID', ), null, null);
        $this->addRelation('SelCustomer', 'SelCustomer', RelationMap::MANY_TO_ONE, array('CUSTOMER_ID' => 'ID', ), null, null);
        $this->addRelation('SelProductOrder', 'SelProductOrder', RelationMap::ONE_TO_MANY, array('ID' => 'ORDER_ID', ), null, null, 'SelProductOrders');
    } // buildRelations()

} // SelOrderTableMap
