<?php



/**
 * This class defines the structure of the 'cho_user' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class ChoUserTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.ChoUserTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cho_user');
        $this->setPhpName('ChoUser');
        $this->setClassname('ChoUser');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('ROL_ID', 'RolId', 'INTEGER', true, null, null);
        $this->addColumn('FIRST_LASTNAME', 'FirstLastname', 'VARCHAR', true, 30, null);
        $this->addColumn('SECOND_LASTNAME', 'SecondLastname', 'VARCHAR', false, 30, null);
        $this->addColumn('FIRST_NAME', 'FirstName', 'VARCHAR', true, 30, null);
        $this->addColumn('SECOND_NAME', 'SecondName', 'VARCHAR', false, 30, null);
        $this->addColumn('USERNAME', 'Username', 'VARCHAR', true, 50, null);
        $this->addColumn('PASSWORD', 'Password', 'VARCHAR', true, 50, null);
        $this->addColumn('EMAIL', 'Email', 'VARCHAR', true, 80, null);
        $this->addColumn('DNI', 'Dni', 'VARCHAR', false, 20, null);
        $this->addColumn('GENDER', 'Gender', 'VARCHAR', false, 10, null);
        $this->addColumn('BIRTHDAY', 'Birthday', 'DATE', false, null, null);
        $this->addColumn('TYPE', 'Type', 'VARCHAR', false, 20, null);
        $this->addColumn('STATUS', 'Status', 'VARCHAR', true, 20, null);
        $this->addColumn('COUNTRY', 'Country', 'VARCHAR', false, 30, null);
        $this->addColumn('STATE', 'State', 'VARCHAR', false, 30, null);
        $this->addColumn('CITY', 'City', 'VARCHAR', false, 50, null);
        $this->addColumn('ADDRESS', 'Address', 'LONGVARCHAR', false, null, null);
        $this->addColumn('ZIP', 'Zip', 'VARCHAR', false, 10, null);
        $this->addColumn('PRIMARY_PHONE', 'PrimaryPhone', 'VARCHAR', false, 20, null);
        $this->addColumn('SECONDARY_PHONE', 'SecondaryPhone', 'VARCHAR', false, 20, null);
        $this->addColumn('CELL_PHONE', 'CellPhone', 'VARCHAR', false, 20, null);
        $this->addColumn('PHOTO_FORMAT', 'PhotoFormat', 'VARCHAR', false, 15, null);
        $this->addColumn('REGISTERED_DATE', 'RegisteredDate', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('LAST_ACCESS', 'LastAccess', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ChoUserXRol', 'ChoUserXRol', RelationMap::ONE_TO_MANY, array('ID' => 'USER_ID', ), null, null, 'ChoUserXRols');
        $this->addRelation('SelEmployee', 'SelEmployee', RelationMap::ONE_TO_MANY, array('ID' => 'USER_ID', ), null, null, 'SelEmployees');
    } // buildRelations()

} // ChoUserTableMap
