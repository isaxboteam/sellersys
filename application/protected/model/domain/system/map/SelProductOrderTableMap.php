<?php



/**
 * This class defines the structure of the 'sel_product_order' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelProductOrderTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelProductOrderTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_product_order');
        $this->setPhpName('SelProductOrder');
        $this->setClassname('SelProductOrder');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('ORDER_ID', 'OrderId', 'INTEGER', 'sel_order', 'ID', true, null, null);
        $this->addForeignKey('PRODUCT_ID', 'ProductId', 'INTEGER', 'sel_product', 'ID', true, null, null);
        $this->addColumn('PRODUCT_ACQUISITION_ID', 'ProductAcquisitionId', 'INTEGER', false, null, null);
        $this->addColumn('STATUS', 'Status', 'VARCHAR', true, 20, null);
        $this->addColumn('CODE', 'Code', 'INTEGER', true, null, null);
        $this->addColumn('PRICE', 'Price', 'FLOAT', true, 9, null);
        $this->addColumn('QUANTITY', 'Quantity', 'INTEGER', true, null, null);
        $this->addColumn('OBSERVATIONS', 'Observations', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelOrder', 'SelOrder', RelationMap::MANY_TO_ONE, array('ORDER_ID' => 'ID', ), null, null);
        $this->addRelation('SelProduct', 'SelProduct', RelationMap::MANY_TO_ONE, array('PRODUCT_ID' => 'ID', ), null, null);
    } // buildRelations()

} // SelProductOrderTableMap
