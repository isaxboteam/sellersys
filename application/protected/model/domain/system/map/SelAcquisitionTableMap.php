<?php



/**
 * This class defines the structure of the 'sel_acquisition' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelAcquisitionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelAcquisitionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_acquisition');
        $this->setPhpName('SelAcquisition');
        $this->setClassname('SelAcquisition');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('DATE', 'Date', 'DATE', false, null, null);
        $this->addForeignKey('PROVIDER_ID', 'ProviderId', 'INTEGER', 'sel_provider', 'ID', false, null, null);
        $this->addColumn('OBSERVATION', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('WARRANTY_PROVIDER', 'WarrantyProvider', 'VARCHAR', false, 30, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelProvider', 'SelProvider', RelationMap::MANY_TO_ONE, array('PROVIDER_ID' => 'ID', ), null, null);
        $this->addRelation('SelProductAcquisition', 'SelProductAcquisition', RelationMap::ONE_TO_MANY, array('ID' => 'AQUISITION_ID', ), null, null, 'SelProductAcquisitions');
    } // buildRelations()

} // SelAcquisitionTableMap
