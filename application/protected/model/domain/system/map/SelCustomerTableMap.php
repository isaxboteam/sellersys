<?php



/**
 * This class defines the structure of the 'sel_customer' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelCustomerTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelCustomerTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_customer');
        $this->setPhpName('SelCustomer');
        $this->setClassname('SelCustomer');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('CUSTOMER_ACCOUNT_ID', 'CustomerAccountId', 'INTEGER', 'sel_customer_account', 'ID', false, null, null);
        $this->addColumn('ID_NUMBER', 'IdNumber', 'VARCHAR', true, 30, null);
        $this->addColumn('NAME', 'Name', 'VARCHAR', true, 100, null);
        $this->addColumn('LASTNAME', 'Lastname', 'VARCHAR', true, 100, null);
        $this->addColumn('EMAIL', 'Email', 'VARCHAR', true, 150, null);
        $this->addColumn('BIRTHDAY', 'Birthday', 'DATE', false, null, null);
        $this->addColumn('ADDRESS', 'Address', 'VARCHAR', false, 500, null);
        $this->addColumn('PHONE', 'Phone', 'VARCHAR', false, 30, null);
        $this->addColumn('CELLPHONE', 'Cellphone', 'VARCHAR', false, 30, null);
        $this->addColumn('STATUS', 'Status', 'VARCHAR', false, 20, null);
        $this->addColumn('CODE', 'Code', 'VARCHAR', false, 20, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelCustomerAccount', 'SelCustomerAccount', RelationMap::MANY_TO_ONE, array('CUSTOMER_ACCOUNT_ID' => 'ID', ), null, null);
        $this->addRelation('SelOrder', 'SelOrder', RelationMap::ONE_TO_MANY, array('ID' => 'CUSTOMER_ID', ), null, null, 'SelOrders');
        $this->addRelation('SelSale', 'SelSale', RelationMap::ONE_TO_MANY, array('ID' => 'CUSTOMER_ID', ), null, null, 'SelSales');
    } // buildRelations()

} // SelCustomerTableMap
