<?php



/**
 * This class defines the structure of the 'sel_category' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelCategoryTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelCategoryTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_category');
        $this->setPhpName('SelCategory');
        $this->setClassname('SelCategory');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('MAIN_CATEGORY_ID', 'MainCategoryId', 'INTEGER', 'sel_category', 'ID', false, null, null);
        $this->addColumn('CODE', 'Code', 'VARCHAR', true, 20, null);
        $this->addColumn('NAME', 'Name', 'VARCHAR', true, 100, null);
        $this->addColumn('DESCRIPTION', 'Description', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelCategoryRelatedByMainCategoryId', 'SelCategory', RelationMap::MANY_TO_ONE, array('MAIN_CATEGORY_ID' => 'ID', ), null, null);
        $this->addRelation('SelBranchCategory', 'SelBranchCategory', RelationMap::ONE_TO_MANY, array('ID' => 'CATEGORY_ID', ), null, null, 'SelBranchCategorys');
        $this->addRelation('SelCategoryRelatedById', 'SelCategory', RelationMap::ONE_TO_MANY, array('ID' => 'MAIN_CATEGORY_ID', ), null, null, 'SelCategorysRelatedById');
        $this->addRelation('SelProduct', 'SelProduct', RelationMap::ONE_TO_MANY, array('ID' => 'CATEGORY_ID', ), null, null, 'SelProducts');
        $this->addRelation('SelProductGroup', 'SelProductGroup', RelationMap::ONE_TO_MANY, array('ID' => 'CATEGORY_ID', ), null, null, 'SelProductGroups');
    } // buildRelations()

} // SelCategoryTableMap
