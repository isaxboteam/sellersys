<?php



/**
 * This class defines the structure of the 'cho_user_x_rol' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class ChoUserXRolTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.ChoUserXRolTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cho_user_x_rol');
        $this->setPhpName('ChoUserXRol');
        $this->setClassname('ChoUserXRol');
        $this->setPackage('system');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('USER_ID', 'UserId', 'INTEGER' , 'cho_user', 'ID', true, null, null);
        $this->addForeignPrimaryKey('ROL_ID', 'RolId', 'INTEGER' , 'cho_rol', 'ID', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ChoUser', 'ChoUser', RelationMap::MANY_TO_ONE, array('USER_ID' => 'ID', ), null, null);
        $this->addRelation('ChoRol', 'ChoRol', RelationMap::MANY_TO_ONE, array('ROL_ID' => 'ID', ), null, null);
    } // buildRelations()

} // ChoUserXRolTableMap
