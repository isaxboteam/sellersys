<?php



/**
 * This class defines the structure of the 'sel_product_image' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelProductImageTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelProductImageTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_product_image');
        $this->setPhpName('SelProductImage');
        $this->setClassname('SelProductImage');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('PRODUCT_ID', 'ProductId', 'INTEGER', 'sel_product', 'ID', true, null, null);
        $this->addColumn('NAME', 'Name', 'VARCHAR', true, 100, null);
        $this->addColumn('SIZE', 'Size', 'INTEGER', true, null, null);
        $this->addColumn('MIMETYPE', 'Mimetype', 'VARCHAR', false, 30, null);
        $this->addColumn('POSITION', 'Position', 'INTEGER', true, null, null);
        $this->addColumn('TITLE', 'Title', 'VARCHAR', true, 100, null);
        $this->addColumn('DESCRIPTION', 'Description', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelProductRelatedByProductId', 'SelProduct', RelationMap::MANY_TO_ONE, array('PRODUCT_ID' => 'ID', ), null, null);
        $this->addRelation('SelProductRelatedByMainImageId', 'SelProduct', RelationMap::ONE_TO_MANY, array('ID' => 'MAIN_IMAGE_ID', ), null, null, 'SelProductsRelatedByMainImageId');
    } // buildRelations()

} // SelProductImageTableMap
