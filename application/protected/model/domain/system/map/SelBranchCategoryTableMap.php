<?php



/**
 * This class defines the structure of the 'sel_branch_category' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelBranchCategoryTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelBranchCategoryTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_branch_category');
        $this->setPhpName('SelBranchCategory');
        $this->setClassname('SelBranchCategory');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('BRANCH_ID', 'BranchId', 'INTEGER', 'sel_branch', 'ID', true, null, null);
        $this->addForeignKey('CATEGORY_ID', 'CategoryId', 'INTEGER', 'sel_category', 'ID', true, null, null);
        $this->addColumn('STATE', 'State', 'VARCHAR', true, 15, null);
        $this->addColumn('OBSERVATION', 'Observation', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelBranch', 'SelBranch', RelationMap::MANY_TO_ONE, array('BRANCH_ID' => 'ID', ), null, null);
        $this->addRelation('SelCategory', 'SelCategory', RelationMap::MANY_TO_ONE, array('CATEGORY_ID' => 'ID', ), null, null);
    } // buildRelations()

} // SelBranchCategoryTableMap
