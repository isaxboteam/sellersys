<?php



/**
 * This class defines the structure of the 'sel_branch' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelBranchTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelBranchTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_branch');
        $this->setPhpName('SelBranch');
        $this->setClassname('SelBranch');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('NUMBER', 'Number', 'INTEGER', true, null, 0);
        $this->addColumn('STATUS', 'Status', 'VARCHAR', true, 15, 'OPEN');
        $this->addColumn('CODE', 'Code', 'VARCHAR', true, 20, null);
        $this->addColumn('NAME', 'Name', 'VARCHAR', true, 20, null);
        $this->addColumn('ADDRESS', 'Address', 'VARCHAR', true, 100, null);
        $this->addColumn('PHONE', 'Phone', 'VARCHAR', false, 20, null);
        $this->addColumn('FAX', 'Fax', 'VARCHAR', false, 20, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelBranchCategory', 'SelBranchCategory', RelationMap::ONE_TO_MANY, array('ID' => 'BRANCH_ID', ), null, null, 'SelBranchCategorys');
        $this->addRelation('SelBranchEmployee', 'SelBranchEmployee', RelationMap::ONE_TO_MANY, array('ID' => 'BRANCH_ID', ), null, null, 'SelBranchEmployees');
        $this->addRelation('SelProductItem', 'SelProductItem', RelationMap::ONE_TO_MANY, array('ID' => 'BRANCH_ID', ), null, null, 'SelProductItems');
        $this->addRelation('SelSale', 'SelSale', RelationMap::ONE_TO_MANY, array('ID' => 'BRANCH_ID', ), null, null, 'SelSales');
    } // buildRelations()

} // SelBranchTableMap
