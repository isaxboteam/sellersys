<?php



/**
 * This class defines the structure of the 'sel_product_sell' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelProductSellTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelProductSellTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_product_sell');
        $this->setPhpName('SelProductSell');
        $this->setClassname('SelProductSell');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('PRODUCT_ID', 'ProductId', 'INTEGER', 'sel_product', 'ID', true, null, null);
        $this->addForeignKey('SALE_ID', 'SaleId', 'INTEGER', 'sel_sale', 'ID', true, null, null);
        $this->addColumn('QUANTITY', 'Quantity', 'INTEGER', true, null, null);
        $this->addColumn('PRICE', 'Price', 'FLOAT', true, 9, null);
        $this->addColumn('ADDED_DATE', 'AddedDate', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelProduct', 'SelProduct', RelationMap::MANY_TO_ONE, array('PRODUCT_ID' => 'ID', ), null, null);
        $this->addRelation('SelSale', 'SelSale', RelationMap::MANY_TO_ONE, array('SALE_ID' => 'ID', ), null, null);
        $this->addRelation('SelProductItem', 'SelProductItem', RelationMap::ONE_TO_MANY, array('ID' => 'PRODUCT_SELL_ID', ), null, null, 'SelProductItems');
    } // buildRelations()

} // SelProductSellTableMap
