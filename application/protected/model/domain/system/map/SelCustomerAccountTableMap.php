<?php



/**
 * This class defines the structure of the 'sel_customer_account' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelCustomerAccountTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelCustomerAccountTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_customer_account');
        $this->setPhpName('SelCustomerAccount');
        $this->setClassname('SelCustomerAccount');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('EMAIL', 'Email', 'VARCHAR', true, 100, null);
        $this->addColumn('PASSWORD', 'Password', 'VARCHAR', true, 200, null);
        $this->addColumn('LAST_ACCESS', 'LastAccess', 'TIMESTAMP', false, null, null);
        $this->addColumn('CURRENT_ACCESS', 'CurrentAccess', 'TIMESTAMP', false, null, null);
        $this->addColumn('CREATION_DATE', 'CreationDate', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelCustomer', 'SelCustomer', RelationMap::ONE_TO_MANY, array('ID' => 'CUSTOMER_ACCOUNT_ID', ), null, null, 'SelCustomers');
    } // buildRelations()

} // SelCustomerAccountTableMap
