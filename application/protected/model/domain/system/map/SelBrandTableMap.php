<?php



/**
 * This class defines the structure of the 'sel_brand' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelBrandTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelBrandTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_brand');
        $this->setPhpName('SelBrand');
        $this->setClassname('SelBrand');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('NAME', 'Name', 'VARCHAR', false, 100, null);
        $this->addColumn('CODE', 'Code', 'VARCHAR', false, 20, null);
        $this->addColumn('DESCRIPTION', 'Description', 'VARCHAR', false, 300, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelProduct', 'SelProduct', RelationMap::ONE_TO_MANY, array('ID' => 'BRAND_ID', ), null, null, 'SelProducts');
        $this->addRelation('SelProductItem', 'SelProductItem', RelationMap::ONE_TO_MANY, array('ID' => 'BRAND_ID', ), null, null, 'SelProductItems');
    } // buildRelations()

} // SelBrandTableMap
