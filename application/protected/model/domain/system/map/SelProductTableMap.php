<?php



/**
 * This class defines the structure of the 'sel_product' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelProductTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelProductTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_product');
        $this->setPhpName('SelProduct');
        $this->setClassname('SelProduct');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('PRODUCT_GROUP_ID', 'ProductGroupId', 'INTEGER', 'sel_product_group', 'ID', false, null, null);
        $this->addForeignKey('BRAND_ID', 'BrandId', 'INTEGER', 'sel_brand', 'ID', true, null, null);
        $this->addForeignKey('CATEGORY_ID', 'CategoryId', 'INTEGER', 'sel_category', 'ID', true, null, null);
        $this->addForeignKey('MAIN_IMAGE_ID', 'MainImageId', 'INTEGER', 'sel_product_image', 'ID', false, null, null);
        $this->addColumn('CODE', 'Code', 'VARCHAR', true, 20, null);
        $this->addColumn('NAME', 'Name', 'VARCHAR', true, 100, null);
        $this->addColumn('BASE_PRICE', 'BasePrice', 'FLOAT', true, 9, null);
        $this->addColumn('DESCRIPTION', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('MODEL', 'Model', 'VARCHAR', false, 100, null);
        $this->addColumn('NUMBER_PARTS', 'NumberParts', 'INTEGER', false, null, null);
        $this->addColumn('IS_MAIN', 'IsMain', 'BOOLEAN', true, 1, false);
        $this->addColumn('IS_NEW', 'IsNew', 'BOOLEAN', true, 1, false);
        $this->addColumn('IS_OFFER', 'IsOffer', 'BOOLEAN', true, 1, false);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelCategory', 'SelCategory', RelationMap::MANY_TO_ONE, array('CATEGORY_ID' => 'ID', ), null, null);
        $this->addRelation('SelProductGroup', 'SelProductGroup', RelationMap::MANY_TO_ONE, array('PRODUCT_GROUP_ID' => 'ID', ), null, null);
        $this->addRelation('SelBrand', 'SelBrand', RelationMap::MANY_TO_ONE, array('BRAND_ID' => 'ID', ), null, null);
        $this->addRelation('SelProductImageRelatedByMainImageId', 'SelProductImage', RelationMap::MANY_TO_ONE, array('MAIN_IMAGE_ID' => 'ID', ), null, null);
        $this->addRelation('SelProductAcquisition', 'SelProductAcquisition', RelationMap::ONE_TO_MANY, array('ID' => 'PRODUCT_ID', ), null, null, 'SelProductAcquisitions');
        $this->addRelation('SelProductImageRelatedByProductId', 'SelProductImage', RelationMap::ONE_TO_MANY, array('ID' => 'PRODUCT_ID', ), null, null, 'SelProductImagesRelatedByProductId');
        $this->addRelation('SelProductOffer', 'SelProductOffer', RelationMap::ONE_TO_ONE, array('ID' => 'ID', ), null, null);
        $this->addRelation('SelProductOrder', 'SelProductOrder', RelationMap::ONE_TO_MANY, array('ID' => 'PRODUCT_ID', ), null, null, 'SelProductOrders');
        $this->addRelation('SelProductSell', 'SelProductSell', RelationMap::ONE_TO_MANY, array('ID' => 'PRODUCT_ID', ), null, null, 'SelProductSells');
    } // buildRelations()

} // SelProductTableMap
