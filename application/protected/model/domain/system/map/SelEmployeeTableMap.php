<?php



/**
 * This class defines the structure of the 'sel_employee' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelEmployeeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelEmployeeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_employee');
        $this->setPhpName('SelEmployee');
        $this->setClassname('SelEmployee');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('USER_ID', 'UserId', 'INTEGER', 'cho_user', 'ID', true, null, null);
        $this->addColumn('INIT_DATE', 'InitDate', 'DATE', true, null, null);
        $this->addColumn('END_DATE', 'EndDate', 'DATE', true, null, null);
        $this->addColumn('DETAILS', 'Details', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ChoUser', 'ChoUser', RelationMap::MANY_TO_ONE, array('USER_ID' => 'ID', ), null, null);
        $this->addRelation('SelBranchEmployee', 'SelBranchEmployee', RelationMap::ONE_TO_MANY, array('ID' => 'EMPLOYEE_ID', ), null, null, 'SelBranchEmployees');
        $this->addRelation('SelSale', 'SelSale', RelationMap::ONE_TO_MANY, array('ID' => 'EMPLOYEE_ID', ), null, null, 'SelSales');
    } // buildRelations()

} // SelEmployeeTableMap
