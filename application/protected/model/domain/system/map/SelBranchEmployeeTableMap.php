<?php



/**
 * This class defines the structure of the 'sel_branch_employee' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelBranchEmployeeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelBranchEmployeeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_branch_employee');
        $this->setPhpName('SelBranchEmployee');
        $this->setClassname('SelBranchEmployee');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('BRANCH_ID', 'BranchId', 'INTEGER', 'sel_branch', 'ID', true, null, null);
        $this->addForeignKey('EMPLOYEE_ID', 'EmployeeId', 'INTEGER', 'sel_employee', 'ID', true, null, null);
        $this->addColumn('INIT_DATE', 'InitDate', 'DATE', true, null, null);
        $this->addColumn('END_DATE', 'EndDate', 'DATE', true, null, null);
        $this->addColumn('ADMIN', 'Admin', 'BOOLEAN', true, 1, null);
        $this->addColumn('STATUS', 'Status', 'VARCHAR', false, 15, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelEmployee', 'SelEmployee', RelationMap::MANY_TO_ONE, array('EMPLOYEE_ID' => 'ID', ), null, null);
        $this->addRelation('SelBranch', 'SelBranch', RelationMap::MANY_TO_ONE, array('BRANCH_ID' => 'ID', ), null, null);
    } // buildRelations()

} // SelBranchEmployeeTableMap
