<?php



/**
 * This class defines the structure of the 'cho_rol_x_uri' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class ChoRolXUriTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.ChoRolXUriTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cho_rol_x_uri');
        $this->setPhpName('ChoRolXUri');
        $this->setClassname('ChoRolXUri');
        $this->setPackage('system');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('ROL_ID', 'RolId', 'INTEGER' , 'cho_rol', 'ID', true, null, null);
        $this->addForeignPrimaryKey('URI_ID', 'UriId', 'INTEGER' , 'cho_uri', 'ID', true, null, null);
        $this->addColumn('READ', 'Read', 'VARCHAR', true, 3, 'SI');
        $this->addColumn('CREATE', 'Create', 'VARCHAR', true, 3, 'NO');
        $this->addColumn('UPDATE', 'Update', 'VARCHAR', true, 3, 'NO');
        $this->addColumn('DELETE', 'Delete', 'VARCHAR', true, 3, 'NO');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ChoRol', 'ChoRol', RelationMap::MANY_TO_ONE, array('ROL_ID' => 'ID', ), null, null);
        $this->addRelation('ChoUri', 'ChoUri', RelationMap::MANY_TO_ONE, array('URI_ID' => 'ID', ), null, null);
    } // buildRelations()

} // ChoRolXUriTableMap
