<?php



/**
 * This class defines the structure of the 'sel_person' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelPersonTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelPersonTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_person');
        $this->setPhpName('SelPerson');
        $this->setClassname('SelPerson');
        $this->setPackage('system');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('NAME', 'Name', 'VARCHAR', false, 20, null);
        $this->addColumn('FIRST_LASTNAME', 'FirstLastname', 'VARCHAR', false, 20, null);
        $this->addColumn('SECOND_LASTNAME', 'SecondLastname', 'VARCHAR', false, 20, null);
        $this->addColumn('IDENTITY', 'Identity', 'INTEGER', false, null, null);
        $this->addColumn('IDENTITY_EXT', 'IdentityExt', 'VARCHAR', false, 20, null);
        $this->addColumn('GENDER', 'Gender', 'VARCHAR', false, 20, null);
        $this->addColumn('BIRTHDAY_DATE', 'BirthdayDate', 'DATE', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelProvider', 'SelProvider', RelationMap::ONE_TO_MANY, array('ID' => 'PERSON_ID', ), null, null, 'SelProviders');
    } // buildRelations()

} // SelPersonTableMap
