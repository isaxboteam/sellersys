<?php



/**
 * This class defines the structure of the 'sel_product_offer' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelProductOfferTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelProductOfferTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_product_offer');
        $this->setPhpName('SelProductOffer');
        $this->setClassname('SelProductOffer');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addForeignPrimaryKey('ID', 'Id', 'INTEGER' , 'sel_product', 'ID', true, null, null);
        $this->addColumn('PRODUCT_ID', 'ProductId', 'INTEGER', true, null, null);
        $this->addColumn('TYPE', 'Type', 'VARCHAR', true, 20, null);
        $this->addColumn('INITIAL_DATE', 'InitialDate', 'DATE', true, null, null);
        $this->addColumn('END_DATE', 'EndDate', 'DATE', true, null, null);
        $this->addColumn('POSiTION', 'Position', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelProduct', 'SelProduct', RelationMap::MANY_TO_ONE, array('ID' => 'ID', ), null, null);
    } // buildRelations()

} // SelProductOfferTableMap
