<?php



/**
 * This class defines the structure of the 'sel_sale' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelSaleTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelSaleTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_sale');
        $this->setPhpName('SelSale');
        $this->setClassname('SelSale');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('BRANCH_ID', 'BranchId', 'INTEGER', 'sel_branch', 'ID', true, null, null);
        $this->addForeignKey('EMPLOYEE_ID', 'EmployeeId', 'INTEGER', 'sel_employee', 'ID', true, null, null);
        $this->addForeignKey('CUSTOMER_ID', 'CustomerId', 'INTEGER', 'sel_customer', 'ID', true, null, null);
        $this->addColumn('DATE', 'Date', 'TIMESTAMP', false, null, null);
        $this->addColumn('STATUS', 'Status', 'VARCHAR', false, 20, null);
        $this->addColumn('CODE', 'Code', 'VARCHAR', true, 20, null);
        $this->addColumn('MONEY_CHANGE', 'MoneyChange', 'INTEGER', false, null, null);
        $this->addColumn('OBSERVATION', 'Observation', 'VARCHAR', false, 300, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelCustomer', 'SelCustomer', RelationMap::MANY_TO_ONE, array('CUSTOMER_ID' => 'ID', ), null, null);
        $this->addRelation('SelBranch', 'SelBranch', RelationMap::MANY_TO_ONE, array('BRANCH_ID' => 'ID', ), null, null);
        $this->addRelation('SelEmployee', 'SelEmployee', RelationMap::MANY_TO_ONE, array('EMPLOYEE_ID' => 'ID', ), null, null);
        $this->addRelation('SelOrder', 'SelOrder', RelationMap::ONE_TO_MANY, array('ID' => 'SALES_ID', ), null, null, 'SelOrders');
        $this->addRelation('SelProductSell', 'SelProductSell', RelationMap::ONE_TO_MANY, array('ID' => 'SALE_ID', ), null, null, 'SelProductSells');
    } // buildRelations()

} // SelSaleTableMap
