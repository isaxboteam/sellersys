<?php



/**
 * This class defines the structure of the 'sel_product_group' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class SelProductGroupTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.SelProductGroupTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sel_product_group');
        $this->setPhpName('SelProductGroup');
        $this->setClassname('SelProductGroup');
        $this->setPackage('system');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('CATEGORY_ID', 'CategoryId', 'INTEGER', 'sel_category', 'ID', true, null, null);
        $this->addColumn('CODE', 'Code', 'VARCHAR', true, 20, null);
        $this->addColumn('NAME', 'Name', 'VARCHAR', true, 200, null);
        $this->addColumn('DESCRIPTION', 'Description', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SelCategory', 'SelCategory', RelationMap::MANY_TO_ONE, array('CATEGORY_ID' => 'ID', ), null, null);
        $this->addRelation('SelProduct', 'SelProduct', RelationMap::ONE_TO_MANY, array('ID' => 'PRODUCT_GROUP_ID', ), null, null, 'SelProducts');
    } // buildRelations()

} // SelProductGroupTableMap
