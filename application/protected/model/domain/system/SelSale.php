<?php
/**
 *
 */
class SelSale extends BaseSelSale
{


    const OPEN = 'OPEN';
    const CLOSED = 'CLOSED';

    public static function enum()
    {
        $statusTypes = array(
            SelSale::OPEN => SelSale::OPEN,
            SelSale::CLOSED=>SelSale::CLOSED
        );
        return $statusTypes;
    }

    public function preSave(PropelPDO $con = null)
    {
        return parent::preSave($con);
    }

    public function preValidate($columns = null)
    {
        return $this->preSave();
    }

    public function preInsert(PropelPDO $con = null)
    {
        $this->setCode(
            SelSaleQuery::create()
                        ->filterBySelBranch($this->getSelBranch())
                    ->count() + 1
        );
        return $this->preSave();
    }

    /**
     * @return int
     */
    public function countTotalItems()
    {
        $total = 0;
        foreach($this->getSelProductSells() as $productSales){
            $total+= $productSales->getQuantity();
        }
        return $total;
    }

    /**
     * @return float
     */
    public function totalPrice()
    {
        $total = 0.0;
        foreach($this->getSelProductSells() as $productSales){
            $total+= $productSales->totalPrice();
        }
        return $total;
    }

}