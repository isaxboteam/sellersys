<?php
/**
 * Skeleton subclass for performing query and update operations on the 'sel_category' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class SelCategoryQuery extends BaseSelCategoryQuery
{
    static public function principales(){
        $query = new self();
//        $query->filterByMainCategoryId(null)->orderByName(Criteria::DESC)->orderByCode();
        $query->filterByMainCategoryId(null)->orderByName()->orderByCode();
        return $query;
    }

    static public function esPrincipal(){
        $succes = false;
        $category = SelCategoryPeer::retrieveByPK(id);
    }

    static public function mainProductItemsBySucursal($id,$branch){
        $categories=self::create()
            ->filterByMainCategoryId($id)
            ->find();
        $products=SelProductQuery::create()
            ->useSelCategoryQuery()
            ->filterByMainCategoryId($id)
            ->endUse()
            ->find();

        $productsItem=function($product){
            $branch=SelBranchPeer::retrieveByPK(Req::_('branchId'));
            return array("product"=>$product,"count"=>SelProductItemQuery::productItemByProduct($product,$branch)->count());
        };
        $masterProducts=array_map($productsItem,$products->getArrayCopy());
    }

}