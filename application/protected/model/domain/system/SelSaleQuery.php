<?php
/**
 *
 */
class SelSaleQuery extends BaseSelSaleQuery
{

    public static function createBetween($initDate, $endDate)
    {
        return self::create()
            ->filterByDate($initDate, Criteria::GREATER_EQUAL)
            ->filterByDate($endDate, Criteria::LESS_EQUAL);
    }

}