<?php
/**
 * Skeleton subclass for representing a row from the 'cho_rol' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class ChoRol extends BaseChoRol
{

    static $validationRules = array(
            'code' => array(
                'null' => false, 'blank'=> false,
                'size'=> array('min' => 3, 'max' => 30),
            ),
            'name' => array(
                'null' => false, 'blank'=> false,
                'size'=> array('min' => 3, 'max' => 50),
            ),
            'description' => array(
                'null' => true,  'blank'=> false,
            ),
        );

}