<?php



/**
 * Skeleton subclass for performing query and update operations on the 'sel_product_acquisition' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class SelProductAcquisitionQuery extends BaseSelProductAcquisitionQuery
{
    static public function withBranch(){
        $query = new self();
        $query->filterByMainCategoryId (null)->orderByName()->orderByCode();
        return $query;
    }

    static public function lessBranch(){
        $query = new self();
        $query->filterByMainCategoryId(null)->orderByName()->orderByCode();
        return $query;
    }
}
