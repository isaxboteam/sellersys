<?php

class SelAcquisition extends BaseSelAcquisition
{

    static $validationRules = array(
        'ProviderId' => array(
            'null' => false, 'blank' => false,
        ),
        'Date' => array(
            'null' => false, 'blank'=> false,
        ),
        'WarrantyProvider' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 30),
        ),
    );

    public function preSave()
    {
        $this->provider_id = $this->provider_id != ''? $this->provider_id: null;
        $this->warranty_provider = $this->warranty_provider != ''? $this->warranty_provider: null;
        return parent::preSave();
    }

    public function preValidate()
    {
        return $this->preSave();
    }

    /**
     * @param string $order
     * @return array|mixed|PropelObjectCollection
     * @throws PropelException
     */
    public function productAcquisitions($order = Criteria::ASC)
    {
        return SelProductAcquisitionQuery::create()
                ->filterBySelAcquisition($this)
                ->useSelProductQuery()
                    ->orderByName($order)
                ->endUse()
                ->orderByQuantity()
            ->find();
    }

    /**
     * @return int
     */
    public function countTotalItems()
    {
        $total = 0;
        foreach($this->productAcquisitions() as $productAcquisition){
            $total+= $productAcquisition->countSelProductItems();
        }
        return $total;
    }

    /**
     * @return float
     */
    public function totalPrice()
    {
        $total = 0.0;
        foreach($this->productAcquisitions() as $productAcquisition){
            $total+= $productAcquisition->totalPrice();
        }
        return $total;
    }

}