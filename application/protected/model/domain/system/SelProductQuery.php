<?php



/**
 * Skeleton subclass for performing query and update operations on the 'sel_product' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class SelProductQuery extends BaseSelProductQuery
{
    static public function productByMainCategory($mainCategoryId){
        $products=self::create()
            ->useSelCategoryQuery()
            ->filterByMainCategoryId($mainCategoryId)
            ->endUse();
        return $products;
    }
    static public function productByCategory($categoryId){
        $products=self::create()
            ->useSelCategoryQuery()
            ->filterById($categoryId)
            ->endUse();
        return $products;
    }
}
