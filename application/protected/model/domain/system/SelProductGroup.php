<?php
/**
 * Skeleton subclass for representing a row from the 'sel_product_group' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class SelProductGroup extends BaseSelProductGroup
{

    static $validationRules = array(
        'code' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 20),
        ),
        'name' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 100),
        ),
        'description' => array(
            'null' => true, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 2000),
        )
    );

    public function preValidate($columns = null)
    {
        return $this->preSave();
    }

    public function preSave(PropelPDO $con = null)
    {
        $this->code = strtoupper($this->code);
        $this->description = trim($this->description)?: null;
        return parent::preSave($con);
    }

    public function numberOfItems()
    {
        return SelProductItemQuery::createInStock()
                ->useSelProductAcquisitionQuery()
                    ->useSelProductQuery()
                        ->filterBySelProductGroup($this)
                    ->endUse()
                ->endUse()
            ->count();
    }

}