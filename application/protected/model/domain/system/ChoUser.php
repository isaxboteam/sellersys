<?php
class ChoUser extends BaseChoUser
{

    const DIR = 'users';

    static $validationRules = array(
        'firstName' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 30),
        ),
        'secondName' => array(
            'null' => true, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 30),
        ),
        'firstLastname' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 30),
        ),
        'secondLastname' => array(
            'null' => true, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 30),
        ),
        'dni' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 5, 'max' => 20),
        ),
        'username' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 50),
        ),
        'email' => array(
            'null' => false, 'blank'=> false, 'email'=> true,
            'size'=> array('min' => 3, 'max' => 50),
        ),
        'type' => array(
            'null' => false, 'blank'=> false,
            //'inlist' => UserTypesEnum::enum(),
        ),
        'gender' => array(
            'null' => false, 'blank'=> false,
            'inlist'=> array('MALE', 'FEMALE'),
        ),
        'birthday' => array(
            'null' => false
        ),
        'address' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 500),
        ),
        'primaryPhone' => array(
            'null' => true, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 20),
        ),
        'cellPhone' => array(
            'null' => true, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 20),
        ),
    );

    /*
    public function doValidate($columns = null)
    {
        $parentErrors = parent::doValidate($columns);
        $validationErrors = ValidationHelper::validateObject($this, self::$validationRules);
        return ValidationHelper::mergeFailures($parentErrors, $validationErrors);
    }

    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }
     */

    public function preValidate($columns = null)
    {
        return $this->preSave();
    }

    public function preSave(PropelPDO $con = null)
    {
        $this->first_name = trim(strtoupper($this->first_name))?: null;
        $this->second_name = trim(strtoupper($this->second_name))?: null;
        $this->first_lastname = trim(strtoupper($this->first_lastname))?: null;
        $this->second_lastname = trim(strtoupper($this->second_lastname))?: null;
        $this->username = trim(strtolower($this->username))?: null;
        $this->email = trim(strtolower($this->email))?: null;
        $this->address = trim($this->address)?: null;
        $this->primary_phone = trim($this->primary_phone)?: null;
        $this->secondary_phone = trim($this->secondary_phone)?: null;
        $this->cell_phone = trim($this->cell_phone)?: null;
        return parent::preSave($con);
    }

    /**
     * Crypt a string by a encryption method
     * @param string $string
     * @return string
     */
    public static function crypt($string)
    {
        /* TODO implements a encryption method*/
        $hash = $string;
        return $hash;
    }

    /**
     * Decrypt a string by a reverse encryption method
     * @param string $hash
     * @return string
     */
    public static function decrypt($hash)
    {
        /* TODO implements a decryption method*/
        $string = $hash;
        return $string;
    }

    /**
     * Change the password of this user
     * @param string $oldPassword
     * @param string $newPassword
     * @return bool
     */
    public function changePassword($oldPassword, $newPassword)
    {
        if(self::crypt($oldPassword) != $this->getPassword()){
            $this->setPassword(self::crypt($newPassword));
            $this->save();
            return true;
        }else{
            return false;
        }
    }

    /**
     * Return the complete normal name of this User
     * @param bool $long
     * @return string
     */
    public function completeName($long = true)
    {
        return $this->getFirstName()
        .($long? ' '.$this->getSecondName(): '')
        .' '.$this->getFirstLastname()
        .($long? ' '.$this->getSecondLastname(): '');
    }

    /**
     * Return the complete formal name of this User
     * @param bool $long
     * @return string
     */
    public function formalName($long = true)
    {
        return $this->getFirstLastname()
        .($long? ' '.$this->getSecondLastname(): '')
        .' '.$this->getFirstName()
        .($long? ' '.$this->getSecondName(): '');
    }

    /**
     *
     * @return PropelObjectCollection|ChoRol[] array. List of Rol
     */
    public function inOrderRols()
    {
        return ChoRolQuery::create()
                ->useChoUserXRolQuery()
                ->filterByChoUser($this)
                ->endUse()
            ->orderByName()->find();
    }

    /**
     * @return string
     */
    public function imageName()
    {
        return $this->getId().'.'.
        ImageMimeTypes::mimeExtensionFrom($this->getPhotoFormat());
    }

    /**
     * @return string
     */
    public function imageLocation()
    {
        return self::DIR.'/'.$this->imageName();
    }

    /**
     * @return string
     */
    public function defaultImageLocation()
    {
        return self::DIR.'/0.png';
    }


    /**
     * @return bool
     */
    public function tieneImagen()
    {
        return file_exists(IMG_DIR.$this->imageLocation());

    }

    /**
     * @return string
     */
    public function recursoImagen()
    {
        return $this->tieneImagen()? IMG_WEB.$this->imageLocation().'?t='.time(): IMG_WEB.$this->defaultImageLocation();

    }

}