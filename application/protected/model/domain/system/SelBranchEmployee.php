<?php
/**
 *
 *
 */
class SelBranchEmployee extends BaseSelBranchEmployee
{

    static $validationRules = array(
        'InitDate' => array(
            'null' => false, 'blank'=> false,
        ),
        'EndDate' => array(
            'null' => false, 'blank'=> false,
        ),
        'BranchId' => array(
            'null' => false,  'blank'=> false,
        ),
        'EmployeeId' => array(
            'null' => false,  'blank'=> false,
        ),
    );

    public function preValidate($columns = null)
    {
        return $this->preSave();
    }

    public function preSave(PropelPDO $con = null)
    {
        $this->branch_id = $this->branch_id?: null;
        $this->employee_id = $this->employee_id?: null;
        return parent::preSave($con);
    }

}