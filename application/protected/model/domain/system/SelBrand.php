<?php
/**
 *
 * @package    propel.generator.system
 */
class SelBrand extends BaseSelBrand
{

    static $validationRules = array(
        'code' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 10),
        ),
        'name' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 100),
        ),
        'description' => array(
            'null' => true, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 30),
        )
    );

    public function preValidate($columns = null)
    {
        return $this->preSave();
    }

    public function preSave(PropelPDO $con = null)
    {
        $this->code = strtoupper($this->code);
        $this->description = trim($this->description)?: null;
        return parent::preSave($con);
    }

}
