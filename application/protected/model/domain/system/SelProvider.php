<?php

class SelProvider extends BaseSelProvider
{

    static $validationRules = array(
        'Company' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 100),
        ),
        'Address' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 200),
        ),
        'Person' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 8, 'max' => 100),
        ),
        'Phone' => array(
            'null' => true, 'blank'=> true,
            'size'=> array('min' => 7, 'max' => 20),
        ),
        'Cellphone' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 7, 'max' => 20),
        ),
    );

    public function preSave()
    {
        $this->address = $this->address != ''? $this->address: null;
        $this->phone = $this->phone != ''? $this->phone: null;
        $this->cellphone = $this->cellphone != ''? $this->cellphone: null;
        return parent::preSave();
    }

    public function preValidate()
    {
        return $this->preSave();
    }


    /**
     * @param string $order
     * @return array|mixed|PropelObjectCollection
     * @throws PropelException
     */
    public function acquisitions($order = Criteria::DESC)
    {
        return SelAcquisitionQuery::create()
                ->filterBySelProvider($this)
                ->orderByDate($order)
            ->find();
    }
   
}