<?php
/**
 * Skeleton subclass for representing a row from the 'sel_acquisition_product' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class SelAcquisitionProduct extends BaseSelAcquisitionProduct
{
    static $validationRules = array(
        'date' => array(
            'null' => false, 'blank'=> false, 
        ),
        'observation' => array(
            'null' => true,  'blank'=> false,
        ),
    );
    
    public function productItems()
    {
//        $productAcquisition = SelAcquisitionProductPeer::retrieveByPK($this->id);
//        $c = new Criteria();
//        $c->addAscendingOrderByColumn(SelProductItemPeer::PRODUCT_ID);
//
        return ProductoItemQuery::create()
            ->filterBySelAcquisitionProduct($this)
            ->orderByCost()
//                    ->orderByName()
            ->find();
    }
}
