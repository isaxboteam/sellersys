<?php
/**
 *
 */
class SelCustomerAccount extends BaseSelCustomerAccount
{

    static $validationRules = array(
        'email'=> array(
            'null' => false, 'blank' => false,
            'email'=> true,
            'size' => array('max' => 30)
        ),
        'password'=> array(
            'null' => false, 'blank' => false,
            'size' => array('min' => 6, 'max' => 50)
        ),
    );

    /**
     * @return SelCustomer
     * @throws PropelException
     */
    public function customer()
    {
        return SelCustomerQuery::create()
                ->filterBySelCustomerAccount($this)
            ->findOne();
    }

}