<?php
/**
 *
 */
class SelCustomer extends BaseSelCustomer
{

    static $validationRules = array(
        'idNumber'=> array(
            'null' => false, 'blank' => false,
            'size' => array('min' => 3, 'max' => 30)
        ),
        'email'=> array(
            'null' => false, 'blank' => false,
            'email'=> true,
            'size' => array('max' => 30)
        ),
        'name'=> array(
            'null' => false, 'blank' => false,
            'size' => array('min' => 2, 'max' => 50)
        ),
        'lastname'=> array(
            'null' => false, 'blank' => false,
            'size' => array('min' => 2, 'max' => 50)
        ),
        'address'=> array(
            'null' => true, 'blank' => false,
            'size' => array('min' => 7, 'max' => 500)
        ),
        'phone'=> array(
            'null' => false, 'blank' => false,
            'size' => array('min' => 2, 'max' => 20)
        ),
        'cellphone'=> array(
            'null' => false, 'blank' => false,
            'size' => array('min' => 2, 'max' => 20)
        ),
    );

    /**
     * @return bool
     */
    public function preValidate()
    {
        $this->id_number = $this->id_number? trim($this->id_number): null;
        $this->email = $this->email? strtolower(trim($this->email)): null;
        $this->name = $this->name? strtoupper(trim($this->name)): null;
        $this->lastname = $this->lastname? strtoupper(trim($this->lastname)): null;
        $this->address = $this->address? trim($this->address): null;
        return true;
    }

    /**
     * @return string
     */
    public function completeName()
    {
        return $this->lastname.' '.$this->name;
    }

    /**
     * @return array|mixed|PropelObjectCollection
     * @throws PropelException
     */
    public function orders()
    {
        return SelOrderQuery::create()
                ->orderByOrderDate()
                ->orderByCreationDate()
                ->filterBySelCustomer($this)
            ->find();
    }

}