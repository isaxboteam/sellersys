<?php
/**
 * Skeleton subclass for representing a row from the 'sel_product' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class SelProduct extends BaseSelProduct
{


    static $validationRules = array(
        'code' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 30),
        ),
        'name' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 50),
        ),
        'basePrice' => array(
            'null' => false, 'blank'=> false,
            'range'=> array('min' => 0.0, 'max' => 999999.99),
        ),
        'description' => array(
            'null' => true,  'blank'=> true,
        ),
        'model' => array(
            'null' => true, 'blank' =>false,
        ),
        'numberParts' => array (
            'null' => true, 'blank' =>false,
        )
    );

    public function preValidate()
    {
        if($this->description==''){
            $this->description=null;
        }
        if($this->model==''){
            $this->model=null;
        }
        return true;
    }

    public function preInsert()
    {
        if($this->description==''){
            $this->description=null;
        }
        if($this->model==''){
            $this->model=null;
        }
        return true;
    }

    /**
     * @return int
     * @throws PropelException
     */
    public function numberOfItems()
    {
        return SelProductItemQuery::createInStock()
                ->useSelProductAcquisitionQuery()
                    ->filterBySelProduct($this)
                ->endUse()
            ->count();
    }

    /**
     * @return bool
     */
    public function hasMainImage(){
        return $this->getSelProductImageRelatedByMainImageId()? true: false;
    }

    /**
     * @return SelProductImage
     */
    public function mainImage(){
        return $this->getSelProductImageRelatedByMainImageId();
    }

    /**
     * @return PropelObjectCollection|SelProductImage[]
     */
    public function images(){
        return SelProductImageQuery::create()
            ->filterBySelProductRelatedByProductId($this)
            ->orderByPosition()
        ->find();
    }

    /**
     * @param $branchId
     * @return int
     */
    public function productItemByBranch($branchId){
        return SelProductItemQuery::create()
                ->filterByStatus(SelProductItem::STOCK)
                ->filterByBranchId($branchId)
                ->useSelProductAcquisitionQuery()
                    ->filterByProductId($this->id)
                ->endUse()
            ->count();
    }

    /**
     * @param $productAcquisition
     * @return int
     * @throws PropelException]
     */
    public function productItemByProductAcquisition($productAcquisition){
        return SelProductItemQuery::create()
                ->filterByStatus(SelProductItem::IN_ORDER)
                ->filterBySelProductAcquisition($productAcquisition)
                ->useSelProductAcquisitionQuery()
                    ->filterByProductId($this->id)
                ->endUse()
            ->count();
    }

    /**
     * @param $productAcquisition
     * @return int
     * @throws PropelException
     */
    public function productItemByProductAcquisitionLess($productAcquisition){
        return SelProductItemQuery::create()
                ->filterByStatus(SelProductItem::IN_ORDER)
                ->filterByBranchId(null)
                ->filterBySelProductAcquisition($productAcquisition)
                ->useSelProductAcquisitionQuery()
                    ->filterByProductId($this->id)
                ->endUse()
            ->count();
    }

    /**
     * @return int
     */
    public function productsItemBranchLess(){
        return SelProductItemQuery::create()
            ->filterByBranchId(null)
            ->orderByBranchId()
            ->filterByStatus(SelProductItem::IN_ORDER)
                ->useSelProductAcquisitionQuery()
                    ->filterByProductId($this->id)
                ->endUse()
            ->count();
    }

}