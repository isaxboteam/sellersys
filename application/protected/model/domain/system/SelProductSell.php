<?php
/**
 *
 */
class SelProductSell extends BaseSelProductSell
{

    /**
     * @return float
     */
    public function totalPrice()
    {
        return $this->quantity * $this->price;
    }

}