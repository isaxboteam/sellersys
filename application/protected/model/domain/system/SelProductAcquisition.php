<?php
/**
 *
 */
class SelProductAcquisition extends BaseSelProductAcquisition
{

    const BS = 'Bs';
    const SUS = '$us';

    static $validationRules = array(
        'ProductId' => array(
            'null' => false, 'blank'=> false,
        ),
        'Quantity' => array(
            'null' => false, 'blank'=> false,
            'range'=> array('min' => 1, 'max' => 100000),
        ),
        'Price' => array(
            'null' => false, 'blank'=> false,
            'range'=> array('min' => 0.01, 'max' => 100000.00),
        ),
        'Money' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 5),
        ),
    );

    /**
     * @return array
     */
    public static function enumMoneda()
    {
        $statusTypes = array(
            SelProductAcquisition::BS => SelProductAcquisition::BS,
            SelProductAcquisition::SUS => SelProductAcquisition::SUS
        );
        return $statusTypes;
    }

    public function preSave(PropelPDO $con = null)
    {
        $this->product_id = ($this->product_id)?: null;
        return parent::preSave($con);
    }

    public function preValidate($columns = null)
    {
        return $this->preSave();
    }

    /**
     * @param string $order
     * @return array|mixed|PropelObjectCollection
     * @throws PropelException
     */
    public function productItems($order = Criteria::ASC)
    {
        return SelProductItemQuery::create()
                ->filterBySelProductAcquisition($this)
                ->orderByCost($order)
            ->find();
    }

    /**
     * @return float
     */
    public function totalPrice()
    {
        return $this->price * $this->countSelProductItems();
    }

    /**
     * @return array|mixed|PropelObjectCollection
     * @throws PropelException
     */
    public function assignedItems()
    {
        return SelProductItemQuery::isAssigneds($this)->find();
    }

    /**
     * @return int
     */
    public function countAssignedItems()
    {
        return SelProductItemQuery::isAssigneds($this)->count();
    }

    /**
     * @param int $maxRes
     * @return array|mixed|PropelObjectCollection
     * @throws PropelException
     */
    public function notAssignedItems($maxRes = 0)
    {
        return SelProductItemQuery::isNotAssigneds($this)
                ->_if($maxRes > 0)
                    ->limit($maxRes)
                ->_endif()
            ->find();
    }

    /**
     * @return int
     */
    public function countNotAssignedItems()
    {
        return SelProductItemQuery::isNotAssigneds($this)->count();
    }

    /**
     * @param SelBranch $branch
     * @return array|mixed|PropelObjectCollection
     */
    public function assignedTo(SelBranch $branch)
    {
        return SelProductItemQuery::assignedsTo($this, $branch)->find();
    }

    /**
     * @param SelBranch $branch
     * @return array|mixed|PropelObjectCollection
     */
    public function countAssignedTo(SelBranch $branch)
    {
        return SelProductItemQuery::assignedsTo($this, $branch)->count();
    }

}