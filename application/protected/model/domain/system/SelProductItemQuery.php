<?php
/**
 * Skeleton subclass for performing query and update operations on the 'sel_product_item' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class SelProductItemQuery extends BaseSelProductItemQuery
{

    public static function createInStock()
    {
        return self::create()->filterByStatus(SelProductItem::STOCK);
    }

    public static function productItemByProduct($product,$branch){
        return self::create()
            ->filterByStatus(SelProductItem::STOCK)
            ->filterBySelBranch($branch)
            ->useSelProductAcquisitionQuery()
            ->filterBySelProduct($product)
            ->endUse();
    }

    static public function productSucursalLess($productAcquisition){
        $query=new self();
        $query->filterBySelProductAcquisition($productAcquisition)
            ->filterByStatus(SelProductItem::IN_ORDER)
            ->filterByBranchId(null);
        return $query;
    }

    /**
     * @param SelProductAcquisition null $productAcquisition
     * @return SelProductItemQuery
     */
    static public function isNotAssigneds($productAcquisition = null)
    {
        return (new self())
            ->_if(is_object($productAcquisition))
                ->filterBySelProductAcquisition($productAcquisition)
            ->_endif()
            ->filterByBranchId(null);
    }

    /**
     * @param SelProductAcquisition $productAcquisition
     * @return SelProductItemQuery
     */
    static public function isAssigneds($productAcquisition)
    {
        return (new self())
            ->filterBySelProductAcquisition($productAcquisition)
            ->filterByBranchId(null, Criteria::NOT_EQUAL);
    }

    /**
     * @param SelProductAcquisition $productAcquisition
     * @param SelBranch $branch
     * @return SelProductItemQuery
     */
    static public function assignedsTo($productAcquisition, $branch)
    {
        return (new self())
            ->filterBySelProductAcquisition($productAcquisition)
            ->filterBySelBranch($branch);
    }

    static public function byProduct($productId){
        return self::create()
            ->filterByBranchId(null)
            ->orderByBranchId()
                ->filterByStatus(SelProductItem::IN_ORDER)
                    ->useSelProductAcquisitionQuery()
                ->filterByProductId($productId)
            ->endUse();
    }

    static public function byProductAndBranch($productId,$branchId){
        return self::create()
            ->filterByBranchId($branchId)
            ->orderByBranchId()
            ->filterByStatus(SelProductItem::STOCK)
            ->useSelProductAcquisitionQuery()
                ->filterByProductId($productId)
            ->endUse();
    }
}