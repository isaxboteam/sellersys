<?php
/**
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class SelCategory extends BaseSelCategory
{

    public function preInsert(){
        if($this->main_category_id==''){
            $this->main_category_id=null;
        }
        return true;
    }

    public function subcategories()
    {
        return SelCategoryQuery::create()
                ->filterByMainCategoryId($this->id)
                ->orderByName()
            ->find();
    }

    public function products()
    {
        $c = new Criteria();
        $c->addAscendingOrderByColumn(SelProductPeer::CODE);
        return SelProductQuery::create()
                    ->filterBySelCategory($this)
                    ->orderByName()
                ->find();
    }

    public function main(){
        if($this->isMain()){
            return SelCategoryPeer::retrieveByPK($this->main_category_id);
        }else{
            return null;
        }
    }

    public function isMain(){
        return $this->main_category_id == null;
    }

    public function productItemsByBranch($branchId, $swMain){
        $branch=SelBranchPeer::retrieveByPK($branchId);
        if($swMain==0){
            $products=SelProductQuery::productByCategory($this->id)->find();
        }
		if($swMain==1){
			$products=SelProductQuery::productByMainCategory($this->id)->find();
		}
        $masterProducts=array_map(function($product) use ($branch){
            $count=SelProductItemQuery::productItemByProduct($product,$branch)->count();
            return $count;
        },$products->getArrayCopy());
		
		return array_sum($masterProducts);
    }
}