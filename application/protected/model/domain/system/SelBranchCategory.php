<?php



/**
 * Skeleton subclass for representing a row from the 'sel_branch_category' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class SelBranchCategory extends BaseSelBranchCategory
{
    static $validationRules = array(
        'branchId' => array(
            'null' => false, 'blank'=> false,
        ),
        'categoryId' => array(
            'null' => false, 'blank'=> false,
        ),
        'state' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 20),
        ),
        'observation' => array(
            'null' => true, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 150),
        ),
    );

    public function preValidate($columns = null)
    {
        return $this->preSave();
    }

    public function preSave(PropelPDO $con = null)
    {
        $this->branch_id = $this->branch_id?: null;
        $this->category_id = $this->category_id?: null;
        $this->observation = trim($this->observation) != ''? $this->observation: null;
        return parent::preSave($con);
    }

}
