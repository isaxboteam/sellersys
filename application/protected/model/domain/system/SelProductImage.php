<?php
/**
 *
 * @package    propel.generator.system
 */
class SelProductImage extends BaseSelProductImage
{

    const DIR = 'products';

    const THUMB_SIZE = 100;
    const SMALL_SIZE = 300;
    const MEDIUM_SIZE = 600;
    const LARGE_SIZE = 1000;

    static $validationRules = array(
        'name' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 100),
        ),
        'title' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 1, 'max' => 100),
        ),
        'description' => array(
            'null' => true, 'blank'=> false,
            'size'=> array('min' => 1, 'max' => 2000),
        ),
    );

    public function preValidate($columns = null)
    {
        return $this->preSave();
    }

    public function preSave(PropelPDO $con = null)
    {
        $this->code = strtoupper($this->code);
        $this->description = trim($this->description)?: null;
        return parent::preSave($con);
    }




    final static public function uploadSizes(){
        return array(self::THUMB_SIZE, self::SMALL_SIZE, self::MEDIUM_SIZE, self::LARGE_SIZE);
    }

    public function generatePosition()
    {
        $nExists = SelProductImageQuery::create()
                ->filterByProductId($this->getProductId())
            ->count();
        if($nExists == 0){
            $this->getSelProductRelatedByProductId()
                ->setSelProductImageRelatedByMainImageId($this);
        }
        $this->setPosition($nExists+1);
    }

    public function imageLocation($size = null)
    {
        return self::DIR.'/'.$this->imageName($size);
    }

    public function imageName($size = null)
    {
        return $this->getId().($size? '_'.$size: '').'.'.
            ImageMimeTypes::mimeExtensionFrom($this->getMimetype());
    }

}