<?php
/**
 *
 */
class SelBranch extends BaseSelBranch
{

    static $validationRules = array(
        'name' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 2, 'max' => 30),
        ),
        'address' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 500),
        ),
        'phone' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 20),
        ),
        'fax' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 20),
        ),
        'code' => array(
            'null' => false, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 20),
        ),

    );

    /**
     * @param SelProduct $product
     * @return int
     * @throws PropelException
     */
    public function countStockItems(SelProduct $product)
    {
        return SelProductItemQuery::create()
                ->filterByStatus(SelProductItem::STOCK)
                ->filterBySelBranch($this)
                ->useSelProductAcquisitionQuery()
                    ->filterByProductId($product->getId())
                ->endUse()
            ->count();
    }

    /**
     * @param SelProduct $product
     * @param int $num
     * @return array|mixed|PropelObjectCollection
     * @throws PropelException
     */
    public function stockItems(SelProduct $product, $num = 0)
    {
        return SelProductItemQuery::create()
                ->filterByStatus(SelProductItem::STOCK)
                ->filterBySelBranch($this)
                ->useSelProductAcquisitionQuery()
                    ->filterByProductId($product->getId())
                ->endUse()
                ->_if($num>0)
                    ->limit($num)
                ->_endif()
                ->orderByRegisterDate(Criteria::ASC)
            ->find();
    }

}