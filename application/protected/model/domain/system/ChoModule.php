<?php
class ChoModule extends BaseChoModule
{
    static $validationRules = array(
        'uri' => array(
            'null' => false, 'blank'=> true,
            'size'=> array('max' => 30),
        ),
        'name' => array(
            'null' => false, 'blank'=> true,
            'size'=> array('min' => 3, 'max' => 50),
        ),
        'access' => array(
            'null' => false, 'blank'=> false
        ),
        'description' => array(
            'null' => true,  'blank'=> false,
        ),
    );

    public function save(PropelPDO $con = null)
    {
      $this->uri = trim($this->uri)?: null;
      $this->description = strlen(trim($this->description))>0? $this->description: null;
      parent::save();
    }

}