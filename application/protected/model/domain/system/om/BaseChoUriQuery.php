<?php


/**
 * Base class that represents a query for the 'cho_uri' table.
 *
 * 
 *
 * @method ChoUriQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method ChoUriQuery orderByModuleId($order = Criteria::ASC) Order by the MODULE_ID column
 * @method ChoUriQuery orderByUri($order = Criteria::ASC) Order by the URI column
 * @method ChoUriQuery orderByTitle($order = Criteria::ASC) Order by the TITLE column
 * @method ChoUriQuery orderByAccess($order = Criteria::ASC) Order by the ACCESS column
 * @method ChoUriQuery orderByType($order = Criteria::ASC) Order by the TYPE column
 * @method ChoUriQuery orderByPosition($order = Criteria::ASC) Order by the POSITION column
 * @method ChoUriQuery orderByDescription($order = Criteria::ASC) Order by the DESCRIPTION column
 *
 * @method ChoUriQuery groupById() Group by the ID column
 * @method ChoUriQuery groupByModuleId() Group by the MODULE_ID column
 * @method ChoUriQuery groupByUri() Group by the URI column
 * @method ChoUriQuery groupByTitle() Group by the TITLE column
 * @method ChoUriQuery groupByAccess() Group by the ACCESS column
 * @method ChoUriQuery groupByType() Group by the TYPE column
 * @method ChoUriQuery groupByPosition() Group by the POSITION column
 * @method ChoUriQuery groupByDescription() Group by the DESCRIPTION column
 *
 * @method ChoUriQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ChoUriQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ChoUriQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ChoUriQuery leftJoinChoModule($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChoModule relation
 * @method ChoUriQuery rightJoinChoModule($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChoModule relation
 * @method ChoUriQuery innerJoinChoModule($relationAlias = null) Adds a INNER JOIN clause to the query using the ChoModule relation
 *
 * @method ChoUriQuery leftJoinChoRolXUri($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChoRolXUri relation
 * @method ChoUriQuery rightJoinChoRolXUri($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChoRolXUri relation
 * @method ChoUriQuery innerJoinChoRolXUri($relationAlias = null) Adds a INNER JOIN clause to the query using the ChoRolXUri relation
 *
 * @method ChoUri findOne(PropelPDO $con = null) Return the first ChoUri matching the query
 * @method ChoUri findOneOrCreate(PropelPDO $con = null) Return the first ChoUri matching the query, or a new ChoUri object populated from the query conditions when no match is found
 *
 * @method ChoUri findOneByModuleId(int $MODULE_ID) Return the first ChoUri filtered by the MODULE_ID column
 * @method ChoUri findOneByUri(string $URI) Return the first ChoUri filtered by the URI column
 * @method ChoUri findOneByTitle(string $TITLE) Return the first ChoUri filtered by the TITLE column
 * @method ChoUri findOneByAccess(string $ACCESS) Return the first ChoUri filtered by the ACCESS column
 * @method ChoUri findOneByType(string $TYPE) Return the first ChoUri filtered by the TYPE column
 * @method ChoUri findOneByPosition(int $POSITION) Return the first ChoUri filtered by the POSITION column
 * @method ChoUri findOneByDescription(string $DESCRIPTION) Return the first ChoUri filtered by the DESCRIPTION column
 *
 * @method array findById(int $ID) Return ChoUri objects filtered by the ID column
 * @method array findByModuleId(int $MODULE_ID) Return ChoUri objects filtered by the MODULE_ID column
 * @method array findByUri(string $URI) Return ChoUri objects filtered by the URI column
 * @method array findByTitle(string $TITLE) Return ChoUri objects filtered by the TITLE column
 * @method array findByAccess(string $ACCESS) Return ChoUri objects filtered by the ACCESS column
 * @method array findByType(string $TYPE) Return ChoUri objects filtered by the TYPE column
 * @method array findByPosition(int $POSITION) Return ChoUri objects filtered by the POSITION column
 * @method array findByDescription(string $DESCRIPTION) Return ChoUri objects filtered by the DESCRIPTION column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseChoUriQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseChoUriQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'ChoUri', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChoUriQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     ChoUriQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChoUriQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ChoUriQuery) {
            return $criteria;
        }
        $query = new ChoUriQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ChoUri|ChoUri[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ChoUriPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ChoUriPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   ChoUri A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   ChoUri A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `MODULE_ID`, `URI`, `TITLE`, `ACCESS`, `TYPE`, `POSITION`, `DESCRIPTION` FROM `cho_uri` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ChoUri();
            $obj->hydrate($row);
            ChoUriPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ChoUri|ChoUri[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ChoUri[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ChoUriPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ChoUriPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ChoUriPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the MODULE_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleId(1234); // WHERE MODULE_ID = 1234
     * $query->filterByModuleId(array(12, 34)); // WHERE MODULE_ID IN (12, 34)
     * $query->filterByModuleId(array('min' => 12)); // WHERE MODULE_ID > 12
     * </code>
     *
     * @see       filterByChoModule()
     *
     * @param     mixed $moduleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function filterByModuleId($moduleId = null, $comparison = null)
    {
        if (is_array($moduleId)) {
            $useMinMax = false;
            if (isset($moduleId['min'])) {
                $this->addUsingAlias(ChoUriPeer::MODULE_ID, $moduleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($moduleId['max'])) {
                $this->addUsingAlias(ChoUriPeer::MODULE_ID, $moduleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChoUriPeer::MODULE_ID, $moduleId, $comparison);
    }

    /**
     * Filter the query on the URI column
     *
     * Example usage:
     * <code>
     * $query->filterByUri('fooValue');   // WHERE URI = 'fooValue'
     * $query->filterByUri('%fooValue%'); // WHERE URI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uri The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function filterByUri($uri = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uri)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $uri)) {
                $uri = str_replace('*', '%', $uri);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUriPeer::URI, $uri, $comparison);
    }

    /**
     * Filter the query on the TITLE column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE TITLE = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE TITLE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUriPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the ACCESS column
     *
     * Example usage:
     * <code>
     * $query->filterByAccess('fooValue');   // WHERE ACCESS = 'fooValue'
     * $query->filterByAccess('%fooValue%'); // WHERE ACCESS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $access The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function filterByAccess($access = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($access)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $access)) {
                $access = str_replace('*', '%', $access);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUriPeer::ACCESS, $access, $comparison);
    }

    /**
     * Filter the query on the TYPE column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE TYPE = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE TYPE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUriPeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the POSITION column
     *
     * Example usage:
     * <code>
     * $query->filterByPosition(1234); // WHERE POSITION = 1234
     * $query->filterByPosition(array(12, 34)); // WHERE POSITION IN (12, 34)
     * $query->filterByPosition(array('min' => 12)); // WHERE POSITION > 12
     * </code>
     *
     * @param     mixed $position The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function filterByPosition($position = null, $comparison = null)
    {
        if (is_array($position)) {
            $useMinMax = false;
            if (isset($position['min'])) {
                $this->addUsingAlias(ChoUriPeer::POSITION, $position['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($position['max'])) {
                $this->addUsingAlias(ChoUriPeer::POSITION, $position['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChoUriPeer::POSITION, $position, $comparison);
    }

    /**
     * Filter the query on the DESCRIPTION column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE DESCRIPTION = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE DESCRIPTION LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUriPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query by a related ChoModule object
     *
     * @param   ChoModule|PropelObjectCollection $choModule The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ChoUriQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByChoModule($choModule, $comparison = null)
    {
        if ($choModule instanceof ChoModule) {
            return $this
                ->addUsingAlias(ChoUriPeer::MODULE_ID, $choModule->getId(), $comparison);
        } elseif ($choModule instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChoUriPeer::MODULE_ID, $choModule->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChoModule() only accepts arguments of type ChoModule or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChoModule relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function joinChoModule($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChoModule');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChoModule');
        }

        return $this;
    }

    /**
     * Use the ChoModule relation ChoModule object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ChoModuleQuery A secondary query class using the current class as primary query
     */
    public function useChoModuleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChoModule($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChoModule', 'ChoModuleQuery');
    }

    /**
     * Filter the query by a related ChoRolXUri object
     *
     * @param   ChoRolXUri|PropelObjectCollection $choRolXUri  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ChoUriQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByChoRolXUri($choRolXUri, $comparison = null)
    {
        if ($choRolXUri instanceof ChoRolXUri) {
            return $this
                ->addUsingAlias(ChoUriPeer::ID, $choRolXUri->getUriId(), $comparison);
        } elseif ($choRolXUri instanceof PropelObjectCollection) {
            return $this
                ->useChoRolXUriQuery()
                ->filterByPrimaryKeys($choRolXUri->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChoRolXUri() only accepts arguments of type ChoRolXUri or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChoRolXUri relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function joinChoRolXUri($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChoRolXUri');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChoRolXUri');
        }

        return $this;
    }

    /**
     * Use the ChoRolXUri relation ChoRolXUri object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ChoRolXUriQuery A secondary query class using the current class as primary query
     */
    public function useChoRolXUriQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChoRolXUri($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChoRolXUri', 'ChoRolXUriQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChoUri $choUri Object to remove from the list of results
     *
     * @return ChoUriQuery The current query, for fluid interface
     */
    public function prune($choUri = null)
    {
        if ($choUri) {
            $this->addUsingAlias(ChoUriPeer::ID, $choUri->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
