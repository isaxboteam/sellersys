<?php


/**
 * Base class that represents a row from the 'sel_order' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelOrder extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelOrderPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelOrderPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the customer_id field.
     * @var        int
     */
    protected $customer_id;

    /**
     * The value for the sales_id field.
     * @var        int
     */
    protected $sales_id;

    /**
     * The value for the status field.
     * @var        string
     */
    protected $status;

    /**
     * The value for the shipping_address field.
     * @var        string
     */
    protected $shipping_address;

    /**
     * The value for the phone field.
     * @var        string
     */
    protected $phone;

    /**
     * The value for the custommer_comment field.
     * @var        string
     */
    protected $custommer_comment;

    /**
     * The value for the sales_comment field.
     * @var        int
     */
    protected $sales_comment;

    /**
     * The value for the creation_date field.
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        string
     */
    protected $creation_date;

    /**
     * The value for the order_date field.
     * @var        string
     */
    protected $order_date;

    /**
     * The value for the payment_date field.
     * @var        string
     */
    protected $payment_date;

    /**
     * The value for the payment_code field.
     * @var        string
     */
    protected $payment_code;

    /**
     * The value for the payment_entity field.
     * @var        string
     */
    protected $payment_entity;

    /**
     * The value for the check_date field.
     * @var        string
     */
    protected $check_date;

    /**
     * The value for the approved_date field.
     * @var        string
     */
    protected $approved_date;

    /**
     * The value for the shipping_date field.
     * @var        string
     */
    protected $shipping_date;

    /**
     * The value for the delivery_date field.
     * @var        string
     */
    protected $delivery_date;

    /**
     * @var        SelSale
     */
    protected $aSelSale;

    /**
     * @var        SelCustomer
     */
    protected $aSelCustomer;

    /**
     * @var        PropelObjectCollection|SelProductOrder[] Collection to store aggregation of SelProductOrder objects.
     */
    protected $collSelProductOrders;
    protected $collSelProductOrdersPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductOrdersScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
    }

    /**
     * Initializes internal state of BaseSelOrder object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [customer_id] column value.
     * 
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Get the [sales_id] column value.
     * 
     * @return int
     */
    public function getSalesId()
    {
        return $this->sales_id;
    }

    /**
     * Get the [status] column value.
     * 
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [shipping_address] column value.
     * 
     * @return string
     */
    public function getShippingAddress()
    {
        return $this->shipping_address;
    }

    /**
     * Get the [phone] column value.
     * 
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get the [custommer_comment] column value.
     * 
     * @return string
     */
    public function getCustommerComment()
    {
        return $this->custommer_comment;
    }

    /**
     * Get the [sales_comment] column value.
     * 
     * @return int
     */
    public function getSalesComment()
    {
        return $this->sales_comment;
    }

    /**
     * Get the [optionally formatted] temporal [creation_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreationDate($format = 'Y-m-d H:i:s')
    {
        if ($this->creation_date === null) {
            return null;
        }

        if ($this->creation_date === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->creation_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->creation_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [order_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getOrderDate($format = 'Y-m-d H:i:s')
    {
        if ($this->order_date === null) {
            return null;
        }

        if ($this->order_date === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->order_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->order_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [payment_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPaymentDate($format = 'Y-m-d H:i:s')
    {
        if ($this->payment_date === null) {
            return null;
        }

        if ($this->payment_date === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->payment_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->payment_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [payment_code] column value.
     * 
     * @return string
     */
    public function getPaymentCode()
    {
        return $this->payment_code;
    }

    /**
     * Get the [payment_entity] column value.
     * 
     * @return string
     */
    public function getPaymentEntity()
    {
        return $this->payment_entity;
    }

    /**
     * Get the [optionally formatted] temporal [check_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCheckDate($format = 'Y-m-d H:i:s')
    {
        if ($this->check_date === null) {
            return null;
        }

        if ($this->check_date === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->check_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->check_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [approved_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getApprovedDate($format = 'Y-m-d H:i:s')
    {
        if ($this->approved_date === null) {
            return null;
        }

        if ($this->approved_date === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->approved_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->approved_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [shipping_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getShippingDate($format = 'Y-m-d H:i:s')
    {
        if ($this->shipping_date === null) {
            return null;
        }

        if ($this->shipping_date === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->shipping_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->shipping_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [delivery_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeliveryDate($format = 'Y-m-d H:i:s')
    {
        if ($this->delivery_date === null) {
            return null;
        }

        if ($this->delivery_date === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->delivery_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->delivery_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelOrder The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelOrderPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [customer_id] column.
     * 
     * @param int $v new value
     * @return SelOrder The current object (for fluent API support)
     */
    public function setCustomerId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->customer_id !== $v) {
            $this->customer_id = $v;
            $this->modifiedColumns[] = SelOrderPeer::CUSTOMER_ID;
        }

        if ($this->aSelCustomer !== null && $this->aSelCustomer->getId() !== $v) {
            $this->aSelCustomer = null;
        }


        return $this;
    } // setCustomerId()

    /**
     * Set the value of [sales_id] column.
     * 
     * @param int $v new value
     * @return SelOrder The current object (for fluent API support)
     */
    public function setSalesId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sales_id !== $v) {
            $this->sales_id = $v;
            $this->modifiedColumns[] = SelOrderPeer::SALES_ID;
        }

        if ($this->aSelSale !== null && $this->aSelSale->getId() !== $v) {
            $this->aSelSale = null;
        }


        return $this;
    } // setSalesId()

    /**
     * Set the value of [status] column.
     * 
     * @param string $v new value
     * @return SelOrder The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[] = SelOrderPeer::STATUS;
        }


        return $this;
    } // setStatus()

    /**
     * Set the value of [shipping_address] column.
     * 
     * @param string $v new value
     * @return SelOrder The current object (for fluent API support)
     */
    public function setShippingAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shipping_address !== $v) {
            $this->shipping_address = $v;
            $this->modifiedColumns[] = SelOrderPeer::SHIPPING_ADDRESS;
        }


        return $this;
    } // setShippingAddress()

    /**
     * Set the value of [phone] column.
     * 
     * @param string $v new value
     * @return SelOrder The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[] = SelOrderPeer::PHONE;
        }


        return $this;
    } // setPhone()

    /**
     * Set the value of [custommer_comment] column.
     * 
     * @param string $v new value
     * @return SelOrder The current object (for fluent API support)
     */
    public function setCustommerComment($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->custommer_comment !== $v) {
            $this->custommer_comment = $v;
            $this->modifiedColumns[] = SelOrderPeer::CUSTOMMER_COMMENT;
        }


        return $this;
    } // setCustommerComment()

    /**
     * Set the value of [sales_comment] column.
     * 
     * @param int $v new value
     * @return SelOrder The current object (for fluent API support)
     */
    public function setSalesComment($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sales_comment !== $v) {
            $this->sales_comment = $v;
            $this->modifiedColumns[] = SelOrderPeer::SALES_COMMENT;
        }


        return $this;
    } // setSalesComment()

    /**
     * Sets the value of [creation_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelOrder The current object (for fluent API support)
     */
    public function setCreationDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->creation_date !== null || $dt !== null) {
            $currentDateAsString = ($this->creation_date !== null && $tmpDt = new DateTime($this->creation_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->creation_date = $newDateAsString;
                $this->modifiedColumns[] = SelOrderPeer::CREATION_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreationDate()

    /**
     * Sets the value of [order_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelOrder The current object (for fluent API support)
     */
    public function setOrderDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->order_date !== null || $dt !== null) {
            $currentDateAsString = ($this->order_date !== null && $tmpDt = new DateTime($this->order_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->order_date = $newDateAsString;
                $this->modifiedColumns[] = SelOrderPeer::ORDER_DATE;
            }
        } // if either are not null


        return $this;
    } // setOrderDate()

    /**
     * Sets the value of [payment_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelOrder The current object (for fluent API support)
     */
    public function setPaymentDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->payment_date !== null || $dt !== null) {
            $currentDateAsString = ($this->payment_date !== null && $tmpDt = new DateTime($this->payment_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->payment_date = $newDateAsString;
                $this->modifiedColumns[] = SelOrderPeer::PAYMENT_DATE;
            }
        } // if either are not null


        return $this;
    } // setPaymentDate()

    /**
     * Set the value of [payment_code] column.
     * 
     * @param string $v new value
     * @return SelOrder The current object (for fluent API support)
     */
    public function setPaymentCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->payment_code !== $v) {
            $this->payment_code = $v;
            $this->modifiedColumns[] = SelOrderPeer::PAYMENT_CODE;
        }


        return $this;
    } // setPaymentCode()

    /**
     * Set the value of [payment_entity] column.
     * 
     * @param string $v new value
     * @return SelOrder The current object (for fluent API support)
     */
    public function setPaymentEntity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->payment_entity !== $v) {
            $this->payment_entity = $v;
            $this->modifiedColumns[] = SelOrderPeer::PAYMENT_ENTITY;
        }


        return $this;
    } // setPaymentEntity()

    /**
     * Sets the value of [check_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelOrder The current object (for fluent API support)
     */
    public function setCheckDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->check_date !== null || $dt !== null) {
            $currentDateAsString = ($this->check_date !== null && $tmpDt = new DateTime($this->check_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->check_date = $newDateAsString;
                $this->modifiedColumns[] = SelOrderPeer::CHECK_DATE;
            }
        } // if either are not null


        return $this;
    } // setCheckDate()

    /**
     * Sets the value of [approved_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelOrder The current object (for fluent API support)
     */
    public function setApprovedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->approved_date !== null || $dt !== null) {
            $currentDateAsString = ($this->approved_date !== null && $tmpDt = new DateTime($this->approved_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->approved_date = $newDateAsString;
                $this->modifiedColumns[] = SelOrderPeer::APPROVED_DATE;
            }
        } // if either are not null


        return $this;
    } // setApprovedDate()

    /**
     * Sets the value of [shipping_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelOrder The current object (for fluent API support)
     */
    public function setShippingDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->shipping_date !== null || $dt !== null) {
            $currentDateAsString = ($this->shipping_date !== null && $tmpDt = new DateTime($this->shipping_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->shipping_date = $newDateAsString;
                $this->modifiedColumns[] = SelOrderPeer::SHIPPING_DATE;
            }
        } // if either are not null


        return $this;
    } // setShippingDate()

    /**
     * Sets the value of [delivery_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelOrder The current object (for fluent API support)
     */
    public function setDeliveryDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->delivery_date !== null || $dt !== null) {
            $currentDateAsString = ($this->delivery_date !== null && $tmpDt = new DateTime($this->delivery_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->delivery_date = $newDateAsString;
                $this->modifiedColumns[] = SelOrderPeer::DELIVERY_DATE;
            }
        } // if either are not null


        return $this;
    } // setDeliveryDate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->customer_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->sales_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->status = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->shipping_address = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->phone = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->custommer_comment = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->sales_comment = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->creation_date = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->order_date = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->payment_date = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->payment_code = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->payment_entity = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->check_date = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->approved_date = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->shipping_date = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->delivery_date = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 17; // 17 = SelOrderPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelOrder object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSelCustomer !== null && $this->customer_id !== $this->aSelCustomer->getId()) {
            $this->aSelCustomer = null;
        }
        if ($this->aSelSale !== null && $this->sales_id !== $this->aSelSale->getId()) {
            $this->aSelSale = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelOrderPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSelSale = null;
            $this->aSelCustomer = null;
            $this->collSelProductOrders = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelOrderQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelOrderPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelSale !== null) {
                if ($this->aSelSale->isModified() || $this->aSelSale->isNew()) {
                    $affectedRows += $this->aSelSale->save($con);
                }
                $this->setSelSale($this->aSelSale);
            }

            if ($this->aSelCustomer !== null) {
                if ($this->aSelCustomer->isModified() || $this->aSelCustomer->isNew()) {
                    $affectedRows += $this->aSelCustomer->save($con);
                }
                $this->setSelCustomer($this->aSelCustomer);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->selProductOrdersScheduledForDeletion !== null) {
                if (!$this->selProductOrdersScheduledForDeletion->isEmpty()) {
                    SelProductOrderQuery::create()
                        ->filterByPrimaryKeys($this->selProductOrdersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selProductOrdersScheduledForDeletion = null;
                }
            }

            if ($this->collSelProductOrders !== null) {
                foreach ($this->collSelProductOrders as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SelOrderPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SelOrderPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelOrderPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelOrderPeer::CUSTOMER_ID)) {
            $modifiedColumns[':p' . $index++]  = '`CUSTOMER_ID`';
        }
        if ($this->isColumnModified(SelOrderPeer::SALES_ID)) {
            $modifiedColumns[':p' . $index++]  = '`SALES_ID`';
        }
        if ($this->isColumnModified(SelOrderPeer::STATUS)) {
            $modifiedColumns[':p' . $index++]  = '`STATUS`';
        }
        if ($this->isColumnModified(SelOrderPeer::SHIPPING_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`SHIPPING_ADDRESS`';
        }
        if ($this->isColumnModified(SelOrderPeer::PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`PHONE`';
        }
        if ($this->isColumnModified(SelOrderPeer::CUSTOMMER_COMMENT)) {
            $modifiedColumns[':p' . $index++]  = '`CUSTOMMER_COMMENT`';
        }
        if ($this->isColumnModified(SelOrderPeer::SALES_COMMENT)) {
            $modifiedColumns[':p' . $index++]  = '`SALES_COMMENT`';
        }
        if ($this->isColumnModified(SelOrderPeer::CREATION_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`CREATION_DATE`';
        }
        if ($this->isColumnModified(SelOrderPeer::ORDER_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`ORDER_DATE`';
        }
        if ($this->isColumnModified(SelOrderPeer::PAYMENT_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`PAYMENT_DATE`';
        }
        if ($this->isColumnModified(SelOrderPeer::PAYMENT_CODE)) {
            $modifiedColumns[':p' . $index++]  = '`PAYMENT_CODE`';
        }
        if ($this->isColumnModified(SelOrderPeer::PAYMENT_ENTITY)) {
            $modifiedColumns[':p' . $index++]  = '`PAYMENT_ENTITY`';
        }
        if ($this->isColumnModified(SelOrderPeer::CHECK_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`CHECK_DATE`';
        }
        if ($this->isColumnModified(SelOrderPeer::APPROVED_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`APPROVED_DATE`';
        }
        if ($this->isColumnModified(SelOrderPeer::SHIPPING_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`SHIPPING_DATE`';
        }
        if ($this->isColumnModified(SelOrderPeer::DELIVERY_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`DELIVERY_DATE`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_order` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`CUSTOMER_ID`':						
                        $stmt->bindValue($identifier, $this->customer_id, PDO::PARAM_INT);
                        break;
                    case '`SALES_ID`':						
                        $stmt->bindValue($identifier, $this->sales_id, PDO::PARAM_INT);
                        break;
                    case '`STATUS`':						
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_STR);
                        break;
                    case '`SHIPPING_ADDRESS`':						
                        $stmt->bindValue($identifier, $this->shipping_address, PDO::PARAM_STR);
                        break;
                    case '`PHONE`':						
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case '`CUSTOMMER_COMMENT`':						
                        $stmt->bindValue($identifier, $this->custommer_comment, PDO::PARAM_STR);
                        break;
                    case '`SALES_COMMENT`':						
                        $stmt->bindValue($identifier, $this->sales_comment, PDO::PARAM_INT);
                        break;
                    case '`CREATION_DATE`':						
                        $stmt->bindValue($identifier, $this->creation_date, PDO::PARAM_STR);
                        break;
                    case '`ORDER_DATE`':						
                        $stmt->bindValue($identifier, $this->order_date, PDO::PARAM_STR);
                        break;
                    case '`PAYMENT_DATE`':						
                        $stmt->bindValue($identifier, $this->payment_date, PDO::PARAM_STR);
                        break;
                    case '`PAYMENT_CODE`':						
                        $stmt->bindValue($identifier, $this->payment_code, PDO::PARAM_STR);
                        break;
                    case '`PAYMENT_ENTITY`':						
                        $stmt->bindValue($identifier, $this->payment_entity, PDO::PARAM_STR);
                        break;
                    case '`CHECK_DATE`':						
                        $stmt->bindValue($identifier, $this->check_date, PDO::PARAM_STR);
                        break;
                    case '`APPROVED_DATE`':						
                        $stmt->bindValue($identifier, $this->approved_date, PDO::PARAM_STR);
                        break;
                    case '`SHIPPING_DATE`':						
                        $stmt->bindValue($identifier, $this->shipping_date, PDO::PARAM_STR);
                        break;
                    case '`DELIVERY_DATE`':						
                        $stmt->bindValue($identifier, $this->delivery_date, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelSale !== null) {
                if (!$this->aSelSale->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelSale->getValidationFailures());
                }
            }

            if ($this->aSelCustomer !== null) {
                if (!$this->aSelCustomer->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelCustomer->getValidationFailures());
                }
            }

            if (($retval = SelOrderPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelProductOrders !== null) {
                    foreach ($this->collSelProductOrders as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelOrderPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getCustomerId();
                break;
            case 2:
                return $this->getSalesId();
                break;
            case 3:
                return $this->getStatus();
                break;
            case 4:
                return $this->getShippingAddress();
                break;
            case 5:
                return $this->getPhone();
                break;
            case 6:
                return $this->getCustommerComment();
                break;
            case 7:
                return $this->getSalesComment();
                break;
            case 8:
                return $this->getCreationDate();
                break;
            case 9:
                return $this->getOrderDate();
                break;
            case 10:
                return $this->getPaymentDate();
                break;
            case 11:
                return $this->getPaymentCode();
                break;
            case 12:
                return $this->getPaymentEntity();
                break;
            case 13:
                return $this->getCheckDate();
                break;
            case 14:
                return $this->getApprovedDate();
                break;
            case 15:
                return $this->getShippingDate();
                break;
            case 16:
                return $this->getDeliveryDate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelOrder'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelOrder'][$this->getPrimaryKey()] = true;
        $keys = SelOrderPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getCustomerId(),
            $keys[2] => $this->getSalesId(),
            $keys[3] => $this->getStatus(),
            $keys[4] => $this->getShippingAddress(),
            $keys[5] => $this->getPhone(),
            $keys[6] => $this->getCustommerComment(),
            $keys[7] => $this->getSalesComment(),
            $keys[8] => $this->getCreationDate(),
            $keys[9] => $this->getOrderDate(),
            $keys[10] => $this->getPaymentDate(),
            $keys[11] => $this->getPaymentCode(),
            $keys[12] => $this->getPaymentEntity(),
            $keys[13] => $this->getCheckDate(),
            $keys[14] => $this->getApprovedDate(),
            $keys[15] => $this->getShippingDate(),
            $keys[16] => $this->getDeliveryDate(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSelSale) {
                $result['SelSale'] = $this->aSelSale->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSelCustomer) {
                $result['SelCustomer'] = $this->aSelCustomer->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSelProductOrders) {
                $result['SelProductOrders'] = $this->collSelProductOrders->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelOrderPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setCustomerId($value);
                break;
            case 2:
                $this->setSalesId($value);
                break;
            case 3:
                $this->setStatus($value);
                break;
            case 4:
                $this->setShippingAddress($value);
                break;
            case 5:
                $this->setPhone($value);
                break;
            case 6:
                $this->setCustommerComment($value);
                break;
            case 7:
                $this->setSalesComment($value);
                break;
            case 8:
                $this->setCreationDate($value);
                break;
            case 9:
                $this->setOrderDate($value);
                break;
            case 10:
                $this->setPaymentDate($value);
                break;
            case 11:
                $this->setPaymentCode($value);
                break;
            case 12:
                $this->setPaymentEntity($value);
                break;
            case 13:
                $this->setCheckDate($value);
                break;
            case 14:
                $this->setApprovedDate($value);
                break;
            case 15:
                $this->setShippingDate($value);
                break;
            case 16:
                $this->setDeliveryDate($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelOrderPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setCustomerId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSalesId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setStatus($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setShippingAddress($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPhone($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCustommerComment($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setSalesComment($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setCreationDate($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setOrderDate($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setPaymentDate($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setPaymentCode($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setPaymentEntity($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setCheckDate($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setApprovedDate($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setShippingDate($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setDeliveryDate($arr[$keys[16]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelOrderPeer::DATABASE_NAME);

        if ($this->isColumnModified(SelOrderPeer::ID)) $criteria->add(SelOrderPeer::ID, $this->id);
        if ($this->isColumnModified(SelOrderPeer::CUSTOMER_ID)) $criteria->add(SelOrderPeer::CUSTOMER_ID, $this->customer_id);
        if ($this->isColumnModified(SelOrderPeer::SALES_ID)) $criteria->add(SelOrderPeer::SALES_ID, $this->sales_id);
        if ($this->isColumnModified(SelOrderPeer::STATUS)) $criteria->add(SelOrderPeer::STATUS, $this->status);
        if ($this->isColumnModified(SelOrderPeer::SHIPPING_ADDRESS)) $criteria->add(SelOrderPeer::SHIPPING_ADDRESS, $this->shipping_address);
        if ($this->isColumnModified(SelOrderPeer::PHONE)) $criteria->add(SelOrderPeer::PHONE, $this->phone);
        if ($this->isColumnModified(SelOrderPeer::CUSTOMMER_COMMENT)) $criteria->add(SelOrderPeer::CUSTOMMER_COMMENT, $this->custommer_comment);
        if ($this->isColumnModified(SelOrderPeer::SALES_COMMENT)) $criteria->add(SelOrderPeer::SALES_COMMENT, $this->sales_comment);
        if ($this->isColumnModified(SelOrderPeer::CREATION_DATE)) $criteria->add(SelOrderPeer::CREATION_DATE, $this->creation_date);
        if ($this->isColumnModified(SelOrderPeer::ORDER_DATE)) $criteria->add(SelOrderPeer::ORDER_DATE, $this->order_date);
        if ($this->isColumnModified(SelOrderPeer::PAYMENT_DATE)) $criteria->add(SelOrderPeer::PAYMENT_DATE, $this->payment_date);
        if ($this->isColumnModified(SelOrderPeer::PAYMENT_CODE)) $criteria->add(SelOrderPeer::PAYMENT_CODE, $this->payment_code);
        if ($this->isColumnModified(SelOrderPeer::PAYMENT_ENTITY)) $criteria->add(SelOrderPeer::PAYMENT_ENTITY, $this->payment_entity);
        if ($this->isColumnModified(SelOrderPeer::CHECK_DATE)) $criteria->add(SelOrderPeer::CHECK_DATE, $this->check_date);
        if ($this->isColumnModified(SelOrderPeer::APPROVED_DATE)) $criteria->add(SelOrderPeer::APPROVED_DATE, $this->approved_date);
        if ($this->isColumnModified(SelOrderPeer::SHIPPING_DATE)) $criteria->add(SelOrderPeer::SHIPPING_DATE, $this->shipping_date);
        if ($this->isColumnModified(SelOrderPeer::DELIVERY_DATE)) $criteria->add(SelOrderPeer::DELIVERY_DATE, $this->delivery_date);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelOrderPeer::DATABASE_NAME);
        $criteria->add(SelOrderPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelOrder (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setCustomerId($this->getCustomerId());
        $copyObj->setSalesId($this->getSalesId());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setShippingAddress($this->getShippingAddress());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setCustommerComment($this->getCustommerComment());
        $copyObj->setSalesComment($this->getSalesComment());
        $copyObj->setCreationDate($this->getCreationDate());
        $copyObj->setOrderDate($this->getOrderDate());
        $copyObj->setPaymentDate($this->getPaymentDate());
        $copyObj->setPaymentCode($this->getPaymentCode());
        $copyObj->setPaymentEntity($this->getPaymentEntity());
        $copyObj->setCheckDate($this->getCheckDate());
        $copyObj->setApprovedDate($this->getApprovedDate());
        $copyObj->setShippingDate($this->getShippingDate());
        $copyObj->setDeliveryDate($this->getDeliveryDate());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelProductOrders() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProductOrder($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelOrder Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelOrderPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelOrderPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SelSale object.
     *
     * @param             SelSale $v
     * @return SelOrder The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelSale(SelSale $v = null)
    {
        if ($v === null) {
            $this->setSalesId(NULL);
        } else {
            $this->setSalesId($v->getId());
        }

        $this->aSelSale = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelSale object, it will not be re-added.
        if ($v !== null) {
            $v->addSelOrder($this);
        }


        return $this;
    }


    /**
     * Get the associated SelSale object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelSale The associated SelSale object.
     * @throws PropelException
     */
    public function getSelSale(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelSale === null && ($this->sales_id !== null) && $doQuery) {
            $this->aSelSale = SelSaleQuery::create()->findPk($this->sales_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelSale->addSelOrders($this);
             */
        }

        return $this->aSelSale;
    }

    /**
     * Declares an association between this object and a SelCustomer object.
     *
     * @param             SelCustomer $v
     * @return SelOrder The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelCustomer(SelCustomer $v = null)
    {
        if ($v === null) {
            $this->setCustomerId(NULL);
        } else {
            $this->setCustomerId($v->getId());
        }

        $this->aSelCustomer = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelCustomer object, it will not be re-added.
        if ($v !== null) {
            $v->addSelOrder($this);
        }


        return $this;
    }


    /**
     * Get the associated SelCustomer object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelCustomer The associated SelCustomer object.
     * @throws PropelException
     */
    public function getSelCustomer(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelCustomer === null && ($this->customer_id !== null) && $doQuery) {
            $this->aSelCustomer = SelCustomerQuery::create()->findPk($this->customer_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelCustomer->addSelOrders($this);
             */
        }

        return $this->aSelCustomer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelProductOrder' == $relationName) {
            $this->initSelProductOrders();
        }
    }

    /**
     * Clears out the collSelProductOrders collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelOrder The current object (for fluent API support)
     * @see        addSelProductOrders()
     */
    public function clearSelProductOrders()
    {
        $this->collSelProductOrders = null; // important to set this to null since that means it is uninitialized
        $this->collSelProductOrdersPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProductOrders collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProductOrders($v = true)
    {
        $this->collSelProductOrdersPartial = $v;
    }

    /**
     * Initializes the collSelProductOrders collection.
     *
     * By default this just sets the collSelProductOrders collection to an empty array (like clearcollSelProductOrders());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProductOrders($overrideExisting = true)
    {
        if (null !== $this->collSelProductOrders && !$overrideExisting) {
            return;
        }
        $this->collSelProductOrders = new PropelObjectCollection();
        $this->collSelProductOrders->setModel('SelProductOrder');
    }

    /**
     * Gets an array of SelProductOrder objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelOrder is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProductOrder[] List of SelProductOrder objects
     * @throws PropelException
     */
    public function getSelProductOrders($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProductOrdersPartial && !$this->isNew();
        if (null === $this->collSelProductOrders || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProductOrders) {
                // return empty collection
                $this->initSelProductOrders();
            } else {
                $collSelProductOrders = SelProductOrderQuery::create(null, $criteria)
                    ->filterBySelOrder($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProductOrdersPartial && count($collSelProductOrders)) {
                      $this->initSelProductOrders(false);

                      foreach($collSelProductOrders as $obj) {
                        if (false == $this->collSelProductOrders->contains($obj)) {
                          $this->collSelProductOrders->append($obj);
                        }
                      }

                      $this->collSelProductOrdersPartial = true;
                    }

                    return $collSelProductOrders;
                }

                if($partial && $this->collSelProductOrders) {
                    foreach($this->collSelProductOrders as $obj) {
                        if($obj->isNew()) {
                            $collSelProductOrders[] = $obj;
                        }
                    }
                }

                $this->collSelProductOrders = $collSelProductOrders;
                $this->collSelProductOrdersPartial = false;
            }
        }

        return $this->collSelProductOrders;
    }

    /**
     * Sets a collection of SelProductOrder objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProductOrders A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelOrder The current object (for fluent API support)
     */
    public function setSelProductOrders(PropelCollection $selProductOrders, PropelPDO $con = null)
    {
        $selProductOrdersToDelete = $this->getSelProductOrders(new Criteria(), $con)->diff($selProductOrders);

        $this->selProductOrdersScheduledForDeletion = unserialize(serialize($selProductOrdersToDelete));

        foreach ($selProductOrdersToDelete as $selProductOrderRemoved) {
            $selProductOrderRemoved->setSelOrder(null);
        }

        $this->collSelProductOrders = null;
        foreach ($selProductOrders as $selProductOrder) {
            $this->addSelProductOrder($selProductOrder);
        }

        $this->collSelProductOrders = $selProductOrders;
        $this->collSelProductOrdersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProductOrder objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProductOrder objects.
     * @throws PropelException
     */
    public function countSelProductOrders(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProductOrdersPartial && !$this->isNew();
        if (null === $this->collSelProductOrders || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProductOrders) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProductOrders());
            }
            $query = SelProductOrderQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelOrder($this)
                ->count($con);
        }

        return count($this->collSelProductOrders);
    }

    /**
     * Method called to associate a SelProductOrder object to this object
     * through the SelProductOrder foreign key attribute.
     *
     * @param    SelProductOrder $l SelProductOrder
     * @return SelOrder The current object (for fluent API support)
     */
    public function addSelProductOrder(SelProductOrder $l)
    {
        if ($this->collSelProductOrders === null) {
            $this->initSelProductOrders();
            $this->collSelProductOrdersPartial = true;
        }
        if (!in_array($l, $this->collSelProductOrders->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProductOrder($l);
        }

        return $this;
    }

    /**
     * @param	SelProductOrder $selProductOrder The selProductOrder object to add.
     */
    protected function doAddSelProductOrder($selProductOrder)
    {
        $this->collSelProductOrders[]= $selProductOrder;
        $selProductOrder->setSelOrder($this);
    }

    /**
     * @param	SelProductOrder $selProductOrder The selProductOrder object to remove.
     * @return SelOrder The current object (for fluent API support)
     */
    public function removeSelProductOrder($selProductOrder)
    {
        if ($this->getSelProductOrders()->contains($selProductOrder)) {
            $this->collSelProductOrders->remove($this->collSelProductOrders->search($selProductOrder));
            if (null === $this->selProductOrdersScheduledForDeletion) {
                $this->selProductOrdersScheduledForDeletion = clone $this->collSelProductOrders;
                $this->selProductOrdersScheduledForDeletion->clear();
            }
            $this->selProductOrdersScheduledForDeletion[]= clone $selProductOrder;
            $selProductOrder->setSelOrder(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelOrder is new, it will return
     * an empty collection; or if this SelOrder has previously
     * been saved, it will retrieve related SelProductOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelOrder.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProductOrder[] List of SelProductOrder objects
     */
    public function getSelProductOrdersJoinSelProduct($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductOrderQuery::create(null, $criteria);
        $query->joinWith('SelProduct', $join_behavior);

        return $this->getSelProductOrders($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->customer_id = null;
        $this->sales_id = null;
        $this->status = null;
        $this->shipping_address = null;
        $this->phone = null;
        $this->custommer_comment = null;
        $this->sales_comment = null;
        $this->creation_date = null;
        $this->order_date = null;
        $this->payment_date = null;
        $this->payment_code = null;
        $this->payment_entity = null;
        $this->check_date = null;
        $this->approved_date = null;
        $this->shipping_date = null;
        $this->delivery_date = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSelProductOrders) {
                foreach ($this->collSelProductOrders as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collSelProductOrders instanceof PropelCollection) {
            $this->collSelProductOrders->clearIterator();
        }
        $this->collSelProductOrders = null;
        $this->aSelSale = null;
        $this->aSelCustomer = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelOrderPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
