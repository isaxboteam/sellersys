<?php


/**
 * Base class that represents a query for the 'cho_user' table.
 *
 * 
 *
 * @method ChoUserQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method ChoUserQuery orderByRolId($order = Criteria::ASC) Order by the ROL_ID column
 * @method ChoUserQuery orderByFirstLastname($order = Criteria::ASC) Order by the FIRST_LASTNAME column
 * @method ChoUserQuery orderBySecondLastname($order = Criteria::ASC) Order by the SECOND_LASTNAME column
 * @method ChoUserQuery orderByFirstName($order = Criteria::ASC) Order by the FIRST_NAME column
 * @method ChoUserQuery orderBySecondName($order = Criteria::ASC) Order by the SECOND_NAME column
 * @method ChoUserQuery orderByUsername($order = Criteria::ASC) Order by the USERNAME column
 * @method ChoUserQuery orderByPassword($order = Criteria::ASC) Order by the PASSWORD column
 * @method ChoUserQuery orderByEmail($order = Criteria::ASC) Order by the EMAIL column
 * @method ChoUserQuery orderByDni($order = Criteria::ASC) Order by the DNI column
 * @method ChoUserQuery orderByGender($order = Criteria::ASC) Order by the GENDER column
 * @method ChoUserQuery orderByBirthday($order = Criteria::ASC) Order by the BIRTHDAY column
 * @method ChoUserQuery orderByType($order = Criteria::ASC) Order by the TYPE column
 * @method ChoUserQuery orderByStatus($order = Criteria::ASC) Order by the STATUS column
 * @method ChoUserQuery orderByCountry($order = Criteria::ASC) Order by the COUNTRY column
 * @method ChoUserQuery orderByState($order = Criteria::ASC) Order by the STATE column
 * @method ChoUserQuery orderByCity($order = Criteria::ASC) Order by the CITY column
 * @method ChoUserQuery orderByAddress($order = Criteria::ASC) Order by the ADDRESS column
 * @method ChoUserQuery orderByZip($order = Criteria::ASC) Order by the ZIP column
 * @method ChoUserQuery orderByPrimaryPhone($order = Criteria::ASC) Order by the PRIMARY_PHONE column
 * @method ChoUserQuery orderBySecondaryPhone($order = Criteria::ASC) Order by the SECONDARY_PHONE column
 * @method ChoUserQuery orderByCellPhone($order = Criteria::ASC) Order by the CELL_PHONE column
 * @method ChoUserQuery orderByPhotoFormat($order = Criteria::ASC) Order by the PHOTO_FORMAT column
 * @method ChoUserQuery orderByRegisteredDate($order = Criteria::ASC) Order by the REGISTERED_DATE column
 * @method ChoUserQuery orderByLastAccess($order = Criteria::ASC) Order by the LAST_ACCESS column
 *
 * @method ChoUserQuery groupById() Group by the ID column
 * @method ChoUserQuery groupByRolId() Group by the ROL_ID column
 * @method ChoUserQuery groupByFirstLastname() Group by the FIRST_LASTNAME column
 * @method ChoUserQuery groupBySecondLastname() Group by the SECOND_LASTNAME column
 * @method ChoUserQuery groupByFirstName() Group by the FIRST_NAME column
 * @method ChoUserQuery groupBySecondName() Group by the SECOND_NAME column
 * @method ChoUserQuery groupByUsername() Group by the USERNAME column
 * @method ChoUserQuery groupByPassword() Group by the PASSWORD column
 * @method ChoUserQuery groupByEmail() Group by the EMAIL column
 * @method ChoUserQuery groupByDni() Group by the DNI column
 * @method ChoUserQuery groupByGender() Group by the GENDER column
 * @method ChoUserQuery groupByBirthday() Group by the BIRTHDAY column
 * @method ChoUserQuery groupByType() Group by the TYPE column
 * @method ChoUserQuery groupByStatus() Group by the STATUS column
 * @method ChoUserQuery groupByCountry() Group by the COUNTRY column
 * @method ChoUserQuery groupByState() Group by the STATE column
 * @method ChoUserQuery groupByCity() Group by the CITY column
 * @method ChoUserQuery groupByAddress() Group by the ADDRESS column
 * @method ChoUserQuery groupByZip() Group by the ZIP column
 * @method ChoUserQuery groupByPrimaryPhone() Group by the PRIMARY_PHONE column
 * @method ChoUserQuery groupBySecondaryPhone() Group by the SECONDARY_PHONE column
 * @method ChoUserQuery groupByCellPhone() Group by the CELL_PHONE column
 * @method ChoUserQuery groupByPhotoFormat() Group by the PHOTO_FORMAT column
 * @method ChoUserQuery groupByRegisteredDate() Group by the REGISTERED_DATE column
 * @method ChoUserQuery groupByLastAccess() Group by the LAST_ACCESS column
 *
 * @method ChoUserQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ChoUserQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ChoUserQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ChoUserQuery leftJoinChoUserXRol($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChoUserXRol relation
 * @method ChoUserQuery rightJoinChoUserXRol($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChoUserXRol relation
 * @method ChoUserQuery innerJoinChoUserXRol($relationAlias = null) Adds a INNER JOIN clause to the query using the ChoUserXRol relation
 *
 * @method ChoUserQuery leftJoinSelEmployee($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelEmployee relation
 * @method ChoUserQuery rightJoinSelEmployee($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelEmployee relation
 * @method ChoUserQuery innerJoinSelEmployee($relationAlias = null) Adds a INNER JOIN clause to the query using the SelEmployee relation
 *
 * @method ChoUser findOne(PropelPDO $con = null) Return the first ChoUser matching the query
 * @method ChoUser findOneOrCreate(PropelPDO $con = null) Return the first ChoUser matching the query, or a new ChoUser object populated from the query conditions when no match is found
 *
 * @method ChoUser findOneByRolId(int $ROL_ID) Return the first ChoUser filtered by the ROL_ID column
 * @method ChoUser findOneByFirstLastname(string $FIRST_LASTNAME) Return the first ChoUser filtered by the FIRST_LASTNAME column
 * @method ChoUser findOneBySecondLastname(string $SECOND_LASTNAME) Return the first ChoUser filtered by the SECOND_LASTNAME column
 * @method ChoUser findOneByFirstName(string $FIRST_NAME) Return the first ChoUser filtered by the FIRST_NAME column
 * @method ChoUser findOneBySecondName(string $SECOND_NAME) Return the first ChoUser filtered by the SECOND_NAME column
 * @method ChoUser findOneByUsername(string $USERNAME) Return the first ChoUser filtered by the USERNAME column
 * @method ChoUser findOneByPassword(string $PASSWORD) Return the first ChoUser filtered by the PASSWORD column
 * @method ChoUser findOneByEmail(string $EMAIL) Return the first ChoUser filtered by the EMAIL column
 * @method ChoUser findOneByDni(string $DNI) Return the first ChoUser filtered by the DNI column
 * @method ChoUser findOneByGender(string $GENDER) Return the first ChoUser filtered by the GENDER column
 * @method ChoUser findOneByBirthday(string $BIRTHDAY) Return the first ChoUser filtered by the BIRTHDAY column
 * @method ChoUser findOneByType(string $TYPE) Return the first ChoUser filtered by the TYPE column
 * @method ChoUser findOneByStatus(string $STATUS) Return the first ChoUser filtered by the STATUS column
 * @method ChoUser findOneByCountry(string $COUNTRY) Return the first ChoUser filtered by the COUNTRY column
 * @method ChoUser findOneByState(string $STATE) Return the first ChoUser filtered by the STATE column
 * @method ChoUser findOneByCity(string $CITY) Return the first ChoUser filtered by the CITY column
 * @method ChoUser findOneByAddress(string $ADDRESS) Return the first ChoUser filtered by the ADDRESS column
 * @method ChoUser findOneByZip(string $ZIP) Return the first ChoUser filtered by the ZIP column
 * @method ChoUser findOneByPrimaryPhone(string $PRIMARY_PHONE) Return the first ChoUser filtered by the PRIMARY_PHONE column
 * @method ChoUser findOneBySecondaryPhone(string $SECONDARY_PHONE) Return the first ChoUser filtered by the SECONDARY_PHONE column
 * @method ChoUser findOneByCellPhone(string $CELL_PHONE) Return the first ChoUser filtered by the CELL_PHONE column
 * @method ChoUser findOneByPhotoFormat(string $PHOTO_FORMAT) Return the first ChoUser filtered by the PHOTO_FORMAT column
 * @method ChoUser findOneByRegisteredDate(string $REGISTERED_DATE) Return the first ChoUser filtered by the REGISTERED_DATE column
 * @method ChoUser findOneByLastAccess(string $LAST_ACCESS) Return the first ChoUser filtered by the LAST_ACCESS column
 *
 * @method array findById(int $ID) Return ChoUser objects filtered by the ID column
 * @method array findByRolId(int $ROL_ID) Return ChoUser objects filtered by the ROL_ID column
 * @method array findByFirstLastname(string $FIRST_LASTNAME) Return ChoUser objects filtered by the FIRST_LASTNAME column
 * @method array findBySecondLastname(string $SECOND_LASTNAME) Return ChoUser objects filtered by the SECOND_LASTNAME column
 * @method array findByFirstName(string $FIRST_NAME) Return ChoUser objects filtered by the FIRST_NAME column
 * @method array findBySecondName(string $SECOND_NAME) Return ChoUser objects filtered by the SECOND_NAME column
 * @method array findByUsername(string $USERNAME) Return ChoUser objects filtered by the USERNAME column
 * @method array findByPassword(string $PASSWORD) Return ChoUser objects filtered by the PASSWORD column
 * @method array findByEmail(string $EMAIL) Return ChoUser objects filtered by the EMAIL column
 * @method array findByDni(string $DNI) Return ChoUser objects filtered by the DNI column
 * @method array findByGender(string $GENDER) Return ChoUser objects filtered by the GENDER column
 * @method array findByBirthday(string $BIRTHDAY) Return ChoUser objects filtered by the BIRTHDAY column
 * @method array findByType(string $TYPE) Return ChoUser objects filtered by the TYPE column
 * @method array findByStatus(string $STATUS) Return ChoUser objects filtered by the STATUS column
 * @method array findByCountry(string $COUNTRY) Return ChoUser objects filtered by the COUNTRY column
 * @method array findByState(string $STATE) Return ChoUser objects filtered by the STATE column
 * @method array findByCity(string $CITY) Return ChoUser objects filtered by the CITY column
 * @method array findByAddress(string $ADDRESS) Return ChoUser objects filtered by the ADDRESS column
 * @method array findByZip(string $ZIP) Return ChoUser objects filtered by the ZIP column
 * @method array findByPrimaryPhone(string $PRIMARY_PHONE) Return ChoUser objects filtered by the PRIMARY_PHONE column
 * @method array findBySecondaryPhone(string $SECONDARY_PHONE) Return ChoUser objects filtered by the SECONDARY_PHONE column
 * @method array findByCellPhone(string $CELL_PHONE) Return ChoUser objects filtered by the CELL_PHONE column
 * @method array findByPhotoFormat(string $PHOTO_FORMAT) Return ChoUser objects filtered by the PHOTO_FORMAT column
 * @method array findByRegisteredDate(string $REGISTERED_DATE) Return ChoUser objects filtered by the REGISTERED_DATE column
 * @method array findByLastAccess(string $LAST_ACCESS) Return ChoUser objects filtered by the LAST_ACCESS column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseChoUserQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseChoUserQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'ChoUser', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChoUserQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     ChoUserQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChoUserQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ChoUserQuery) {
            return $criteria;
        }
        $query = new ChoUserQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ChoUser|ChoUser[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ChoUserPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   ChoUser A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   ChoUser A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `ROL_ID`, `FIRST_LASTNAME`, `SECOND_LASTNAME`, `FIRST_NAME`, `SECOND_NAME`, `USERNAME`, `PASSWORD`, `EMAIL`, `DNI`, `GENDER`, `BIRTHDAY`, `TYPE`, `STATUS`, `COUNTRY`, `STATE`, `CITY`, `ADDRESS`, `ZIP`, `PRIMARY_PHONE`, `SECONDARY_PHONE`, `CELL_PHONE`, `PHOTO_FORMAT`, `REGISTERED_DATE`, `LAST_ACCESS` FROM `cho_user` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ChoUser();
            $obj->hydrate($row);
            ChoUserPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ChoUser|ChoUser[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ChoUser[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ChoUserPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ChoUserPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ChoUserPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the ROL_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByRolId(1234); // WHERE ROL_ID = 1234
     * $query->filterByRolId(array(12, 34)); // WHERE ROL_ID IN (12, 34)
     * $query->filterByRolId(array('min' => 12)); // WHERE ROL_ID > 12
     * </code>
     *
     * @param     mixed $rolId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByRolId($rolId = null, $comparison = null)
    {
        if (is_array($rolId)) {
            $useMinMax = false;
            if (isset($rolId['min'])) {
                $this->addUsingAlias(ChoUserPeer::ROL_ID, $rolId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rolId['max'])) {
                $this->addUsingAlias(ChoUserPeer::ROL_ID, $rolId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::ROL_ID, $rolId, $comparison);
    }

    /**
     * Filter the query on the FIRST_LASTNAME column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstLastname('fooValue');   // WHERE FIRST_LASTNAME = 'fooValue'
     * $query->filterByFirstLastname('%fooValue%'); // WHERE FIRST_LASTNAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstLastname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByFirstLastname($firstLastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstLastname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $firstLastname)) {
                $firstLastname = str_replace('*', '%', $firstLastname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::FIRST_LASTNAME, $firstLastname, $comparison);
    }

    /**
     * Filter the query on the SECOND_LASTNAME column
     *
     * Example usage:
     * <code>
     * $query->filterBySecondLastname('fooValue');   // WHERE SECOND_LASTNAME = 'fooValue'
     * $query->filterBySecondLastname('%fooValue%'); // WHERE SECOND_LASTNAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $secondLastname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterBySecondLastname($secondLastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($secondLastname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $secondLastname)) {
                $secondLastname = str_replace('*', '%', $secondLastname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::SECOND_LASTNAME, $secondLastname, $comparison);
    }

    /**
     * Filter the query on the FIRST_NAME column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE FIRST_NAME = 'fooValue'
     * $query->filterByFirstName('%fooValue%'); // WHERE FIRST_NAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $firstName)) {
                $firstName = str_replace('*', '%', $firstName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::FIRST_NAME, $firstName, $comparison);
    }

    /**
     * Filter the query on the SECOND_NAME column
     *
     * Example usage:
     * <code>
     * $query->filterBySecondName('fooValue');   // WHERE SECOND_NAME = 'fooValue'
     * $query->filterBySecondName('%fooValue%'); // WHERE SECOND_NAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $secondName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterBySecondName($secondName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($secondName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $secondName)) {
                $secondName = str_replace('*', '%', $secondName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::SECOND_NAME, $secondName, $comparison);
    }

    /**
     * Filter the query on the USERNAME column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE USERNAME = 'fooValue'
     * $query->filterByUsername('%fooValue%'); // WHERE USERNAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $username)) {
                $username = str_replace('*', '%', $username);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the PASSWORD column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE PASSWORD = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE PASSWORD LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the EMAIL column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE EMAIL = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE EMAIL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the DNI column
     *
     * Example usage:
     * <code>
     * $query->filterByDni('fooValue');   // WHERE DNI = 'fooValue'
     * $query->filterByDni('%fooValue%'); // WHERE DNI LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dni The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByDni($dni = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dni)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dni)) {
                $dni = str_replace('*', '%', $dni);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::DNI, $dni, $comparison);
    }

    /**
     * Filter the query on the GENDER column
     *
     * Example usage:
     * <code>
     * $query->filterByGender('fooValue');   // WHERE GENDER = 'fooValue'
     * $query->filterByGender('%fooValue%'); // WHERE GENDER LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gender The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByGender($gender = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gender)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gender)) {
                $gender = str_replace('*', '%', $gender);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::GENDER, $gender, $comparison);
    }

    /**
     * Filter the query on the BIRTHDAY column
     *
     * Example usage:
     * <code>
     * $query->filterByBirthday('2011-03-14'); // WHERE BIRTHDAY = '2011-03-14'
     * $query->filterByBirthday('now'); // WHERE BIRTHDAY = '2011-03-14'
     * $query->filterByBirthday(array('max' => 'yesterday')); // WHERE BIRTHDAY > '2011-03-13'
     * </code>
     *
     * @param     mixed $birthday The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByBirthday($birthday = null, $comparison = null)
    {
        if (is_array($birthday)) {
            $useMinMax = false;
            if (isset($birthday['min'])) {
                $this->addUsingAlias(ChoUserPeer::BIRTHDAY, $birthday['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($birthday['max'])) {
                $this->addUsingAlias(ChoUserPeer::BIRTHDAY, $birthday['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::BIRTHDAY, $birthday, $comparison);
    }

    /**
     * Filter the query on the TYPE column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE TYPE = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE TYPE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the STATUS column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE STATUS = 'fooValue'
     * $query->filterByStatus('%fooValue%'); // WHERE STATUS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $status)) {
                $status = str_replace('*', '%', $status);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the COUNTRY column
     *
     * Example usage:
     * <code>
     * $query->filterByCountry('fooValue');   // WHERE COUNTRY = 'fooValue'
     * $query->filterByCountry('%fooValue%'); // WHERE COUNTRY LIKE '%fooValue%'
     * </code>
     *
     * @param     string $country The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($country)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $country)) {
                $country = str_replace('*', '%', $country);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::COUNTRY, $country, $comparison);
    }

    /**
     * Filter the query on the STATE column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE STATE = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE STATE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the CITY column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE CITY = 'fooValue'
     * $query->filterByCity('%fooValue%'); // WHERE CITY LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $city)) {
                $city = str_replace('*', '%', $city);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::CITY, $city, $comparison);
    }

    /**
     * Filter the query on the ADDRESS column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE ADDRESS = 'fooValue'
     * $query->filterByAddress('%fooValue%'); // WHERE ADDRESS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $address)) {
                $address = str_replace('*', '%', $address);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the ZIP column
     *
     * Example usage:
     * <code>
     * $query->filterByZip('fooValue');   // WHERE ZIP = 'fooValue'
     * $query->filterByZip('%fooValue%'); // WHERE ZIP LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zip The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByZip($zip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zip)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $zip)) {
                $zip = str_replace('*', '%', $zip);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::ZIP, $zip, $comparison);
    }

    /**
     * Filter the query on the PRIMARY_PHONE column
     *
     * Example usage:
     * <code>
     * $query->filterByPrimaryPhone('fooValue');   // WHERE PRIMARY_PHONE = 'fooValue'
     * $query->filterByPrimaryPhone('%fooValue%'); // WHERE PRIMARY_PHONE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $primaryPhone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryPhone($primaryPhone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($primaryPhone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $primaryPhone)) {
                $primaryPhone = str_replace('*', '%', $primaryPhone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::PRIMARY_PHONE, $primaryPhone, $comparison);
    }

    /**
     * Filter the query on the SECONDARY_PHONE column
     *
     * Example usage:
     * <code>
     * $query->filterBySecondaryPhone('fooValue');   // WHERE SECONDARY_PHONE = 'fooValue'
     * $query->filterBySecondaryPhone('%fooValue%'); // WHERE SECONDARY_PHONE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $secondaryPhone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterBySecondaryPhone($secondaryPhone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($secondaryPhone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $secondaryPhone)) {
                $secondaryPhone = str_replace('*', '%', $secondaryPhone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::SECONDARY_PHONE, $secondaryPhone, $comparison);
    }

    /**
     * Filter the query on the CELL_PHONE column
     *
     * Example usage:
     * <code>
     * $query->filterByCellPhone('fooValue');   // WHERE CELL_PHONE = 'fooValue'
     * $query->filterByCellPhone('%fooValue%'); // WHERE CELL_PHONE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cellPhone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByCellPhone($cellPhone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cellPhone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cellPhone)) {
                $cellPhone = str_replace('*', '%', $cellPhone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::CELL_PHONE, $cellPhone, $comparison);
    }

    /**
     * Filter the query on the PHOTO_FORMAT column
     *
     * Example usage:
     * <code>
     * $query->filterByPhotoFormat('fooValue');   // WHERE PHOTO_FORMAT = 'fooValue'
     * $query->filterByPhotoFormat('%fooValue%'); // WHERE PHOTO_FORMAT LIKE '%fooValue%'
     * </code>
     *
     * @param     string $photoFormat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByPhotoFormat($photoFormat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($photoFormat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $photoFormat)) {
                $photoFormat = str_replace('*', '%', $photoFormat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::PHOTO_FORMAT, $photoFormat, $comparison);
    }

    /**
     * Filter the query on the REGISTERED_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByRegisteredDate('2011-03-14'); // WHERE REGISTERED_DATE = '2011-03-14'
     * $query->filterByRegisteredDate('now'); // WHERE REGISTERED_DATE = '2011-03-14'
     * $query->filterByRegisteredDate(array('max' => 'yesterday')); // WHERE REGISTERED_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $registeredDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByRegisteredDate($registeredDate = null, $comparison = null)
    {
        if (is_array($registeredDate)) {
            $useMinMax = false;
            if (isset($registeredDate['min'])) {
                $this->addUsingAlias(ChoUserPeer::REGISTERED_DATE, $registeredDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($registeredDate['max'])) {
                $this->addUsingAlias(ChoUserPeer::REGISTERED_DATE, $registeredDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::REGISTERED_DATE, $registeredDate, $comparison);
    }

    /**
     * Filter the query on the LAST_ACCESS column
     *
     * Example usage:
     * <code>
     * $query->filterByLastAccess('2011-03-14'); // WHERE LAST_ACCESS = '2011-03-14'
     * $query->filterByLastAccess('now'); // WHERE LAST_ACCESS = '2011-03-14'
     * $query->filterByLastAccess(array('max' => 'yesterday')); // WHERE LAST_ACCESS > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastAccess The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function filterByLastAccess($lastAccess = null, $comparison = null)
    {
        if (is_array($lastAccess)) {
            $useMinMax = false;
            if (isset($lastAccess['min'])) {
                $this->addUsingAlias(ChoUserPeer::LAST_ACCESS, $lastAccess['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastAccess['max'])) {
                $this->addUsingAlias(ChoUserPeer::LAST_ACCESS, $lastAccess['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChoUserPeer::LAST_ACCESS, $lastAccess, $comparison);
    }

    /**
     * Filter the query by a related ChoUserXRol object
     *
     * @param   ChoUserXRol|PropelObjectCollection $choUserXRol  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ChoUserQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByChoUserXRol($choUserXRol, $comparison = null)
    {
        if ($choUserXRol instanceof ChoUserXRol) {
            return $this
                ->addUsingAlias(ChoUserPeer::ID, $choUserXRol->getUserId(), $comparison);
        } elseif ($choUserXRol instanceof PropelObjectCollection) {
            return $this
                ->useChoUserXRolQuery()
                ->filterByPrimaryKeys($choUserXRol->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChoUserXRol() only accepts arguments of type ChoUserXRol or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChoUserXRol relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function joinChoUserXRol($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChoUserXRol');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChoUserXRol');
        }

        return $this;
    }

    /**
     * Use the ChoUserXRol relation ChoUserXRol object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ChoUserXRolQuery A secondary query class using the current class as primary query
     */
    public function useChoUserXRolQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChoUserXRol($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChoUserXRol', 'ChoUserXRolQuery');
    }

    /**
     * Filter the query by a related SelEmployee object
     *
     * @param   SelEmployee|PropelObjectCollection $selEmployee  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ChoUserQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelEmployee($selEmployee, $comparison = null)
    {
        if ($selEmployee instanceof SelEmployee) {
            return $this
                ->addUsingAlias(ChoUserPeer::ID, $selEmployee->getUserId(), $comparison);
        } elseif ($selEmployee instanceof PropelObjectCollection) {
            return $this
                ->useSelEmployeeQuery()
                ->filterByPrimaryKeys($selEmployee->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelEmployee() only accepts arguments of type SelEmployee or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelEmployee relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function joinSelEmployee($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelEmployee');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelEmployee');
        }

        return $this;
    }

    /**
     * Use the SelEmployee relation SelEmployee object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelEmployeeQuery A secondary query class using the current class as primary query
     */
    public function useSelEmployeeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelEmployee($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelEmployee', 'SelEmployeeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChoUser $choUser Object to remove from the list of results
     *
     * @return ChoUserQuery The current query, for fluid interface
     */
    public function prune($choUser = null)
    {
        if ($choUser) {
            $this->addUsingAlias(ChoUserPeer::ID, $choUser->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
