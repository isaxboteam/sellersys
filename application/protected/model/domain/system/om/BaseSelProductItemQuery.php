<?php


/**
 * Base class that represents a query for the 'sel_product_item' table.
 *
 * 
 *
 * @method SelProductItemQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelProductItemQuery orderByProductAcquisitionId($order = Criteria::ASC) Order by the PRODUCT_ACQUISITION_ID column
 * @method SelProductItemQuery orderByBrandId($order = Criteria::ASC) Order by the BRAND_ID column
 * @method SelProductItemQuery orderByBranchId($order = Criteria::ASC) Order by the BRANCH_ID column
 * @method SelProductItemQuery orderByProductDepositId($order = Criteria::ASC) Order by the PRODUCT_DEPOSIT_ID column
 * @method SelProductItemQuery orderByProductSellId($order = Criteria::ASC) Order by the PRODUCT_SELL_ID column
 * @method SelProductItemQuery orderByCode($order = Criteria::ASC) Order by the CODE column
 * @method SelProductItemQuery orderByStatus($order = Criteria::ASC) Order by the STATUS column
 * @method SelProductItemQuery orderByCost($order = Criteria::ASC) Order by the COST column
 * @method SelProductItemQuery orderByPrice($order = Criteria::ASC) Order by the PRICE column
 * @method SelProductItemQuery orderByColor($order = Criteria::ASC) Order by the COLOR column
 * @method SelProductItemQuery orderByWarrantySell($order = Criteria::ASC) Order by the WARRANTY_SELL column
 * @method SelProductItemQuery orderByObservation($order = Criteria::ASC) Order by the OBSERVATION column
 * @method SelProductItemQuery orderByRegisterDate($order = Criteria::ASC) Order by the REGISTER_DATE column
 *
 * @method SelProductItemQuery groupById() Group by the ID column
 * @method SelProductItemQuery groupByProductAcquisitionId() Group by the PRODUCT_ACQUISITION_ID column
 * @method SelProductItemQuery groupByBrandId() Group by the BRAND_ID column
 * @method SelProductItemQuery groupByBranchId() Group by the BRANCH_ID column
 * @method SelProductItemQuery groupByProductDepositId() Group by the PRODUCT_DEPOSIT_ID column
 * @method SelProductItemQuery groupByProductSellId() Group by the PRODUCT_SELL_ID column
 * @method SelProductItemQuery groupByCode() Group by the CODE column
 * @method SelProductItemQuery groupByStatus() Group by the STATUS column
 * @method SelProductItemQuery groupByCost() Group by the COST column
 * @method SelProductItemQuery groupByPrice() Group by the PRICE column
 * @method SelProductItemQuery groupByColor() Group by the COLOR column
 * @method SelProductItemQuery groupByWarrantySell() Group by the WARRANTY_SELL column
 * @method SelProductItemQuery groupByObservation() Group by the OBSERVATION column
 * @method SelProductItemQuery groupByRegisterDate() Group by the REGISTER_DATE column
 *
 * @method SelProductItemQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelProductItemQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelProductItemQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelProductItemQuery leftJoinSelProductAcquisition($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductAcquisition relation
 * @method SelProductItemQuery rightJoinSelProductAcquisition($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductAcquisition relation
 * @method SelProductItemQuery innerJoinSelProductAcquisition($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductAcquisition relation
 *
 * @method SelProductItemQuery leftJoinSelBranch($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelBranch relation
 * @method SelProductItemQuery rightJoinSelBranch($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelBranch relation
 * @method SelProductItemQuery innerJoinSelBranch($relationAlias = null) Adds a INNER JOIN clause to the query using the SelBranch relation
 *
 * @method SelProductItemQuery leftJoinSelBrand($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelBrand relation
 * @method SelProductItemQuery rightJoinSelBrand($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelBrand relation
 * @method SelProductItemQuery innerJoinSelBrand($relationAlias = null) Adds a INNER JOIN clause to the query using the SelBrand relation
 *
 * @method SelProductItemQuery leftJoinSelProductSell($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductSell relation
 * @method SelProductItemQuery rightJoinSelProductSell($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductSell relation
 * @method SelProductItemQuery innerJoinSelProductSell($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductSell relation
 *
 * @method SelProductItem findOne(PropelPDO $con = null) Return the first SelProductItem matching the query
 * @method SelProductItem findOneOrCreate(PropelPDO $con = null) Return the first SelProductItem matching the query, or a new SelProductItem object populated from the query conditions when no match is found
 *
 * @method SelProductItem findOneByProductAcquisitionId(int $PRODUCT_ACQUISITION_ID) Return the first SelProductItem filtered by the PRODUCT_ACQUISITION_ID column
 * @method SelProductItem findOneByBrandId(int $BRAND_ID) Return the first SelProductItem filtered by the BRAND_ID column
 * @method SelProductItem findOneByBranchId(int $BRANCH_ID) Return the first SelProductItem filtered by the BRANCH_ID column
 * @method SelProductItem findOneByProductDepositId(int $PRODUCT_DEPOSIT_ID) Return the first SelProductItem filtered by the PRODUCT_DEPOSIT_ID column
 * @method SelProductItem findOneByProductSellId(int $PRODUCT_SELL_ID) Return the first SelProductItem filtered by the PRODUCT_SELL_ID column
 * @method SelProductItem findOneByCode(string $CODE) Return the first SelProductItem filtered by the CODE column
 * @method SelProductItem findOneByStatus(string $STATUS) Return the first SelProductItem filtered by the STATUS column
 * @method SelProductItem findOneByCost(double $COST) Return the first SelProductItem filtered by the COST column
 * @method SelProductItem findOneByPrice(double $PRICE) Return the first SelProductItem filtered by the PRICE column
 * @method SelProductItem findOneByColor(string $COLOR) Return the first SelProductItem filtered by the COLOR column
 * @method SelProductItem findOneByWarrantySell(int $WARRANTY_SELL) Return the first SelProductItem filtered by the WARRANTY_SELL column
 * @method SelProductItem findOneByObservation(string $OBSERVATION) Return the first SelProductItem filtered by the OBSERVATION column
 * @method SelProductItem findOneByRegisterDate(string $REGISTER_DATE) Return the first SelProductItem filtered by the REGISTER_DATE column
 *
 * @method array findById(int $ID) Return SelProductItem objects filtered by the ID column
 * @method array findByProductAcquisitionId(int $PRODUCT_ACQUISITION_ID) Return SelProductItem objects filtered by the PRODUCT_ACQUISITION_ID column
 * @method array findByBrandId(int $BRAND_ID) Return SelProductItem objects filtered by the BRAND_ID column
 * @method array findByBranchId(int $BRANCH_ID) Return SelProductItem objects filtered by the BRANCH_ID column
 * @method array findByProductDepositId(int $PRODUCT_DEPOSIT_ID) Return SelProductItem objects filtered by the PRODUCT_DEPOSIT_ID column
 * @method array findByProductSellId(int $PRODUCT_SELL_ID) Return SelProductItem objects filtered by the PRODUCT_SELL_ID column
 * @method array findByCode(string $CODE) Return SelProductItem objects filtered by the CODE column
 * @method array findByStatus(string $STATUS) Return SelProductItem objects filtered by the STATUS column
 * @method array findByCost(double $COST) Return SelProductItem objects filtered by the COST column
 * @method array findByPrice(double $PRICE) Return SelProductItem objects filtered by the PRICE column
 * @method array findByColor(string $COLOR) Return SelProductItem objects filtered by the COLOR column
 * @method array findByWarrantySell(int $WARRANTY_SELL) Return SelProductItem objects filtered by the WARRANTY_SELL column
 * @method array findByObservation(string $OBSERVATION) Return SelProductItem objects filtered by the OBSERVATION column
 * @method array findByRegisterDate(string $REGISTER_DATE) Return SelProductItem objects filtered by the REGISTER_DATE column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProductItemQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelProductItemQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelProductItem', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelProductItemQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelProductItemQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelProductItemQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelProductItemQuery) {
            return $criteria;
        }
        $query = new SelProductItemQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelProductItem|SelProductItem[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelProductItemPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductItem A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductItem A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `PRODUCT_ACQUISITION_ID`, `BRAND_ID`, `BRANCH_ID`, `PRODUCT_DEPOSIT_ID`, `PRODUCT_SELL_ID`, `CODE`, `STATUS`, `COST`, `PRICE`, `COLOR`, `WARRANTY_SELL`, `OBSERVATION`, `REGISTER_DATE` FROM `sel_product_item` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelProductItem();
            $obj->hydrate($row);
            SelProductItemPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelProductItem|SelProductItem[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelProductItem[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelProductItemPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelProductItemPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelProductItemPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the PRODUCT_ACQUISITION_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByProductAcquisitionId(1234); // WHERE PRODUCT_ACQUISITION_ID = 1234
     * $query->filterByProductAcquisitionId(array(12, 34)); // WHERE PRODUCT_ACQUISITION_ID IN (12, 34)
     * $query->filterByProductAcquisitionId(array('min' => 12)); // WHERE PRODUCT_ACQUISITION_ID > 12
     * </code>
     *
     * @see       filterBySelProductAcquisition()
     *
     * @param     mixed $productAcquisitionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByProductAcquisitionId($productAcquisitionId = null, $comparison = null)
    {
        if (is_array($productAcquisitionId)) {
            $useMinMax = false;
            if (isset($productAcquisitionId['min'])) {
                $this->addUsingAlias(SelProductItemPeer::PRODUCT_ACQUISITION_ID, $productAcquisitionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productAcquisitionId['max'])) {
                $this->addUsingAlias(SelProductItemPeer::PRODUCT_ACQUISITION_ID, $productAcquisitionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::PRODUCT_ACQUISITION_ID, $productAcquisitionId, $comparison);
    }

    /**
     * Filter the query on the BRAND_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByBrandId(1234); // WHERE BRAND_ID = 1234
     * $query->filterByBrandId(array(12, 34)); // WHERE BRAND_ID IN (12, 34)
     * $query->filterByBrandId(array('min' => 12)); // WHERE BRAND_ID > 12
     * </code>
     *
     * @see       filterBySelBrand()
     *
     * @param     mixed $brandId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByBrandId($brandId = null, $comparison = null)
    {
        if (is_array($brandId)) {
            $useMinMax = false;
            if (isset($brandId['min'])) {
                $this->addUsingAlias(SelProductItemPeer::BRAND_ID, $brandId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($brandId['max'])) {
                $this->addUsingAlias(SelProductItemPeer::BRAND_ID, $brandId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::BRAND_ID, $brandId, $comparison);
    }

    /**
     * Filter the query on the BRANCH_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByBranchId(1234); // WHERE BRANCH_ID = 1234
     * $query->filterByBranchId(array(12, 34)); // WHERE BRANCH_ID IN (12, 34)
     * $query->filterByBranchId(array('min' => 12)); // WHERE BRANCH_ID > 12
     * </code>
     *
     * @see       filterBySelBranch()
     *
     * @param     mixed $branchId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByBranchId($branchId = null, $comparison = null)
    {
        if (is_array($branchId)) {
            $useMinMax = false;
            if (isset($branchId['min'])) {
                $this->addUsingAlias(SelProductItemPeer::BRANCH_ID, $branchId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($branchId['max'])) {
                $this->addUsingAlias(SelProductItemPeer::BRANCH_ID, $branchId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::BRANCH_ID, $branchId, $comparison);
    }

    /**
     * Filter the query on the PRODUCT_DEPOSIT_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByProductDepositId(1234); // WHERE PRODUCT_DEPOSIT_ID = 1234
     * $query->filterByProductDepositId(array(12, 34)); // WHERE PRODUCT_DEPOSIT_ID IN (12, 34)
     * $query->filterByProductDepositId(array('min' => 12)); // WHERE PRODUCT_DEPOSIT_ID > 12
     * </code>
     *
     * @param     mixed $productDepositId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByProductDepositId($productDepositId = null, $comparison = null)
    {
        if (is_array($productDepositId)) {
            $useMinMax = false;
            if (isset($productDepositId['min'])) {
                $this->addUsingAlias(SelProductItemPeer::PRODUCT_DEPOSIT_ID, $productDepositId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productDepositId['max'])) {
                $this->addUsingAlias(SelProductItemPeer::PRODUCT_DEPOSIT_ID, $productDepositId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::PRODUCT_DEPOSIT_ID, $productDepositId, $comparison);
    }

    /**
     * Filter the query on the PRODUCT_SELL_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByProductSellId(1234); // WHERE PRODUCT_SELL_ID = 1234
     * $query->filterByProductSellId(array(12, 34)); // WHERE PRODUCT_SELL_ID IN (12, 34)
     * $query->filterByProductSellId(array('min' => 12)); // WHERE PRODUCT_SELL_ID > 12
     * </code>
     *
     * @see       filterBySelProductSell()
     *
     * @param     mixed $productSellId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByProductSellId($productSellId = null, $comparison = null)
    {
        if (is_array($productSellId)) {
            $useMinMax = false;
            if (isset($productSellId['min'])) {
                $this->addUsingAlias(SelProductItemPeer::PRODUCT_SELL_ID, $productSellId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productSellId['max'])) {
                $this->addUsingAlias(SelProductItemPeer::PRODUCT_SELL_ID, $productSellId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::PRODUCT_SELL_ID, $productSellId, $comparison);
    }

    /**
     * Filter the query on the CODE column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE CODE = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE CODE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the STATUS column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE STATUS = 'fooValue'
     * $query->filterByStatus('%fooValue%'); // WHERE STATUS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $status)) {
                $status = str_replace('*', '%', $status);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the COST column
     *
     * Example usage:
     * <code>
     * $query->filterByCost(1234); // WHERE COST = 1234
     * $query->filterByCost(array(12, 34)); // WHERE COST IN (12, 34)
     * $query->filterByCost(array('min' => 12)); // WHERE COST > 12
     * </code>
     *
     * @param     mixed $cost The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByCost($cost = null, $comparison = null)
    {
        if (is_array($cost)) {
            $useMinMax = false;
            if (isset($cost['min'])) {
                $this->addUsingAlias(SelProductItemPeer::COST, $cost['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cost['max'])) {
                $this->addUsingAlias(SelProductItemPeer::COST, $cost['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::COST, $cost, $comparison);
    }

    /**
     * Filter the query on the PRICE column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE PRICE = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE PRICE IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE PRICE > 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(SelProductItemPeer::PRICE, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(SelProductItemPeer::PRICE, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::PRICE, $price, $comparison);
    }

    /**
     * Filter the query on the COLOR column
     *
     * Example usage:
     * <code>
     * $query->filterByColor('fooValue');   // WHERE COLOR = 'fooValue'
     * $query->filterByColor('%fooValue%'); // WHERE COLOR LIKE '%fooValue%'
     * </code>
     *
     * @param     string $color The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByColor($color = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($color)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $color)) {
                $color = str_replace('*', '%', $color);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::COLOR, $color, $comparison);
    }

    /**
     * Filter the query on the WARRANTY_SELL column
     *
     * Example usage:
     * <code>
     * $query->filterByWarrantySell(1234); // WHERE WARRANTY_SELL = 1234
     * $query->filterByWarrantySell(array(12, 34)); // WHERE WARRANTY_SELL IN (12, 34)
     * $query->filterByWarrantySell(array('min' => 12)); // WHERE WARRANTY_SELL > 12
     * </code>
     *
     * @param     mixed $warrantySell The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByWarrantySell($warrantySell = null, $comparison = null)
    {
        if (is_array($warrantySell)) {
            $useMinMax = false;
            if (isset($warrantySell['min'])) {
                $this->addUsingAlias(SelProductItemPeer::WARRANTY_SELL, $warrantySell['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($warrantySell['max'])) {
                $this->addUsingAlias(SelProductItemPeer::WARRANTY_SELL, $warrantySell['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::WARRANTY_SELL, $warrantySell, $comparison);
    }

    /**
     * Filter the query on the OBSERVATION column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE OBSERVATION = 'fooValue'
     * $query->filterByObservation('%fooValue%'); // WHERE OBSERVATION LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $observation)) {
                $observation = str_replace('*', '%', $observation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the REGISTER_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByRegisterDate('2011-03-14'); // WHERE REGISTER_DATE = '2011-03-14'
     * $query->filterByRegisterDate('now'); // WHERE REGISTER_DATE = '2011-03-14'
     * $query->filterByRegisterDate(array('max' => 'yesterday')); // WHERE REGISTER_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $registerDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function filterByRegisterDate($registerDate = null, $comparison = null)
    {
        if (is_array($registerDate)) {
            $useMinMax = false;
            if (isset($registerDate['min'])) {
                $this->addUsingAlias(SelProductItemPeer::REGISTER_DATE, $registerDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($registerDate['max'])) {
                $this->addUsingAlias(SelProductItemPeer::REGISTER_DATE, $registerDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductItemPeer::REGISTER_DATE, $registerDate, $comparison);
    }

    /**
     * Filter the query by a related SelProductAcquisition object
     *
     * @param   SelProductAcquisition|PropelObjectCollection $selProductAcquisition The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductItemQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductAcquisition($selProductAcquisition, $comparison = null)
    {
        if ($selProductAcquisition instanceof SelProductAcquisition) {
            return $this
                ->addUsingAlias(SelProductItemPeer::PRODUCT_ACQUISITION_ID, $selProductAcquisition->getId(), $comparison);
        } elseif ($selProductAcquisition instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductItemPeer::PRODUCT_ACQUISITION_ID, $selProductAcquisition->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelProductAcquisition() only accepts arguments of type SelProductAcquisition or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductAcquisition relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function joinSelProductAcquisition($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductAcquisition');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductAcquisition');
        }

        return $this;
    }

    /**
     * Use the SelProductAcquisition relation SelProductAcquisition object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductAcquisitionQuery A secondary query class using the current class as primary query
     */
    public function useSelProductAcquisitionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductAcquisition($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductAcquisition', 'SelProductAcquisitionQuery');
    }

    /**
     * Filter the query by a related SelBranch object
     *
     * @param   SelBranch|PropelObjectCollection $selBranch The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductItemQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelBranch($selBranch, $comparison = null)
    {
        if ($selBranch instanceof SelBranch) {
            return $this
                ->addUsingAlias(SelProductItemPeer::BRANCH_ID, $selBranch->getId(), $comparison);
        } elseif ($selBranch instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductItemPeer::BRANCH_ID, $selBranch->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelBranch() only accepts arguments of type SelBranch or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelBranch relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function joinSelBranch($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelBranch');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelBranch');
        }

        return $this;
    }

    /**
     * Use the SelBranch relation SelBranch object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelBranchQuery A secondary query class using the current class as primary query
     */
    public function useSelBranchQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelBranch($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelBranch', 'SelBranchQuery');
    }

    /**
     * Filter the query by a related SelBrand object
     *
     * @param   SelBrand|PropelObjectCollection $selBrand The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductItemQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelBrand($selBrand, $comparison = null)
    {
        if ($selBrand instanceof SelBrand) {
            return $this
                ->addUsingAlias(SelProductItemPeer::BRAND_ID, $selBrand->getId(), $comparison);
        } elseif ($selBrand instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductItemPeer::BRAND_ID, $selBrand->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelBrand() only accepts arguments of type SelBrand or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelBrand relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function joinSelBrand($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelBrand');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelBrand');
        }

        return $this;
    }

    /**
     * Use the SelBrand relation SelBrand object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelBrandQuery A secondary query class using the current class as primary query
     */
    public function useSelBrandQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelBrand($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelBrand', 'SelBrandQuery');
    }

    /**
     * Filter the query by a related SelProductSell object
     *
     * @param   SelProductSell|PropelObjectCollection $selProductSell The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductItemQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductSell($selProductSell, $comparison = null)
    {
        if ($selProductSell instanceof SelProductSell) {
            return $this
                ->addUsingAlias(SelProductItemPeer::PRODUCT_SELL_ID, $selProductSell->getId(), $comparison);
        } elseif ($selProductSell instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductItemPeer::PRODUCT_SELL_ID, $selProductSell->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelProductSell() only accepts arguments of type SelProductSell or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductSell relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function joinSelProductSell($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductSell');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductSell');
        }

        return $this;
    }

    /**
     * Use the SelProductSell relation SelProductSell object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductSellQuery A secondary query class using the current class as primary query
     */
    public function useSelProductSellQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelProductSell($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductSell', 'SelProductSellQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelProductItem $selProductItem Object to remove from the list of results
     *
     * @return SelProductItemQuery The current query, for fluid interface
     */
    public function prune($selProductItem = null)
    {
        if ($selProductItem) {
            $this->addUsingAlias(SelProductItemPeer::ID, $selProductItem->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
