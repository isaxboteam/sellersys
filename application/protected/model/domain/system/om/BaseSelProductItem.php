<?php


/**
 * Base class that represents a row from the 'sel_product_item' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProductItem extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelProductItemPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelProductItemPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the product_acquisition_id field.
     * @var        int
     */
    protected $product_acquisition_id;

    /**
     * The value for the brand_id field.
     * @var        int
     */
    protected $brand_id;

    /**
     * The value for the branch_id field.
     * @var        int
     */
    protected $branch_id;

    /**
     * The value for the product_deposit_id field.
     * @var        int
     */
    protected $product_deposit_id;

    /**
     * The value for the product_sell_id field.
     * @var        int
     */
    protected $product_sell_id;

    /**
     * The value for the code field.
     * @var        string
     */
    protected $code;

    /**
     * The value for the status field.
     * @var        string
     */
    protected $status;

    /**
     * The value for the cost field.
     * @var        double
     */
    protected $cost;

    /**
     * The value for the price field.
     * @var        double
     */
    protected $price;

    /**
     * The value for the color field.
     * @var        string
     */
    protected $color;

    /**
     * The value for the warranty_sell field.
     * @var        int
     */
    protected $warranty_sell;

    /**
     * The value for the observation field.
     * @var        string
     */
    protected $observation;

    /**
     * The value for the register_date field.
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        string
     */
    protected $register_date;

    /**
     * @var        SelProductAcquisition
     */
    protected $aSelProductAcquisition;

    /**
     * @var        SelBranch
     */
    protected $aSelBranch;

    /**
     * @var        SelBrand
     */
    protected $aSelBrand;

    /**
     * @var        SelProductSell
     */
    protected $aSelProductSell;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
    }

    /**
     * Initializes internal state of BaseSelProductItem object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [product_acquisition_id] column value.
     * 
     * @return int
     */
    public function getProductAcquisitionId()
    {
        return $this->product_acquisition_id;
    }

    /**
     * Get the [brand_id] column value.
     * 
     * @return int
     */
    public function getBrandId()
    {
        return $this->brand_id;
    }

    /**
     * Get the [branch_id] column value.
     * 
     * @return int
     */
    public function getBranchId()
    {
        return $this->branch_id;
    }

    /**
     * Get the [product_deposit_id] column value.
     * 
     * @return int
     */
    public function getProductDepositId()
    {
        return $this->product_deposit_id;
    }

    /**
     * Get the [product_sell_id] column value.
     * 
     * @return int
     */
    public function getProductSellId()
    {
        return $this->product_sell_id;
    }

    /**
     * Get the [code] column value.
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [status] column value.
     * 
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [cost] column value.
     * 
     * @return double
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Get the [price] column value.
     * 
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get the [color] column value.
     * 
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Get the [warranty_sell] column value.
     * 
     * @return int
     */
    public function getWarrantySell()
    {
        return $this->warranty_sell;
    }

    /**
     * Get the [observation] column value.
     * 
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Get the [optionally formatted] temporal [register_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getRegisterDate($format = 'Y-m-d H:i:s')
    {
        if ($this->register_date === null) {
            return null;
        }

        if ($this->register_date === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->register_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->register_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelProductItemPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [product_acquisition_id] column.
     * 
     * @param int $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setProductAcquisitionId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->product_acquisition_id !== $v) {
            $this->product_acquisition_id = $v;
            $this->modifiedColumns[] = SelProductItemPeer::PRODUCT_ACQUISITION_ID;
        }

        if ($this->aSelProductAcquisition !== null && $this->aSelProductAcquisition->getId() !== $v) {
            $this->aSelProductAcquisition = null;
        }


        return $this;
    } // setProductAcquisitionId()

    /**
     * Set the value of [brand_id] column.
     * 
     * @param int $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setBrandId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->brand_id !== $v) {
            $this->brand_id = $v;
            $this->modifiedColumns[] = SelProductItemPeer::BRAND_ID;
        }

        if ($this->aSelBrand !== null && $this->aSelBrand->getId() !== $v) {
            $this->aSelBrand = null;
        }


        return $this;
    } // setBrandId()

    /**
     * Set the value of [branch_id] column.
     * 
     * @param int $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setBranchId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->branch_id !== $v) {
            $this->branch_id = $v;
            $this->modifiedColumns[] = SelProductItemPeer::BRANCH_ID;
        }

        if ($this->aSelBranch !== null && $this->aSelBranch->getId() !== $v) {
            $this->aSelBranch = null;
        }


        return $this;
    } // setBranchId()

    /**
     * Set the value of [product_deposit_id] column.
     * 
     * @param int $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setProductDepositId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->product_deposit_id !== $v) {
            $this->product_deposit_id = $v;
            $this->modifiedColumns[] = SelProductItemPeer::PRODUCT_DEPOSIT_ID;
        }


        return $this;
    } // setProductDepositId()

    /**
     * Set the value of [product_sell_id] column.
     * 
     * @param int $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setProductSellId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->product_sell_id !== $v) {
            $this->product_sell_id = $v;
            $this->modifiedColumns[] = SelProductItemPeer::PRODUCT_SELL_ID;
        }

        if ($this->aSelProductSell !== null && $this->aSelProductSell->getId() !== $v) {
            $this->aSelProductSell = null;
        }


        return $this;
    } // setProductSellId()

    /**
     * Set the value of [code] column.
     * 
     * @param string $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[] = SelProductItemPeer::CODE;
        }


        return $this;
    } // setCode()

    /**
     * Set the value of [status] column.
     * 
     * @param string $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[] = SelProductItemPeer::STATUS;
        }


        return $this;
    } // setStatus()

    /**
     * Set the value of [cost] column.
     * 
     * @param double $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setCost($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->cost !== $v) {
            $this->cost = $v;
            $this->modifiedColumns[] = SelProductItemPeer::COST;
        }


        return $this;
    } // setCost()

    /**
     * Set the value of [price] column.
     * 
     * @param double $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->price !== $v) {
            $this->price = $v;
            $this->modifiedColumns[] = SelProductItemPeer::PRICE;
        }


        return $this;
    } // setPrice()

    /**
     * Set the value of [color] column.
     * 
     * @param string $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->color !== $v) {
            $this->color = $v;
            $this->modifiedColumns[] = SelProductItemPeer::COLOR;
        }


        return $this;
    } // setColor()

    /**
     * Set the value of [warranty_sell] column.
     * 
     * @param int $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setWarrantySell($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->warranty_sell !== $v) {
            $this->warranty_sell = $v;
            $this->modifiedColumns[] = SelProductItemPeer::WARRANTY_SELL;
        }


        return $this;
    } // setWarrantySell()

    /**
     * Set the value of [observation] column.
     * 
     * @param string $v new value
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[] = SelProductItemPeer::OBSERVATION;
        }


        return $this;
    } // setObservation()

    /**
     * Sets the value of [register_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelProductItem The current object (for fluent API support)
     */
    public function setRegisterDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->register_date !== null || $dt !== null) {
            $currentDateAsString = ($this->register_date !== null && $tmpDt = new DateTime($this->register_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->register_date = $newDateAsString;
                $this->modifiedColumns[] = SelProductItemPeer::REGISTER_DATE;
            }
        } // if either are not null


        return $this;
    } // setRegisterDate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->product_acquisition_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->brand_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->branch_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->product_deposit_id = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->product_sell_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->code = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->status = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->cost = ($row[$startcol + 8] !== null) ? (double) $row[$startcol + 8] : null;
            $this->price = ($row[$startcol + 9] !== null) ? (double) $row[$startcol + 9] : null;
            $this->color = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->warranty_sell = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->observation = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->register_date = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 14; // 14 = SelProductItemPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelProductItem object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSelProductAcquisition !== null && $this->product_acquisition_id !== $this->aSelProductAcquisition->getId()) {
            $this->aSelProductAcquisition = null;
        }
        if ($this->aSelBrand !== null && $this->brand_id !== $this->aSelBrand->getId()) {
            $this->aSelBrand = null;
        }
        if ($this->aSelBranch !== null && $this->branch_id !== $this->aSelBranch->getId()) {
            $this->aSelBranch = null;
        }
        if ($this->aSelProductSell !== null && $this->product_sell_id !== $this->aSelProductSell->getId()) {
            $this->aSelProductSell = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelProductItemPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSelProductAcquisition = null;
            $this->aSelBranch = null;
            $this->aSelBrand = null;
            $this->aSelProductSell = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelProductItemQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelProductItemPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelProductAcquisition !== null) {
                if ($this->aSelProductAcquisition->isModified() || $this->aSelProductAcquisition->isNew()) {
                    $affectedRows += $this->aSelProductAcquisition->save($con);
                }
                $this->setSelProductAcquisition($this->aSelProductAcquisition);
            }

            if ($this->aSelBranch !== null) {
                if ($this->aSelBranch->isModified() || $this->aSelBranch->isNew()) {
                    $affectedRows += $this->aSelBranch->save($con);
                }
                $this->setSelBranch($this->aSelBranch);
            }

            if ($this->aSelBrand !== null) {
                if ($this->aSelBrand->isModified() || $this->aSelBrand->isNew()) {
                    $affectedRows += $this->aSelBrand->save($con);
                }
                $this->setSelBrand($this->aSelBrand);
            }

            if ($this->aSelProductSell !== null) {
                if ($this->aSelProductSell->isModified() || $this->aSelProductSell->isNew()) {
                    $affectedRows += $this->aSelProductSell->save($con);
                }
                $this->setSelProductSell($this->aSelProductSell);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SelProductItemPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SelProductItemPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelProductItemPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelProductItemPeer::PRODUCT_ACQUISITION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`PRODUCT_ACQUISITION_ID`';
        }
        if ($this->isColumnModified(SelProductItemPeer::BRAND_ID)) {
            $modifiedColumns[':p' . $index++]  = '`BRAND_ID`';
        }
        if ($this->isColumnModified(SelProductItemPeer::BRANCH_ID)) {
            $modifiedColumns[':p' . $index++]  = '`BRANCH_ID`';
        }
        if ($this->isColumnModified(SelProductItemPeer::PRODUCT_DEPOSIT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`PRODUCT_DEPOSIT_ID`';
        }
        if ($this->isColumnModified(SelProductItemPeer::PRODUCT_SELL_ID)) {
            $modifiedColumns[':p' . $index++]  = '`PRODUCT_SELL_ID`';
        }
        if ($this->isColumnModified(SelProductItemPeer::CODE)) {
            $modifiedColumns[':p' . $index++]  = '`CODE`';
        }
        if ($this->isColumnModified(SelProductItemPeer::STATUS)) {
            $modifiedColumns[':p' . $index++]  = '`STATUS`';
        }
        if ($this->isColumnModified(SelProductItemPeer::COST)) {
            $modifiedColumns[':p' . $index++]  = '`COST`';
        }
        if ($this->isColumnModified(SelProductItemPeer::PRICE)) {
            $modifiedColumns[':p' . $index++]  = '`PRICE`';
        }
        if ($this->isColumnModified(SelProductItemPeer::COLOR)) {
            $modifiedColumns[':p' . $index++]  = '`COLOR`';
        }
        if ($this->isColumnModified(SelProductItemPeer::WARRANTY_SELL)) {
            $modifiedColumns[':p' . $index++]  = '`WARRANTY_SELL`';
        }
        if ($this->isColumnModified(SelProductItemPeer::OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`OBSERVATION`';
        }
        if ($this->isColumnModified(SelProductItemPeer::REGISTER_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`REGISTER_DATE`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_product_item` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`PRODUCT_ACQUISITION_ID`':						
                        $stmt->bindValue($identifier, $this->product_acquisition_id, PDO::PARAM_INT);
                        break;
                    case '`BRAND_ID`':						
                        $stmt->bindValue($identifier, $this->brand_id, PDO::PARAM_INT);
                        break;
                    case '`BRANCH_ID`':						
                        $stmt->bindValue($identifier, $this->branch_id, PDO::PARAM_INT);
                        break;
                    case '`PRODUCT_DEPOSIT_ID`':						
                        $stmt->bindValue($identifier, $this->product_deposit_id, PDO::PARAM_INT);
                        break;
                    case '`PRODUCT_SELL_ID`':						
                        $stmt->bindValue($identifier, $this->product_sell_id, PDO::PARAM_INT);
                        break;
                    case '`CODE`':						
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case '`STATUS`':						
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_STR);
                        break;
                    case '`COST`':						
                        $stmt->bindValue($identifier, $this->cost, PDO::PARAM_STR);
                        break;
                    case '`PRICE`':						
                        $stmt->bindValue($identifier, $this->price, PDO::PARAM_STR);
                        break;
                    case '`COLOR`':						
                        $stmt->bindValue($identifier, $this->color, PDO::PARAM_STR);
                        break;
                    case '`WARRANTY_SELL`':						
                        $stmt->bindValue($identifier, $this->warranty_sell, PDO::PARAM_INT);
                        break;
                    case '`OBSERVATION`':						
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                    case '`REGISTER_DATE`':						
                        $stmt->bindValue($identifier, $this->register_date, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelProductAcquisition !== null) {
                if (!$this->aSelProductAcquisition->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelProductAcquisition->getValidationFailures());
                }
            }

            if ($this->aSelBranch !== null) {
                if (!$this->aSelBranch->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelBranch->getValidationFailures());
                }
            }

            if ($this->aSelBrand !== null) {
                if (!$this->aSelBrand->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelBrand->getValidationFailures());
                }
            }

            if ($this->aSelProductSell !== null) {
                if (!$this->aSelProductSell->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelProductSell->getValidationFailures());
                }
            }

            if (($retval = SelProductItemPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelProductItemPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getProductAcquisitionId();
                break;
            case 2:
                return $this->getBrandId();
                break;
            case 3:
                return $this->getBranchId();
                break;
            case 4:
                return $this->getProductDepositId();
                break;
            case 5:
                return $this->getProductSellId();
                break;
            case 6:
                return $this->getCode();
                break;
            case 7:
                return $this->getStatus();
                break;
            case 8:
                return $this->getCost();
                break;
            case 9:
                return $this->getPrice();
                break;
            case 10:
                return $this->getColor();
                break;
            case 11:
                return $this->getWarrantySell();
                break;
            case 12:
                return $this->getObservation();
                break;
            case 13:
                return $this->getRegisterDate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelProductItem'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelProductItem'][$this->getPrimaryKey()] = true;
        $keys = SelProductItemPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getProductAcquisitionId(),
            $keys[2] => $this->getBrandId(),
            $keys[3] => $this->getBranchId(),
            $keys[4] => $this->getProductDepositId(),
            $keys[5] => $this->getProductSellId(),
            $keys[6] => $this->getCode(),
            $keys[7] => $this->getStatus(),
            $keys[8] => $this->getCost(),
            $keys[9] => $this->getPrice(),
            $keys[10] => $this->getColor(),
            $keys[11] => $this->getWarrantySell(),
            $keys[12] => $this->getObservation(),
            $keys[13] => $this->getRegisterDate(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSelProductAcquisition) {
                $result['SelProductAcquisition'] = $this->aSelProductAcquisition->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSelBranch) {
                $result['SelBranch'] = $this->aSelBranch->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSelBrand) {
                $result['SelBrand'] = $this->aSelBrand->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSelProductSell) {
                $result['SelProductSell'] = $this->aSelProductSell->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelProductItemPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setProductAcquisitionId($value);
                break;
            case 2:
                $this->setBrandId($value);
                break;
            case 3:
                $this->setBranchId($value);
                break;
            case 4:
                $this->setProductDepositId($value);
                break;
            case 5:
                $this->setProductSellId($value);
                break;
            case 6:
                $this->setCode($value);
                break;
            case 7:
                $this->setStatus($value);
                break;
            case 8:
                $this->setCost($value);
                break;
            case 9:
                $this->setPrice($value);
                break;
            case 10:
                $this->setColor($value);
                break;
            case 11:
                $this->setWarrantySell($value);
                break;
            case 12:
                $this->setObservation($value);
                break;
            case 13:
                $this->setRegisterDate($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelProductItemPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setProductAcquisitionId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setBrandId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setBranchId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setProductDepositId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setProductSellId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCode($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setStatus($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setCost($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setPrice($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setColor($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setWarrantySell($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setObservation($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setRegisterDate($arr[$keys[13]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelProductItemPeer::DATABASE_NAME);

        if ($this->isColumnModified(SelProductItemPeer::ID)) $criteria->add(SelProductItemPeer::ID, $this->id);
        if ($this->isColumnModified(SelProductItemPeer::PRODUCT_ACQUISITION_ID)) $criteria->add(SelProductItemPeer::PRODUCT_ACQUISITION_ID, $this->product_acquisition_id);
        if ($this->isColumnModified(SelProductItemPeer::BRAND_ID)) $criteria->add(SelProductItemPeer::BRAND_ID, $this->brand_id);
        if ($this->isColumnModified(SelProductItemPeer::BRANCH_ID)) $criteria->add(SelProductItemPeer::BRANCH_ID, $this->branch_id);
        if ($this->isColumnModified(SelProductItemPeer::PRODUCT_DEPOSIT_ID)) $criteria->add(SelProductItemPeer::PRODUCT_DEPOSIT_ID, $this->product_deposit_id);
        if ($this->isColumnModified(SelProductItemPeer::PRODUCT_SELL_ID)) $criteria->add(SelProductItemPeer::PRODUCT_SELL_ID, $this->product_sell_id);
        if ($this->isColumnModified(SelProductItemPeer::CODE)) $criteria->add(SelProductItemPeer::CODE, $this->code);
        if ($this->isColumnModified(SelProductItemPeer::STATUS)) $criteria->add(SelProductItemPeer::STATUS, $this->status);
        if ($this->isColumnModified(SelProductItemPeer::COST)) $criteria->add(SelProductItemPeer::COST, $this->cost);
        if ($this->isColumnModified(SelProductItemPeer::PRICE)) $criteria->add(SelProductItemPeer::PRICE, $this->price);
        if ($this->isColumnModified(SelProductItemPeer::COLOR)) $criteria->add(SelProductItemPeer::COLOR, $this->color);
        if ($this->isColumnModified(SelProductItemPeer::WARRANTY_SELL)) $criteria->add(SelProductItemPeer::WARRANTY_SELL, $this->warranty_sell);
        if ($this->isColumnModified(SelProductItemPeer::OBSERVATION)) $criteria->add(SelProductItemPeer::OBSERVATION, $this->observation);
        if ($this->isColumnModified(SelProductItemPeer::REGISTER_DATE)) $criteria->add(SelProductItemPeer::REGISTER_DATE, $this->register_date);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelProductItemPeer::DATABASE_NAME);
        $criteria->add(SelProductItemPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelProductItem (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setProductAcquisitionId($this->getProductAcquisitionId());
        $copyObj->setBrandId($this->getBrandId());
        $copyObj->setBranchId($this->getBranchId());
        $copyObj->setProductDepositId($this->getProductDepositId());
        $copyObj->setProductSellId($this->getProductSellId());
        $copyObj->setCode($this->getCode());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setCost($this->getCost());
        $copyObj->setPrice($this->getPrice());
        $copyObj->setColor($this->getColor());
        $copyObj->setWarrantySell($this->getWarrantySell());
        $copyObj->setObservation($this->getObservation());
        $copyObj->setRegisterDate($this->getRegisterDate());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelProductItem Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelProductItemPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelProductItemPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SelProductAcquisition object.
     *
     * @param             SelProductAcquisition $v
     * @return SelProductItem The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelProductAcquisition(SelProductAcquisition $v = null)
    {
        if ($v === null) {
            $this->setProductAcquisitionId(NULL);
        } else {
            $this->setProductAcquisitionId($v->getId());
        }

        $this->aSelProductAcquisition = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelProductAcquisition object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProductItem($this);
        }


        return $this;
    }


    /**
     * Get the associated SelProductAcquisition object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelProductAcquisition The associated SelProductAcquisition object.
     * @throws PropelException
     */
    public function getSelProductAcquisition(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelProductAcquisition === null && ($this->product_acquisition_id !== null) && $doQuery) {
            $this->aSelProductAcquisition = SelProductAcquisitionQuery::create()->findPk($this->product_acquisition_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelProductAcquisition->addSelProductItems($this);
             */
        }

        return $this->aSelProductAcquisition;
    }

    /**
     * Declares an association between this object and a SelBranch object.
     *
     * @param             SelBranch $v
     * @return SelProductItem The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelBranch(SelBranch $v = null)
    {
        if ($v === null) {
            $this->setBranchId(NULL);
        } else {
            $this->setBranchId($v->getId());
        }

        $this->aSelBranch = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelBranch object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProductItem($this);
        }


        return $this;
    }


    /**
     * Get the associated SelBranch object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelBranch The associated SelBranch object.
     * @throws PropelException
     */
    public function getSelBranch(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelBranch === null && ($this->branch_id !== null) && $doQuery) {
            $this->aSelBranch = SelBranchQuery::create()->findPk($this->branch_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelBranch->addSelProductItems($this);
             */
        }

        return $this->aSelBranch;
    }

    /**
     * Declares an association between this object and a SelBrand object.
     *
     * @param             SelBrand $v
     * @return SelProductItem The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelBrand(SelBrand $v = null)
    {
        if ($v === null) {
            $this->setBrandId(NULL);
        } else {
            $this->setBrandId($v->getId());
        }

        $this->aSelBrand = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelBrand object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProductItem($this);
        }


        return $this;
    }


    /**
     * Get the associated SelBrand object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelBrand The associated SelBrand object.
     * @throws PropelException
     */
    public function getSelBrand(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelBrand === null && ($this->brand_id !== null) && $doQuery) {
            $this->aSelBrand = SelBrandQuery::create()->findPk($this->brand_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelBrand->addSelProductItems($this);
             */
        }

        return $this->aSelBrand;
    }

    /**
     * Declares an association between this object and a SelProductSell object.
     *
     * @param             SelProductSell $v
     * @return SelProductItem The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelProductSell(SelProductSell $v = null)
    {
        if ($v === null) {
            $this->setProductSellId(NULL);
        } else {
            $this->setProductSellId($v->getId());
        }

        $this->aSelProductSell = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelProductSell object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProductItem($this);
        }


        return $this;
    }


    /**
     * Get the associated SelProductSell object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelProductSell The associated SelProductSell object.
     * @throws PropelException
     */
    public function getSelProductSell(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelProductSell === null && ($this->product_sell_id !== null) && $doQuery) {
            $this->aSelProductSell = SelProductSellQuery::create()->findPk($this->product_sell_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelProductSell->addSelProductItems($this);
             */
        }

        return $this->aSelProductSell;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->product_acquisition_id = null;
        $this->brand_id = null;
        $this->branch_id = null;
        $this->product_deposit_id = null;
        $this->product_sell_id = null;
        $this->code = null;
        $this->status = null;
        $this->cost = null;
        $this->price = null;
        $this->color = null;
        $this->warranty_sell = null;
        $this->observation = null;
        $this->register_date = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aSelProductAcquisition = null;
        $this->aSelBranch = null;
        $this->aSelBrand = null;
        $this->aSelProductSell = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelProductItemPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
