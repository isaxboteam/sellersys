<?php


/**
 * Base class that represents a row from the 'sel_product' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProduct extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelProductPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelProductPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the product_group_id field.
     * @var        int
     */
    protected $product_group_id;

    /**
     * The value for the brand_id field.
     * @var        int
     */
    protected $brand_id;

    /**
     * The value for the category_id field.
     * @var        int
     */
    protected $category_id;

    /**
     * The value for the main_image_id field.
     * @var        int
     */
    protected $main_image_id;

    /**
     * The value for the code field.
     * @var        string
     */
    protected $code;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the base_price field.
     * @var        double
     */
    protected $base_price;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * The value for the model field.
     * @var        string
     */
    protected $model;

    /**
     * The value for the number_parts field.
     * @var        int
     */
    protected $number_parts;

    /**
     * The value for the is_main field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_main;

    /**
     * The value for the is_new field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_new;

    /**
     * The value for the is_offer field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_offer;

    /**
     * @var        SelCategory
     */
    protected $aSelCategory;

    /**
     * @var        SelProductGroup
     */
    protected $aSelProductGroup;

    /**
     * @var        SelBrand
     */
    protected $aSelBrand;

    /**
     * @var        SelProductImage
     */
    protected $aSelProductImageRelatedByMainImageId;

    /**
     * @var        PropelObjectCollection|SelProductAcquisition[] Collection to store aggregation of SelProductAcquisition objects.
     */
    protected $collSelProductAcquisitions;
    protected $collSelProductAcquisitionsPartial;

    /**
     * @var        PropelObjectCollection|SelProductImage[] Collection to store aggregation of SelProductImage objects.
     */
    protected $collSelProductImagesRelatedByProductId;
    protected $collSelProductImagesRelatedByProductIdPartial;

    /**
     * @var        SelProductOffer one-to-one related SelProductOffer object
     */
    protected $singleSelProductOffer;

    /**
     * @var        PropelObjectCollection|SelProductOrder[] Collection to store aggregation of SelProductOrder objects.
     */
    protected $collSelProductOrders;
    protected $collSelProductOrdersPartial;

    /**
     * @var        PropelObjectCollection|SelProductSell[] Collection to store aggregation of SelProductSell objects.
     */
    protected $collSelProductSells;
    protected $collSelProductSellsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductAcquisitionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductImagesRelatedByProductIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductOffersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductOrdersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductSellsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_main = false;
        $this->is_new = false;
        $this->is_offer = false;
    }

    /**
     * Initializes internal state of BaseSelProduct object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [product_group_id] column value.
     * 
     * @return int
     */
    public function getProductGroupId()
    {
        return $this->product_group_id;
    }

    /**
     * Get the [brand_id] column value.
     * 
     * @return int
     */
    public function getBrandId()
    {
        return $this->brand_id;
    }

    /**
     * Get the [category_id] column value.
     * 
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * Get the [main_image_id] column value.
     * 
     * @return int
     */
    public function getMainImageId()
    {
        return $this->main_image_id;
    }

    /**
     * Get the [code] column value.
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [name] column value.
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [base_price] column value.
     * 
     * @return double
     */
    public function getBasePrice()
    {
        return $this->base_price;
    }

    /**
     * Get the [description] column value.
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [model] column value.
     * 
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Get the [number_parts] column value.
     * 
     * @return int
     */
    public function getNumberParts()
    {
        return $this->number_parts;
    }

    /**
     * Get the [is_main] column value.
     * 
     * @return boolean
     */
    public function getIsMain()
    {
        return $this->is_main;
    }

    /**
     * Get the [is_new] column value.
     * 
     * @return boolean
     */
    public function getIsNew()
    {
        return $this->is_new;
    }

    /**
     * Get the [is_offer] column value.
     * 
     * @return boolean
     */
    public function getIsOffer()
    {
        return $this->is_offer;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelProductPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [product_group_id] column.
     * 
     * @param int $v new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setProductGroupId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->product_group_id !== $v) {
            $this->product_group_id = $v;
            $this->modifiedColumns[] = SelProductPeer::PRODUCT_GROUP_ID;
        }

        if ($this->aSelProductGroup !== null && $this->aSelProductGroup->getId() !== $v) {
            $this->aSelProductGroup = null;
        }


        return $this;
    } // setProductGroupId()

    /**
     * Set the value of [brand_id] column.
     * 
     * @param int $v new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setBrandId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->brand_id !== $v) {
            $this->brand_id = $v;
            $this->modifiedColumns[] = SelProductPeer::BRAND_ID;
        }

        if ($this->aSelBrand !== null && $this->aSelBrand->getId() !== $v) {
            $this->aSelBrand = null;
        }


        return $this;
    } // setBrandId()

    /**
     * Set the value of [category_id] column.
     * 
     * @param int $v new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setCategoryId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->category_id !== $v) {
            $this->category_id = $v;
            $this->modifiedColumns[] = SelProductPeer::CATEGORY_ID;
        }

        if ($this->aSelCategory !== null && $this->aSelCategory->getId() !== $v) {
            $this->aSelCategory = null;
        }


        return $this;
    } // setCategoryId()

    /**
     * Set the value of [main_image_id] column.
     * 
     * @param int $v new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setMainImageId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->main_image_id !== $v) {
            $this->main_image_id = $v;
            $this->modifiedColumns[] = SelProductPeer::MAIN_IMAGE_ID;
        }

        if ($this->aSelProductImageRelatedByMainImageId !== null && $this->aSelProductImageRelatedByMainImageId->getId() !== $v) {
            $this->aSelProductImageRelatedByMainImageId = null;
        }


        return $this;
    } // setMainImageId()

    /**
     * Set the value of [code] column.
     * 
     * @param string $v new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[] = SelProductPeer::CODE;
        }


        return $this;
    } // setCode()

    /**
     * Set the value of [name] column.
     * 
     * @param string $v new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = SelProductPeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [base_price] column.
     * 
     * @param double $v new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setBasePrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->base_price !== $v) {
            $this->base_price = $v;
            $this->modifiedColumns[] = SelProductPeer::BASE_PRICE;
        }


        return $this;
    } // setBasePrice()

    /**
     * Set the value of [description] column.
     * 
     * @param string $v new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = SelProductPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Set the value of [model] column.
     * 
     * @param string $v new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setModel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->model !== $v) {
            $this->model = $v;
            $this->modifiedColumns[] = SelProductPeer::MODEL;
        }


        return $this;
    } // setModel()

    /**
     * Set the value of [number_parts] column.
     * 
     * @param int $v new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setNumberParts($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->number_parts !== $v) {
            $this->number_parts = $v;
            $this->modifiedColumns[] = SelProductPeer::NUMBER_PARTS;
        }


        return $this;
    } // setNumberParts()

    /**
     * Sets the value of the [is_main] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setIsMain($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_main !== $v) {
            $this->is_main = $v;
            $this->modifiedColumns[] = SelProductPeer::IS_MAIN;
        }


        return $this;
    } // setIsMain()

    /**
     * Sets the value of the [is_new] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setIsNew($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_new !== $v) {
            $this->is_new = $v;
            $this->modifiedColumns[] = SelProductPeer::IS_NEW;
        }


        return $this;
    } // setIsNew()

    /**
     * Sets the value of the [is_offer] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return SelProduct The current object (for fluent API support)
     */
    public function setIsOffer($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_offer !== $v) {
            $this->is_offer = $v;
            $this->modifiedColumns[] = SelProductPeer::IS_OFFER;
        }


        return $this;
    } // setIsOffer()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_main !== false) {
                return false;
            }

            if ($this->is_new !== false) {
                return false;
            }

            if ($this->is_offer !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->product_group_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->brand_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->category_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->main_image_id = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->code = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->name = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->base_price = ($row[$startcol + 7] !== null) ? (double) $row[$startcol + 7] : null;
            $this->description = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->model = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->number_parts = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->is_main = ($row[$startcol + 11] !== null) ? (boolean) $row[$startcol + 11] : null;
            $this->is_new = ($row[$startcol + 12] !== null) ? (boolean) $row[$startcol + 12] : null;
            $this->is_offer = ($row[$startcol + 13] !== null) ? (boolean) $row[$startcol + 13] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 14; // 14 = SelProductPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelProduct object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSelProductGroup !== null && $this->product_group_id !== $this->aSelProductGroup->getId()) {
            $this->aSelProductGroup = null;
        }
        if ($this->aSelBrand !== null && $this->brand_id !== $this->aSelBrand->getId()) {
            $this->aSelBrand = null;
        }
        if ($this->aSelCategory !== null && $this->category_id !== $this->aSelCategory->getId()) {
            $this->aSelCategory = null;
        }
        if ($this->aSelProductImageRelatedByMainImageId !== null && $this->main_image_id !== $this->aSelProductImageRelatedByMainImageId->getId()) {
            $this->aSelProductImageRelatedByMainImageId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelProductPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSelCategory = null;
            $this->aSelProductGroup = null;
            $this->aSelBrand = null;
            $this->aSelProductImageRelatedByMainImageId = null;
            $this->collSelProductAcquisitions = null;

            $this->collSelProductImagesRelatedByProductId = null;

            $this->singleSelProductOffer = null;

            $this->collSelProductOrders = null;

            $this->collSelProductSells = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelProductQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelProductPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelCategory !== null) {
                if ($this->aSelCategory->isModified() || $this->aSelCategory->isNew()) {
                    $affectedRows += $this->aSelCategory->save($con);
                }
                $this->setSelCategory($this->aSelCategory);
            }

            if ($this->aSelProductGroup !== null) {
                if ($this->aSelProductGroup->isModified() || $this->aSelProductGroup->isNew()) {
                    $affectedRows += $this->aSelProductGroup->save($con);
                }
                $this->setSelProductGroup($this->aSelProductGroup);
            }

            if ($this->aSelBrand !== null) {
                if ($this->aSelBrand->isModified() || $this->aSelBrand->isNew()) {
                    $affectedRows += $this->aSelBrand->save($con);
                }
                $this->setSelBrand($this->aSelBrand);
            }

            if ($this->aSelProductImageRelatedByMainImageId !== null) {
                if ($this->aSelProductImageRelatedByMainImageId->isModified() || $this->aSelProductImageRelatedByMainImageId->isNew()) {
                    $affectedRows += $this->aSelProductImageRelatedByMainImageId->save($con);
                }
                $this->setSelProductImageRelatedByMainImageId($this->aSelProductImageRelatedByMainImageId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->selProductAcquisitionsScheduledForDeletion !== null) {
                if (!$this->selProductAcquisitionsScheduledForDeletion->isEmpty()) {
                    SelProductAcquisitionQuery::create()
                        ->filterByPrimaryKeys($this->selProductAcquisitionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selProductAcquisitionsScheduledForDeletion = null;
                }
            }

            if ($this->collSelProductAcquisitions !== null) {
                foreach ($this->collSelProductAcquisitions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selProductImagesRelatedByProductIdScheduledForDeletion !== null) {
                if (!$this->selProductImagesRelatedByProductIdScheduledForDeletion->isEmpty()) {
                    SelProductImageQuery::create()
                        ->filterByPrimaryKeys($this->selProductImagesRelatedByProductIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selProductImagesRelatedByProductIdScheduledForDeletion = null;
                }
            }

            if ($this->collSelProductImagesRelatedByProductId !== null) {
                foreach ($this->collSelProductImagesRelatedByProductId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selProductOffersScheduledForDeletion !== null) {
                if (!$this->selProductOffersScheduledForDeletion->isEmpty()) {
                    SelProductOfferQuery::create()
                        ->filterByPrimaryKeys($this->selProductOffersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selProductOffersScheduledForDeletion = null;
                }
            }

            if ($this->singleSelProductOffer !== null) {
                if (!$this->singleSelProductOffer->isDeleted() && ($this->singleSelProductOffer->isNew() || $this->singleSelProductOffer->isModified())) {
                        $affectedRows += $this->singleSelProductOffer->save($con);
                }
            }

            if ($this->selProductOrdersScheduledForDeletion !== null) {
                if (!$this->selProductOrdersScheduledForDeletion->isEmpty()) {
                    SelProductOrderQuery::create()
                        ->filterByPrimaryKeys($this->selProductOrdersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selProductOrdersScheduledForDeletion = null;
                }
            }

            if ($this->collSelProductOrders !== null) {
                foreach ($this->collSelProductOrders as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selProductSellsScheduledForDeletion !== null) {
                if (!$this->selProductSellsScheduledForDeletion->isEmpty()) {
                    SelProductSellQuery::create()
                        ->filterByPrimaryKeys($this->selProductSellsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selProductSellsScheduledForDeletion = null;
                }
            }

            if ($this->collSelProductSells !== null) {
                foreach ($this->collSelProductSells as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SelProductPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SelProductPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelProductPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelProductPeer::PRODUCT_GROUP_ID)) {
            $modifiedColumns[':p' . $index++]  = '`PRODUCT_GROUP_ID`';
        }
        if ($this->isColumnModified(SelProductPeer::BRAND_ID)) {
            $modifiedColumns[':p' . $index++]  = '`BRAND_ID`';
        }
        if ($this->isColumnModified(SelProductPeer::CATEGORY_ID)) {
            $modifiedColumns[':p' . $index++]  = '`CATEGORY_ID`';
        }
        if ($this->isColumnModified(SelProductPeer::MAIN_IMAGE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`MAIN_IMAGE_ID`';
        }
        if ($this->isColumnModified(SelProductPeer::CODE)) {
            $modifiedColumns[':p' . $index++]  = '`CODE`';
        }
        if ($this->isColumnModified(SelProductPeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`NAME`';
        }
        if ($this->isColumnModified(SelProductPeer::BASE_PRICE)) {
            $modifiedColumns[':p' . $index++]  = '`BASE_PRICE`';
        }
        if ($this->isColumnModified(SelProductPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`DESCRIPTION`';
        }
        if ($this->isColumnModified(SelProductPeer::MODEL)) {
            $modifiedColumns[':p' . $index++]  = '`MODEL`';
        }
        if ($this->isColumnModified(SelProductPeer::NUMBER_PARTS)) {
            $modifiedColumns[':p' . $index++]  = '`NUMBER_PARTS`';
        }
        if ($this->isColumnModified(SelProductPeer::IS_MAIN)) {
            $modifiedColumns[':p' . $index++]  = '`IS_MAIN`';
        }
        if ($this->isColumnModified(SelProductPeer::IS_NEW)) {
            $modifiedColumns[':p' . $index++]  = '`IS_NEW`';
        }
        if ($this->isColumnModified(SelProductPeer::IS_OFFER)) {
            $modifiedColumns[':p' . $index++]  = '`IS_OFFER`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_product` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`PRODUCT_GROUP_ID`':						
                        $stmt->bindValue($identifier, $this->product_group_id, PDO::PARAM_INT);
                        break;
                    case '`BRAND_ID`':						
                        $stmt->bindValue($identifier, $this->brand_id, PDO::PARAM_INT);
                        break;
                    case '`CATEGORY_ID`':						
                        $stmt->bindValue($identifier, $this->category_id, PDO::PARAM_INT);
                        break;
                    case '`MAIN_IMAGE_ID`':						
                        $stmt->bindValue($identifier, $this->main_image_id, PDO::PARAM_INT);
                        break;
                    case '`CODE`':						
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case '`NAME`':						
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`BASE_PRICE`':						
                        $stmt->bindValue($identifier, $this->base_price, PDO::PARAM_STR);
                        break;
                    case '`DESCRIPTION`':						
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`MODEL`':						
                        $stmt->bindValue($identifier, $this->model, PDO::PARAM_STR);
                        break;
                    case '`NUMBER_PARTS`':						
                        $stmt->bindValue($identifier, $this->number_parts, PDO::PARAM_INT);
                        break;
                    case '`IS_MAIN`':
                        $stmt->bindValue($identifier, (int) $this->is_main, PDO::PARAM_INT);
                        break;
                    case '`IS_NEW`':
                        $stmt->bindValue($identifier, (int) $this->is_new, PDO::PARAM_INT);
                        break;
                    case '`IS_OFFER`':
                        $stmt->bindValue($identifier, (int) $this->is_offer, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelCategory !== null) {
                if (!$this->aSelCategory->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelCategory->getValidationFailures());
                }
            }

            if ($this->aSelProductGroup !== null) {
                if (!$this->aSelProductGroup->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelProductGroup->getValidationFailures());
                }
            }

            if ($this->aSelBrand !== null) {
                if (!$this->aSelBrand->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelBrand->getValidationFailures());
                }
            }

            if ($this->aSelProductImageRelatedByMainImageId !== null) {
                if (!$this->aSelProductImageRelatedByMainImageId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelProductImageRelatedByMainImageId->getValidationFailures());
                }
            }

            if (($retval = SelProductPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelProductAcquisitions !== null) {
                    foreach ($this->collSelProductAcquisitions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelProductImagesRelatedByProductId !== null) {
                    foreach ($this->collSelProductImagesRelatedByProductId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->singleSelProductOffer !== null) {
                    if (!$this->singleSelProductOffer->validate($columns)) {
                        $failureMap = array_merge($failureMap, $this->singleSelProductOffer->getValidationFailures());
                    }
                }

                if ($this->collSelProductOrders !== null) {
                    foreach ($this->collSelProductOrders as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelProductSells !== null) {
                    foreach ($this->collSelProductSells as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelProductPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getProductGroupId();
                break;
            case 2:
                return $this->getBrandId();
                break;
            case 3:
                return $this->getCategoryId();
                break;
            case 4:
                return $this->getMainImageId();
                break;
            case 5:
                return $this->getCode();
                break;
            case 6:
                return $this->getName();
                break;
            case 7:
                return $this->getBasePrice();
                break;
            case 8:
                return $this->getDescription();
                break;
            case 9:
                return $this->getModel();
                break;
            case 10:
                return $this->getNumberParts();
                break;
            case 11:
                return $this->getIsMain();
                break;
            case 12:
                return $this->getIsNew();
                break;
            case 13:
                return $this->getIsOffer();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelProduct'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelProduct'][$this->getPrimaryKey()] = true;
        $keys = SelProductPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getProductGroupId(),
            $keys[2] => $this->getBrandId(),
            $keys[3] => $this->getCategoryId(),
            $keys[4] => $this->getMainImageId(),
            $keys[5] => $this->getCode(),
            $keys[6] => $this->getName(),
            $keys[7] => $this->getBasePrice(),
            $keys[8] => $this->getDescription(),
            $keys[9] => $this->getModel(),
            $keys[10] => $this->getNumberParts(),
            $keys[11] => $this->getIsMain(),
            $keys[12] => $this->getIsNew(),
            $keys[13] => $this->getIsOffer(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSelCategory) {
                $result['SelCategory'] = $this->aSelCategory->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSelProductGroup) {
                $result['SelProductGroup'] = $this->aSelProductGroup->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSelBrand) {
                $result['SelBrand'] = $this->aSelBrand->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSelProductImageRelatedByMainImageId) {
                $result['SelProductImageRelatedByMainImageId'] = $this->aSelProductImageRelatedByMainImageId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSelProductAcquisitions) {
                $result['SelProductAcquisitions'] = $this->collSelProductAcquisitions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelProductImagesRelatedByProductId) {
                $result['SelProductImagesRelatedByProductId'] = $this->collSelProductImagesRelatedByProductId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->singleSelProductOffer) {
                $result['SelProductOffer'] = $this->singleSelProductOffer->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSelProductOrders) {
                $result['SelProductOrders'] = $this->collSelProductOrders->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelProductSells) {
                $result['SelProductSells'] = $this->collSelProductSells->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelProductPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setProductGroupId($value);
                break;
            case 2:
                $this->setBrandId($value);
                break;
            case 3:
                $this->setCategoryId($value);
                break;
            case 4:
                $this->setMainImageId($value);
                break;
            case 5:
                $this->setCode($value);
                break;
            case 6:
                $this->setName($value);
                break;
            case 7:
                $this->setBasePrice($value);
                break;
            case 8:
                $this->setDescription($value);
                break;
            case 9:
                $this->setModel($value);
                break;
            case 10:
                $this->setNumberParts($value);
                break;
            case 11:
                $this->setIsMain($value);
                break;
            case 12:
                $this->setIsNew($value);
                break;
            case 13:
                $this->setIsOffer($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelProductPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setProductGroupId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setBrandId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCategoryId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setMainImageId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setCode($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setName($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setBasePrice($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setDescription($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setModel($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setNumberParts($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setIsMain($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setIsNew($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setIsOffer($arr[$keys[13]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelProductPeer::DATABASE_NAME);

        if ($this->isColumnModified(SelProductPeer::ID)) $criteria->add(SelProductPeer::ID, $this->id);
        if ($this->isColumnModified(SelProductPeer::PRODUCT_GROUP_ID)) $criteria->add(SelProductPeer::PRODUCT_GROUP_ID, $this->product_group_id);
        if ($this->isColumnModified(SelProductPeer::BRAND_ID)) $criteria->add(SelProductPeer::BRAND_ID, $this->brand_id);
        if ($this->isColumnModified(SelProductPeer::CATEGORY_ID)) $criteria->add(SelProductPeer::CATEGORY_ID, $this->category_id);
        if ($this->isColumnModified(SelProductPeer::MAIN_IMAGE_ID)) $criteria->add(SelProductPeer::MAIN_IMAGE_ID, $this->main_image_id);
        if ($this->isColumnModified(SelProductPeer::CODE)) $criteria->add(SelProductPeer::CODE, $this->code);
        if ($this->isColumnModified(SelProductPeer::NAME)) $criteria->add(SelProductPeer::NAME, $this->name);
        if ($this->isColumnModified(SelProductPeer::BASE_PRICE)) $criteria->add(SelProductPeer::BASE_PRICE, $this->base_price);
        if ($this->isColumnModified(SelProductPeer::DESCRIPTION)) $criteria->add(SelProductPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(SelProductPeer::MODEL)) $criteria->add(SelProductPeer::MODEL, $this->model);
        if ($this->isColumnModified(SelProductPeer::NUMBER_PARTS)) $criteria->add(SelProductPeer::NUMBER_PARTS, $this->number_parts);
        if ($this->isColumnModified(SelProductPeer::IS_MAIN)) $criteria->add(SelProductPeer::IS_MAIN, $this->is_main);
        if ($this->isColumnModified(SelProductPeer::IS_NEW)) $criteria->add(SelProductPeer::IS_NEW, $this->is_new);
        if ($this->isColumnModified(SelProductPeer::IS_OFFER)) $criteria->add(SelProductPeer::IS_OFFER, $this->is_offer);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelProductPeer::DATABASE_NAME);
        $criteria->add(SelProductPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelProduct (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setProductGroupId($this->getProductGroupId());
        $copyObj->setBrandId($this->getBrandId());
        $copyObj->setCategoryId($this->getCategoryId());
        $copyObj->setMainImageId($this->getMainImageId());
        $copyObj->setCode($this->getCode());
        $copyObj->setName($this->getName());
        $copyObj->setBasePrice($this->getBasePrice());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setModel($this->getModel());
        $copyObj->setNumberParts($this->getNumberParts());
        $copyObj->setIsMain($this->getIsMain());
        $copyObj->setIsNew($this->getIsNew());
        $copyObj->setIsOffer($this->getIsOffer());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelProductAcquisitions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProductAcquisition($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelProductImagesRelatedByProductId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProductImageRelatedByProductId($relObj->copy($deepCopy));
                }
            }

            $relObj = $this->getSelProductOffer();
            if ($relObj) {
                $copyObj->setSelProductOffer($relObj->copy($deepCopy));
            }

            foreach ($this->getSelProductOrders() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProductOrder($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelProductSells() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProductSell($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelProduct Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelProductPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelProductPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SelCategory object.
     *
     * @param             SelCategory $v
     * @return SelProduct The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelCategory(SelCategory $v = null)
    {
        if ($v === null) {
            $this->setCategoryId(NULL);
        } else {
            $this->setCategoryId($v->getId());
        }

        $this->aSelCategory = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelCategory object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProduct($this);
        }


        return $this;
    }


    /**
     * Get the associated SelCategory object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelCategory The associated SelCategory object.
     * @throws PropelException
     */
    public function getSelCategory(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelCategory === null && ($this->category_id !== null) && $doQuery) {
            $this->aSelCategory = SelCategoryQuery::create()->findPk($this->category_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelCategory->addSelProducts($this);
             */
        }

        return $this->aSelCategory;
    }

    /**
     * Declares an association between this object and a SelProductGroup object.
     *
     * @param             SelProductGroup $v
     * @return SelProduct The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelProductGroup(SelProductGroup $v = null)
    {
        if ($v === null) {
            $this->setProductGroupId(NULL);
        } else {
            $this->setProductGroupId($v->getId());
        }

        $this->aSelProductGroup = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelProductGroup object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProduct($this);
        }


        return $this;
    }


    /**
     * Get the associated SelProductGroup object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelProductGroup The associated SelProductGroup object.
     * @throws PropelException
     */
    public function getSelProductGroup(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelProductGroup === null && ($this->product_group_id !== null) && $doQuery) {
            $this->aSelProductGroup = SelProductGroupQuery::create()->findPk($this->product_group_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelProductGroup->addSelProducts($this);
             */
        }

        return $this->aSelProductGroup;
    }

    /**
     * Declares an association between this object and a SelBrand object.
     *
     * @param             SelBrand $v
     * @return SelProduct The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelBrand(SelBrand $v = null)
    {
        if ($v === null) {
            $this->setBrandId(NULL);
        } else {
            $this->setBrandId($v->getId());
        }

        $this->aSelBrand = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelBrand object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProduct($this);
        }


        return $this;
    }


    /**
     * Get the associated SelBrand object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelBrand The associated SelBrand object.
     * @throws PropelException
     */
    public function getSelBrand(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelBrand === null && ($this->brand_id !== null) && $doQuery) {
            $this->aSelBrand = SelBrandQuery::create()->findPk($this->brand_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelBrand->addSelProducts($this);
             */
        }

        return $this->aSelBrand;
    }

    /**
     * Declares an association between this object and a SelProductImage object.
     *
     * @param             SelProductImage $v
     * @return SelProduct The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelProductImageRelatedByMainImageId(SelProductImage $v = null)
    {
        if ($v === null) {
            $this->setMainImageId(NULL);
        } else {
            $this->setMainImageId($v->getId());
        }

        $this->aSelProductImageRelatedByMainImageId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelProductImage object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProductRelatedByMainImageId($this);
        }


        return $this;
    }


    /**
     * Get the associated SelProductImage object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelProductImage The associated SelProductImage object.
     * @throws PropelException
     */
    public function getSelProductImageRelatedByMainImageId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelProductImageRelatedByMainImageId === null && ($this->main_image_id !== null) && $doQuery) {
            $this->aSelProductImageRelatedByMainImageId = SelProductImageQuery::create()->findPk($this->main_image_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelProductImageRelatedByMainImageId->addSelProductsRelatedByMainImageId($this);
             */
        }

        return $this->aSelProductImageRelatedByMainImageId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelProductAcquisition' == $relationName) {
            $this->initSelProductAcquisitions();
        }
        if ('SelProductImageRelatedByProductId' == $relationName) {
            $this->initSelProductImagesRelatedByProductId();
        }
        if ('SelProductOrder' == $relationName) {
            $this->initSelProductOrders();
        }
        if ('SelProductSell' == $relationName) {
            $this->initSelProductSells();
        }
    }

    /**
     * Clears out the collSelProductAcquisitions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelProduct The current object (for fluent API support)
     * @see        addSelProductAcquisitions()
     */
    public function clearSelProductAcquisitions()
    {
        $this->collSelProductAcquisitions = null; // important to set this to null since that means it is uninitialized
        $this->collSelProductAcquisitionsPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProductAcquisitions collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProductAcquisitions($v = true)
    {
        $this->collSelProductAcquisitionsPartial = $v;
    }

    /**
     * Initializes the collSelProductAcquisitions collection.
     *
     * By default this just sets the collSelProductAcquisitions collection to an empty array (like clearcollSelProductAcquisitions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProductAcquisitions($overrideExisting = true)
    {
        if (null !== $this->collSelProductAcquisitions && !$overrideExisting) {
            return;
        }
        $this->collSelProductAcquisitions = new PropelObjectCollection();
        $this->collSelProductAcquisitions->setModel('SelProductAcquisition');
    }

    /**
     * Gets an array of SelProductAcquisition objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProductAcquisition[] List of SelProductAcquisition objects
     * @throws PropelException
     */
    public function getSelProductAcquisitions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProductAcquisitionsPartial && !$this->isNew();
        if (null === $this->collSelProductAcquisitions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProductAcquisitions) {
                // return empty collection
                $this->initSelProductAcquisitions();
            } else {
                $collSelProductAcquisitions = SelProductAcquisitionQuery::create(null, $criteria)
                    ->filterBySelProduct($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProductAcquisitionsPartial && count($collSelProductAcquisitions)) {
                      $this->initSelProductAcquisitions(false);

                      foreach($collSelProductAcquisitions as $obj) {
                        if (false == $this->collSelProductAcquisitions->contains($obj)) {
                          $this->collSelProductAcquisitions->append($obj);
                        }
                      }

                      $this->collSelProductAcquisitionsPartial = true;
                    }

                    return $collSelProductAcquisitions;
                }

                if($partial && $this->collSelProductAcquisitions) {
                    foreach($this->collSelProductAcquisitions as $obj) {
                        if($obj->isNew()) {
                            $collSelProductAcquisitions[] = $obj;
                        }
                    }
                }

                $this->collSelProductAcquisitions = $collSelProductAcquisitions;
                $this->collSelProductAcquisitionsPartial = false;
            }
        }

        return $this->collSelProductAcquisitions;
    }

    /**
     * Sets a collection of SelProductAcquisition objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProductAcquisitions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelProduct The current object (for fluent API support)
     */
    public function setSelProductAcquisitions(PropelCollection $selProductAcquisitions, PropelPDO $con = null)
    {
        $selProductAcquisitionsToDelete = $this->getSelProductAcquisitions(new Criteria(), $con)->diff($selProductAcquisitions);

        $this->selProductAcquisitionsScheduledForDeletion = unserialize(serialize($selProductAcquisitionsToDelete));

        foreach ($selProductAcquisitionsToDelete as $selProductAcquisitionRemoved) {
            $selProductAcquisitionRemoved->setSelProduct(null);
        }

        $this->collSelProductAcquisitions = null;
        foreach ($selProductAcquisitions as $selProductAcquisition) {
            $this->addSelProductAcquisition($selProductAcquisition);
        }

        $this->collSelProductAcquisitions = $selProductAcquisitions;
        $this->collSelProductAcquisitionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProductAcquisition objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProductAcquisition objects.
     * @throws PropelException
     */
    public function countSelProductAcquisitions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProductAcquisitionsPartial && !$this->isNew();
        if (null === $this->collSelProductAcquisitions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProductAcquisitions) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProductAcquisitions());
            }
            $query = SelProductAcquisitionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelProduct($this)
                ->count($con);
        }

        return count($this->collSelProductAcquisitions);
    }

    /**
     * Method called to associate a SelProductAcquisition object to this object
     * through the SelProductAcquisition foreign key attribute.
     *
     * @param    SelProductAcquisition $l SelProductAcquisition
     * @return SelProduct The current object (for fluent API support)
     */
    public function addSelProductAcquisition(SelProductAcquisition $l)
    {
        if ($this->collSelProductAcquisitions === null) {
            $this->initSelProductAcquisitions();
            $this->collSelProductAcquisitionsPartial = true;
        }
        if (!in_array($l, $this->collSelProductAcquisitions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProductAcquisition($l);
        }

        return $this;
    }

    /**
     * @param	SelProductAcquisition $selProductAcquisition The selProductAcquisition object to add.
     */
    protected function doAddSelProductAcquisition($selProductAcquisition)
    {
        $this->collSelProductAcquisitions[]= $selProductAcquisition;
        $selProductAcquisition->setSelProduct($this);
    }

    /**
     * @param	SelProductAcquisition $selProductAcquisition The selProductAcquisition object to remove.
     * @return SelProduct The current object (for fluent API support)
     */
    public function removeSelProductAcquisition($selProductAcquisition)
    {
        if ($this->getSelProductAcquisitions()->contains($selProductAcquisition)) {
            $this->collSelProductAcquisitions->remove($this->collSelProductAcquisitions->search($selProductAcquisition));
            if (null === $this->selProductAcquisitionsScheduledForDeletion) {
                $this->selProductAcquisitionsScheduledForDeletion = clone $this->collSelProductAcquisitions;
                $this->selProductAcquisitionsScheduledForDeletion->clear();
            }
            $this->selProductAcquisitionsScheduledForDeletion[]= clone $selProductAcquisition;
            $selProductAcquisition->setSelProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelProduct is new, it will return
     * an empty collection; or if this SelProduct has previously
     * been saved, it will retrieve related SelProductAcquisitions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelProduct.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProductAcquisition[] List of SelProductAcquisition objects
     */
    public function getSelProductAcquisitionsJoinSelAcquisition($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductAcquisitionQuery::create(null, $criteria);
        $query->joinWith('SelAcquisition', $join_behavior);

        return $this->getSelProductAcquisitions($query, $con);
    }

    /**
     * Clears out the collSelProductImagesRelatedByProductId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelProduct The current object (for fluent API support)
     * @see        addSelProductImagesRelatedByProductId()
     */
    public function clearSelProductImagesRelatedByProductId()
    {
        $this->collSelProductImagesRelatedByProductId = null; // important to set this to null since that means it is uninitialized
        $this->collSelProductImagesRelatedByProductIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProductImagesRelatedByProductId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProductImagesRelatedByProductId($v = true)
    {
        $this->collSelProductImagesRelatedByProductIdPartial = $v;
    }

    /**
     * Initializes the collSelProductImagesRelatedByProductId collection.
     *
     * By default this just sets the collSelProductImagesRelatedByProductId collection to an empty array (like clearcollSelProductImagesRelatedByProductId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProductImagesRelatedByProductId($overrideExisting = true)
    {
        if (null !== $this->collSelProductImagesRelatedByProductId && !$overrideExisting) {
            return;
        }
        $this->collSelProductImagesRelatedByProductId = new PropelObjectCollection();
        $this->collSelProductImagesRelatedByProductId->setModel('SelProductImage');
    }

    /**
     * Gets an array of SelProductImage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProductImage[] List of SelProductImage objects
     * @throws PropelException
     */
    public function getSelProductImagesRelatedByProductId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProductImagesRelatedByProductIdPartial && !$this->isNew();
        if (null === $this->collSelProductImagesRelatedByProductId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProductImagesRelatedByProductId) {
                // return empty collection
                $this->initSelProductImagesRelatedByProductId();
            } else {
                $collSelProductImagesRelatedByProductId = SelProductImageQuery::create(null, $criteria)
                    ->filterBySelProductRelatedByProductId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProductImagesRelatedByProductIdPartial && count($collSelProductImagesRelatedByProductId)) {
                      $this->initSelProductImagesRelatedByProductId(false);

                      foreach($collSelProductImagesRelatedByProductId as $obj) {
                        if (false == $this->collSelProductImagesRelatedByProductId->contains($obj)) {
                          $this->collSelProductImagesRelatedByProductId->append($obj);
                        }
                      }

                      $this->collSelProductImagesRelatedByProductIdPartial = true;
                    }

                    return $collSelProductImagesRelatedByProductId;
                }

                if($partial && $this->collSelProductImagesRelatedByProductId) {
                    foreach($this->collSelProductImagesRelatedByProductId as $obj) {
                        if($obj->isNew()) {
                            $collSelProductImagesRelatedByProductId[] = $obj;
                        }
                    }
                }

                $this->collSelProductImagesRelatedByProductId = $collSelProductImagesRelatedByProductId;
                $this->collSelProductImagesRelatedByProductIdPartial = false;
            }
        }

        return $this->collSelProductImagesRelatedByProductId;
    }

    /**
     * Sets a collection of SelProductImageRelatedByProductId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProductImagesRelatedByProductId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelProduct The current object (for fluent API support)
     */
    public function setSelProductImagesRelatedByProductId(PropelCollection $selProductImagesRelatedByProductId, PropelPDO $con = null)
    {
        $selProductImagesRelatedByProductIdToDelete = $this->getSelProductImagesRelatedByProductId(new Criteria(), $con)->diff($selProductImagesRelatedByProductId);

        $this->selProductImagesRelatedByProductIdScheduledForDeletion = unserialize(serialize($selProductImagesRelatedByProductIdToDelete));

        foreach ($selProductImagesRelatedByProductIdToDelete as $selProductImageRelatedByProductIdRemoved) {
            $selProductImageRelatedByProductIdRemoved->setSelProductRelatedByProductId(null);
        }

        $this->collSelProductImagesRelatedByProductId = null;
        foreach ($selProductImagesRelatedByProductId as $selProductImageRelatedByProductId) {
            $this->addSelProductImageRelatedByProductId($selProductImageRelatedByProductId);
        }

        $this->collSelProductImagesRelatedByProductId = $selProductImagesRelatedByProductId;
        $this->collSelProductImagesRelatedByProductIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProductImage objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProductImage objects.
     * @throws PropelException
     */
    public function countSelProductImagesRelatedByProductId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProductImagesRelatedByProductIdPartial && !$this->isNew();
        if (null === $this->collSelProductImagesRelatedByProductId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProductImagesRelatedByProductId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProductImagesRelatedByProductId());
            }
            $query = SelProductImageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelProductRelatedByProductId($this)
                ->count($con);
        }

        return count($this->collSelProductImagesRelatedByProductId);
    }

    /**
     * Method called to associate a SelProductImage object to this object
     * through the SelProductImage foreign key attribute.
     *
     * @param    SelProductImage $l SelProductImage
     * @return SelProduct The current object (for fluent API support)
     */
    public function addSelProductImageRelatedByProductId(SelProductImage $l)
    {
        if ($this->collSelProductImagesRelatedByProductId === null) {
            $this->initSelProductImagesRelatedByProductId();
            $this->collSelProductImagesRelatedByProductIdPartial = true;
        }
        if (!in_array($l, $this->collSelProductImagesRelatedByProductId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProductImageRelatedByProductId($l);
        }

        return $this;
    }

    /**
     * @param	SelProductImageRelatedByProductId $selProductImageRelatedByProductId The selProductImageRelatedByProductId object to add.
     */
    protected function doAddSelProductImageRelatedByProductId($selProductImageRelatedByProductId)
    {
        $this->collSelProductImagesRelatedByProductId[]= $selProductImageRelatedByProductId;
        $selProductImageRelatedByProductId->setSelProductRelatedByProductId($this);
    }

    /**
     * @param	SelProductImageRelatedByProductId $selProductImageRelatedByProductId The selProductImageRelatedByProductId object to remove.
     * @return SelProduct The current object (for fluent API support)
     */
    public function removeSelProductImageRelatedByProductId($selProductImageRelatedByProductId)
    {
        if ($this->getSelProductImagesRelatedByProductId()->contains($selProductImageRelatedByProductId)) {
            $this->collSelProductImagesRelatedByProductId->remove($this->collSelProductImagesRelatedByProductId->search($selProductImageRelatedByProductId));
            if (null === $this->selProductImagesRelatedByProductIdScheduledForDeletion) {
                $this->selProductImagesRelatedByProductIdScheduledForDeletion = clone $this->collSelProductImagesRelatedByProductId;
                $this->selProductImagesRelatedByProductIdScheduledForDeletion->clear();
            }
            $this->selProductImagesRelatedByProductIdScheduledForDeletion[]= clone $selProductImageRelatedByProductId;
            $selProductImageRelatedByProductId->setSelProductRelatedByProductId(null);
        }

        return $this;
    }

    /**
     * Gets a single SelProductOffer object, which is related to this object by a one-to-one relationship.
     *
     * @param PropelPDO $con optional connection object
     * @return SelProductOffer
     * @throws PropelException
     */
    public function getSelProductOffer(PropelPDO $con = null)
    {

        if ($this->singleSelProductOffer === null && !$this->isNew()) {
            $this->singleSelProductOffer = SelProductOfferQuery::create()->findPk($this->getPrimaryKey(), $con);
        }

        return $this->singleSelProductOffer;
    }

    /**
     * Sets a single SelProductOffer object as related to this object by a one-to-one relationship.
     *
     * @param             SelProductOffer $v SelProductOffer
     * @return SelProduct The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelProductOffer(SelProductOffer $v = null)
    {
        $this->singleSelProductOffer = $v;

        // Make sure that that the passed-in SelProductOffer isn't already associated with this object
        if ($v !== null && $v->getSelProduct(null, false) === null) {
            $v->setSelProduct($this);
        }

        return $this;
    }

    /**
     * Clears out the collSelProductOrders collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelProduct The current object (for fluent API support)
     * @see        addSelProductOrders()
     */
    public function clearSelProductOrders()
    {
        $this->collSelProductOrders = null; // important to set this to null since that means it is uninitialized
        $this->collSelProductOrdersPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProductOrders collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProductOrders($v = true)
    {
        $this->collSelProductOrdersPartial = $v;
    }

    /**
     * Initializes the collSelProductOrders collection.
     *
     * By default this just sets the collSelProductOrders collection to an empty array (like clearcollSelProductOrders());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProductOrders($overrideExisting = true)
    {
        if (null !== $this->collSelProductOrders && !$overrideExisting) {
            return;
        }
        $this->collSelProductOrders = new PropelObjectCollection();
        $this->collSelProductOrders->setModel('SelProductOrder');
    }

    /**
     * Gets an array of SelProductOrder objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProductOrder[] List of SelProductOrder objects
     * @throws PropelException
     */
    public function getSelProductOrders($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProductOrdersPartial && !$this->isNew();
        if (null === $this->collSelProductOrders || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProductOrders) {
                // return empty collection
                $this->initSelProductOrders();
            } else {
                $collSelProductOrders = SelProductOrderQuery::create(null, $criteria)
                    ->filterBySelProduct($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProductOrdersPartial && count($collSelProductOrders)) {
                      $this->initSelProductOrders(false);

                      foreach($collSelProductOrders as $obj) {
                        if (false == $this->collSelProductOrders->contains($obj)) {
                          $this->collSelProductOrders->append($obj);
                        }
                      }

                      $this->collSelProductOrdersPartial = true;
                    }

                    return $collSelProductOrders;
                }

                if($partial && $this->collSelProductOrders) {
                    foreach($this->collSelProductOrders as $obj) {
                        if($obj->isNew()) {
                            $collSelProductOrders[] = $obj;
                        }
                    }
                }

                $this->collSelProductOrders = $collSelProductOrders;
                $this->collSelProductOrdersPartial = false;
            }
        }

        return $this->collSelProductOrders;
    }

    /**
     * Sets a collection of SelProductOrder objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProductOrders A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelProduct The current object (for fluent API support)
     */
    public function setSelProductOrders(PropelCollection $selProductOrders, PropelPDO $con = null)
    {
        $selProductOrdersToDelete = $this->getSelProductOrders(new Criteria(), $con)->diff($selProductOrders);

        $this->selProductOrdersScheduledForDeletion = unserialize(serialize($selProductOrdersToDelete));

        foreach ($selProductOrdersToDelete as $selProductOrderRemoved) {
            $selProductOrderRemoved->setSelProduct(null);
        }

        $this->collSelProductOrders = null;
        foreach ($selProductOrders as $selProductOrder) {
            $this->addSelProductOrder($selProductOrder);
        }

        $this->collSelProductOrders = $selProductOrders;
        $this->collSelProductOrdersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProductOrder objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProductOrder objects.
     * @throws PropelException
     */
    public function countSelProductOrders(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProductOrdersPartial && !$this->isNew();
        if (null === $this->collSelProductOrders || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProductOrders) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProductOrders());
            }
            $query = SelProductOrderQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelProduct($this)
                ->count($con);
        }

        return count($this->collSelProductOrders);
    }

    /**
     * Method called to associate a SelProductOrder object to this object
     * through the SelProductOrder foreign key attribute.
     *
     * @param    SelProductOrder $l SelProductOrder
     * @return SelProduct The current object (for fluent API support)
     */
    public function addSelProductOrder(SelProductOrder $l)
    {
        if ($this->collSelProductOrders === null) {
            $this->initSelProductOrders();
            $this->collSelProductOrdersPartial = true;
        }
        if (!in_array($l, $this->collSelProductOrders->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProductOrder($l);
        }

        return $this;
    }

    /**
     * @param	SelProductOrder $selProductOrder The selProductOrder object to add.
     */
    protected function doAddSelProductOrder($selProductOrder)
    {
        $this->collSelProductOrders[]= $selProductOrder;
        $selProductOrder->setSelProduct($this);
    }

    /**
     * @param	SelProductOrder $selProductOrder The selProductOrder object to remove.
     * @return SelProduct The current object (for fluent API support)
     */
    public function removeSelProductOrder($selProductOrder)
    {
        if ($this->getSelProductOrders()->contains($selProductOrder)) {
            $this->collSelProductOrders->remove($this->collSelProductOrders->search($selProductOrder));
            if (null === $this->selProductOrdersScheduledForDeletion) {
                $this->selProductOrdersScheduledForDeletion = clone $this->collSelProductOrders;
                $this->selProductOrdersScheduledForDeletion->clear();
            }
            $this->selProductOrdersScheduledForDeletion[]= clone $selProductOrder;
            $selProductOrder->setSelProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelProduct is new, it will return
     * an empty collection; or if this SelProduct has previously
     * been saved, it will retrieve related SelProductOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelProduct.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProductOrder[] List of SelProductOrder objects
     */
    public function getSelProductOrdersJoinSelOrder($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductOrderQuery::create(null, $criteria);
        $query->joinWith('SelOrder', $join_behavior);

        return $this->getSelProductOrders($query, $con);
    }

    /**
     * Clears out the collSelProductSells collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelProduct The current object (for fluent API support)
     * @see        addSelProductSells()
     */
    public function clearSelProductSells()
    {
        $this->collSelProductSells = null; // important to set this to null since that means it is uninitialized
        $this->collSelProductSellsPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProductSells collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProductSells($v = true)
    {
        $this->collSelProductSellsPartial = $v;
    }

    /**
     * Initializes the collSelProductSells collection.
     *
     * By default this just sets the collSelProductSells collection to an empty array (like clearcollSelProductSells());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProductSells($overrideExisting = true)
    {
        if (null !== $this->collSelProductSells && !$overrideExisting) {
            return;
        }
        $this->collSelProductSells = new PropelObjectCollection();
        $this->collSelProductSells->setModel('SelProductSell');
    }

    /**
     * Gets an array of SelProductSell objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProductSell[] List of SelProductSell objects
     * @throws PropelException
     */
    public function getSelProductSells($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProductSellsPartial && !$this->isNew();
        if (null === $this->collSelProductSells || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProductSells) {
                // return empty collection
                $this->initSelProductSells();
            } else {
                $collSelProductSells = SelProductSellQuery::create(null, $criteria)
                    ->filterBySelProduct($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProductSellsPartial && count($collSelProductSells)) {
                      $this->initSelProductSells(false);

                      foreach($collSelProductSells as $obj) {
                        if (false == $this->collSelProductSells->contains($obj)) {
                          $this->collSelProductSells->append($obj);
                        }
                      }

                      $this->collSelProductSellsPartial = true;
                    }

                    return $collSelProductSells;
                }

                if($partial && $this->collSelProductSells) {
                    foreach($this->collSelProductSells as $obj) {
                        if($obj->isNew()) {
                            $collSelProductSells[] = $obj;
                        }
                    }
                }

                $this->collSelProductSells = $collSelProductSells;
                $this->collSelProductSellsPartial = false;
            }
        }

        return $this->collSelProductSells;
    }

    /**
     * Sets a collection of SelProductSell objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProductSells A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelProduct The current object (for fluent API support)
     */
    public function setSelProductSells(PropelCollection $selProductSells, PropelPDO $con = null)
    {
        $selProductSellsToDelete = $this->getSelProductSells(new Criteria(), $con)->diff($selProductSells);

        $this->selProductSellsScheduledForDeletion = unserialize(serialize($selProductSellsToDelete));

        foreach ($selProductSellsToDelete as $selProductSellRemoved) {
            $selProductSellRemoved->setSelProduct(null);
        }

        $this->collSelProductSells = null;
        foreach ($selProductSells as $selProductSell) {
            $this->addSelProductSell($selProductSell);
        }

        $this->collSelProductSells = $selProductSells;
        $this->collSelProductSellsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProductSell objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProductSell objects.
     * @throws PropelException
     */
    public function countSelProductSells(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProductSellsPartial && !$this->isNew();
        if (null === $this->collSelProductSells || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProductSells) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProductSells());
            }
            $query = SelProductSellQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelProduct($this)
                ->count($con);
        }

        return count($this->collSelProductSells);
    }

    /**
     * Method called to associate a SelProductSell object to this object
     * through the SelProductSell foreign key attribute.
     *
     * @param    SelProductSell $l SelProductSell
     * @return SelProduct The current object (for fluent API support)
     */
    public function addSelProductSell(SelProductSell $l)
    {
        if ($this->collSelProductSells === null) {
            $this->initSelProductSells();
            $this->collSelProductSellsPartial = true;
        }
        if (!in_array($l, $this->collSelProductSells->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProductSell($l);
        }

        return $this;
    }

    /**
     * @param	SelProductSell $selProductSell The selProductSell object to add.
     */
    protected function doAddSelProductSell($selProductSell)
    {
        $this->collSelProductSells[]= $selProductSell;
        $selProductSell->setSelProduct($this);
    }

    /**
     * @param	SelProductSell $selProductSell The selProductSell object to remove.
     * @return SelProduct The current object (for fluent API support)
     */
    public function removeSelProductSell($selProductSell)
    {
        if ($this->getSelProductSells()->contains($selProductSell)) {
            $this->collSelProductSells->remove($this->collSelProductSells->search($selProductSell));
            if (null === $this->selProductSellsScheduledForDeletion) {
                $this->selProductSellsScheduledForDeletion = clone $this->collSelProductSells;
                $this->selProductSellsScheduledForDeletion->clear();
            }
            $this->selProductSellsScheduledForDeletion[]= clone $selProductSell;
            $selProductSell->setSelProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelProduct is new, it will return
     * an empty collection; or if this SelProduct has previously
     * been saved, it will retrieve related SelProductSells from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelProduct.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProductSell[] List of SelProductSell objects
     */
    public function getSelProductSellsJoinSelSale($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductSellQuery::create(null, $criteria);
        $query->joinWith('SelSale', $join_behavior);

        return $this->getSelProductSells($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->product_group_id = null;
        $this->brand_id = null;
        $this->category_id = null;
        $this->main_image_id = null;
        $this->code = null;
        $this->name = null;
        $this->base_price = null;
        $this->description = null;
        $this->model = null;
        $this->number_parts = null;
        $this->is_main = null;
        $this->is_new = null;
        $this->is_offer = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSelProductAcquisitions) {
                foreach ($this->collSelProductAcquisitions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelProductImagesRelatedByProductId) {
                foreach ($this->collSelProductImagesRelatedByProductId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->singleSelProductOffer) {
                $this->singleSelProductOffer->clearAllReferences($deep);
            }
            if ($this->collSelProductOrders) {
                foreach ($this->collSelProductOrders as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelProductSells) {
                foreach ($this->collSelProductSells as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collSelProductAcquisitions instanceof PropelCollection) {
            $this->collSelProductAcquisitions->clearIterator();
        }
        $this->collSelProductAcquisitions = null;
        if ($this->collSelProductImagesRelatedByProductId instanceof PropelCollection) {
            $this->collSelProductImagesRelatedByProductId->clearIterator();
        }
        $this->collSelProductImagesRelatedByProductId = null;
        if ($this->singleSelProductOffer instanceof PropelCollection) {
            $this->singleSelProductOffer->clearIterator();
        }
        $this->singleSelProductOffer = null;
        if ($this->collSelProductOrders instanceof PropelCollection) {
            $this->collSelProductOrders->clearIterator();
        }
        $this->collSelProductOrders = null;
        if ($this->collSelProductSells instanceof PropelCollection) {
            $this->collSelProductSells->clearIterator();
        }
        $this->collSelProductSells = null;
        $this->aSelCategory = null;
        $this->aSelProductGroup = null;
        $this->aSelBrand = null;
        $this->aSelProductImageRelatedByMainImageId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelProductPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
