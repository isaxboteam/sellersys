<?php


/**
 * Base class that represents a query for the 'sel_employee' table.
 *
 * 
 *
 * @method SelEmployeeQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelEmployeeQuery orderByUserId($order = Criteria::ASC) Order by the USER_ID column
 * @method SelEmployeeQuery orderByInitDate($order = Criteria::ASC) Order by the INIT_DATE column
 * @method SelEmployeeQuery orderByEndDate($order = Criteria::ASC) Order by the END_DATE column
 * @method SelEmployeeQuery orderByDetails($order = Criteria::ASC) Order by the DETAILS column
 *
 * @method SelEmployeeQuery groupById() Group by the ID column
 * @method SelEmployeeQuery groupByUserId() Group by the USER_ID column
 * @method SelEmployeeQuery groupByInitDate() Group by the INIT_DATE column
 * @method SelEmployeeQuery groupByEndDate() Group by the END_DATE column
 * @method SelEmployeeQuery groupByDetails() Group by the DETAILS column
 *
 * @method SelEmployeeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelEmployeeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelEmployeeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelEmployeeQuery leftJoinChoUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChoUser relation
 * @method SelEmployeeQuery rightJoinChoUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChoUser relation
 * @method SelEmployeeQuery innerJoinChoUser($relationAlias = null) Adds a INNER JOIN clause to the query using the ChoUser relation
 *
 * @method SelEmployeeQuery leftJoinSelBranchEmployee($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelBranchEmployee relation
 * @method SelEmployeeQuery rightJoinSelBranchEmployee($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelBranchEmployee relation
 * @method SelEmployeeQuery innerJoinSelBranchEmployee($relationAlias = null) Adds a INNER JOIN clause to the query using the SelBranchEmployee relation
 *
 * @method SelEmployeeQuery leftJoinSelSale($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelSale relation
 * @method SelEmployeeQuery rightJoinSelSale($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelSale relation
 * @method SelEmployeeQuery innerJoinSelSale($relationAlias = null) Adds a INNER JOIN clause to the query using the SelSale relation
 *
 * @method SelEmployee findOne(PropelPDO $con = null) Return the first SelEmployee matching the query
 * @method SelEmployee findOneOrCreate(PropelPDO $con = null) Return the first SelEmployee matching the query, or a new SelEmployee object populated from the query conditions when no match is found
 *
 * @method SelEmployee findOneByUserId(int $USER_ID) Return the first SelEmployee filtered by the USER_ID column
 * @method SelEmployee findOneByInitDate(string $INIT_DATE) Return the first SelEmployee filtered by the INIT_DATE column
 * @method SelEmployee findOneByEndDate(string $END_DATE) Return the first SelEmployee filtered by the END_DATE column
 * @method SelEmployee findOneByDetails(string $DETAILS) Return the first SelEmployee filtered by the DETAILS column
 *
 * @method array findById(int $ID) Return SelEmployee objects filtered by the ID column
 * @method array findByUserId(int $USER_ID) Return SelEmployee objects filtered by the USER_ID column
 * @method array findByInitDate(string $INIT_DATE) Return SelEmployee objects filtered by the INIT_DATE column
 * @method array findByEndDate(string $END_DATE) Return SelEmployee objects filtered by the END_DATE column
 * @method array findByDetails(string $DETAILS) Return SelEmployee objects filtered by the DETAILS column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelEmployeeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelEmployeeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelEmployee', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelEmployeeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelEmployeeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelEmployeeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelEmployeeQuery) {
            return $criteria;
        }
        $query = new SelEmployeeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelEmployee|SelEmployee[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelEmployeePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelEmployeePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelEmployee A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelEmployee A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `USER_ID`, `INIT_DATE`, `END_DATE`, `DETAILS` FROM `sel_employee` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelEmployee();
            $obj->hydrate($row);
            SelEmployeePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelEmployee|SelEmployee[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelEmployee[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelEmployeeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelEmployeePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelEmployeeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelEmployeePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelEmployeeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelEmployeePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the USER_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE USER_ID = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE USER_ID IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE USER_ID > 12
     * </code>
     *
     * @see       filterByChoUser()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelEmployeeQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(SelEmployeePeer::USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(SelEmployeePeer::USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelEmployeePeer::USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the INIT_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByInitDate('2011-03-14'); // WHERE INIT_DATE = '2011-03-14'
     * $query->filterByInitDate('now'); // WHERE INIT_DATE = '2011-03-14'
     * $query->filterByInitDate(array('max' => 'yesterday')); // WHERE INIT_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $initDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelEmployeeQuery The current query, for fluid interface
     */
    public function filterByInitDate($initDate = null, $comparison = null)
    {
        if (is_array($initDate)) {
            $useMinMax = false;
            if (isset($initDate['min'])) {
                $this->addUsingAlias(SelEmployeePeer::INIT_DATE, $initDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($initDate['max'])) {
                $this->addUsingAlias(SelEmployeePeer::INIT_DATE, $initDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelEmployeePeer::INIT_DATE, $initDate, $comparison);
    }

    /**
     * Filter the query on the END_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByEndDate('2011-03-14'); // WHERE END_DATE = '2011-03-14'
     * $query->filterByEndDate('now'); // WHERE END_DATE = '2011-03-14'
     * $query->filterByEndDate(array('max' => 'yesterday')); // WHERE END_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $endDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelEmployeeQuery The current query, for fluid interface
     */
    public function filterByEndDate($endDate = null, $comparison = null)
    {
        if (is_array($endDate)) {
            $useMinMax = false;
            if (isset($endDate['min'])) {
                $this->addUsingAlias(SelEmployeePeer::END_DATE, $endDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endDate['max'])) {
                $this->addUsingAlias(SelEmployeePeer::END_DATE, $endDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelEmployeePeer::END_DATE, $endDate, $comparison);
    }

    /**
     * Filter the query on the DETAILS column
     *
     * Example usage:
     * <code>
     * $query->filterByDetails('fooValue');   // WHERE DETAILS = 'fooValue'
     * $query->filterByDetails('%fooValue%'); // WHERE DETAILS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $details The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelEmployeeQuery The current query, for fluid interface
     */
    public function filterByDetails($details = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($details)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $details)) {
                $details = str_replace('*', '%', $details);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelEmployeePeer::DETAILS, $details, $comparison);
    }

    /**
     * Filter the query by a related ChoUser object
     *
     * @param   ChoUser|PropelObjectCollection $choUser The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelEmployeeQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByChoUser($choUser, $comparison = null)
    {
        if ($choUser instanceof ChoUser) {
            return $this
                ->addUsingAlias(SelEmployeePeer::USER_ID, $choUser->getId(), $comparison);
        } elseif ($choUser instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelEmployeePeer::USER_ID, $choUser->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChoUser() only accepts arguments of type ChoUser or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChoUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelEmployeeQuery The current query, for fluid interface
     */
    public function joinChoUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChoUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChoUser');
        }

        return $this;
    }

    /**
     * Use the ChoUser relation ChoUser object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ChoUserQuery A secondary query class using the current class as primary query
     */
    public function useChoUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChoUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChoUser', 'ChoUserQuery');
    }

    /**
     * Filter the query by a related SelBranchEmployee object
     *
     * @param   SelBranchEmployee|PropelObjectCollection $selBranchEmployee  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelEmployeeQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelBranchEmployee($selBranchEmployee, $comparison = null)
    {
        if ($selBranchEmployee instanceof SelBranchEmployee) {
            return $this
                ->addUsingAlias(SelEmployeePeer::ID, $selBranchEmployee->getEmployeeId(), $comparison);
        } elseif ($selBranchEmployee instanceof PropelObjectCollection) {
            return $this
                ->useSelBranchEmployeeQuery()
                ->filterByPrimaryKeys($selBranchEmployee->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelBranchEmployee() only accepts arguments of type SelBranchEmployee or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelBranchEmployee relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelEmployeeQuery The current query, for fluid interface
     */
    public function joinSelBranchEmployee($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelBranchEmployee');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelBranchEmployee');
        }

        return $this;
    }

    /**
     * Use the SelBranchEmployee relation SelBranchEmployee object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelBranchEmployeeQuery A secondary query class using the current class as primary query
     */
    public function useSelBranchEmployeeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelBranchEmployee($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelBranchEmployee', 'SelBranchEmployeeQuery');
    }

    /**
     * Filter the query by a related SelSale object
     *
     * @param   SelSale|PropelObjectCollection $selSale  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelEmployeeQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelSale($selSale, $comparison = null)
    {
        if ($selSale instanceof SelSale) {
            return $this
                ->addUsingAlias(SelEmployeePeer::ID, $selSale->getEmployeeId(), $comparison);
        } elseif ($selSale instanceof PropelObjectCollection) {
            return $this
                ->useSelSaleQuery()
                ->filterByPrimaryKeys($selSale->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelSale() only accepts arguments of type SelSale or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelSale relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelEmployeeQuery The current query, for fluid interface
     */
    public function joinSelSale($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelSale');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelSale');
        }

        return $this;
    }

    /**
     * Use the SelSale relation SelSale object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelSaleQuery A secondary query class using the current class as primary query
     */
    public function useSelSaleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelSale($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelSale', 'SelSaleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelEmployee $selEmployee Object to remove from the list of results
     *
     * @return SelEmployeeQuery The current query, for fluid interface
     */
    public function prune($selEmployee = null)
    {
        if ($selEmployee) {
            $this->addUsingAlias(SelEmployeePeer::ID, $selEmployee->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
