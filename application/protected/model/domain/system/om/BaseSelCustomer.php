<?php


/**
 * Base class that represents a row from the 'sel_customer' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelCustomer extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelCustomerPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelCustomerPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the customer_account_id field.
     * @var        int
     */
    protected $customer_account_id;

    /**
     * The value for the id_number field.
     * @var        string
     */
    protected $id_number;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the lastname field.
     * @var        string
     */
    protected $lastname;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the birthday field.
     * @var        string
     */
    protected $birthday;

    /**
     * The value for the address field.
     * @var        string
     */
    protected $address;

    /**
     * The value for the phone field.
     * @var        string
     */
    protected $phone;

    /**
     * The value for the cellphone field.
     * @var        string
     */
    protected $cellphone;

    /**
     * The value for the status field.
     * @var        string
     */
    protected $status;

    /**
     * The value for the code field.
     * @var        string
     */
    protected $code;

    /**
     * @var        SelCustomerAccount
     */
    protected $aSelCustomerAccount;

    /**
     * @var        PropelObjectCollection|SelOrder[] Collection to store aggregation of SelOrder objects.
     */
    protected $collSelOrders;
    protected $collSelOrdersPartial;

    /**
     * @var        PropelObjectCollection|SelSale[] Collection to store aggregation of SelSale objects.
     */
    protected $collSelSales;
    protected $collSelSalesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selOrdersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selSalesScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [customer_account_id] column value.
     * 
     * @return int
     */
    public function getCustomerAccountId()
    {
        return $this->customer_account_id;
    }

    /**
     * Get the [id_number] column value.
     * 
     * @return string
     */
    public function getIdNumber()
    {
        return $this->id_number;
    }

    /**
     * Get the [name] column value.
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [lastname] column value.
     * 
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Get the [email] column value.
     * 
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [optionally formatted] temporal [birthday] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getBirthday($format = '%x')
    {
        if ($this->birthday === null) {
            return null;
        }

        if ($this->birthday === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->birthday);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->birthday, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [address] column value.
     * 
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get the [phone] column value.
     * 
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get the [cellphone] column value.
     * 
     * @return string
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * Get the [status] column value.
     * 
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [code] column value.
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelCustomerPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [customer_account_id] column.
     * 
     * @param int $v new value
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setCustomerAccountId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->customer_account_id !== $v) {
            $this->customer_account_id = $v;
            $this->modifiedColumns[] = SelCustomerPeer::CUSTOMER_ACCOUNT_ID;
        }

        if ($this->aSelCustomerAccount !== null && $this->aSelCustomerAccount->getId() !== $v) {
            $this->aSelCustomerAccount = null;
        }


        return $this;
    } // setCustomerAccountId()

    /**
     * Set the value of [id_number] column.
     * 
     * @param string $v new value
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setIdNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_number !== $v) {
            $this->id_number = $v;
            $this->modifiedColumns[] = SelCustomerPeer::ID_NUMBER;
        }


        return $this;
    } // setIdNumber()

    /**
     * Set the value of [name] column.
     * 
     * @param string $v new value
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = SelCustomerPeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [lastname] column.
     * 
     * @param string $v new value
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setLastname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->lastname !== $v) {
            $this->lastname = $v;
            $this->modifiedColumns[] = SelCustomerPeer::LASTNAME;
        }


        return $this;
    } // setLastname()

    /**
     * Set the value of [email] column.
     * 
     * @param string $v new value
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = SelCustomerPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Sets the value of [birthday] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setBirthday($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->birthday !== null || $dt !== null) {
            $currentDateAsString = ($this->birthday !== null && $tmpDt = new DateTime($this->birthday)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->birthday = $newDateAsString;
                $this->modifiedColumns[] = SelCustomerPeer::BIRTHDAY;
            }
        } // if either are not null


        return $this;
    } // setBirthday()

    /**
     * Set the value of [address] column.
     * 
     * @param string $v new value
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->address !== $v) {
            $this->address = $v;
            $this->modifiedColumns[] = SelCustomerPeer::ADDRESS;
        }


        return $this;
    } // setAddress()

    /**
     * Set the value of [phone] column.
     * 
     * @param string $v new value
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[] = SelCustomerPeer::PHONE;
        }


        return $this;
    } // setPhone()

    /**
     * Set the value of [cellphone] column.
     * 
     * @param string $v new value
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setCellphone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cellphone !== $v) {
            $this->cellphone = $v;
            $this->modifiedColumns[] = SelCustomerPeer::CELLPHONE;
        }


        return $this;
    } // setCellphone()

    /**
     * Set the value of [status] column.
     * 
     * @param string $v new value
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[] = SelCustomerPeer::STATUS;
        }


        return $this;
    } // setStatus()

    /**
     * Set the value of [code] column.
     * 
     * @param string $v new value
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[] = SelCustomerPeer::CODE;
        }


        return $this;
    } // setCode()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->customer_account_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->id_number = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->name = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->lastname = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->email = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->birthday = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->address = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->phone = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->cellphone = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->status = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->code = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 12; // 12 = SelCustomerPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelCustomer object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSelCustomerAccount !== null && $this->customer_account_id !== $this->aSelCustomerAccount->getId()) {
            $this->aSelCustomerAccount = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelCustomerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelCustomerPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSelCustomerAccount = null;
            $this->collSelOrders = null;

            $this->collSelSales = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelCustomerPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelCustomerQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelCustomerPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelCustomerPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelCustomerAccount !== null) {
                if ($this->aSelCustomerAccount->isModified() || $this->aSelCustomerAccount->isNew()) {
                    $affectedRows += $this->aSelCustomerAccount->save($con);
                }
                $this->setSelCustomerAccount($this->aSelCustomerAccount);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->selOrdersScheduledForDeletion !== null) {
                if (!$this->selOrdersScheduledForDeletion->isEmpty()) {
                    SelOrderQuery::create()
                        ->filterByPrimaryKeys($this->selOrdersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selOrdersScheduledForDeletion = null;
                }
            }

            if ($this->collSelOrders !== null) {
                foreach ($this->collSelOrders as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selSalesScheduledForDeletion !== null) {
                if (!$this->selSalesScheduledForDeletion->isEmpty()) {
                    SelSaleQuery::create()
                        ->filterByPrimaryKeys($this->selSalesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selSalesScheduledForDeletion = null;
                }
            }

            if ($this->collSelSales !== null) {
                foreach ($this->collSelSales as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SelCustomerPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SelCustomerPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelCustomerPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelCustomerPeer::CUSTOMER_ACCOUNT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`CUSTOMER_ACCOUNT_ID`';
        }
        if ($this->isColumnModified(SelCustomerPeer::ID_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = '`ID_NUMBER`';
        }
        if ($this->isColumnModified(SelCustomerPeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`NAME`';
        }
        if ($this->isColumnModified(SelCustomerPeer::LASTNAME)) {
            $modifiedColumns[':p' . $index++]  = '`LASTNAME`';
        }
        if ($this->isColumnModified(SelCustomerPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`EMAIL`';
        }
        if ($this->isColumnModified(SelCustomerPeer::BIRTHDAY)) {
            $modifiedColumns[':p' . $index++]  = '`BIRTHDAY`';
        }
        if ($this->isColumnModified(SelCustomerPeer::ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`ADDRESS`';
        }
        if ($this->isColumnModified(SelCustomerPeer::PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`PHONE`';
        }
        if ($this->isColumnModified(SelCustomerPeer::CELLPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`CELLPHONE`';
        }
        if ($this->isColumnModified(SelCustomerPeer::STATUS)) {
            $modifiedColumns[':p' . $index++]  = '`STATUS`';
        }
        if ($this->isColumnModified(SelCustomerPeer::CODE)) {
            $modifiedColumns[':p' . $index++]  = '`CODE`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_customer` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`CUSTOMER_ACCOUNT_ID`':						
                        $stmt->bindValue($identifier, $this->customer_account_id, PDO::PARAM_INT);
                        break;
                    case '`ID_NUMBER`':						
                        $stmt->bindValue($identifier, $this->id_number, PDO::PARAM_STR);
                        break;
                    case '`NAME`':						
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`LASTNAME`':						
                        $stmt->bindValue($identifier, $this->lastname, PDO::PARAM_STR);
                        break;
                    case '`EMAIL`':						
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`BIRTHDAY`':						
                        $stmt->bindValue($identifier, $this->birthday, PDO::PARAM_STR);
                        break;
                    case '`ADDRESS`':						
                        $stmt->bindValue($identifier, $this->address, PDO::PARAM_STR);
                        break;
                    case '`PHONE`':						
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case '`CELLPHONE`':						
                        $stmt->bindValue($identifier, $this->cellphone, PDO::PARAM_STR);
                        break;
                    case '`STATUS`':						
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_STR);
                        break;
                    case '`CODE`':						
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelCustomerAccount !== null) {
                if (!$this->aSelCustomerAccount->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelCustomerAccount->getValidationFailures());
                }
            }

            if (($retval = SelCustomerPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelOrders !== null) {
                    foreach ($this->collSelOrders as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelSales !== null) {
                    foreach ($this->collSelSales as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelCustomerPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getCustomerAccountId();
                break;
            case 2:
                return $this->getIdNumber();
                break;
            case 3:
                return $this->getName();
                break;
            case 4:
                return $this->getLastname();
                break;
            case 5:
                return $this->getEmail();
                break;
            case 6:
                return $this->getBirthday();
                break;
            case 7:
                return $this->getAddress();
                break;
            case 8:
                return $this->getPhone();
                break;
            case 9:
                return $this->getCellphone();
                break;
            case 10:
                return $this->getStatus();
                break;
            case 11:
                return $this->getCode();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelCustomer'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelCustomer'][$this->getPrimaryKey()] = true;
        $keys = SelCustomerPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getCustomerAccountId(),
            $keys[2] => $this->getIdNumber(),
            $keys[3] => $this->getName(),
            $keys[4] => $this->getLastname(),
            $keys[5] => $this->getEmail(),
            $keys[6] => $this->getBirthday(),
            $keys[7] => $this->getAddress(),
            $keys[8] => $this->getPhone(),
            $keys[9] => $this->getCellphone(),
            $keys[10] => $this->getStatus(),
            $keys[11] => $this->getCode(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSelCustomerAccount) {
                $result['SelCustomerAccount'] = $this->aSelCustomerAccount->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSelOrders) {
                $result['SelOrders'] = $this->collSelOrders->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelSales) {
                $result['SelSales'] = $this->collSelSales->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelCustomerPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setCustomerAccountId($value);
                break;
            case 2:
                $this->setIdNumber($value);
                break;
            case 3:
                $this->setName($value);
                break;
            case 4:
                $this->setLastname($value);
                break;
            case 5:
                $this->setEmail($value);
                break;
            case 6:
                $this->setBirthday($value);
                break;
            case 7:
                $this->setAddress($value);
                break;
            case 8:
                $this->setPhone($value);
                break;
            case 9:
                $this->setCellphone($value);
                break;
            case 10:
                $this->setStatus($value);
                break;
            case 11:
                $this->setCode($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelCustomerPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setCustomerAccountId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdNumber($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setName($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setLastname($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setEmail($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setBirthday($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setAddress($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setPhone($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setCellphone($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setStatus($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setCode($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelCustomerPeer::DATABASE_NAME);

        if ($this->isColumnModified(SelCustomerPeer::ID)) $criteria->add(SelCustomerPeer::ID, $this->id);
        if ($this->isColumnModified(SelCustomerPeer::CUSTOMER_ACCOUNT_ID)) $criteria->add(SelCustomerPeer::CUSTOMER_ACCOUNT_ID, $this->customer_account_id);
        if ($this->isColumnModified(SelCustomerPeer::ID_NUMBER)) $criteria->add(SelCustomerPeer::ID_NUMBER, $this->id_number);
        if ($this->isColumnModified(SelCustomerPeer::NAME)) $criteria->add(SelCustomerPeer::NAME, $this->name);
        if ($this->isColumnModified(SelCustomerPeer::LASTNAME)) $criteria->add(SelCustomerPeer::LASTNAME, $this->lastname);
        if ($this->isColumnModified(SelCustomerPeer::EMAIL)) $criteria->add(SelCustomerPeer::EMAIL, $this->email);
        if ($this->isColumnModified(SelCustomerPeer::BIRTHDAY)) $criteria->add(SelCustomerPeer::BIRTHDAY, $this->birthday);
        if ($this->isColumnModified(SelCustomerPeer::ADDRESS)) $criteria->add(SelCustomerPeer::ADDRESS, $this->address);
        if ($this->isColumnModified(SelCustomerPeer::PHONE)) $criteria->add(SelCustomerPeer::PHONE, $this->phone);
        if ($this->isColumnModified(SelCustomerPeer::CELLPHONE)) $criteria->add(SelCustomerPeer::CELLPHONE, $this->cellphone);
        if ($this->isColumnModified(SelCustomerPeer::STATUS)) $criteria->add(SelCustomerPeer::STATUS, $this->status);
        if ($this->isColumnModified(SelCustomerPeer::CODE)) $criteria->add(SelCustomerPeer::CODE, $this->code);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelCustomerPeer::DATABASE_NAME);
        $criteria->add(SelCustomerPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelCustomer (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setCustomerAccountId($this->getCustomerAccountId());
        $copyObj->setIdNumber($this->getIdNumber());
        $copyObj->setName($this->getName());
        $copyObj->setLastname($this->getLastname());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setBirthday($this->getBirthday());
        $copyObj->setAddress($this->getAddress());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setCellphone($this->getCellphone());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setCode($this->getCode());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelOrders() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelOrder($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelSales() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelSale($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelCustomer Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelCustomerPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelCustomerPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SelCustomerAccount object.
     *
     * @param             SelCustomerAccount $v
     * @return SelCustomer The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelCustomerAccount(SelCustomerAccount $v = null)
    {
        if ($v === null) {
            $this->setCustomerAccountId(NULL);
        } else {
            $this->setCustomerAccountId($v->getId());
        }

        $this->aSelCustomerAccount = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelCustomerAccount object, it will not be re-added.
        if ($v !== null) {
            $v->addSelCustomer($this);
        }


        return $this;
    }


    /**
     * Get the associated SelCustomerAccount object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelCustomerAccount The associated SelCustomerAccount object.
     * @throws PropelException
     */
    public function getSelCustomerAccount(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelCustomerAccount === null && ($this->customer_account_id !== null) && $doQuery) {
            $this->aSelCustomerAccount = SelCustomerAccountQuery::create()->findPk($this->customer_account_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelCustomerAccount->addSelCustomers($this);
             */
        }

        return $this->aSelCustomerAccount;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelOrder' == $relationName) {
            $this->initSelOrders();
        }
        if ('SelSale' == $relationName) {
            $this->initSelSales();
        }
    }

    /**
     * Clears out the collSelOrders collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelCustomer The current object (for fluent API support)
     * @see        addSelOrders()
     */
    public function clearSelOrders()
    {
        $this->collSelOrders = null; // important to set this to null since that means it is uninitialized
        $this->collSelOrdersPartial = null;

        return $this;
    }

    /**
     * reset is the collSelOrders collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelOrders($v = true)
    {
        $this->collSelOrdersPartial = $v;
    }

    /**
     * Initializes the collSelOrders collection.
     *
     * By default this just sets the collSelOrders collection to an empty array (like clearcollSelOrders());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelOrders($overrideExisting = true)
    {
        if (null !== $this->collSelOrders && !$overrideExisting) {
            return;
        }
        $this->collSelOrders = new PropelObjectCollection();
        $this->collSelOrders->setModel('SelOrder');
    }

    /**
     * Gets an array of SelOrder objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelOrder[] List of SelOrder objects
     * @throws PropelException
     */
    public function getSelOrders($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelOrdersPartial && !$this->isNew();
        if (null === $this->collSelOrders || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelOrders) {
                // return empty collection
                $this->initSelOrders();
            } else {
                $collSelOrders = SelOrderQuery::create(null, $criteria)
                    ->filterBySelCustomer($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelOrdersPartial && count($collSelOrders)) {
                      $this->initSelOrders(false);

                      foreach($collSelOrders as $obj) {
                        if (false == $this->collSelOrders->contains($obj)) {
                          $this->collSelOrders->append($obj);
                        }
                      }

                      $this->collSelOrdersPartial = true;
                    }

                    return $collSelOrders;
                }

                if($partial && $this->collSelOrders) {
                    foreach($this->collSelOrders as $obj) {
                        if($obj->isNew()) {
                            $collSelOrders[] = $obj;
                        }
                    }
                }

                $this->collSelOrders = $collSelOrders;
                $this->collSelOrdersPartial = false;
            }
        }

        return $this->collSelOrders;
    }

    /**
     * Sets a collection of SelOrder objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selOrders A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setSelOrders(PropelCollection $selOrders, PropelPDO $con = null)
    {
        $selOrdersToDelete = $this->getSelOrders(new Criteria(), $con)->diff($selOrders);

        $this->selOrdersScheduledForDeletion = unserialize(serialize($selOrdersToDelete));

        foreach ($selOrdersToDelete as $selOrderRemoved) {
            $selOrderRemoved->setSelCustomer(null);
        }

        $this->collSelOrders = null;
        foreach ($selOrders as $selOrder) {
            $this->addSelOrder($selOrder);
        }

        $this->collSelOrders = $selOrders;
        $this->collSelOrdersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelOrder objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelOrder objects.
     * @throws PropelException
     */
    public function countSelOrders(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelOrdersPartial && !$this->isNew();
        if (null === $this->collSelOrders || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelOrders) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelOrders());
            }
            $query = SelOrderQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelCustomer($this)
                ->count($con);
        }

        return count($this->collSelOrders);
    }

    /**
     * Method called to associate a SelOrder object to this object
     * through the SelOrder foreign key attribute.
     *
     * @param    SelOrder $l SelOrder
     * @return SelCustomer The current object (for fluent API support)
     */
    public function addSelOrder(SelOrder $l)
    {
        if ($this->collSelOrders === null) {
            $this->initSelOrders();
            $this->collSelOrdersPartial = true;
        }
        if (!in_array($l, $this->collSelOrders->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelOrder($l);
        }

        return $this;
    }

    /**
     * @param	SelOrder $selOrder The selOrder object to add.
     */
    protected function doAddSelOrder($selOrder)
    {
        $this->collSelOrders[]= $selOrder;
        $selOrder->setSelCustomer($this);
    }

    /**
     * @param	SelOrder $selOrder The selOrder object to remove.
     * @return SelCustomer The current object (for fluent API support)
     */
    public function removeSelOrder($selOrder)
    {
        if ($this->getSelOrders()->contains($selOrder)) {
            $this->collSelOrders->remove($this->collSelOrders->search($selOrder));
            if (null === $this->selOrdersScheduledForDeletion) {
                $this->selOrdersScheduledForDeletion = clone $this->collSelOrders;
                $this->selOrdersScheduledForDeletion->clear();
            }
            $this->selOrdersScheduledForDeletion[]= clone $selOrder;
            $selOrder->setSelCustomer(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelCustomer is new, it will return
     * an empty collection; or if this SelCustomer has previously
     * been saved, it will retrieve related SelOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelCustomer.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelOrder[] List of SelOrder objects
     */
    public function getSelOrdersJoinSelSale($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelOrderQuery::create(null, $criteria);
        $query->joinWith('SelSale', $join_behavior);

        return $this->getSelOrders($query, $con);
    }

    /**
     * Clears out the collSelSales collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelCustomer The current object (for fluent API support)
     * @see        addSelSales()
     */
    public function clearSelSales()
    {
        $this->collSelSales = null; // important to set this to null since that means it is uninitialized
        $this->collSelSalesPartial = null;

        return $this;
    }

    /**
     * reset is the collSelSales collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelSales($v = true)
    {
        $this->collSelSalesPartial = $v;
    }

    /**
     * Initializes the collSelSales collection.
     *
     * By default this just sets the collSelSales collection to an empty array (like clearcollSelSales());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelSales($overrideExisting = true)
    {
        if (null !== $this->collSelSales && !$overrideExisting) {
            return;
        }
        $this->collSelSales = new PropelObjectCollection();
        $this->collSelSales->setModel('SelSale');
    }

    /**
     * Gets an array of SelSale objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelSale[] List of SelSale objects
     * @throws PropelException
     */
    public function getSelSales($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelSalesPartial && !$this->isNew();
        if (null === $this->collSelSales || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelSales) {
                // return empty collection
                $this->initSelSales();
            } else {
                $collSelSales = SelSaleQuery::create(null, $criteria)
                    ->filterBySelCustomer($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelSalesPartial && count($collSelSales)) {
                      $this->initSelSales(false);

                      foreach($collSelSales as $obj) {
                        if (false == $this->collSelSales->contains($obj)) {
                          $this->collSelSales->append($obj);
                        }
                      }

                      $this->collSelSalesPartial = true;
                    }

                    return $collSelSales;
                }

                if($partial && $this->collSelSales) {
                    foreach($this->collSelSales as $obj) {
                        if($obj->isNew()) {
                            $collSelSales[] = $obj;
                        }
                    }
                }

                $this->collSelSales = $collSelSales;
                $this->collSelSalesPartial = false;
            }
        }

        return $this->collSelSales;
    }

    /**
     * Sets a collection of SelSale objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selSales A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelCustomer The current object (for fluent API support)
     */
    public function setSelSales(PropelCollection $selSales, PropelPDO $con = null)
    {
        $selSalesToDelete = $this->getSelSales(new Criteria(), $con)->diff($selSales);

        $this->selSalesScheduledForDeletion = unserialize(serialize($selSalesToDelete));

        foreach ($selSalesToDelete as $selSaleRemoved) {
            $selSaleRemoved->setSelCustomer(null);
        }

        $this->collSelSales = null;
        foreach ($selSales as $selSale) {
            $this->addSelSale($selSale);
        }

        $this->collSelSales = $selSales;
        $this->collSelSalesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelSale objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelSale objects.
     * @throws PropelException
     */
    public function countSelSales(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelSalesPartial && !$this->isNew();
        if (null === $this->collSelSales || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelSales) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelSales());
            }
            $query = SelSaleQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelCustomer($this)
                ->count($con);
        }

        return count($this->collSelSales);
    }

    /**
     * Method called to associate a SelSale object to this object
     * through the SelSale foreign key attribute.
     *
     * @param    SelSale $l SelSale
     * @return SelCustomer The current object (for fluent API support)
     */
    public function addSelSale(SelSale $l)
    {
        if ($this->collSelSales === null) {
            $this->initSelSales();
            $this->collSelSalesPartial = true;
        }
        if (!in_array($l, $this->collSelSales->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelSale($l);
        }

        return $this;
    }

    /**
     * @param	SelSale $selSale The selSale object to add.
     */
    protected function doAddSelSale($selSale)
    {
        $this->collSelSales[]= $selSale;
        $selSale->setSelCustomer($this);
    }

    /**
     * @param	SelSale $selSale The selSale object to remove.
     * @return SelCustomer The current object (for fluent API support)
     */
    public function removeSelSale($selSale)
    {
        if ($this->getSelSales()->contains($selSale)) {
            $this->collSelSales->remove($this->collSelSales->search($selSale));
            if (null === $this->selSalesScheduledForDeletion) {
                $this->selSalesScheduledForDeletion = clone $this->collSelSales;
                $this->selSalesScheduledForDeletion->clear();
            }
            $this->selSalesScheduledForDeletion[]= clone $selSale;
            $selSale->setSelCustomer(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelCustomer is new, it will return
     * an empty collection; or if this SelCustomer has previously
     * been saved, it will retrieve related SelSales from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelCustomer.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelSale[] List of SelSale objects
     */
    public function getSelSalesJoinSelBranch($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelSaleQuery::create(null, $criteria);
        $query->joinWith('SelBranch', $join_behavior);

        return $this->getSelSales($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelCustomer is new, it will return
     * an empty collection; or if this SelCustomer has previously
     * been saved, it will retrieve related SelSales from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelCustomer.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelSale[] List of SelSale objects
     */
    public function getSelSalesJoinSelEmployee($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelSaleQuery::create(null, $criteria);
        $query->joinWith('SelEmployee', $join_behavior);

        return $this->getSelSales($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->customer_account_id = null;
        $this->id_number = null;
        $this->name = null;
        $this->lastname = null;
        $this->email = null;
        $this->birthday = null;
        $this->address = null;
        $this->phone = null;
        $this->cellphone = null;
        $this->status = null;
        $this->code = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSelOrders) {
                foreach ($this->collSelOrders as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelSales) {
                foreach ($this->collSelSales as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collSelOrders instanceof PropelCollection) {
            $this->collSelOrders->clearIterator();
        }
        $this->collSelOrders = null;
        if ($this->collSelSales instanceof PropelCollection) {
            $this->collSelSales->clearIterator();
        }
        $this->collSelSales = null;
        $this->aSelCustomerAccount = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelCustomerPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
