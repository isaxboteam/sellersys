<?php


/**
 * Base class that represents a query for the 'sel_person' table.
 *
 * 
 *
 * @method SelPersonQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelPersonQuery orderByName($order = Criteria::ASC) Order by the NAME column
 * @method SelPersonQuery orderByFirstLastname($order = Criteria::ASC) Order by the FIRST_LASTNAME column
 * @method SelPersonQuery orderBySecondLastname($order = Criteria::ASC) Order by the SECOND_LASTNAME column
 * @method SelPersonQuery orderByIdentity($order = Criteria::ASC) Order by the IDENTITY column
 * @method SelPersonQuery orderByIdentityExt($order = Criteria::ASC) Order by the IDENTITY_EXT column
 * @method SelPersonQuery orderByGender($order = Criteria::ASC) Order by the GENDER column
 * @method SelPersonQuery orderByBirthdayDate($order = Criteria::ASC) Order by the BIRTHDAY_DATE column
 *
 * @method SelPersonQuery groupById() Group by the ID column
 * @method SelPersonQuery groupByName() Group by the NAME column
 * @method SelPersonQuery groupByFirstLastname() Group by the FIRST_LASTNAME column
 * @method SelPersonQuery groupBySecondLastname() Group by the SECOND_LASTNAME column
 * @method SelPersonQuery groupByIdentity() Group by the IDENTITY column
 * @method SelPersonQuery groupByIdentityExt() Group by the IDENTITY_EXT column
 * @method SelPersonQuery groupByGender() Group by the GENDER column
 * @method SelPersonQuery groupByBirthdayDate() Group by the BIRTHDAY_DATE column
 *
 * @method SelPersonQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelPersonQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelPersonQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelPersonQuery leftJoinSelProvider($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProvider relation
 * @method SelPersonQuery rightJoinSelProvider($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProvider relation
 * @method SelPersonQuery innerJoinSelProvider($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProvider relation
 *
 * @method SelPerson findOne(PropelPDO $con = null) Return the first SelPerson matching the query
 * @method SelPerson findOneOrCreate(PropelPDO $con = null) Return the first SelPerson matching the query, or a new SelPerson object populated from the query conditions when no match is found
 *
 * @method SelPerson findOneByName(string $NAME) Return the first SelPerson filtered by the NAME column
 * @method SelPerson findOneByFirstLastname(string $FIRST_LASTNAME) Return the first SelPerson filtered by the FIRST_LASTNAME column
 * @method SelPerson findOneBySecondLastname(string $SECOND_LASTNAME) Return the first SelPerson filtered by the SECOND_LASTNAME column
 * @method SelPerson findOneByIdentity(int $IDENTITY) Return the first SelPerson filtered by the IDENTITY column
 * @method SelPerson findOneByIdentityExt(string $IDENTITY_EXT) Return the first SelPerson filtered by the IDENTITY_EXT column
 * @method SelPerson findOneByGender(string $GENDER) Return the first SelPerson filtered by the GENDER column
 * @method SelPerson findOneByBirthdayDate(string $BIRTHDAY_DATE) Return the first SelPerson filtered by the BIRTHDAY_DATE column
 *
 * @method array findById(int $ID) Return SelPerson objects filtered by the ID column
 * @method array findByName(string $NAME) Return SelPerson objects filtered by the NAME column
 * @method array findByFirstLastname(string $FIRST_LASTNAME) Return SelPerson objects filtered by the FIRST_LASTNAME column
 * @method array findBySecondLastname(string $SECOND_LASTNAME) Return SelPerson objects filtered by the SECOND_LASTNAME column
 * @method array findByIdentity(int $IDENTITY) Return SelPerson objects filtered by the IDENTITY column
 * @method array findByIdentityExt(string $IDENTITY_EXT) Return SelPerson objects filtered by the IDENTITY_EXT column
 * @method array findByGender(string $GENDER) Return SelPerson objects filtered by the GENDER column
 * @method array findByBirthdayDate(string $BIRTHDAY_DATE) Return SelPerson objects filtered by the BIRTHDAY_DATE column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelPersonQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelPersonQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelPerson', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelPersonQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelPersonQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelPersonQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelPersonQuery) {
            return $criteria;
        }
        $query = new SelPersonQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelPerson|SelPerson[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelPersonPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelPersonPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelPerson A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelPerson A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `NAME`, `FIRST_LASTNAME`, `SECOND_LASTNAME`, `IDENTITY`, `IDENTITY_EXT`, `GENDER`, `BIRTHDAY_DATE` FROM `sel_person` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelPerson();
            $obj->hydrate($row);
            SelPersonPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelPerson|SelPerson[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelPerson[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelPersonPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelPersonPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelPersonPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the NAME column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE NAME = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE NAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelPersonPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the FIRST_LASTNAME column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstLastname('fooValue');   // WHERE FIRST_LASTNAME = 'fooValue'
     * $query->filterByFirstLastname('%fooValue%'); // WHERE FIRST_LASTNAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstLastname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function filterByFirstLastname($firstLastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstLastname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $firstLastname)) {
                $firstLastname = str_replace('*', '%', $firstLastname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelPersonPeer::FIRST_LASTNAME, $firstLastname, $comparison);
    }

    /**
     * Filter the query on the SECOND_LASTNAME column
     *
     * Example usage:
     * <code>
     * $query->filterBySecondLastname('fooValue');   // WHERE SECOND_LASTNAME = 'fooValue'
     * $query->filterBySecondLastname('%fooValue%'); // WHERE SECOND_LASTNAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $secondLastname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function filterBySecondLastname($secondLastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($secondLastname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $secondLastname)) {
                $secondLastname = str_replace('*', '%', $secondLastname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelPersonPeer::SECOND_LASTNAME, $secondLastname, $comparison);
    }

    /**
     * Filter the query on the IDENTITY column
     *
     * Example usage:
     * <code>
     * $query->filterByIdentity(1234); // WHERE IDENTITY = 1234
     * $query->filterByIdentity(array(12, 34)); // WHERE IDENTITY IN (12, 34)
     * $query->filterByIdentity(array('min' => 12)); // WHERE IDENTITY > 12
     * </code>
     *
     * @param     mixed $identity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function filterByIdentity($identity = null, $comparison = null)
    {
        if (is_array($identity)) {
            $useMinMax = false;
            if (isset($identity['min'])) {
                $this->addUsingAlias(SelPersonPeer::IDENTITY, $identity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($identity['max'])) {
                $this->addUsingAlias(SelPersonPeer::IDENTITY, $identity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelPersonPeer::IDENTITY, $identity, $comparison);
    }

    /**
     * Filter the query on the IDENTITY_EXT column
     *
     * Example usage:
     * <code>
     * $query->filterByIdentityExt('fooValue');   // WHERE IDENTITY_EXT = 'fooValue'
     * $query->filterByIdentityExt('%fooValue%'); // WHERE IDENTITY_EXT LIKE '%fooValue%'
     * </code>
     *
     * @param     string $identityExt The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function filterByIdentityExt($identityExt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($identityExt)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $identityExt)) {
                $identityExt = str_replace('*', '%', $identityExt);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelPersonPeer::IDENTITY_EXT, $identityExt, $comparison);
    }

    /**
     * Filter the query on the GENDER column
     *
     * Example usage:
     * <code>
     * $query->filterByGender('fooValue');   // WHERE GENDER = 'fooValue'
     * $query->filterByGender('%fooValue%'); // WHERE GENDER LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gender The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function filterByGender($gender = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gender)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gender)) {
                $gender = str_replace('*', '%', $gender);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelPersonPeer::GENDER, $gender, $comparison);
    }

    /**
     * Filter the query on the BIRTHDAY_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByBirthdayDate('2011-03-14'); // WHERE BIRTHDAY_DATE = '2011-03-14'
     * $query->filterByBirthdayDate('now'); // WHERE BIRTHDAY_DATE = '2011-03-14'
     * $query->filterByBirthdayDate(array('max' => 'yesterday')); // WHERE BIRTHDAY_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $birthdayDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function filterByBirthdayDate($birthdayDate = null, $comparison = null)
    {
        if (is_array($birthdayDate)) {
            $useMinMax = false;
            if (isset($birthdayDate['min'])) {
                $this->addUsingAlias(SelPersonPeer::BIRTHDAY_DATE, $birthdayDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($birthdayDate['max'])) {
                $this->addUsingAlias(SelPersonPeer::BIRTHDAY_DATE, $birthdayDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelPersonPeer::BIRTHDAY_DATE, $birthdayDate, $comparison);
    }

    /**
     * Filter the query by a related SelProvider object
     *
     * @param   SelProvider|PropelObjectCollection $selProvider  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelPersonQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProvider($selProvider, $comparison = null)
    {
        if ($selProvider instanceof SelProvider) {
            return $this
                ->addUsingAlias(SelPersonPeer::ID, $selProvider->getPersonId(), $comparison);
        } elseif ($selProvider instanceof PropelObjectCollection) {
            return $this
                ->useSelProviderQuery()
                ->filterByPrimaryKeys($selProvider->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProvider() only accepts arguments of type SelProvider or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProvider relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function joinSelProvider($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProvider');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProvider');
        }

        return $this;
    }

    /**
     * Use the SelProvider relation SelProvider object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProviderQuery A secondary query class using the current class as primary query
     */
    public function useSelProviderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelProvider($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProvider', 'SelProviderQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelPerson $selPerson Object to remove from the list of results
     *
     * @return SelPersonQuery The current query, for fluid interface
     */
    public function prune($selPerson = null)
    {
        if ($selPerson) {
            $this->addUsingAlias(SelPersonPeer::ID, $selPerson->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
