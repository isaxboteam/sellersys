<?php


/**
 * Base class that represents a row from the 'cho_user' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseChoUser extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'ChoUserPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  ChoUserPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the rol_id field.
     * @var        int
     */
    protected $rol_id;

    /**
     * The value for the first_lastname field.
     * @var        string
     */
    protected $first_lastname;

    /**
     * The value for the second_lastname field.
     * @var        string
     */
    protected $second_lastname;

    /**
     * The value for the first_name field.
     * @var        string
     */
    protected $first_name;

    /**
     * The value for the second_name field.
     * @var        string
     */
    protected $second_name;

    /**
     * The value for the username field.
     * @var        string
     */
    protected $username;

    /**
     * The value for the password field.
     * @var        string
     */
    protected $password;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the dni field.
     * @var        string
     */
    protected $dni;

    /**
     * The value for the gender field.
     * @var        string
     */
    protected $gender;

    /**
     * The value for the birthday field.
     * @var        string
     */
    protected $birthday;

    /**
     * The value for the type field.
     * @var        string
     */
    protected $type;

    /**
     * The value for the status field.
     * @var        string
     */
    protected $status;

    /**
     * The value for the country field.
     * @var        string
     */
    protected $country;

    /**
     * The value for the state field.
     * @var        string
     */
    protected $state;

    /**
     * The value for the city field.
     * @var        string
     */
    protected $city;

    /**
     * The value for the address field.
     * @var        string
     */
    protected $address;

    /**
     * The value for the zip field.
     * @var        string
     */
    protected $zip;

    /**
     * The value for the primary_phone field.
     * @var        string
     */
    protected $primary_phone;

    /**
     * The value for the secondary_phone field.
     * @var        string
     */
    protected $secondary_phone;

    /**
     * The value for the cell_phone field.
     * @var        string
     */
    protected $cell_phone;

    /**
     * The value for the photo_format field.
     * @var        string
     */
    protected $photo_format;

    /**
     * The value for the registered_date field.
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        string
     */
    protected $registered_date;

    /**
     * The value for the last_access field.
     * Note: this column has a database default value of: NULL
     * @var        string
     */
    protected $last_access;

    /**
     * @var        PropelObjectCollection|ChoUserXRol[] Collection to store aggregation of ChoUserXRol objects.
     */
    protected $collChoUserXRols;
    protected $collChoUserXRolsPartial;

    /**
     * @var        PropelObjectCollection|SelEmployee[] Collection to store aggregation of SelEmployee objects.
     */
    protected $collSelEmployees;
    protected $collSelEmployeesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $choUserXRolsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selEmployeesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->last_access = NULL;
    }

    /**
     * Initializes internal state of BaseChoUser object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [rol_id] column value.
     * 
     * @return int
     */
    public function getRolId()
    {
        return $this->rol_id;
    }

    /**
     * Get the [first_lastname] column value.
     * 
     * @return string
     */
    public function getFirstLastname()
    {
        return $this->first_lastname;
    }

    /**
     * Get the [second_lastname] column value.
     * 
     * @return string
     */
    public function getSecondLastname()
    {
        return $this->second_lastname;
    }

    /**
     * Get the [first_name] column value.
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Get the [second_name] column value.
     * 
     * @return string
     */
    public function getSecondName()
    {
        return $this->second_name;
    }

    /**
     * Get the [username] column value.
     * 
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the [password] column value.
     * 
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [email] column value.
     * 
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [dni] column value.
     * 
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Get the [gender] column value.
     * 
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Get the [optionally formatted] temporal [birthday] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getBirthday($format = '%x')
    {
        if ($this->birthday === null) {
            return null;
        }

        if ($this->birthday === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->birthday);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->birthday, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [type] column value.
     * 
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the [status] column value.
     * 
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [country] column value.
     * 
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get the [state] column value.
     * 
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Get the [city] column value.
     * 
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Get the [address] column value.
     * 
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get the [zip] column value.
     * 
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Get the [primary_phone] column value.
     * 
     * @return string
     */
    public function getPrimaryPhone()
    {
        return $this->primary_phone;
    }

    /**
     * Get the [secondary_phone] column value.
     * 
     * @return string
     */
    public function getSecondaryPhone()
    {
        return $this->secondary_phone;
    }

    /**
     * Get the [cell_phone] column value.
     * 
     * @return string
     */
    public function getCellPhone()
    {
        return $this->cell_phone;
    }

    /**
     * Get the [photo_format] column value.
     * 
     * @return string
     */
    public function getPhotoFormat()
    {
        return $this->photo_format;
    }

    /**
     * Get the [optionally formatted] temporal [registered_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getRegisteredDate($format = 'Y-m-d H:i:s')
    {
        if ($this->registered_date === null) {
            return null;
        }

        if ($this->registered_date === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->registered_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->registered_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [last_access] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastAccess($format = 'Y-m-d H:i:s')
    {
        if ($this->last_access === null) {
            return null;
        }

        if ($this->last_access === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->last_access);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_access, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ChoUserPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [rol_id] column.
     * 
     * @param int $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setRolId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->rol_id !== $v) {
            $this->rol_id = $v;
            $this->modifiedColumns[] = ChoUserPeer::ROL_ID;
        }


        return $this;
    } // setRolId()

    /**
     * Set the value of [first_lastname] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setFirstLastname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->first_lastname !== $v) {
            $this->first_lastname = $v;
            $this->modifiedColumns[] = ChoUserPeer::FIRST_LASTNAME;
        }


        return $this;
    } // setFirstLastname()

    /**
     * Set the value of [second_lastname] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setSecondLastname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->second_lastname !== $v) {
            $this->second_lastname = $v;
            $this->modifiedColumns[] = ChoUserPeer::SECOND_LASTNAME;
        }


        return $this;
    } // setSecondLastname()

    /**
     * Set the value of [first_name] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setFirstName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->first_name !== $v) {
            $this->first_name = $v;
            $this->modifiedColumns[] = ChoUserPeer::FIRST_NAME;
        }


        return $this;
    } // setFirstName()

    /**
     * Set the value of [second_name] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setSecondName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->second_name !== $v) {
            $this->second_name = $v;
            $this->modifiedColumns[] = ChoUserPeer::SECOND_NAME;
        }


        return $this;
    } // setSecondName()

    /**
     * Set the value of [username] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[] = ChoUserPeer::USERNAME;
        }


        return $this;
    } // setUsername()

    /**
     * Set the value of [password] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[] = ChoUserPeer::PASSWORD;
        }


        return $this;
    } // setPassword()

    /**
     * Set the value of [email] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = ChoUserPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [dni] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setDni($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dni !== $v) {
            $this->dni = $v;
            $this->modifiedColumns[] = ChoUserPeer::DNI;
        }


        return $this;
    } // setDni()

    /**
     * Set the value of [gender] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setGender($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gender !== $v) {
            $this->gender = $v;
            $this->modifiedColumns[] = ChoUserPeer::GENDER;
        }


        return $this;
    } // setGender()

    /**
     * Sets the value of [birthday] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return ChoUser The current object (for fluent API support)
     */
    public function setBirthday($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->birthday !== null || $dt !== null) {
            $currentDateAsString = ($this->birthday !== null && $tmpDt = new DateTime($this->birthday)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->birthday = $newDateAsString;
                $this->modifiedColumns[] = ChoUserPeer::BIRTHDAY;
            }
        } // if either are not null


        return $this;
    } // setBirthday()

    /**
     * Set the value of [type] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[] = ChoUserPeer::TYPE;
        }


        return $this;
    } // setType()

    /**
     * Set the value of [status] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[] = ChoUserPeer::STATUS;
        }


        return $this;
    } // setStatus()

    /**
     * Set the value of [country] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setCountry($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->country !== $v) {
            $this->country = $v;
            $this->modifiedColumns[] = ChoUserPeer::COUNTRY;
        }


        return $this;
    } // setCountry()

    /**
     * Set the value of [state] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setState($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->state !== $v) {
            $this->state = $v;
            $this->modifiedColumns[] = ChoUserPeer::STATE;
        }


        return $this;
    } // setState()

    /**
     * Set the value of [city] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->city !== $v) {
            $this->city = $v;
            $this->modifiedColumns[] = ChoUserPeer::CITY;
        }


        return $this;
    } // setCity()

    /**
     * Set the value of [address] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->address !== $v) {
            $this->address = $v;
            $this->modifiedColumns[] = ChoUserPeer::ADDRESS;
        }


        return $this;
    } // setAddress()

    /**
     * Set the value of [zip] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setZip($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->zip !== $v) {
            $this->zip = $v;
            $this->modifiedColumns[] = ChoUserPeer::ZIP;
        }


        return $this;
    } // setZip()

    /**
     * Set the value of [primary_phone] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setPrimaryPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->primary_phone !== $v) {
            $this->primary_phone = $v;
            $this->modifiedColumns[] = ChoUserPeer::PRIMARY_PHONE;
        }


        return $this;
    } // setPrimaryPhone()

    /**
     * Set the value of [secondary_phone] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setSecondaryPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->secondary_phone !== $v) {
            $this->secondary_phone = $v;
            $this->modifiedColumns[] = ChoUserPeer::SECONDARY_PHONE;
        }


        return $this;
    } // setSecondaryPhone()

    /**
     * Set the value of [cell_phone] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setCellPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cell_phone !== $v) {
            $this->cell_phone = $v;
            $this->modifiedColumns[] = ChoUserPeer::CELL_PHONE;
        }


        return $this;
    } // setCellPhone()

    /**
     * Set the value of [photo_format] column.
     * 
     * @param string $v new value
     * @return ChoUser The current object (for fluent API support)
     */
    public function setPhotoFormat($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->photo_format !== $v) {
            $this->photo_format = $v;
            $this->modifiedColumns[] = ChoUserPeer::PHOTO_FORMAT;
        }


        return $this;
    } // setPhotoFormat()

    /**
     * Sets the value of [registered_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return ChoUser The current object (for fluent API support)
     */
    public function setRegisteredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->registered_date !== null || $dt !== null) {
            $currentDateAsString = ($this->registered_date !== null && $tmpDt = new DateTime($this->registered_date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->registered_date = $newDateAsString;
                $this->modifiedColumns[] = ChoUserPeer::REGISTERED_DATE;
            }
        } // if either are not null


        return $this;
    } // setRegisteredDate()

    /**
     * Sets the value of [last_access] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return ChoUser The current object (for fluent API support)
     */
    public function setLastAccess($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_access !== null || $dt !== null) {
            $currentDateAsString = ($this->last_access !== null && $tmpDt = new DateTime($this->last_access)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ( ($currentDateAsString !== $newDateAsString) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s') === NULL) // or the entered value matches the default
                 ) {
                $this->last_access = $newDateAsString;
                $this->modifiedColumns[] = ChoUserPeer::LAST_ACCESS;
            }
        } // if either are not null


        return $this;
    } // setLastAccess()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->last_access !== NULL) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->rol_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->first_lastname = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->second_lastname = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->first_name = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->second_name = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->username = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->password = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->email = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->dni = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->gender = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->birthday = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->type = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->status = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->country = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->state = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->city = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->address = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->zip = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->primary_phone = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->secondary_phone = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->cell_phone = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->photo_format = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->registered_date = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->last_access = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 25; // 25 = ChoUserPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating ChoUser object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ChoUserPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collChoUserXRols = null;

            $this->collSelEmployees = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ChoUserQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ChoUserPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->choUserXRolsScheduledForDeletion !== null) {
                if (!$this->choUserXRolsScheduledForDeletion->isEmpty()) {
                    ChoUserXRolQuery::create()
                        ->filterByPrimaryKeys($this->choUserXRolsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->choUserXRolsScheduledForDeletion = null;
                }
            }

            if ($this->collChoUserXRols !== null) {
                foreach ($this->collChoUserXRols as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selEmployeesScheduledForDeletion !== null) {
                if (!$this->selEmployeesScheduledForDeletion->isEmpty()) {
                    SelEmployeeQuery::create()
                        ->filterByPrimaryKeys($this->selEmployeesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selEmployeesScheduledForDeletion = null;
                }
            }

            if ($this->collSelEmployees !== null) {
                foreach ($this->collSelEmployees as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ChoUserPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ChoUserPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ChoUserPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(ChoUserPeer::ROL_ID)) {
            $modifiedColumns[':p' . $index++]  = '`ROL_ID`';
        }
        if ($this->isColumnModified(ChoUserPeer::FIRST_LASTNAME)) {
            $modifiedColumns[':p' . $index++]  = '`FIRST_LASTNAME`';
        }
        if ($this->isColumnModified(ChoUserPeer::SECOND_LASTNAME)) {
            $modifiedColumns[':p' . $index++]  = '`SECOND_LASTNAME`';
        }
        if ($this->isColumnModified(ChoUserPeer::FIRST_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`FIRST_NAME`';
        }
        if ($this->isColumnModified(ChoUserPeer::SECOND_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`SECOND_NAME`';
        }
        if ($this->isColumnModified(ChoUserPeer::USERNAME)) {
            $modifiedColumns[':p' . $index++]  = '`USERNAME`';
        }
        if ($this->isColumnModified(ChoUserPeer::PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = '`PASSWORD`';
        }
        if ($this->isColumnModified(ChoUserPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`EMAIL`';
        }
        if ($this->isColumnModified(ChoUserPeer::DNI)) {
            $modifiedColumns[':p' . $index++]  = '`DNI`';
        }
        if ($this->isColumnModified(ChoUserPeer::GENDER)) {
            $modifiedColumns[':p' . $index++]  = '`GENDER`';
        }
        if ($this->isColumnModified(ChoUserPeer::BIRTHDAY)) {
            $modifiedColumns[':p' . $index++]  = '`BIRTHDAY`';
        }
        if ($this->isColumnModified(ChoUserPeer::TYPE)) {
            $modifiedColumns[':p' . $index++]  = '`TYPE`';
        }
        if ($this->isColumnModified(ChoUserPeer::STATUS)) {
            $modifiedColumns[':p' . $index++]  = '`STATUS`';
        }
        if ($this->isColumnModified(ChoUserPeer::COUNTRY)) {
            $modifiedColumns[':p' . $index++]  = '`COUNTRY`';
        }
        if ($this->isColumnModified(ChoUserPeer::STATE)) {
            $modifiedColumns[':p' . $index++]  = '`STATE`';
        }
        if ($this->isColumnModified(ChoUserPeer::CITY)) {
            $modifiedColumns[':p' . $index++]  = '`CITY`';
        }
        if ($this->isColumnModified(ChoUserPeer::ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`ADDRESS`';
        }
        if ($this->isColumnModified(ChoUserPeer::ZIP)) {
            $modifiedColumns[':p' . $index++]  = '`ZIP`';
        }
        if ($this->isColumnModified(ChoUserPeer::PRIMARY_PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`PRIMARY_PHONE`';
        }
        if ($this->isColumnModified(ChoUserPeer::SECONDARY_PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`SECONDARY_PHONE`';
        }
        if ($this->isColumnModified(ChoUserPeer::CELL_PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`CELL_PHONE`';
        }
        if ($this->isColumnModified(ChoUserPeer::PHOTO_FORMAT)) {
            $modifiedColumns[':p' . $index++]  = '`PHOTO_FORMAT`';
        }
        if ($this->isColumnModified(ChoUserPeer::REGISTERED_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`REGISTERED_DATE`';
        }
        if ($this->isColumnModified(ChoUserPeer::LAST_ACCESS)) {
            $modifiedColumns[':p' . $index++]  = '`LAST_ACCESS`';
        }

        $sql = sprintf(
            'INSERT INTO `cho_user` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`ROL_ID`':						
                        $stmt->bindValue($identifier, $this->rol_id, PDO::PARAM_INT);
                        break;
                    case '`FIRST_LASTNAME`':						
                        $stmt->bindValue($identifier, $this->first_lastname, PDO::PARAM_STR);
                        break;
                    case '`SECOND_LASTNAME`':						
                        $stmt->bindValue($identifier, $this->second_lastname, PDO::PARAM_STR);
                        break;
                    case '`FIRST_NAME`':						
                        $stmt->bindValue($identifier, $this->first_name, PDO::PARAM_STR);
                        break;
                    case '`SECOND_NAME`':						
                        $stmt->bindValue($identifier, $this->second_name, PDO::PARAM_STR);
                        break;
                    case '`USERNAME`':						
                        $stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
                        break;
                    case '`PASSWORD`':						
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case '`EMAIL`':						
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`DNI`':						
                        $stmt->bindValue($identifier, $this->dni, PDO::PARAM_STR);
                        break;
                    case '`GENDER`':						
                        $stmt->bindValue($identifier, $this->gender, PDO::PARAM_STR);
                        break;
                    case '`BIRTHDAY`':						
                        $stmt->bindValue($identifier, $this->birthday, PDO::PARAM_STR);
                        break;
                    case '`TYPE`':						
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                    case '`STATUS`':						
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_STR);
                        break;
                    case '`COUNTRY`':						
                        $stmt->bindValue($identifier, $this->country, PDO::PARAM_STR);
                        break;
                    case '`STATE`':						
                        $stmt->bindValue($identifier, $this->state, PDO::PARAM_STR);
                        break;
                    case '`CITY`':						
                        $stmt->bindValue($identifier, $this->city, PDO::PARAM_STR);
                        break;
                    case '`ADDRESS`':						
                        $stmt->bindValue($identifier, $this->address, PDO::PARAM_STR);
                        break;
                    case '`ZIP`':						
                        $stmt->bindValue($identifier, $this->zip, PDO::PARAM_STR);
                        break;
                    case '`PRIMARY_PHONE`':						
                        $stmt->bindValue($identifier, $this->primary_phone, PDO::PARAM_STR);
                        break;
                    case '`SECONDARY_PHONE`':						
                        $stmt->bindValue($identifier, $this->secondary_phone, PDO::PARAM_STR);
                        break;
                    case '`CELL_PHONE`':						
                        $stmt->bindValue($identifier, $this->cell_phone, PDO::PARAM_STR);
                        break;
                    case '`PHOTO_FORMAT`':						
                        $stmt->bindValue($identifier, $this->photo_format, PDO::PARAM_STR);
                        break;
                    case '`REGISTERED_DATE`':						
                        $stmt->bindValue($identifier, $this->registered_date, PDO::PARAM_STR);
                        break;
                    case '`LAST_ACCESS`':						
                        $stmt->bindValue($identifier, $this->last_access, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();

            if (($retval = ChoUserPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collChoUserXRols !== null) {
                    foreach ($this->collChoUserXRols as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelEmployees !== null) {
                    foreach ($this->collSelEmployees as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ChoUserPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getRolId();
                break;
            case 2:
                return $this->getFirstLastname();
                break;
            case 3:
                return $this->getSecondLastname();
                break;
            case 4:
                return $this->getFirstName();
                break;
            case 5:
                return $this->getSecondName();
                break;
            case 6:
                return $this->getUsername();
                break;
            case 7:
                return $this->getPassword();
                break;
            case 8:
                return $this->getEmail();
                break;
            case 9:
                return $this->getDni();
                break;
            case 10:
                return $this->getGender();
                break;
            case 11:
                return $this->getBirthday();
                break;
            case 12:
                return $this->getType();
                break;
            case 13:
                return $this->getStatus();
                break;
            case 14:
                return $this->getCountry();
                break;
            case 15:
                return $this->getState();
                break;
            case 16:
                return $this->getCity();
                break;
            case 17:
                return $this->getAddress();
                break;
            case 18:
                return $this->getZip();
                break;
            case 19:
                return $this->getPrimaryPhone();
                break;
            case 20:
                return $this->getSecondaryPhone();
                break;
            case 21:
                return $this->getCellPhone();
                break;
            case 22:
                return $this->getPhotoFormat();
                break;
            case 23:
                return $this->getRegisteredDate();
                break;
            case 24:
                return $this->getLastAccess();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['ChoUser'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ChoUser'][$this->getPrimaryKey()] = true;
        $keys = ChoUserPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getRolId(),
            $keys[2] => $this->getFirstLastname(),
            $keys[3] => $this->getSecondLastname(),
            $keys[4] => $this->getFirstName(),
            $keys[5] => $this->getSecondName(),
            $keys[6] => $this->getUsername(),
            $keys[7] => $this->getPassword(),
            $keys[8] => $this->getEmail(),
            $keys[9] => $this->getDni(),
            $keys[10] => $this->getGender(),
            $keys[11] => $this->getBirthday(),
            $keys[12] => $this->getType(),
            $keys[13] => $this->getStatus(),
            $keys[14] => $this->getCountry(),
            $keys[15] => $this->getState(),
            $keys[16] => $this->getCity(),
            $keys[17] => $this->getAddress(),
            $keys[18] => $this->getZip(),
            $keys[19] => $this->getPrimaryPhone(),
            $keys[20] => $this->getSecondaryPhone(),
            $keys[21] => $this->getCellPhone(),
            $keys[22] => $this->getPhotoFormat(),
            $keys[23] => $this->getRegisteredDate(),
            $keys[24] => $this->getLastAccess(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collChoUserXRols) {
                $result['ChoUserXRols'] = $this->collChoUserXRols->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelEmployees) {
                $result['SelEmployees'] = $this->collSelEmployees->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ChoUserPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setRolId($value);
                break;
            case 2:
                $this->setFirstLastname($value);
                break;
            case 3:
                $this->setSecondLastname($value);
                break;
            case 4:
                $this->setFirstName($value);
                break;
            case 5:
                $this->setSecondName($value);
                break;
            case 6:
                $this->setUsername($value);
                break;
            case 7:
                $this->setPassword($value);
                break;
            case 8:
                $this->setEmail($value);
                break;
            case 9:
                $this->setDni($value);
                break;
            case 10:
                $this->setGender($value);
                break;
            case 11:
                $this->setBirthday($value);
                break;
            case 12:
                $this->setType($value);
                break;
            case 13:
                $this->setStatus($value);
                break;
            case 14:
                $this->setCountry($value);
                break;
            case 15:
                $this->setState($value);
                break;
            case 16:
                $this->setCity($value);
                break;
            case 17:
                $this->setAddress($value);
                break;
            case 18:
                $this->setZip($value);
                break;
            case 19:
                $this->setPrimaryPhone($value);
                break;
            case 20:
                $this->setSecondaryPhone($value);
                break;
            case 21:
                $this->setCellPhone($value);
                break;
            case 22:
                $this->setPhotoFormat($value);
                break;
            case 23:
                $this->setRegisteredDate($value);
                break;
            case 24:
                $this->setLastAccess($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ChoUserPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setRolId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setFirstLastname($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setSecondLastname($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setFirstName($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSecondName($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setUsername($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setPassword($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setEmail($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setDni($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setGender($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setBirthday($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setType($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setStatus($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setCountry($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setState($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setCity($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setAddress($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setZip($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setPrimaryPhone($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setSecondaryPhone($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setCellPhone($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setPhotoFormat($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setRegisteredDate($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setLastAccess($arr[$keys[24]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ChoUserPeer::DATABASE_NAME);

        if ($this->isColumnModified(ChoUserPeer::ID)) $criteria->add(ChoUserPeer::ID, $this->id);
        if ($this->isColumnModified(ChoUserPeer::ROL_ID)) $criteria->add(ChoUserPeer::ROL_ID, $this->rol_id);
        if ($this->isColumnModified(ChoUserPeer::FIRST_LASTNAME)) $criteria->add(ChoUserPeer::FIRST_LASTNAME, $this->first_lastname);
        if ($this->isColumnModified(ChoUserPeer::SECOND_LASTNAME)) $criteria->add(ChoUserPeer::SECOND_LASTNAME, $this->second_lastname);
        if ($this->isColumnModified(ChoUserPeer::FIRST_NAME)) $criteria->add(ChoUserPeer::FIRST_NAME, $this->first_name);
        if ($this->isColumnModified(ChoUserPeer::SECOND_NAME)) $criteria->add(ChoUserPeer::SECOND_NAME, $this->second_name);
        if ($this->isColumnModified(ChoUserPeer::USERNAME)) $criteria->add(ChoUserPeer::USERNAME, $this->username);
        if ($this->isColumnModified(ChoUserPeer::PASSWORD)) $criteria->add(ChoUserPeer::PASSWORD, $this->password);
        if ($this->isColumnModified(ChoUserPeer::EMAIL)) $criteria->add(ChoUserPeer::EMAIL, $this->email);
        if ($this->isColumnModified(ChoUserPeer::DNI)) $criteria->add(ChoUserPeer::DNI, $this->dni);
        if ($this->isColumnModified(ChoUserPeer::GENDER)) $criteria->add(ChoUserPeer::GENDER, $this->gender);
        if ($this->isColumnModified(ChoUserPeer::BIRTHDAY)) $criteria->add(ChoUserPeer::BIRTHDAY, $this->birthday);
        if ($this->isColumnModified(ChoUserPeer::TYPE)) $criteria->add(ChoUserPeer::TYPE, $this->type);
        if ($this->isColumnModified(ChoUserPeer::STATUS)) $criteria->add(ChoUserPeer::STATUS, $this->status);
        if ($this->isColumnModified(ChoUserPeer::COUNTRY)) $criteria->add(ChoUserPeer::COUNTRY, $this->country);
        if ($this->isColumnModified(ChoUserPeer::STATE)) $criteria->add(ChoUserPeer::STATE, $this->state);
        if ($this->isColumnModified(ChoUserPeer::CITY)) $criteria->add(ChoUserPeer::CITY, $this->city);
        if ($this->isColumnModified(ChoUserPeer::ADDRESS)) $criteria->add(ChoUserPeer::ADDRESS, $this->address);
        if ($this->isColumnModified(ChoUserPeer::ZIP)) $criteria->add(ChoUserPeer::ZIP, $this->zip);
        if ($this->isColumnModified(ChoUserPeer::PRIMARY_PHONE)) $criteria->add(ChoUserPeer::PRIMARY_PHONE, $this->primary_phone);
        if ($this->isColumnModified(ChoUserPeer::SECONDARY_PHONE)) $criteria->add(ChoUserPeer::SECONDARY_PHONE, $this->secondary_phone);
        if ($this->isColumnModified(ChoUserPeer::CELL_PHONE)) $criteria->add(ChoUserPeer::CELL_PHONE, $this->cell_phone);
        if ($this->isColumnModified(ChoUserPeer::PHOTO_FORMAT)) $criteria->add(ChoUserPeer::PHOTO_FORMAT, $this->photo_format);
        if ($this->isColumnModified(ChoUserPeer::REGISTERED_DATE)) $criteria->add(ChoUserPeer::REGISTERED_DATE, $this->registered_date);
        if ($this->isColumnModified(ChoUserPeer::LAST_ACCESS)) $criteria->add(ChoUserPeer::LAST_ACCESS, $this->last_access);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ChoUserPeer::DATABASE_NAME);
        $criteria->add(ChoUserPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of ChoUser (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setRolId($this->getRolId());
        $copyObj->setFirstLastname($this->getFirstLastname());
        $copyObj->setSecondLastname($this->getSecondLastname());
        $copyObj->setFirstName($this->getFirstName());
        $copyObj->setSecondName($this->getSecondName());
        $copyObj->setUsername($this->getUsername());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setDni($this->getDni());
        $copyObj->setGender($this->getGender());
        $copyObj->setBirthday($this->getBirthday());
        $copyObj->setType($this->getType());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setCountry($this->getCountry());
        $copyObj->setState($this->getState());
        $copyObj->setCity($this->getCity());
        $copyObj->setAddress($this->getAddress());
        $copyObj->setZip($this->getZip());
        $copyObj->setPrimaryPhone($this->getPrimaryPhone());
        $copyObj->setSecondaryPhone($this->getSecondaryPhone());
        $copyObj->setCellPhone($this->getCellPhone());
        $copyObj->setPhotoFormat($this->getPhotoFormat());
        $copyObj->setRegisteredDate($this->getRegisteredDate());
        $copyObj->setLastAccess($this->getLastAccess());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getChoUserXRols() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChoUserXRol($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelEmployees() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelEmployee($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return ChoUser Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ChoUserPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ChoUserPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ChoUserXRol' == $relationName) {
            $this->initChoUserXRols();
        }
        if ('SelEmployee' == $relationName) {
            $this->initSelEmployees();
        }
    }

    /**
     * Clears out the collChoUserXRols collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return ChoUser The current object (for fluent API support)
     * @see        addChoUserXRols()
     */
    public function clearChoUserXRols()
    {
        $this->collChoUserXRols = null; // important to set this to null since that means it is uninitialized
        $this->collChoUserXRolsPartial = null;

        return $this;
    }

    /**
     * reset is the collChoUserXRols collection loaded partially
     *
     * @return void
     */
    public function resetPartialChoUserXRols($v = true)
    {
        $this->collChoUserXRolsPartial = $v;
    }

    /**
     * Initializes the collChoUserXRols collection.
     *
     * By default this just sets the collChoUserXRols collection to an empty array (like clearcollChoUserXRols());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChoUserXRols($overrideExisting = true)
    {
        if (null !== $this->collChoUserXRols && !$overrideExisting) {
            return;
        }
        $this->collChoUserXRols = new PropelObjectCollection();
        $this->collChoUserXRols->setModel('ChoUserXRol');
    }

    /**
     * Gets an array of ChoUserXRol objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChoUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|ChoUserXRol[] List of ChoUserXRol objects
     * @throws PropelException
     */
    public function getChoUserXRols($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collChoUserXRolsPartial && !$this->isNew();
        if (null === $this->collChoUserXRols || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChoUserXRols) {
                // return empty collection
                $this->initChoUserXRols();
            } else {
                $collChoUserXRols = ChoUserXRolQuery::create(null, $criteria)
                    ->filterByChoUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collChoUserXRolsPartial && count($collChoUserXRols)) {
                      $this->initChoUserXRols(false);

                      foreach($collChoUserXRols as $obj) {
                        if (false == $this->collChoUserXRols->contains($obj)) {
                          $this->collChoUserXRols->append($obj);
                        }
                      }

                      $this->collChoUserXRolsPartial = true;
                    }

                    return $collChoUserXRols;
                }

                if($partial && $this->collChoUserXRols) {
                    foreach($this->collChoUserXRols as $obj) {
                        if($obj->isNew()) {
                            $collChoUserXRols[] = $obj;
                        }
                    }
                }

                $this->collChoUserXRols = $collChoUserXRols;
                $this->collChoUserXRolsPartial = false;
            }
        }

        return $this->collChoUserXRols;
    }

    /**
     * Sets a collection of ChoUserXRol objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $choUserXRols A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return ChoUser The current object (for fluent API support)
     */
    public function setChoUserXRols(PropelCollection $choUserXRols, PropelPDO $con = null)
    {
        $choUserXRolsToDelete = $this->getChoUserXRols(new Criteria(), $con)->diff($choUserXRols);

        $this->choUserXRolsScheduledForDeletion = unserialize(serialize($choUserXRolsToDelete));

        foreach ($choUserXRolsToDelete as $choUserXRolRemoved) {
            $choUserXRolRemoved->setChoUser(null);
        }

        $this->collChoUserXRols = null;
        foreach ($choUserXRols as $choUserXRol) {
            $this->addChoUserXRol($choUserXRol);
        }

        $this->collChoUserXRols = $choUserXRols;
        $this->collChoUserXRolsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ChoUserXRol objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related ChoUserXRol objects.
     * @throws PropelException
     */
    public function countChoUserXRols(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collChoUserXRolsPartial && !$this->isNew();
        if (null === $this->collChoUserXRols || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChoUserXRols) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getChoUserXRols());
            }
            $query = ChoUserXRolQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChoUser($this)
                ->count($con);
        }

        return count($this->collChoUserXRols);
    }

    /**
     * Method called to associate a ChoUserXRol object to this object
     * through the ChoUserXRol foreign key attribute.
     *
     * @param    ChoUserXRol $l ChoUserXRol
     * @return ChoUser The current object (for fluent API support)
     */
    public function addChoUserXRol(ChoUserXRol $l)
    {
        if ($this->collChoUserXRols === null) {
            $this->initChoUserXRols();
            $this->collChoUserXRolsPartial = true;
        }
        if (!in_array($l, $this->collChoUserXRols->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddChoUserXRol($l);
        }

        return $this;
    }

    /**
     * @param	ChoUserXRol $choUserXRol The choUserXRol object to add.
     */
    protected function doAddChoUserXRol($choUserXRol)
    {
        $this->collChoUserXRols[]= $choUserXRol;
        $choUserXRol->setChoUser($this);
    }

    /**
     * @param	ChoUserXRol $choUserXRol The choUserXRol object to remove.
     * @return ChoUser The current object (for fluent API support)
     */
    public function removeChoUserXRol($choUserXRol)
    {
        if ($this->getChoUserXRols()->contains($choUserXRol)) {
            $this->collChoUserXRols->remove($this->collChoUserXRols->search($choUserXRol));
            if (null === $this->choUserXRolsScheduledForDeletion) {
                $this->choUserXRolsScheduledForDeletion = clone $this->collChoUserXRols;
                $this->choUserXRolsScheduledForDeletion->clear();
            }
            $this->choUserXRolsScheduledForDeletion[]= clone $choUserXRol;
            $choUserXRol->setChoUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChoUser is new, it will return
     * an empty collection; or if this ChoUser has previously
     * been saved, it will retrieve related ChoUserXRols from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChoUser.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|ChoUserXRol[] List of ChoUserXRol objects
     */
    public function getChoUserXRolsJoinChoRol($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ChoUserXRolQuery::create(null, $criteria);
        $query->joinWith('ChoRol', $join_behavior);

        return $this->getChoUserXRols($query, $con);
    }

    /**
     * Clears out the collSelEmployees collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return ChoUser The current object (for fluent API support)
     * @see        addSelEmployees()
     */
    public function clearSelEmployees()
    {
        $this->collSelEmployees = null; // important to set this to null since that means it is uninitialized
        $this->collSelEmployeesPartial = null;

        return $this;
    }

    /**
     * reset is the collSelEmployees collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelEmployees($v = true)
    {
        $this->collSelEmployeesPartial = $v;
    }

    /**
     * Initializes the collSelEmployees collection.
     *
     * By default this just sets the collSelEmployees collection to an empty array (like clearcollSelEmployees());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelEmployees($overrideExisting = true)
    {
        if (null !== $this->collSelEmployees && !$overrideExisting) {
            return;
        }
        $this->collSelEmployees = new PropelObjectCollection();
        $this->collSelEmployees->setModel('SelEmployee');
    }

    /**
     * Gets an array of SelEmployee objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChoUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelEmployee[] List of SelEmployee objects
     * @throws PropelException
     */
    public function getSelEmployees($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelEmployeesPartial && !$this->isNew();
        if (null === $this->collSelEmployees || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelEmployees) {
                // return empty collection
                $this->initSelEmployees();
            } else {
                $collSelEmployees = SelEmployeeQuery::create(null, $criteria)
                    ->filterByChoUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelEmployeesPartial && count($collSelEmployees)) {
                      $this->initSelEmployees(false);

                      foreach($collSelEmployees as $obj) {
                        if (false == $this->collSelEmployees->contains($obj)) {
                          $this->collSelEmployees->append($obj);
                        }
                      }

                      $this->collSelEmployeesPartial = true;
                    }

                    return $collSelEmployees;
                }

                if($partial && $this->collSelEmployees) {
                    foreach($this->collSelEmployees as $obj) {
                        if($obj->isNew()) {
                            $collSelEmployees[] = $obj;
                        }
                    }
                }

                $this->collSelEmployees = $collSelEmployees;
                $this->collSelEmployeesPartial = false;
            }
        }

        return $this->collSelEmployees;
    }

    /**
     * Sets a collection of SelEmployee objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selEmployees A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return ChoUser The current object (for fluent API support)
     */
    public function setSelEmployees(PropelCollection $selEmployees, PropelPDO $con = null)
    {
        $selEmployeesToDelete = $this->getSelEmployees(new Criteria(), $con)->diff($selEmployees);

        $this->selEmployeesScheduledForDeletion = unserialize(serialize($selEmployeesToDelete));

        foreach ($selEmployeesToDelete as $selEmployeeRemoved) {
            $selEmployeeRemoved->setChoUser(null);
        }

        $this->collSelEmployees = null;
        foreach ($selEmployees as $selEmployee) {
            $this->addSelEmployee($selEmployee);
        }

        $this->collSelEmployees = $selEmployees;
        $this->collSelEmployeesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelEmployee objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelEmployee objects.
     * @throws PropelException
     */
    public function countSelEmployees(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelEmployeesPartial && !$this->isNew();
        if (null === $this->collSelEmployees || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelEmployees) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelEmployees());
            }
            $query = SelEmployeeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChoUser($this)
                ->count($con);
        }

        return count($this->collSelEmployees);
    }

    /**
     * Method called to associate a SelEmployee object to this object
     * through the SelEmployee foreign key attribute.
     *
     * @param    SelEmployee $l SelEmployee
     * @return ChoUser The current object (for fluent API support)
     */
    public function addSelEmployee(SelEmployee $l)
    {
        if ($this->collSelEmployees === null) {
            $this->initSelEmployees();
            $this->collSelEmployeesPartial = true;
        }
        if (!in_array($l, $this->collSelEmployees->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelEmployee($l);
        }

        return $this;
    }

    /**
     * @param	SelEmployee $selEmployee The selEmployee object to add.
     */
    protected function doAddSelEmployee($selEmployee)
    {
        $this->collSelEmployees[]= $selEmployee;
        $selEmployee->setChoUser($this);
    }

    /**
     * @param	SelEmployee $selEmployee The selEmployee object to remove.
     * @return ChoUser The current object (for fluent API support)
     */
    public function removeSelEmployee($selEmployee)
    {
        if ($this->getSelEmployees()->contains($selEmployee)) {
            $this->collSelEmployees->remove($this->collSelEmployees->search($selEmployee));
            if (null === $this->selEmployeesScheduledForDeletion) {
                $this->selEmployeesScheduledForDeletion = clone $this->collSelEmployees;
                $this->selEmployeesScheduledForDeletion->clear();
            }
            $this->selEmployeesScheduledForDeletion[]= clone $selEmployee;
            $selEmployee->setChoUser(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->rol_id = null;
        $this->first_lastname = null;
        $this->second_lastname = null;
        $this->first_name = null;
        $this->second_name = null;
        $this->username = null;
        $this->password = null;
        $this->email = null;
        $this->dni = null;
        $this->gender = null;
        $this->birthday = null;
        $this->type = null;
        $this->status = null;
        $this->country = null;
        $this->state = null;
        $this->city = null;
        $this->address = null;
        $this->zip = null;
        $this->primary_phone = null;
        $this->secondary_phone = null;
        $this->cell_phone = null;
        $this->photo_format = null;
        $this->registered_date = null;
        $this->last_access = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collChoUserXRols) {
                foreach ($this->collChoUserXRols as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelEmployees) {
                foreach ($this->collSelEmployees as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collChoUserXRols instanceof PropelCollection) {
            $this->collChoUserXRols->clearIterator();
        }
        $this->collChoUserXRols = null;
        if ($this->collSelEmployees instanceof PropelCollection) {
            $this->collSelEmployees->clearIterator();
        }
        $this->collSelEmployees = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ChoUserPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
