<?php


/**
 * Base class that represents a query for the 'sel_product_offer' table.
 *
 * 
 *
 * @method SelProductOfferQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelProductOfferQuery orderByProductId($order = Criteria::ASC) Order by the PRODUCT_ID column
 * @method SelProductOfferQuery orderByType($order = Criteria::ASC) Order by the TYPE column
 * @method SelProductOfferQuery orderByInitialDate($order = Criteria::ASC) Order by the INITIAL_DATE column
 * @method SelProductOfferQuery orderByEndDate($order = Criteria::ASC) Order by the END_DATE column
 * @method SelProductOfferQuery orderByPosition($order = Criteria::ASC) Order by the POSiTION column
 *
 * @method SelProductOfferQuery groupById() Group by the ID column
 * @method SelProductOfferQuery groupByProductId() Group by the PRODUCT_ID column
 * @method SelProductOfferQuery groupByType() Group by the TYPE column
 * @method SelProductOfferQuery groupByInitialDate() Group by the INITIAL_DATE column
 * @method SelProductOfferQuery groupByEndDate() Group by the END_DATE column
 * @method SelProductOfferQuery groupByPosition() Group by the POSiTION column
 *
 * @method SelProductOfferQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelProductOfferQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelProductOfferQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelProductOfferQuery leftJoinSelProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProduct relation
 * @method SelProductOfferQuery rightJoinSelProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProduct relation
 * @method SelProductOfferQuery innerJoinSelProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProduct relation
 *
 * @method SelProductOffer findOne(PropelPDO $con = null) Return the first SelProductOffer matching the query
 * @method SelProductOffer findOneOrCreate(PropelPDO $con = null) Return the first SelProductOffer matching the query, or a new SelProductOffer object populated from the query conditions when no match is found
 *
 * @method SelProductOffer findOneByProductId(int $PRODUCT_ID) Return the first SelProductOffer filtered by the PRODUCT_ID column
 * @method SelProductOffer findOneByType(string $TYPE) Return the first SelProductOffer filtered by the TYPE column
 * @method SelProductOffer findOneByInitialDate(string $INITIAL_DATE) Return the first SelProductOffer filtered by the INITIAL_DATE column
 * @method SelProductOffer findOneByEndDate(string $END_DATE) Return the first SelProductOffer filtered by the END_DATE column
 * @method SelProductOffer findOneByPosition(int $POSiTION) Return the first SelProductOffer filtered by the POSiTION column
 *
 * @method array findById(int $ID) Return SelProductOffer objects filtered by the ID column
 * @method array findByProductId(int $PRODUCT_ID) Return SelProductOffer objects filtered by the PRODUCT_ID column
 * @method array findByType(string $TYPE) Return SelProductOffer objects filtered by the TYPE column
 * @method array findByInitialDate(string $INITIAL_DATE) Return SelProductOffer objects filtered by the INITIAL_DATE column
 * @method array findByEndDate(string $END_DATE) Return SelProductOffer objects filtered by the END_DATE column
 * @method array findByPosition(int $POSiTION) Return SelProductOffer objects filtered by the POSiTION column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProductOfferQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelProductOfferQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelProductOffer', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelProductOfferQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelProductOfferQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelProductOfferQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelProductOfferQuery) {
            return $criteria;
        }
        $query = new SelProductOfferQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelProductOffer|SelProductOffer[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelProductOfferPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelProductOfferPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductOffer A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductOffer A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `PRODUCT_ID`, `TYPE`, `INITIAL_DATE`, `END_DATE`, `POSiTION` FROM `sel_product_offer` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelProductOffer();
            $obj->hydrate($row);
            SelProductOfferPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelProductOffer|SelProductOffer[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelProductOffer[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelProductOfferQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelProductOfferPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelProductOfferQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelProductOfferPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @see       filterBySelProduct()
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOfferQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelProductOfferPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the PRODUCT_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByProductId(1234); // WHERE PRODUCT_ID = 1234
     * $query->filterByProductId(array(12, 34)); // WHERE PRODUCT_ID IN (12, 34)
     * $query->filterByProductId(array('min' => 12)); // WHERE PRODUCT_ID > 12
     * </code>
     *
     * @param     mixed $productId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOfferQuery The current query, for fluid interface
     */
    public function filterByProductId($productId = null, $comparison = null)
    {
        if (is_array($productId)) {
            $useMinMax = false;
            if (isset($productId['min'])) {
                $this->addUsingAlias(SelProductOfferPeer::PRODUCT_ID, $productId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productId['max'])) {
                $this->addUsingAlias(SelProductOfferPeer::PRODUCT_ID, $productId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductOfferPeer::PRODUCT_ID, $productId, $comparison);
    }

    /**
     * Filter the query on the TYPE column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE TYPE = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE TYPE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOfferQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductOfferPeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the INITIAL_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByInitialDate('2011-03-14'); // WHERE INITIAL_DATE = '2011-03-14'
     * $query->filterByInitialDate('now'); // WHERE INITIAL_DATE = '2011-03-14'
     * $query->filterByInitialDate(array('max' => 'yesterday')); // WHERE INITIAL_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $initialDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOfferQuery The current query, for fluid interface
     */
    public function filterByInitialDate($initialDate = null, $comparison = null)
    {
        if (is_array($initialDate)) {
            $useMinMax = false;
            if (isset($initialDate['min'])) {
                $this->addUsingAlias(SelProductOfferPeer::INITIAL_DATE, $initialDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($initialDate['max'])) {
                $this->addUsingAlias(SelProductOfferPeer::INITIAL_DATE, $initialDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductOfferPeer::INITIAL_DATE, $initialDate, $comparison);
    }

    /**
     * Filter the query on the END_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByEndDate('2011-03-14'); // WHERE END_DATE = '2011-03-14'
     * $query->filterByEndDate('now'); // WHERE END_DATE = '2011-03-14'
     * $query->filterByEndDate(array('max' => 'yesterday')); // WHERE END_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $endDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOfferQuery The current query, for fluid interface
     */
    public function filterByEndDate($endDate = null, $comparison = null)
    {
        if (is_array($endDate)) {
            $useMinMax = false;
            if (isset($endDate['min'])) {
                $this->addUsingAlias(SelProductOfferPeer::END_DATE, $endDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endDate['max'])) {
                $this->addUsingAlias(SelProductOfferPeer::END_DATE, $endDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductOfferPeer::END_DATE, $endDate, $comparison);
    }

    /**
     * Filter the query on the POSiTION column
     *
     * Example usage:
     * <code>
     * $query->filterByPosition(1234); // WHERE POSiTION = 1234
     * $query->filterByPosition(array(12, 34)); // WHERE POSiTION IN (12, 34)
     * $query->filterByPosition(array('min' => 12)); // WHERE POSiTION > 12
     * </code>
     *
     * @param     mixed $position The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOfferQuery The current query, for fluid interface
     */
    public function filterByPosition($position = null, $comparison = null)
    {
        if (is_array($position)) {
            $useMinMax = false;
            if (isset($position['min'])) {
                $this->addUsingAlias(SelProductOfferPeer::POSITION, $position['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($position['max'])) {
                $this->addUsingAlias(SelProductOfferPeer::POSITION, $position['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductOfferPeer::POSITION, $position, $comparison);
    }

    /**
     * Filter the query by a related SelProduct object
     *
     * @param   SelProduct|PropelObjectCollection $selProduct The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductOfferQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProduct($selProduct, $comparison = null)
    {
        if ($selProduct instanceof SelProduct) {
            return $this
                ->addUsingAlias(SelProductOfferPeer::ID, $selProduct->getId(), $comparison);
        } elseif ($selProduct instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductOfferPeer::ID, $selProduct->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelProduct() only accepts arguments of type SelProduct or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProduct relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductOfferQuery The current query, for fluid interface
     */
    public function joinSelProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProduct');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProduct');
        }

        return $this;
    }

    /**
     * Use the SelProduct relation SelProduct object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductQuery A secondary query class using the current class as primary query
     */
    public function useSelProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProduct', 'SelProductQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelProductOffer $selProductOffer Object to remove from the list of results
     *
     * @return SelProductOfferQuery The current query, for fluid interface
     */
    public function prune($selProductOffer = null)
    {
        if ($selProductOffer) {
            $this->addUsingAlias(SelProductOfferPeer::ID, $selProductOffer->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
