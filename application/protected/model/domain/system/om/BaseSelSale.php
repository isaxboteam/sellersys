<?php


/**
 * Base class that represents a row from the 'sel_sale' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelSale extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelSalePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelSalePeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the branch_id field.
     * @var        int
     */
    protected $branch_id;

    /**
     * The value for the employee_id field.
     * @var        int
     */
    protected $employee_id;

    /**
     * The value for the customer_id field.
     * @var        int
     */
    protected $customer_id;

    /**
     * The value for the date field.
     * @var        string
     */
    protected $date;

    /**
     * The value for the status field.
     * @var        string
     */
    protected $status;

    /**
     * The value for the code field.
     * @var        string
     */
    protected $code;

    /**
     * The value for the money_change field.
     * @var        int
     */
    protected $money_change;

    /**
     * The value for the observation field.
     * @var        string
     */
    protected $observation;

    /**
     * @var        SelCustomer
     */
    protected $aSelCustomer;

    /**
     * @var        SelBranch
     */
    protected $aSelBranch;

    /**
     * @var        SelEmployee
     */
    protected $aSelEmployee;

    /**
     * @var        PropelObjectCollection|SelOrder[] Collection to store aggregation of SelOrder objects.
     */
    protected $collSelOrders;
    protected $collSelOrdersPartial;

    /**
     * @var        PropelObjectCollection|SelProductSell[] Collection to store aggregation of SelProductSell objects.
     */
    protected $collSelProductSells;
    protected $collSelProductSellsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selOrdersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductSellsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [branch_id] column value.
     * 
     * @return int
     */
    public function getBranchId()
    {
        return $this->branch_id;
    }

    /**
     * Get the [employee_id] column value.
     * 
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employee_id;
    }

    /**
     * Get the [customer_id] column value.
     * 
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Get the [optionally formatted] temporal [date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDate($format = 'Y-m-d H:i:s')
    {
        if ($this->date === null) {
            return null;
        }

        if ($this->date === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [status] column value.
     * 
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [code] column value.
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [money_change] column value.
     * 
     * @return int
     */
    public function getMoneyChange()
    {
        return $this->money_change;
    }

    /**
     * Get the [observation] column value.
     * 
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelSale The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelSalePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [branch_id] column.
     * 
     * @param int $v new value
     * @return SelSale The current object (for fluent API support)
     */
    public function setBranchId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->branch_id !== $v) {
            $this->branch_id = $v;
            $this->modifiedColumns[] = SelSalePeer::BRANCH_ID;
        }

        if ($this->aSelBranch !== null && $this->aSelBranch->getId() !== $v) {
            $this->aSelBranch = null;
        }


        return $this;
    } // setBranchId()

    /**
     * Set the value of [employee_id] column.
     * 
     * @param int $v new value
     * @return SelSale The current object (for fluent API support)
     */
    public function setEmployeeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->employee_id !== $v) {
            $this->employee_id = $v;
            $this->modifiedColumns[] = SelSalePeer::EMPLOYEE_ID;
        }

        if ($this->aSelEmployee !== null && $this->aSelEmployee->getId() !== $v) {
            $this->aSelEmployee = null;
        }


        return $this;
    } // setEmployeeId()

    /**
     * Set the value of [customer_id] column.
     * 
     * @param int $v new value
     * @return SelSale The current object (for fluent API support)
     */
    public function setCustomerId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->customer_id !== $v) {
            $this->customer_id = $v;
            $this->modifiedColumns[] = SelSalePeer::CUSTOMER_ID;
        }

        if ($this->aSelCustomer !== null && $this->aSelCustomer->getId() !== $v) {
            $this->aSelCustomer = null;
        }


        return $this;
    } // setCustomerId()

    /**
     * Sets the value of [date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelSale The current object (for fluent API support)
     */
    public function setDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date !== null || $dt !== null) {
            $currentDateAsString = ($this->date !== null && $tmpDt = new DateTime($this->date)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date = $newDateAsString;
                $this->modifiedColumns[] = SelSalePeer::DATE;
            }
        } // if either are not null


        return $this;
    } // setDate()

    /**
     * Set the value of [status] column.
     * 
     * @param string $v new value
     * @return SelSale The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[] = SelSalePeer::STATUS;
        }


        return $this;
    } // setStatus()

    /**
     * Set the value of [code] column.
     * 
     * @param string $v new value
     * @return SelSale The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[] = SelSalePeer::CODE;
        }


        return $this;
    } // setCode()

    /**
     * Set the value of [money_change] column.
     * 
     * @param int $v new value
     * @return SelSale The current object (for fluent API support)
     */
    public function setMoneyChange($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->money_change !== $v) {
            $this->money_change = $v;
            $this->modifiedColumns[] = SelSalePeer::MONEY_CHANGE;
        }


        return $this;
    } // setMoneyChange()

    /**
     * Set the value of [observation] column.
     * 
     * @param string $v new value
     * @return SelSale The current object (for fluent API support)
     */
    public function setObservation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observation !== $v) {
            $this->observation = $v;
            $this->modifiedColumns[] = SelSalePeer::OBSERVATION;
        }


        return $this;
    } // setObservation()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->branch_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->employee_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->customer_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->date = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->status = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->code = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->money_change = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->observation = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 9; // 9 = SelSalePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelSale object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSelBranch !== null && $this->branch_id !== $this->aSelBranch->getId()) {
            $this->aSelBranch = null;
        }
        if ($this->aSelEmployee !== null && $this->employee_id !== $this->aSelEmployee->getId()) {
            $this->aSelEmployee = null;
        }
        if ($this->aSelCustomer !== null && $this->customer_id !== $this->aSelCustomer->getId()) {
            $this->aSelCustomer = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelSalePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelSalePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSelCustomer = null;
            $this->aSelBranch = null;
            $this->aSelEmployee = null;
            $this->collSelOrders = null;

            $this->collSelProductSells = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelSalePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelSaleQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelSalePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelSalePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelCustomer !== null) {
                if ($this->aSelCustomer->isModified() || $this->aSelCustomer->isNew()) {
                    $affectedRows += $this->aSelCustomer->save($con);
                }
                $this->setSelCustomer($this->aSelCustomer);
            }

            if ($this->aSelBranch !== null) {
                if ($this->aSelBranch->isModified() || $this->aSelBranch->isNew()) {
                    $affectedRows += $this->aSelBranch->save($con);
                }
                $this->setSelBranch($this->aSelBranch);
            }

            if ($this->aSelEmployee !== null) {
                if ($this->aSelEmployee->isModified() || $this->aSelEmployee->isNew()) {
                    $affectedRows += $this->aSelEmployee->save($con);
                }
                $this->setSelEmployee($this->aSelEmployee);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->selOrdersScheduledForDeletion !== null) {
                if (!$this->selOrdersScheduledForDeletion->isEmpty()) {
                    foreach ($this->selOrdersScheduledForDeletion as $selOrder) {
                        // need to save related object because we set the relation to null
                        $selOrder->save($con);
                    }
                    $this->selOrdersScheduledForDeletion = null;
                }
            }

            if ($this->collSelOrders !== null) {
                foreach ($this->collSelOrders as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selProductSellsScheduledForDeletion !== null) {
                if (!$this->selProductSellsScheduledForDeletion->isEmpty()) {
                    SelProductSellQuery::create()
                        ->filterByPrimaryKeys($this->selProductSellsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selProductSellsScheduledForDeletion = null;
                }
            }

            if ($this->collSelProductSells !== null) {
                foreach ($this->collSelProductSells as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SelSalePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SelSalePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelSalePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelSalePeer::BRANCH_ID)) {
            $modifiedColumns[':p' . $index++]  = '`BRANCH_ID`';
        }
        if ($this->isColumnModified(SelSalePeer::EMPLOYEE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`EMPLOYEE_ID`';
        }
        if ($this->isColumnModified(SelSalePeer::CUSTOMER_ID)) {
            $modifiedColumns[':p' . $index++]  = '`CUSTOMER_ID`';
        }
        if ($this->isColumnModified(SelSalePeer::DATE)) {
            $modifiedColumns[':p' . $index++]  = '`DATE`';
        }
        if ($this->isColumnModified(SelSalePeer::STATUS)) {
            $modifiedColumns[':p' . $index++]  = '`STATUS`';
        }
        if ($this->isColumnModified(SelSalePeer::CODE)) {
            $modifiedColumns[':p' . $index++]  = '`CODE`';
        }
        if ($this->isColumnModified(SelSalePeer::MONEY_CHANGE)) {
            $modifiedColumns[':p' . $index++]  = '`MONEY_CHANGE`';
        }
        if ($this->isColumnModified(SelSalePeer::OBSERVATION)) {
            $modifiedColumns[':p' . $index++]  = '`OBSERVATION`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_sale` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`BRANCH_ID`':						
                        $stmt->bindValue($identifier, $this->branch_id, PDO::PARAM_INT);
                        break;
                    case '`EMPLOYEE_ID`':						
                        $stmt->bindValue($identifier, $this->employee_id, PDO::PARAM_INT);
                        break;
                    case '`CUSTOMER_ID`':						
                        $stmt->bindValue($identifier, $this->customer_id, PDO::PARAM_INT);
                        break;
                    case '`DATE`':						
                        $stmt->bindValue($identifier, $this->date, PDO::PARAM_STR);
                        break;
                    case '`STATUS`':						
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_STR);
                        break;
                    case '`CODE`':						
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case '`MONEY_CHANGE`':						
                        $stmt->bindValue($identifier, $this->money_change, PDO::PARAM_INT);
                        break;
                    case '`OBSERVATION`':						
                        $stmt->bindValue($identifier, $this->observation, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelCustomer !== null) {
                if (!$this->aSelCustomer->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelCustomer->getValidationFailures());
                }
            }

            if ($this->aSelBranch !== null) {
                if (!$this->aSelBranch->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelBranch->getValidationFailures());
                }
            }

            if ($this->aSelEmployee !== null) {
                if (!$this->aSelEmployee->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelEmployee->getValidationFailures());
                }
            }

            if (($retval = SelSalePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelOrders !== null) {
                    foreach ($this->collSelOrders as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelProductSells !== null) {
                    foreach ($this->collSelProductSells as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelSalePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getBranchId();
                break;
            case 2:
                return $this->getEmployeeId();
                break;
            case 3:
                return $this->getCustomerId();
                break;
            case 4:
                return $this->getDate();
                break;
            case 5:
                return $this->getStatus();
                break;
            case 6:
                return $this->getCode();
                break;
            case 7:
                return $this->getMoneyChange();
                break;
            case 8:
                return $this->getObservation();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelSale'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelSale'][$this->getPrimaryKey()] = true;
        $keys = SelSalePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getBranchId(),
            $keys[2] => $this->getEmployeeId(),
            $keys[3] => $this->getCustomerId(),
            $keys[4] => $this->getDate(),
            $keys[5] => $this->getStatus(),
            $keys[6] => $this->getCode(),
            $keys[7] => $this->getMoneyChange(),
            $keys[8] => $this->getObservation(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSelCustomer) {
                $result['SelCustomer'] = $this->aSelCustomer->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSelBranch) {
                $result['SelBranch'] = $this->aSelBranch->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSelEmployee) {
                $result['SelEmployee'] = $this->aSelEmployee->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSelOrders) {
                $result['SelOrders'] = $this->collSelOrders->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelProductSells) {
                $result['SelProductSells'] = $this->collSelProductSells->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelSalePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setBranchId($value);
                break;
            case 2:
                $this->setEmployeeId($value);
                break;
            case 3:
                $this->setCustomerId($value);
                break;
            case 4:
                $this->setDate($value);
                break;
            case 5:
                $this->setStatus($value);
                break;
            case 6:
                $this->setCode($value);
                break;
            case 7:
                $this->setMoneyChange($value);
                break;
            case 8:
                $this->setObservation($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelSalePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setBranchId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setEmployeeId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCustomerId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDate($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setStatus($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCode($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setMoneyChange($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setObservation($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelSalePeer::DATABASE_NAME);

        if ($this->isColumnModified(SelSalePeer::ID)) $criteria->add(SelSalePeer::ID, $this->id);
        if ($this->isColumnModified(SelSalePeer::BRANCH_ID)) $criteria->add(SelSalePeer::BRANCH_ID, $this->branch_id);
        if ($this->isColumnModified(SelSalePeer::EMPLOYEE_ID)) $criteria->add(SelSalePeer::EMPLOYEE_ID, $this->employee_id);
        if ($this->isColumnModified(SelSalePeer::CUSTOMER_ID)) $criteria->add(SelSalePeer::CUSTOMER_ID, $this->customer_id);
        if ($this->isColumnModified(SelSalePeer::DATE)) $criteria->add(SelSalePeer::DATE, $this->date);
        if ($this->isColumnModified(SelSalePeer::STATUS)) $criteria->add(SelSalePeer::STATUS, $this->status);
        if ($this->isColumnModified(SelSalePeer::CODE)) $criteria->add(SelSalePeer::CODE, $this->code);
        if ($this->isColumnModified(SelSalePeer::MONEY_CHANGE)) $criteria->add(SelSalePeer::MONEY_CHANGE, $this->money_change);
        if ($this->isColumnModified(SelSalePeer::OBSERVATION)) $criteria->add(SelSalePeer::OBSERVATION, $this->observation);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelSalePeer::DATABASE_NAME);
        $criteria->add(SelSalePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelSale (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBranchId($this->getBranchId());
        $copyObj->setEmployeeId($this->getEmployeeId());
        $copyObj->setCustomerId($this->getCustomerId());
        $copyObj->setDate($this->getDate());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setCode($this->getCode());
        $copyObj->setMoneyChange($this->getMoneyChange());
        $copyObj->setObservation($this->getObservation());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelOrders() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelOrder($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelProductSells() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProductSell($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelSale Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelSalePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelSalePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SelCustomer object.
     *
     * @param             SelCustomer $v
     * @return SelSale The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelCustomer(SelCustomer $v = null)
    {
        if ($v === null) {
            $this->setCustomerId(NULL);
        } else {
            $this->setCustomerId($v->getId());
        }

        $this->aSelCustomer = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelCustomer object, it will not be re-added.
        if ($v !== null) {
            $v->addSelSale($this);
        }


        return $this;
    }


    /**
     * Get the associated SelCustomer object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelCustomer The associated SelCustomer object.
     * @throws PropelException
     */
    public function getSelCustomer(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelCustomer === null && ($this->customer_id !== null) && $doQuery) {
            $this->aSelCustomer = SelCustomerQuery::create()->findPk($this->customer_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelCustomer->addSelSales($this);
             */
        }

        return $this->aSelCustomer;
    }

    /**
     * Declares an association between this object and a SelBranch object.
     *
     * @param             SelBranch $v
     * @return SelSale The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelBranch(SelBranch $v = null)
    {
        if ($v === null) {
            $this->setBranchId(NULL);
        } else {
            $this->setBranchId($v->getId());
        }

        $this->aSelBranch = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelBranch object, it will not be re-added.
        if ($v !== null) {
            $v->addSelSale($this);
        }


        return $this;
    }


    /**
     * Get the associated SelBranch object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelBranch The associated SelBranch object.
     * @throws PropelException
     */
    public function getSelBranch(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelBranch === null && ($this->branch_id !== null) && $doQuery) {
            $this->aSelBranch = SelBranchQuery::create()->findPk($this->branch_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelBranch->addSelSales($this);
             */
        }

        return $this->aSelBranch;
    }

    /**
     * Declares an association between this object and a SelEmployee object.
     *
     * @param             SelEmployee $v
     * @return SelSale The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelEmployee(SelEmployee $v = null)
    {
        if ($v === null) {
            $this->setEmployeeId(NULL);
        } else {
            $this->setEmployeeId($v->getId());
        }

        $this->aSelEmployee = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelEmployee object, it will not be re-added.
        if ($v !== null) {
            $v->addSelSale($this);
        }


        return $this;
    }


    /**
     * Get the associated SelEmployee object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelEmployee The associated SelEmployee object.
     * @throws PropelException
     */
    public function getSelEmployee(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelEmployee === null && ($this->employee_id !== null) && $doQuery) {
            $this->aSelEmployee = SelEmployeeQuery::create()->findPk($this->employee_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelEmployee->addSelSales($this);
             */
        }

        return $this->aSelEmployee;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelOrder' == $relationName) {
            $this->initSelOrders();
        }
        if ('SelProductSell' == $relationName) {
            $this->initSelProductSells();
        }
    }

    /**
     * Clears out the collSelOrders collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelSale The current object (for fluent API support)
     * @see        addSelOrders()
     */
    public function clearSelOrders()
    {
        $this->collSelOrders = null; // important to set this to null since that means it is uninitialized
        $this->collSelOrdersPartial = null;

        return $this;
    }

    /**
     * reset is the collSelOrders collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelOrders($v = true)
    {
        $this->collSelOrdersPartial = $v;
    }

    /**
     * Initializes the collSelOrders collection.
     *
     * By default this just sets the collSelOrders collection to an empty array (like clearcollSelOrders());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelOrders($overrideExisting = true)
    {
        if (null !== $this->collSelOrders && !$overrideExisting) {
            return;
        }
        $this->collSelOrders = new PropelObjectCollection();
        $this->collSelOrders->setModel('SelOrder');
    }

    /**
     * Gets an array of SelOrder objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelSale is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelOrder[] List of SelOrder objects
     * @throws PropelException
     */
    public function getSelOrders($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelOrdersPartial && !$this->isNew();
        if (null === $this->collSelOrders || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelOrders) {
                // return empty collection
                $this->initSelOrders();
            } else {
                $collSelOrders = SelOrderQuery::create(null, $criteria)
                    ->filterBySelSale($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelOrdersPartial && count($collSelOrders)) {
                      $this->initSelOrders(false);

                      foreach($collSelOrders as $obj) {
                        if (false == $this->collSelOrders->contains($obj)) {
                          $this->collSelOrders->append($obj);
                        }
                      }

                      $this->collSelOrdersPartial = true;
                    }

                    return $collSelOrders;
                }

                if($partial && $this->collSelOrders) {
                    foreach($this->collSelOrders as $obj) {
                        if($obj->isNew()) {
                            $collSelOrders[] = $obj;
                        }
                    }
                }

                $this->collSelOrders = $collSelOrders;
                $this->collSelOrdersPartial = false;
            }
        }

        return $this->collSelOrders;
    }

    /**
     * Sets a collection of SelOrder objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selOrders A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelSale The current object (for fluent API support)
     */
    public function setSelOrders(PropelCollection $selOrders, PropelPDO $con = null)
    {
        $selOrdersToDelete = $this->getSelOrders(new Criteria(), $con)->diff($selOrders);

        $this->selOrdersScheduledForDeletion = unserialize(serialize($selOrdersToDelete));

        foreach ($selOrdersToDelete as $selOrderRemoved) {
            $selOrderRemoved->setSelSale(null);
        }

        $this->collSelOrders = null;
        foreach ($selOrders as $selOrder) {
            $this->addSelOrder($selOrder);
        }

        $this->collSelOrders = $selOrders;
        $this->collSelOrdersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelOrder objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelOrder objects.
     * @throws PropelException
     */
    public function countSelOrders(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelOrdersPartial && !$this->isNew();
        if (null === $this->collSelOrders || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelOrders) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelOrders());
            }
            $query = SelOrderQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelSale($this)
                ->count($con);
        }

        return count($this->collSelOrders);
    }

    /**
     * Method called to associate a SelOrder object to this object
     * through the SelOrder foreign key attribute.
     *
     * @param    SelOrder $l SelOrder
     * @return SelSale The current object (for fluent API support)
     */
    public function addSelOrder(SelOrder $l)
    {
        if ($this->collSelOrders === null) {
            $this->initSelOrders();
            $this->collSelOrdersPartial = true;
        }
        if (!in_array($l, $this->collSelOrders->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelOrder($l);
        }

        return $this;
    }

    /**
     * @param	SelOrder $selOrder The selOrder object to add.
     */
    protected function doAddSelOrder($selOrder)
    {
        $this->collSelOrders[]= $selOrder;
        $selOrder->setSelSale($this);
    }

    /**
     * @param	SelOrder $selOrder The selOrder object to remove.
     * @return SelSale The current object (for fluent API support)
     */
    public function removeSelOrder($selOrder)
    {
        if ($this->getSelOrders()->contains($selOrder)) {
            $this->collSelOrders->remove($this->collSelOrders->search($selOrder));
            if (null === $this->selOrdersScheduledForDeletion) {
                $this->selOrdersScheduledForDeletion = clone $this->collSelOrders;
                $this->selOrdersScheduledForDeletion->clear();
            }
            $this->selOrdersScheduledForDeletion[]= $selOrder;
            $selOrder->setSelSale(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelSale is new, it will return
     * an empty collection; or if this SelSale has previously
     * been saved, it will retrieve related SelOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelSale.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelOrder[] List of SelOrder objects
     */
    public function getSelOrdersJoinSelCustomer($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelOrderQuery::create(null, $criteria);
        $query->joinWith('SelCustomer', $join_behavior);

        return $this->getSelOrders($query, $con);
    }

    /**
     * Clears out the collSelProductSells collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelSale The current object (for fluent API support)
     * @see        addSelProductSells()
     */
    public function clearSelProductSells()
    {
        $this->collSelProductSells = null; // important to set this to null since that means it is uninitialized
        $this->collSelProductSellsPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProductSells collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProductSells($v = true)
    {
        $this->collSelProductSellsPartial = $v;
    }

    /**
     * Initializes the collSelProductSells collection.
     *
     * By default this just sets the collSelProductSells collection to an empty array (like clearcollSelProductSells());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProductSells($overrideExisting = true)
    {
        if (null !== $this->collSelProductSells && !$overrideExisting) {
            return;
        }
        $this->collSelProductSells = new PropelObjectCollection();
        $this->collSelProductSells->setModel('SelProductSell');
    }

    /**
     * Gets an array of SelProductSell objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelSale is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProductSell[] List of SelProductSell objects
     * @throws PropelException
     */
    public function getSelProductSells($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProductSellsPartial && !$this->isNew();
        if (null === $this->collSelProductSells || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProductSells) {
                // return empty collection
                $this->initSelProductSells();
            } else {
                $collSelProductSells = SelProductSellQuery::create(null, $criteria)
                    ->filterBySelSale($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProductSellsPartial && count($collSelProductSells)) {
                      $this->initSelProductSells(false);

                      foreach($collSelProductSells as $obj) {
                        if (false == $this->collSelProductSells->contains($obj)) {
                          $this->collSelProductSells->append($obj);
                        }
                      }

                      $this->collSelProductSellsPartial = true;
                    }

                    return $collSelProductSells;
                }

                if($partial && $this->collSelProductSells) {
                    foreach($this->collSelProductSells as $obj) {
                        if($obj->isNew()) {
                            $collSelProductSells[] = $obj;
                        }
                    }
                }

                $this->collSelProductSells = $collSelProductSells;
                $this->collSelProductSellsPartial = false;
            }
        }

        return $this->collSelProductSells;
    }

    /**
     * Sets a collection of SelProductSell objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProductSells A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelSale The current object (for fluent API support)
     */
    public function setSelProductSells(PropelCollection $selProductSells, PropelPDO $con = null)
    {
        $selProductSellsToDelete = $this->getSelProductSells(new Criteria(), $con)->diff($selProductSells);

        $this->selProductSellsScheduledForDeletion = unserialize(serialize($selProductSellsToDelete));

        foreach ($selProductSellsToDelete as $selProductSellRemoved) {
            $selProductSellRemoved->setSelSale(null);
        }

        $this->collSelProductSells = null;
        foreach ($selProductSells as $selProductSell) {
            $this->addSelProductSell($selProductSell);
        }

        $this->collSelProductSells = $selProductSells;
        $this->collSelProductSellsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProductSell objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProductSell objects.
     * @throws PropelException
     */
    public function countSelProductSells(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProductSellsPartial && !$this->isNew();
        if (null === $this->collSelProductSells || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProductSells) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProductSells());
            }
            $query = SelProductSellQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelSale($this)
                ->count($con);
        }

        return count($this->collSelProductSells);
    }

    /**
     * Method called to associate a SelProductSell object to this object
     * through the SelProductSell foreign key attribute.
     *
     * @param    SelProductSell $l SelProductSell
     * @return SelSale The current object (for fluent API support)
     */
    public function addSelProductSell(SelProductSell $l)
    {
        if ($this->collSelProductSells === null) {
            $this->initSelProductSells();
            $this->collSelProductSellsPartial = true;
        }
        if (!in_array($l, $this->collSelProductSells->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProductSell($l);
        }

        return $this;
    }

    /**
     * @param	SelProductSell $selProductSell The selProductSell object to add.
     */
    protected function doAddSelProductSell($selProductSell)
    {
        $this->collSelProductSells[]= $selProductSell;
        $selProductSell->setSelSale($this);
    }

    /**
     * @param	SelProductSell $selProductSell The selProductSell object to remove.
     * @return SelSale The current object (for fluent API support)
     */
    public function removeSelProductSell($selProductSell)
    {
        if ($this->getSelProductSells()->contains($selProductSell)) {
            $this->collSelProductSells->remove($this->collSelProductSells->search($selProductSell));
            if (null === $this->selProductSellsScheduledForDeletion) {
                $this->selProductSellsScheduledForDeletion = clone $this->collSelProductSells;
                $this->selProductSellsScheduledForDeletion->clear();
            }
            $this->selProductSellsScheduledForDeletion[]= clone $selProductSell;
            $selProductSell->setSelSale(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelSale is new, it will return
     * an empty collection; or if this SelSale has previously
     * been saved, it will retrieve related SelProductSells from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelSale.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProductSell[] List of SelProductSell objects
     */
    public function getSelProductSellsJoinSelProduct($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductSellQuery::create(null, $criteria);
        $query->joinWith('SelProduct', $join_behavior);

        return $this->getSelProductSells($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->branch_id = null;
        $this->employee_id = null;
        $this->customer_id = null;
        $this->date = null;
        $this->status = null;
        $this->code = null;
        $this->money_change = null;
        $this->observation = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSelOrders) {
                foreach ($this->collSelOrders as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelProductSells) {
                foreach ($this->collSelProductSells as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collSelOrders instanceof PropelCollection) {
            $this->collSelOrders->clearIterator();
        }
        $this->collSelOrders = null;
        if ($this->collSelProductSells instanceof PropelCollection) {
            $this->collSelProductSells->clearIterator();
        }
        $this->collSelProductSells = null;
        $this->aSelCustomer = null;
        $this->aSelBranch = null;
        $this->aSelEmployee = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelSalePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
