<?php


/**
 * Base class that represents a row from the 'sel_branch' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelBranch extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelBranchPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelBranchPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the number field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $number;

    /**
     * The value for the status field.
     * Note: this column has a database default value of: 'OPEN'
     * @var        string
     */
    protected $status;

    /**
     * The value for the code field.
     * @var        string
     */
    protected $code;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the address field.
     * @var        string
     */
    protected $address;

    /**
     * The value for the phone field.
     * @var        string
     */
    protected $phone;

    /**
     * The value for the fax field.
     * @var        string
     */
    protected $fax;

    /**
     * @var        PropelObjectCollection|SelBranchCategory[] Collection to store aggregation of SelBranchCategory objects.
     */
    protected $collSelBranchCategorys;
    protected $collSelBranchCategorysPartial;

    /**
     * @var        PropelObjectCollection|SelBranchEmployee[] Collection to store aggregation of SelBranchEmployee objects.
     */
    protected $collSelBranchEmployees;
    protected $collSelBranchEmployeesPartial;

    /**
     * @var        PropelObjectCollection|SelProductItem[] Collection to store aggregation of SelProductItem objects.
     */
    protected $collSelProductItems;
    protected $collSelProductItemsPartial;

    /**
     * @var        PropelObjectCollection|SelSale[] Collection to store aggregation of SelSale objects.
     */
    protected $collSelSales;
    protected $collSelSalesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selBranchCategorysScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selBranchEmployeesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductItemsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selSalesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->number = 0;
        $this->status = 'OPEN';
    }

    /**
     * Initializes internal state of BaseSelBranch object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [number] column value.
     * 
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get the [status] column value.
     * 
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [code] column value.
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [name] column value.
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [address] column value.
     * 
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get the [phone] column value.
     * 
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get the [fax] column value.
     * 
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelBranch The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelBranchPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [number] column.
     * 
     * @param int $v new value
     * @return SelBranch The current object (for fluent API support)
     */
    public function setNumber($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->number !== $v) {
            $this->number = $v;
            $this->modifiedColumns[] = SelBranchPeer::NUMBER;
        }


        return $this;
    } // setNumber()

    /**
     * Set the value of [status] column.
     * 
     * @param string $v new value
     * @return SelBranch The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[] = SelBranchPeer::STATUS;
        }


        return $this;
    } // setStatus()

    /**
     * Set the value of [code] column.
     * 
     * @param string $v new value
     * @return SelBranch The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[] = SelBranchPeer::CODE;
        }


        return $this;
    } // setCode()

    /**
     * Set the value of [name] column.
     * 
     * @param string $v new value
     * @return SelBranch The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = SelBranchPeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [address] column.
     * 
     * @param string $v new value
     * @return SelBranch The current object (for fluent API support)
     */
    public function setAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->address !== $v) {
            $this->address = $v;
            $this->modifiedColumns[] = SelBranchPeer::ADDRESS;
        }


        return $this;
    } // setAddress()

    /**
     * Set the value of [phone] column.
     * 
     * @param string $v new value
     * @return SelBranch The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[] = SelBranchPeer::PHONE;
        }


        return $this;
    } // setPhone()

    /**
     * Set the value of [fax] column.
     * 
     * @param string $v new value
     * @return SelBranch The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[] = SelBranchPeer::FAX;
        }


        return $this;
    } // setFax()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->number !== 0) {
                return false;
            }

            if ($this->status !== 'OPEN') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->number = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->status = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->code = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->name = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->address = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->phone = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->fax = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 8; // 8 = SelBranchPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelBranch object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelBranchPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelBranchPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collSelBranchCategorys = null;

            $this->collSelBranchEmployees = null;

            $this->collSelProductItems = null;

            $this->collSelSales = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelBranchPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelBranchQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelBranchPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelBranchPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->selBranchCategorysScheduledForDeletion !== null) {
                if (!$this->selBranchCategorysScheduledForDeletion->isEmpty()) {
                    SelBranchCategoryQuery::create()
                        ->filterByPrimaryKeys($this->selBranchCategorysScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selBranchCategorysScheduledForDeletion = null;
                }
            }

            if ($this->collSelBranchCategorys !== null) {
                foreach ($this->collSelBranchCategorys as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selBranchEmployeesScheduledForDeletion !== null) {
                if (!$this->selBranchEmployeesScheduledForDeletion->isEmpty()) {
                    SelBranchEmployeeQuery::create()
                        ->filterByPrimaryKeys($this->selBranchEmployeesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selBranchEmployeesScheduledForDeletion = null;
                }
            }

            if ($this->collSelBranchEmployees !== null) {
                foreach ($this->collSelBranchEmployees as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selProductItemsScheduledForDeletion !== null) {
                if (!$this->selProductItemsScheduledForDeletion->isEmpty()) {
                    foreach ($this->selProductItemsScheduledForDeletion as $selProductItem) {
                        // need to save related object because we set the relation to null
                        $selProductItem->save($con);
                    }
                    $this->selProductItemsScheduledForDeletion = null;
                }
            }

            if ($this->collSelProductItems !== null) {
                foreach ($this->collSelProductItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selSalesScheduledForDeletion !== null) {
                if (!$this->selSalesScheduledForDeletion->isEmpty()) {
                    SelSaleQuery::create()
                        ->filterByPrimaryKeys($this->selSalesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selSalesScheduledForDeletion = null;
                }
            }

            if ($this->collSelSales !== null) {
                foreach ($this->collSelSales as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SelBranchPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SelBranchPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelBranchPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelBranchPeer::NUMBER)) {
            $modifiedColumns[':p' . $index++]  = '`NUMBER`';
        }
        if ($this->isColumnModified(SelBranchPeer::STATUS)) {
            $modifiedColumns[':p' . $index++]  = '`STATUS`';
        }
        if ($this->isColumnModified(SelBranchPeer::CODE)) {
            $modifiedColumns[':p' . $index++]  = '`CODE`';
        }
        if ($this->isColumnModified(SelBranchPeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`NAME`';
        }
        if ($this->isColumnModified(SelBranchPeer::ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`ADDRESS`';
        }
        if ($this->isColumnModified(SelBranchPeer::PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`PHONE`';
        }
        if ($this->isColumnModified(SelBranchPeer::FAX)) {
            $modifiedColumns[':p' . $index++]  = '`FAX`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_branch` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`NUMBER`':						
                        $stmt->bindValue($identifier, $this->number, PDO::PARAM_INT);
                        break;
                    case '`STATUS`':						
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_STR);
                        break;
                    case '`CODE`':						
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case '`NAME`':						
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`ADDRESS`':						
                        $stmt->bindValue($identifier, $this->address, PDO::PARAM_STR);
                        break;
                    case '`PHONE`':						
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case '`FAX`':						
                        $stmt->bindValue($identifier, $this->fax, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();

            if (($retval = SelBranchPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelBranchCategorys !== null) {
                    foreach ($this->collSelBranchCategorys as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelBranchEmployees !== null) {
                    foreach ($this->collSelBranchEmployees as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelProductItems !== null) {
                    foreach ($this->collSelProductItems as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelSales !== null) {
                    foreach ($this->collSelSales as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelBranchPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNumber();
                break;
            case 2:
                return $this->getStatus();
                break;
            case 3:
                return $this->getCode();
                break;
            case 4:
                return $this->getName();
                break;
            case 5:
                return $this->getAddress();
                break;
            case 6:
                return $this->getPhone();
                break;
            case 7:
                return $this->getFax();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelBranch'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelBranch'][$this->getPrimaryKey()] = true;
        $keys = SelBranchPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNumber(),
            $keys[2] => $this->getStatus(),
            $keys[3] => $this->getCode(),
            $keys[4] => $this->getName(),
            $keys[5] => $this->getAddress(),
            $keys[6] => $this->getPhone(),
            $keys[7] => $this->getFax(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collSelBranchCategorys) {
                $result['SelBranchCategorys'] = $this->collSelBranchCategorys->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelBranchEmployees) {
                $result['SelBranchEmployees'] = $this->collSelBranchEmployees->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelProductItems) {
                $result['SelProductItems'] = $this->collSelProductItems->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelSales) {
                $result['SelSales'] = $this->collSelSales->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelBranchPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNumber($value);
                break;
            case 2:
                $this->setStatus($value);
                break;
            case 3:
                $this->setCode($value);
                break;
            case 4:
                $this->setName($value);
                break;
            case 5:
                $this->setAddress($value);
                break;
            case 6:
                $this->setPhone($value);
                break;
            case 7:
                $this->setFax($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelBranchPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNumber($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setStatus($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCode($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setName($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setAddress($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setPhone($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setFax($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelBranchPeer::DATABASE_NAME);

        if ($this->isColumnModified(SelBranchPeer::ID)) $criteria->add(SelBranchPeer::ID, $this->id);
        if ($this->isColumnModified(SelBranchPeer::NUMBER)) $criteria->add(SelBranchPeer::NUMBER, $this->number);
        if ($this->isColumnModified(SelBranchPeer::STATUS)) $criteria->add(SelBranchPeer::STATUS, $this->status);
        if ($this->isColumnModified(SelBranchPeer::CODE)) $criteria->add(SelBranchPeer::CODE, $this->code);
        if ($this->isColumnModified(SelBranchPeer::NAME)) $criteria->add(SelBranchPeer::NAME, $this->name);
        if ($this->isColumnModified(SelBranchPeer::ADDRESS)) $criteria->add(SelBranchPeer::ADDRESS, $this->address);
        if ($this->isColumnModified(SelBranchPeer::PHONE)) $criteria->add(SelBranchPeer::PHONE, $this->phone);
        if ($this->isColumnModified(SelBranchPeer::FAX)) $criteria->add(SelBranchPeer::FAX, $this->fax);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelBranchPeer::DATABASE_NAME);
        $criteria->add(SelBranchPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelBranch (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNumber($this->getNumber());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setCode($this->getCode());
        $copyObj->setName($this->getName());
        $copyObj->setAddress($this->getAddress());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setFax($this->getFax());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelBranchCategorys() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelBranchCategory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelBranchEmployees() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelBranchEmployee($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelProductItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProductItem($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelSales() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelSale($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelBranch Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelBranchPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelBranchPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelBranchCategory' == $relationName) {
            $this->initSelBranchCategorys();
        }
        if ('SelBranchEmployee' == $relationName) {
            $this->initSelBranchEmployees();
        }
        if ('SelProductItem' == $relationName) {
            $this->initSelProductItems();
        }
        if ('SelSale' == $relationName) {
            $this->initSelSales();
        }
    }

    /**
     * Clears out the collSelBranchCategorys collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelBranch The current object (for fluent API support)
     * @see        addSelBranchCategorys()
     */
    public function clearSelBranchCategorys()
    {
        $this->collSelBranchCategorys = null; // important to set this to null since that means it is uninitialized
        $this->collSelBranchCategorysPartial = null;

        return $this;
    }

    /**
     * reset is the collSelBranchCategorys collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelBranchCategorys($v = true)
    {
        $this->collSelBranchCategorysPartial = $v;
    }

    /**
     * Initializes the collSelBranchCategorys collection.
     *
     * By default this just sets the collSelBranchCategorys collection to an empty array (like clearcollSelBranchCategorys());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelBranchCategorys($overrideExisting = true)
    {
        if (null !== $this->collSelBranchCategorys && !$overrideExisting) {
            return;
        }
        $this->collSelBranchCategorys = new PropelObjectCollection();
        $this->collSelBranchCategorys->setModel('SelBranchCategory');
    }

    /**
     * Gets an array of SelBranchCategory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelBranch is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelBranchCategory[] List of SelBranchCategory objects
     * @throws PropelException
     */
    public function getSelBranchCategorys($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelBranchCategorysPartial && !$this->isNew();
        if (null === $this->collSelBranchCategorys || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelBranchCategorys) {
                // return empty collection
                $this->initSelBranchCategorys();
            } else {
                $collSelBranchCategorys = SelBranchCategoryQuery::create(null, $criteria)
                    ->filterBySelBranch($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelBranchCategorysPartial && count($collSelBranchCategorys)) {
                      $this->initSelBranchCategorys(false);

                      foreach($collSelBranchCategorys as $obj) {
                        if (false == $this->collSelBranchCategorys->contains($obj)) {
                          $this->collSelBranchCategorys->append($obj);
                        }
                      }

                      $this->collSelBranchCategorysPartial = true;
                    }

                    return $collSelBranchCategorys;
                }

                if($partial && $this->collSelBranchCategorys) {
                    foreach($this->collSelBranchCategorys as $obj) {
                        if($obj->isNew()) {
                            $collSelBranchCategorys[] = $obj;
                        }
                    }
                }

                $this->collSelBranchCategorys = $collSelBranchCategorys;
                $this->collSelBranchCategorysPartial = false;
            }
        }

        return $this->collSelBranchCategorys;
    }

    /**
     * Sets a collection of SelBranchCategory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selBranchCategorys A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelBranch The current object (for fluent API support)
     */
    public function setSelBranchCategorys(PropelCollection $selBranchCategorys, PropelPDO $con = null)
    {
        $selBranchCategorysToDelete = $this->getSelBranchCategorys(new Criteria(), $con)->diff($selBranchCategorys);

        $this->selBranchCategorysScheduledForDeletion = unserialize(serialize($selBranchCategorysToDelete));

        foreach ($selBranchCategorysToDelete as $selBranchCategoryRemoved) {
            $selBranchCategoryRemoved->setSelBranch(null);
        }

        $this->collSelBranchCategorys = null;
        foreach ($selBranchCategorys as $selBranchCategory) {
            $this->addSelBranchCategory($selBranchCategory);
        }

        $this->collSelBranchCategorys = $selBranchCategorys;
        $this->collSelBranchCategorysPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelBranchCategory objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelBranchCategory objects.
     * @throws PropelException
     */
    public function countSelBranchCategorys(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelBranchCategorysPartial && !$this->isNew();
        if (null === $this->collSelBranchCategorys || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelBranchCategorys) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelBranchCategorys());
            }
            $query = SelBranchCategoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelBranch($this)
                ->count($con);
        }

        return count($this->collSelBranchCategorys);
    }

    /**
     * Method called to associate a SelBranchCategory object to this object
     * through the SelBranchCategory foreign key attribute.
     *
     * @param    SelBranchCategory $l SelBranchCategory
     * @return SelBranch The current object (for fluent API support)
     */
    public function addSelBranchCategory(SelBranchCategory $l)
    {
        if ($this->collSelBranchCategorys === null) {
            $this->initSelBranchCategorys();
            $this->collSelBranchCategorysPartial = true;
        }
        if (!in_array($l, $this->collSelBranchCategorys->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelBranchCategory($l);
        }

        return $this;
    }

    /**
     * @param	SelBranchCategory $selBranchCategory The selBranchCategory object to add.
     */
    protected function doAddSelBranchCategory($selBranchCategory)
    {
        $this->collSelBranchCategorys[]= $selBranchCategory;
        $selBranchCategory->setSelBranch($this);
    }

    /**
     * @param	SelBranchCategory $selBranchCategory The selBranchCategory object to remove.
     * @return SelBranch The current object (for fluent API support)
     */
    public function removeSelBranchCategory($selBranchCategory)
    {
        if ($this->getSelBranchCategorys()->contains($selBranchCategory)) {
            $this->collSelBranchCategorys->remove($this->collSelBranchCategorys->search($selBranchCategory));
            if (null === $this->selBranchCategorysScheduledForDeletion) {
                $this->selBranchCategorysScheduledForDeletion = clone $this->collSelBranchCategorys;
                $this->selBranchCategorysScheduledForDeletion->clear();
            }
            $this->selBranchCategorysScheduledForDeletion[]= clone $selBranchCategory;
            $selBranchCategory->setSelBranch(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelBranch is new, it will return
     * an empty collection; or if this SelBranch has previously
     * been saved, it will retrieve related SelBranchCategorys from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelBranch.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelBranchCategory[] List of SelBranchCategory objects
     */
    public function getSelBranchCategorysJoinSelCategory($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelBranchCategoryQuery::create(null, $criteria);
        $query->joinWith('SelCategory', $join_behavior);

        return $this->getSelBranchCategorys($query, $con);
    }

    /**
     * Clears out the collSelBranchEmployees collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelBranch The current object (for fluent API support)
     * @see        addSelBranchEmployees()
     */
    public function clearSelBranchEmployees()
    {
        $this->collSelBranchEmployees = null; // important to set this to null since that means it is uninitialized
        $this->collSelBranchEmployeesPartial = null;

        return $this;
    }

    /**
     * reset is the collSelBranchEmployees collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelBranchEmployees($v = true)
    {
        $this->collSelBranchEmployeesPartial = $v;
    }

    /**
     * Initializes the collSelBranchEmployees collection.
     *
     * By default this just sets the collSelBranchEmployees collection to an empty array (like clearcollSelBranchEmployees());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelBranchEmployees($overrideExisting = true)
    {
        if (null !== $this->collSelBranchEmployees && !$overrideExisting) {
            return;
        }
        $this->collSelBranchEmployees = new PropelObjectCollection();
        $this->collSelBranchEmployees->setModel('SelBranchEmployee');
    }

    /**
     * Gets an array of SelBranchEmployee objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelBranch is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelBranchEmployee[] List of SelBranchEmployee objects
     * @throws PropelException
     */
    public function getSelBranchEmployees($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelBranchEmployeesPartial && !$this->isNew();
        if (null === $this->collSelBranchEmployees || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelBranchEmployees) {
                // return empty collection
                $this->initSelBranchEmployees();
            } else {
                $collSelBranchEmployees = SelBranchEmployeeQuery::create(null, $criteria)
                    ->filterBySelBranch($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelBranchEmployeesPartial && count($collSelBranchEmployees)) {
                      $this->initSelBranchEmployees(false);

                      foreach($collSelBranchEmployees as $obj) {
                        if (false == $this->collSelBranchEmployees->contains($obj)) {
                          $this->collSelBranchEmployees->append($obj);
                        }
                      }

                      $this->collSelBranchEmployeesPartial = true;
                    }

                    return $collSelBranchEmployees;
                }

                if($partial && $this->collSelBranchEmployees) {
                    foreach($this->collSelBranchEmployees as $obj) {
                        if($obj->isNew()) {
                            $collSelBranchEmployees[] = $obj;
                        }
                    }
                }

                $this->collSelBranchEmployees = $collSelBranchEmployees;
                $this->collSelBranchEmployeesPartial = false;
            }
        }

        return $this->collSelBranchEmployees;
    }

    /**
     * Sets a collection of SelBranchEmployee objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selBranchEmployees A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelBranch The current object (for fluent API support)
     */
    public function setSelBranchEmployees(PropelCollection $selBranchEmployees, PropelPDO $con = null)
    {
        $selBranchEmployeesToDelete = $this->getSelBranchEmployees(new Criteria(), $con)->diff($selBranchEmployees);

        $this->selBranchEmployeesScheduledForDeletion = unserialize(serialize($selBranchEmployeesToDelete));

        foreach ($selBranchEmployeesToDelete as $selBranchEmployeeRemoved) {
            $selBranchEmployeeRemoved->setSelBranch(null);
        }

        $this->collSelBranchEmployees = null;
        foreach ($selBranchEmployees as $selBranchEmployee) {
            $this->addSelBranchEmployee($selBranchEmployee);
        }

        $this->collSelBranchEmployees = $selBranchEmployees;
        $this->collSelBranchEmployeesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelBranchEmployee objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelBranchEmployee objects.
     * @throws PropelException
     */
    public function countSelBranchEmployees(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelBranchEmployeesPartial && !$this->isNew();
        if (null === $this->collSelBranchEmployees || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelBranchEmployees) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelBranchEmployees());
            }
            $query = SelBranchEmployeeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelBranch($this)
                ->count($con);
        }

        return count($this->collSelBranchEmployees);
    }

    /**
     * Method called to associate a SelBranchEmployee object to this object
     * through the SelBranchEmployee foreign key attribute.
     *
     * @param    SelBranchEmployee $l SelBranchEmployee
     * @return SelBranch The current object (for fluent API support)
     */
    public function addSelBranchEmployee(SelBranchEmployee $l)
    {
        if ($this->collSelBranchEmployees === null) {
            $this->initSelBranchEmployees();
            $this->collSelBranchEmployeesPartial = true;
        }
        if (!in_array($l, $this->collSelBranchEmployees->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelBranchEmployee($l);
        }

        return $this;
    }

    /**
     * @param	SelBranchEmployee $selBranchEmployee The selBranchEmployee object to add.
     */
    protected function doAddSelBranchEmployee($selBranchEmployee)
    {
        $this->collSelBranchEmployees[]= $selBranchEmployee;
        $selBranchEmployee->setSelBranch($this);
    }

    /**
     * @param	SelBranchEmployee $selBranchEmployee The selBranchEmployee object to remove.
     * @return SelBranch The current object (for fluent API support)
     */
    public function removeSelBranchEmployee($selBranchEmployee)
    {
        if ($this->getSelBranchEmployees()->contains($selBranchEmployee)) {
            $this->collSelBranchEmployees->remove($this->collSelBranchEmployees->search($selBranchEmployee));
            if (null === $this->selBranchEmployeesScheduledForDeletion) {
                $this->selBranchEmployeesScheduledForDeletion = clone $this->collSelBranchEmployees;
                $this->selBranchEmployeesScheduledForDeletion->clear();
            }
            $this->selBranchEmployeesScheduledForDeletion[]= clone $selBranchEmployee;
            $selBranchEmployee->setSelBranch(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelBranch is new, it will return
     * an empty collection; or if this SelBranch has previously
     * been saved, it will retrieve related SelBranchEmployees from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelBranch.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelBranchEmployee[] List of SelBranchEmployee objects
     */
    public function getSelBranchEmployeesJoinSelEmployee($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelBranchEmployeeQuery::create(null, $criteria);
        $query->joinWith('SelEmployee', $join_behavior);

        return $this->getSelBranchEmployees($query, $con);
    }

    /**
     * Clears out the collSelProductItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelBranch The current object (for fluent API support)
     * @see        addSelProductItems()
     */
    public function clearSelProductItems()
    {
        $this->collSelProductItems = null; // important to set this to null since that means it is uninitialized
        $this->collSelProductItemsPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProductItems collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProductItems($v = true)
    {
        $this->collSelProductItemsPartial = $v;
    }

    /**
     * Initializes the collSelProductItems collection.
     *
     * By default this just sets the collSelProductItems collection to an empty array (like clearcollSelProductItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProductItems($overrideExisting = true)
    {
        if (null !== $this->collSelProductItems && !$overrideExisting) {
            return;
        }
        $this->collSelProductItems = new PropelObjectCollection();
        $this->collSelProductItems->setModel('SelProductItem');
    }

    /**
     * Gets an array of SelProductItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelBranch is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProductItem[] List of SelProductItem objects
     * @throws PropelException
     */
    public function getSelProductItems($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProductItemsPartial && !$this->isNew();
        if (null === $this->collSelProductItems || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProductItems) {
                // return empty collection
                $this->initSelProductItems();
            } else {
                $collSelProductItems = SelProductItemQuery::create(null, $criteria)
                    ->filterBySelBranch($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProductItemsPartial && count($collSelProductItems)) {
                      $this->initSelProductItems(false);

                      foreach($collSelProductItems as $obj) {
                        if (false == $this->collSelProductItems->contains($obj)) {
                          $this->collSelProductItems->append($obj);
                        }
                      }

                      $this->collSelProductItemsPartial = true;
                    }

                    return $collSelProductItems;
                }

                if($partial && $this->collSelProductItems) {
                    foreach($this->collSelProductItems as $obj) {
                        if($obj->isNew()) {
                            $collSelProductItems[] = $obj;
                        }
                    }
                }

                $this->collSelProductItems = $collSelProductItems;
                $this->collSelProductItemsPartial = false;
            }
        }

        return $this->collSelProductItems;
    }

    /**
     * Sets a collection of SelProductItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProductItems A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelBranch The current object (for fluent API support)
     */
    public function setSelProductItems(PropelCollection $selProductItems, PropelPDO $con = null)
    {
        $selProductItemsToDelete = $this->getSelProductItems(new Criteria(), $con)->diff($selProductItems);

        $this->selProductItemsScheduledForDeletion = unserialize(serialize($selProductItemsToDelete));

        foreach ($selProductItemsToDelete as $selProductItemRemoved) {
            $selProductItemRemoved->setSelBranch(null);
        }

        $this->collSelProductItems = null;
        foreach ($selProductItems as $selProductItem) {
            $this->addSelProductItem($selProductItem);
        }

        $this->collSelProductItems = $selProductItems;
        $this->collSelProductItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProductItem objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProductItem objects.
     * @throws PropelException
     */
    public function countSelProductItems(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProductItemsPartial && !$this->isNew();
        if (null === $this->collSelProductItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProductItems) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProductItems());
            }
            $query = SelProductItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelBranch($this)
                ->count($con);
        }

        return count($this->collSelProductItems);
    }

    /**
     * Method called to associate a SelProductItem object to this object
     * through the SelProductItem foreign key attribute.
     *
     * @param    SelProductItem $l SelProductItem
     * @return SelBranch The current object (for fluent API support)
     */
    public function addSelProductItem(SelProductItem $l)
    {
        if ($this->collSelProductItems === null) {
            $this->initSelProductItems();
            $this->collSelProductItemsPartial = true;
        }
        if (!in_array($l, $this->collSelProductItems->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProductItem($l);
        }

        return $this;
    }

    /**
     * @param	SelProductItem $selProductItem The selProductItem object to add.
     */
    protected function doAddSelProductItem($selProductItem)
    {
        $this->collSelProductItems[]= $selProductItem;
        $selProductItem->setSelBranch($this);
    }

    /**
     * @param	SelProductItem $selProductItem The selProductItem object to remove.
     * @return SelBranch The current object (for fluent API support)
     */
    public function removeSelProductItem($selProductItem)
    {
        if ($this->getSelProductItems()->contains($selProductItem)) {
            $this->collSelProductItems->remove($this->collSelProductItems->search($selProductItem));
            if (null === $this->selProductItemsScheduledForDeletion) {
                $this->selProductItemsScheduledForDeletion = clone $this->collSelProductItems;
                $this->selProductItemsScheduledForDeletion->clear();
            }
            $this->selProductItemsScheduledForDeletion[]= $selProductItem;
            $selProductItem->setSelBranch(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelBranch is new, it will return
     * an empty collection; or if this SelBranch has previously
     * been saved, it will retrieve related SelProductItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelBranch.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProductItem[] List of SelProductItem objects
     */
    public function getSelProductItemsJoinSelProductAcquisition($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductItemQuery::create(null, $criteria);
        $query->joinWith('SelProductAcquisition', $join_behavior);

        return $this->getSelProductItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelBranch is new, it will return
     * an empty collection; or if this SelBranch has previously
     * been saved, it will retrieve related SelProductItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelBranch.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProductItem[] List of SelProductItem objects
     */
    public function getSelProductItemsJoinSelBrand($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductItemQuery::create(null, $criteria);
        $query->joinWith('SelBrand', $join_behavior);

        return $this->getSelProductItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelBranch is new, it will return
     * an empty collection; or if this SelBranch has previously
     * been saved, it will retrieve related SelProductItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelBranch.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProductItem[] List of SelProductItem objects
     */
    public function getSelProductItemsJoinSelProductSell($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductItemQuery::create(null, $criteria);
        $query->joinWith('SelProductSell', $join_behavior);

        return $this->getSelProductItems($query, $con);
    }

    /**
     * Clears out the collSelSales collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelBranch The current object (for fluent API support)
     * @see        addSelSales()
     */
    public function clearSelSales()
    {
        $this->collSelSales = null; // important to set this to null since that means it is uninitialized
        $this->collSelSalesPartial = null;

        return $this;
    }

    /**
     * reset is the collSelSales collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelSales($v = true)
    {
        $this->collSelSalesPartial = $v;
    }

    /**
     * Initializes the collSelSales collection.
     *
     * By default this just sets the collSelSales collection to an empty array (like clearcollSelSales());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelSales($overrideExisting = true)
    {
        if (null !== $this->collSelSales && !$overrideExisting) {
            return;
        }
        $this->collSelSales = new PropelObjectCollection();
        $this->collSelSales->setModel('SelSale');
    }

    /**
     * Gets an array of SelSale objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelBranch is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelSale[] List of SelSale objects
     * @throws PropelException
     */
    public function getSelSales($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelSalesPartial && !$this->isNew();
        if (null === $this->collSelSales || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelSales) {
                // return empty collection
                $this->initSelSales();
            } else {
                $collSelSales = SelSaleQuery::create(null, $criteria)
                    ->filterBySelBranch($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelSalesPartial && count($collSelSales)) {
                      $this->initSelSales(false);

                      foreach($collSelSales as $obj) {
                        if (false == $this->collSelSales->contains($obj)) {
                          $this->collSelSales->append($obj);
                        }
                      }

                      $this->collSelSalesPartial = true;
                    }

                    return $collSelSales;
                }

                if($partial && $this->collSelSales) {
                    foreach($this->collSelSales as $obj) {
                        if($obj->isNew()) {
                            $collSelSales[] = $obj;
                        }
                    }
                }

                $this->collSelSales = $collSelSales;
                $this->collSelSalesPartial = false;
            }
        }

        return $this->collSelSales;
    }

    /**
     * Sets a collection of SelSale objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selSales A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelBranch The current object (for fluent API support)
     */
    public function setSelSales(PropelCollection $selSales, PropelPDO $con = null)
    {
        $selSalesToDelete = $this->getSelSales(new Criteria(), $con)->diff($selSales);

        $this->selSalesScheduledForDeletion = unserialize(serialize($selSalesToDelete));

        foreach ($selSalesToDelete as $selSaleRemoved) {
            $selSaleRemoved->setSelBranch(null);
        }

        $this->collSelSales = null;
        foreach ($selSales as $selSale) {
            $this->addSelSale($selSale);
        }

        $this->collSelSales = $selSales;
        $this->collSelSalesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelSale objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelSale objects.
     * @throws PropelException
     */
    public function countSelSales(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelSalesPartial && !$this->isNew();
        if (null === $this->collSelSales || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelSales) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelSales());
            }
            $query = SelSaleQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelBranch($this)
                ->count($con);
        }

        return count($this->collSelSales);
    }

    /**
     * Method called to associate a SelSale object to this object
     * through the SelSale foreign key attribute.
     *
     * @param    SelSale $l SelSale
     * @return SelBranch The current object (for fluent API support)
     */
    public function addSelSale(SelSale $l)
    {
        if ($this->collSelSales === null) {
            $this->initSelSales();
            $this->collSelSalesPartial = true;
        }
        if (!in_array($l, $this->collSelSales->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelSale($l);
        }

        return $this;
    }

    /**
     * @param	SelSale $selSale The selSale object to add.
     */
    protected function doAddSelSale($selSale)
    {
        $this->collSelSales[]= $selSale;
        $selSale->setSelBranch($this);
    }

    /**
     * @param	SelSale $selSale The selSale object to remove.
     * @return SelBranch The current object (for fluent API support)
     */
    public function removeSelSale($selSale)
    {
        if ($this->getSelSales()->contains($selSale)) {
            $this->collSelSales->remove($this->collSelSales->search($selSale));
            if (null === $this->selSalesScheduledForDeletion) {
                $this->selSalesScheduledForDeletion = clone $this->collSelSales;
                $this->selSalesScheduledForDeletion->clear();
            }
            $this->selSalesScheduledForDeletion[]= clone $selSale;
            $selSale->setSelBranch(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelBranch is new, it will return
     * an empty collection; or if this SelBranch has previously
     * been saved, it will retrieve related SelSales from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelBranch.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelSale[] List of SelSale objects
     */
    public function getSelSalesJoinSelCustomer($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelSaleQuery::create(null, $criteria);
        $query->joinWith('SelCustomer', $join_behavior);

        return $this->getSelSales($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelBranch is new, it will return
     * an empty collection; or if this SelBranch has previously
     * been saved, it will retrieve related SelSales from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelBranch.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelSale[] List of SelSale objects
     */
    public function getSelSalesJoinSelEmployee($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelSaleQuery::create(null, $criteria);
        $query->joinWith('SelEmployee', $join_behavior);

        return $this->getSelSales($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->number = null;
        $this->status = null;
        $this->code = null;
        $this->name = null;
        $this->address = null;
        $this->phone = null;
        $this->fax = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSelBranchCategorys) {
                foreach ($this->collSelBranchCategorys as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelBranchEmployees) {
                foreach ($this->collSelBranchEmployees as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelProductItems) {
                foreach ($this->collSelProductItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelSales) {
                foreach ($this->collSelSales as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collSelBranchCategorys instanceof PropelCollection) {
            $this->collSelBranchCategorys->clearIterator();
        }
        $this->collSelBranchCategorys = null;
        if ($this->collSelBranchEmployees instanceof PropelCollection) {
            $this->collSelBranchEmployees->clearIterator();
        }
        $this->collSelBranchEmployees = null;
        if ($this->collSelProductItems instanceof PropelCollection) {
            $this->collSelProductItems->clearIterator();
        }
        $this->collSelProductItems = null;
        if ($this->collSelSales instanceof PropelCollection) {
            $this->collSelSales->clearIterator();
        }
        $this->collSelSales = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelBranchPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
