<?php


/**
 * Base class that represents a row from the 'sel_category' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelCategory extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelCategoryPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelCategoryPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the main_category_id field.
     * @var        int
     */
    protected $main_category_id;

    /**
     * The value for the code field.
     * @var        string
     */
    protected $code;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * @var        SelCategory
     */
    protected $aSelCategoryRelatedByMainCategoryId;

    /**
     * @var        PropelObjectCollection|SelBranchCategory[] Collection to store aggregation of SelBranchCategory objects.
     */
    protected $collSelBranchCategorys;
    protected $collSelBranchCategorysPartial;

    /**
     * @var        PropelObjectCollection|SelCategory[] Collection to store aggregation of SelCategory objects.
     */
    protected $collSelCategorysRelatedById;
    protected $collSelCategorysRelatedByIdPartial;

    /**
     * @var        PropelObjectCollection|SelProduct[] Collection to store aggregation of SelProduct objects.
     */
    protected $collSelProducts;
    protected $collSelProductsPartial;

    /**
     * @var        PropelObjectCollection|SelProductGroup[] Collection to store aggregation of SelProductGroup objects.
     */
    protected $collSelProductGroups;
    protected $collSelProductGroupsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selBranchCategorysScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selCategorysRelatedByIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductGroupsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [main_category_id] column value.
     * 
     * @return int
     */
    public function getMainCategoryId()
    {
        return $this->main_category_id;
    }

    /**
     * Get the [code] column value.
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [name] column value.
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [description] column value.
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelCategory The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelCategoryPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [main_category_id] column.
     * 
     * @param int $v new value
     * @return SelCategory The current object (for fluent API support)
     */
    public function setMainCategoryId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->main_category_id !== $v) {
            $this->main_category_id = $v;
            $this->modifiedColumns[] = SelCategoryPeer::MAIN_CATEGORY_ID;
        }

        if ($this->aSelCategoryRelatedByMainCategoryId !== null && $this->aSelCategoryRelatedByMainCategoryId->getId() !== $v) {
            $this->aSelCategoryRelatedByMainCategoryId = null;
        }


        return $this;
    } // setMainCategoryId()

    /**
     * Set the value of [code] column.
     * 
     * @param string $v new value
     * @return SelCategory The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[] = SelCategoryPeer::CODE;
        }


        return $this;
    } // setCode()

    /**
     * Set the value of [name] column.
     * 
     * @param string $v new value
     * @return SelCategory The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = SelCategoryPeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [description] column.
     * 
     * @param string $v new value
     * @return SelCategory The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = SelCategoryPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->main_category_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->code = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->name = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->description = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 5; // 5 = SelCategoryPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelCategory object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSelCategoryRelatedByMainCategoryId !== null && $this->main_category_id !== $this->aSelCategoryRelatedByMainCategoryId->getId()) {
            $this->aSelCategoryRelatedByMainCategoryId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelCategoryPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelCategoryPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSelCategoryRelatedByMainCategoryId = null;
            $this->collSelBranchCategorys = null;

            $this->collSelCategorysRelatedById = null;

            $this->collSelProducts = null;

            $this->collSelProductGroups = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelCategoryPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelCategoryQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelCategoryPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelCategoryPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelCategoryRelatedByMainCategoryId !== null) {
                if ($this->aSelCategoryRelatedByMainCategoryId->isModified() || $this->aSelCategoryRelatedByMainCategoryId->isNew()) {
                    $affectedRows += $this->aSelCategoryRelatedByMainCategoryId->save($con);
                }
                $this->setSelCategoryRelatedByMainCategoryId($this->aSelCategoryRelatedByMainCategoryId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->selBranchCategorysScheduledForDeletion !== null) {
                if (!$this->selBranchCategorysScheduledForDeletion->isEmpty()) {
                    SelBranchCategoryQuery::create()
                        ->filterByPrimaryKeys($this->selBranchCategorysScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selBranchCategorysScheduledForDeletion = null;
                }
            }

            if ($this->collSelBranchCategorys !== null) {
                foreach ($this->collSelBranchCategorys as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selCategorysRelatedByIdScheduledForDeletion !== null) {
                if (!$this->selCategorysRelatedByIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->selCategorysRelatedByIdScheduledForDeletion as $selCategoryRelatedById) {
                        // need to save related object because we set the relation to null
                        $selCategoryRelatedById->save($con);
                    }
                    $this->selCategorysRelatedByIdScheduledForDeletion = null;
                }
            }

            if ($this->collSelCategorysRelatedById !== null) {
                foreach ($this->collSelCategorysRelatedById as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selProductsScheduledForDeletion !== null) {
                if (!$this->selProductsScheduledForDeletion->isEmpty()) {
                    SelProductQuery::create()
                        ->filterByPrimaryKeys($this->selProductsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selProductsScheduledForDeletion = null;
                }
            }

            if ($this->collSelProducts !== null) {
                foreach ($this->collSelProducts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selProductGroupsScheduledForDeletion !== null) {
                if (!$this->selProductGroupsScheduledForDeletion->isEmpty()) {
                    SelProductGroupQuery::create()
                        ->filterByPrimaryKeys($this->selProductGroupsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selProductGroupsScheduledForDeletion = null;
                }
            }

            if ($this->collSelProductGroups !== null) {
                foreach ($this->collSelProductGroups as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SelCategoryPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SelCategoryPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelCategoryPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelCategoryPeer::MAIN_CATEGORY_ID)) {
            $modifiedColumns[':p' . $index++]  = '`MAIN_CATEGORY_ID`';
        }
        if ($this->isColumnModified(SelCategoryPeer::CODE)) {
            $modifiedColumns[':p' . $index++]  = '`CODE`';
        }
        if ($this->isColumnModified(SelCategoryPeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`NAME`';
        }
        if ($this->isColumnModified(SelCategoryPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`DESCRIPTION`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_category` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`MAIN_CATEGORY_ID`':						
                        $stmt->bindValue($identifier, $this->main_category_id, PDO::PARAM_INT);
                        break;
                    case '`CODE`':						
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case '`NAME`':						
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`DESCRIPTION`':						
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelCategoryRelatedByMainCategoryId !== null) {
                if (!$this->aSelCategoryRelatedByMainCategoryId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelCategoryRelatedByMainCategoryId->getValidationFailures());
                }
            }

            if (($retval = SelCategoryPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelBranchCategorys !== null) {
                    foreach ($this->collSelBranchCategorys as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelCategorysRelatedById !== null) {
                    foreach ($this->collSelCategorysRelatedById as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelProducts !== null) {
                    foreach ($this->collSelProducts as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelProductGroups !== null) {
                    foreach ($this->collSelProductGroups as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelCategoryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getMainCategoryId();
                break;
            case 2:
                return $this->getCode();
                break;
            case 3:
                return $this->getName();
                break;
            case 4:
                return $this->getDescription();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelCategory'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelCategory'][$this->getPrimaryKey()] = true;
        $keys = SelCategoryPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getMainCategoryId(),
            $keys[2] => $this->getCode(),
            $keys[3] => $this->getName(),
            $keys[4] => $this->getDescription(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSelCategoryRelatedByMainCategoryId) {
                $result['SelCategoryRelatedByMainCategoryId'] = $this->aSelCategoryRelatedByMainCategoryId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSelBranchCategorys) {
                $result['SelBranchCategorys'] = $this->collSelBranchCategorys->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelCategorysRelatedById) {
                $result['SelCategorysRelatedById'] = $this->collSelCategorysRelatedById->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelProducts) {
                $result['SelProducts'] = $this->collSelProducts->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelProductGroups) {
                $result['SelProductGroups'] = $this->collSelProductGroups->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelCategoryPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setMainCategoryId($value);
                break;
            case 2:
                $this->setCode($value);
                break;
            case 3:
                $this->setName($value);
                break;
            case 4:
                $this->setDescription($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelCategoryPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setMainCategoryId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCode($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setName($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDescription($arr[$keys[4]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelCategoryPeer::DATABASE_NAME);

        if ($this->isColumnModified(SelCategoryPeer::ID)) $criteria->add(SelCategoryPeer::ID, $this->id);
        if ($this->isColumnModified(SelCategoryPeer::MAIN_CATEGORY_ID)) $criteria->add(SelCategoryPeer::MAIN_CATEGORY_ID, $this->main_category_id);
        if ($this->isColumnModified(SelCategoryPeer::CODE)) $criteria->add(SelCategoryPeer::CODE, $this->code);
        if ($this->isColumnModified(SelCategoryPeer::NAME)) $criteria->add(SelCategoryPeer::NAME, $this->name);
        if ($this->isColumnModified(SelCategoryPeer::DESCRIPTION)) $criteria->add(SelCategoryPeer::DESCRIPTION, $this->description);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelCategoryPeer::DATABASE_NAME);
        $criteria->add(SelCategoryPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelCategory (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setMainCategoryId($this->getMainCategoryId());
        $copyObj->setCode($this->getCode());
        $copyObj->setName($this->getName());
        $copyObj->setDescription($this->getDescription());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelBranchCategorys() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelBranchCategory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelCategorysRelatedById() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelCategoryRelatedById($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelProducts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProduct($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelProductGroups() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProductGroup($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelCategory Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelCategoryPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelCategoryPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SelCategory object.
     *
     * @param             SelCategory $v
     * @return SelCategory The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelCategoryRelatedByMainCategoryId(SelCategory $v = null)
    {
        if ($v === null) {
            $this->setMainCategoryId(NULL);
        } else {
            $this->setMainCategoryId($v->getId());
        }

        $this->aSelCategoryRelatedByMainCategoryId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelCategory object, it will not be re-added.
        if ($v !== null) {
            $v->addSelCategoryRelatedById($this);
        }


        return $this;
    }


    /**
     * Get the associated SelCategory object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelCategory The associated SelCategory object.
     * @throws PropelException
     */
    public function getSelCategoryRelatedByMainCategoryId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelCategoryRelatedByMainCategoryId === null && ($this->main_category_id !== null) && $doQuery) {
            $this->aSelCategoryRelatedByMainCategoryId = SelCategoryQuery::create()->findPk($this->main_category_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelCategoryRelatedByMainCategoryId->addSelCategorysRelatedById($this);
             */
        }

        return $this->aSelCategoryRelatedByMainCategoryId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelBranchCategory' == $relationName) {
            $this->initSelBranchCategorys();
        }
        if ('SelCategoryRelatedById' == $relationName) {
            $this->initSelCategorysRelatedById();
        }
        if ('SelProduct' == $relationName) {
            $this->initSelProducts();
        }
        if ('SelProductGroup' == $relationName) {
            $this->initSelProductGroups();
        }
    }

    /**
     * Clears out the collSelBranchCategorys collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelCategory The current object (for fluent API support)
     * @see        addSelBranchCategorys()
     */
    public function clearSelBranchCategorys()
    {
        $this->collSelBranchCategorys = null; // important to set this to null since that means it is uninitialized
        $this->collSelBranchCategorysPartial = null;

        return $this;
    }

    /**
     * reset is the collSelBranchCategorys collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelBranchCategorys($v = true)
    {
        $this->collSelBranchCategorysPartial = $v;
    }

    /**
     * Initializes the collSelBranchCategorys collection.
     *
     * By default this just sets the collSelBranchCategorys collection to an empty array (like clearcollSelBranchCategorys());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelBranchCategorys($overrideExisting = true)
    {
        if (null !== $this->collSelBranchCategorys && !$overrideExisting) {
            return;
        }
        $this->collSelBranchCategorys = new PropelObjectCollection();
        $this->collSelBranchCategorys->setModel('SelBranchCategory');
    }

    /**
     * Gets an array of SelBranchCategory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelCategory is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelBranchCategory[] List of SelBranchCategory objects
     * @throws PropelException
     */
    public function getSelBranchCategorys($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelBranchCategorysPartial && !$this->isNew();
        if (null === $this->collSelBranchCategorys || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelBranchCategorys) {
                // return empty collection
                $this->initSelBranchCategorys();
            } else {
                $collSelBranchCategorys = SelBranchCategoryQuery::create(null, $criteria)
                    ->filterBySelCategory($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelBranchCategorysPartial && count($collSelBranchCategorys)) {
                      $this->initSelBranchCategorys(false);

                      foreach($collSelBranchCategorys as $obj) {
                        if (false == $this->collSelBranchCategorys->contains($obj)) {
                          $this->collSelBranchCategorys->append($obj);
                        }
                      }

                      $this->collSelBranchCategorysPartial = true;
                    }

                    return $collSelBranchCategorys;
                }

                if($partial && $this->collSelBranchCategorys) {
                    foreach($this->collSelBranchCategorys as $obj) {
                        if($obj->isNew()) {
                            $collSelBranchCategorys[] = $obj;
                        }
                    }
                }

                $this->collSelBranchCategorys = $collSelBranchCategorys;
                $this->collSelBranchCategorysPartial = false;
            }
        }

        return $this->collSelBranchCategorys;
    }

    /**
     * Sets a collection of SelBranchCategory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selBranchCategorys A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelCategory The current object (for fluent API support)
     */
    public function setSelBranchCategorys(PropelCollection $selBranchCategorys, PropelPDO $con = null)
    {
        $selBranchCategorysToDelete = $this->getSelBranchCategorys(new Criteria(), $con)->diff($selBranchCategorys);

        $this->selBranchCategorysScheduledForDeletion = unserialize(serialize($selBranchCategorysToDelete));

        foreach ($selBranchCategorysToDelete as $selBranchCategoryRemoved) {
            $selBranchCategoryRemoved->setSelCategory(null);
        }

        $this->collSelBranchCategorys = null;
        foreach ($selBranchCategorys as $selBranchCategory) {
            $this->addSelBranchCategory($selBranchCategory);
        }

        $this->collSelBranchCategorys = $selBranchCategorys;
        $this->collSelBranchCategorysPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelBranchCategory objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelBranchCategory objects.
     * @throws PropelException
     */
    public function countSelBranchCategorys(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelBranchCategorysPartial && !$this->isNew();
        if (null === $this->collSelBranchCategorys || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelBranchCategorys) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelBranchCategorys());
            }
            $query = SelBranchCategoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelCategory($this)
                ->count($con);
        }

        return count($this->collSelBranchCategorys);
    }

    /**
     * Method called to associate a SelBranchCategory object to this object
     * through the SelBranchCategory foreign key attribute.
     *
     * @param    SelBranchCategory $l SelBranchCategory
     * @return SelCategory The current object (for fluent API support)
     */
    public function addSelBranchCategory(SelBranchCategory $l)
    {
        if ($this->collSelBranchCategorys === null) {
            $this->initSelBranchCategorys();
            $this->collSelBranchCategorysPartial = true;
        }
        if (!in_array($l, $this->collSelBranchCategorys->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelBranchCategory($l);
        }

        return $this;
    }

    /**
     * @param	SelBranchCategory $selBranchCategory The selBranchCategory object to add.
     */
    protected function doAddSelBranchCategory($selBranchCategory)
    {
        $this->collSelBranchCategorys[]= $selBranchCategory;
        $selBranchCategory->setSelCategory($this);
    }

    /**
     * @param	SelBranchCategory $selBranchCategory The selBranchCategory object to remove.
     * @return SelCategory The current object (for fluent API support)
     */
    public function removeSelBranchCategory($selBranchCategory)
    {
        if ($this->getSelBranchCategorys()->contains($selBranchCategory)) {
            $this->collSelBranchCategorys->remove($this->collSelBranchCategorys->search($selBranchCategory));
            if (null === $this->selBranchCategorysScheduledForDeletion) {
                $this->selBranchCategorysScheduledForDeletion = clone $this->collSelBranchCategorys;
                $this->selBranchCategorysScheduledForDeletion->clear();
            }
            $this->selBranchCategorysScheduledForDeletion[]= clone $selBranchCategory;
            $selBranchCategory->setSelCategory(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelCategory is new, it will return
     * an empty collection; or if this SelCategory has previously
     * been saved, it will retrieve related SelBranchCategorys from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelCategory.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelBranchCategory[] List of SelBranchCategory objects
     */
    public function getSelBranchCategorysJoinSelBranch($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelBranchCategoryQuery::create(null, $criteria);
        $query->joinWith('SelBranch', $join_behavior);

        return $this->getSelBranchCategorys($query, $con);
    }

    /**
     * Clears out the collSelCategorysRelatedById collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelCategory The current object (for fluent API support)
     * @see        addSelCategorysRelatedById()
     */
    public function clearSelCategorysRelatedById()
    {
        $this->collSelCategorysRelatedById = null; // important to set this to null since that means it is uninitialized
        $this->collSelCategorysRelatedByIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSelCategorysRelatedById collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelCategorysRelatedById($v = true)
    {
        $this->collSelCategorysRelatedByIdPartial = $v;
    }

    /**
     * Initializes the collSelCategorysRelatedById collection.
     *
     * By default this just sets the collSelCategorysRelatedById collection to an empty array (like clearcollSelCategorysRelatedById());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelCategorysRelatedById($overrideExisting = true)
    {
        if (null !== $this->collSelCategorysRelatedById && !$overrideExisting) {
            return;
        }
        $this->collSelCategorysRelatedById = new PropelObjectCollection();
        $this->collSelCategorysRelatedById->setModel('SelCategory');
    }

    /**
     * Gets an array of SelCategory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelCategory is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelCategory[] List of SelCategory objects
     * @throws PropelException
     */
    public function getSelCategorysRelatedById($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelCategorysRelatedByIdPartial && !$this->isNew();
        if (null === $this->collSelCategorysRelatedById || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelCategorysRelatedById) {
                // return empty collection
                $this->initSelCategorysRelatedById();
            } else {
                $collSelCategorysRelatedById = SelCategoryQuery::create(null, $criteria)
                    ->filterBySelCategoryRelatedByMainCategoryId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelCategorysRelatedByIdPartial && count($collSelCategorysRelatedById)) {
                      $this->initSelCategorysRelatedById(false);

                      foreach($collSelCategorysRelatedById as $obj) {
                        if (false == $this->collSelCategorysRelatedById->contains($obj)) {
                          $this->collSelCategorysRelatedById->append($obj);
                        }
                      }

                      $this->collSelCategorysRelatedByIdPartial = true;
                    }

                    return $collSelCategorysRelatedById;
                }

                if($partial && $this->collSelCategorysRelatedById) {
                    foreach($this->collSelCategorysRelatedById as $obj) {
                        if($obj->isNew()) {
                            $collSelCategorysRelatedById[] = $obj;
                        }
                    }
                }

                $this->collSelCategorysRelatedById = $collSelCategorysRelatedById;
                $this->collSelCategorysRelatedByIdPartial = false;
            }
        }

        return $this->collSelCategorysRelatedById;
    }

    /**
     * Sets a collection of SelCategoryRelatedById objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selCategorysRelatedById A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelCategory The current object (for fluent API support)
     */
    public function setSelCategorysRelatedById(PropelCollection $selCategorysRelatedById, PropelPDO $con = null)
    {
        $selCategorysRelatedByIdToDelete = $this->getSelCategorysRelatedById(new Criteria(), $con)->diff($selCategorysRelatedById);

        $this->selCategorysRelatedByIdScheduledForDeletion = unserialize(serialize($selCategorysRelatedByIdToDelete));

        foreach ($selCategorysRelatedByIdToDelete as $selCategoryRelatedByIdRemoved) {
            $selCategoryRelatedByIdRemoved->setSelCategoryRelatedByMainCategoryId(null);
        }

        $this->collSelCategorysRelatedById = null;
        foreach ($selCategorysRelatedById as $selCategoryRelatedById) {
            $this->addSelCategoryRelatedById($selCategoryRelatedById);
        }

        $this->collSelCategorysRelatedById = $selCategorysRelatedById;
        $this->collSelCategorysRelatedByIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelCategory objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelCategory objects.
     * @throws PropelException
     */
    public function countSelCategorysRelatedById(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelCategorysRelatedByIdPartial && !$this->isNew();
        if (null === $this->collSelCategorysRelatedById || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelCategorysRelatedById) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelCategorysRelatedById());
            }
            $query = SelCategoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelCategoryRelatedByMainCategoryId($this)
                ->count($con);
        }

        return count($this->collSelCategorysRelatedById);
    }

    /**
     * Method called to associate a SelCategory object to this object
     * through the SelCategory foreign key attribute.
     *
     * @param    SelCategory $l SelCategory
     * @return SelCategory The current object (for fluent API support)
     */
    public function addSelCategoryRelatedById(SelCategory $l)
    {
        if ($this->collSelCategorysRelatedById === null) {
            $this->initSelCategorysRelatedById();
            $this->collSelCategorysRelatedByIdPartial = true;
        }
        if (!in_array($l, $this->collSelCategorysRelatedById->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelCategoryRelatedById($l);
        }

        return $this;
    }

    /**
     * @param	SelCategoryRelatedById $selCategoryRelatedById The selCategoryRelatedById object to add.
     */
    protected function doAddSelCategoryRelatedById($selCategoryRelatedById)
    {
        $this->collSelCategorysRelatedById[]= $selCategoryRelatedById;
        $selCategoryRelatedById->setSelCategoryRelatedByMainCategoryId($this);
    }

    /**
     * @param	SelCategoryRelatedById $selCategoryRelatedById The selCategoryRelatedById object to remove.
     * @return SelCategory The current object (for fluent API support)
     */
    public function removeSelCategoryRelatedById($selCategoryRelatedById)
    {
        if ($this->getSelCategorysRelatedById()->contains($selCategoryRelatedById)) {
            $this->collSelCategorysRelatedById->remove($this->collSelCategorysRelatedById->search($selCategoryRelatedById));
            if (null === $this->selCategorysRelatedByIdScheduledForDeletion) {
                $this->selCategorysRelatedByIdScheduledForDeletion = clone $this->collSelCategorysRelatedById;
                $this->selCategorysRelatedByIdScheduledForDeletion->clear();
            }
            $this->selCategorysRelatedByIdScheduledForDeletion[]= $selCategoryRelatedById;
            $selCategoryRelatedById->setSelCategoryRelatedByMainCategoryId(null);
        }

        return $this;
    }

    /**
     * Clears out the collSelProducts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelCategory The current object (for fluent API support)
     * @see        addSelProducts()
     */
    public function clearSelProducts()
    {
        $this->collSelProducts = null; // important to set this to null since that means it is uninitialized
        $this->collSelProductsPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProducts collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProducts($v = true)
    {
        $this->collSelProductsPartial = $v;
    }

    /**
     * Initializes the collSelProducts collection.
     *
     * By default this just sets the collSelProducts collection to an empty array (like clearcollSelProducts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProducts($overrideExisting = true)
    {
        if (null !== $this->collSelProducts && !$overrideExisting) {
            return;
        }
        $this->collSelProducts = new PropelObjectCollection();
        $this->collSelProducts->setModel('SelProduct');
    }

    /**
     * Gets an array of SelProduct objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelCategory is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProduct[] List of SelProduct objects
     * @throws PropelException
     */
    public function getSelProducts($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProductsPartial && !$this->isNew();
        if (null === $this->collSelProducts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProducts) {
                // return empty collection
                $this->initSelProducts();
            } else {
                $collSelProducts = SelProductQuery::create(null, $criteria)
                    ->filterBySelCategory($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProductsPartial && count($collSelProducts)) {
                      $this->initSelProducts(false);

                      foreach($collSelProducts as $obj) {
                        if (false == $this->collSelProducts->contains($obj)) {
                          $this->collSelProducts->append($obj);
                        }
                      }

                      $this->collSelProductsPartial = true;
                    }

                    return $collSelProducts;
                }

                if($partial && $this->collSelProducts) {
                    foreach($this->collSelProducts as $obj) {
                        if($obj->isNew()) {
                            $collSelProducts[] = $obj;
                        }
                    }
                }

                $this->collSelProducts = $collSelProducts;
                $this->collSelProductsPartial = false;
            }
        }

        return $this->collSelProducts;
    }

    /**
     * Sets a collection of SelProduct objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProducts A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelCategory The current object (for fluent API support)
     */
    public function setSelProducts(PropelCollection $selProducts, PropelPDO $con = null)
    {
        $selProductsToDelete = $this->getSelProducts(new Criteria(), $con)->diff($selProducts);

        $this->selProductsScheduledForDeletion = unserialize(serialize($selProductsToDelete));

        foreach ($selProductsToDelete as $selProductRemoved) {
            $selProductRemoved->setSelCategory(null);
        }

        $this->collSelProducts = null;
        foreach ($selProducts as $selProduct) {
            $this->addSelProduct($selProduct);
        }

        $this->collSelProducts = $selProducts;
        $this->collSelProductsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProduct objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProduct objects.
     * @throws PropelException
     */
    public function countSelProducts(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProductsPartial && !$this->isNew();
        if (null === $this->collSelProducts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProducts) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProducts());
            }
            $query = SelProductQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelCategory($this)
                ->count($con);
        }

        return count($this->collSelProducts);
    }

    /**
     * Method called to associate a SelProduct object to this object
     * through the SelProduct foreign key attribute.
     *
     * @param    SelProduct $l SelProduct
     * @return SelCategory The current object (for fluent API support)
     */
    public function addSelProduct(SelProduct $l)
    {
        if ($this->collSelProducts === null) {
            $this->initSelProducts();
            $this->collSelProductsPartial = true;
        }
        if (!in_array($l, $this->collSelProducts->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProduct($l);
        }

        return $this;
    }

    /**
     * @param	SelProduct $selProduct The selProduct object to add.
     */
    protected function doAddSelProduct($selProduct)
    {
        $this->collSelProducts[]= $selProduct;
        $selProduct->setSelCategory($this);
    }

    /**
     * @param	SelProduct $selProduct The selProduct object to remove.
     * @return SelCategory The current object (for fluent API support)
     */
    public function removeSelProduct($selProduct)
    {
        if ($this->getSelProducts()->contains($selProduct)) {
            $this->collSelProducts->remove($this->collSelProducts->search($selProduct));
            if (null === $this->selProductsScheduledForDeletion) {
                $this->selProductsScheduledForDeletion = clone $this->collSelProducts;
                $this->selProductsScheduledForDeletion->clear();
            }
            $this->selProductsScheduledForDeletion[]= clone $selProduct;
            $selProduct->setSelCategory(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelCategory is new, it will return
     * an empty collection; or if this SelCategory has previously
     * been saved, it will retrieve related SelProducts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelCategory.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProduct[] List of SelProduct objects
     */
    public function getSelProductsJoinSelProductGroup($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductQuery::create(null, $criteria);
        $query->joinWith('SelProductGroup', $join_behavior);

        return $this->getSelProducts($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelCategory is new, it will return
     * an empty collection; or if this SelCategory has previously
     * been saved, it will retrieve related SelProducts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelCategory.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProduct[] List of SelProduct objects
     */
    public function getSelProductsJoinSelBrand($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductQuery::create(null, $criteria);
        $query->joinWith('SelBrand', $join_behavior);

        return $this->getSelProducts($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelCategory is new, it will return
     * an empty collection; or if this SelCategory has previously
     * been saved, it will retrieve related SelProducts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelCategory.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProduct[] List of SelProduct objects
     */
    public function getSelProductsJoinSelProductImageRelatedByMainImageId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductQuery::create(null, $criteria);
        $query->joinWith('SelProductImageRelatedByMainImageId', $join_behavior);

        return $this->getSelProducts($query, $con);
    }

    /**
     * Clears out the collSelProductGroups collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelCategory The current object (for fluent API support)
     * @see        addSelProductGroups()
     */
    public function clearSelProductGroups()
    {
        $this->collSelProductGroups = null; // important to set this to null since that means it is uninitialized
        $this->collSelProductGroupsPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProductGroups collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProductGroups($v = true)
    {
        $this->collSelProductGroupsPartial = $v;
    }

    /**
     * Initializes the collSelProductGroups collection.
     *
     * By default this just sets the collSelProductGroups collection to an empty array (like clearcollSelProductGroups());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProductGroups($overrideExisting = true)
    {
        if (null !== $this->collSelProductGroups && !$overrideExisting) {
            return;
        }
        $this->collSelProductGroups = new PropelObjectCollection();
        $this->collSelProductGroups->setModel('SelProductGroup');
    }

    /**
     * Gets an array of SelProductGroup objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelCategory is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProductGroup[] List of SelProductGroup objects
     * @throws PropelException
     */
    public function getSelProductGroups($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProductGroupsPartial && !$this->isNew();
        if (null === $this->collSelProductGroups || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProductGroups) {
                // return empty collection
                $this->initSelProductGroups();
            } else {
                $collSelProductGroups = SelProductGroupQuery::create(null, $criteria)
                    ->filterBySelCategory($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProductGroupsPartial && count($collSelProductGroups)) {
                      $this->initSelProductGroups(false);

                      foreach($collSelProductGroups as $obj) {
                        if (false == $this->collSelProductGroups->contains($obj)) {
                          $this->collSelProductGroups->append($obj);
                        }
                      }

                      $this->collSelProductGroupsPartial = true;
                    }

                    return $collSelProductGroups;
                }

                if($partial && $this->collSelProductGroups) {
                    foreach($this->collSelProductGroups as $obj) {
                        if($obj->isNew()) {
                            $collSelProductGroups[] = $obj;
                        }
                    }
                }

                $this->collSelProductGroups = $collSelProductGroups;
                $this->collSelProductGroupsPartial = false;
            }
        }

        return $this->collSelProductGroups;
    }

    /**
     * Sets a collection of SelProductGroup objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProductGroups A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelCategory The current object (for fluent API support)
     */
    public function setSelProductGroups(PropelCollection $selProductGroups, PropelPDO $con = null)
    {
        $selProductGroupsToDelete = $this->getSelProductGroups(new Criteria(), $con)->diff($selProductGroups);

        $this->selProductGroupsScheduledForDeletion = unserialize(serialize($selProductGroupsToDelete));

        foreach ($selProductGroupsToDelete as $selProductGroupRemoved) {
            $selProductGroupRemoved->setSelCategory(null);
        }

        $this->collSelProductGroups = null;
        foreach ($selProductGroups as $selProductGroup) {
            $this->addSelProductGroup($selProductGroup);
        }

        $this->collSelProductGroups = $selProductGroups;
        $this->collSelProductGroupsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProductGroup objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProductGroup objects.
     * @throws PropelException
     */
    public function countSelProductGroups(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProductGroupsPartial && !$this->isNew();
        if (null === $this->collSelProductGroups || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProductGroups) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProductGroups());
            }
            $query = SelProductGroupQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelCategory($this)
                ->count($con);
        }

        return count($this->collSelProductGroups);
    }

    /**
     * Method called to associate a SelProductGroup object to this object
     * through the SelProductGroup foreign key attribute.
     *
     * @param    SelProductGroup $l SelProductGroup
     * @return SelCategory The current object (for fluent API support)
     */
    public function addSelProductGroup(SelProductGroup $l)
    {
        if ($this->collSelProductGroups === null) {
            $this->initSelProductGroups();
            $this->collSelProductGroupsPartial = true;
        }
        if (!in_array($l, $this->collSelProductGroups->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProductGroup($l);
        }

        return $this;
    }

    /**
     * @param	SelProductGroup $selProductGroup The selProductGroup object to add.
     */
    protected function doAddSelProductGroup($selProductGroup)
    {
        $this->collSelProductGroups[]= $selProductGroup;
        $selProductGroup->setSelCategory($this);
    }

    /**
     * @param	SelProductGroup $selProductGroup The selProductGroup object to remove.
     * @return SelCategory The current object (for fluent API support)
     */
    public function removeSelProductGroup($selProductGroup)
    {
        if ($this->getSelProductGroups()->contains($selProductGroup)) {
            $this->collSelProductGroups->remove($this->collSelProductGroups->search($selProductGroup));
            if (null === $this->selProductGroupsScheduledForDeletion) {
                $this->selProductGroupsScheduledForDeletion = clone $this->collSelProductGroups;
                $this->selProductGroupsScheduledForDeletion->clear();
            }
            $this->selProductGroupsScheduledForDeletion[]= clone $selProductGroup;
            $selProductGroup->setSelCategory(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->main_category_id = null;
        $this->code = null;
        $this->name = null;
        $this->description = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSelBranchCategorys) {
                foreach ($this->collSelBranchCategorys as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelCategorysRelatedById) {
                foreach ($this->collSelCategorysRelatedById as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelProducts) {
                foreach ($this->collSelProducts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelProductGroups) {
                foreach ($this->collSelProductGroups as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collSelBranchCategorys instanceof PropelCollection) {
            $this->collSelBranchCategorys->clearIterator();
        }
        $this->collSelBranchCategorys = null;
        if ($this->collSelCategorysRelatedById instanceof PropelCollection) {
            $this->collSelCategorysRelatedById->clearIterator();
        }
        $this->collSelCategorysRelatedById = null;
        if ($this->collSelProducts instanceof PropelCollection) {
            $this->collSelProducts->clearIterator();
        }
        $this->collSelProducts = null;
        if ($this->collSelProductGroups instanceof PropelCollection) {
            $this->collSelProductGroups->clearIterator();
        }
        $this->collSelProductGroups = null;
        $this->aSelCategoryRelatedByMainCategoryId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelCategoryPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
