<?php


/**
 * Base class that represents a query for the 'sel_product_group' table.
 *
 * 
 *
 * @method SelProductGroupQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelProductGroupQuery orderByCategoryId($order = Criteria::ASC) Order by the CATEGORY_ID column
 * @method SelProductGroupQuery orderByCode($order = Criteria::ASC) Order by the CODE column
 * @method SelProductGroupQuery orderByName($order = Criteria::ASC) Order by the NAME column
 * @method SelProductGroupQuery orderByDescription($order = Criteria::ASC) Order by the DESCRIPTION column
 *
 * @method SelProductGroupQuery groupById() Group by the ID column
 * @method SelProductGroupQuery groupByCategoryId() Group by the CATEGORY_ID column
 * @method SelProductGroupQuery groupByCode() Group by the CODE column
 * @method SelProductGroupQuery groupByName() Group by the NAME column
 * @method SelProductGroupQuery groupByDescription() Group by the DESCRIPTION column
 *
 * @method SelProductGroupQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelProductGroupQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelProductGroupQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelProductGroupQuery leftJoinSelCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelCategory relation
 * @method SelProductGroupQuery rightJoinSelCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelCategory relation
 * @method SelProductGroupQuery innerJoinSelCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the SelCategory relation
 *
 * @method SelProductGroupQuery leftJoinSelProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProduct relation
 * @method SelProductGroupQuery rightJoinSelProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProduct relation
 * @method SelProductGroupQuery innerJoinSelProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProduct relation
 *
 * @method SelProductGroup findOne(PropelPDO $con = null) Return the first SelProductGroup matching the query
 * @method SelProductGroup findOneOrCreate(PropelPDO $con = null) Return the first SelProductGroup matching the query, or a new SelProductGroup object populated from the query conditions when no match is found
 *
 * @method SelProductGroup findOneByCategoryId(int $CATEGORY_ID) Return the first SelProductGroup filtered by the CATEGORY_ID column
 * @method SelProductGroup findOneByCode(string $CODE) Return the first SelProductGroup filtered by the CODE column
 * @method SelProductGroup findOneByName(string $NAME) Return the first SelProductGroup filtered by the NAME column
 * @method SelProductGroup findOneByDescription(string $DESCRIPTION) Return the first SelProductGroup filtered by the DESCRIPTION column
 *
 * @method array findById(int $ID) Return SelProductGroup objects filtered by the ID column
 * @method array findByCategoryId(int $CATEGORY_ID) Return SelProductGroup objects filtered by the CATEGORY_ID column
 * @method array findByCode(string $CODE) Return SelProductGroup objects filtered by the CODE column
 * @method array findByName(string $NAME) Return SelProductGroup objects filtered by the NAME column
 * @method array findByDescription(string $DESCRIPTION) Return SelProductGroup objects filtered by the DESCRIPTION column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProductGroupQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelProductGroupQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelProductGroup', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelProductGroupQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelProductGroupQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelProductGroupQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelProductGroupQuery) {
            return $criteria;
        }
        $query = new SelProductGroupQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelProductGroup|SelProductGroup[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelProductGroupPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelProductGroupPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductGroup A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductGroup A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `CATEGORY_ID`, `CODE`, `NAME`, `DESCRIPTION` FROM `sel_product_group` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelProductGroup();
            $obj->hydrate($row);
            SelProductGroupPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelProductGroup|SelProductGroup[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelProductGroup[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelProductGroupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelProductGroupPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelProductGroupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelProductGroupPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductGroupQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelProductGroupPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the CATEGORY_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryId(1234); // WHERE CATEGORY_ID = 1234
     * $query->filterByCategoryId(array(12, 34)); // WHERE CATEGORY_ID IN (12, 34)
     * $query->filterByCategoryId(array('min' => 12)); // WHERE CATEGORY_ID > 12
     * </code>
     *
     * @see       filterBySelCategory()
     *
     * @param     mixed $categoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductGroupQuery The current query, for fluid interface
     */
    public function filterByCategoryId($categoryId = null, $comparison = null)
    {
        if (is_array($categoryId)) {
            $useMinMax = false;
            if (isset($categoryId['min'])) {
                $this->addUsingAlias(SelProductGroupPeer::CATEGORY_ID, $categoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoryId['max'])) {
                $this->addUsingAlias(SelProductGroupPeer::CATEGORY_ID, $categoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductGroupPeer::CATEGORY_ID, $categoryId, $comparison);
    }

    /**
     * Filter the query on the CODE column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE CODE = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE CODE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductGroupQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductGroupPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the NAME column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE NAME = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE NAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductGroupQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductGroupPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the DESCRIPTION column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE DESCRIPTION = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE DESCRIPTION LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductGroupQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductGroupPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query by a related SelCategory object
     *
     * @param   SelCategory|PropelObjectCollection $selCategory The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductGroupQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelCategory($selCategory, $comparison = null)
    {
        if ($selCategory instanceof SelCategory) {
            return $this
                ->addUsingAlias(SelProductGroupPeer::CATEGORY_ID, $selCategory->getId(), $comparison);
        } elseif ($selCategory instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductGroupPeer::CATEGORY_ID, $selCategory->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelCategory() only accepts arguments of type SelCategory or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelCategory relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductGroupQuery The current query, for fluid interface
     */
    public function joinSelCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelCategory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelCategory');
        }

        return $this;
    }

    /**
     * Use the SelCategory relation SelCategory object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelCategoryQuery A secondary query class using the current class as primary query
     */
    public function useSelCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelCategory', 'SelCategoryQuery');
    }

    /**
     * Filter the query by a related SelProduct object
     *
     * @param   SelProduct|PropelObjectCollection $selProduct  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductGroupQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProduct($selProduct, $comparison = null)
    {
        if ($selProduct instanceof SelProduct) {
            return $this
                ->addUsingAlias(SelProductGroupPeer::ID, $selProduct->getProductGroupId(), $comparison);
        } elseif ($selProduct instanceof PropelObjectCollection) {
            return $this
                ->useSelProductQuery()
                ->filterByPrimaryKeys($selProduct->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProduct() only accepts arguments of type SelProduct or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProduct relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductGroupQuery The current query, for fluid interface
     */
    public function joinSelProduct($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProduct');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProduct');
        }

        return $this;
    }

    /**
     * Use the SelProduct relation SelProduct object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductQuery A secondary query class using the current class as primary query
     */
    public function useSelProductQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProduct', 'SelProductQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelProductGroup $selProductGroup Object to remove from the list of results
     *
     * @return SelProductGroupQuery The current query, for fluid interface
     */
    public function prune($selProductGroup = null)
    {
        if ($selProductGroup) {
            $this->addUsingAlias(SelProductGroupPeer::ID, $selProductGroup->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
