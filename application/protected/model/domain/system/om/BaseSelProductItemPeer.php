<?php


/**
 * Base static class for performing query and update operations on the 'sel_product_item' table.
 *
 * 
 *
 * @package propel.generator.system.om
 */
abstract class BaseSelProductItemPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'system';

    /** the table name for this class */
    const TABLE_NAME = 'sel_product_item';

    /** the related Propel class for this table */
    const OM_CLASS = 'SelProductItem';

    /** the related TableMap class for this table */
    const TM_CLASS = 'SelProductItemTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 14;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 14;

    /** the column name for the ID field */
    const ID = 'sel_product_item.ID';

    /** the column name for the PRODUCT_ACQUISITION_ID field */
    const PRODUCT_ACQUISITION_ID = 'sel_product_item.PRODUCT_ACQUISITION_ID';

    /** the column name for the BRAND_ID field */
    const BRAND_ID = 'sel_product_item.BRAND_ID';

    /** the column name for the BRANCH_ID field */
    const BRANCH_ID = 'sel_product_item.BRANCH_ID';

    /** the column name for the PRODUCT_DEPOSIT_ID field */
    const PRODUCT_DEPOSIT_ID = 'sel_product_item.PRODUCT_DEPOSIT_ID';

    /** the column name for the PRODUCT_SELL_ID field */
    const PRODUCT_SELL_ID = 'sel_product_item.PRODUCT_SELL_ID';

    /** the column name for the CODE field */
    const CODE = 'sel_product_item.CODE';

    /** the column name for the STATUS field */
    const STATUS = 'sel_product_item.STATUS';

    /** the column name for the COST field */
    const COST = 'sel_product_item.COST';

    /** the column name for the PRICE field */
    const PRICE = 'sel_product_item.PRICE';

    /** the column name for the COLOR field */
    const COLOR = 'sel_product_item.COLOR';

    /** the column name for the WARRANTY_SELL field */
    const WARRANTY_SELL = 'sel_product_item.WARRANTY_SELL';

    /** the column name for the OBSERVATION field */
    const OBSERVATION = 'sel_product_item.OBSERVATION';

    /** the column name for the REGISTER_DATE field */
    const REGISTER_DATE = 'sel_product_item.REGISTER_DATE';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of SelProductItem objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array SelProductItem[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. SelProductItemPeer::$fieldNames[SelProductItemPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'ProductAcquisitionId', 'BrandId', 'BranchId', 'ProductDepositId', 'ProductSellId', 'Code', 'Status', 'Cost', 'Price', 'Color', 'WarrantySell', 'Observation', 'RegisterDate', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'productAcquisitionId', 'brandId', 'branchId', 'productDepositId', 'productSellId', 'code', 'status', 'cost', 'price', 'color', 'warrantySell', 'observation', 'registerDate', ),
        BasePeer::TYPE_COLNAME => array (SelProductItemPeer::ID, SelProductItemPeer::PRODUCT_ACQUISITION_ID, SelProductItemPeer::BRAND_ID, SelProductItemPeer::BRANCH_ID, SelProductItemPeer::PRODUCT_DEPOSIT_ID, SelProductItemPeer::PRODUCT_SELL_ID, SelProductItemPeer::CODE, SelProductItemPeer::STATUS, SelProductItemPeer::COST, SelProductItemPeer::PRICE, SelProductItemPeer::COLOR, SelProductItemPeer::WARRANTY_SELL, SelProductItemPeer::OBSERVATION, SelProductItemPeer::REGISTER_DATE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'PRODUCT_ACQUISITION_ID', 'BRAND_ID', 'BRANCH_ID', 'PRODUCT_DEPOSIT_ID', 'PRODUCT_SELL_ID', 'CODE', 'STATUS', 'COST', 'PRICE', 'COLOR', 'WARRANTY_SELL', 'OBSERVATION', 'REGISTER_DATE', ),
        BasePeer::TYPE_FIELDNAME => array ('ID', 'PRODUCT_ACQUISITION_ID', 'BRAND_ID', 'BRANCH_ID', 'PRODUCT_DEPOSIT_ID', 'PRODUCT_SELL_ID', 'CODE', 'STATUS', 'COST', 'PRICE', 'COLOR', 'WARRANTY_SELL', 'OBSERVATION', 'REGISTER_DATE', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. SelProductItemPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ProductAcquisitionId' => 1, 'BrandId' => 2, 'BranchId' => 3, 'ProductDepositId' => 4, 'ProductSellId' => 5, 'Code' => 6, 'Status' => 7, 'Cost' => 8, 'Price' => 9, 'Color' => 10, 'WarrantySell' => 11, 'Observation' => 12, 'RegisterDate' => 13, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'productAcquisitionId' => 1, 'brandId' => 2, 'branchId' => 3, 'productDepositId' => 4, 'productSellId' => 5, 'code' => 6, 'status' => 7, 'cost' => 8, 'price' => 9, 'color' => 10, 'warrantySell' => 11, 'observation' => 12, 'registerDate' => 13, ),
        BasePeer::TYPE_COLNAME => array (SelProductItemPeer::ID => 0, SelProductItemPeer::PRODUCT_ACQUISITION_ID => 1, SelProductItemPeer::BRAND_ID => 2, SelProductItemPeer::BRANCH_ID => 3, SelProductItemPeer::PRODUCT_DEPOSIT_ID => 4, SelProductItemPeer::PRODUCT_SELL_ID => 5, SelProductItemPeer::CODE => 6, SelProductItemPeer::STATUS => 7, SelProductItemPeer::COST => 8, SelProductItemPeer::PRICE => 9, SelProductItemPeer::COLOR => 10, SelProductItemPeer::WARRANTY_SELL => 11, SelProductItemPeer::OBSERVATION => 12, SelProductItemPeer::REGISTER_DATE => 13, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'PRODUCT_ACQUISITION_ID' => 1, 'BRAND_ID' => 2, 'BRANCH_ID' => 3, 'PRODUCT_DEPOSIT_ID' => 4, 'PRODUCT_SELL_ID' => 5, 'CODE' => 6, 'STATUS' => 7, 'COST' => 8, 'PRICE' => 9, 'COLOR' => 10, 'WARRANTY_SELL' => 11, 'OBSERVATION' => 12, 'REGISTER_DATE' => 13, ),
        BasePeer::TYPE_FIELDNAME => array ('ID' => 0, 'PRODUCT_ACQUISITION_ID' => 1, 'BRAND_ID' => 2, 'BRANCH_ID' => 3, 'PRODUCT_DEPOSIT_ID' => 4, 'PRODUCT_SELL_ID' => 5, 'CODE' => 6, 'STATUS' => 7, 'COST' => 8, 'PRICE' => 9, 'COLOR' => 10, 'WARRANTY_SELL' => 11, 'OBSERVATION' => 12, 'REGISTER_DATE' => 13, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = SelProductItemPeer::getFieldNames($toType);
        $key = isset(SelProductItemPeer::$fieldKeys[$fromType][$name]) ? SelProductItemPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(SelProductItemPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, SelProductItemPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return SelProductItemPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. SelProductItemPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(SelProductItemPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SelProductItemPeer::ID);
            $criteria->addSelectColumn(SelProductItemPeer::PRODUCT_ACQUISITION_ID);
            $criteria->addSelectColumn(SelProductItemPeer::BRAND_ID);
            $criteria->addSelectColumn(SelProductItemPeer::BRANCH_ID);
            $criteria->addSelectColumn(SelProductItemPeer::PRODUCT_DEPOSIT_ID);
            $criteria->addSelectColumn(SelProductItemPeer::PRODUCT_SELL_ID);
            $criteria->addSelectColumn(SelProductItemPeer::CODE);
            $criteria->addSelectColumn(SelProductItemPeer::STATUS);
            $criteria->addSelectColumn(SelProductItemPeer::COST);
            $criteria->addSelectColumn(SelProductItemPeer::PRICE);
            $criteria->addSelectColumn(SelProductItemPeer::COLOR);
            $criteria->addSelectColumn(SelProductItemPeer::WARRANTY_SELL);
            $criteria->addSelectColumn(SelProductItemPeer::OBSERVATION);
            $criteria->addSelectColumn(SelProductItemPeer::REGISTER_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.ID');
            $criteria->addSelectColumn($alias . '.PRODUCT_ACQUISITION_ID');
            $criteria->addSelectColumn($alias . '.BRAND_ID');
            $criteria->addSelectColumn($alias . '.BRANCH_ID');
            $criteria->addSelectColumn($alias . '.PRODUCT_DEPOSIT_ID');
            $criteria->addSelectColumn($alias . '.PRODUCT_SELL_ID');
            $criteria->addSelectColumn($alias . '.CODE');
            $criteria->addSelectColumn($alias . '.STATUS');
            $criteria->addSelectColumn($alias . '.COST');
            $criteria->addSelectColumn($alias . '.PRICE');
            $criteria->addSelectColumn($alias . '.COLOR');
            $criteria->addSelectColumn($alias . '.WARRANTY_SELL');
            $criteria->addSelectColumn($alias . '.OBSERVATION');
            $criteria->addSelectColumn($alias . '.REGISTER_DATE');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductItemPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductItemPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 SelProductItem
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = SelProductItemPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return SelProductItemPeer::populateObjects(SelProductItemPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            SelProductItemPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      SelProductItem $obj A SelProductItem object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            SelProductItemPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A SelProductItem object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof SelProductItem) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or SelProductItem object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(SelProductItemPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   SelProductItem Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(SelProductItemPeer::$instances[$key])) {
                return SelProductItemPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool()
    {
        SelProductItemPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to sel_product_item
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = SelProductItemPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = SelProductItemPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = SelProductItemPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SelProductItemPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (SelProductItem object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = SelProductItemPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = SelProductItemPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + SelProductItemPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SelProductItemPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            SelProductItemPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelProductAcquisition table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSelProductAcquisition(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductItemPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductItemPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelProductItemPeer::PRODUCT_ACQUISITION_ID, SelProductAcquisitionPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelBranch table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSelBranch(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductItemPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductItemPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelProductItemPeer::BRANCH_ID, SelBranchPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelBrand table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSelBrand(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductItemPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductItemPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelProductItemPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelProductSell table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSelProductSell(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductItemPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductItemPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelProductItemPeer::PRODUCT_SELL_ID, SelProductSellPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of SelProductItem objects pre-filled with their SelProductAcquisition objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProductItem objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSelProductAcquisition(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);
        }

        SelProductItemPeer::addSelectColumns($criteria);
        $startcol = SelProductItemPeer::NUM_HYDRATE_COLUMNS;
        SelProductAcquisitionPeer::addSelectColumns($criteria);

        $criteria->addJoin(SelProductItemPeer::PRODUCT_ACQUISITION_ID, SelProductAcquisitionPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductItemPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductItemPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SelProductItemPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductItemPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SelProductAcquisitionPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SelProductAcquisitionPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelProductAcquisitionPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SelProductAcquisitionPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SelProductItem) to $obj2 (SelProductAcquisition)
                $obj2->addSelProductItem($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProductItem objects pre-filled with their SelBranch objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProductItem objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSelBranch(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);
        }

        SelProductItemPeer::addSelectColumns($criteria);
        $startcol = SelProductItemPeer::NUM_HYDRATE_COLUMNS;
        SelBranchPeer::addSelectColumns($criteria);

        $criteria->addJoin(SelProductItemPeer::BRANCH_ID, SelBranchPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductItemPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductItemPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SelProductItemPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductItemPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SelBranchPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SelBranchPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelBranchPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SelBranchPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SelProductItem) to $obj2 (SelBranch)
                $obj2->addSelProductItem($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProductItem objects pre-filled with their SelBrand objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProductItem objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSelBrand(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);
        }

        SelProductItemPeer::addSelectColumns($criteria);
        $startcol = SelProductItemPeer::NUM_HYDRATE_COLUMNS;
        SelBrandPeer::addSelectColumns($criteria);

        $criteria->addJoin(SelProductItemPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductItemPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductItemPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SelProductItemPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductItemPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SelBrandPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SelBrandPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelBrandPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SelBrandPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SelProductItem) to $obj2 (SelBrand)
                $obj2->addSelProductItem($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProductItem objects pre-filled with their SelProductSell objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProductItem objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSelProductSell(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);
        }

        SelProductItemPeer::addSelectColumns($criteria);
        $startcol = SelProductItemPeer::NUM_HYDRATE_COLUMNS;
        SelProductSellPeer::addSelectColumns($criteria);

        $criteria->addJoin(SelProductItemPeer::PRODUCT_SELL_ID, SelProductSellPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductItemPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductItemPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SelProductItemPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductItemPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SelProductSellPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SelProductSellPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelProductSellPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SelProductSellPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SelProductItem) to $obj2 (SelProductSell)
                $obj2->addSelProductItem($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductItemPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductItemPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelProductItemPeer::PRODUCT_ACQUISITION_ID, SelProductAcquisitionPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRANCH_ID, SelBranchPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::PRODUCT_SELL_ID, SelProductSellPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of SelProductItem objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProductItem objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);
        }

        SelProductItemPeer::addSelectColumns($criteria);
        $startcol2 = SelProductItemPeer::NUM_HYDRATE_COLUMNS;

        SelProductAcquisitionPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelProductAcquisitionPeer::NUM_HYDRATE_COLUMNS;

        SelBranchPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SelBranchPeer::NUM_HYDRATE_COLUMNS;

        SelBrandPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SelBrandPeer::NUM_HYDRATE_COLUMNS;

        SelProductSellPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SelProductSellPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelProductItemPeer::PRODUCT_ACQUISITION_ID, SelProductAcquisitionPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRANCH_ID, SelBranchPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::PRODUCT_SELL_ID, SelProductSellPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductItemPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductItemPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelProductItemPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductItemPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined SelProductAcquisition rows

            $key2 = SelProductAcquisitionPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = SelProductAcquisitionPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelProductAcquisitionPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelProductAcquisitionPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj2 (SelProductAcquisition)
                $obj2->addSelProductItem($obj1);
            } // if joined row not null

            // Add objects for joined SelBranch rows

            $key3 = SelBranchPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = SelBranchPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = SelBranchPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SelBranchPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj3 (SelBranch)
                $obj3->addSelProductItem($obj1);
            } // if joined row not null

            // Add objects for joined SelBrand rows

            $key4 = SelBrandPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = SelBrandPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = SelBrandPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SelBrandPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj4 (SelBrand)
                $obj4->addSelProductItem($obj1);
            } // if joined row not null

            // Add objects for joined SelProductSell rows

            $key5 = SelProductSellPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = SelProductSellPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = SelProductSellPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SelProductSellPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj5 (SelProductSell)
                $obj5->addSelProductItem($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelProductAcquisition table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSelProductAcquisition(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductItemPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductItemPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SelProductItemPeer::BRANCH_ID, SelBranchPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::PRODUCT_SELL_ID, SelProductSellPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelBranch table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSelBranch(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductItemPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductItemPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SelProductItemPeer::PRODUCT_ACQUISITION_ID, SelProductAcquisitionPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::PRODUCT_SELL_ID, SelProductSellPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelBrand table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSelBrand(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductItemPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductItemPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SelProductItemPeer::PRODUCT_ACQUISITION_ID, SelProductAcquisitionPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRANCH_ID, SelBranchPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::PRODUCT_SELL_ID, SelProductSellPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelProductSell table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSelProductSell(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductItemPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductItemPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SelProductItemPeer::PRODUCT_ACQUISITION_ID, SelProductAcquisitionPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRANCH_ID, SelBranchPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of SelProductItem objects pre-filled with all related objects except SelProductAcquisition.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProductItem objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSelProductAcquisition(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);
        }

        SelProductItemPeer::addSelectColumns($criteria);
        $startcol2 = SelProductItemPeer::NUM_HYDRATE_COLUMNS;

        SelBranchPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelBranchPeer::NUM_HYDRATE_COLUMNS;

        SelBrandPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SelBrandPeer::NUM_HYDRATE_COLUMNS;

        SelProductSellPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SelProductSellPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelProductItemPeer::BRANCH_ID, SelBranchPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::PRODUCT_SELL_ID, SelProductSellPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductItemPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductItemPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelProductItemPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductItemPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SelBranch rows

                $key2 = SelBranchPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SelBranchPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SelBranchPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelBranchPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj2 (SelBranch)
                $obj2->addSelProductItem($obj1);

            } // if joined row is not null

                // Add objects for joined SelBrand rows

                $key3 = SelBrandPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SelBrandPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SelBrandPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SelBrandPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj3 (SelBrand)
                $obj3->addSelProductItem($obj1);

            } // if joined row is not null

                // Add objects for joined SelProductSell rows

                $key4 = SelProductSellPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SelProductSellPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SelProductSellPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SelProductSellPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj4 (SelProductSell)
                $obj4->addSelProductItem($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProductItem objects pre-filled with all related objects except SelBranch.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProductItem objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSelBranch(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);
        }

        SelProductItemPeer::addSelectColumns($criteria);
        $startcol2 = SelProductItemPeer::NUM_HYDRATE_COLUMNS;

        SelProductAcquisitionPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelProductAcquisitionPeer::NUM_HYDRATE_COLUMNS;

        SelBrandPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SelBrandPeer::NUM_HYDRATE_COLUMNS;

        SelProductSellPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SelProductSellPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelProductItemPeer::PRODUCT_ACQUISITION_ID, SelProductAcquisitionPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::PRODUCT_SELL_ID, SelProductSellPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductItemPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductItemPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelProductItemPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductItemPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SelProductAcquisition rows

                $key2 = SelProductAcquisitionPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SelProductAcquisitionPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SelProductAcquisitionPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelProductAcquisitionPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj2 (SelProductAcquisition)
                $obj2->addSelProductItem($obj1);

            } // if joined row is not null

                // Add objects for joined SelBrand rows

                $key3 = SelBrandPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SelBrandPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SelBrandPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SelBrandPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj3 (SelBrand)
                $obj3->addSelProductItem($obj1);

            } // if joined row is not null

                // Add objects for joined SelProductSell rows

                $key4 = SelProductSellPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SelProductSellPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SelProductSellPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SelProductSellPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj4 (SelProductSell)
                $obj4->addSelProductItem($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProductItem objects pre-filled with all related objects except SelBrand.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProductItem objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSelBrand(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);
        }

        SelProductItemPeer::addSelectColumns($criteria);
        $startcol2 = SelProductItemPeer::NUM_HYDRATE_COLUMNS;

        SelProductAcquisitionPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelProductAcquisitionPeer::NUM_HYDRATE_COLUMNS;

        SelBranchPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SelBranchPeer::NUM_HYDRATE_COLUMNS;

        SelProductSellPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SelProductSellPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelProductItemPeer::PRODUCT_ACQUISITION_ID, SelProductAcquisitionPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRANCH_ID, SelBranchPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::PRODUCT_SELL_ID, SelProductSellPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductItemPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductItemPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelProductItemPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductItemPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SelProductAcquisition rows

                $key2 = SelProductAcquisitionPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SelProductAcquisitionPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SelProductAcquisitionPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelProductAcquisitionPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj2 (SelProductAcquisition)
                $obj2->addSelProductItem($obj1);

            } // if joined row is not null

                // Add objects for joined SelBranch rows

                $key3 = SelBranchPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SelBranchPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SelBranchPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SelBranchPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj3 (SelBranch)
                $obj3->addSelProductItem($obj1);

            } // if joined row is not null

                // Add objects for joined SelProductSell rows

                $key4 = SelProductSellPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SelProductSellPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SelProductSellPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SelProductSellPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj4 (SelProductSell)
                $obj4->addSelProductItem($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProductItem objects pre-filled with all related objects except SelProductSell.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProductItem objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSelProductSell(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);
        }

        SelProductItemPeer::addSelectColumns($criteria);
        $startcol2 = SelProductItemPeer::NUM_HYDRATE_COLUMNS;

        SelProductAcquisitionPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelProductAcquisitionPeer::NUM_HYDRATE_COLUMNS;

        SelBranchPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SelBranchPeer::NUM_HYDRATE_COLUMNS;

        SelBrandPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SelBrandPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelProductItemPeer::PRODUCT_ACQUISITION_ID, SelProductAcquisitionPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRANCH_ID, SelBranchPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductItemPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductItemPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductItemPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelProductItemPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductItemPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SelProductAcquisition rows

                $key2 = SelProductAcquisitionPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SelProductAcquisitionPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SelProductAcquisitionPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelProductAcquisitionPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj2 (SelProductAcquisition)
                $obj2->addSelProductItem($obj1);

            } // if joined row is not null

                // Add objects for joined SelBranch rows

                $key3 = SelBranchPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SelBranchPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SelBranchPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SelBranchPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj3 (SelBranch)
                $obj3->addSelProductItem($obj1);

            } // if joined row is not null

                // Add objects for joined SelBrand rows

                $key4 = SelBrandPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SelBrandPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SelBrandPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SelBrandPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (SelProductItem) to the collection in $obj4 (SelBrand)
                $obj4->addSelProductItem($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(SelProductItemPeer::DATABASE_NAME)->getTable(SelProductItemPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseSelProductItemPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseSelProductItemPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new SelProductItemTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return SelProductItemPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a SelProductItem or Criteria object.
     *
     * @param      mixed $values Criteria or SelProductItem object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from SelProductItem object
        }

        if ($criteria->containsKey(SelProductItemPeer::ID) && $criteria->keyContainsValue(SelProductItemPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SelProductItemPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a SelProductItem or Criteria object.
     *
     * @param      mixed $values Criteria or SelProductItem object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(SelProductItemPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(SelProductItemPeer::ID);
            $value = $criteria->remove(SelProductItemPeer::ID);
            if ($value) {
                $selectCriteria->add(SelProductItemPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(SelProductItemPeer::TABLE_NAME);
            }

        } else { // $values is SelProductItem object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the sel_product_item table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(SelProductItemPeer::TABLE_NAME, $con, SelProductItemPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SelProductItemPeer::clearInstancePool();
            SelProductItemPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a SelProductItem or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or SelProductItem object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            SelProductItemPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof SelProductItem) { // it's a model object
            // invalidate the cache for this single object
            SelProductItemPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SelProductItemPeer::DATABASE_NAME);
            $criteria->add(SelProductItemPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                SelProductItemPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(SelProductItemPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            SelProductItemPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given SelProductItem object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      SelProductItem $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(SelProductItemPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(SelProductItemPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(SelProductItemPeer::DATABASE_NAME, SelProductItemPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return SelProductItem
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = SelProductItemPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(SelProductItemPeer::DATABASE_NAME);
        $criteria->add(SelProductItemPeer::ID, $pk);

        $v = SelProductItemPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return SelProductItem[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelProductItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(SelProductItemPeer::DATABASE_NAME);
            $criteria->add(SelProductItemPeer::ID, $pks, Criteria::IN);
            $objs = SelProductItemPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseSelProductItemPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseSelProductItemPeer::buildTableMap();

