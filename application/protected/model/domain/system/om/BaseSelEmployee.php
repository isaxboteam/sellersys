<?php


/**
 * Base class that represents a row from the 'sel_employee' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelEmployee extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelEmployeePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelEmployeePeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the user_id field.
     * @var        int
     */
    protected $user_id;

    /**
     * The value for the init_date field.
     * @var        string
     */
    protected $init_date;

    /**
     * The value for the end_date field.
     * @var        string
     */
    protected $end_date;

    /**
     * The value for the details field.
     * @var        string
     */
    protected $details;

    /**
     * @var        ChoUser
     */
    protected $aChoUser;

    /**
     * @var        PropelObjectCollection|SelBranchEmployee[] Collection to store aggregation of SelBranchEmployee objects.
     */
    protected $collSelBranchEmployees;
    protected $collSelBranchEmployeesPartial;

    /**
     * @var        PropelObjectCollection|SelSale[] Collection to store aggregation of SelSale objects.
     */
    protected $collSelSales;
    protected $collSelSalesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selBranchEmployeesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selSalesScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [user_id] column value.
     * 
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Get the [optionally formatted] temporal [init_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getInitDate($format = '%x')
    {
        if ($this->init_date === null) {
            return null;
        }

        if ($this->init_date === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->init_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->init_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [end_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEndDate($format = '%x')
    {
        if ($this->end_date === null) {
            return null;
        }

        if ($this->end_date === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->end_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->end_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [details] column value.
     * 
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelEmployee The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelEmployeePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [user_id] column.
     * 
     * @param int $v new value
     * @return SelEmployee The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->user_id !== $v) {
            $this->user_id = $v;
            $this->modifiedColumns[] = SelEmployeePeer::USER_ID;
        }

        if ($this->aChoUser !== null && $this->aChoUser->getId() !== $v) {
            $this->aChoUser = null;
        }


        return $this;
    } // setUserId()

    /**
     * Sets the value of [init_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelEmployee The current object (for fluent API support)
     */
    public function setInitDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->init_date !== null || $dt !== null) {
            $currentDateAsString = ($this->init_date !== null && $tmpDt = new DateTime($this->init_date)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->init_date = $newDateAsString;
                $this->modifiedColumns[] = SelEmployeePeer::INIT_DATE;
            }
        } // if either are not null


        return $this;
    } // setInitDate()

    /**
     * Sets the value of [end_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelEmployee The current object (for fluent API support)
     */
    public function setEndDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->end_date !== null || $dt !== null) {
            $currentDateAsString = ($this->end_date !== null && $tmpDt = new DateTime($this->end_date)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->end_date = $newDateAsString;
                $this->modifiedColumns[] = SelEmployeePeer::END_DATE;
            }
        } // if either are not null


        return $this;
    } // setEndDate()

    /**
     * Set the value of [details] column.
     * 
     * @param string $v new value
     * @return SelEmployee The current object (for fluent API support)
     */
    public function setDetails($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->details !== $v) {
            $this->details = $v;
            $this->modifiedColumns[] = SelEmployeePeer::DETAILS;
        }


        return $this;
    } // setDetails()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->user_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->init_date = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->end_date = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->details = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 5; // 5 = SelEmployeePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelEmployee object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aChoUser !== null && $this->user_id !== $this->aChoUser->getId()) {
            $this->aChoUser = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelEmployeePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelEmployeePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aChoUser = null;
            $this->collSelBranchEmployees = null;

            $this->collSelSales = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelEmployeePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelEmployeeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelEmployeePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelEmployeePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aChoUser !== null) {
                if ($this->aChoUser->isModified() || $this->aChoUser->isNew()) {
                    $affectedRows += $this->aChoUser->save($con);
                }
                $this->setChoUser($this->aChoUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->selBranchEmployeesScheduledForDeletion !== null) {
                if (!$this->selBranchEmployeesScheduledForDeletion->isEmpty()) {
                    SelBranchEmployeeQuery::create()
                        ->filterByPrimaryKeys($this->selBranchEmployeesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selBranchEmployeesScheduledForDeletion = null;
                }
            }

            if ($this->collSelBranchEmployees !== null) {
                foreach ($this->collSelBranchEmployees as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->selSalesScheduledForDeletion !== null) {
                if (!$this->selSalesScheduledForDeletion->isEmpty()) {
                    SelSaleQuery::create()
                        ->filterByPrimaryKeys($this->selSalesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selSalesScheduledForDeletion = null;
                }
            }

            if ($this->collSelSales !== null) {
                foreach ($this->collSelSales as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SelEmployeePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SelEmployeePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelEmployeePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelEmployeePeer::USER_ID)) {
            $modifiedColumns[':p' . $index++]  = '`USER_ID`';
        }
        if ($this->isColumnModified(SelEmployeePeer::INIT_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`INIT_DATE`';
        }
        if ($this->isColumnModified(SelEmployeePeer::END_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`END_DATE`';
        }
        if ($this->isColumnModified(SelEmployeePeer::DETAILS)) {
            $modifiedColumns[':p' . $index++]  = '`DETAILS`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_employee` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`USER_ID`':						
                        $stmt->bindValue($identifier, $this->user_id, PDO::PARAM_INT);
                        break;
                    case '`INIT_DATE`':						
                        $stmt->bindValue($identifier, $this->init_date, PDO::PARAM_STR);
                        break;
                    case '`END_DATE`':						
                        $stmt->bindValue($identifier, $this->end_date, PDO::PARAM_STR);
                        break;
                    case '`DETAILS`':						
                        $stmt->bindValue($identifier, $this->details, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aChoUser !== null) {
                if (!$this->aChoUser->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aChoUser->getValidationFailures());
                }
            }

            if (($retval = SelEmployeePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelBranchEmployees !== null) {
                    foreach ($this->collSelBranchEmployees as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSelSales !== null) {
                    foreach ($this->collSelSales as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelEmployeePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUserId();
                break;
            case 2:
                return $this->getInitDate();
                break;
            case 3:
                return $this->getEndDate();
                break;
            case 4:
                return $this->getDetails();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelEmployee'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelEmployee'][$this->getPrimaryKey()] = true;
        $keys = SelEmployeePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUserId(),
            $keys[2] => $this->getInitDate(),
            $keys[3] => $this->getEndDate(),
            $keys[4] => $this->getDetails(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aChoUser) {
                $result['ChoUser'] = $this->aChoUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSelBranchEmployees) {
                $result['SelBranchEmployees'] = $this->collSelBranchEmployees->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSelSales) {
                $result['SelSales'] = $this->collSelSales->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelEmployeePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUserId($value);
                break;
            case 2:
                $this->setInitDate($value);
                break;
            case 3:
                $this->setEndDate($value);
                break;
            case 4:
                $this->setDetails($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelEmployeePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setUserId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setInitDate($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setEndDate($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDetails($arr[$keys[4]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelEmployeePeer::DATABASE_NAME);

        if ($this->isColumnModified(SelEmployeePeer::ID)) $criteria->add(SelEmployeePeer::ID, $this->id);
        if ($this->isColumnModified(SelEmployeePeer::USER_ID)) $criteria->add(SelEmployeePeer::USER_ID, $this->user_id);
        if ($this->isColumnModified(SelEmployeePeer::INIT_DATE)) $criteria->add(SelEmployeePeer::INIT_DATE, $this->init_date);
        if ($this->isColumnModified(SelEmployeePeer::END_DATE)) $criteria->add(SelEmployeePeer::END_DATE, $this->end_date);
        if ($this->isColumnModified(SelEmployeePeer::DETAILS)) $criteria->add(SelEmployeePeer::DETAILS, $this->details);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelEmployeePeer::DATABASE_NAME);
        $criteria->add(SelEmployeePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelEmployee (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUserId($this->getUserId());
        $copyObj->setInitDate($this->getInitDate());
        $copyObj->setEndDate($this->getEndDate());
        $copyObj->setDetails($this->getDetails());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelBranchEmployees() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelBranchEmployee($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSelSales() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelSale($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelEmployee Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelEmployeePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelEmployeePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a ChoUser object.
     *
     * @param             ChoUser $v
     * @return SelEmployee The current object (for fluent API support)
     * @throws PropelException
     */
    public function setChoUser(ChoUser $v = null)
    {
        if ($v === null) {
            $this->setUserId(NULL);
        } else {
            $this->setUserId($v->getId());
        }

        $this->aChoUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChoUser object, it will not be re-added.
        if ($v !== null) {
            $v->addSelEmployee($this);
        }


        return $this;
    }


    /**
     * Get the associated ChoUser object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return ChoUser The associated ChoUser object.
     * @throws PropelException
     */
    public function getChoUser(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aChoUser === null && ($this->user_id !== null) && $doQuery) {
            $this->aChoUser = ChoUserQuery::create()->findPk($this->user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aChoUser->addSelEmployees($this);
             */
        }

        return $this->aChoUser;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelBranchEmployee' == $relationName) {
            $this->initSelBranchEmployees();
        }
        if ('SelSale' == $relationName) {
            $this->initSelSales();
        }
    }

    /**
     * Clears out the collSelBranchEmployees collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelEmployee The current object (for fluent API support)
     * @see        addSelBranchEmployees()
     */
    public function clearSelBranchEmployees()
    {
        $this->collSelBranchEmployees = null; // important to set this to null since that means it is uninitialized
        $this->collSelBranchEmployeesPartial = null;

        return $this;
    }

    /**
     * reset is the collSelBranchEmployees collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelBranchEmployees($v = true)
    {
        $this->collSelBranchEmployeesPartial = $v;
    }

    /**
     * Initializes the collSelBranchEmployees collection.
     *
     * By default this just sets the collSelBranchEmployees collection to an empty array (like clearcollSelBranchEmployees());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelBranchEmployees($overrideExisting = true)
    {
        if (null !== $this->collSelBranchEmployees && !$overrideExisting) {
            return;
        }
        $this->collSelBranchEmployees = new PropelObjectCollection();
        $this->collSelBranchEmployees->setModel('SelBranchEmployee');
    }

    /**
     * Gets an array of SelBranchEmployee objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelEmployee is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelBranchEmployee[] List of SelBranchEmployee objects
     * @throws PropelException
     */
    public function getSelBranchEmployees($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelBranchEmployeesPartial && !$this->isNew();
        if (null === $this->collSelBranchEmployees || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelBranchEmployees) {
                // return empty collection
                $this->initSelBranchEmployees();
            } else {
                $collSelBranchEmployees = SelBranchEmployeeQuery::create(null, $criteria)
                    ->filterBySelEmployee($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelBranchEmployeesPartial && count($collSelBranchEmployees)) {
                      $this->initSelBranchEmployees(false);

                      foreach($collSelBranchEmployees as $obj) {
                        if (false == $this->collSelBranchEmployees->contains($obj)) {
                          $this->collSelBranchEmployees->append($obj);
                        }
                      }

                      $this->collSelBranchEmployeesPartial = true;
                    }

                    return $collSelBranchEmployees;
                }

                if($partial && $this->collSelBranchEmployees) {
                    foreach($this->collSelBranchEmployees as $obj) {
                        if($obj->isNew()) {
                            $collSelBranchEmployees[] = $obj;
                        }
                    }
                }

                $this->collSelBranchEmployees = $collSelBranchEmployees;
                $this->collSelBranchEmployeesPartial = false;
            }
        }

        return $this->collSelBranchEmployees;
    }

    /**
     * Sets a collection of SelBranchEmployee objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selBranchEmployees A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelEmployee The current object (for fluent API support)
     */
    public function setSelBranchEmployees(PropelCollection $selBranchEmployees, PropelPDO $con = null)
    {
        $selBranchEmployeesToDelete = $this->getSelBranchEmployees(new Criteria(), $con)->diff($selBranchEmployees);

        $this->selBranchEmployeesScheduledForDeletion = unserialize(serialize($selBranchEmployeesToDelete));

        foreach ($selBranchEmployeesToDelete as $selBranchEmployeeRemoved) {
            $selBranchEmployeeRemoved->setSelEmployee(null);
        }

        $this->collSelBranchEmployees = null;
        foreach ($selBranchEmployees as $selBranchEmployee) {
            $this->addSelBranchEmployee($selBranchEmployee);
        }

        $this->collSelBranchEmployees = $selBranchEmployees;
        $this->collSelBranchEmployeesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelBranchEmployee objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelBranchEmployee objects.
     * @throws PropelException
     */
    public function countSelBranchEmployees(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelBranchEmployeesPartial && !$this->isNew();
        if (null === $this->collSelBranchEmployees || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelBranchEmployees) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelBranchEmployees());
            }
            $query = SelBranchEmployeeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelEmployee($this)
                ->count($con);
        }

        return count($this->collSelBranchEmployees);
    }

    /**
     * Method called to associate a SelBranchEmployee object to this object
     * through the SelBranchEmployee foreign key attribute.
     *
     * @param    SelBranchEmployee $l SelBranchEmployee
     * @return SelEmployee The current object (for fluent API support)
     */
    public function addSelBranchEmployee(SelBranchEmployee $l)
    {
        if ($this->collSelBranchEmployees === null) {
            $this->initSelBranchEmployees();
            $this->collSelBranchEmployeesPartial = true;
        }
        if (!in_array($l, $this->collSelBranchEmployees->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelBranchEmployee($l);
        }

        return $this;
    }

    /**
     * @param	SelBranchEmployee $selBranchEmployee The selBranchEmployee object to add.
     */
    protected function doAddSelBranchEmployee($selBranchEmployee)
    {
        $this->collSelBranchEmployees[]= $selBranchEmployee;
        $selBranchEmployee->setSelEmployee($this);
    }

    /**
     * @param	SelBranchEmployee $selBranchEmployee The selBranchEmployee object to remove.
     * @return SelEmployee The current object (for fluent API support)
     */
    public function removeSelBranchEmployee($selBranchEmployee)
    {
        if ($this->getSelBranchEmployees()->contains($selBranchEmployee)) {
            $this->collSelBranchEmployees->remove($this->collSelBranchEmployees->search($selBranchEmployee));
            if (null === $this->selBranchEmployeesScheduledForDeletion) {
                $this->selBranchEmployeesScheduledForDeletion = clone $this->collSelBranchEmployees;
                $this->selBranchEmployeesScheduledForDeletion->clear();
            }
            $this->selBranchEmployeesScheduledForDeletion[]= clone $selBranchEmployee;
            $selBranchEmployee->setSelEmployee(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelEmployee is new, it will return
     * an empty collection; or if this SelEmployee has previously
     * been saved, it will retrieve related SelBranchEmployees from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelEmployee.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelBranchEmployee[] List of SelBranchEmployee objects
     */
    public function getSelBranchEmployeesJoinSelBranch($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelBranchEmployeeQuery::create(null, $criteria);
        $query->joinWith('SelBranch', $join_behavior);

        return $this->getSelBranchEmployees($query, $con);
    }

    /**
     * Clears out the collSelSales collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelEmployee The current object (for fluent API support)
     * @see        addSelSales()
     */
    public function clearSelSales()
    {
        $this->collSelSales = null; // important to set this to null since that means it is uninitialized
        $this->collSelSalesPartial = null;

        return $this;
    }

    /**
     * reset is the collSelSales collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelSales($v = true)
    {
        $this->collSelSalesPartial = $v;
    }

    /**
     * Initializes the collSelSales collection.
     *
     * By default this just sets the collSelSales collection to an empty array (like clearcollSelSales());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelSales($overrideExisting = true)
    {
        if (null !== $this->collSelSales && !$overrideExisting) {
            return;
        }
        $this->collSelSales = new PropelObjectCollection();
        $this->collSelSales->setModel('SelSale');
    }

    /**
     * Gets an array of SelSale objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelEmployee is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelSale[] List of SelSale objects
     * @throws PropelException
     */
    public function getSelSales($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelSalesPartial && !$this->isNew();
        if (null === $this->collSelSales || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelSales) {
                // return empty collection
                $this->initSelSales();
            } else {
                $collSelSales = SelSaleQuery::create(null, $criteria)
                    ->filterBySelEmployee($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelSalesPartial && count($collSelSales)) {
                      $this->initSelSales(false);

                      foreach($collSelSales as $obj) {
                        if (false == $this->collSelSales->contains($obj)) {
                          $this->collSelSales->append($obj);
                        }
                      }

                      $this->collSelSalesPartial = true;
                    }

                    return $collSelSales;
                }

                if($partial && $this->collSelSales) {
                    foreach($this->collSelSales as $obj) {
                        if($obj->isNew()) {
                            $collSelSales[] = $obj;
                        }
                    }
                }

                $this->collSelSales = $collSelSales;
                $this->collSelSalesPartial = false;
            }
        }

        return $this->collSelSales;
    }

    /**
     * Sets a collection of SelSale objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selSales A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelEmployee The current object (for fluent API support)
     */
    public function setSelSales(PropelCollection $selSales, PropelPDO $con = null)
    {
        $selSalesToDelete = $this->getSelSales(new Criteria(), $con)->diff($selSales);

        $this->selSalesScheduledForDeletion = unserialize(serialize($selSalesToDelete));

        foreach ($selSalesToDelete as $selSaleRemoved) {
            $selSaleRemoved->setSelEmployee(null);
        }

        $this->collSelSales = null;
        foreach ($selSales as $selSale) {
            $this->addSelSale($selSale);
        }

        $this->collSelSales = $selSales;
        $this->collSelSalesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelSale objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelSale objects.
     * @throws PropelException
     */
    public function countSelSales(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelSalesPartial && !$this->isNew();
        if (null === $this->collSelSales || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelSales) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelSales());
            }
            $query = SelSaleQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelEmployee($this)
                ->count($con);
        }

        return count($this->collSelSales);
    }

    /**
     * Method called to associate a SelSale object to this object
     * through the SelSale foreign key attribute.
     *
     * @param    SelSale $l SelSale
     * @return SelEmployee The current object (for fluent API support)
     */
    public function addSelSale(SelSale $l)
    {
        if ($this->collSelSales === null) {
            $this->initSelSales();
            $this->collSelSalesPartial = true;
        }
        if (!in_array($l, $this->collSelSales->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelSale($l);
        }

        return $this;
    }

    /**
     * @param	SelSale $selSale The selSale object to add.
     */
    protected function doAddSelSale($selSale)
    {
        $this->collSelSales[]= $selSale;
        $selSale->setSelEmployee($this);
    }

    /**
     * @param	SelSale $selSale The selSale object to remove.
     * @return SelEmployee The current object (for fluent API support)
     */
    public function removeSelSale($selSale)
    {
        if ($this->getSelSales()->contains($selSale)) {
            $this->collSelSales->remove($this->collSelSales->search($selSale));
            if (null === $this->selSalesScheduledForDeletion) {
                $this->selSalesScheduledForDeletion = clone $this->collSelSales;
                $this->selSalesScheduledForDeletion->clear();
            }
            $this->selSalesScheduledForDeletion[]= clone $selSale;
            $selSale->setSelEmployee(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelEmployee is new, it will return
     * an empty collection; or if this SelEmployee has previously
     * been saved, it will retrieve related SelSales from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelEmployee.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelSale[] List of SelSale objects
     */
    public function getSelSalesJoinSelCustomer($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelSaleQuery::create(null, $criteria);
        $query->joinWith('SelCustomer', $join_behavior);

        return $this->getSelSales($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelEmployee is new, it will return
     * an empty collection; or if this SelEmployee has previously
     * been saved, it will retrieve related SelSales from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelEmployee.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelSale[] List of SelSale objects
     */
    public function getSelSalesJoinSelBranch($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelSaleQuery::create(null, $criteria);
        $query->joinWith('SelBranch', $join_behavior);

        return $this->getSelSales($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->user_id = null;
        $this->init_date = null;
        $this->end_date = null;
        $this->details = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSelBranchEmployees) {
                foreach ($this->collSelBranchEmployees as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSelSales) {
                foreach ($this->collSelSales as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collSelBranchEmployees instanceof PropelCollection) {
            $this->collSelBranchEmployees->clearIterator();
        }
        $this->collSelBranchEmployees = null;
        if ($this->collSelSales instanceof PropelCollection) {
            $this->collSelSales->clearIterator();
        }
        $this->collSelSales = null;
        $this->aChoUser = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelEmployeePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
