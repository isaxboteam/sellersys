<?php


/**
 * Base static class for performing query and update operations on the 'sel_order' table.
 *
 * 
 *
 * @package propel.generator.system.om
 */
abstract class BaseSelOrderPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'system';

    /** the table name for this class */
    const TABLE_NAME = 'sel_order';

    /** the related Propel class for this table */
    const OM_CLASS = 'SelOrder';

    /** the related TableMap class for this table */
    const TM_CLASS = 'SelOrderTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 17;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 17;

    /** the column name for the ID field */
    const ID = 'sel_order.ID';

    /** the column name for the CUSTOMER_ID field */
    const CUSTOMER_ID = 'sel_order.CUSTOMER_ID';

    /** the column name for the SALES_ID field */
    const SALES_ID = 'sel_order.SALES_ID';

    /** the column name for the STATUS field */
    const STATUS = 'sel_order.STATUS';

    /** the column name for the SHIPPING_ADDRESS field */
    const SHIPPING_ADDRESS = 'sel_order.SHIPPING_ADDRESS';

    /** the column name for the PHONE field */
    const PHONE = 'sel_order.PHONE';

    /** the column name for the CUSTOMMER_COMMENT field */
    const CUSTOMMER_COMMENT = 'sel_order.CUSTOMMER_COMMENT';

    /** the column name for the SALES_COMMENT field */
    const SALES_COMMENT = 'sel_order.SALES_COMMENT';

    /** the column name for the CREATION_DATE field */
    const CREATION_DATE = 'sel_order.CREATION_DATE';

    /** the column name for the ORDER_DATE field */
    const ORDER_DATE = 'sel_order.ORDER_DATE';

    /** the column name for the PAYMENT_DATE field */
    const PAYMENT_DATE = 'sel_order.PAYMENT_DATE';

    /** the column name for the PAYMENT_CODE field */
    const PAYMENT_CODE = 'sel_order.PAYMENT_CODE';

    /** the column name for the PAYMENT_ENTITY field */
    const PAYMENT_ENTITY = 'sel_order.PAYMENT_ENTITY';

    /** the column name for the CHECK_DATE field */
    const CHECK_DATE = 'sel_order.CHECK_DATE';

    /** the column name for the APPROVED_DATE field */
    const APPROVED_DATE = 'sel_order.APPROVED_DATE';

    /** the column name for the SHIPPING_DATE field */
    const SHIPPING_DATE = 'sel_order.SHIPPING_DATE';

    /** the column name for the DELIVERY_DATE field */
    const DELIVERY_DATE = 'sel_order.DELIVERY_DATE';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of SelOrder objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array SelOrder[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. SelOrderPeer::$fieldNames[SelOrderPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'CustomerId', 'SalesId', 'Status', 'ShippingAddress', 'Phone', 'CustommerComment', 'SalesComment', 'CreationDate', 'OrderDate', 'PaymentDate', 'PaymentCode', 'PaymentEntity', 'CheckDate', 'ApprovedDate', 'ShippingDate', 'DeliveryDate', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'customerId', 'salesId', 'status', 'shippingAddress', 'phone', 'custommerComment', 'salesComment', 'creationDate', 'orderDate', 'paymentDate', 'paymentCode', 'paymentEntity', 'checkDate', 'approvedDate', 'shippingDate', 'deliveryDate', ),
        BasePeer::TYPE_COLNAME => array (SelOrderPeer::ID, SelOrderPeer::CUSTOMER_ID, SelOrderPeer::SALES_ID, SelOrderPeer::STATUS, SelOrderPeer::SHIPPING_ADDRESS, SelOrderPeer::PHONE, SelOrderPeer::CUSTOMMER_COMMENT, SelOrderPeer::SALES_COMMENT, SelOrderPeer::CREATION_DATE, SelOrderPeer::ORDER_DATE, SelOrderPeer::PAYMENT_DATE, SelOrderPeer::PAYMENT_CODE, SelOrderPeer::PAYMENT_ENTITY, SelOrderPeer::CHECK_DATE, SelOrderPeer::APPROVED_DATE, SelOrderPeer::SHIPPING_DATE, SelOrderPeer::DELIVERY_DATE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'CUSTOMER_ID', 'SALES_ID', 'STATUS', 'SHIPPING_ADDRESS', 'PHONE', 'CUSTOMMER_COMMENT', 'SALES_COMMENT', 'CREATION_DATE', 'ORDER_DATE', 'PAYMENT_DATE', 'PAYMENT_CODE', 'PAYMENT_ENTITY', 'CHECK_DATE', 'APPROVED_DATE', 'SHIPPING_DATE', 'DELIVERY_DATE', ),
        BasePeer::TYPE_FIELDNAME => array ('ID', 'CUSTOMER_ID', 'SALES_ID', 'STATUS', 'SHIPPING_ADDRESS', 'PHONE', 'CUSTOMMER_COMMENT', 'SALES_COMMENT', 'CREATION_DATE', 'ORDER_DATE', 'PAYMENT_DATE', 'PAYMENT_CODE', 'PAYMENT_ENTITY', 'CHECK_DATE', 'APPROVED_DATE', 'SHIPPING_DATE', 'DELIVERY_DATE', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. SelOrderPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'CustomerId' => 1, 'SalesId' => 2, 'Status' => 3, 'ShippingAddress' => 4, 'Phone' => 5, 'CustommerComment' => 6, 'SalesComment' => 7, 'CreationDate' => 8, 'OrderDate' => 9, 'PaymentDate' => 10, 'PaymentCode' => 11, 'PaymentEntity' => 12, 'CheckDate' => 13, 'ApprovedDate' => 14, 'ShippingDate' => 15, 'DeliveryDate' => 16, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'customerId' => 1, 'salesId' => 2, 'status' => 3, 'shippingAddress' => 4, 'phone' => 5, 'custommerComment' => 6, 'salesComment' => 7, 'creationDate' => 8, 'orderDate' => 9, 'paymentDate' => 10, 'paymentCode' => 11, 'paymentEntity' => 12, 'checkDate' => 13, 'approvedDate' => 14, 'shippingDate' => 15, 'deliveryDate' => 16, ),
        BasePeer::TYPE_COLNAME => array (SelOrderPeer::ID => 0, SelOrderPeer::CUSTOMER_ID => 1, SelOrderPeer::SALES_ID => 2, SelOrderPeer::STATUS => 3, SelOrderPeer::SHIPPING_ADDRESS => 4, SelOrderPeer::PHONE => 5, SelOrderPeer::CUSTOMMER_COMMENT => 6, SelOrderPeer::SALES_COMMENT => 7, SelOrderPeer::CREATION_DATE => 8, SelOrderPeer::ORDER_DATE => 9, SelOrderPeer::PAYMENT_DATE => 10, SelOrderPeer::PAYMENT_CODE => 11, SelOrderPeer::PAYMENT_ENTITY => 12, SelOrderPeer::CHECK_DATE => 13, SelOrderPeer::APPROVED_DATE => 14, SelOrderPeer::SHIPPING_DATE => 15, SelOrderPeer::DELIVERY_DATE => 16, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'CUSTOMER_ID' => 1, 'SALES_ID' => 2, 'STATUS' => 3, 'SHIPPING_ADDRESS' => 4, 'PHONE' => 5, 'CUSTOMMER_COMMENT' => 6, 'SALES_COMMENT' => 7, 'CREATION_DATE' => 8, 'ORDER_DATE' => 9, 'PAYMENT_DATE' => 10, 'PAYMENT_CODE' => 11, 'PAYMENT_ENTITY' => 12, 'CHECK_DATE' => 13, 'APPROVED_DATE' => 14, 'SHIPPING_DATE' => 15, 'DELIVERY_DATE' => 16, ),
        BasePeer::TYPE_FIELDNAME => array ('ID' => 0, 'CUSTOMER_ID' => 1, 'SALES_ID' => 2, 'STATUS' => 3, 'SHIPPING_ADDRESS' => 4, 'PHONE' => 5, 'CUSTOMMER_COMMENT' => 6, 'SALES_COMMENT' => 7, 'CREATION_DATE' => 8, 'ORDER_DATE' => 9, 'PAYMENT_DATE' => 10, 'PAYMENT_CODE' => 11, 'PAYMENT_ENTITY' => 12, 'CHECK_DATE' => 13, 'APPROVED_DATE' => 14, 'SHIPPING_DATE' => 15, 'DELIVERY_DATE' => 16, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = SelOrderPeer::getFieldNames($toType);
        $key = isset(SelOrderPeer::$fieldKeys[$fromType][$name]) ? SelOrderPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(SelOrderPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, SelOrderPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return SelOrderPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. SelOrderPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(SelOrderPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SelOrderPeer::ID);
            $criteria->addSelectColumn(SelOrderPeer::CUSTOMER_ID);
            $criteria->addSelectColumn(SelOrderPeer::SALES_ID);
            $criteria->addSelectColumn(SelOrderPeer::STATUS);
            $criteria->addSelectColumn(SelOrderPeer::SHIPPING_ADDRESS);
            $criteria->addSelectColumn(SelOrderPeer::PHONE);
            $criteria->addSelectColumn(SelOrderPeer::CUSTOMMER_COMMENT);
            $criteria->addSelectColumn(SelOrderPeer::SALES_COMMENT);
            $criteria->addSelectColumn(SelOrderPeer::CREATION_DATE);
            $criteria->addSelectColumn(SelOrderPeer::ORDER_DATE);
            $criteria->addSelectColumn(SelOrderPeer::PAYMENT_DATE);
            $criteria->addSelectColumn(SelOrderPeer::PAYMENT_CODE);
            $criteria->addSelectColumn(SelOrderPeer::PAYMENT_ENTITY);
            $criteria->addSelectColumn(SelOrderPeer::CHECK_DATE);
            $criteria->addSelectColumn(SelOrderPeer::APPROVED_DATE);
            $criteria->addSelectColumn(SelOrderPeer::SHIPPING_DATE);
            $criteria->addSelectColumn(SelOrderPeer::DELIVERY_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.ID');
            $criteria->addSelectColumn($alias . '.CUSTOMER_ID');
            $criteria->addSelectColumn($alias . '.SALES_ID');
            $criteria->addSelectColumn($alias . '.STATUS');
            $criteria->addSelectColumn($alias . '.SHIPPING_ADDRESS');
            $criteria->addSelectColumn($alias . '.PHONE');
            $criteria->addSelectColumn($alias . '.CUSTOMMER_COMMENT');
            $criteria->addSelectColumn($alias . '.SALES_COMMENT');
            $criteria->addSelectColumn($alias . '.CREATION_DATE');
            $criteria->addSelectColumn($alias . '.ORDER_DATE');
            $criteria->addSelectColumn($alias . '.PAYMENT_DATE');
            $criteria->addSelectColumn($alias . '.PAYMENT_CODE');
            $criteria->addSelectColumn($alias . '.PAYMENT_ENTITY');
            $criteria->addSelectColumn($alias . '.CHECK_DATE');
            $criteria->addSelectColumn($alias . '.APPROVED_DATE');
            $criteria->addSelectColumn($alias . '.SHIPPING_DATE');
            $criteria->addSelectColumn($alias . '.DELIVERY_DATE');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(SelOrderPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 SelOrder
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = SelOrderPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return SelOrderPeer::populateObjects(SelOrderPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            SelOrderPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(SelOrderPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      SelOrder $obj A SelOrder object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            SelOrderPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A SelOrder object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof SelOrder) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or SelOrder object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(SelOrderPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   SelOrder Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(SelOrderPeer::$instances[$key])) {
                return SelOrderPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool()
    {
        SelOrderPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to sel_order
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = SelOrderPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = SelOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = SelOrderPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SelOrderPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (SelOrder object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = SelOrderPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = SelOrderPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + SelOrderPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SelOrderPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            SelOrderPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelSale table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSelSale(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelOrderPeer::SALES_ID, SelSalePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelCustomer table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSelCustomer(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelOrderPeer::CUSTOMER_ID, SelCustomerPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of SelOrder objects pre-filled with their SelSale objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSelSale(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelOrderPeer::DATABASE_NAME);
        }

        SelOrderPeer::addSelectColumns($criteria);
        $startcol = SelOrderPeer::NUM_HYDRATE_COLUMNS;
        SelSalePeer::addSelectColumns($criteria);

        $criteria->addJoin(SelOrderPeer::SALES_ID, SelSalePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SelOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelOrderPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SelSalePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SelSalePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelSalePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SelSalePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SelOrder) to $obj2 (SelSale)
                $obj2->addSelOrder($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelOrder objects pre-filled with their SelCustomer objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSelCustomer(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelOrderPeer::DATABASE_NAME);
        }

        SelOrderPeer::addSelectColumns($criteria);
        $startcol = SelOrderPeer::NUM_HYDRATE_COLUMNS;
        SelCustomerPeer::addSelectColumns($criteria);

        $criteria->addJoin(SelOrderPeer::CUSTOMER_ID, SelCustomerPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SelOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelOrderPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SelCustomerPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SelCustomerPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelCustomerPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SelCustomerPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SelOrder) to $obj2 (SelCustomer)
                $obj2->addSelOrder($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelOrderPeer::SALES_ID, SelSalePeer::ID, $join_behavior);

        $criteria->addJoin(SelOrderPeer::CUSTOMER_ID, SelCustomerPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of SelOrder objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelOrderPeer::DATABASE_NAME);
        }

        SelOrderPeer::addSelectColumns($criteria);
        $startcol2 = SelOrderPeer::NUM_HYDRATE_COLUMNS;

        SelSalePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelSalePeer::NUM_HYDRATE_COLUMNS;

        SelCustomerPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SelCustomerPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelOrderPeer::SALES_ID, SelSalePeer::ID, $join_behavior);

        $criteria->addJoin(SelOrderPeer::CUSTOMER_ID, SelCustomerPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelOrderPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined SelSale rows

            $key2 = SelSalePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = SelSalePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelSalePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelSalePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (SelOrder) to the collection in $obj2 (SelSale)
                $obj2->addSelOrder($obj1);
            } // if joined row not null

            // Add objects for joined SelCustomer rows

            $key3 = SelCustomerPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = SelCustomerPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = SelCustomerPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SelCustomerPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (SelOrder) to the collection in $obj3 (SelCustomer)
                $obj3->addSelOrder($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelSale table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSelSale(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SelOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SelOrderPeer::CUSTOMER_ID, SelCustomerPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelCustomer table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSelCustomer(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelOrderPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelOrderPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SelOrderPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SelOrderPeer::SALES_ID, SelSalePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of SelOrder objects pre-filled with all related objects except SelSale.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSelSale(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelOrderPeer::DATABASE_NAME);
        }

        SelOrderPeer::addSelectColumns($criteria);
        $startcol2 = SelOrderPeer::NUM_HYDRATE_COLUMNS;

        SelCustomerPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelCustomerPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelOrderPeer::CUSTOMER_ID, SelCustomerPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelOrderPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SelCustomer rows

                $key2 = SelCustomerPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SelCustomerPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SelCustomerPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelCustomerPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SelOrder) to the collection in $obj2 (SelCustomer)
                $obj2->addSelOrder($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelOrder objects pre-filled with all related objects except SelCustomer.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelOrder objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSelCustomer(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelOrderPeer::DATABASE_NAME);
        }

        SelOrderPeer::addSelectColumns($criteria);
        $startcol2 = SelOrderPeer::NUM_HYDRATE_COLUMNS;

        SelSalePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelSalePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelOrderPeer::SALES_ID, SelSalePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelOrderPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelOrderPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelOrderPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelOrderPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SelSale rows

                $key2 = SelSalePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SelSalePeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SelSalePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelSalePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SelOrder) to the collection in $obj2 (SelSale)
                $obj2->addSelOrder($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(SelOrderPeer::DATABASE_NAME)->getTable(SelOrderPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseSelOrderPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseSelOrderPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new SelOrderTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return SelOrderPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a SelOrder or Criteria object.
     *
     * @param      mixed $values Criteria or SelOrder object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from SelOrder object
        }

        if ($criteria->containsKey(SelOrderPeer::ID) && $criteria->keyContainsValue(SelOrderPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SelOrderPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(SelOrderPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a SelOrder or Criteria object.
     *
     * @param      mixed $values Criteria or SelOrder object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(SelOrderPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(SelOrderPeer::ID);
            $value = $criteria->remove(SelOrderPeer::ID);
            if ($value) {
                $selectCriteria->add(SelOrderPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(SelOrderPeer::TABLE_NAME);
            }

        } else { // $values is SelOrder object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(SelOrderPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the sel_order table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(SelOrderPeer::TABLE_NAME, $con, SelOrderPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SelOrderPeer::clearInstancePool();
            SelOrderPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a SelOrder or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or SelOrder object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            SelOrderPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof SelOrder) { // it's a model object
            // invalidate the cache for this single object
            SelOrderPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SelOrderPeer::DATABASE_NAME);
            $criteria->add(SelOrderPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                SelOrderPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(SelOrderPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            SelOrderPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given SelOrder object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      SelOrder $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(SelOrderPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(SelOrderPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(SelOrderPeer::DATABASE_NAME, SelOrderPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return SelOrder
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = SelOrderPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(SelOrderPeer::DATABASE_NAME);
        $criteria->add(SelOrderPeer::ID, $pk);

        $v = SelOrderPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return SelOrder[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(SelOrderPeer::DATABASE_NAME);
            $criteria->add(SelOrderPeer::ID, $pks, Criteria::IN);
            $objs = SelOrderPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseSelOrderPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseSelOrderPeer::buildTableMap();

