<?php


/**
 * Base class that represents a row from the 'sel_provider' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProvider extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelProviderPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelProviderPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the person_id field.
     * @var        int
     */
    protected $person_id;

    /**
     * The value for the company field.
     * @var        string
     */
    protected $company;

    /**
     * The value for the address field.
     * @var        string
     */
    protected $address;

    /**
     * The value for the person field.
     * @var        string
     */
    protected $person;

    /**
     * The value for the phone field.
     * @var        string
     */
    protected $phone;

    /**
     * The value for the cellphone field.
     * @var        string
     */
    protected $cellphone;

    /**
     * The value for the observations field.
     * @var        string
     */
    protected $observations;

    /**
     * @var        SelPerson
     */
    protected $aSelPerson;

    /**
     * @var        PropelObjectCollection|SelAcquisition[] Collection to store aggregation of SelAcquisition objects.
     */
    protected $collSelAcquisitions;
    protected $collSelAcquisitionsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selAcquisitionsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [person_id] column value.
     * 
     * @return int
     */
    public function getPersonId()
    {
        return $this->person_id;
    }

    /**
     * Get the [company] column value.
     * 
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Get the [address] column value.
     * 
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get the [person] column value.
     * 
     * @return string
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Get the [phone] column value.
     * 
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get the [cellphone] column value.
     * 
     * @return string
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * Get the [observations] column value.
     * 
     * @return string
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelProvider The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelProviderPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [person_id] column.
     * 
     * @param int $v new value
     * @return SelProvider The current object (for fluent API support)
     */
    public function setPersonId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->person_id !== $v) {
            $this->person_id = $v;
            $this->modifiedColumns[] = SelProviderPeer::PERSON_ID;
        }

        if ($this->aSelPerson !== null && $this->aSelPerson->getId() !== $v) {
            $this->aSelPerson = null;
        }


        return $this;
    } // setPersonId()

    /**
     * Set the value of [company] column.
     * 
     * @param string $v new value
     * @return SelProvider The current object (for fluent API support)
     */
    public function setCompany($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->company !== $v) {
            $this->company = $v;
            $this->modifiedColumns[] = SelProviderPeer::COMPANY;
        }


        return $this;
    } // setCompany()

    /**
     * Set the value of [address] column.
     * 
     * @param string $v new value
     * @return SelProvider The current object (for fluent API support)
     */
    public function setAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->address !== $v) {
            $this->address = $v;
            $this->modifiedColumns[] = SelProviderPeer::ADDRESS;
        }


        return $this;
    } // setAddress()

    /**
     * Set the value of [person] column.
     * 
     * @param string $v new value
     * @return SelProvider The current object (for fluent API support)
     */
    public function setPerson($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->person !== $v) {
            $this->person = $v;
            $this->modifiedColumns[] = SelProviderPeer::PERSON;
        }


        return $this;
    } // setPerson()

    /**
     * Set the value of [phone] column.
     * 
     * @param string $v new value
     * @return SelProvider The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[] = SelProviderPeer::PHONE;
        }


        return $this;
    } // setPhone()

    /**
     * Set the value of [cellphone] column.
     * 
     * @param string $v new value
     * @return SelProvider The current object (for fluent API support)
     */
    public function setCellphone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cellphone !== $v) {
            $this->cellphone = $v;
            $this->modifiedColumns[] = SelProviderPeer::CELLPHONE;
        }


        return $this;
    } // setCellphone()

    /**
     * Set the value of [observations] column.
     * 
     * @param string $v new value
     * @return SelProvider The current object (for fluent API support)
     */
    public function setObservations($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observations !== $v) {
            $this->observations = $v;
            $this->modifiedColumns[] = SelProviderPeer::OBSERVATIONS;
        }


        return $this;
    } // setObservations()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->person_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->company = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->address = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->person = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->phone = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->cellphone = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->observations = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 8; // 8 = SelProviderPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelProvider object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSelPerson !== null && $this->person_id !== $this->aSelPerson->getId()) {
            $this->aSelPerson = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProviderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelProviderPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSelPerson = null;
            $this->collSelAcquisitions = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProviderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelProviderQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProviderPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelProviderPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelPerson !== null) {
                if ($this->aSelPerson->isModified() || $this->aSelPerson->isNew()) {
                    $affectedRows += $this->aSelPerson->save($con);
                }
                $this->setSelPerson($this->aSelPerson);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->selAcquisitionsScheduledForDeletion !== null) {
                if (!$this->selAcquisitionsScheduledForDeletion->isEmpty()) {
                    foreach ($this->selAcquisitionsScheduledForDeletion as $selAcquisition) {
                        // need to save related object because we set the relation to null
                        $selAcquisition->save($con);
                    }
                    $this->selAcquisitionsScheduledForDeletion = null;
                }
            }

            if ($this->collSelAcquisitions !== null) {
                foreach ($this->collSelAcquisitions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SelProviderPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SelProviderPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelProviderPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelProviderPeer::PERSON_ID)) {
            $modifiedColumns[':p' . $index++]  = '`PERSON_ID`';
        }
        if ($this->isColumnModified(SelProviderPeer::COMPANY)) {
            $modifiedColumns[':p' . $index++]  = '`COMPANY`';
        }
        if ($this->isColumnModified(SelProviderPeer::ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`ADDRESS`';
        }
        if ($this->isColumnModified(SelProviderPeer::PERSON)) {
            $modifiedColumns[':p' . $index++]  = '`PERSON`';
        }
        if ($this->isColumnModified(SelProviderPeer::PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`PHONE`';
        }
        if ($this->isColumnModified(SelProviderPeer::CELLPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`CELLPHONE`';
        }
        if ($this->isColumnModified(SelProviderPeer::OBSERVATIONS)) {
            $modifiedColumns[':p' . $index++]  = '`OBSERVATIONS`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_provider` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`PERSON_ID`':						
                        $stmt->bindValue($identifier, $this->person_id, PDO::PARAM_INT);
                        break;
                    case '`COMPANY`':						
                        $stmt->bindValue($identifier, $this->company, PDO::PARAM_STR);
                        break;
                    case '`ADDRESS`':						
                        $stmt->bindValue($identifier, $this->address, PDO::PARAM_STR);
                        break;
                    case '`PERSON`':						
                        $stmt->bindValue($identifier, $this->person, PDO::PARAM_STR);
                        break;
                    case '`PHONE`':						
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case '`CELLPHONE`':						
                        $stmt->bindValue($identifier, $this->cellphone, PDO::PARAM_STR);
                        break;
                    case '`OBSERVATIONS`':						
                        $stmt->bindValue($identifier, $this->observations, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelPerson !== null) {
                if (!$this->aSelPerson->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelPerson->getValidationFailures());
                }
            }

            if (($retval = SelProviderPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelAcquisitions !== null) {
                    foreach ($this->collSelAcquisitions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelProviderPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getPersonId();
                break;
            case 2:
                return $this->getCompany();
                break;
            case 3:
                return $this->getAddress();
                break;
            case 4:
                return $this->getPerson();
                break;
            case 5:
                return $this->getPhone();
                break;
            case 6:
                return $this->getCellphone();
                break;
            case 7:
                return $this->getObservations();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelProvider'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelProvider'][$this->getPrimaryKey()] = true;
        $keys = SelProviderPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getPersonId(),
            $keys[2] => $this->getCompany(),
            $keys[3] => $this->getAddress(),
            $keys[4] => $this->getPerson(),
            $keys[5] => $this->getPhone(),
            $keys[6] => $this->getCellphone(),
            $keys[7] => $this->getObservations(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSelPerson) {
                $result['SelPerson'] = $this->aSelPerson->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSelAcquisitions) {
                $result['SelAcquisitions'] = $this->collSelAcquisitions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelProviderPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setPersonId($value);
                break;
            case 2:
                $this->setCompany($value);
                break;
            case 3:
                $this->setAddress($value);
                break;
            case 4:
                $this->setPerson($value);
                break;
            case 5:
                $this->setPhone($value);
                break;
            case 6:
                $this->setCellphone($value);
                break;
            case 7:
                $this->setObservations($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelProviderPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setPersonId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCompany($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setAddress($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPerson($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPhone($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCellphone($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setObservations($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelProviderPeer::DATABASE_NAME);

        if ($this->isColumnModified(SelProviderPeer::ID)) $criteria->add(SelProviderPeer::ID, $this->id);
        if ($this->isColumnModified(SelProviderPeer::PERSON_ID)) $criteria->add(SelProviderPeer::PERSON_ID, $this->person_id);
        if ($this->isColumnModified(SelProviderPeer::COMPANY)) $criteria->add(SelProviderPeer::COMPANY, $this->company);
        if ($this->isColumnModified(SelProviderPeer::ADDRESS)) $criteria->add(SelProviderPeer::ADDRESS, $this->address);
        if ($this->isColumnModified(SelProviderPeer::PERSON)) $criteria->add(SelProviderPeer::PERSON, $this->person);
        if ($this->isColumnModified(SelProviderPeer::PHONE)) $criteria->add(SelProviderPeer::PHONE, $this->phone);
        if ($this->isColumnModified(SelProviderPeer::CELLPHONE)) $criteria->add(SelProviderPeer::CELLPHONE, $this->cellphone);
        if ($this->isColumnModified(SelProviderPeer::OBSERVATIONS)) $criteria->add(SelProviderPeer::OBSERVATIONS, $this->observations);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelProviderPeer::DATABASE_NAME);
        $criteria->add(SelProviderPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelProvider (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPersonId($this->getPersonId());
        $copyObj->setCompany($this->getCompany());
        $copyObj->setAddress($this->getAddress());
        $copyObj->setPerson($this->getPerson());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setCellphone($this->getCellphone());
        $copyObj->setObservations($this->getObservations());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelAcquisitions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelAcquisition($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelProvider Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelProviderPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelProviderPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SelPerson object.
     *
     * @param             SelPerson $v
     * @return SelProvider The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelPerson(SelPerson $v = null)
    {
        if ($v === null) {
            $this->setPersonId(NULL);
        } else {
            $this->setPersonId($v->getId());
        }

        $this->aSelPerson = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelPerson object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProvider($this);
        }


        return $this;
    }


    /**
     * Get the associated SelPerson object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelPerson The associated SelPerson object.
     * @throws PropelException
     */
    public function getSelPerson(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelPerson === null && ($this->person_id !== null) && $doQuery) {
            $this->aSelPerson = SelPersonQuery::create()->findPk($this->person_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelPerson->addSelProviders($this);
             */
        }

        return $this->aSelPerson;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelAcquisition' == $relationName) {
            $this->initSelAcquisitions();
        }
    }

    /**
     * Clears out the collSelAcquisitions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelProvider The current object (for fluent API support)
     * @see        addSelAcquisitions()
     */
    public function clearSelAcquisitions()
    {
        $this->collSelAcquisitions = null; // important to set this to null since that means it is uninitialized
        $this->collSelAcquisitionsPartial = null;

        return $this;
    }

    /**
     * reset is the collSelAcquisitions collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelAcquisitions($v = true)
    {
        $this->collSelAcquisitionsPartial = $v;
    }

    /**
     * Initializes the collSelAcquisitions collection.
     *
     * By default this just sets the collSelAcquisitions collection to an empty array (like clearcollSelAcquisitions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelAcquisitions($overrideExisting = true)
    {
        if (null !== $this->collSelAcquisitions && !$overrideExisting) {
            return;
        }
        $this->collSelAcquisitions = new PropelObjectCollection();
        $this->collSelAcquisitions->setModel('SelAcquisition');
    }

    /**
     * Gets an array of SelAcquisition objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelProvider is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelAcquisition[] List of SelAcquisition objects
     * @throws PropelException
     */
    public function getSelAcquisitions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelAcquisitionsPartial && !$this->isNew();
        if (null === $this->collSelAcquisitions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelAcquisitions) {
                // return empty collection
                $this->initSelAcquisitions();
            } else {
                $collSelAcquisitions = SelAcquisitionQuery::create(null, $criteria)
                    ->filterBySelProvider($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelAcquisitionsPartial && count($collSelAcquisitions)) {
                      $this->initSelAcquisitions(false);

                      foreach($collSelAcquisitions as $obj) {
                        if (false == $this->collSelAcquisitions->contains($obj)) {
                          $this->collSelAcquisitions->append($obj);
                        }
                      }

                      $this->collSelAcquisitionsPartial = true;
                    }

                    return $collSelAcquisitions;
                }

                if($partial && $this->collSelAcquisitions) {
                    foreach($this->collSelAcquisitions as $obj) {
                        if($obj->isNew()) {
                            $collSelAcquisitions[] = $obj;
                        }
                    }
                }

                $this->collSelAcquisitions = $collSelAcquisitions;
                $this->collSelAcquisitionsPartial = false;
            }
        }

        return $this->collSelAcquisitions;
    }

    /**
     * Sets a collection of SelAcquisition objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selAcquisitions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelProvider The current object (for fluent API support)
     */
    public function setSelAcquisitions(PropelCollection $selAcquisitions, PropelPDO $con = null)
    {
        $selAcquisitionsToDelete = $this->getSelAcquisitions(new Criteria(), $con)->diff($selAcquisitions);

        $this->selAcquisitionsScheduledForDeletion = unserialize(serialize($selAcquisitionsToDelete));

        foreach ($selAcquisitionsToDelete as $selAcquisitionRemoved) {
            $selAcquisitionRemoved->setSelProvider(null);
        }

        $this->collSelAcquisitions = null;
        foreach ($selAcquisitions as $selAcquisition) {
            $this->addSelAcquisition($selAcquisition);
        }

        $this->collSelAcquisitions = $selAcquisitions;
        $this->collSelAcquisitionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelAcquisition objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelAcquisition objects.
     * @throws PropelException
     */
    public function countSelAcquisitions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelAcquisitionsPartial && !$this->isNew();
        if (null === $this->collSelAcquisitions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelAcquisitions) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelAcquisitions());
            }
            $query = SelAcquisitionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelProvider($this)
                ->count($con);
        }

        return count($this->collSelAcquisitions);
    }

    /**
     * Method called to associate a SelAcquisition object to this object
     * through the SelAcquisition foreign key attribute.
     *
     * @param    SelAcquisition $l SelAcquisition
     * @return SelProvider The current object (for fluent API support)
     */
    public function addSelAcquisition(SelAcquisition $l)
    {
        if ($this->collSelAcquisitions === null) {
            $this->initSelAcquisitions();
            $this->collSelAcquisitionsPartial = true;
        }
        if (!in_array($l, $this->collSelAcquisitions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelAcquisition($l);
        }

        return $this;
    }

    /**
     * @param	SelAcquisition $selAcquisition The selAcquisition object to add.
     */
    protected function doAddSelAcquisition($selAcquisition)
    {
        $this->collSelAcquisitions[]= $selAcquisition;
        $selAcquisition->setSelProvider($this);
    }

    /**
     * @param	SelAcquisition $selAcquisition The selAcquisition object to remove.
     * @return SelProvider The current object (for fluent API support)
     */
    public function removeSelAcquisition($selAcquisition)
    {
        if ($this->getSelAcquisitions()->contains($selAcquisition)) {
            $this->collSelAcquisitions->remove($this->collSelAcquisitions->search($selAcquisition));
            if (null === $this->selAcquisitionsScheduledForDeletion) {
                $this->selAcquisitionsScheduledForDeletion = clone $this->collSelAcquisitions;
                $this->selAcquisitionsScheduledForDeletion->clear();
            }
            $this->selAcquisitionsScheduledForDeletion[]= $selAcquisition;
            $selAcquisition->setSelProvider(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->person_id = null;
        $this->company = null;
        $this->address = null;
        $this->person = null;
        $this->phone = null;
        $this->cellphone = null;
        $this->observations = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSelAcquisitions) {
                foreach ($this->collSelAcquisitions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collSelAcquisitions instanceof PropelCollection) {
            $this->collSelAcquisitions->clearIterator();
        }
        $this->collSelAcquisitions = null;
        $this->aSelPerson = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelProviderPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
