<?php


/**
 * Base class that represents a query for the 'sel_sale' table.
 *
 * 
 *
 * @method SelSaleQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelSaleQuery orderByBranchId($order = Criteria::ASC) Order by the BRANCH_ID column
 * @method SelSaleQuery orderByEmployeeId($order = Criteria::ASC) Order by the EMPLOYEE_ID column
 * @method SelSaleQuery orderByCustomerId($order = Criteria::ASC) Order by the CUSTOMER_ID column
 * @method SelSaleQuery orderByDate($order = Criteria::ASC) Order by the DATE column
 * @method SelSaleQuery orderByStatus($order = Criteria::ASC) Order by the STATUS column
 * @method SelSaleQuery orderByCode($order = Criteria::ASC) Order by the CODE column
 * @method SelSaleQuery orderByMoneyChange($order = Criteria::ASC) Order by the MONEY_CHANGE column
 * @method SelSaleQuery orderByObservation($order = Criteria::ASC) Order by the OBSERVATION column
 *
 * @method SelSaleQuery groupById() Group by the ID column
 * @method SelSaleQuery groupByBranchId() Group by the BRANCH_ID column
 * @method SelSaleQuery groupByEmployeeId() Group by the EMPLOYEE_ID column
 * @method SelSaleQuery groupByCustomerId() Group by the CUSTOMER_ID column
 * @method SelSaleQuery groupByDate() Group by the DATE column
 * @method SelSaleQuery groupByStatus() Group by the STATUS column
 * @method SelSaleQuery groupByCode() Group by the CODE column
 * @method SelSaleQuery groupByMoneyChange() Group by the MONEY_CHANGE column
 * @method SelSaleQuery groupByObservation() Group by the OBSERVATION column
 *
 * @method SelSaleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelSaleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelSaleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelSaleQuery leftJoinSelCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelCustomer relation
 * @method SelSaleQuery rightJoinSelCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelCustomer relation
 * @method SelSaleQuery innerJoinSelCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the SelCustomer relation
 *
 * @method SelSaleQuery leftJoinSelBranch($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelBranch relation
 * @method SelSaleQuery rightJoinSelBranch($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelBranch relation
 * @method SelSaleQuery innerJoinSelBranch($relationAlias = null) Adds a INNER JOIN clause to the query using the SelBranch relation
 *
 * @method SelSaleQuery leftJoinSelEmployee($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelEmployee relation
 * @method SelSaleQuery rightJoinSelEmployee($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelEmployee relation
 * @method SelSaleQuery innerJoinSelEmployee($relationAlias = null) Adds a INNER JOIN clause to the query using the SelEmployee relation
 *
 * @method SelSaleQuery leftJoinSelOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelOrder relation
 * @method SelSaleQuery rightJoinSelOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelOrder relation
 * @method SelSaleQuery innerJoinSelOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SelOrder relation
 *
 * @method SelSaleQuery leftJoinSelProductSell($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductSell relation
 * @method SelSaleQuery rightJoinSelProductSell($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductSell relation
 * @method SelSaleQuery innerJoinSelProductSell($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductSell relation
 *
 * @method SelSale findOne(PropelPDO $con = null) Return the first SelSale matching the query
 * @method SelSale findOneOrCreate(PropelPDO $con = null) Return the first SelSale matching the query, or a new SelSale object populated from the query conditions when no match is found
 *
 * @method SelSale findOneByBranchId(int $BRANCH_ID) Return the first SelSale filtered by the BRANCH_ID column
 * @method SelSale findOneByEmployeeId(int $EMPLOYEE_ID) Return the first SelSale filtered by the EMPLOYEE_ID column
 * @method SelSale findOneByCustomerId(int $CUSTOMER_ID) Return the first SelSale filtered by the CUSTOMER_ID column
 * @method SelSale findOneByDate(string $DATE) Return the first SelSale filtered by the DATE column
 * @method SelSale findOneByStatus(string $STATUS) Return the first SelSale filtered by the STATUS column
 * @method SelSale findOneByCode(string $CODE) Return the first SelSale filtered by the CODE column
 * @method SelSale findOneByMoneyChange(int $MONEY_CHANGE) Return the first SelSale filtered by the MONEY_CHANGE column
 * @method SelSale findOneByObservation(string $OBSERVATION) Return the first SelSale filtered by the OBSERVATION column
 *
 * @method array findById(int $ID) Return SelSale objects filtered by the ID column
 * @method array findByBranchId(int $BRANCH_ID) Return SelSale objects filtered by the BRANCH_ID column
 * @method array findByEmployeeId(int $EMPLOYEE_ID) Return SelSale objects filtered by the EMPLOYEE_ID column
 * @method array findByCustomerId(int $CUSTOMER_ID) Return SelSale objects filtered by the CUSTOMER_ID column
 * @method array findByDate(string $DATE) Return SelSale objects filtered by the DATE column
 * @method array findByStatus(string $STATUS) Return SelSale objects filtered by the STATUS column
 * @method array findByCode(string $CODE) Return SelSale objects filtered by the CODE column
 * @method array findByMoneyChange(int $MONEY_CHANGE) Return SelSale objects filtered by the MONEY_CHANGE column
 * @method array findByObservation(string $OBSERVATION) Return SelSale objects filtered by the OBSERVATION column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelSaleQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelSaleQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelSale', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelSaleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelSaleQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelSaleQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelSaleQuery) {
            return $criteria;
        }
        $query = new SelSaleQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelSale|SelSale[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelSalePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelSalePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelSale A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelSale A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `BRANCH_ID`, `EMPLOYEE_ID`, `CUSTOMER_ID`, `DATE`, `STATUS`, `CODE`, `MONEY_CHANGE`, `OBSERVATION` FROM `sel_sale` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelSale();
            $obj->hydrate($row);
            SelSalePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelSale|SelSale[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelSale[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelSalePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelSalePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelSalePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the BRANCH_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByBranchId(1234); // WHERE BRANCH_ID = 1234
     * $query->filterByBranchId(array(12, 34)); // WHERE BRANCH_ID IN (12, 34)
     * $query->filterByBranchId(array('min' => 12)); // WHERE BRANCH_ID > 12
     * </code>
     *
     * @see       filterBySelBranch()
     *
     * @param     mixed $branchId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function filterByBranchId($branchId = null, $comparison = null)
    {
        if (is_array($branchId)) {
            $useMinMax = false;
            if (isset($branchId['min'])) {
                $this->addUsingAlias(SelSalePeer::BRANCH_ID, $branchId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($branchId['max'])) {
                $this->addUsingAlias(SelSalePeer::BRANCH_ID, $branchId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelSalePeer::BRANCH_ID, $branchId, $comparison);
    }

    /**
     * Filter the query on the EMPLOYEE_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByEmployeeId(1234); // WHERE EMPLOYEE_ID = 1234
     * $query->filterByEmployeeId(array(12, 34)); // WHERE EMPLOYEE_ID IN (12, 34)
     * $query->filterByEmployeeId(array('min' => 12)); // WHERE EMPLOYEE_ID > 12
     * </code>
     *
     * @see       filterBySelEmployee()
     *
     * @param     mixed $employeeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function filterByEmployeeId($employeeId = null, $comparison = null)
    {
        if (is_array($employeeId)) {
            $useMinMax = false;
            if (isset($employeeId['min'])) {
                $this->addUsingAlias(SelSalePeer::EMPLOYEE_ID, $employeeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($employeeId['max'])) {
                $this->addUsingAlias(SelSalePeer::EMPLOYEE_ID, $employeeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelSalePeer::EMPLOYEE_ID, $employeeId, $comparison);
    }

    /**
     * Filter the query on the CUSTOMER_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerId(1234); // WHERE CUSTOMER_ID = 1234
     * $query->filterByCustomerId(array(12, 34)); // WHERE CUSTOMER_ID IN (12, 34)
     * $query->filterByCustomerId(array('min' => 12)); // WHERE CUSTOMER_ID > 12
     * </code>
     *
     * @see       filterBySelCustomer()
     *
     * @param     mixed $customerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function filterByCustomerId($customerId = null, $comparison = null)
    {
        if (is_array($customerId)) {
            $useMinMax = false;
            if (isset($customerId['min'])) {
                $this->addUsingAlias(SelSalePeer::CUSTOMER_ID, $customerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerId['max'])) {
                $this->addUsingAlias(SelSalePeer::CUSTOMER_ID, $customerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelSalePeer::CUSTOMER_ID, $customerId, $comparison);
    }

    /**
     * Filter the query on the DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE DATE = '2011-03-14'
     * $query->filterByDate('now'); // WHERE DATE = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(SelSalePeer::DATE, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(SelSalePeer::DATE, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelSalePeer::DATE, $date, $comparison);
    }

    /**
     * Filter the query on the STATUS column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE STATUS = 'fooValue'
     * $query->filterByStatus('%fooValue%'); // WHERE STATUS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $status)) {
                $status = str_replace('*', '%', $status);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelSalePeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the CODE column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE CODE = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE CODE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelSalePeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the MONEY_CHANGE column
     *
     * Example usage:
     * <code>
     * $query->filterByMoneyChange(1234); // WHERE MONEY_CHANGE = 1234
     * $query->filterByMoneyChange(array(12, 34)); // WHERE MONEY_CHANGE IN (12, 34)
     * $query->filterByMoneyChange(array('min' => 12)); // WHERE MONEY_CHANGE > 12
     * </code>
     *
     * @param     mixed $moneyChange The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function filterByMoneyChange($moneyChange = null, $comparison = null)
    {
        if (is_array($moneyChange)) {
            $useMinMax = false;
            if (isset($moneyChange['min'])) {
                $this->addUsingAlias(SelSalePeer::MONEY_CHANGE, $moneyChange['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($moneyChange['max'])) {
                $this->addUsingAlias(SelSalePeer::MONEY_CHANGE, $moneyChange['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelSalePeer::MONEY_CHANGE, $moneyChange, $comparison);
    }

    /**
     * Filter the query on the OBSERVATION column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE OBSERVATION = 'fooValue'
     * $query->filterByObservation('%fooValue%'); // WHERE OBSERVATION LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $observation)) {
                $observation = str_replace('*', '%', $observation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelSalePeer::OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query by a related SelCustomer object
     *
     * @param   SelCustomer|PropelObjectCollection $selCustomer The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelSaleQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelCustomer($selCustomer, $comparison = null)
    {
        if ($selCustomer instanceof SelCustomer) {
            return $this
                ->addUsingAlias(SelSalePeer::CUSTOMER_ID, $selCustomer->getId(), $comparison);
        } elseif ($selCustomer instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelSalePeer::CUSTOMER_ID, $selCustomer->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelCustomer() only accepts arguments of type SelCustomer or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelCustomer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function joinSelCustomer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelCustomer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelCustomer');
        }

        return $this;
    }

    /**
     * Use the SelCustomer relation SelCustomer object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelCustomerQuery A secondary query class using the current class as primary query
     */
    public function useSelCustomerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelCustomer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelCustomer', 'SelCustomerQuery');
    }

    /**
     * Filter the query by a related SelBranch object
     *
     * @param   SelBranch|PropelObjectCollection $selBranch The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelSaleQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelBranch($selBranch, $comparison = null)
    {
        if ($selBranch instanceof SelBranch) {
            return $this
                ->addUsingAlias(SelSalePeer::BRANCH_ID, $selBranch->getId(), $comparison);
        } elseif ($selBranch instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelSalePeer::BRANCH_ID, $selBranch->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelBranch() only accepts arguments of type SelBranch or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelBranch relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function joinSelBranch($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelBranch');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelBranch');
        }

        return $this;
    }

    /**
     * Use the SelBranch relation SelBranch object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelBranchQuery A secondary query class using the current class as primary query
     */
    public function useSelBranchQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelBranch($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelBranch', 'SelBranchQuery');
    }

    /**
     * Filter the query by a related SelEmployee object
     *
     * @param   SelEmployee|PropelObjectCollection $selEmployee The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelSaleQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelEmployee($selEmployee, $comparison = null)
    {
        if ($selEmployee instanceof SelEmployee) {
            return $this
                ->addUsingAlias(SelSalePeer::EMPLOYEE_ID, $selEmployee->getId(), $comparison);
        } elseif ($selEmployee instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelSalePeer::EMPLOYEE_ID, $selEmployee->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelEmployee() only accepts arguments of type SelEmployee or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelEmployee relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function joinSelEmployee($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelEmployee');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelEmployee');
        }

        return $this;
    }

    /**
     * Use the SelEmployee relation SelEmployee object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelEmployeeQuery A secondary query class using the current class as primary query
     */
    public function useSelEmployeeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelEmployee($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelEmployee', 'SelEmployeeQuery');
    }

    /**
     * Filter the query by a related SelOrder object
     *
     * @param   SelOrder|PropelObjectCollection $selOrder  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelSaleQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelOrder($selOrder, $comparison = null)
    {
        if ($selOrder instanceof SelOrder) {
            return $this
                ->addUsingAlias(SelSalePeer::ID, $selOrder->getSalesId(), $comparison);
        } elseif ($selOrder instanceof PropelObjectCollection) {
            return $this
                ->useSelOrderQuery()
                ->filterByPrimaryKeys($selOrder->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelOrder() only accepts arguments of type SelOrder or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function joinSelOrder($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelOrder');
        }

        return $this;
    }

    /**
     * Use the SelOrder relation SelOrder object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelOrderQuery A secondary query class using the current class as primary query
     */
    public function useSelOrderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelOrder', 'SelOrderQuery');
    }

    /**
     * Filter the query by a related SelProductSell object
     *
     * @param   SelProductSell|PropelObjectCollection $selProductSell  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelSaleQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductSell($selProductSell, $comparison = null)
    {
        if ($selProductSell instanceof SelProductSell) {
            return $this
                ->addUsingAlias(SelSalePeer::ID, $selProductSell->getSaleId(), $comparison);
        } elseif ($selProductSell instanceof PropelObjectCollection) {
            return $this
                ->useSelProductSellQuery()
                ->filterByPrimaryKeys($selProductSell->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductSell() only accepts arguments of type SelProductSell or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductSell relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function joinSelProductSell($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductSell');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductSell');
        }

        return $this;
    }

    /**
     * Use the SelProductSell relation SelProductSell object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductSellQuery A secondary query class using the current class as primary query
     */
    public function useSelProductSellQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductSell($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductSell', 'SelProductSellQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelSale $selSale Object to remove from the list of results
     *
     * @return SelSaleQuery The current query, for fluid interface
     */
    public function prune($selSale = null)
    {
        if ($selSale) {
            $this->addUsingAlias(SelSalePeer::ID, $selSale->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
