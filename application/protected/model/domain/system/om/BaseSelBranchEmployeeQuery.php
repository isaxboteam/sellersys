<?php


/**
 * Base class that represents a query for the 'sel_branch_employee' table.
 *
 * 
 *
 * @method SelBranchEmployeeQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelBranchEmployeeQuery orderByBranchId($order = Criteria::ASC) Order by the BRANCH_ID column
 * @method SelBranchEmployeeQuery orderByEmployeeId($order = Criteria::ASC) Order by the EMPLOYEE_ID column
 * @method SelBranchEmployeeQuery orderByInitDate($order = Criteria::ASC) Order by the INIT_DATE column
 * @method SelBranchEmployeeQuery orderByEndDate($order = Criteria::ASC) Order by the END_DATE column
 * @method SelBranchEmployeeQuery orderByAdmin($order = Criteria::ASC) Order by the ADMIN column
 * @method SelBranchEmployeeQuery orderByStatus($order = Criteria::ASC) Order by the STATUS column
 *
 * @method SelBranchEmployeeQuery groupById() Group by the ID column
 * @method SelBranchEmployeeQuery groupByBranchId() Group by the BRANCH_ID column
 * @method SelBranchEmployeeQuery groupByEmployeeId() Group by the EMPLOYEE_ID column
 * @method SelBranchEmployeeQuery groupByInitDate() Group by the INIT_DATE column
 * @method SelBranchEmployeeQuery groupByEndDate() Group by the END_DATE column
 * @method SelBranchEmployeeQuery groupByAdmin() Group by the ADMIN column
 * @method SelBranchEmployeeQuery groupByStatus() Group by the STATUS column
 *
 * @method SelBranchEmployeeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelBranchEmployeeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelBranchEmployeeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelBranchEmployeeQuery leftJoinSelEmployee($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelEmployee relation
 * @method SelBranchEmployeeQuery rightJoinSelEmployee($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelEmployee relation
 * @method SelBranchEmployeeQuery innerJoinSelEmployee($relationAlias = null) Adds a INNER JOIN clause to the query using the SelEmployee relation
 *
 * @method SelBranchEmployeeQuery leftJoinSelBranch($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelBranch relation
 * @method SelBranchEmployeeQuery rightJoinSelBranch($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelBranch relation
 * @method SelBranchEmployeeQuery innerJoinSelBranch($relationAlias = null) Adds a INNER JOIN clause to the query using the SelBranch relation
 *
 * @method SelBranchEmployee findOne(PropelPDO $con = null) Return the first SelBranchEmployee matching the query
 * @method SelBranchEmployee findOneOrCreate(PropelPDO $con = null) Return the first SelBranchEmployee matching the query, or a new SelBranchEmployee object populated from the query conditions when no match is found
 *
 * @method SelBranchEmployee findOneByBranchId(int $BRANCH_ID) Return the first SelBranchEmployee filtered by the BRANCH_ID column
 * @method SelBranchEmployee findOneByEmployeeId(int $EMPLOYEE_ID) Return the first SelBranchEmployee filtered by the EMPLOYEE_ID column
 * @method SelBranchEmployee findOneByInitDate(string $INIT_DATE) Return the first SelBranchEmployee filtered by the INIT_DATE column
 * @method SelBranchEmployee findOneByEndDate(string $END_DATE) Return the first SelBranchEmployee filtered by the END_DATE column
 * @method SelBranchEmployee findOneByAdmin(boolean $ADMIN) Return the first SelBranchEmployee filtered by the ADMIN column
 * @method SelBranchEmployee findOneByStatus(string $STATUS) Return the first SelBranchEmployee filtered by the STATUS column
 *
 * @method array findById(int $ID) Return SelBranchEmployee objects filtered by the ID column
 * @method array findByBranchId(int $BRANCH_ID) Return SelBranchEmployee objects filtered by the BRANCH_ID column
 * @method array findByEmployeeId(int $EMPLOYEE_ID) Return SelBranchEmployee objects filtered by the EMPLOYEE_ID column
 * @method array findByInitDate(string $INIT_DATE) Return SelBranchEmployee objects filtered by the INIT_DATE column
 * @method array findByEndDate(string $END_DATE) Return SelBranchEmployee objects filtered by the END_DATE column
 * @method array findByAdmin(boolean $ADMIN) Return SelBranchEmployee objects filtered by the ADMIN column
 * @method array findByStatus(string $STATUS) Return SelBranchEmployee objects filtered by the STATUS column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelBranchEmployeeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelBranchEmployeeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelBranchEmployee', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelBranchEmployeeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelBranchEmployeeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelBranchEmployeeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelBranchEmployeeQuery) {
            return $criteria;
        }
        $query = new SelBranchEmployeeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelBranchEmployee|SelBranchEmployee[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelBranchEmployeePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelBranchEmployeePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelBranchEmployee A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelBranchEmployee A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `BRANCH_ID`, `EMPLOYEE_ID`, `INIT_DATE`, `END_DATE`, `ADMIN`, `STATUS` FROM `sel_branch_employee` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelBranchEmployee();
            $obj->hydrate($row);
            SelBranchEmployeePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelBranchEmployee|SelBranchEmployee[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelBranchEmployee[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelBranchEmployeePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelBranchEmployeePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelBranchEmployeePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the BRANCH_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByBranchId(1234); // WHERE BRANCH_ID = 1234
     * $query->filterByBranchId(array(12, 34)); // WHERE BRANCH_ID IN (12, 34)
     * $query->filterByBranchId(array('min' => 12)); // WHERE BRANCH_ID > 12
     * </code>
     *
     * @see       filterBySelBranch()
     *
     * @param     mixed $branchId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function filterByBranchId($branchId = null, $comparison = null)
    {
        if (is_array($branchId)) {
            $useMinMax = false;
            if (isset($branchId['min'])) {
                $this->addUsingAlias(SelBranchEmployeePeer::BRANCH_ID, $branchId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($branchId['max'])) {
                $this->addUsingAlias(SelBranchEmployeePeer::BRANCH_ID, $branchId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelBranchEmployeePeer::BRANCH_ID, $branchId, $comparison);
    }

    /**
     * Filter the query on the EMPLOYEE_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByEmployeeId(1234); // WHERE EMPLOYEE_ID = 1234
     * $query->filterByEmployeeId(array(12, 34)); // WHERE EMPLOYEE_ID IN (12, 34)
     * $query->filterByEmployeeId(array('min' => 12)); // WHERE EMPLOYEE_ID > 12
     * </code>
     *
     * @see       filterBySelEmployee()
     *
     * @param     mixed $employeeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function filterByEmployeeId($employeeId = null, $comparison = null)
    {
        if (is_array($employeeId)) {
            $useMinMax = false;
            if (isset($employeeId['min'])) {
                $this->addUsingAlias(SelBranchEmployeePeer::EMPLOYEE_ID, $employeeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($employeeId['max'])) {
                $this->addUsingAlias(SelBranchEmployeePeer::EMPLOYEE_ID, $employeeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelBranchEmployeePeer::EMPLOYEE_ID, $employeeId, $comparison);
    }

    /**
     * Filter the query on the INIT_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByInitDate('2011-03-14'); // WHERE INIT_DATE = '2011-03-14'
     * $query->filterByInitDate('now'); // WHERE INIT_DATE = '2011-03-14'
     * $query->filterByInitDate(array('max' => 'yesterday')); // WHERE INIT_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $initDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function filterByInitDate($initDate = null, $comparison = null)
    {
        if (is_array($initDate)) {
            $useMinMax = false;
            if (isset($initDate['min'])) {
                $this->addUsingAlias(SelBranchEmployeePeer::INIT_DATE, $initDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($initDate['max'])) {
                $this->addUsingAlias(SelBranchEmployeePeer::INIT_DATE, $initDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelBranchEmployeePeer::INIT_DATE, $initDate, $comparison);
    }

    /**
     * Filter the query on the END_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByEndDate('2011-03-14'); // WHERE END_DATE = '2011-03-14'
     * $query->filterByEndDate('now'); // WHERE END_DATE = '2011-03-14'
     * $query->filterByEndDate(array('max' => 'yesterday')); // WHERE END_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $endDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function filterByEndDate($endDate = null, $comparison = null)
    {
        if (is_array($endDate)) {
            $useMinMax = false;
            if (isset($endDate['min'])) {
                $this->addUsingAlias(SelBranchEmployeePeer::END_DATE, $endDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endDate['max'])) {
                $this->addUsingAlias(SelBranchEmployeePeer::END_DATE, $endDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelBranchEmployeePeer::END_DATE, $endDate, $comparison);
    }

    /**
     * Filter the query on the ADMIN column
     *
     * Example usage:
     * <code>
     * $query->filterByAdmin(true); // WHERE ADMIN = true
     * $query->filterByAdmin('yes'); // WHERE ADMIN = true
     * </code>
     *
     * @param     boolean|string $admin The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function filterByAdmin($admin = null, $comparison = null)
    {
        if (is_string($admin)) {
            $ADMIN = in_array(strtolower($admin), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SelBranchEmployeePeer::ADMIN, $admin, $comparison);
    }

    /**
     * Filter the query on the STATUS column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE STATUS = 'fooValue'
     * $query->filterByStatus('%fooValue%'); // WHERE STATUS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $status)) {
                $status = str_replace('*', '%', $status);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelBranchEmployeePeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query by a related SelEmployee object
     *
     * @param   SelEmployee|PropelObjectCollection $selEmployee The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelBranchEmployeeQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelEmployee($selEmployee, $comparison = null)
    {
        if ($selEmployee instanceof SelEmployee) {
            return $this
                ->addUsingAlias(SelBranchEmployeePeer::EMPLOYEE_ID, $selEmployee->getId(), $comparison);
        } elseif ($selEmployee instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelBranchEmployeePeer::EMPLOYEE_ID, $selEmployee->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelEmployee() only accepts arguments of type SelEmployee or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelEmployee relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function joinSelEmployee($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelEmployee');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelEmployee');
        }

        return $this;
    }

    /**
     * Use the SelEmployee relation SelEmployee object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelEmployeeQuery A secondary query class using the current class as primary query
     */
    public function useSelEmployeeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelEmployee($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelEmployee', 'SelEmployeeQuery');
    }

    /**
     * Filter the query by a related SelBranch object
     *
     * @param   SelBranch|PropelObjectCollection $selBranch The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelBranchEmployeeQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelBranch($selBranch, $comparison = null)
    {
        if ($selBranch instanceof SelBranch) {
            return $this
                ->addUsingAlias(SelBranchEmployeePeer::BRANCH_ID, $selBranch->getId(), $comparison);
        } elseif ($selBranch instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelBranchEmployeePeer::BRANCH_ID, $selBranch->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelBranch() only accepts arguments of type SelBranch or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelBranch relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function joinSelBranch($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelBranch');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelBranch');
        }

        return $this;
    }

    /**
     * Use the SelBranch relation SelBranch object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelBranchQuery A secondary query class using the current class as primary query
     */
    public function useSelBranchQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelBranch($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelBranch', 'SelBranchQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelBranchEmployee $selBranchEmployee Object to remove from the list of results
     *
     * @return SelBranchEmployeeQuery The current query, for fluid interface
     */
    public function prune($selBranchEmployee = null)
    {
        if ($selBranchEmployee) {
            $this->addUsingAlias(SelBranchEmployeePeer::ID, $selBranchEmployee->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
