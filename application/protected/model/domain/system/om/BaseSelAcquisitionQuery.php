<?php


/**
 * Base class that represents a query for the 'sel_acquisition' table.
 *
 * 
 *
 * @method SelAcquisitionQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelAcquisitionQuery orderByDate($order = Criteria::ASC) Order by the DATE column
 * @method SelAcquisitionQuery orderByProviderId($order = Criteria::ASC) Order by the PROVIDER_ID column
 * @method SelAcquisitionQuery orderByObservation($order = Criteria::ASC) Order by the OBSERVATION column
 * @method SelAcquisitionQuery orderByWarrantyProvider($order = Criteria::ASC) Order by the WARRANTY_PROVIDER column
 *
 * @method SelAcquisitionQuery groupById() Group by the ID column
 * @method SelAcquisitionQuery groupByDate() Group by the DATE column
 * @method SelAcquisitionQuery groupByProviderId() Group by the PROVIDER_ID column
 * @method SelAcquisitionQuery groupByObservation() Group by the OBSERVATION column
 * @method SelAcquisitionQuery groupByWarrantyProvider() Group by the WARRANTY_PROVIDER column
 *
 * @method SelAcquisitionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelAcquisitionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelAcquisitionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelAcquisitionQuery leftJoinSelProvider($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProvider relation
 * @method SelAcquisitionQuery rightJoinSelProvider($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProvider relation
 * @method SelAcquisitionQuery innerJoinSelProvider($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProvider relation
 *
 * @method SelAcquisitionQuery leftJoinSelProductAcquisition($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductAcquisition relation
 * @method SelAcquisitionQuery rightJoinSelProductAcquisition($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductAcquisition relation
 * @method SelAcquisitionQuery innerJoinSelProductAcquisition($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductAcquisition relation
 *
 * @method SelAcquisition findOne(PropelPDO $con = null) Return the first SelAcquisition matching the query
 * @method SelAcquisition findOneOrCreate(PropelPDO $con = null) Return the first SelAcquisition matching the query, or a new SelAcquisition object populated from the query conditions when no match is found
 *
 * @method SelAcquisition findOneByDate(string $DATE) Return the first SelAcquisition filtered by the DATE column
 * @method SelAcquisition findOneByProviderId(int $PROVIDER_ID) Return the first SelAcquisition filtered by the PROVIDER_ID column
 * @method SelAcquisition findOneByObservation(string $OBSERVATION) Return the first SelAcquisition filtered by the OBSERVATION column
 * @method SelAcquisition findOneByWarrantyProvider(string $WARRANTY_PROVIDER) Return the first SelAcquisition filtered by the WARRANTY_PROVIDER column
 *
 * @method array findById(int $ID) Return SelAcquisition objects filtered by the ID column
 * @method array findByDate(string $DATE) Return SelAcquisition objects filtered by the DATE column
 * @method array findByProviderId(int $PROVIDER_ID) Return SelAcquisition objects filtered by the PROVIDER_ID column
 * @method array findByObservation(string $OBSERVATION) Return SelAcquisition objects filtered by the OBSERVATION column
 * @method array findByWarrantyProvider(string $WARRANTY_PROVIDER) Return SelAcquisition objects filtered by the WARRANTY_PROVIDER column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelAcquisitionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelAcquisitionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelAcquisition', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelAcquisitionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelAcquisitionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelAcquisitionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelAcquisitionQuery) {
            return $criteria;
        }
        $query = new SelAcquisitionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelAcquisition|SelAcquisition[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelAcquisitionPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelAcquisitionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelAcquisition A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelAcquisition A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `DATE`, `PROVIDER_ID`, `OBSERVATION`, `WARRANTY_PROVIDER` FROM `sel_acquisition` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelAcquisition();
            $obj->hydrate($row);
            SelAcquisitionPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelAcquisition|SelAcquisition[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelAcquisition[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelAcquisitionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelAcquisitionPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelAcquisitionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelAcquisitionPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelAcquisitionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelAcquisitionPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE DATE = '2011-03-14'
     * $query->filterByDate('now'); // WHERE DATE = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelAcquisitionQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(SelAcquisitionPeer::DATE, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(SelAcquisitionPeer::DATE, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelAcquisitionPeer::DATE, $date, $comparison);
    }

    /**
     * Filter the query on the PROVIDER_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByProviderId(1234); // WHERE PROVIDER_ID = 1234
     * $query->filterByProviderId(array(12, 34)); // WHERE PROVIDER_ID IN (12, 34)
     * $query->filterByProviderId(array('min' => 12)); // WHERE PROVIDER_ID > 12
     * </code>
     *
     * @see       filterBySelProvider()
     *
     * @param     mixed $providerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelAcquisitionQuery The current query, for fluid interface
     */
    public function filterByProviderId($providerId = null, $comparison = null)
    {
        if (is_array($providerId)) {
            $useMinMax = false;
            if (isset($providerId['min'])) {
                $this->addUsingAlias(SelAcquisitionPeer::PROVIDER_ID, $providerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($providerId['max'])) {
                $this->addUsingAlias(SelAcquisitionPeer::PROVIDER_ID, $providerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelAcquisitionPeer::PROVIDER_ID, $providerId, $comparison);
    }

    /**
     * Filter the query on the OBSERVATION column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE OBSERVATION = 'fooValue'
     * $query->filterByObservation('%fooValue%'); // WHERE OBSERVATION LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelAcquisitionQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $observation)) {
                $observation = str_replace('*', '%', $observation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelAcquisitionPeer::OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query on the WARRANTY_PROVIDER column
     *
     * Example usage:
     * <code>
     * $query->filterByWarrantyProvider('fooValue');   // WHERE WARRANTY_PROVIDER = 'fooValue'
     * $query->filterByWarrantyProvider('%fooValue%'); // WHERE WARRANTY_PROVIDER LIKE '%fooValue%'
     * </code>
     *
     * @param     string $warrantyProvider The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelAcquisitionQuery The current query, for fluid interface
     */
    public function filterByWarrantyProvider($warrantyProvider = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($warrantyProvider)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $warrantyProvider)) {
                $warrantyProvider = str_replace('*', '%', $warrantyProvider);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelAcquisitionPeer::WARRANTY_PROVIDER, $warrantyProvider, $comparison);
    }

    /**
     * Filter the query by a related SelProvider object
     *
     * @param   SelProvider|PropelObjectCollection $selProvider The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelAcquisitionQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProvider($selProvider, $comparison = null)
    {
        if ($selProvider instanceof SelProvider) {
            return $this
                ->addUsingAlias(SelAcquisitionPeer::PROVIDER_ID, $selProvider->getId(), $comparison);
        } elseif ($selProvider instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelAcquisitionPeer::PROVIDER_ID, $selProvider->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelProvider() only accepts arguments of type SelProvider or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProvider relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelAcquisitionQuery The current query, for fluid interface
     */
    public function joinSelProvider($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProvider');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProvider');
        }

        return $this;
    }

    /**
     * Use the SelProvider relation SelProvider object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProviderQuery A secondary query class using the current class as primary query
     */
    public function useSelProviderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelProvider($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProvider', 'SelProviderQuery');
    }

    /**
     * Filter the query by a related SelProductAcquisition object
     *
     * @param   SelProductAcquisition|PropelObjectCollection $selProductAcquisition  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelAcquisitionQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductAcquisition($selProductAcquisition, $comparison = null)
    {
        if ($selProductAcquisition instanceof SelProductAcquisition) {
            return $this
                ->addUsingAlias(SelAcquisitionPeer::ID, $selProductAcquisition->getAquisitionId(), $comparison);
        } elseif ($selProductAcquisition instanceof PropelObjectCollection) {
            return $this
                ->useSelProductAcquisitionQuery()
                ->filterByPrimaryKeys($selProductAcquisition->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductAcquisition() only accepts arguments of type SelProductAcquisition or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductAcquisition relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelAcquisitionQuery The current query, for fluid interface
     */
    public function joinSelProductAcquisition($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductAcquisition');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductAcquisition');
        }

        return $this;
    }

    /**
     * Use the SelProductAcquisition relation SelProductAcquisition object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductAcquisitionQuery A secondary query class using the current class as primary query
     */
    public function useSelProductAcquisitionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductAcquisition($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductAcquisition', 'SelProductAcquisitionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelAcquisition $selAcquisition Object to remove from the list of results
     *
     * @return SelAcquisitionQuery The current query, for fluid interface
     */
    public function prune($selAcquisition = null)
    {
        if ($selAcquisition) {
            $this->addUsingAlias(SelAcquisitionPeer::ID, $selAcquisition->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
