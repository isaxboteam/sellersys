<?php


/**
 * Base class that represents a query for the 'sel_product_image' table.
 *
 * 
 *
 * @method SelProductImageQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelProductImageQuery orderByProductId($order = Criteria::ASC) Order by the PRODUCT_ID column
 * @method SelProductImageQuery orderByName($order = Criteria::ASC) Order by the NAME column
 * @method SelProductImageQuery orderBySize($order = Criteria::ASC) Order by the SIZE column
 * @method SelProductImageQuery orderByMimetype($order = Criteria::ASC) Order by the MIMETYPE column
 * @method SelProductImageQuery orderByPosition($order = Criteria::ASC) Order by the POSITION column
 * @method SelProductImageQuery orderByTitle($order = Criteria::ASC) Order by the TITLE column
 * @method SelProductImageQuery orderByDescription($order = Criteria::ASC) Order by the DESCRIPTION column
 *
 * @method SelProductImageQuery groupById() Group by the ID column
 * @method SelProductImageQuery groupByProductId() Group by the PRODUCT_ID column
 * @method SelProductImageQuery groupByName() Group by the NAME column
 * @method SelProductImageQuery groupBySize() Group by the SIZE column
 * @method SelProductImageQuery groupByMimetype() Group by the MIMETYPE column
 * @method SelProductImageQuery groupByPosition() Group by the POSITION column
 * @method SelProductImageQuery groupByTitle() Group by the TITLE column
 * @method SelProductImageQuery groupByDescription() Group by the DESCRIPTION column
 *
 * @method SelProductImageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelProductImageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelProductImageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelProductImageQuery leftJoinSelProductRelatedByProductId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductRelatedByProductId relation
 * @method SelProductImageQuery rightJoinSelProductRelatedByProductId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductRelatedByProductId relation
 * @method SelProductImageQuery innerJoinSelProductRelatedByProductId($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductRelatedByProductId relation
 *
 * @method SelProductImageQuery leftJoinSelProductRelatedByMainImageId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductRelatedByMainImageId relation
 * @method SelProductImageQuery rightJoinSelProductRelatedByMainImageId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductRelatedByMainImageId relation
 * @method SelProductImageQuery innerJoinSelProductRelatedByMainImageId($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductRelatedByMainImageId relation
 *
 * @method SelProductImage findOne(PropelPDO $con = null) Return the first SelProductImage matching the query
 * @method SelProductImage findOneOrCreate(PropelPDO $con = null) Return the first SelProductImage matching the query, or a new SelProductImage object populated from the query conditions when no match is found
 *
 * @method SelProductImage findOneByProductId(int $PRODUCT_ID) Return the first SelProductImage filtered by the PRODUCT_ID column
 * @method SelProductImage findOneByName(string $NAME) Return the first SelProductImage filtered by the NAME column
 * @method SelProductImage findOneBySize(int $SIZE) Return the first SelProductImage filtered by the SIZE column
 * @method SelProductImage findOneByMimetype(string $MIMETYPE) Return the first SelProductImage filtered by the MIMETYPE column
 * @method SelProductImage findOneByPosition(int $POSITION) Return the first SelProductImage filtered by the POSITION column
 * @method SelProductImage findOneByTitle(string $TITLE) Return the first SelProductImage filtered by the TITLE column
 * @method SelProductImage findOneByDescription(string $DESCRIPTION) Return the first SelProductImage filtered by the DESCRIPTION column
 *
 * @method array findById(int $ID) Return SelProductImage objects filtered by the ID column
 * @method array findByProductId(int $PRODUCT_ID) Return SelProductImage objects filtered by the PRODUCT_ID column
 * @method array findByName(string $NAME) Return SelProductImage objects filtered by the NAME column
 * @method array findBySize(int $SIZE) Return SelProductImage objects filtered by the SIZE column
 * @method array findByMimetype(string $MIMETYPE) Return SelProductImage objects filtered by the MIMETYPE column
 * @method array findByPosition(int $POSITION) Return SelProductImage objects filtered by the POSITION column
 * @method array findByTitle(string $TITLE) Return SelProductImage objects filtered by the TITLE column
 * @method array findByDescription(string $DESCRIPTION) Return SelProductImage objects filtered by the DESCRIPTION column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProductImageQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelProductImageQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelProductImage', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelProductImageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelProductImageQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelProductImageQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelProductImageQuery) {
            return $criteria;
        }
        $query = new SelProductImageQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelProductImage|SelProductImage[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelProductImagePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelProductImagePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductImage A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductImage A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `PRODUCT_ID`, `NAME`, `SIZE`, `MIMETYPE`, `POSITION`, `TITLE`, `DESCRIPTION` FROM `sel_product_image` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelProductImage();
            $obj->hydrate($row);
            SelProductImagePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelProductImage|SelProductImage[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelProductImage[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelProductImagePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelProductImagePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelProductImagePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the PRODUCT_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByProductId(1234); // WHERE PRODUCT_ID = 1234
     * $query->filterByProductId(array(12, 34)); // WHERE PRODUCT_ID IN (12, 34)
     * $query->filterByProductId(array('min' => 12)); // WHERE PRODUCT_ID > 12
     * </code>
     *
     * @see       filterBySelProductRelatedByProductId()
     *
     * @param     mixed $productId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function filterByProductId($productId = null, $comparison = null)
    {
        if (is_array($productId)) {
            $useMinMax = false;
            if (isset($productId['min'])) {
                $this->addUsingAlias(SelProductImagePeer::PRODUCT_ID, $productId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productId['max'])) {
                $this->addUsingAlias(SelProductImagePeer::PRODUCT_ID, $productId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductImagePeer::PRODUCT_ID, $productId, $comparison);
    }

    /**
     * Filter the query on the NAME column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE NAME = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE NAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductImagePeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the SIZE column
     *
     * Example usage:
     * <code>
     * $query->filterBySize(1234); // WHERE SIZE = 1234
     * $query->filterBySize(array(12, 34)); // WHERE SIZE IN (12, 34)
     * $query->filterBySize(array('min' => 12)); // WHERE SIZE > 12
     * </code>
     *
     * @param     mixed $size The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function filterBySize($size = null, $comparison = null)
    {
        if (is_array($size)) {
            $useMinMax = false;
            if (isset($size['min'])) {
                $this->addUsingAlias(SelProductImagePeer::SIZE, $size['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($size['max'])) {
                $this->addUsingAlias(SelProductImagePeer::SIZE, $size['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductImagePeer::SIZE, $size, $comparison);
    }

    /**
     * Filter the query on the MIMETYPE column
     *
     * Example usage:
     * <code>
     * $query->filterByMimetype('fooValue');   // WHERE MIMETYPE = 'fooValue'
     * $query->filterByMimetype('%fooValue%'); // WHERE MIMETYPE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mimetype The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function filterByMimetype($mimetype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mimetype)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mimetype)) {
                $mimetype = str_replace('*', '%', $mimetype);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductImagePeer::MIMETYPE, $mimetype, $comparison);
    }

    /**
     * Filter the query on the POSITION column
     *
     * Example usage:
     * <code>
     * $query->filterByPosition(1234); // WHERE POSITION = 1234
     * $query->filterByPosition(array(12, 34)); // WHERE POSITION IN (12, 34)
     * $query->filterByPosition(array('min' => 12)); // WHERE POSITION > 12
     * </code>
     *
     * @param     mixed $position The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function filterByPosition($position = null, $comparison = null)
    {
        if (is_array($position)) {
            $useMinMax = false;
            if (isset($position['min'])) {
                $this->addUsingAlias(SelProductImagePeer::POSITION, $position['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($position['max'])) {
                $this->addUsingAlias(SelProductImagePeer::POSITION, $position['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductImagePeer::POSITION, $position, $comparison);
    }

    /**
     * Filter the query on the TITLE column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE TITLE = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE TITLE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductImagePeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the DESCRIPTION column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE DESCRIPTION = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE DESCRIPTION LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductImagePeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query by a related SelProduct object
     *
     * @param   SelProduct|PropelObjectCollection $selProduct The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductImageQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductRelatedByProductId($selProduct, $comparison = null)
    {
        if ($selProduct instanceof SelProduct) {
            return $this
                ->addUsingAlias(SelProductImagePeer::PRODUCT_ID, $selProduct->getId(), $comparison);
        } elseif ($selProduct instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductImagePeer::PRODUCT_ID, $selProduct->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelProductRelatedByProductId() only accepts arguments of type SelProduct or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductRelatedByProductId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function joinSelProductRelatedByProductId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductRelatedByProductId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductRelatedByProductId');
        }

        return $this;
    }

    /**
     * Use the SelProductRelatedByProductId relation SelProduct object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductQuery A secondary query class using the current class as primary query
     */
    public function useSelProductRelatedByProductIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductRelatedByProductId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductRelatedByProductId', 'SelProductQuery');
    }

    /**
     * Filter the query by a related SelProduct object
     *
     * @param   SelProduct|PropelObjectCollection $selProduct  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductImageQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductRelatedByMainImageId($selProduct, $comparison = null)
    {
        if ($selProduct instanceof SelProduct) {
            return $this
                ->addUsingAlias(SelProductImagePeer::ID, $selProduct->getMainImageId(), $comparison);
        } elseif ($selProduct instanceof PropelObjectCollection) {
            return $this
                ->useSelProductRelatedByMainImageIdQuery()
                ->filterByPrimaryKeys($selProduct->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductRelatedByMainImageId() only accepts arguments of type SelProduct or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductRelatedByMainImageId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function joinSelProductRelatedByMainImageId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductRelatedByMainImageId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductRelatedByMainImageId');
        }

        return $this;
    }

    /**
     * Use the SelProductRelatedByMainImageId relation SelProduct object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductQuery A secondary query class using the current class as primary query
     */
    public function useSelProductRelatedByMainImageIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelProductRelatedByMainImageId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductRelatedByMainImageId', 'SelProductQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelProductImage $selProductImage Object to remove from the list of results
     *
     * @return SelProductImageQuery The current query, for fluid interface
     */
    public function prune($selProductImage = null)
    {
        if ($selProductImage) {
            $this->addUsingAlias(SelProductImagePeer::ID, $selProductImage->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
