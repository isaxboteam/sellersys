<?php


/**
 * Base class that represents a query for the 'sel_product_sell' table.
 *
 * 
 *
 * @method SelProductSellQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelProductSellQuery orderByProductId($order = Criteria::ASC) Order by the PRODUCT_ID column
 * @method SelProductSellQuery orderBySaleId($order = Criteria::ASC) Order by the SALE_ID column
 * @method SelProductSellQuery orderByQuantity($order = Criteria::ASC) Order by the QUANTITY column
 * @method SelProductSellQuery orderByPrice($order = Criteria::ASC) Order by the PRICE column
 * @method SelProductSellQuery orderByAddedDate($order = Criteria::ASC) Order by the ADDED_DATE column
 *
 * @method SelProductSellQuery groupById() Group by the ID column
 * @method SelProductSellQuery groupByProductId() Group by the PRODUCT_ID column
 * @method SelProductSellQuery groupBySaleId() Group by the SALE_ID column
 * @method SelProductSellQuery groupByQuantity() Group by the QUANTITY column
 * @method SelProductSellQuery groupByPrice() Group by the PRICE column
 * @method SelProductSellQuery groupByAddedDate() Group by the ADDED_DATE column
 *
 * @method SelProductSellQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelProductSellQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelProductSellQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelProductSellQuery leftJoinSelProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProduct relation
 * @method SelProductSellQuery rightJoinSelProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProduct relation
 * @method SelProductSellQuery innerJoinSelProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProduct relation
 *
 * @method SelProductSellQuery leftJoinSelSale($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelSale relation
 * @method SelProductSellQuery rightJoinSelSale($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelSale relation
 * @method SelProductSellQuery innerJoinSelSale($relationAlias = null) Adds a INNER JOIN clause to the query using the SelSale relation
 *
 * @method SelProductSellQuery leftJoinSelProductItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductItem relation
 * @method SelProductSellQuery rightJoinSelProductItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductItem relation
 * @method SelProductSellQuery innerJoinSelProductItem($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductItem relation
 *
 * @method SelProductSell findOne(PropelPDO $con = null) Return the first SelProductSell matching the query
 * @method SelProductSell findOneOrCreate(PropelPDO $con = null) Return the first SelProductSell matching the query, or a new SelProductSell object populated from the query conditions when no match is found
 *
 * @method SelProductSell findOneByProductId(int $PRODUCT_ID) Return the first SelProductSell filtered by the PRODUCT_ID column
 * @method SelProductSell findOneBySaleId(int $SALE_ID) Return the first SelProductSell filtered by the SALE_ID column
 * @method SelProductSell findOneByQuantity(int $QUANTITY) Return the first SelProductSell filtered by the QUANTITY column
 * @method SelProductSell findOneByPrice(double $PRICE) Return the first SelProductSell filtered by the PRICE column
 * @method SelProductSell findOneByAddedDate(string $ADDED_DATE) Return the first SelProductSell filtered by the ADDED_DATE column
 *
 * @method array findById(int $ID) Return SelProductSell objects filtered by the ID column
 * @method array findByProductId(int $PRODUCT_ID) Return SelProductSell objects filtered by the PRODUCT_ID column
 * @method array findBySaleId(int $SALE_ID) Return SelProductSell objects filtered by the SALE_ID column
 * @method array findByQuantity(int $QUANTITY) Return SelProductSell objects filtered by the QUANTITY column
 * @method array findByPrice(double $PRICE) Return SelProductSell objects filtered by the PRICE column
 * @method array findByAddedDate(string $ADDED_DATE) Return SelProductSell objects filtered by the ADDED_DATE column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProductSellQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelProductSellQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelProductSell', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelProductSellQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelProductSellQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelProductSellQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelProductSellQuery) {
            return $criteria;
        }
        $query = new SelProductSellQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelProductSell|SelProductSell[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelProductSellPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelProductSellPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductSell A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductSell A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `PRODUCT_ID`, `SALE_ID`, `QUANTITY`, `PRICE`, `ADDED_DATE` FROM `sel_product_sell` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelProductSell();
            $obj->hydrate($row);
            SelProductSellPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelProductSell|SelProductSell[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelProductSell[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelProductSellPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelProductSellPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelProductSellPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the PRODUCT_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByProductId(1234); // WHERE PRODUCT_ID = 1234
     * $query->filterByProductId(array(12, 34)); // WHERE PRODUCT_ID IN (12, 34)
     * $query->filterByProductId(array('min' => 12)); // WHERE PRODUCT_ID > 12
     * </code>
     *
     * @see       filterBySelProduct()
     *
     * @param     mixed $productId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function filterByProductId($productId = null, $comparison = null)
    {
        if (is_array($productId)) {
            $useMinMax = false;
            if (isset($productId['min'])) {
                $this->addUsingAlias(SelProductSellPeer::PRODUCT_ID, $productId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productId['max'])) {
                $this->addUsingAlias(SelProductSellPeer::PRODUCT_ID, $productId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductSellPeer::PRODUCT_ID, $productId, $comparison);
    }

    /**
     * Filter the query on the SALE_ID column
     *
     * Example usage:
     * <code>
     * $query->filterBySaleId(1234); // WHERE SALE_ID = 1234
     * $query->filterBySaleId(array(12, 34)); // WHERE SALE_ID IN (12, 34)
     * $query->filterBySaleId(array('min' => 12)); // WHERE SALE_ID > 12
     * </code>
     *
     * @see       filterBySelSale()
     *
     * @param     mixed $saleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function filterBySaleId($saleId = null, $comparison = null)
    {
        if (is_array($saleId)) {
            $useMinMax = false;
            if (isset($saleId['min'])) {
                $this->addUsingAlias(SelProductSellPeer::SALE_ID, $saleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($saleId['max'])) {
                $this->addUsingAlias(SelProductSellPeer::SALE_ID, $saleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductSellPeer::SALE_ID, $saleId, $comparison);
    }

    /**
     * Filter the query on the QUANTITY column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantity(1234); // WHERE QUANTITY = 1234
     * $query->filterByQuantity(array(12, 34)); // WHERE QUANTITY IN (12, 34)
     * $query->filterByQuantity(array('min' => 12)); // WHERE QUANTITY > 12
     * </code>
     *
     * @param     mixed $quantity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function filterByQuantity($quantity = null, $comparison = null)
    {
        if (is_array($quantity)) {
            $useMinMax = false;
            if (isset($quantity['min'])) {
                $this->addUsingAlias(SelProductSellPeer::QUANTITY, $quantity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantity['max'])) {
                $this->addUsingAlias(SelProductSellPeer::QUANTITY, $quantity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductSellPeer::QUANTITY, $quantity, $comparison);
    }

    /**
     * Filter the query on the PRICE column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE PRICE = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE PRICE IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE PRICE > 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(SelProductSellPeer::PRICE, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(SelProductSellPeer::PRICE, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductSellPeer::PRICE, $price, $comparison);
    }

    /**
     * Filter the query on the ADDED_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByAddedDate('2011-03-14'); // WHERE ADDED_DATE = '2011-03-14'
     * $query->filterByAddedDate('now'); // WHERE ADDED_DATE = '2011-03-14'
     * $query->filterByAddedDate(array('max' => 'yesterday')); // WHERE ADDED_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $addedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function filterByAddedDate($addedDate = null, $comparison = null)
    {
        if (is_array($addedDate)) {
            $useMinMax = false;
            if (isset($addedDate['min'])) {
                $this->addUsingAlias(SelProductSellPeer::ADDED_DATE, $addedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($addedDate['max'])) {
                $this->addUsingAlias(SelProductSellPeer::ADDED_DATE, $addedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductSellPeer::ADDED_DATE, $addedDate, $comparison);
    }

    /**
     * Filter the query by a related SelProduct object
     *
     * @param   SelProduct|PropelObjectCollection $selProduct The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductSellQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProduct($selProduct, $comparison = null)
    {
        if ($selProduct instanceof SelProduct) {
            return $this
                ->addUsingAlias(SelProductSellPeer::PRODUCT_ID, $selProduct->getId(), $comparison);
        } elseif ($selProduct instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductSellPeer::PRODUCT_ID, $selProduct->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelProduct() only accepts arguments of type SelProduct or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProduct relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function joinSelProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProduct');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProduct');
        }

        return $this;
    }

    /**
     * Use the SelProduct relation SelProduct object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductQuery A secondary query class using the current class as primary query
     */
    public function useSelProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProduct', 'SelProductQuery');
    }

    /**
     * Filter the query by a related SelSale object
     *
     * @param   SelSale|PropelObjectCollection $selSale The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductSellQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelSale($selSale, $comparison = null)
    {
        if ($selSale instanceof SelSale) {
            return $this
                ->addUsingAlias(SelProductSellPeer::SALE_ID, $selSale->getId(), $comparison);
        } elseif ($selSale instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductSellPeer::SALE_ID, $selSale->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelSale() only accepts arguments of type SelSale or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelSale relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function joinSelSale($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelSale');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelSale');
        }

        return $this;
    }

    /**
     * Use the SelSale relation SelSale object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelSaleQuery A secondary query class using the current class as primary query
     */
    public function useSelSaleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelSale($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelSale', 'SelSaleQuery');
    }

    /**
     * Filter the query by a related SelProductItem object
     *
     * @param   SelProductItem|PropelObjectCollection $selProductItem  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductSellQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductItem($selProductItem, $comparison = null)
    {
        if ($selProductItem instanceof SelProductItem) {
            return $this
                ->addUsingAlias(SelProductSellPeer::ID, $selProductItem->getProductSellId(), $comparison);
        } elseif ($selProductItem instanceof PropelObjectCollection) {
            return $this
                ->useSelProductItemQuery()
                ->filterByPrimaryKeys($selProductItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductItem() only accepts arguments of type SelProductItem or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function joinSelProductItem($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductItem');
        }

        return $this;
    }

    /**
     * Use the SelProductItem relation SelProductItem object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductItemQuery A secondary query class using the current class as primary query
     */
    public function useSelProductItemQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelProductItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductItem', 'SelProductItemQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelProductSell $selProductSell Object to remove from the list of results
     *
     * @return SelProductSellQuery The current query, for fluid interface
     */
    public function prune($selProductSell = null)
    {
        if ($selProductSell) {
            $this->addUsingAlias(SelProductSellPeer::ID, $selProductSell->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
