<?php


/**
 * Base class that represents a query for the 'sel_provider' table.
 *
 * 
 *
 * @method SelProviderQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelProviderQuery orderByPersonId($order = Criteria::ASC) Order by the PERSON_ID column
 * @method SelProviderQuery orderByCompany($order = Criteria::ASC) Order by the COMPANY column
 * @method SelProviderQuery orderByAddress($order = Criteria::ASC) Order by the ADDRESS column
 * @method SelProviderQuery orderByPerson($order = Criteria::ASC) Order by the PERSON column
 * @method SelProviderQuery orderByPhone($order = Criteria::ASC) Order by the PHONE column
 * @method SelProviderQuery orderByCellphone($order = Criteria::ASC) Order by the CELLPHONE column
 * @method SelProviderQuery orderByObservations($order = Criteria::ASC) Order by the OBSERVATIONS column
 *
 * @method SelProviderQuery groupById() Group by the ID column
 * @method SelProviderQuery groupByPersonId() Group by the PERSON_ID column
 * @method SelProviderQuery groupByCompany() Group by the COMPANY column
 * @method SelProviderQuery groupByAddress() Group by the ADDRESS column
 * @method SelProviderQuery groupByPerson() Group by the PERSON column
 * @method SelProviderQuery groupByPhone() Group by the PHONE column
 * @method SelProviderQuery groupByCellphone() Group by the CELLPHONE column
 * @method SelProviderQuery groupByObservations() Group by the OBSERVATIONS column
 *
 * @method SelProviderQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelProviderQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelProviderQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelProviderQuery leftJoinSelPerson($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelPerson relation
 * @method SelProviderQuery rightJoinSelPerson($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelPerson relation
 * @method SelProviderQuery innerJoinSelPerson($relationAlias = null) Adds a INNER JOIN clause to the query using the SelPerson relation
 *
 * @method SelProviderQuery leftJoinSelAcquisition($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelAcquisition relation
 * @method SelProviderQuery rightJoinSelAcquisition($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelAcquisition relation
 * @method SelProviderQuery innerJoinSelAcquisition($relationAlias = null) Adds a INNER JOIN clause to the query using the SelAcquisition relation
 *
 * @method SelProvider findOne(PropelPDO $con = null) Return the first SelProvider matching the query
 * @method SelProvider findOneOrCreate(PropelPDO $con = null) Return the first SelProvider matching the query, or a new SelProvider object populated from the query conditions when no match is found
 *
 * @method SelProvider findOneByPersonId(int $PERSON_ID) Return the first SelProvider filtered by the PERSON_ID column
 * @method SelProvider findOneByCompany(string $COMPANY) Return the first SelProvider filtered by the COMPANY column
 * @method SelProvider findOneByAddress(string $ADDRESS) Return the first SelProvider filtered by the ADDRESS column
 * @method SelProvider findOneByPerson(string $PERSON) Return the first SelProvider filtered by the PERSON column
 * @method SelProvider findOneByPhone(string $PHONE) Return the first SelProvider filtered by the PHONE column
 * @method SelProvider findOneByCellphone(string $CELLPHONE) Return the first SelProvider filtered by the CELLPHONE column
 * @method SelProvider findOneByObservations(string $OBSERVATIONS) Return the first SelProvider filtered by the OBSERVATIONS column
 *
 * @method array findById(int $ID) Return SelProvider objects filtered by the ID column
 * @method array findByPersonId(int $PERSON_ID) Return SelProvider objects filtered by the PERSON_ID column
 * @method array findByCompany(string $COMPANY) Return SelProvider objects filtered by the COMPANY column
 * @method array findByAddress(string $ADDRESS) Return SelProvider objects filtered by the ADDRESS column
 * @method array findByPerson(string $PERSON) Return SelProvider objects filtered by the PERSON column
 * @method array findByPhone(string $PHONE) Return SelProvider objects filtered by the PHONE column
 * @method array findByCellphone(string $CELLPHONE) Return SelProvider objects filtered by the CELLPHONE column
 * @method array findByObservations(string $OBSERVATIONS) Return SelProvider objects filtered by the OBSERVATIONS column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProviderQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelProviderQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelProvider', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelProviderQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelProviderQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelProviderQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelProviderQuery) {
            return $criteria;
        }
        $query = new SelProviderQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelProvider|SelProvider[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelProviderPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelProviderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProvider A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProvider A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `PERSON_ID`, `COMPANY`, `ADDRESS`, `PERSON`, `PHONE`, `CELLPHONE`, `OBSERVATIONS` FROM `sel_provider` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelProvider();
            $obj->hydrate($row);
            SelProviderPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelProvider|SelProvider[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelProvider[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelProviderPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelProviderPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelProviderPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the PERSON_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonId(1234); // WHERE PERSON_ID = 1234
     * $query->filterByPersonId(array(12, 34)); // WHERE PERSON_ID IN (12, 34)
     * $query->filterByPersonId(array('min' => 12)); // WHERE PERSON_ID > 12
     * </code>
     *
     * @see       filterBySelPerson()
     *
     * @param     mixed $personId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function filterByPersonId($personId = null, $comparison = null)
    {
        if (is_array($personId)) {
            $useMinMax = false;
            if (isset($personId['min'])) {
                $this->addUsingAlias(SelProviderPeer::PERSON_ID, $personId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personId['max'])) {
                $this->addUsingAlias(SelProviderPeer::PERSON_ID, $personId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProviderPeer::PERSON_ID, $personId, $comparison);
    }

    /**
     * Filter the query on the COMPANY column
     *
     * Example usage:
     * <code>
     * $query->filterByCompany('fooValue');   // WHERE COMPANY = 'fooValue'
     * $query->filterByCompany('%fooValue%'); // WHERE COMPANY LIKE '%fooValue%'
     * </code>
     *
     * @param     string $company The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function filterByCompany($company = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($company)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $company)) {
                $company = str_replace('*', '%', $company);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProviderPeer::COMPANY, $company, $comparison);
    }

    /**
     * Filter the query on the ADDRESS column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE ADDRESS = 'fooValue'
     * $query->filterByAddress('%fooValue%'); // WHERE ADDRESS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $address)) {
                $address = str_replace('*', '%', $address);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProviderPeer::ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the PERSON column
     *
     * Example usage:
     * <code>
     * $query->filterByPerson('fooValue');   // WHERE PERSON = 'fooValue'
     * $query->filterByPerson('%fooValue%'); // WHERE PERSON LIKE '%fooValue%'
     * </code>
     *
     * @param     string $person The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function filterByPerson($person = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($person)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $person)) {
                $person = str_replace('*', '%', $person);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProviderPeer::PERSON, $person, $comparison);
    }

    /**
     * Filter the query on the PHONE column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE PHONE = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE PHONE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProviderPeer::PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the CELLPHONE column
     *
     * Example usage:
     * <code>
     * $query->filterByCellphone('fooValue');   // WHERE CELLPHONE = 'fooValue'
     * $query->filterByCellphone('%fooValue%'); // WHERE CELLPHONE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cellphone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function filterByCellphone($cellphone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cellphone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cellphone)) {
                $cellphone = str_replace('*', '%', $cellphone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProviderPeer::CELLPHONE, $cellphone, $comparison);
    }

    /**
     * Filter the query on the OBSERVATIONS column
     *
     * Example usage:
     * <code>
     * $query->filterByObservations('fooValue');   // WHERE OBSERVATIONS = 'fooValue'
     * $query->filterByObservations('%fooValue%'); // WHERE OBSERVATIONS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observations The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function filterByObservations($observations = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observations)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $observations)) {
                $observations = str_replace('*', '%', $observations);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProviderPeer::OBSERVATIONS, $observations, $comparison);
    }

    /**
     * Filter the query by a related SelPerson object
     *
     * @param   SelPerson|PropelObjectCollection $selPerson The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProviderQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelPerson($selPerson, $comparison = null)
    {
        if ($selPerson instanceof SelPerson) {
            return $this
                ->addUsingAlias(SelProviderPeer::PERSON_ID, $selPerson->getId(), $comparison);
        } elseif ($selPerson instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProviderPeer::PERSON_ID, $selPerson->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelPerson() only accepts arguments of type SelPerson or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelPerson relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function joinSelPerson($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelPerson');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelPerson');
        }

        return $this;
    }

    /**
     * Use the SelPerson relation SelPerson object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelPersonQuery A secondary query class using the current class as primary query
     */
    public function useSelPersonQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelPerson($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelPerson', 'SelPersonQuery');
    }

    /**
     * Filter the query by a related SelAcquisition object
     *
     * @param   SelAcquisition|PropelObjectCollection $selAcquisition  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProviderQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelAcquisition($selAcquisition, $comparison = null)
    {
        if ($selAcquisition instanceof SelAcquisition) {
            return $this
                ->addUsingAlias(SelProviderPeer::ID, $selAcquisition->getProviderId(), $comparison);
        } elseif ($selAcquisition instanceof PropelObjectCollection) {
            return $this
                ->useSelAcquisitionQuery()
                ->filterByPrimaryKeys($selAcquisition->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelAcquisition() only accepts arguments of type SelAcquisition or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelAcquisition relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function joinSelAcquisition($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelAcquisition');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelAcquisition');
        }

        return $this;
    }

    /**
     * Use the SelAcquisition relation SelAcquisition object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelAcquisitionQuery A secondary query class using the current class as primary query
     */
    public function useSelAcquisitionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelAcquisition($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelAcquisition', 'SelAcquisitionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelProvider $selProvider Object to remove from the list of results
     *
     * @return SelProviderQuery The current query, for fluid interface
     */
    public function prune($selProvider = null)
    {
        if ($selProvider) {
            $this->addUsingAlias(SelProviderPeer::ID, $selProvider->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
