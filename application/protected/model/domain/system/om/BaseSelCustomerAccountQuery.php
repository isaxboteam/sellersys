<?php


/**
 * Base class that represents a query for the 'sel_customer_account' table.
 *
 * 
 *
 * @method SelCustomerAccountQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelCustomerAccountQuery orderByEmail($order = Criteria::ASC) Order by the EMAIL column
 * @method SelCustomerAccountQuery orderByPassword($order = Criteria::ASC) Order by the PASSWORD column
 * @method SelCustomerAccountQuery orderByLastAccess($order = Criteria::ASC) Order by the LAST_ACCESS column
 * @method SelCustomerAccountQuery orderByCurrentAccess($order = Criteria::ASC) Order by the CURRENT_ACCESS column
 * @method SelCustomerAccountQuery orderByCreationDate($order = Criteria::ASC) Order by the CREATION_DATE column
 *
 * @method SelCustomerAccountQuery groupById() Group by the ID column
 * @method SelCustomerAccountQuery groupByEmail() Group by the EMAIL column
 * @method SelCustomerAccountQuery groupByPassword() Group by the PASSWORD column
 * @method SelCustomerAccountQuery groupByLastAccess() Group by the LAST_ACCESS column
 * @method SelCustomerAccountQuery groupByCurrentAccess() Group by the CURRENT_ACCESS column
 * @method SelCustomerAccountQuery groupByCreationDate() Group by the CREATION_DATE column
 *
 * @method SelCustomerAccountQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelCustomerAccountQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelCustomerAccountQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelCustomerAccountQuery leftJoinSelCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelCustomer relation
 * @method SelCustomerAccountQuery rightJoinSelCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelCustomer relation
 * @method SelCustomerAccountQuery innerJoinSelCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the SelCustomer relation
 *
 * @method SelCustomerAccount findOne(PropelPDO $con = null) Return the first SelCustomerAccount matching the query
 * @method SelCustomerAccount findOneOrCreate(PropelPDO $con = null) Return the first SelCustomerAccount matching the query, or a new SelCustomerAccount object populated from the query conditions when no match is found
 *
 * @method SelCustomerAccount findOneByEmail(string $EMAIL) Return the first SelCustomerAccount filtered by the EMAIL column
 * @method SelCustomerAccount findOneByPassword(string $PASSWORD) Return the first SelCustomerAccount filtered by the PASSWORD column
 * @method SelCustomerAccount findOneByLastAccess(string $LAST_ACCESS) Return the first SelCustomerAccount filtered by the LAST_ACCESS column
 * @method SelCustomerAccount findOneByCurrentAccess(string $CURRENT_ACCESS) Return the first SelCustomerAccount filtered by the CURRENT_ACCESS column
 * @method SelCustomerAccount findOneByCreationDate(string $CREATION_DATE) Return the first SelCustomerAccount filtered by the CREATION_DATE column
 *
 * @method array findById(int $ID) Return SelCustomerAccount objects filtered by the ID column
 * @method array findByEmail(string $EMAIL) Return SelCustomerAccount objects filtered by the EMAIL column
 * @method array findByPassword(string $PASSWORD) Return SelCustomerAccount objects filtered by the PASSWORD column
 * @method array findByLastAccess(string $LAST_ACCESS) Return SelCustomerAccount objects filtered by the LAST_ACCESS column
 * @method array findByCurrentAccess(string $CURRENT_ACCESS) Return SelCustomerAccount objects filtered by the CURRENT_ACCESS column
 * @method array findByCreationDate(string $CREATION_DATE) Return SelCustomerAccount objects filtered by the CREATION_DATE column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelCustomerAccountQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelCustomerAccountQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelCustomerAccount', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelCustomerAccountQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelCustomerAccountQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelCustomerAccountQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelCustomerAccountQuery) {
            return $criteria;
        }
        $query = new SelCustomerAccountQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelCustomerAccount|SelCustomerAccount[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelCustomerAccountPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelCustomerAccountPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelCustomerAccount A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelCustomerAccount A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `EMAIL`, `PASSWORD`, `LAST_ACCESS`, `CURRENT_ACCESS`, `CREATION_DATE` FROM `sel_customer_account` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelCustomerAccount();
            $obj->hydrate($row);
            SelCustomerAccountPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelCustomerAccount|SelCustomerAccount[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelCustomerAccount[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelCustomerAccountQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelCustomerAccountPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelCustomerAccountQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelCustomerAccountPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerAccountQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelCustomerAccountPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the EMAIL column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE EMAIL = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE EMAIL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerAccountQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCustomerAccountPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the PASSWORD column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE PASSWORD = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE PASSWORD LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerAccountQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCustomerAccountPeer::PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the LAST_ACCESS column
     *
     * Example usage:
     * <code>
     * $query->filterByLastAccess('2011-03-14'); // WHERE LAST_ACCESS = '2011-03-14'
     * $query->filterByLastAccess('now'); // WHERE LAST_ACCESS = '2011-03-14'
     * $query->filterByLastAccess(array('max' => 'yesterday')); // WHERE LAST_ACCESS > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastAccess The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerAccountQuery The current query, for fluid interface
     */
    public function filterByLastAccess($lastAccess = null, $comparison = null)
    {
        if (is_array($lastAccess)) {
            $useMinMax = false;
            if (isset($lastAccess['min'])) {
                $this->addUsingAlias(SelCustomerAccountPeer::LAST_ACCESS, $lastAccess['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastAccess['max'])) {
                $this->addUsingAlias(SelCustomerAccountPeer::LAST_ACCESS, $lastAccess['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelCustomerAccountPeer::LAST_ACCESS, $lastAccess, $comparison);
    }

    /**
     * Filter the query on the CURRENT_ACCESS column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrentAccess('2011-03-14'); // WHERE CURRENT_ACCESS = '2011-03-14'
     * $query->filterByCurrentAccess('now'); // WHERE CURRENT_ACCESS = '2011-03-14'
     * $query->filterByCurrentAccess(array('max' => 'yesterday')); // WHERE CURRENT_ACCESS > '2011-03-13'
     * </code>
     *
     * @param     mixed $currentAccess The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerAccountQuery The current query, for fluid interface
     */
    public function filterByCurrentAccess($currentAccess = null, $comparison = null)
    {
        if (is_array($currentAccess)) {
            $useMinMax = false;
            if (isset($currentAccess['min'])) {
                $this->addUsingAlias(SelCustomerAccountPeer::CURRENT_ACCESS, $currentAccess['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($currentAccess['max'])) {
                $this->addUsingAlias(SelCustomerAccountPeer::CURRENT_ACCESS, $currentAccess['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelCustomerAccountPeer::CURRENT_ACCESS, $currentAccess, $comparison);
    }

    /**
     * Filter the query on the CREATION_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByCreationDate('2011-03-14'); // WHERE CREATION_DATE = '2011-03-14'
     * $query->filterByCreationDate('now'); // WHERE CREATION_DATE = '2011-03-14'
     * $query->filterByCreationDate(array('max' => 'yesterday')); // WHERE CREATION_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $creationDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerAccountQuery The current query, for fluid interface
     */
    public function filterByCreationDate($creationDate = null, $comparison = null)
    {
        if (is_array($creationDate)) {
            $useMinMax = false;
            if (isset($creationDate['min'])) {
                $this->addUsingAlias(SelCustomerAccountPeer::CREATION_DATE, $creationDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($creationDate['max'])) {
                $this->addUsingAlias(SelCustomerAccountPeer::CREATION_DATE, $creationDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelCustomerAccountPeer::CREATION_DATE, $creationDate, $comparison);
    }

    /**
     * Filter the query by a related SelCustomer object
     *
     * @param   SelCustomer|PropelObjectCollection $selCustomer  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelCustomerAccountQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelCustomer($selCustomer, $comparison = null)
    {
        if ($selCustomer instanceof SelCustomer) {
            return $this
                ->addUsingAlias(SelCustomerAccountPeer::ID, $selCustomer->getCustomerAccountId(), $comparison);
        } elseif ($selCustomer instanceof PropelObjectCollection) {
            return $this
                ->useSelCustomerQuery()
                ->filterByPrimaryKeys($selCustomer->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelCustomer() only accepts arguments of type SelCustomer or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelCustomer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelCustomerAccountQuery The current query, for fluid interface
     */
    public function joinSelCustomer($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelCustomer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelCustomer');
        }

        return $this;
    }

    /**
     * Use the SelCustomer relation SelCustomer object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelCustomerQuery A secondary query class using the current class as primary query
     */
    public function useSelCustomerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelCustomer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelCustomer', 'SelCustomerQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelCustomerAccount $selCustomerAccount Object to remove from the list of results
     *
     * @return SelCustomerAccountQuery The current query, for fluid interface
     */
    public function prune($selCustomerAccount = null)
    {
        if ($selCustomerAccount) {
            $this->addUsingAlias(SelCustomerAccountPeer::ID, $selCustomerAccount->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
