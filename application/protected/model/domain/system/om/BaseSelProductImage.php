<?php


/**
 * Base class that represents a row from the 'sel_product_image' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProductImage extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelProductImagePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelProductImagePeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the product_id field.
     * @var        int
     */
    protected $product_id;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the size field.
     * @var        int
     */
    protected $size;

    /**
     * The value for the mimetype field.
     * @var        string
     */
    protected $mimetype;

    /**
     * The value for the position field.
     * @var        int
     */
    protected $position;

    /**
     * The value for the title field.
     * @var        string
     */
    protected $title;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * @var        SelProduct
     */
    protected $aSelProductRelatedByProductId;

    /**
     * @var        PropelObjectCollection|SelProduct[] Collection to store aggregation of SelProduct objects.
     */
    protected $collSelProductsRelatedByMainImageId;
    protected $collSelProductsRelatedByMainImageIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductsRelatedByMainImageIdScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [product_id] column value.
     * 
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * Get the [name] column value.
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [size] column value.
     * 
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Get the [mimetype] column value.
     * 
     * @return string
     */
    public function getMimetype()
    {
        return $this->mimetype;
    }

    /**
     * Get the [position] column value.
     * 
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Get the [title] column value.
     * 
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [description] column value.
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelProductImage The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelProductImagePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [product_id] column.
     * 
     * @param int $v new value
     * @return SelProductImage The current object (for fluent API support)
     */
    public function setProductId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->product_id !== $v) {
            $this->product_id = $v;
            $this->modifiedColumns[] = SelProductImagePeer::PRODUCT_ID;
        }

        if ($this->aSelProductRelatedByProductId !== null && $this->aSelProductRelatedByProductId->getId() !== $v) {
            $this->aSelProductRelatedByProductId = null;
        }


        return $this;
    } // setProductId()

    /**
     * Set the value of [name] column.
     * 
     * @param string $v new value
     * @return SelProductImage The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = SelProductImagePeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [size] column.
     * 
     * @param int $v new value
     * @return SelProductImage The current object (for fluent API support)
     */
    public function setSize($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->size !== $v) {
            $this->size = $v;
            $this->modifiedColumns[] = SelProductImagePeer::SIZE;
        }


        return $this;
    } // setSize()

    /**
     * Set the value of [mimetype] column.
     * 
     * @param string $v new value
     * @return SelProductImage The current object (for fluent API support)
     */
    public function setMimetype($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mimetype !== $v) {
            $this->mimetype = $v;
            $this->modifiedColumns[] = SelProductImagePeer::MIMETYPE;
        }


        return $this;
    } // setMimetype()

    /**
     * Set the value of [position] column.
     * 
     * @param int $v new value
     * @return SelProductImage The current object (for fluent API support)
     */
    public function setPosition($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->position !== $v) {
            $this->position = $v;
            $this->modifiedColumns[] = SelProductImagePeer::POSITION;
        }


        return $this;
    } // setPosition()

    /**
     * Set the value of [title] column.
     * 
     * @param string $v new value
     * @return SelProductImage The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[] = SelProductImagePeer::TITLE;
        }


        return $this;
    } // setTitle()

    /**
     * Set the value of [description] column.
     * 
     * @param string $v new value
     * @return SelProductImage The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = SelProductImagePeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->product_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->name = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->size = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->mimetype = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->position = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->title = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->description = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 8; // 8 = SelProductImagePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelProductImage object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSelProductRelatedByProductId !== null && $this->product_id !== $this->aSelProductRelatedByProductId->getId()) {
            $this->aSelProductRelatedByProductId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductImagePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelProductImagePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSelProductRelatedByProductId = null;
            $this->collSelProductsRelatedByMainImageId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductImagePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelProductImageQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductImagePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelProductImagePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelProductRelatedByProductId !== null) {
                if ($this->aSelProductRelatedByProductId->isModified() || $this->aSelProductRelatedByProductId->isNew()) {
                    $affectedRows += $this->aSelProductRelatedByProductId->save($con);
                }
                $this->setSelProductRelatedByProductId($this->aSelProductRelatedByProductId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->selProductsRelatedByMainImageIdScheduledForDeletion !== null) {
                if (!$this->selProductsRelatedByMainImageIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->selProductsRelatedByMainImageIdScheduledForDeletion as $selProductRelatedByMainImageId) {
                        // need to save related object because we set the relation to null
                        $selProductRelatedByMainImageId->save($con);
                    }
                    $this->selProductsRelatedByMainImageIdScheduledForDeletion = null;
                }
            }

            if ($this->collSelProductsRelatedByMainImageId !== null) {
                foreach ($this->collSelProductsRelatedByMainImageId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SelProductImagePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SelProductImagePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelProductImagePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelProductImagePeer::PRODUCT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`PRODUCT_ID`';
        }
        if ($this->isColumnModified(SelProductImagePeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`NAME`';
        }
        if ($this->isColumnModified(SelProductImagePeer::SIZE)) {
            $modifiedColumns[':p' . $index++]  = '`SIZE`';
        }
        if ($this->isColumnModified(SelProductImagePeer::MIMETYPE)) {
            $modifiedColumns[':p' . $index++]  = '`MIMETYPE`';
        }
        if ($this->isColumnModified(SelProductImagePeer::POSITION)) {
            $modifiedColumns[':p' . $index++]  = '`POSITION`';
        }
        if ($this->isColumnModified(SelProductImagePeer::TITLE)) {
            $modifiedColumns[':p' . $index++]  = '`TITLE`';
        }
        if ($this->isColumnModified(SelProductImagePeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`DESCRIPTION`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_product_image` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`PRODUCT_ID`':						
                        $stmt->bindValue($identifier, $this->product_id, PDO::PARAM_INT);
                        break;
                    case '`NAME`':						
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`SIZE`':						
                        $stmt->bindValue($identifier, $this->size, PDO::PARAM_INT);
                        break;
                    case '`MIMETYPE`':						
                        $stmt->bindValue($identifier, $this->mimetype, PDO::PARAM_STR);
                        break;
                    case '`POSITION`':						
                        $stmt->bindValue($identifier, $this->position, PDO::PARAM_INT);
                        break;
                    case '`TITLE`':						
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case '`DESCRIPTION`':						
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelProductRelatedByProductId !== null) {
                if (!$this->aSelProductRelatedByProductId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelProductRelatedByProductId->getValidationFailures());
                }
            }

            if (($retval = SelProductImagePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelProductsRelatedByMainImageId !== null) {
                    foreach ($this->collSelProductsRelatedByMainImageId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelProductImagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getProductId();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getSize();
                break;
            case 4:
                return $this->getMimetype();
                break;
            case 5:
                return $this->getPosition();
                break;
            case 6:
                return $this->getTitle();
                break;
            case 7:
                return $this->getDescription();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelProductImage'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelProductImage'][$this->getPrimaryKey()] = true;
        $keys = SelProductImagePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getProductId(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getSize(),
            $keys[4] => $this->getMimetype(),
            $keys[5] => $this->getPosition(),
            $keys[6] => $this->getTitle(),
            $keys[7] => $this->getDescription(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSelProductRelatedByProductId) {
                $result['SelProductRelatedByProductId'] = $this->aSelProductRelatedByProductId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSelProductsRelatedByMainImageId) {
                $result['SelProductsRelatedByMainImageId'] = $this->collSelProductsRelatedByMainImageId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelProductImagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setProductId($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setSize($value);
                break;
            case 4:
                $this->setMimetype($value);
                break;
            case 5:
                $this->setPosition($value);
                break;
            case 6:
                $this->setTitle($value);
                break;
            case 7:
                $this->setDescription($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelProductImagePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setProductId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setSize($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setMimetype($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPosition($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setTitle($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setDescription($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelProductImagePeer::DATABASE_NAME);

        if ($this->isColumnModified(SelProductImagePeer::ID)) $criteria->add(SelProductImagePeer::ID, $this->id);
        if ($this->isColumnModified(SelProductImagePeer::PRODUCT_ID)) $criteria->add(SelProductImagePeer::PRODUCT_ID, $this->product_id);
        if ($this->isColumnModified(SelProductImagePeer::NAME)) $criteria->add(SelProductImagePeer::NAME, $this->name);
        if ($this->isColumnModified(SelProductImagePeer::SIZE)) $criteria->add(SelProductImagePeer::SIZE, $this->size);
        if ($this->isColumnModified(SelProductImagePeer::MIMETYPE)) $criteria->add(SelProductImagePeer::MIMETYPE, $this->mimetype);
        if ($this->isColumnModified(SelProductImagePeer::POSITION)) $criteria->add(SelProductImagePeer::POSITION, $this->position);
        if ($this->isColumnModified(SelProductImagePeer::TITLE)) $criteria->add(SelProductImagePeer::TITLE, $this->title);
        if ($this->isColumnModified(SelProductImagePeer::DESCRIPTION)) $criteria->add(SelProductImagePeer::DESCRIPTION, $this->description);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelProductImagePeer::DATABASE_NAME);
        $criteria->add(SelProductImagePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelProductImage (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setProductId($this->getProductId());
        $copyObj->setName($this->getName());
        $copyObj->setSize($this->getSize());
        $copyObj->setMimetype($this->getMimetype());
        $copyObj->setPosition($this->getPosition());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setDescription($this->getDescription());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelProductsRelatedByMainImageId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProductRelatedByMainImageId($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelProductImage Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelProductImagePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelProductImagePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SelProduct object.
     *
     * @param             SelProduct $v
     * @return SelProductImage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelProductRelatedByProductId(SelProduct $v = null)
    {
        if ($v === null) {
            $this->setProductId(NULL);
        } else {
            $this->setProductId($v->getId());
        }

        $this->aSelProductRelatedByProductId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelProduct object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProductImageRelatedByProductId($this);
        }


        return $this;
    }


    /**
     * Get the associated SelProduct object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelProduct The associated SelProduct object.
     * @throws PropelException
     */
    public function getSelProductRelatedByProductId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelProductRelatedByProductId === null && ($this->product_id !== null) && $doQuery) {
            $this->aSelProductRelatedByProductId = SelProductQuery::create()->findPk($this->product_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelProductRelatedByProductId->addSelProductImagesRelatedByProductId($this);
             */
        }

        return $this->aSelProductRelatedByProductId;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelProductRelatedByMainImageId' == $relationName) {
            $this->initSelProductsRelatedByMainImageId();
        }
    }

    /**
     * Clears out the collSelProductsRelatedByMainImageId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelProductImage The current object (for fluent API support)
     * @see        addSelProductsRelatedByMainImageId()
     */
    public function clearSelProductsRelatedByMainImageId()
    {
        $this->collSelProductsRelatedByMainImageId = null; // important to set this to null since that means it is uninitialized
        $this->collSelProductsRelatedByMainImageIdPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProductsRelatedByMainImageId collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProductsRelatedByMainImageId($v = true)
    {
        $this->collSelProductsRelatedByMainImageIdPartial = $v;
    }

    /**
     * Initializes the collSelProductsRelatedByMainImageId collection.
     *
     * By default this just sets the collSelProductsRelatedByMainImageId collection to an empty array (like clearcollSelProductsRelatedByMainImageId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProductsRelatedByMainImageId($overrideExisting = true)
    {
        if (null !== $this->collSelProductsRelatedByMainImageId && !$overrideExisting) {
            return;
        }
        $this->collSelProductsRelatedByMainImageId = new PropelObjectCollection();
        $this->collSelProductsRelatedByMainImageId->setModel('SelProduct');
    }

    /**
     * Gets an array of SelProduct objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelProductImage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProduct[] List of SelProduct objects
     * @throws PropelException
     */
    public function getSelProductsRelatedByMainImageId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProductsRelatedByMainImageIdPartial && !$this->isNew();
        if (null === $this->collSelProductsRelatedByMainImageId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProductsRelatedByMainImageId) {
                // return empty collection
                $this->initSelProductsRelatedByMainImageId();
            } else {
                $collSelProductsRelatedByMainImageId = SelProductQuery::create(null, $criteria)
                    ->filterBySelProductImageRelatedByMainImageId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProductsRelatedByMainImageIdPartial && count($collSelProductsRelatedByMainImageId)) {
                      $this->initSelProductsRelatedByMainImageId(false);

                      foreach($collSelProductsRelatedByMainImageId as $obj) {
                        if (false == $this->collSelProductsRelatedByMainImageId->contains($obj)) {
                          $this->collSelProductsRelatedByMainImageId->append($obj);
                        }
                      }

                      $this->collSelProductsRelatedByMainImageIdPartial = true;
                    }

                    return $collSelProductsRelatedByMainImageId;
                }

                if($partial && $this->collSelProductsRelatedByMainImageId) {
                    foreach($this->collSelProductsRelatedByMainImageId as $obj) {
                        if($obj->isNew()) {
                            $collSelProductsRelatedByMainImageId[] = $obj;
                        }
                    }
                }

                $this->collSelProductsRelatedByMainImageId = $collSelProductsRelatedByMainImageId;
                $this->collSelProductsRelatedByMainImageIdPartial = false;
            }
        }

        return $this->collSelProductsRelatedByMainImageId;
    }

    /**
     * Sets a collection of SelProductRelatedByMainImageId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProductsRelatedByMainImageId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelProductImage The current object (for fluent API support)
     */
    public function setSelProductsRelatedByMainImageId(PropelCollection $selProductsRelatedByMainImageId, PropelPDO $con = null)
    {
        $selProductsRelatedByMainImageIdToDelete = $this->getSelProductsRelatedByMainImageId(new Criteria(), $con)->diff($selProductsRelatedByMainImageId);

        $this->selProductsRelatedByMainImageIdScheduledForDeletion = unserialize(serialize($selProductsRelatedByMainImageIdToDelete));

        foreach ($selProductsRelatedByMainImageIdToDelete as $selProductRelatedByMainImageIdRemoved) {
            $selProductRelatedByMainImageIdRemoved->setSelProductImageRelatedByMainImageId(null);
        }

        $this->collSelProductsRelatedByMainImageId = null;
        foreach ($selProductsRelatedByMainImageId as $selProductRelatedByMainImageId) {
            $this->addSelProductRelatedByMainImageId($selProductRelatedByMainImageId);
        }

        $this->collSelProductsRelatedByMainImageId = $selProductsRelatedByMainImageId;
        $this->collSelProductsRelatedByMainImageIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProduct objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProduct objects.
     * @throws PropelException
     */
    public function countSelProductsRelatedByMainImageId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProductsRelatedByMainImageIdPartial && !$this->isNew();
        if (null === $this->collSelProductsRelatedByMainImageId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProductsRelatedByMainImageId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProductsRelatedByMainImageId());
            }
            $query = SelProductQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelProductImageRelatedByMainImageId($this)
                ->count($con);
        }

        return count($this->collSelProductsRelatedByMainImageId);
    }

    /**
     * Method called to associate a SelProduct object to this object
     * through the SelProduct foreign key attribute.
     *
     * @param    SelProduct $l SelProduct
     * @return SelProductImage The current object (for fluent API support)
     */
    public function addSelProductRelatedByMainImageId(SelProduct $l)
    {
        if ($this->collSelProductsRelatedByMainImageId === null) {
            $this->initSelProductsRelatedByMainImageId();
            $this->collSelProductsRelatedByMainImageIdPartial = true;
        }
        if (!in_array($l, $this->collSelProductsRelatedByMainImageId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProductRelatedByMainImageId($l);
        }

        return $this;
    }

    /**
     * @param	SelProductRelatedByMainImageId $selProductRelatedByMainImageId The selProductRelatedByMainImageId object to add.
     */
    protected function doAddSelProductRelatedByMainImageId($selProductRelatedByMainImageId)
    {
        $this->collSelProductsRelatedByMainImageId[]= $selProductRelatedByMainImageId;
        $selProductRelatedByMainImageId->setSelProductImageRelatedByMainImageId($this);
    }

    /**
     * @param	SelProductRelatedByMainImageId $selProductRelatedByMainImageId The selProductRelatedByMainImageId object to remove.
     * @return SelProductImage The current object (for fluent API support)
     */
    public function removeSelProductRelatedByMainImageId($selProductRelatedByMainImageId)
    {
        if ($this->getSelProductsRelatedByMainImageId()->contains($selProductRelatedByMainImageId)) {
            $this->collSelProductsRelatedByMainImageId->remove($this->collSelProductsRelatedByMainImageId->search($selProductRelatedByMainImageId));
            if (null === $this->selProductsRelatedByMainImageIdScheduledForDeletion) {
                $this->selProductsRelatedByMainImageIdScheduledForDeletion = clone $this->collSelProductsRelatedByMainImageId;
                $this->selProductsRelatedByMainImageIdScheduledForDeletion->clear();
            }
            $this->selProductsRelatedByMainImageIdScheduledForDeletion[]= $selProductRelatedByMainImageId;
            $selProductRelatedByMainImageId->setSelProductImageRelatedByMainImageId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelProductImage is new, it will return
     * an empty collection; or if this SelProductImage has previously
     * been saved, it will retrieve related SelProductsRelatedByMainImageId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelProductImage.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProduct[] List of SelProduct objects
     */
    public function getSelProductsRelatedByMainImageIdJoinSelCategory($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductQuery::create(null, $criteria);
        $query->joinWith('SelCategory', $join_behavior);

        return $this->getSelProductsRelatedByMainImageId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelProductImage is new, it will return
     * an empty collection; or if this SelProductImage has previously
     * been saved, it will retrieve related SelProductsRelatedByMainImageId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelProductImage.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProduct[] List of SelProduct objects
     */
    public function getSelProductsRelatedByMainImageIdJoinSelProductGroup($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductQuery::create(null, $criteria);
        $query->joinWith('SelProductGroup', $join_behavior);

        return $this->getSelProductsRelatedByMainImageId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelProductImage is new, it will return
     * an empty collection; or if this SelProductImage has previously
     * been saved, it will retrieve related SelProductsRelatedByMainImageId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelProductImage.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProduct[] List of SelProduct objects
     */
    public function getSelProductsRelatedByMainImageIdJoinSelBrand($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductQuery::create(null, $criteria);
        $query->joinWith('SelBrand', $join_behavior);

        return $this->getSelProductsRelatedByMainImageId($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->product_id = null;
        $this->name = null;
        $this->size = null;
        $this->mimetype = null;
        $this->position = null;
        $this->title = null;
        $this->description = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSelProductsRelatedByMainImageId) {
                foreach ($this->collSelProductsRelatedByMainImageId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collSelProductsRelatedByMainImageId instanceof PropelCollection) {
            $this->collSelProductsRelatedByMainImageId->clearIterator();
        }
        $this->collSelProductsRelatedByMainImageId = null;
        $this->aSelProductRelatedByProductId = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelProductImagePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
