<?php


/**
 * Base class that represents a query for the 'cho_rol_x_uri' table.
 *
 * 
 *
 * @method ChoRolXUriQuery orderByRolId($order = Criteria::ASC) Order by the ROL_ID column
 * @method ChoRolXUriQuery orderByUriId($order = Criteria::ASC) Order by the URI_ID column
 * @method ChoRolXUriQuery orderByRead($order = Criteria::ASC) Order by the READ column
 * @method ChoRolXUriQuery orderByCreate($order = Criteria::ASC) Order by the CREATE column
 * @method ChoRolXUriQuery orderByUpdate($order = Criteria::ASC) Order by the UPDATE column
 * @method ChoRolXUriQuery orderByDelete($order = Criteria::ASC) Order by the DELETE column
 *
 * @method ChoRolXUriQuery groupByRolId() Group by the ROL_ID column
 * @method ChoRolXUriQuery groupByUriId() Group by the URI_ID column
 * @method ChoRolXUriQuery groupByRead() Group by the READ column
 * @method ChoRolXUriQuery groupByCreate() Group by the CREATE column
 * @method ChoRolXUriQuery groupByUpdate() Group by the UPDATE column
 * @method ChoRolXUriQuery groupByDelete() Group by the DELETE column
 *
 * @method ChoRolXUriQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ChoRolXUriQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ChoRolXUriQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ChoRolXUriQuery leftJoinChoRol($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChoRol relation
 * @method ChoRolXUriQuery rightJoinChoRol($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChoRol relation
 * @method ChoRolXUriQuery innerJoinChoRol($relationAlias = null) Adds a INNER JOIN clause to the query using the ChoRol relation
 *
 * @method ChoRolXUriQuery leftJoinChoUri($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChoUri relation
 * @method ChoRolXUriQuery rightJoinChoUri($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChoUri relation
 * @method ChoRolXUriQuery innerJoinChoUri($relationAlias = null) Adds a INNER JOIN clause to the query using the ChoUri relation
 *
 * @method ChoRolXUri findOne(PropelPDO $con = null) Return the first ChoRolXUri matching the query
 * @method ChoRolXUri findOneOrCreate(PropelPDO $con = null) Return the first ChoRolXUri matching the query, or a new ChoRolXUri object populated from the query conditions when no match is found
 *
 * @method ChoRolXUri findOneByRolId(int $ROL_ID) Return the first ChoRolXUri filtered by the ROL_ID column
 * @method ChoRolXUri findOneByUriId(int $URI_ID) Return the first ChoRolXUri filtered by the URI_ID column
 * @method ChoRolXUri findOneByRead(string $READ) Return the first ChoRolXUri filtered by the READ column
 * @method ChoRolXUri findOneByCreate(string $CREATE) Return the first ChoRolXUri filtered by the CREATE column
 * @method ChoRolXUri findOneByUpdate(string $UPDATE) Return the first ChoRolXUri filtered by the UPDATE column
 * @method ChoRolXUri findOneByDelete(string $DELETE) Return the first ChoRolXUri filtered by the DELETE column
 *
 * @method array findByRolId(int $ROL_ID) Return ChoRolXUri objects filtered by the ROL_ID column
 * @method array findByUriId(int $URI_ID) Return ChoRolXUri objects filtered by the URI_ID column
 * @method array findByRead(string $READ) Return ChoRolXUri objects filtered by the READ column
 * @method array findByCreate(string $CREATE) Return ChoRolXUri objects filtered by the CREATE column
 * @method array findByUpdate(string $UPDATE) Return ChoRolXUri objects filtered by the UPDATE column
 * @method array findByDelete(string $DELETE) Return ChoRolXUri objects filtered by the DELETE column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseChoRolXUriQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseChoRolXUriQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'ChoRolXUri', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChoRolXUriQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     ChoRolXUriQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChoRolXUriQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ChoRolXUriQuery) {
            return $criteria;
        }
        $query = new ChoRolXUriQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$ROL_ID, $URI_ID]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ChoRolXUri|ChoRolXUri[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ChoRolXUriPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ChoRolXUriPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   ChoRolXUri A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ROL_ID`, `URI_ID`, `READ`, `CREATE`, `UPDATE`, `DELETE` FROM `cho_rol_x_uri` WHERE `ROL_ID` = :p0 AND `URI_ID` = :p1';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);			
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ChoRolXUri();
            $obj->hydrate($row);
            ChoRolXUriPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ChoRolXUri|ChoRolXUri[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ChoRolXUri[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ChoRolXUriQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ChoRolXUriPeer::ROL_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ChoRolXUriPeer::URI_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ChoRolXUriQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ChoRolXUriPeer::ROL_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ChoRolXUriPeer::URI_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the ROL_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByRolId(1234); // WHERE ROL_ID = 1234
     * $query->filterByRolId(array(12, 34)); // WHERE ROL_ID IN (12, 34)
     * $query->filterByRolId(array('min' => 12)); // WHERE ROL_ID > 12
     * </code>
     *
     * @see       filterByChoRol()
     *
     * @param     mixed $rolId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoRolXUriQuery The current query, for fluid interface
     */
    public function filterByRolId($rolId = null, $comparison = null)
    {
        if (is_array($rolId) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ChoRolXUriPeer::ROL_ID, $rolId, $comparison);
    }

    /**
     * Filter the query on the URI_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUriId(1234); // WHERE URI_ID = 1234
     * $query->filterByUriId(array(12, 34)); // WHERE URI_ID IN (12, 34)
     * $query->filterByUriId(array('min' => 12)); // WHERE URI_ID > 12
     * </code>
     *
     * @see       filterByChoUri()
     *
     * @param     mixed $uriId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoRolXUriQuery The current query, for fluid interface
     */
    public function filterByUriId($uriId = null, $comparison = null)
    {
        if (is_array($uriId) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ChoRolXUriPeer::URI_ID, $uriId, $comparison);
    }

    /**
     * Filter the query on the READ column
     *
     * Example usage:
     * <code>
     * $query->filterByRead('fooValue');   // WHERE READ = 'fooValue'
     * $query->filterByRead('%fooValue%'); // WHERE READ LIKE '%fooValue%'
     * </code>
     *
     * @param     string $read The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoRolXUriQuery The current query, for fluid interface
     */
    public function filterByRead($read = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($read)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $read)) {
                $read = str_replace('*', '%', $read);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoRolXUriPeer::READ, $read, $comparison);
    }

    /**
     * Filter the query on the CREATE column
     *
     * Example usage:
     * <code>
     * $query->filterByCreate('fooValue');   // WHERE CREATE = 'fooValue'
     * $query->filterByCreate('%fooValue%'); // WHERE CREATE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $create The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoRolXUriQuery The current query, for fluid interface
     */
    public function filterByCreate($create = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($create)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $create)) {
                $create = str_replace('*', '%', $create);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoRolXUriPeer::CREATE, $create, $comparison);
    }

    /**
     * Filter the query on the UPDATE column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdate('fooValue');   // WHERE UPDATE = 'fooValue'
     * $query->filterByUpdate('%fooValue%'); // WHERE UPDATE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $update The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoRolXUriQuery The current query, for fluid interface
     */
    public function filterByUpdate($update = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($update)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $update)) {
                $update = str_replace('*', '%', $update);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoRolXUriPeer::UPDATE, $update, $comparison);
    }

    /**
     * Filter the query on the DELETE column
     *
     * Example usage:
     * <code>
     * $query->filterByDelete('fooValue');   // WHERE DELETE = 'fooValue'
     * $query->filterByDelete('%fooValue%'); // WHERE DELETE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $delete The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoRolXUriQuery The current query, for fluid interface
     */
    public function filterByDelete($delete = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($delete)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $delete)) {
                $delete = str_replace('*', '%', $delete);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoRolXUriPeer::DELETE, $delete, $comparison);
    }

    /**
     * Filter the query by a related ChoRol object
     *
     * @param   ChoRol|PropelObjectCollection $choRol The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ChoRolXUriQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByChoRol($choRol, $comparison = null)
    {
        if ($choRol instanceof ChoRol) {
            return $this
                ->addUsingAlias(ChoRolXUriPeer::ROL_ID, $choRol->getId(), $comparison);
        } elseif ($choRol instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChoRolXUriPeer::ROL_ID, $choRol->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChoRol() only accepts arguments of type ChoRol or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChoRol relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ChoRolXUriQuery The current query, for fluid interface
     */
    public function joinChoRol($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChoRol');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChoRol');
        }

        return $this;
    }

    /**
     * Use the ChoRol relation ChoRol object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ChoRolQuery A secondary query class using the current class as primary query
     */
    public function useChoRolQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChoRol($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChoRol', 'ChoRolQuery');
    }

    /**
     * Filter the query by a related ChoUri object
     *
     * @param   ChoUri|PropelObjectCollection $choUri The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ChoRolXUriQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByChoUri($choUri, $comparison = null)
    {
        if ($choUri instanceof ChoUri) {
            return $this
                ->addUsingAlias(ChoRolXUriPeer::URI_ID, $choUri->getId(), $comparison);
        } elseif ($choUri instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChoRolXUriPeer::URI_ID, $choUri->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChoUri() only accepts arguments of type ChoUri or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChoUri relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ChoRolXUriQuery The current query, for fluid interface
     */
    public function joinChoUri($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChoUri');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChoUri');
        }

        return $this;
    }

    /**
     * Use the ChoUri relation ChoUri object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ChoUriQuery A secondary query class using the current class as primary query
     */
    public function useChoUriQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChoUri($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChoUri', 'ChoUriQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChoRolXUri $choRolXUri Object to remove from the list of results
     *
     * @return ChoRolXUriQuery The current query, for fluid interface
     */
    public function prune($choRolXUri = null)
    {
        if ($choRolXUri) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ChoRolXUriPeer::ROL_ID), $choRolXUri->getRolId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ChoRolXUriPeer::URI_ID), $choRolXUri->getUriId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
