<?php


/**
 * Base class that represents a query for the 'sel_branch' table.
 *
 * 
 *
 * @method SelBranchQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelBranchQuery orderByNumber($order = Criteria::ASC) Order by the NUMBER column
 * @method SelBranchQuery orderByStatus($order = Criteria::ASC) Order by the STATUS column
 * @method SelBranchQuery orderByCode($order = Criteria::ASC) Order by the CODE column
 * @method SelBranchQuery orderByName($order = Criteria::ASC) Order by the NAME column
 * @method SelBranchQuery orderByAddress($order = Criteria::ASC) Order by the ADDRESS column
 * @method SelBranchQuery orderByPhone($order = Criteria::ASC) Order by the PHONE column
 * @method SelBranchQuery orderByFax($order = Criteria::ASC) Order by the FAX column
 *
 * @method SelBranchQuery groupById() Group by the ID column
 * @method SelBranchQuery groupByNumber() Group by the NUMBER column
 * @method SelBranchQuery groupByStatus() Group by the STATUS column
 * @method SelBranchQuery groupByCode() Group by the CODE column
 * @method SelBranchQuery groupByName() Group by the NAME column
 * @method SelBranchQuery groupByAddress() Group by the ADDRESS column
 * @method SelBranchQuery groupByPhone() Group by the PHONE column
 * @method SelBranchQuery groupByFax() Group by the FAX column
 *
 * @method SelBranchQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelBranchQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelBranchQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelBranchQuery leftJoinSelBranchCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelBranchCategory relation
 * @method SelBranchQuery rightJoinSelBranchCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelBranchCategory relation
 * @method SelBranchQuery innerJoinSelBranchCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the SelBranchCategory relation
 *
 * @method SelBranchQuery leftJoinSelBranchEmployee($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelBranchEmployee relation
 * @method SelBranchQuery rightJoinSelBranchEmployee($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelBranchEmployee relation
 * @method SelBranchQuery innerJoinSelBranchEmployee($relationAlias = null) Adds a INNER JOIN clause to the query using the SelBranchEmployee relation
 *
 * @method SelBranchQuery leftJoinSelProductItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductItem relation
 * @method SelBranchQuery rightJoinSelProductItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductItem relation
 * @method SelBranchQuery innerJoinSelProductItem($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductItem relation
 *
 * @method SelBranchQuery leftJoinSelSale($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelSale relation
 * @method SelBranchQuery rightJoinSelSale($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelSale relation
 * @method SelBranchQuery innerJoinSelSale($relationAlias = null) Adds a INNER JOIN clause to the query using the SelSale relation
 *
 * @method SelBranch findOne(PropelPDO $con = null) Return the first SelBranch matching the query
 * @method SelBranch findOneOrCreate(PropelPDO $con = null) Return the first SelBranch matching the query, or a new SelBranch object populated from the query conditions when no match is found
 *
 * @method SelBranch findOneByNumber(int $NUMBER) Return the first SelBranch filtered by the NUMBER column
 * @method SelBranch findOneByStatus(string $STATUS) Return the first SelBranch filtered by the STATUS column
 * @method SelBranch findOneByCode(string $CODE) Return the first SelBranch filtered by the CODE column
 * @method SelBranch findOneByName(string $NAME) Return the first SelBranch filtered by the NAME column
 * @method SelBranch findOneByAddress(string $ADDRESS) Return the first SelBranch filtered by the ADDRESS column
 * @method SelBranch findOneByPhone(string $PHONE) Return the first SelBranch filtered by the PHONE column
 * @method SelBranch findOneByFax(string $FAX) Return the first SelBranch filtered by the FAX column
 *
 * @method array findById(int $ID) Return SelBranch objects filtered by the ID column
 * @method array findByNumber(int $NUMBER) Return SelBranch objects filtered by the NUMBER column
 * @method array findByStatus(string $STATUS) Return SelBranch objects filtered by the STATUS column
 * @method array findByCode(string $CODE) Return SelBranch objects filtered by the CODE column
 * @method array findByName(string $NAME) Return SelBranch objects filtered by the NAME column
 * @method array findByAddress(string $ADDRESS) Return SelBranch objects filtered by the ADDRESS column
 * @method array findByPhone(string $PHONE) Return SelBranch objects filtered by the PHONE column
 * @method array findByFax(string $FAX) Return SelBranch objects filtered by the FAX column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelBranchQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelBranchQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelBranch', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelBranchQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelBranchQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelBranchQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelBranchQuery) {
            return $criteria;
        }
        $query = new SelBranchQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelBranch|SelBranch[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelBranchPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelBranchPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelBranch A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelBranch A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `NUMBER`, `STATUS`, `CODE`, `NAME`, `ADDRESS`, `PHONE`, `FAX` FROM `sel_branch` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelBranch();
            $obj->hydrate($row);
            SelBranchPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelBranch|SelBranch[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelBranch[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelBranchPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelBranchPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelBranchPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the NUMBER column
     *
     * Example usage:
     * <code>
     * $query->filterByNumber(1234); // WHERE NUMBER = 1234
     * $query->filterByNumber(array(12, 34)); // WHERE NUMBER IN (12, 34)
     * $query->filterByNumber(array('min' => 12)); // WHERE NUMBER > 12
     * </code>
     *
     * @param     mixed $number The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function filterByNumber($number = null, $comparison = null)
    {
        if (is_array($number)) {
            $useMinMax = false;
            if (isset($number['min'])) {
                $this->addUsingAlias(SelBranchPeer::NUMBER, $number['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($number['max'])) {
                $this->addUsingAlias(SelBranchPeer::NUMBER, $number['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelBranchPeer::NUMBER, $number, $comparison);
    }

    /**
     * Filter the query on the STATUS column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE STATUS = 'fooValue'
     * $query->filterByStatus('%fooValue%'); // WHERE STATUS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $status)) {
                $status = str_replace('*', '%', $status);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelBranchPeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the CODE column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE CODE = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE CODE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelBranchPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the NAME column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE NAME = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE NAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelBranchPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the ADDRESS column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE ADDRESS = 'fooValue'
     * $query->filterByAddress('%fooValue%'); // WHERE ADDRESS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $address)) {
                $address = str_replace('*', '%', $address);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelBranchPeer::ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the PHONE column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE PHONE = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE PHONE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelBranchPeer::PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the FAX column
     *
     * Example usage:
     * <code>
     * $query->filterByFax('fooValue');   // WHERE FAX = 'fooValue'
     * $query->filterByFax('%fooValue%'); // WHERE FAX LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fax The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function filterByFax($fax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fax)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fax)) {
                $fax = str_replace('*', '%', $fax);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelBranchPeer::FAX, $fax, $comparison);
    }

    /**
     * Filter the query by a related SelBranchCategory object
     *
     * @param   SelBranchCategory|PropelObjectCollection $selBranchCategory  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelBranchQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelBranchCategory($selBranchCategory, $comparison = null)
    {
        if ($selBranchCategory instanceof SelBranchCategory) {
            return $this
                ->addUsingAlias(SelBranchPeer::ID, $selBranchCategory->getBranchId(), $comparison);
        } elseif ($selBranchCategory instanceof PropelObjectCollection) {
            return $this
                ->useSelBranchCategoryQuery()
                ->filterByPrimaryKeys($selBranchCategory->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelBranchCategory() only accepts arguments of type SelBranchCategory or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelBranchCategory relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function joinSelBranchCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelBranchCategory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelBranchCategory');
        }

        return $this;
    }

    /**
     * Use the SelBranchCategory relation SelBranchCategory object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelBranchCategoryQuery A secondary query class using the current class as primary query
     */
    public function useSelBranchCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelBranchCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelBranchCategory', 'SelBranchCategoryQuery');
    }

    /**
     * Filter the query by a related SelBranchEmployee object
     *
     * @param   SelBranchEmployee|PropelObjectCollection $selBranchEmployee  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelBranchQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelBranchEmployee($selBranchEmployee, $comparison = null)
    {
        if ($selBranchEmployee instanceof SelBranchEmployee) {
            return $this
                ->addUsingAlias(SelBranchPeer::ID, $selBranchEmployee->getBranchId(), $comparison);
        } elseif ($selBranchEmployee instanceof PropelObjectCollection) {
            return $this
                ->useSelBranchEmployeeQuery()
                ->filterByPrimaryKeys($selBranchEmployee->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelBranchEmployee() only accepts arguments of type SelBranchEmployee or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelBranchEmployee relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function joinSelBranchEmployee($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelBranchEmployee');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelBranchEmployee');
        }

        return $this;
    }

    /**
     * Use the SelBranchEmployee relation SelBranchEmployee object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelBranchEmployeeQuery A secondary query class using the current class as primary query
     */
    public function useSelBranchEmployeeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelBranchEmployee($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelBranchEmployee', 'SelBranchEmployeeQuery');
    }

    /**
     * Filter the query by a related SelProductItem object
     *
     * @param   SelProductItem|PropelObjectCollection $selProductItem  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelBranchQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductItem($selProductItem, $comparison = null)
    {
        if ($selProductItem instanceof SelProductItem) {
            return $this
                ->addUsingAlias(SelBranchPeer::ID, $selProductItem->getBranchId(), $comparison);
        } elseif ($selProductItem instanceof PropelObjectCollection) {
            return $this
                ->useSelProductItemQuery()
                ->filterByPrimaryKeys($selProductItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductItem() only accepts arguments of type SelProductItem or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function joinSelProductItem($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductItem');
        }

        return $this;
    }

    /**
     * Use the SelProductItem relation SelProductItem object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductItemQuery A secondary query class using the current class as primary query
     */
    public function useSelProductItemQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelProductItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductItem', 'SelProductItemQuery');
    }

    /**
     * Filter the query by a related SelSale object
     *
     * @param   SelSale|PropelObjectCollection $selSale  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelBranchQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelSale($selSale, $comparison = null)
    {
        if ($selSale instanceof SelSale) {
            return $this
                ->addUsingAlias(SelBranchPeer::ID, $selSale->getBranchId(), $comparison);
        } elseif ($selSale instanceof PropelObjectCollection) {
            return $this
                ->useSelSaleQuery()
                ->filterByPrimaryKeys($selSale->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelSale() only accepts arguments of type SelSale or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelSale relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function joinSelSale($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelSale');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelSale');
        }

        return $this;
    }

    /**
     * Use the SelSale relation SelSale object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelSaleQuery A secondary query class using the current class as primary query
     */
    public function useSelSaleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelSale($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelSale', 'SelSaleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelBranch $selBranch Object to remove from the list of results
     *
     * @return SelBranchQuery The current query, for fluid interface
     */
    public function prune($selBranch = null)
    {
        if ($selBranch) {
            $this->addUsingAlias(SelBranchPeer::ID, $selBranch->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
