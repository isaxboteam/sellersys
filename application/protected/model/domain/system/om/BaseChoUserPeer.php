<?php


/**
 * Base static class for performing query and update operations on the 'cho_user' table.
 *
 * 
 *
 * @package propel.generator.system.om
 */
abstract class BaseChoUserPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'system';

    /** the table name for this class */
    const TABLE_NAME = 'cho_user';

    /** the related Propel class for this table */
    const OM_CLASS = 'ChoUser';

    /** the related TableMap class for this table */
    const TM_CLASS = 'ChoUserTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 25;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 25;

    /** the column name for the ID field */
    const ID = 'cho_user.ID';

    /** the column name for the ROL_ID field */
    const ROL_ID = 'cho_user.ROL_ID';

    /** the column name for the FIRST_LASTNAME field */
    const FIRST_LASTNAME = 'cho_user.FIRST_LASTNAME';

    /** the column name for the SECOND_LASTNAME field */
    const SECOND_LASTNAME = 'cho_user.SECOND_LASTNAME';

    /** the column name for the FIRST_NAME field */
    const FIRST_NAME = 'cho_user.FIRST_NAME';

    /** the column name for the SECOND_NAME field */
    const SECOND_NAME = 'cho_user.SECOND_NAME';

    /** the column name for the USERNAME field */
    const USERNAME = 'cho_user.USERNAME';

    /** the column name for the PASSWORD field */
    const PASSWORD = 'cho_user.PASSWORD';

    /** the column name for the EMAIL field */
    const EMAIL = 'cho_user.EMAIL';

    /** the column name for the DNI field */
    const DNI = 'cho_user.DNI';

    /** the column name for the GENDER field */
    const GENDER = 'cho_user.GENDER';

    /** the column name for the BIRTHDAY field */
    const BIRTHDAY = 'cho_user.BIRTHDAY';

    /** the column name for the TYPE field */
    const TYPE = 'cho_user.TYPE';

    /** the column name for the STATUS field */
    const STATUS = 'cho_user.STATUS';

    /** the column name for the COUNTRY field */
    const COUNTRY = 'cho_user.COUNTRY';

    /** the column name for the STATE field */
    const STATE = 'cho_user.STATE';

    /** the column name for the CITY field */
    const CITY = 'cho_user.CITY';

    /** the column name for the ADDRESS field */
    const ADDRESS = 'cho_user.ADDRESS';

    /** the column name for the ZIP field */
    const ZIP = 'cho_user.ZIP';

    /** the column name for the PRIMARY_PHONE field */
    const PRIMARY_PHONE = 'cho_user.PRIMARY_PHONE';

    /** the column name for the SECONDARY_PHONE field */
    const SECONDARY_PHONE = 'cho_user.SECONDARY_PHONE';

    /** the column name for the CELL_PHONE field */
    const CELL_PHONE = 'cho_user.CELL_PHONE';

    /** the column name for the PHOTO_FORMAT field */
    const PHOTO_FORMAT = 'cho_user.PHOTO_FORMAT';

    /** the column name for the REGISTERED_DATE field */
    const REGISTERED_DATE = 'cho_user.REGISTERED_DATE';

    /** the column name for the LAST_ACCESS field */
    const LAST_ACCESS = 'cho_user.LAST_ACCESS';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of ChoUser objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array ChoUser[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. ChoUserPeer::$fieldNames[ChoUserPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'RolId', 'FirstLastname', 'SecondLastname', 'FirstName', 'SecondName', 'Username', 'Password', 'Email', 'Dni', 'Gender', 'Birthday', 'Type', 'Status', 'Country', 'State', 'City', 'Address', 'Zip', 'PrimaryPhone', 'SecondaryPhone', 'CellPhone', 'PhotoFormat', 'RegisteredDate', 'LastAccess', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'rolId', 'firstLastname', 'secondLastname', 'firstName', 'secondName', 'username', 'password', 'email', 'dni', 'gender', 'birthday', 'type', 'status', 'country', 'state', 'city', 'address', 'zip', 'primaryPhone', 'secondaryPhone', 'cellPhone', 'photoFormat', 'registeredDate', 'lastAccess', ),
        BasePeer::TYPE_COLNAME => array (ChoUserPeer::ID, ChoUserPeer::ROL_ID, ChoUserPeer::FIRST_LASTNAME, ChoUserPeer::SECOND_LASTNAME, ChoUserPeer::FIRST_NAME, ChoUserPeer::SECOND_NAME, ChoUserPeer::USERNAME, ChoUserPeer::PASSWORD, ChoUserPeer::EMAIL, ChoUserPeer::DNI, ChoUserPeer::GENDER, ChoUserPeer::BIRTHDAY, ChoUserPeer::TYPE, ChoUserPeer::STATUS, ChoUserPeer::COUNTRY, ChoUserPeer::STATE, ChoUserPeer::CITY, ChoUserPeer::ADDRESS, ChoUserPeer::ZIP, ChoUserPeer::PRIMARY_PHONE, ChoUserPeer::SECONDARY_PHONE, ChoUserPeer::CELL_PHONE, ChoUserPeer::PHOTO_FORMAT, ChoUserPeer::REGISTERED_DATE, ChoUserPeer::LAST_ACCESS, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ROL_ID', 'FIRST_LASTNAME', 'SECOND_LASTNAME', 'FIRST_NAME', 'SECOND_NAME', 'USERNAME', 'PASSWORD', 'EMAIL', 'DNI', 'GENDER', 'BIRTHDAY', 'TYPE', 'STATUS', 'COUNTRY', 'STATE', 'CITY', 'ADDRESS', 'ZIP', 'PRIMARY_PHONE', 'SECONDARY_PHONE', 'CELL_PHONE', 'PHOTO_FORMAT', 'REGISTERED_DATE', 'LAST_ACCESS', ),
        BasePeer::TYPE_FIELDNAME => array ('ID', 'ROL_ID', 'FIRST_LASTNAME', 'SECOND_LASTNAME', 'FIRST_NAME', 'SECOND_NAME', 'USERNAME', 'PASSWORD', 'EMAIL', 'DNI', 'GENDER', 'BIRTHDAY', 'TYPE', 'STATUS', 'COUNTRY', 'STATE', 'CITY', 'ADDRESS', 'ZIP', 'PRIMARY_PHONE', 'SECONDARY_PHONE', 'CELL_PHONE', 'PHOTO_FORMAT', 'REGISTERED_DATE', 'LAST_ACCESS', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. ChoUserPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'RolId' => 1, 'FirstLastname' => 2, 'SecondLastname' => 3, 'FirstName' => 4, 'SecondName' => 5, 'Username' => 6, 'Password' => 7, 'Email' => 8, 'Dni' => 9, 'Gender' => 10, 'Birthday' => 11, 'Type' => 12, 'Status' => 13, 'Country' => 14, 'State' => 15, 'City' => 16, 'Address' => 17, 'Zip' => 18, 'PrimaryPhone' => 19, 'SecondaryPhone' => 20, 'CellPhone' => 21, 'PhotoFormat' => 22, 'RegisteredDate' => 23, 'LastAccess' => 24, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'rolId' => 1, 'firstLastname' => 2, 'secondLastname' => 3, 'firstName' => 4, 'secondName' => 5, 'username' => 6, 'password' => 7, 'email' => 8, 'dni' => 9, 'gender' => 10, 'birthday' => 11, 'type' => 12, 'status' => 13, 'country' => 14, 'state' => 15, 'city' => 16, 'address' => 17, 'zip' => 18, 'primaryPhone' => 19, 'secondaryPhone' => 20, 'cellPhone' => 21, 'photoFormat' => 22, 'registeredDate' => 23, 'lastAccess' => 24, ),
        BasePeer::TYPE_COLNAME => array (ChoUserPeer::ID => 0, ChoUserPeer::ROL_ID => 1, ChoUserPeer::FIRST_LASTNAME => 2, ChoUserPeer::SECOND_LASTNAME => 3, ChoUserPeer::FIRST_NAME => 4, ChoUserPeer::SECOND_NAME => 5, ChoUserPeer::USERNAME => 6, ChoUserPeer::PASSWORD => 7, ChoUserPeer::EMAIL => 8, ChoUserPeer::DNI => 9, ChoUserPeer::GENDER => 10, ChoUserPeer::BIRTHDAY => 11, ChoUserPeer::TYPE => 12, ChoUserPeer::STATUS => 13, ChoUserPeer::COUNTRY => 14, ChoUserPeer::STATE => 15, ChoUserPeer::CITY => 16, ChoUserPeer::ADDRESS => 17, ChoUserPeer::ZIP => 18, ChoUserPeer::PRIMARY_PHONE => 19, ChoUserPeer::SECONDARY_PHONE => 20, ChoUserPeer::CELL_PHONE => 21, ChoUserPeer::PHOTO_FORMAT => 22, ChoUserPeer::REGISTERED_DATE => 23, ChoUserPeer::LAST_ACCESS => 24, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ROL_ID' => 1, 'FIRST_LASTNAME' => 2, 'SECOND_LASTNAME' => 3, 'FIRST_NAME' => 4, 'SECOND_NAME' => 5, 'USERNAME' => 6, 'PASSWORD' => 7, 'EMAIL' => 8, 'DNI' => 9, 'GENDER' => 10, 'BIRTHDAY' => 11, 'TYPE' => 12, 'STATUS' => 13, 'COUNTRY' => 14, 'STATE' => 15, 'CITY' => 16, 'ADDRESS' => 17, 'ZIP' => 18, 'PRIMARY_PHONE' => 19, 'SECONDARY_PHONE' => 20, 'CELL_PHONE' => 21, 'PHOTO_FORMAT' => 22, 'REGISTERED_DATE' => 23, 'LAST_ACCESS' => 24, ),
        BasePeer::TYPE_FIELDNAME => array ('ID' => 0, 'ROL_ID' => 1, 'FIRST_LASTNAME' => 2, 'SECOND_LASTNAME' => 3, 'FIRST_NAME' => 4, 'SECOND_NAME' => 5, 'USERNAME' => 6, 'PASSWORD' => 7, 'EMAIL' => 8, 'DNI' => 9, 'GENDER' => 10, 'BIRTHDAY' => 11, 'TYPE' => 12, 'STATUS' => 13, 'COUNTRY' => 14, 'STATE' => 15, 'CITY' => 16, 'ADDRESS' => 17, 'ZIP' => 18, 'PRIMARY_PHONE' => 19, 'SECONDARY_PHONE' => 20, 'CELL_PHONE' => 21, 'PHOTO_FORMAT' => 22, 'REGISTERED_DATE' => 23, 'LAST_ACCESS' => 24, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = ChoUserPeer::getFieldNames($toType);
        $key = isset(ChoUserPeer::$fieldKeys[$fromType][$name]) ? ChoUserPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(ChoUserPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, ChoUserPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return ChoUserPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. ChoUserPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(ChoUserPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ChoUserPeer::ID);
            $criteria->addSelectColumn(ChoUserPeer::ROL_ID);
            $criteria->addSelectColumn(ChoUserPeer::FIRST_LASTNAME);
            $criteria->addSelectColumn(ChoUserPeer::SECOND_LASTNAME);
            $criteria->addSelectColumn(ChoUserPeer::FIRST_NAME);
            $criteria->addSelectColumn(ChoUserPeer::SECOND_NAME);
            $criteria->addSelectColumn(ChoUserPeer::USERNAME);
            $criteria->addSelectColumn(ChoUserPeer::PASSWORD);
            $criteria->addSelectColumn(ChoUserPeer::EMAIL);
            $criteria->addSelectColumn(ChoUserPeer::DNI);
            $criteria->addSelectColumn(ChoUserPeer::GENDER);
            $criteria->addSelectColumn(ChoUserPeer::BIRTHDAY);
            $criteria->addSelectColumn(ChoUserPeer::TYPE);
            $criteria->addSelectColumn(ChoUserPeer::STATUS);
            $criteria->addSelectColumn(ChoUserPeer::COUNTRY);
            $criteria->addSelectColumn(ChoUserPeer::STATE);
            $criteria->addSelectColumn(ChoUserPeer::CITY);
            $criteria->addSelectColumn(ChoUserPeer::ADDRESS);
            $criteria->addSelectColumn(ChoUserPeer::ZIP);
            $criteria->addSelectColumn(ChoUserPeer::PRIMARY_PHONE);
            $criteria->addSelectColumn(ChoUserPeer::SECONDARY_PHONE);
            $criteria->addSelectColumn(ChoUserPeer::CELL_PHONE);
            $criteria->addSelectColumn(ChoUserPeer::PHOTO_FORMAT);
            $criteria->addSelectColumn(ChoUserPeer::REGISTERED_DATE);
            $criteria->addSelectColumn(ChoUserPeer::LAST_ACCESS);
        } else {
            $criteria->addSelectColumn($alias . '.ID');
            $criteria->addSelectColumn($alias . '.ROL_ID');
            $criteria->addSelectColumn($alias . '.FIRST_LASTNAME');
            $criteria->addSelectColumn($alias . '.SECOND_LASTNAME');
            $criteria->addSelectColumn($alias . '.FIRST_NAME');
            $criteria->addSelectColumn($alias . '.SECOND_NAME');
            $criteria->addSelectColumn($alias . '.USERNAME');
            $criteria->addSelectColumn($alias . '.PASSWORD');
            $criteria->addSelectColumn($alias . '.EMAIL');
            $criteria->addSelectColumn($alias . '.DNI');
            $criteria->addSelectColumn($alias . '.GENDER');
            $criteria->addSelectColumn($alias . '.BIRTHDAY');
            $criteria->addSelectColumn($alias . '.TYPE');
            $criteria->addSelectColumn($alias . '.STATUS');
            $criteria->addSelectColumn($alias . '.COUNTRY');
            $criteria->addSelectColumn($alias . '.STATE');
            $criteria->addSelectColumn($alias . '.CITY');
            $criteria->addSelectColumn($alias . '.ADDRESS');
            $criteria->addSelectColumn($alias . '.ZIP');
            $criteria->addSelectColumn($alias . '.PRIMARY_PHONE');
            $criteria->addSelectColumn($alias . '.SECONDARY_PHONE');
            $criteria->addSelectColumn($alias . '.CELL_PHONE');
            $criteria->addSelectColumn($alias . '.PHOTO_FORMAT');
            $criteria->addSelectColumn($alias . '.REGISTERED_DATE');
            $criteria->addSelectColumn($alias . '.LAST_ACCESS');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(ChoUserPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            ChoUserPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(ChoUserPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 ChoUser
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = ChoUserPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return ChoUserPeer::populateObjects(ChoUserPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            ChoUserPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(ChoUserPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      ChoUser $obj A ChoUser object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            ChoUserPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A ChoUser object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof ChoUser) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or ChoUser object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(ChoUserPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   ChoUser Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(ChoUserPeer::$instances[$key])) {
                return ChoUserPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool()
    {
        ChoUserPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to cho_user
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = ChoUserPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = ChoUserPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = ChoUserPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ChoUserPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (ChoUser object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = ChoUserPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = ChoUserPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + ChoUserPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ChoUserPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            ChoUserPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(ChoUserPeer::DATABASE_NAME)->getTable(ChoUserPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseChoUserPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseChoUserPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new ChoUserTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return ChoUserPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a ChoUser or Criteria object.
     *
     * @param      mixed $values Criteria or ChoUser object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from ChoUser object
        }

        if ($criteria->containsKey(ChoUserPeer::ID) && $criteria->keyContainsValue(ChoUserPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ChoUserPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(ChoUserPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a ChoUser or Criteria object.
     *
     * @param      mixed $values Criteria or ChoUser object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(ChoUserPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(ChoUserPeer::ID);
            $value = $criteria->remove(ChoUserPeer::ID);
            if ($value) {
                $selectCriteria->add(ChoUserPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(ChoUserPeer::TABLE_NAME);
            }

        } else { // $values is ChoUser object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(ChoUserPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the cho_user table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(ChoUserPeer::TABLE_NAME, $con, ChoUserPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ChoUserPeer::clearInstancePool();
            ChoUserPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a ChoUser or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or ChoUser object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            ChoUserPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof ChoUser) { // it's a model object
            // invalidate the cache for this single object
            ChoUserPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ChoUserPeer::DATABASE_NAME);
            $criteria->add(ChoUserPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                ChoUserPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(ChoUserPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            ChoUserPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given ChoUser object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      ChoUser $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(ChoUserPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(ChoUserPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(ChoUserPeer::DATABASE_NAME, ChoUserPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return ChoUser
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = ChoUserPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(ChoUserPeer::DATABASE_NAME);
        $criteria->add(ChoUserPeer::ID, $pk);

        $v = ChoUserPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return ChoUser[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(ChoUserPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(ChoUserPeer::DATABASE_NAME);
            $criteria->add(ChoUserPeer::ID, $pks, Criteria::IN);
            $objs = ChoUserPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseChoUserPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseChoUserPeer::buildTableMap();

