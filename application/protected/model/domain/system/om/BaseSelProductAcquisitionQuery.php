<?php


/**
 * Base class that represents a query for the 'sel_product_acquisition' table.
 *
 * 
 *
 * @method SelProductAcquisitionQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelProductAcquisitionQuery orderByAquisitionId($order = Criteria::ASC) Order by the AQUISITION_ID column
 * @method SelProductAcquisitionQuery orderByProductId($order = Criteria::ASC) Order by the PRODUCT_ID column
 * @method SelProductAcquisitionQuery orderByQuantity($order = Criteria::ASC) Order by the QUANTITY column
 * @method SelProductAcquisitionQuery orderByPrice($order = Criteria::ASC) Order by the PRICE column
 * @method SelProductAcquisitionQuery orderByMoney($order = Criteria::ASC) Order by the MONEY column
 * @method SelProductAcquisitionQuery orderByWarrantyProvider($order = Criteria::ASC) Order by the WARRANTY_PROVIDER column
 *
 * @method SelProductAcquisitionQuery groupById() Group by the ID column
 * @method SelProductAcquisitionQuery groupByAquisitionId() Group by the AQUISITION_ID column
 * @method SelProductAcquisitionQuery groupByProductId() Group by the PRODUCT_ID column
 * @method SelProductAcquisitionQuery groupByQuantity() Group by the QUANTITY column
 * @method SelProductAcquisitionQuery groupByPrice() Group by the PRICE column
 * @method SelProductAcquisitionQuery groupByMoney() Group by the MONEY column
 * @method SelProductAcquisitionQuery groupByWarrantyProvider() Group by the WARRANTY_PROVIDER column
 *
 * @method SelProductAcquisitionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelProductAcquisitionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelProductAcquisitionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelProductAcquisitionQuery leftJoinSelAcquisition($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelAcquisition relation
 * @method SelProductAcquisitionQuery rightJoinSelAcquisition($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelAcquisition relation
 * @method SelProductAcquisitionQuery innerJoinSelAcquisition($relationAlias = null) Adds a INNER JOIN clause to the query using the SelAcquisition relation
 *
 * @method SelProductAcquisitionQuery leftJoinSelProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProduct relation
 * @method SelProductAcquisitionQuery rightJoinSelProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProduct relation
 * @method SelProductAcquisitionQuery innerJoinSelProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProduct relation
 *
 * @method SelProductAcquisitionQuery leftJoinSelProductItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductItem relation
 * @method SelProductAcquisitionQuery rightJoinSelProductItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductItem relation
 * @method SelProductAcquisitionQuery innerJoinSelProductItem($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductItem relation
 *
 * @method SelProductAcquisition findOne(PropelPDO $con = null) Return the first SelProductAcquisition matching the query
 * @method SelProductAcquisition findOneOrCreate(PropelPDO $con = null) Return the first SelProductAcquisition matching the query, or a new SelProductAcquisition object populated from the query conditions when no match is found
 *
 * @method SelProductAcquisition findOneByAquisitionId(int $AQUISITION_ID) Return the first SelProductAcquisition filtered by the AQUISITION_ID column
 * @method SelProductAcquisition findOneByProductId(int $PRODUCT_ID) Return the first SelProductAcquisition filtered by the PRODUCT_ID column
 * @method SelProductAcquisition findOneByQuantity(int $QUANTITY) Return the first SelProductAcquisition filtered by the QUANTITY column
 * @method SelProductAcquisition findOneByPrice(double $PRICE) Return the first SelProductAcquisition filtered by the PRICE column
 * @method SelProductAcquisition findOneByMoney(string $MONEY) Return the first SelProductAcquisition filtered by the MONEY column
 * @method SelProductAcquisition findOneByWarrantyProvider(string $WARRANTY_PROVIDER) Return the first SelProductAcquisition filtered by the WARRANTY_PROVIDER column
 *
 * @method array findById(int $ID) Return SelProductAcquisition objects filtered by the ID column
 * @method array findByAquisitionId(int $AQUISITION_ID) Return SelProductAcquisition objects filtered by the AQUISITION_ID column
 * @method array findByProductId(int $PRODUCT_ID) Return SelProductAcquisition objects filtered by the PRODUCT_ID column
 * @method array findByQuantity(int $QUANTITY) Return SelProductAcquisition objects filtered by the QUANTITY column
 * @method array findByPrice(double $PRICE) Return SelProductAcquisition objects filtered by the PRICE column
 * @method array findByMoney(string $MONEY) Return SelProductAcquisition objects filtered by the MONEY column
 * @method array findByWarrantyProvider(string $WARRANTY_PROVIDER) Return SelProductAcquisition objects filtered by the WARRANTY_PROVIDER column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProductAcquisitionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelProductAcquisitionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelProductAcquisition', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelProductAcquisitionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelProductAcquisitionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelProductAcquisitionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelProductAcquisitionQuery) {
            return $criteria;
        }
        $query = new SelProductAcquisitionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelProductAcquisition|SelProductAcquisition[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelProductAcquisitionPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelProductAcquisitionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductAcquisition A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductAcquisition A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `AQUISITION_ID`, `PRODUCT_ID`, `QUANTITY`, `PRICE`, `MONEY`, `WARRANTY_PROVIDER` FROM `sel_product_acquisition` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelProductAcquisition();
            $obj->hydrate($row);
            SelProductAcquisitionPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelProductAcquisition|SelProductAcquisition[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelProductAcquisition[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelProductAcquisitionPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelProductAcquisitionPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelProductAcquisitionPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the AQUISITION_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByAquisitionId(1234); // WHERE AQUISITION_ID = 1234
     * $query->filterByAquisitionId(array(12, 34)); // WHERE AQUISITION_ID IN (12, 34)
     * $query->filterByAquisitionId(array('min' => 12)); // WHERE AQUISITION_ID > 12
     * </code>
     *
     * @see       filterBySelAcquisition()
     *
     * @param     mixed $aquisitionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function filterByAquisitionId($aquisitionId = null, $comparison = null)
    {
        if (is_array($aquisitionId)) {
            $useMinMax = false;
            if (isset($aquisitionId['min'])) {
                $this->addUsingAlias(SelProductAcquisitionPeer::AQUISITION_ID, $aquisitionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($aquisitionId['max'])) {
                $this->addUsingAlias(SelProductAcquisitionPeer::AQUISITION_ID, $aquisitionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductAcquisitionPeer::AQUISITION_ID, $aquisitionId, $comparison);
    }

    /**
     * Filter the query on the PRODUCT_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByProductId(1234); // WHERE PRODUCT_ID = 1234
     * $query->filterByProductId(array(12, 34)); // WHERE PRODUCT_ID IN (12, 34)
     * $query->filterByProductId(array('min' => 12)); // WHERE PRODUCT_ID > 12
     * </code>
     *
     * @see       filterBySelProduct()
     *
     * @param     mixed $productId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function filterByProductId($productId = null, $comparison = null)
    {
        if (is_array($productId)) {
            $useMinMax = false;
            if (isset($productId['min'])) {
                $this->addUsingAlias(SelProductAcquisitionPeer::PRODUCT_ID, $productId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productId['max'])) {
                $this->addUsingAlias(SelProductAcquisitionPeer::PRODUCT_ID, $productId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductAcquisitionPeer::PRODUCT_ID, $productId, $comparison);
    }

    /**
     * Filter the query on the QUANTITY column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantity(1234); // WHERE QUANTITY = 1234
     * $query->filterByQuantity(array(12, 34)); // WHERE QUANTITY IN (12, 34)
     * $query->filterByQuantity(array('min' => 12)); // WHERE QUANTITY > 12
     * </code>
     *
     * @param     mixed $quantity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function filterByQuantity($quantity = null, $comparison = null)
    {
        if (is_array($quantity)) {
            $useMinMax = false;
            if (isset($quantity['min'])) {
                $this->addUsingAlias(SelProductAcquisitionPeer::QUANTITY, $quantity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantity['max'])) {
                $this->addUsingAlias(SelProductAcquisitionPeer::QUANTITY, $quantity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductAcquisitionPeer::QUANTITY, $quantity, $comparison);
    }

    /**
     * Filter the query on the PRICE column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE PRICE = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE PRICE IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE PRICE > 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(SelProductAcquisitionPeer::PRICE, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(SelProductAcquisitionPeer::PRICE, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductAcquisitionPeer::PRICE, $price, $comparison);
    }

    /**
     * Filter the query on the MONEY column
     *
     * Example usage:
     * <code>
     * $query->filterByMoney('fooValue');   // WHERE MONEY = 'fooValue'
     * $query->filterByMoney('%fooValue%'); // WHERE MONEY LIKE '%fooValue%'
     * </code>
     *
     * @param     string $money The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function filterByMoney($money = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($money)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $money)) {
                $money = str_replace('*', '%', $money);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductAcquisitionPeer::MONEY, $money, $comparison);
    }

    /**
     * Filter the query on the WARRANTY_PROVIDER column
     *
     * Example usage:
     * <code>
     * $query->filterByWarrantyProvider('fooValue');   // WHERE WARRANTY_PROVIDER = 'fooValue'
     * $query->filterByWarrantyProvider('%fooValue%'); // WHERE WARRANTY_PROVIDER LIKE '%fooValue%'
     * </code>
     *
     * @param     string $warrantyProvider The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function filterByWarrantyProvider($warrantyProvider = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($warrantyProvider)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $warrantyProvider)) {
                $warrantyProvider = str_replace('*', '%', $warrantyProvider);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductAcquisitionPeer::WARRANTY_PROVIDER, $warrantyProvider, $comparison);
    }

    /**
     * Filter the query by a related SelAcquisition object
     *
     * @param   SelAcquisition|PropelObjectCollection $selAcquisition The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductAcquisitionQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelAcquisition($selAcquisition, $comparison = null)
    {
        if ($selAcquisition instanceof SelAcquisition) {
            return $this
                ->addUsingAlias(SelProductAcquisitionPeer::AQUISITION_ID, $selAcquisition->getId(), $comparison);
        } elseif ($selAcquisition instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductAcquisitionPeer::AQUISITION_ID, $selAcquisition->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelAcquisition() only accepts arguments of type SelAcquisition or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelAcquisition relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function joinSelAcquisition($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelAcquisition');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelAcquisition');
        }

        return $this;
    }

    /**
     * Use the SelAcquisition relation SelAcquisition object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelAcquisitionQuery A secondary query class using the current class as primary query
     */
    public function useSelAcquisitionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelAcquisition($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelAcquisition', 'SelAcquisitionQuery');
    }

    /**
     * Filter the query by a related SelProduct object
     *
     * @param   SelProduct|PropelObjectCollection $selProduct The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductAcquisitionQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProduct($selProduct, $comparison = null)
    {
        if ($selProduct instanceof SelProduct) {
            return $this
                ->addUsingAlias(SelProductAcquisitionPeer::PRODUCT_ID, $selProduct->getId(), $comparison);
        } elseif ($selProduct instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductAcquisitionPeer::PRODUCT_ID, $selProduct->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelProduct() only accepts arguments of type SelProduct or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProduct relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function joinSelProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProduct');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProduct');
        }

        return $this;
    }

    /**
     * Use the SelProduct relation SelProduct object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductQuery A secondary query class using the current class as primary query
     */
    public function useSelProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProduct', 'SelProductQuery');
    }

    /**
     * Filter the query by a related SelProductItem object
     *
     * @param   SelProductItem|PropelObjectCollection $selProductItem  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductAcquisitionQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductItem($selProductItem, $comparison = null)
    {
        if ($selProductItem instanceof SelProductItem) {
            return $this
                ->addUsingAlias(SelProductAcquisitionPeer::ID, $selProductItem->getProductAcquisitionId(), $comparison);
        } elseif ($selProductItem instanceof PropelObjectCollection) {
            return $this
                ->useSelProductItemQuery()
                ->filterByPrimaryKeys($selProductItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductItem() only accepts arguments of type SelProductItem or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function joinSelProductItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductItem');
        }

        return $this;
    }

    /**
     * Use the SelProductItem relation SelProductItem object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductItemQuery A secondary query class using the current class as primary query
     */
    public function useSelProductItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductItem', 'SelProductItemQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelProductAcquisition $selProductAcquisition Object to remove from the list of results
     *
     * @return SelProductAcquisitionQuery The current query, for fluid interface
     */
    public function prune($selProductAcquisition = null)
    {
        if ($selProductAcquisition) {
            $this->addUsingAlias(SelProductAcquisitionPeer::ID, $selProductAcquisition->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
