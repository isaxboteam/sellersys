<?php


/**
 * Base static class for performing query and update operations on the 'sel_product' table.
 *
 * 
 *
 * @package propel.generator.system.om
 */
abstract class BaseSelProductPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'system';

    /** the table name for this class */
    const TABLE_NAME = 'sel_product';

    /** the related Propel class for this table */
    const OM_CLASS = 'SelProduct';

    /** the related TableMap class for this table */
    const TM_CLASS = 'SelProductTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 14;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 14;

    /** the column name for the ID field */
    const ID = 'sel_product.ID';

    /** the column name for the PRODUCT_GROUP_ID field */
    const PRODUCT_GROUP_ID = 'sel_product.PRODUCT_GROUP_ID';

    /** the column name for the BRAND_ID field */
    const BRAND_ID = 'sel_product.BRAND_ID';

    /** the column name for the CATEGORY_ID field */
    const CATEGORY_ID = 'sel_product.CATEGORY_ID';

    /** the column name for the MAIN_IMAGE_ID field */
    const MAIN_IMAGE_ID = 'sel_product.MAIN_IMAGE_ID';

    /** the column name for the CODE field */
    const CODE = 'sel_product.CODE';

    /** the column name for the NAME field */
    const NAME = 'sel_product.NAME';

    /** the column name for the BASE_PRICE field */
    const BASE_PRICE = 'sel_product.BASE_PRICE';

    /** the column name for the DESCRIPTION field */
    const DESCRIPTION = 'sel_product.DESCRIPTION';

    /** the column name for the MODEL field */
    const MODEL = 'sel_product.MODEL';

    /** the column name for the NUMBER_PARTS field */
    const NUMBER_PARTS = 'sel_product.NUMBER_PARTS';

    /** the column name for the IS_MAIN field */
    const IS_MAIN = 'sel_product.IS_MAIN';

    /** the column name for the IS_NEW field */
    const IS_NEW = 'sel_product.IS_NEW';

    /** the column name for the IS_OFFER field */
    const IS_OFFER = 'sel_product.IS_OFFER';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of SelProduct objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array SelProduct[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. SelProductPeer::$fieldNames[SelProductPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'ProductGroupId', 'BrandId', 'CategoryId', 'MainImageId', 'Code', 'Name', 'BasePrice', 'Description', 'Model', 'NumberParts', 'IsMain', 'IsNew', 'IsOffer', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'productGroupId', 'brandId', 'categoryId', 'mainImageId', 'code', 'name', 'basePrice', 'description', 'model', 'numberParts', 'isMain', 'isNew', 'isOffer', ),
        BasePeer::TYPE_COLNAME => array (SelProductPeer::ID, SelProductPeer::PRODUCT_GROUP_ID, SelProductPeer::BRAND_ID, SelProductPeer::CATEGORY_ID, SelProductPeer::MAIN_IMAGE_ID, SelProductPeer::CODE, SelProductPeer::NAME, SelProductPeer::BASE_PRICE, SelProductPeer::DESCRIPTION, SelProductPeer::MODEL, SelProductPeer::NUMBER_PARTS, SelProductPeer::IS_MAIN, SelProductPeer::IS_NEW, SelProductPeer::IS_OFFER, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'PRODUCT_GROUP_ID', 'BRAND_ID', 'CATEGORY_ID', 'MAIN_IMAGE_ID', 'CODE', 'NAME', 'BASE_PRICE', 'DESCRIPTION', 'MODEL', 'NUMBER_PARTS', 'IS_MAIN', 'IS_NEW', 'IS_OFFER', ),
        BasePeer::TYPE_FIELDNAME => array ('ID', 'PRODUCT_GROUP_ID', 'BRAND_ID', 'CATEGORY_ID', 'MAIN_IMAGE_ID', 'CODE', 'NAME', 'BASE_PRICE', 'DESCRIPTION', 'MODEL', 'NUMBER_PARTS', 'IS_MAIN', 'IS_NEW', 'IS_OFFER', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. SelProductPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ProductGroupId' => 1, 'BrandId' => 2, 'CategoryId' => 3, 'MainImageId' => 4, 'Code' => 5, 'Name' => 6, 'BasePrice' => 7, 'Description' => 8, 'Model' => 9, 'NumberParts' => 10, 'IsMain' => 11, 'IsNew' => 12, 'IsOffer' => 13, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'productGroupId' => 1, 'brandId' => 2, 'categoryId' => 3, 'mainImageId' => 4, 'code' => 5, 'name' => 6, 'basePrice' => 7, 'description' => 8, 'model' => 9, 'numberParts' => 10, 'isMain' => 11, 'isNew' => 12, 'isOffer' => 13, ),
        BasePeer::TYPE_COLNAME => array (SelProductPeer::ID => 0, SelProductPeer::PRODUCT_GROUP_ID => 1, SelProductPeer::BRAND_ID => 2, SelProductPeer::CATEGORY_ID => 3, SelProductPeer::MAIN_IMAGE_ID => 4, SelProductPeer::CODE => 5, SelProductPeer::NAME => 6, SelProductPeer::BASE_PRICE => 7, SelProductPeer::DESCRIPTION => 8, SelProductPeer::MODEL => 9, SelProductPeer::NUMBER_PARTS => 10, SelProductPeer::IS_MAIN => 11, SelProductPeer::IS_NEW => 12, SelProductPeer::IS_OFFER => 13, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'PRODUCT_GROUP_ID' => 1, 'BRAND_ID' => 2, 'CATEGORY_ID' => 3, 'MAIN_IMAGE_ID' => 4, 'CODE' => 5, 'NAME' => 6, 'BASE_PRICE' => 7, 'DESCRIPTION' => 8, 'MODEL' => 9, 'NUMBER_PARTS' => 10, 'IS_MAIN' => 11, 'IS_NEW' => 12, 'IS_OFFER' => 13, ),
        BasePeer::TYPE_FIELDNAME => array ('ID' => 0, 'PRODUCT_GROUP_ID' => 1, 'BRAND_ID' => 2, 'CATEGORY_ID' => 3, 'MAIN_IMAGE_ID' => 4, 'CODE' => 5, 'NAME' => 6, 'BASE_PRICE' => 7, 'DESCRIPTION' => 8, 'MODEL' => 9, 'NUMBER_PARTS' => 10, 'IS_MAIN' => 11, 'IS_NEW' => 12, 'IS_OFFER' => 13, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = SelProductPeer::getFieldNames($toType);
        $key = isset(SelProductPeer::$fieldKeys[$fromType][$name]) ? SelProductPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(SelProductPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, SelProductPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return SelProductPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. SelProductPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(SelProductPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SelProductPeer::ID);
            $criteria->addSelectColumn(SelProductPeer::PRODUCT_GROUP_ID);
            $criteria->addSelectColumn(SelProductPeer::BRAND_ID);
            $criteria->addSelectColumn(SelProductPeer::CATEGORY_ID);
            $criteria->addSelectColumn(SelProductPeer::MAIN_IMAGE_ID);
            $criteria->addSelectColumn(SelProductPeer::CODE);
            $criteria->addSelectColumn(SelProductPeer::NAME);
            $criteria->addSelectColumn(SelProductPeer::BASE_PRICE);
            $criteria->addSelectColumn(SelProductPeer::DESCRIPTION);
            $criteria->addSelectColumn(SelProductPeer::MODEL);
            $criteria->addSelectColumn(SelProductPeer::NUMBER_PARTS);
            $criteria->addSelectColumn(SelProductPeer::IS_MAIN);
            $criteria->addSelectColumn(SelProductPeer::IS_NEW);
            $criteria->addSelectColumn(SelProductPeer::IS_OFFER);
        } else {
            $criteria->addSelectColumn($alias . '.ID');
            $criteria->addSelectColumn($alias . '.PRODUCT_GROUP_ID');
            $criteria->addSelectColumn($alias . '.BRAND_ID');
            $criteria->addSelectColumn($alias . '.CATEGORY_ID');
            $criteria->addSelectColumn($alias . '.MAIN_IMAGE_ID');
            $criteria->addSelectColumn($alias . '.CODE');
            $criteria->addSelectColumn($alias . '.NAME');
            $criteria->addSelectColumn($alias . '.BASE_PRICE');
            $criteria->addSelectColumn($alias . '.DESCRIPTION');
            $criteria->addSelectColumn($alias . '.MODEL');
            $criteria->addSelectColumn($alias . '.NUMBER_PARTS');
            $criteria->addSelectColumn($alias . '.IS_MAIN');
            $criteria->addSelectColumn($alias . '.IS_NEW');
            $criteria->addSelectColumn($alias . '.IS_OFFER');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(SelProductPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 SelProduct
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = SelProductPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return SelProductPeer::populateObjects(SelProductPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            SelProductPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      SelProduct $obj A SelProduct object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            SelProductPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A SelProduct object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof SelProduct) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or SelProduct object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(SelProductPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   SelProduct Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(SelProductPeer::$instances[$key])) {
                return SelProductPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool()
    {
        SelProductPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to sel_product
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = SelProductPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = SelProductPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = SelProductPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SelProductPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (SelProduct object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = SelProductPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = SelProductPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + SelProductPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SelProductPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            SelProductPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelCategory table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSelCategory(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelProductPeer::CATEGORY_ID, SelCategoryPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelProductGroup table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSelProductGroup(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelProductPeer::PRODUCT_GROUP_ID, SelProductGroupPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelBrand table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSelBrand(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelProductPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelProductImageRelatedByMainImageId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSelProductImageRelatedByMainImageId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelProductPeer::MAIN_IMAGE_ID, SelProductImagePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of SelProduct objects pre-filled with their SelCategory objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProduct objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSelCategory(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductPeer::DATABASE_NAME);
        }

        SelProductPeer::addSelectColumns($criteria);
        $startcol = SelProductPeer::NUM_HYDRATE_COLUMNS;
        SelCategoryPeer::addSelectColumns($criteria);

        $criteria->addJoin(SelProductPeer::CATEGORY_ID, SelCategoryPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SelProductPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SelCategoryPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SelCategoryPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelCategoryPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SelCategoryPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SelProduct) to $obj2 (SelCategory)
                $obj2->addSelProduct($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProduct objects pre-filled with their SelProductGroup objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProduct objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSelProductGroup(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductPeer::DATABASE_NAME);
        }

        SelProductPeer::addSelectColumns($criteria);
        $startcol = SelProductPeer::NUM_HYDRATE_COLUMNS;
        SelProductGroupPeer::addSelectColumns($criteria);

        $criteria->addJoin(SelProductPeer::PRODUCT_GROUP_ID, SelProductGroupPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SelProductPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SelProductGroupPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SelProductGroupPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelProductGroupPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SelProductGroupPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SelProduct) to $obj2 (SelProductGroup)
                $obj2->addSelProduct($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProduct objects pre-filled with their SelBrand objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProduct objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSelBrand(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductPeer::DATABASE_NAME);
        }

        SelProductPeer::addSelectColumns($criteria);
        $startcol = SelProductPeer::NUM_HYDRATE_COLUMNS;
        SelBrandPeer::addSelectColumns($criteria);

        $criteria->addJoin(SelProductPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SelProductPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SelBrandPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SelBrandPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelBrandPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SelBrandPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SelProduct) to $obj2 (SelBrand)
                $obj2->addSelProduct($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProduct objects pre-filled with their SelProductImage objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProduct objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSelProductImageRelatedByMainImageId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductPeer::DATABASE_NAME);
        }

        SelProductPeer::addSelectColumns($criteria);
        $startcol = SelProductPeer::NUM_HYDRATE_COLUMNS;
        SelProductImagePeer::addSelectColumns($criteria);

        $criteria->addJoin(SelProductPeer::MAIN_IMAGE_ID, SelProductImagePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SelProductPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SelProductImagePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SelProductImagePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelProductImagePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SelProductImagePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (SelProduct) to $obj2 (SelProductImage)
                $obj2->addSelProductRelatedByMainImageId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SelProductPeer::CATEGORY_ID, SelCategoryPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::PRODUCT_GROUP_ID, SelProductGroupPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::MAIN_IMAGE_ID, SelProductImagePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of SelProduct objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProduct objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductPeer::DATABASE_NAME);
        }

        SelProductPeer::addSelectColumns($criteria);
        $startcol2 = SelProductPeer::NUM_HYDRATE_COLUMNS;

        SelCategoryPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelCategoryPeer::NUM_HYDRATE_COLUMNS;

        SelProductGroupPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SelProductGroupPeer::NUM_HYDRATE_COLUMNS;

        SelBrandPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SelBrandPeer::NUM_HYDRATE_COLUMNS;

        SelProductImagePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SelProductImagePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelProductPeer::CATEGORY_ID, SelCategoryPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::PRODUCT_GROUP_ID, SelProductGroupPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::MAIN_IMAGE_ID, SelProductImagePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelProductPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined SelCategory rows

            $key2 = SelCategoryPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = SelCategoryPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SelCategoryPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelCategoryPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (SelProduct) to the collection in $obj2 (SelCategory)
                $obj2->addSelProduct($obj1);
            } // if joined row not null

            // Add objects for joined SelProductGroup rows

            $key3 = SelProductGroupPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = SelProductGroupPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = SelProductGroupPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SelProductGroupPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (SelProduct) to the collection in $obj3 (SelProductGroup)
                $obj3->addSelProduct($obj1);
            } // if joined row not null

            // Add objects for joined SelBrand rows

            $key4 = SelBrandPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = SelBrandPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = SelBrandPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SelBrandPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (SelProduct) to the collection in $obj4 (SelBrand)
                $obj4->addSelProduct($obj1);
            } // if joined row not null

            // Add objects for joined SelProductImage rows

            $key5 = SelProductImagePeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = SelProductImagePeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = SelProductImagePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SelProductImagePeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (SelProduct) to the collection in $obj5 (SelProductImage)
                $obj5->addSelProductRelatedByMainImageId($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelCategory table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSelCategory(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SelProductPeer::PRODUCT_GROUP_ID, SelProductGroupPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::MAIN_IMAGE_ID, SelProductImagePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelProductGroup table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSelProductGroup(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SelProductPeer::CATEGORY_ID, SelCategoryPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::MAIN_IMAGE_ID, SelProductImagePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelBrand table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSelBrand(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SelProductPeer::CATEGORY_ID, SelCategoryPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::PRODUCT_GROUP_ID, SelProductGroupPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::MAIN_IMAGE_ID, SelProductImagePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SelProductImageRelatedByMainImageId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSelProductImageRelatedByMainImageId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SelProductPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SelProductPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
    
        $criteria->addJoin(SelProductPeer::CATEGORY_ID, SelCategoryPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::PRODUCT_GROUP_ID, SelProductGroupPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of SelProduct objects pre-filled with all related objects except SelCategory.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProduct objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSelCategory(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductPeer::DATABASE_NAME);
        }

        SelProductPeer::addSelectColumns($criteria);
        $startcol2 = SelProductPeer::NUM_HYDRATE_COLUMNS;

        SelProductGroupPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelProductGroupPeer::NUM_HYDRATE_COLUMNS;

        SelBrandPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SelBrandPeer::NUM_HYDRATE_COLUMNS;

        SelProductImagePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SelProductImagePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelProductPeer::PRODUCT_GROUP_ID, SelProductGroupPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::MAIN_IMAGE_ID, SelProductImagePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelProductPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SelProductGroup rows

                $key2 = SelProductGroupPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SelProductGroupPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SelProductGroupPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelProductGroupPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj2 (SelProductGroup)
                $obj2->addSelProduct($obj1);

            } // if joined row is not null

                // Add objects for joined SelBrand rows

                $key3 = SelBrandPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SelBrandPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SelBrandPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SelBrandPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj3 (SelBrand)
                $obj3->addSelProduct($obj1);

            } // if joined row is not null

                // Add objects for joined SelProductImage rows

                $key4 = SelProductImagePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SelProductImagePeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SelProductImagePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SelProductImagePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj4 (SelProductImage)
                $obj4->addSelProductRelatedByMainImageId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProduct objects pre-filled with all related objects except SelProductGroup.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProduct objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSelProductGroup(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductPeer::DATABASE_NAME);
        }

        SelProductPeer::addSelectColumns($criteria);
        $startcol2 = SelProductPeer::NUM_HYDRATE_COLUMNS;

        SelCategoryPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelCategoryPeer::NUM_HYDRATE_COLUMNS;

        SelBrandPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SelBrandPeer::NUM_HYDRATE_COLUMNS;

        SelProductImagePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SelProductImagePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelProductPeer::CATEGORY_ID, SelCategoryPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::MAIN_IMAGE_ID, SelProductImagePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelProductPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SelCategory rows

                $key2 = SelCategoryPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SelCategoryPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SelCategoryPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelCategoryPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj2 (SelCategory)
                $obj2->addSelProduct($obj1);

            } // if joined row is not null

                // Add objects for joined SelBrand rows

                $key3 = SelBrandPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SelBrandPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SelBrandPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SelBrandPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj3 (SelBrand)
                $obj3->addSelProduct($obj1);

            } // if joined row is not null

                // Add objects for joined SelProductImage rows

                $key4 = SelProductImagePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SelProductImagePeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SelProductImagePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SelProductImagePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj4 (SelProductImage)
                $obj4->addSelProductRelatedByMainImageId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProduct objects pre-filled with all related objects except SelBrand.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProduct objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSelBrand(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductPeer::DATABASE_NAME);
        }

        SelProductPeer::addSelectColumns($criteria);
        $startcol2 = SelProductPeer::NUM_HYDRATE_COLUMNS;

        SelCategoryPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelCategoryPeer::NUM_HYDRATE_COLUMNS;

        SelProductGroupPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SelProductGroupPeer::NUM_HYDRATE_COLUMNS;

        SelProductImagePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SelProductImagePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelProductPeer::CATEGORY_ID, SelCategoryPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::PRODUCT_GROUP_ID, SelProductGroupPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::MAIN_IMAGE_ID, SelProductImagePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelProductPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SelCategory rows

                $key2 = SelCategoryPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SelCategoryPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SelCategoryPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelCategoryPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj2 (SelCategory)
                $obj2->addSelProduct($obj1);

            } // if joined row is not null

                // Add objects for joined SelProductGroup rows

                $key3 = SelProductGroupPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SelProductGroupPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SelProductGroupPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SelProductGroupPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj3 (SelProductGroup)
                $obj3->addSelProduct($obj1);

            } // if joined row is not null

                // Add objects for joined SelProductImage rows

                $key4 = SelProductImagePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SelProductImagePeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SelProductImagePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SelProductImagePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj4 (SelProductImage)
                $obj4->addSelProductRelatedByMainImageId($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of SelProduct objects pre-filled with all related objects except SelProductImageRelatedByMainImageId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of SelProduct objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSelProductImageRelatedByMainImageId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SelProductPeer::DATABASE_NAME);
        }

        SelProductPeer::addSelectColumns($criteria);
        $startcol2 = SelProductPeer::NUM_HYDRATE_COLUMNS;

        SelCategoryPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SelCategoryPeer::NUM_HYDRATE_COLUMNS;

        SelProductGroupPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SelProductGroupPeer::NUM_HYDRATE_COLUMNS;

        SelBrandPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SelBrandPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SelProductPeer::CATEGORY_ID, SelCategoryPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::PRODUCT_GROUP_ID, SelProductGroupPeer::ID, $join_behavior);

        $criteria->addJoin(SelProductPeer::BRAND_ID, SelBrandPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SelProductPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SelProductPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SelProductPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SelProductPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SelCategory rows

                $key2 = SelCategoryPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SelCategoryPeer::getInstanceFromPool($key2);
                    if (!$obj2) {
    
                        $cls = SelCategoryPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SelCategoryPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj2 (SelCategory)
                $obj2->addSelProduct($obj1);

            } // if joined row is not null

                // Add objects for joined SelProductGroup rows

                $key3 = SelProductGroupPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SelProductGroupPeer::getInstanceFromPool($key3);
                    if (!$obj3) {
    
                        $cls = SelProductGroupPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SelProductGroupPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj3 (SelProductGroup)
                $obj3->addSelProduct($obj1);

            } // if joined row is not null

                // Add objects for joined SelBrand rows

                $key4 = SelBrandPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SelBrandPeer::getInstanceFromPool($key4);
                    if (!$obj4) {
    
                        $cls = SelBrandPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SelBrandPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (SelProduct) to the collection in $obj4 (SelBrand)
                $obj4->addSelProduct($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(SelProductPeer::DATABASE_NAME)->getTable(SelProductPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseSelProductPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseSelProductPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new SelProductTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return SelProductPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a SelProduct or Criteria object.
     *
     * @param      mixed $values Criteria or SelProduct object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from SelProduct object
        }

        if ($criteria->containsKey(SelProductPeer::ID) && $criteria->keyContainsValue(SelProductPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SelProductPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a SelProduct or Criteria object.
     *
     * @param      mixed $values Criteria or SelProduct object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(SelProductPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(SelProductPeer::ID);
            $value = $criteria->remove(SelProductPeer::ID);
            if ($value) {
                $selectCriteria->add(SelProductPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(SelProductPeer::TABLE_NAME);
            }

        } else { // $values is SelProduct object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the sel_product table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(SelProductPeer::TABLE_NAME, $con, SelProductPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SelProductPeer::clearInstancePool();
            SelProductPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a SelProduct or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or SelProduct object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            SelProductPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof SelProduct) { // it's a model object
            // invalidate the cache for this single object
            SelProductPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SelProductPeer::DATABASE_NAME);
            $criteria->add(SelProductPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                SelProductPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(SelProductPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            SelProductPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given SelProduct object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      SelProduct $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(SelProductPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(SelProductPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(SelProductPeer::DATABASE_NAME, SelProductPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return SelProduct
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = SelProductPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(SelProductPeer::DATABASE_NAME);
        $criteria->add(SelProductPeer::ID, $pk);

        $v = SelProductPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return SelProduct[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(SelProductPeer::DATABASE_NAME);
            $criteria->add(SelProductPeer::ID, $pks, Criteria::IN);
            $objs = SelProductPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseSelProductPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseSelProductPeer::buildTableMap();

