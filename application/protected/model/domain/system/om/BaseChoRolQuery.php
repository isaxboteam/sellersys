<?php


/**
 * Base class that represents a query for the 'cho_rol' table.
 *
 * 
 *
 * @method ChoRolQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method ChoRolQuery orderByCode($order = Criteria::ASC) Order by the CODE column
 * @method ChoRolQuery orderByName($order = Criteria::ASC) Order by the NAME column
 * @method ChoRolQuery orderByDescription($order = Criteria::ASC) Order by the DESCRIPTION column
 *
 * @method ChoRolQuery groupById() Group by the ID column
 * @method ChoRolQuery groupByCode() Group by the CODE column
 * @method ChoRolQuery groupByName() Group by the NAME column
 * @method ChoRolQuery groupByDescription() Group by the DESCRIPTION column
 *
 * @method ChoRolQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ChoRolQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ChoRolQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ChoRolQuery leftJoinChoRolXUri($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChoRolXUri relation
 * @method ChoRolQuery rightJoinChoRolXUri($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChoRolXUri relation
 * @method ChoRolQuery innerJoinChoRolXUri($relationAlias = null) Adds a INNER JOIN clause to the query using the ChoRolXUri relation
 *
 * @method ChoRolQuery leftJoinChoUserXRol($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChoUserXRol relation
 * @method ChoRolQuery rightJoinChoUserXRol($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChoUserXRol relation
 * @method ChoRolQuery innerJoinChoUserXRol($relationAlias = null) Adds a INNER JOIN clause to the query using the ChoUserXRol relation
 *
 * @method ChoRol findOne(PropelPDO $con = null) Return the first ChoRol matching the query
 * @method ChoRol findOneOrCreate(PropelPDO $con = null) Return the first ChoRol matching the query, or a new ChoRol object populated from the query conditions when no match is found
 *
 * @method ChoRol findOneByCode(string $CODE) Return the first ChoRol filtered by the CODE column
 * @method ChoRol findOneByName(string $NAME) Return the first ChoRol filtered by the NAME column
 * @method ChoRol findOneByDescription(string $DESCRIPTION) Return the first ChoRol filtered by the DESCRIPTION column
 *
 * @method array findById(int $ID) Return ChoRol objects filtered by the ID column
 * @method array findByCode(string $CODE) Return ChoRol objects filtered by the CODE column
 * @method array findByName(string $NAME) Return ChoRol objects filtered by the NAME column
 * @method array findByDescription(string $DESCRIPTION) Return ChoRol objects filtered by the DESCRIPTION column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseChoRolQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseChoRolQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'ChoRol', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChoRolQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     ChoRolQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChoRolQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ChoRolQuery) {
            return $criteria;
        }
        $query = new ChoRolQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ChoRol|ChoRol[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ChoRolPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ChoRolPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   ChoRol A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   ChoRol A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `CODE`, `NAME`, `DESCRIPTION` FROM `cho_rol` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ChoRol();
            $obj->hydrate($row);
            ChoRolPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ChoRol|ChoRol[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ChoRol[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ChoRolQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ChoRolPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ChoRolQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ChoRolPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoRolQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ChoRolPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the CODE column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE CODE = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE CODE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoRolQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoRolPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the NAME column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE NAME = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE NAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoRolQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoRolPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the DESCRIPTION column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE DESCRIPTION = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE DESCRIPTION LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoRolQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChoRolPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query by a related ChoRolXUri object
     *
     * @param   ChoRolXUri|PropelObjectCollection $choRolXUri  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ChoRolQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByChoRolXUri($choRolXUri, $comparison = null)
    {
        if ($choRolXUri instanceof ChoRolXUri) {
            return $this
                ->addUsingAlias(ChoRolPeer::ID, $choRolXUri->getRolId(), $comparison);
        } elseif ($choRolXUri instanceof PropelObjectCollection) {
            return $this
                ->useChoRolXUriQuery()
                ->filterByPrimaryKeys($choRolXUri->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChoRolXUri() only accepts arguments of type ChoRolXUri or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChoRolXUri relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ChoRolQuery The current query, for fluid interface
     */
    public function joinChoRolXUri($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChoRolXUri');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChoRolXUri');
        }

        return $this;
    }

    /**
     * Use the ChoRolXUri relation ChoRolXUri object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ChoRolXUriQuery A secondary query class using the current class as primary query
     */
    public function useChoRolXUriQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChoRolXUri($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChoRolXUri', 'ChoRolXUriQuery');
    }

    /**
     * Filter the query by a related ChoUserXRol object
     *
     * @param   ChoUserXRol|PropelObjectCollection $choUserXRol  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ChoRolQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByChoUserXRol($choUserXRol, $comparison = null)
    {
        if ($choUserXRol instanceof ChoUserXRol) {
            return $this
                ->addUsingAlias(ChoRolPeer::ID, $choUserXRol->getRolId(), $comparison);
        } elseif ($choUserXRol instanceof PropelObjectCollection) {
            return $this
                ->useChoUserXRolQuery()
                ->filterByPrimaryKeys($choUserXRol->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChoUserXRol() only accepts arguments of type ChoUserXRol or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChoUserXRol relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ChoRolQuery The current query, for fluid interface
     */
    public function joinChoUserXRol($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChoUserXRol');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChoUserXRol');
        }

        return $this;
    }

    /**
     * Use the ChoUserXRol relation ChoUserXRol object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ChoUserXRolQuery A secondary query class using the current class as primary query
     */
    public function useChoUserXRolQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChoUserXRol($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChoUserXRol', 'ChoUserXRolQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChoRol $choRol Object to remove from the list of results
     *
     * @return ChoRolQuery The current query, for fluid interface
     */
    public function prune($choRol = null)
    {
        if ($choRol) {
            $this->addUsingAlias(ChoRolPeer::ID, $choRol->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
