<?php


/**
 * Base class that represents a row from the 'sel_product_acquisition' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProductAcquisition extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelProductAcquisitionPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelProductAcquisitionPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the aquisition_id field.
     * @var        int
     */
    protected $aquisition_id;

    /**
     * The value for the product_id field.
     * @var        int
     */
    protected $product_id;

    /**
     * The value for the quantity field.
     * @var        int
     */
    protected $quantity;

    /**
     * The value for the price field.
     * @var        double
     */
    protected $price;

    /**
     * The value for the money field.
     * @var        string
     */
    protected $money;

    /**
     * The value for the warranty_provider field.
     * @var        string
     */
    protected $warranty_provider;

    /**
     * @var        SelAcquisition
     */
    protected $aSelAcquisition;

    /**
     * @var        SelProduct
     */
    protected $aSelProduct;

    /**
     * @var        PropelObjectCollection|SelProductItem[] Collection to store aggregation of SelProductItem objects.
     */
    protected $collSelProductItems;
    protected $collSelProductItemsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProductItemsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [aquisition_id] column value.
     * 
     * @return int
     */
    public function getAquisitionId()
    {
        return $this->aquisition_id;
    }

    /**
     * Get the [product_id] column value.
     * 
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * Get the [quantity] column value.
     * 
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Get the [price] column value.
     * 
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get the [money] column value.
     * 
     * @return string
     */
    public function getMoney()
    {
        return $this->money;
    }

    /**
     * Get the [warranty_provider] column value.
     * 
     * @return string
     */
    public function getWarrantyProvider()
    {
        return $this->warranty_provider;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelProductAcquisition The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelProductAcquisitionPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [aquisition_id] column.
     * 
     * @param int $v new value
     * @return SelProductAcquisition The current object (for fluent API support)
     */
    public function setAquisitionId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->aquisition_id !== $v) {
            $this->aquisition_id = $v;
            $this->modifiedColumns[] = SelProductAcquisitionPeer::AQUISITION_ID;
        }

        if ($this->aSelAcquisition !== null && $this->aSelAcquisition->getId() !== $v) {
            $this->aSelAcquisition = null;
        }


        return $this;
    } // setAquisitionId()

    /**
     * Set the value of [product_id] column.
     * 
     * @param int $v new value
     * @return SelProductAcquisition The current object (for fluent API support)
     */
    public function setProductId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->product_id !== $v) {
            $this->product_id = $v;
            $this->modifiedColumns[] = SelProductAcquisitionPeer::PRODUCT_ID;
        }

        if ($this->aSelProduct !== null && $this->aSelProduct->getId() !== $v) {
            $this->aSelProduct = null;
        }


        return $this;
    } // setProductId()

    /**
     * Set the value of [quantity] column.
     * 
     * @param int $v new value
     * @return SelProductAcquisition The current object (for fluent API support)
     */
    public function setQuantity($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->quantity !== $v) {
            $this->quantity = $v;
            $this->modifiedColumns[] = SelProductAcquisitionPeer::QUANTITY;
        }


        return $this;
    } // setQuantity()

    /**
     * Set the value of [price] column.
     * 
     * @param double $v new value
     * @return SelProductAcquisition The current object (for fluent API support)
     */
    public function setPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->price !== $v) {
            $this->price = $v;
            $this->modifiedColumns[] = SelProductAcquisitionPeer::PRICE;
        }


        return $this;
    } // setPrice()

    /**
     * Set the value of [money] column.
     * 
     * @param string $v new value
     * @return SelProductAcquisition The current object (for fluent API support)
     */
    public function setMoney($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->money !== $v) {
            $this->money = $v;
            $this->modifiedColumns[] = SelProductAcquisitionPeer::MONEY;
        }


        return $this;
    } // setMoney()

    /**
     * Set the value of [warranty_provider] column.
     * 
     * @param string $v new value
     * @return SelProductAcquisition The current object (for fluent API support)
     */
    public function setWarrantyProvider($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->warranty_provider !== $v) {
            $this->warranty_provider = $v;
            $this->modifiedColumns[] = SelProductAcquisitionPeer::WARRANTY_PROVIDER;
        }


        return $this;
    } // setWarrantyProvider()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->aquisition_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->product_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->quantity = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->price = ($row[$startcol + 4] !== null) ? (double) $row[$startcol + 4] : null;
            $this->money = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->warranty_provider = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 7; // 7 = SelProductAcquisitionPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelProductAcquisition object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSelAcquisition !== null && $this->aquisition_id !== $this->aSelAcquisition->getId()) {
            $this->aSelAcquisition = null;
        }
        if ($this->aSelProduct !== null && $this->product_id !== $this->aSelProduct->getId()) {
            $this->aSelProduct = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductAcquisitionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelProductAcquisitionPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSelAcquisition = null;
            $this->aSelProduct = null;
            $this->collSelProductItems = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductAcquisitionPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelProductAcquisitionQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelProductAcquisitionPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelProductAcquisitionPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelAcquisition !== null) {
                if ($this->aSelAcquisition->isModified() || $this->aSelAcquisition->isNew()) {
                    $affectedRows += $this->aSelAcquisition->save($con);
                }
                $this->setSelAcquisition($this->aSelAcquisition);
            }

            if ($this->aSelProduct !== null) {
                if ($this->aSelProduct->isModified() || $this->aSelProduct->isNew()) {
                    $affectedRows += $this->aSelProduct->save($con);
                }
                $this->setSelProduct($this->aSelProduct);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->selProductItemsScheduledForDeletion !== null) {
                if (!$this->selProductItemsScheduledForDeletion->isEmpty()) {
                    SelProductItemQuery::create()
                        ->filterByPrimaryKeys($this->selProductItemsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->selProductItemsScheduledForDeletion = null;
                }
            }

            if ($this->collSelProductItems !== null) {
                foreach ($this->collSelProductItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SelProductAcquisitionPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SelProductAcquisitionPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelProductAcquisitionPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelProductAcquisitionPeer::AQUISITION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`AQUISITION_ID`';
        }
        if ($this->isColumnModified(SelProductAcquisitionPeer::PRODUCT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`PRODUCT_ID`';
        }
        if ($this->isColumnModified(SelProductAcquisitionPeer::QUANTITY)) {
            $modifiedColumns[':p' . $index++]  = '`QUANTITY`';
        }
        if ($this->isColumnModified(SelProductAcquisitionPeer::PRICE)) {
            $modifiedColumns[':p' . $index++]  = '`PRICE`';
        }
        if ($this->isColumnModified(SelProductAcquisitionPeer::MONEY)) {
            $modifiedColumns[':p' . $index++]  = '`MONEY`';
        }
        if ($this->isColumnModified(SelProductAcquisitionPeer::WARRANTY_PROVIDER)) {
            $modifiedColumns[':p' . $index++]  = '`WARRANTY_PROVIDER`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_product_acquisition` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`AQUISITION_ID`':						
                        $stmt->bindValue($identifier, $this->aquisition_id, PDO::PARAM_INT);
                        break;
                    case '`PRODUCT_ID`':						
                        $stmt->bindValue($identifier, $this->product_id, PDO::PARAM_INT);
                        break;
                    case '`QUANTITY`':						
                        $stmt->bindValue($identifier, $this->quantity, PDO::PARAM_INT);
                        break;
                    case '`PRICE`':						
                        $stmt->bindValue($identifier, $this->price, PDO::PARAM_STR);
                        break;
                    case '`MONEY`':						
                        $stmt->bindValue($identifier, $this->money, PDO::PARAM_STR);
                        break;
                    case '`WARRANTY_PROVIDER`':						
                        $stmt->bindValue($identifier, $this->warranty_provider, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSelAcquisition !== null) {
                if (!$this->aSelAcquisition->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelAcquisition->getValidationFailures());
                }
            }

            if ($this->aSelProduct !== null) {
                if (!$this->aSelProduct->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSelProduct->getValidationFailures());
                }
            }

            if (($retval = SelProductAcquisitionPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelProductItems !== null) {
                    foreach ($this->collSelProductItems as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelProductAcquisitionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getAquisitionId();
                break;
            case 2:
                return $this->getProductId();
                break;
            case 3:
                return $this->getQuantity();
                break;
            case 4:
                return $this->getPrice();
                break;
            case 5:
                return $this->getMoney();
                break;
            case 6:
                return $this->getWarrantyProvider();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelProductAcquisition'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelProductAcquisition'][$this->getPrimaryKey()] = true;
        $keys = SelProductAcquisitionPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getAquisitionId(),
            $keys[2] => $this->getProductId(),
            $keys[3] => $this->getQuantity(),
            $keys[4] => $this->getPrice(),
            $keys[5] => $this->getMoney(),
            $keys[6] => $this->getWarrantyProvider(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aSelAcquisition) {
                $result['SelAcquisition'] = $this->aSelAcquisition->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSelProduct) {
                $result['SelProduct'] = $this->aSelProduct->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSelProductItems) {
                $result['SelProductItems'] = $this->collSelProductItems->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelProductAcquisitionPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setAquisitionId($value);
                break;
            case 2:
                $this->setProductId($value);
                break;
            case 3:
                $this->setQuantity($value);
                break;
            case 4:
                $this->setPrice($value);
                break;
            case 5:
                $this->setMoney($value);
                break;
            case 6:
                $this->setWarrantyProvider($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelProductAcquisitionPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setAquisitionId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setProductId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setQuantity($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPrice($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setMoney($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setWarrantyProvider($arr[$keys[6]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelProductAcquisitionPeer::DATABASE_NAME);

        if ($this->isColumnModified(SelProductAcquisitionPeer::ID)) $criteria->add(SelProductAcquisitionPeer::ID, $this->id);
        if ($this->isColumnModified(SelProductAcquisitionPeer::AQUISITION_ID)) $criteria->add(SelProductAcquisitionPeer::AQUISITION_ID, $this->aquisition_id);
        if ($this->isColumnModified(SelProductAcquisitionPeer::PRODUCT_ID)) $criteria->add(SelProductAcquisitionPeer::PRODUCT_ID, $this->product_id);
        if ($this->isColumnModified(SelProductAcquisitionPeer::QUANTITY)) $criteria->add(SelProductAcquisitionPeer::QUANTITY, $this->quantity);
        if ($this->isColumnModified(SelProductAcquisitionPeer::PRICE)) $criteria->add(SelProductAcquisitionPeer::PRICE, $this->price);
        if ($this->isColumnModified(SelProductAcquisitionPeer::MONEY)) $criteria->add(SelProductAcquisitionPeer::MONEY, $this->money);
        if ($this->isColumnModified(SelProductAcquisitionPeer::WARRANTY_PROVIDER)) $criteria->add(SelProductAcquisitionPeer::WARRANTY_PROVIDER, $this->warranty_provider);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelProductAcquisitionPeer::DATABASE_NAME);
        $criteria->add(SelProductAcquisitionPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelProductAcquisition (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setAquisitionId($this->getAquisitionId());
        $copyObj->setProductId($this->getProductId());
        $copyObj->setQuantity($this->getQuantity());
        $copyObj->setPrice($this->getPrice());
        $copyObj->setMoney($this->getMoney());
        $copyObj->setWarrantyProvider($this->getWarrantyProvider());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelProductItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProductItem($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelProductAcquisition Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelProductAcquisitionPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelProductAcquisitionPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SelAcquisition object.
     *
     * @param             SelAcquisition $v
     * @return SelProductAcquisition The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelAcquisition(SelAcquisition $v = null)
    {
        if ($v === null) {
            $this->setAquisitionId(NULL);
        } else {
            $this->setAquisitionId($v->getId());
        }

        $this->aSelAcquisition = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelAcquisition object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProductAcquisition($this);
        }


        return $this;
    }


    /**
     * Get the associated SelAcquisition object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelAcquisition The associated SelAcquisition object.
     * @throws PropelException
     */
    public function getSelAcquisition(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelAcquisition === null && ($this->aquisition_id !== null) && $doQuery) {
            $this->aSelAcquisition = SelAcquisitionQuery::create()->findPk($this->aquisition_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelAcquisition->addSelProductAcquisitions($this);
             */
        }

        return $this->aSelAcquisition;
    }

    /**
     * Declares an association between this object and a SelProduct object.
     *
     * @param             SelProduct $v
     * @return SelProductAcquisition The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSelProduct(SelProduct $v = null)
    {
        if ($v === null) {
            $this->setProductId(NULL);
        } else {
            $this->setProductId($v->getId());
        }

        $this->aSelProduct = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SelProduct object, it will not be re-added.
        if ($v !== null) {
            $v->addSelProductAcquisition($this);
        }


        return $this;
    }


    /**
     * Get the associated SelProduct object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SelProduct The associated SelProduct object.
     * @throws PropelException
     */
    public function getSelProduct(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSelProduct === null && ($this->product_id !== null) && $doQuery) {
            $this->aSelProduct = SelProductQuery::create()->findPk($this->product_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSelProduct->addSelProductAcquisitions($this);
             */
        }

        return $this->aSelProduct;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelProductItem' == $relationName) {
            $this->initSelProductItems();
        }
    }

    /**
     * Clears out the collSelProductItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelProductAcquisition The current object (for fluent API support)
     * @see        addSelProductItems()
     */
    public function clearSelProductItems()
    {
        $this->collSelProductItems = null; // important to set this to null since that means it is uninitialized
        $this->collSelProductItemsPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProductItems collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProductItems($v = true)
    {
        $this->collSelProductItemsPartial = $v;
    }

    /**
     * Initializes the collSelProductItems collection.
     *
     * By default this just sets the collSelProductItems collection to an empty array (like clearcollSelProductItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProductItems($overrideExisting = true)
    {
        if (null !== $this->collSelProductItems && !$overrideExisting) {
            return;
        }
        $this->collSelProductItems = new PropelObjectCollection();
        $this->collSelProductItems->setModel('SelProductItem');
    }

    /**
     * Gets an array of SelProductItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelProductAcquisition is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProductItem[] List of SelProductItem objects
     * @throws PropelException
     */
    public function getSelProductItems($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProductItemsPartial && !$this->isNew();
        if (null === $this->collSelProductItems || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProductItems) {
                // return empty collection
                $this->initSelProductItems();
            } else {
                $collSelProductItems = SelProductItemQuery::create(null, $criteria)
                    ->filterBySelProductAcquisition($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProductItemsPartial && count($collSelProductItems)) {
                      $this->initSelProductItems(false);

                      foreach($collSelProductItems as $obj) {
                        if (false == $this->collSelProductItems->contains($obj)) {
                          $this->collSelProductItems->append($obj);
                        }
                      }

                      $this->collSelProductItemsPartial = true;
                    }

                    return $collSelProductItems;
                }

                if($partial && $this->collSelProductItems) {
                    foreach($this->collSelProductItems as $obj) {
                        if($obj->isNew()) {
                            $collSelProductItems[] = $obj;
                        }
                    }
                }

                $this->collSelProductItems = $collSelProductItems;
                $this->collSelProductItemsPartial = false;
            }
        }

        return $this->collSelProductItems;
    }

    /**
     * Sets a collection of SelProductItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProductItems A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelProductAcquisition The current object (for fluent API support)
     */
    public function setSelProductItems(PropelCollection $selProductItems, PropelPDO $con = null)
    {
        $selProductItemsToDelete = $this->getSelProductItems(new Criteria(), $con)->diff($selProductItems);

        $this->selProductItemsScheduledForDeletion = unserialize(serialize($selProductItemsToDelete));

        foreach ($selProductItemsToDelete as $selProductItemRemoved) {
            $selProductItemRemoved->setSelProductAcquisition(null);
        }

        $this->collSelProductItems = null;
        foreach ($selProductItems as $selProductItem) {
            $this->addSelProductItem($selProductItem);
        }

        $this->collSelProductItems = $selProductItems;
        $this->collSelProductItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProductItem objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProductItem objects.
     * @throws PropelException
     */
    public function countSelProductItems(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProductItemsPartial && !$this->isNew();
        if (null === $this->collSelProductItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProductItems) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProductItems());
            }
            $query = SelProductItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelProductAcquisition($this)
                ->count($con);
        }

        return count($this->collSelProductItems);
    }

    /**
     * Method called to associate a SelProductItem object to this object
     * through the SelProductItem foreign key attribute.
     *
     * @param    SelProductItem $l SelProductItem
     * @return SelProductAcquisition The current object (for fluent API support)
     */
    public function addSelProductItem(SelProductItem $l)
    {
        if ($this->collSelProductItems === null) {
            $this->initSelProductItems();
            $this->collSelProductItemsPartial = true;
        }
        if (!in_array($l, $this->collSelProductItems->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProductItem($l);
        }

        return $this;
    }

    /**
     * @param	SelProductItem $selProductItem The selProductItem object to add.
     */
    protected function doAddSelProductItem($selProductItem)
    {
        $this->collSelProductItems[]= $selProductItem;
        $selProductItem->setSelProductAcquisition($this);
    }

    /**
     * @param	SelProductItem $selProductItem The selProductItem object to remove.
     * @return SelProductAcquisition The current object (for fluent API support)
     */
    public function removeSelProductItem($selProductItem)
    {
        if ($this->getSelProductItems()->contains($selProductItem)) {
            $this->collSelProductItems->remove($this->collSelProductItems->search($selProductItem));
            if (null === $this->selProductItemsScheduledForDeletion) {
                $this->selProductItemsScheduledForDeletion = clone $this->collSelProductItems;
                $this->selProductItemsScheduledForDeletion->clear();
            }
            $this->selProductItemsScheduledForDeletion[]= clone $selProductItem;
            $selProductItem->setSelProductAcquisition(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelProductAcquisition is new, it will return
     * an empty collection; or if this SelProductAcquisition has previously
     * been saved, it will retrieve related SelProductItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelProductAcquisition.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProductItem[] List of SelProductItem objects
     */
    public function getSelProductItemsJoinSelBranch($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductItemQuery::create(null, $criteria);
        $query->joinWith('SelBranch', $join_behavior);

        return $this->getSelProductItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelProductAcquisition is new, it will return
     * an empty collection; or if this SelProductAcquisition has previously
     * been saved, it will retrieve related SelProductItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelProductAcquisition.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProductItem[] List of SelProductItem objects
     */
    public function getSelProductItemsJoinSelBrand($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductItemQuery::create(null, $criteria);
        $query->joinWith('SelBrand', $join_behavior);

        return $this->getSelProductItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SelProductAcquisition is new, it will return
     * an empty collection; or if this SelProductAcquisition has previously
     * been saved, it will retrieve related SelProductItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SelProductAcquisition.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SelProductItem[] List of SelProductItem objects
     */
    public function getSelProductItemsJoinSelProductSell($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SelProductItemQuery::create(null, $criteria);
        $query->joinWith('SelProductSell', $join_behavior);

        return $this->getSelProductItems($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->aquisition_id = null;
        $this->product_id = null;
        $this->quantity = null;
        $this->price = null;
        $this->money = null;
        $this->warranty_provider = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSelProductItems) {
                foreach ($this->collSelProductItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collSelProductItems instanceof PropelCollection) {
            $this->collSelProductItems->clearIterator();
        }
        $this->collSelProductItems = null;
        $this->aSelAcquisition = null;
        $this->aSelProduct = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelProductAcquisitionPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
