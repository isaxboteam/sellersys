<?php


/**
 * Base class that represents a row from the 'cho_rol_x_uri' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseChoRolXUri extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'ChoRolXUriPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  ChoRolXUriPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the rol_id field.
     * @var        int
     */
    protected $rol_id;

    /**
     * The value for the uri_id field.
     * @var        int
     */
    protected $uri_id;

    /**
     * The value for the read field.
     * Note: this column has a database default value of: 'SI'
     * @var        string
     */
    protected $read;

    /**
     * The value for the create field.
     * Note: this column has a database default value of: 'NO'
     * @var        string
     */
    protected $create;

    /**
     * The value for the update field.
     * Note: this column has a database default value of: 'NO'
     * @var        string
     */
    protected $update;

    /**
     * The value for the delete field.
     * Note: this column has a database default value of: 'NO'
     * @var        string
     */
    protected $delete;

    /**
     * @var        ChoRol
     */
    protected $aChoRol;

    /**
     * @var        ChoUri
     */
    protected $aChoUri;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->read = 'SI';
        $this->create = 'NO';
        $this->update = 'NO';
        $this->delete = 'NO';
    }

    /**
     * Initializes internal state of BaseChoRolXUri object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [rol_id] column value.
     * 
     * @return int
     */
    public function getRolId()
    {
        return $this->rol_id;
    }

    /**
     * Get the [uri_id] column value.
     * 
     * @return int
     */
    public function getUriId()
    {
        return $this->uri_id;
    }

    /**
     * Get the [read] column value.
     * 
     * @return string
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * Get the [create] column value.
     * 
     * @return string
     */
    public function getCreate()
    {
        return $this->create;
    }

    /**
     * Get the [update] column value.
     * 
     * @return string
     */
    public function getUpdate()
    {
        return $this->update;
    }

    /**
     * Get the [delete] column value.
     * 
     * @return string
     */
    public function getDelete()
    {
        return $this->delete;
    }

    /**
     * Set the value of [rol_id] column.
     * 
     * @param int $v new value
     * @return ChoRolXUri The current object (for fluent API support)
     */
    public function setRolId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->rol_id !== $v) {
            $this->rol_id = $v;
            $this->modifiedColumns[] = ChoRolXUriPeer::ROL_ID;
        }

        if ($this->aChoRol !== null && $this->aChoRol->getId() !== $v) {
            $this->aChoRol = null;
        }


        return $this;
    } // setRolId()

    /**
     * Set the value of [uri_id] column.
     * 
     * @param int $v new value
     * @return ChoRolXUri The current object (for fluent API support)
     */
    public function setUriId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->uri_id !== $v) {
            $this->uri_id = $v;
            $this->modifiedColumns[] = ChoRolXUriPeer::URI_ID;
        }

        if ($this->aChoUri !== null && $this->aChoUri->getId() !== $v) {
            $this->aChoUri = null;
        }


        return $this;
    } // setUriId()

    /**
     * Set the value of [read] column.
     * 
     * @param string $v new value
     * @return ChoRolXUri The current object (for fluent API support)
     */
    public function setRead($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->read !== $v) {
            $this->read = $v;
            $this->modifiedColumns[] = ChoRolXUriPeer::READ;
        }


        return $this;
    } // setRead()

    /**
     * Set the value of [create] column.
     * 
     * @param string $v new value
     * @return ChoRolXUri The current object (for fluent API support)
     */
    public function setCreate($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->create !== $v) {
            $this->create = $v;
            $this->modifiedColumns[] = ChoRolXUriPeer::CREATE;
        }


        return $this;
    } // setCreate()

    /**
     * Set the value of [update] column.
     * 
     * @param string $v new value
     * @return ChoRolXUri The current object (for fluent API support)
     */
    public function setUpdate($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->update !== $v) {
            $this->update = $v;
            $this->modifiedColumns[] = ChoRolXUriPeer::UPDATE;
        }


        return $this;
    } // setUpdate()

    /**
     * Set the value of [delete] column.
     * 
     * @param string $v new value
     * @return ChoRolXUri The current object (for fluent API support)
     */
    public function setDelete($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->delete !== $v) {
            $this->delete = $v;
            $this->modifiedColumns[] = ChoRolXUriPeer::DELETE;
        }


        return $this;
    } // setDelete()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->read !== 'SI') {
                return false;
            }

            if ($this->create !== 'NO') {
                return false;
            }

            if ($this->update !== 'NO') {
                return false;
            }

            if ($this->delete !== 'NO') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->rol_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->uri_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->read = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->create = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->update = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->delete = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 6; // 6 = ChoRolXUriPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating ChoRolXUri object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aChoRol !== null && $this->rol_id !== $this->aChoRol->getId()) {
            $this->aChoRol = null;
        }
        if ($this->aChoUri !== null && $this->uri_id !== $this->aChoUri->getId()) {
            $this->aChoUri = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoRolXUriPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ChoRolXUriPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aChoRol = null;
            $this->aChoUri = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoRolXUriPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ChoRolXUriQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoRolXUriPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ChoRolXUriPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aChoRol !== null) {
                if ($this->aChoRol->isModified() || $this->aChoRol->isNew()) {
                    $affectedRows += $this->aChoRol->save($con);
                }
                $this->setChoRol($this->aChoRol);
            }

            if ($this->aChoUri !== null) {
                if ($this->aChoUri->isModified() || $this->aChoUri->isNew()) {
                    $affectedRows += $this->aChoUri->save($con);
                }
                $this->setChoUri($this->aChoUri);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ChoRolXUriPeer::ROL_ID)) {
            $modifiedColumns[':p' . $index++]  = '`ROL_ID`';
        }
        if ($this->isColumnModified(ChoRolXUriPeer::URI_ID)) {
            $modifiedColumns[':p' . $index++]  = '`URI_ID`';
        }
        if ($this->isColumnModified(ChoRolXUriPeer::READ)) {
            $modifiedColumns[':p' . $index++]  = '`READ`';
        }
        if ($this->isColumnModified(ChoRolXUriPeer::CREATE)) {
            $modifiedColumns[':p' . $index++]  = '`CREATE`';
        }
        if ($this->isColumnModified(ChoRolXUriPeer::UPDATE)) {
            $modifiedColumns[':p' . $index++]  = '`UPDATE`';
        }
        if ($this->isColumnModified(ChoRolXUriPeer::DELETE)) {
            $modifiedColumns[':p' . $index++]  = '`DELETE`';
        }

        $sql = sprintf(
            'INSERT INTO `cho_rol_x_uri` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ROL_ID`':						
                        $stmt->bindValue($identifier, $this->rol_id, PDO::PARAM_INT);
                        break;
                    case '`URI_ID`':						
                        $stmt->bindValue($identifier, $this->uri_id, PDO::PARAM_INT);
                        break;
                    case '`READ`':						
                        $stmt->bindValue($identifier, $this->read, PDO::PARAM_STR);
                        break;
                    case '`CREATE`':						
                        $stmt->bindValue($identifier, $this->create, PDO::PARAM_STR);
                        break;
                    case '`UPDATE`':						
                        $stmt->bindValue($identifier, $this->update, PDO::PARAM_STR);
                        break;
                    case '`DELETE`':						
                        $stmt->bindValue($identifier, $this->delete, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aChoRol !== null) {
                if (!$this->aChoRol->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aChoRol->getValidationFailures());
                }
            }

            if ($this->aChoUri !== null) {
                if (!$this->aChoUri->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aChoUri->getValidationFailures());
                }
            }

            if (($retval = ChoRolXUriPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ChoRolXUriPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getRolId();
                break;
            case 1:
                return $this->getUriId();
                break;
            case 2:
                return $this->getRead();
                break;
            case 3:
                return $this->getCreate();
                break;
            case 4:
                return $this->getUpdate();
                break;
            case 5:
                return $this->getDelete();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['ChoRolXUri'][serialize($this->getPrimaryKey())])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ChoRolXUri'][serialize($this->getPrimaryKey())] = true;
        $keys = ChoRolXUriPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getRolId(),
            $keys[1] => $this->getUriId(),
            $keys[2] => $this->getRead(),
            $keys[3] => $this->getCreate(),
            $keys[4] => $this->getUpdate(),
            $keys[5] => $this->getDelete(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aChoRol) {
                $result['ChoRol'] = $this->aChoRol->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aChoUri) {
                $result['ChoUri'] = $this->aChoUri->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ChoRolXUriPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setRolId($value);
                break;
            case 1:
                $this->setUriId($value);
                break;
            case 2:
                $this->setRead($value);
                break;
            case 3:
                $this->setCreate($value);
                break;
            case 4:
                $this->setUpdate($value);
                break;
            case 5:
                $this->setDelete($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ChoRolXUriPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setRolId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setUriId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setRead($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCreate($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setUpdate($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDelete($arr[$keys[5]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ChoRolXUriPeer::DATABASE_NAME);

        if ($this->isColumnModified(ChoRolXUriPeer::ROL_ID)) $criteria->add(ChoRolXUriPeer::ROL_ID, $this->rol_id);
        if ($this->isColumnModified(ChoRolXUriPeer::URI_ID)) $criteria->add(ChoRolXUriPeer::URI_ID, $this->uri_id);
        if ($this->isColumnModified(ChoRolXUriPeer::READ)) $criteria->add(ChoRolXUriPeer::READ, $this->read);
        if ($this->isColumnModified(ChoRolXUriPeer::CREATE)) $criteria->add(ChoRolXUriPeer::CREATE, $this->create);
        if ($this->isColumnModified(ChoRolXUriPeer::UPDATE)) $criteria->add(ChoRolXUriPeer::UPDATE, $this->update);
        if ($this->isColumnModified(ChoRolXUriPeer::DELETE)) $criteria->add(ChoRolXUriPeer::DELETE, $this->delete);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ChoRolXUriPeer::DATABASE_NAME);
        $criteria->add(ChoRolXUriPeer::ROL_ID, $this->rol_id);
        $criteria->add(ChoRolXUriPeer::URI_ID, $this->uri_id);

        return $criteria;
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getRolId();
        $pks[1] = $this->getUriId();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setRolId($keys[0]);
        $this->setUriId($keys[1]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return (null === $this->getRolId()) && (null === $this->getUriId());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of ChoRolXUri (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setRolId($this->getRolId());
        $copyObj->setUriId($this->getUriId());
        $copyObj->setRead($this->getRead());
        $copyObj->setCreate($this->getCreate());
        $copyObj->setUpdate($this->getUpdate());
        $copyObj->setDelete($this->getDelete());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return ChoRolXUri Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ChoRolXUriPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ChoRolXUriPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a ChoRol object.
     *
     * @param             ChoRol $v
     * @return ChoRolXUri The current object (for fluent API support)
     * @throws PropelException
     */
    public function setChoRol(ChoRol $v = null)
    {
        if ($v === null) {
            $this->setRolId(NULL);
        } else {
            $this->setRolId($v->getId());
        }

        $this->aChoRol = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChoRol object, it will not be re-added.
        if ($v !== null) {
            $v->addChoRolXUri($this);
        }


        return $this;
    }


    /**
     * Get the associated ChoRol object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return ChoRol The associated ChoRol object.
     * @throws PropelException
     */
    public function getChoRol(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aChoRol === null && ($this->rol_id !== null) && $doQuery) {
            $this->aChoRol = ChoRolQuery::create()->findPk($this->rol_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aChoRol->addChoRolXUris($this);
             */
        }

        return $this->aChoRol;
    }

    /**
     * Declares an association between this object and a ChoUri object.
     *
     * @param             ChoUri $v
     * @return ChoRolXUri The current object (for fluent API support)
     * @throws PropelException
     */
    public function setChoUri(ChoUri $v = null)
    {
        if ($v === null) {
            $this->setUriId(NULL);
        } else {
            $this->setUriId($v->getId());
        }

        $this->aChoUri = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChoUri object, it will not be re-added.
        if ($v !== null) {
            $v->addChoRolXUri($this);
        }


        return $this;
    }


    /**
     * Get the associated ChoUri object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return ChoUri The associated ChoUri object.
     * @throws PropelException
     */
    public function getChoUri(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aChoUri === null && ($this->uri_id !== null) && $doQuery) {
            $this->aChoUri = ChoUriQuery::create()->findPk($this->uri_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aChoUri->addChoRolXUris($this);
             */
        }

        return $this->aChoUri;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->rol_id = null;
        $this->uri_id = null;
        $this->read = null;
        $this->create = null;
        $this->update = null;
        $this->delete = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aChoRol = null;
        $this->aChoUri = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ChoRolXUriPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
