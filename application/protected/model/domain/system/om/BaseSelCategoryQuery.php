<?php


/**
 * Base class that represents a query for the 'sel_category' table.
 *
 * 
 *
 * @method SelCategoryQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelCategoryQuery orderByMainCategoryId($order = Criteria::ASC) Order by the MAIN_CATEGORY_ID column
 * @method SelCategoryQuery orderByCode($order = Criteria::ASC) Order by the CODE column
 * @method SelCategoryQuery orderByName($order = Criteria::ASC) Order by the NAME column
 * @method SelCategoryQuery orderByDescription($order = Criteria::ASC) Order by the DESCRIPTION column
 *
 * @method SelCategoryQuery groupById() Group by the ID column
 * @method SelCategoryQuery groupByMainCategoryId() Group by the MAIN_CATEGORY_ID column
 * @method SelCategoryQuery groupByCode() Group by the CODE column
 * @method SelCategoryQuery groupByName() Group by the NAME column
 * @method SelCategoryQuery groupByDescription() Group by the DESCRIPTION column
 *
 * @method SelCategoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelCategoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelCategoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelCategoryQuery leftJoinSelCategoryRelatedByMainCategoryId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelCategoryRelatedByMainCategoryId relation
 * @method SelCategoryQuery rightJoinSelCategoryRelatedByMainCategoryId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelCategoryRelatedByMainCategoryId relation
 * @method SelCategoryQuery innerJoinSelCategoryRelatedByMainCategoryId($relationAlias = null) Adds a INNER JOIN clause to the query using the SelCategoryRelatedByMainCategoryId relation
 *
 * @method SelCategoryQuery leftJoinSelBranchCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelBranchCategory relation
 * @method SelCategoryQuery rightJoinSelBranchCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelBranchCategory relation
 * @method SelCategoryQuery innerJoinSelBranchCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the SelBranchCategory relation
 *
 * @method SelCategoryQuery leftJoinSelCategoryRelatedById($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelCategoryRelatedById relation
 * @method SelCategoryQuery rightJoinSelCategoryRelatedById($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelCategoryRelatedById relation
 * @method SelCategoryQuery innerJoinSelCategoryRelatedById($relationAlias = null) Adds a INNER JOIN clause to the query using the SelCategoryRelatedById relation
 *
 * @method SelCategoryQuery leftJoinSelProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProduct relation
 * @method SelCategoryQuery rightJoinSelProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProduct relation
 * @method SelCategoryQuery innerJoinSelProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProduct relation
 *
 * @method SelCategoryQuery leftJoinSelProductGroup($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductGroup relation
 * @method SelCategoryQuery rightJoinSelProductGroup($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductGroup relation
 * @method SelCategoryQuery innerJoinSelProductGroup($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductGroup relation
 *
 * @method SelCategory findOne(PropelPDO $con = null) Return the first SelCategory matching the query
 * @method SelCategory findOneOrCreate(PropelPDO $con = null) Return the first SelCategory matching the query, or a new SelCategory object populated from the query conditions when no match is found
 *
 * @method SelCategory findOneByMainCategoryId(int $MAIN_CATEGORY_ID) Return the first SelCategory filtered by the MAIN_CATEGORY_ID column
 * @method SelCategory findOneByCode(string $CODE) Return the first SelCategory filtered by the CODE column
 * @method SelCategory findOneByName(string $NAME) Return the first SelCategory filtered by the NAME column
 * @method SelCategory findOneByDescription(string $DESCRIPTION) Return the first SelCategory filtered by the DESCRIPTION column
 *
 * @method array findById(int $ID) Return SelCategory objects filtered by the ID column
 * @method array findByMainCategoryId(int $MAIN_CATEGORY_ID) Return SelCategory objects filtered by the MAIN_CATEGORY_ID column
 * @method array findByCode(string $CODE) Return SelCategory objects filtered by the CODE column
 * @method array findByName(string $NAME) Return SelCategory objects filtered by the NAME column
 * @method array findByDescription(string $DESCRIPTION) Return SelCategory objects filtered by the DESCRIPTION column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelCategoryQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelCategoryQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelCategory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelCategoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelCategoryQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelCategoryQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelCategoryQuery) {
            return $criteria;
        }
        $query = new SelCategoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelCategory|SelCategory[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelCategoryPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelCategoryPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelCategory A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelCategory A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `MAIN_CATEGORY_ID`, `CODE`, `NAME`, `DESCRIPTION` FROM `sel_category` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelCategory();
            $obj->hydrate($row);
            SelCategoryPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelCategory|SelCategory[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelCategory[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelCategoryPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelCategoryPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelCategoryPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the MAIN_CATEGORY_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByMainCategoryId(1234); // WHERE MAIN_CATEGORY_ID = 1234
     * $query->filterByMainCategoryId(array(12, 34)); // WHERE MAIN_CATEGORY_ID IN (12, 34)
     * $query->filterByMainCategoryId(array('min' => 12)); // WHERE MAIN_CATEGORY_ID > 12
     * </code>
     *
     * @see       filterBySelCategoryRelatedByMainCategoryId()
     *
     * @param     mixed $mainCategoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function filterByMainCategoryId($mainCategoryId = null, $comparison = null)
    {
        if (is_array($mainCategoryId)) {
            $useMinMax = false;
            if (isset($mainCategoryId['min'])) {
                $this->addUsingAlias(SelCategoryPeer::MAIN_CATEGORY_ID, $mainCategoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mainCategoryId['max'])) {
                $this->addUsingAlias(SelCategoryPeer::MAIN_CATEGORY_ID, $mainCategoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelCategoryPeer::MAIN_CATEGORY_ID, $mainCategoryId, $comparison);
    }

    /**
     * Filter the query on the CODE column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE CODE = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE CODE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCategoryPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the NAME column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE NAME = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE NAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCategoryPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the DESCRIPTION column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE DESCRIPTION = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE DESCRIPTION LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCategoryPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query by a related SelCategory object
     *
     * @param   SelCategory|PropelObjectCollection $selCategory The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelCategoryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelCategoryRelatedByMainCategoryId($selCategory, $comparison = null)
    {
        if ($selCategory instanceof SelCategory) {
            return $this
                ->addUsingAlias(SelCategoryPeer::MAIN_CATEGORY_ID, $selCategory->getId(), $comparison);
        } elseif ($selCategory instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelCategoryPeer::MAIN_CATEGORY_ID, $selCategory->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelCategoryRelatedByMainCategoryId() only accepts arguments of type SelCategory or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelCategoryRelatedByMainCategoryId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function joinSelCategoryRelatedByMainCategoryId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelCategoryRelatedByMainCategoryId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelCategoryRelatedByMainCategoryId');
        }

        return $this;
    }

    /**
     * Use the SelCategoryRelatedByMainCategoryId relation SelCategory object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelCategoryQuery A secondary query class using the current class as primary query
     */
    public function useSelCategoryRelatedByMainCategoryIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelCategoryRelatedByMainCategoryId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelCategoryRelatedByMainCategoryId', 'SelCategoryQuery');
    }

    /**
     * Filter the query by a related SelBranchCategory object
     *
     * @param   SelBranchCategory|PropelObjectCollection $selBranchCategory  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelCategoryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelBranchCategory($selBranchCategory, $comparison = null)
    {
        if ($selBranchCategory instanceof SelBranchCategory) {
            return $this
                ->addUsingAlias(SelCategoryPeer::ID, $selBranchCategory->getCategoryId(), $comparison);
        } elseif ($selBranchCategory instanceof PropelObjectCollection) {
            return $this
                ->useSelBranchCategoryQuery()
                ->filterByPrimaryKeys($selBranchCategory->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelBranchCategory() only accepts arguments of type SelBranchCategory or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelBranchCategory relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function joinSelBranchCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelBranchCategory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelBranchCategory');
        }

        return $this;
    }

    /**
     * Use the SelBranchCategory relation SelBranchCategory object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelBranchCategoryQuery A secondary query class using the current class as primary query
     */
    public function useSelBranchCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelBranchCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelBranchCategory', 'SelBranchCategoryQuery');
    }

    /**
     * Filter the query by a related SelCategory object
     *
     * @param   SelCategory|PropelObjectCollection $selCategory  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelCategoryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelCategoryRelatedById($selCategory, $comparison = null)
    {
        if ($selCategory instanceof SelCategory) {
            return $this
                ->addUsingAlias(SelCategoryPeer::ID, $selCategory->getMainCategoryId(), $comparison);
        } elseif ($selCategory instanceof PropelObjectCollection) {
            return $this
                ->useSelCategoryRelatedByIdQuery()
                ->filterByPrimaryKeys($selCategory->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelCategoryRelatedById() only accepts arguments of type SelCategory or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelCategoryRelatedById relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function joinSelCategoryRelatedById($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelCategoryRelatedById');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelCategoryRelatedById');
        }

        return $this;
    }

    /**
     * Use the SelCategoryRelatedById relation SelCategory object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelCategoryQuery A secondary query class using the current class as primary query
     */
    public function useSelCategoryRelatedByIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelCategoryRelatedById($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelCategoryRelatedById', 'SelCategoryQuery');
    }

    /**
     * Filter the query by a related SelProduct object
     *
     * @param   SelProduct|PropelObjectCollection $selProduct  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelCategoryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProduct($selProduct, $comparison = null)
    {
        if ($selProduct instanceof SelProduct) {
            return $this
                ->addUsingAlias(SelCategoryPeer::ID, $selProduct->getCategoryId(), $comparison);
        } elseif ($selProduct instanceof PropelObjectCollection) {
            return $this
                ->useSelProductQuery()
                ->filterByPrimaryKeys($selProduct->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProduct() only accepts arguments of type SelProduct or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProduct relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function joinSelProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProduct');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProduct');
        }

        return $this;
    }

    /**
     * Use the SelProduct relation SelProduct object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductQuery A secondary query class using the current class as primary query
     */
    public function useSelProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProduct', 'SelProductQuery');
    }

    /**
     * Filter the query by a related SelProductGroup object
     *
     * @param   SelProductGroup|PropelObjectCollection $selProductGroup  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelCategoryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductGroup($selProductGroup, $comparison = null)
    {
        if ($selProductGroup instanceof SelProductGroup) {
            return $this
                ->addUsingAlias(SelCategoryPeer::ID, $selProductGroup->getCategoryId(), $comparison);
        } elseif ($selProductGroup instanceof PropelObjectCollection) {
            return $this
                ->useSelProductGroupQuery()
                ->filterByPrimaryKeys($selProductGroup->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductGroup() only accepts arguments of type SelProductGroup or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductGroup relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function joinSelProductGroup($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductGroup');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductGroup');
        }

        return $this;
    }

    /**
     * Use the SelProductGroup relation SelProductGroup object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductGroupQuery A secondary query class using the current class as primary query
     */
    public function useSelProductGroupQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductGroup($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductGroup', 'SelProductGroupQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelCategory $selCategory Object to remove from the list of results
     *
     * @return SelCategoryQuery The current query, for fluid interface
     */
    public function prune($selCategory = null)
    {
        if ($selCategory) {
            $this->addUsingAlias(SelCategoryPeer::ID, $selCategory->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
