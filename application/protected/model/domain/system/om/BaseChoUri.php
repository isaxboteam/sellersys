<?php


/**
 * Base class that represents a row from the 'cho_uri' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseChoUri extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'ChoUriPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  ChoUriPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the module_id field.
     * @var        int
     */
    protected $module_id;

    /**
     * The value for the uri field.
     * @var        string
     */
    protected $uri;

    /**
     * The value for the title field.
     * @var        string
     */
    protected $title;

    /**
     * The value for the access field.
     * @var        string
     */
    protected $access;

    /**
     * The value for the type field.
     * @var        string
     */
    protected $type;

    /**
     * The value for the position field.
     * @var        int
     */
    protected $position;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * @var        ChoModule
     */
    protected $aChoModule;

    /**
     * @var        PropelObjectCollection|ChoRolXUri[] Collection to store aggregation of ChoRolXUri objects.
     */
    protected $collChoRolXUris;
    protected $collChoRolXUrisPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $choRolXUrisScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [module_id] column value.
     * 
     * @return int
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * Get the [uri] column value.
     * 
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Get the [title] column value.
     * 
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [access] column value.
     * 
     * @return string
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Get the [type] column value.
     * 
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the [position] column value.
     * 
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Get the [description] column value.
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return ChoUri The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ChoUriPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [module_id] column.
     * 
     * @param int $v new value
     * @return ChoUri The current object (for fluent API support)
     */
    public function setModuleId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->module_id !== $v) {
            $this->module_id = $v;
            $this->modifiedColumns[] = ChoUriPeer::MODULE_ID;
        }

        if ($this->aChoModule !== null && $this->aChoModule->getId() !== $v) {
            $this->aChoModule = null;
        }


        return $this;
    } // setModuleId()

    /**
     * Set the value of [uri] column.
     * 
     * @param string $v new value
     * @return ChoUri The current object (for fluent API support)
     */
    public function setUri($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uri !== $v) {
            $this->uri = $v;
            $this->modifiedColumns[] = ChoUriPeer::URI;
        }


        return $this;
    } // setUri()

    /**
     * Set the value of [title] column.
     * 
     * @param string $v new value
     * @return ChoUri The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[] = ChoUriPeer::TITLE;
        }


        return $this;
    } // setTitle()

    /**
     * Set the value of [access] column.
     * 
     * @param string $v new value
     * @return ChoUri The current object (for fluent API support)
     */
    public function setAccess($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->access !== $v) {
            $this->access = $v;
            $this->modifiedColumns[] = ChoUriPeer::ACCESS;
        }


        return $this;
    } // setAccess()

    /**
     * Set the value of [type] column.
     * 
     * @param string $v new value
     * @return ChoUri The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[] = ChoUriPeer::TYPE;
        }


        return $this;
    } // setType()

    /**
     * Set the value of [position] column.
     * 
     * @param int $v new value
     * @return ChoUri The current object (for fluent API support)
     */
    public function setPosition($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->position !== $v) {
            $this->position = $v;
            $this->modifiedColumns[] = ChoUriPeer::POSITION;
        }


        return $this;
    } // setPosition()

    /**
     * Set the value of [description] column.
     * 
     * @param string $v new value
     * @return ChoUri The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = ChoUriPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->module_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->uri = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->title = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->access = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->type = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->position = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->description = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 8; // 8 = ChoUriPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating ChoUri object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aChoModule !== null && $this->module_id !== $this->aChoModule->getId()) {
            $this->aChoModule = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoUriPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ChoUriPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aChoModule = null;
            $this->collChoRolXUris = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoUriPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ChoUriQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoUriPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ChoUriPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aChoModule !== null) {
                if ($this->aChoModule->isModified() || $this->aChoModule->isNew()) {
                    $affectedRows += $this->aChoModule->save($con);
                }
                $this->setChoModule($this->aChoModule);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->choRolXUrisScheduledForDeletion !== null) {
                if (!$this->choRolXUrisScheduledForDeletion->isEmpty()) {
                    ChoRolXUriQuery::create()
                        ->filterByPrimaryKeys($this->choRolXUrisScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->choRolXUrisScheduledForDeletion = null;
                }
            }

            if ($this->collChoRolXUris !== null) {
                foreach ($this->collChoRolXUris as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ChoUriPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ChoUriPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ChoUriPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(ChoUriPeer::MODULE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`MODULE_ID`';
        }
        if ($this->isColumnModified(ChoUriPeer::URI)) {
            $modifiedColumns[':p' . $index++]  = '`URI`';
        }
        if ($this->isColumnModified(ChoUriPeer::TITLE)) {
            $modifiedColumns[':p' . $index++]  = '`TITLE`';
        }
        if ($this->isColumnModified(ChoUriPeer::ACCESS)) {
            $modifiedColumns[':p' . $index++]  = '`ACCESS`';
        }
        if ($this->isColumnModified(ChoUriPeer::TYPE)) {
            $modifiedColumns[':p' . $index++]  = '`TYPE`';
        }
        if ($this->isColumnModified(ChoUriPeer::POSITION)) {
            $modifiedColumns[':p' . $index++]  = '`POSITION`';
        }
        if ($this->isColumnModified(ChoUriPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`DESCRIPTION`';
        }

        $sql = sprintf(
            'INSERT INTO `cho_uri` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`MODULE_ID`':						
                        $stmt->bindValue($identifier, $this->module_id, PDO::PARAM_INT);
                        break;
                    case '`URI`':						
                        $stmt->bindValue($identifier, $this->uri, PDO::PARAM_STR);
                        break;
                    case '`TITLE`':						
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case '`ACCESS`':						
                        $stmt->bindValue($identifier, $this->access, PDO::PARAM_STR);
                        break;
                    case '`TYPE`':						
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                    case '`POSITION`':						
                        $stmt->bindValue($identifier, $this->position, PDO::PARAM_INT);
                        break;
                    case '`DESCRIPTION`':						
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aChoModule !== null) {
                if (!$this->aChoModule->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aChoModule->getValidationFailures());
                }
            }

            if (($retval = ChoUriPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collChoRolXUris !== null) {
                    foreach ($this->collChoRolXUris as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ChoUriPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getModuleId();
                break;
            case 2:
                return $this->getUri();
                break;
            case 3:
                return $this->getTitle();
                break;
            case 4:
                return $this->getAccess();
                break;
            case 5:
                return $this->getType();
                break;
            case 6:
                return $this->getPosition();
                break;
            case 7:
                return $this->getDescription();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['ChoUri'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ChoUri'][$this->getPrimaryKey()] = true;
        $keys = ChoUriPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getModuleId(),
            $keys[2] => $this->getUri(),
            $keys[3] => $this->getTitle(),
            $keys[4] => $this->getAccess(),
            $keys[5] => $this->getType(),
            $keys[6] => $this->getPosition(),
            $keys[7] => $this->getDescription(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aChoModule) {
                $result['ChoModule'] = $this->aChoModule->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collChoRolXUris) {
                $result['ChoRolXUris'] = $this->collChoRolXUris->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ChoUriPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setModuleId($value);
                break;
            case 2:
                $this->setUri($value);
                break;
            case 3:
                $this->setTitle($value);
                break;
            case 4:
                $this->setAccess($value);
                break;
            case 5:
                $this->setType($value);
                break;
            case 6:
                $this->setPosition($value);
                break;
            case 7:
                $this->setDescription($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ChoUriPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setModuleId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setUri($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTitle($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setAccess($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setType($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setPosition($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setDescription($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ChoUriPeer::DATABASE_NAME);

        if ($this->isColumnModified(ChoUriPeer::ID)) $criteria->add(ChoUriPeer::ID, $this->id);
        if ($this->isColumnModified(ChoUriPeer::MODULE_ID)) $criteria->add(ChoUriPeer::MODULE_ID, $this->module_id);
        if ($this->isColumnModified(ChoUriPeer::URI)) $criteria->add(ChoUriPeer::URI, $this->uri);
        if ($this->isColumnModified(ChoUriPeer::TITLE)) $criteria->add(ChoUriPeer::TITLE, $this->title);
        if ($this->isColumnModified(ChoUriPeer::ACCESS)) $criteria->add(ChoUriPeer::ACCESS, $this->access);
        if ($this->isColumnModified(ChoUriPeer::TYPE)) $criteria->add(ChoUriPeer::TYPE, $this->type);
        if ($this->isColumnModified(ChoUriPeer::POSITION)) $criteria->add(ChoUriPeer::POSITION, $this->position);
        if ($this->isColumnModified(ChoUriPeer::DESCRIPTION)) $criteria->add(ChoUriPeer::DESCRIPTION, $this->description);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ChoUriPeer::DATABASE_NAME);
        $criteria->add(ChoUriPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of ChoUri (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setModuleId($this->getModuleId());
        $copyObj->setUri($this->getUri());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setAccess($this->getAccess());
        $copyObj->setType($this->getType());
        $copyObj->setPosition($this->getPosition());
        $copyObj->setDescription($this->getDescription());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getChoRolXUris() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChoRolXUri($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return ChoUri Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ChoUriPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ChoUriPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a ChoModule object.
     *
     * @param             ChoModule $v
     * @return ChoUri The current object (for fluent API support)
     * @throws PropelException
     */
    public function setChoModule(ChoModule $v = null)
    {
        if ($v === null) {
            $this->setModuleId(NULL);
        } else {
            $this->setModuleId($v->getId());
        }

        $this->aChoModule = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChoModule object, it will not be re-added.
        if ($v !== null) {
            $v->addChoUri($this);
        }


        return $this;
    }


    /**
     * Get the associated ChoModule object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return ChoModule The associated ChoModule object.
     * @throws PropelException
     */
    public function getChoModule(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aChoModule === null && ($this->module_id !== null) && $doQuery) {
            $this->aChoModule = ChoModuleQuery::create()->findPk($this->module_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aChoModule->addChoUris($this);
             */
        }

        return $this->aChoModule;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ChoRolXUri' == $relationName) {
            $this->initChoRolXUris();
        }
    }

    /**
     * Clears out the collChoRolXUris collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return ChoUri The current object (for fluent API support)
     * @see        addChoRolXUris()
     */
    public function clearChoRolXUris()
    {
        $this->collChoRolXUris = null; // important to set this to null since that means it is uninitialized
        $this->collChoRolXUrisPartial = null;

        return $this;
    }

    /**
     * reset is the collChoRolXUris collection loaded partially
     *
     * @return void
     */
    public function resetPartialChoRolXUris($v = true)
    {
        $this->collChoRolXUrisPartial = $v;
    }

    /**
     * Initializes the collChoRolXUris collection.
     *
     * By default this just sets the collChoRolXUris collection to an empty array (like clearcollChoRolXUris());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChoRolXUris($overrideExisting = true)
    {
        if (null !== $this->collChoRolXUris && !$overrideExisting) {
            return;
        }
        $this->collChoRolXUris = new PropelObjectCollection();
        $this->collChoRolXUris->setModel('ChoRolXUri');
    }

    /**
     * Gets an array of ChoRolXUri objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChoUri is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|ChoRolXUri[] List of ChoRolXUri objects
     * @throws PropelException
     */
    public function getChoRolXUris($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collChoRolXUrisPartial && !$this->isNew();
        if (null === $this->collChoRolXUris || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChoRolXUris) {
                // return empty collection
                $this->initChoRolXUris();
            } else {
                $collChoRolXUris = ChoRolXUriQuery::create(null, $criteria)
                    ->filterByChoUri($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collChoRolXUrisPartial && count($collChoRolXUris)) {
                      $this->initChoRolXUris(false);

                      foreach($collChoRolXUris as $obj) {
                        if (false == $this->collChoRolXUris->contains($obj)) {
                          $this->collChoRolXUris->append($obj);
                        }
                      }

                      $this->collChoRolXUrisPartial = true;
                    }

                    return $collChoRolXUris;
                }

                if($partial && $this->collChoRolXUris) {
                    foreach($this->collChoRolXUris as $obj) {
                        if($obj->isNew()) {
                            $collChoRolXUris[] = $obj;
                        }
                    }
                }

                $this->collChoRolXUris = $collChoRolXUris;
                $this->collChoRolXUrisPartial = false;
            }
        }

        return $this->collChoRolXUris;
    }

    /**
     * Sets a collection of ChoRolXUri objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $choRolXUris A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return ChoUri The current object (for fluent API support)
     */
    public function setChoRolXUris(PropelCollection $choRolXUris, PropelPDO $con = null)
    {
        $choRolXUrisToDelete = $this->getChoRolXUris(new Criteria(), $con)->diff($choRolXUris);

        $this->choRolXUrisScheduledForDeletion = unserialize(serialize($choRolXUrisToDelete));

        foreach ($choRolXUrisToDelete as $choRolXUriRemoved) {
            $choRolXUriRemoved->setChoUri(null);
        }

        $this->collChoRolXUris = null;
        foreach ($choRolXUris as $choRolXUri) {
            $this->addChoRolXUri($choRolXUri);
        }

        $this->collChoRolXUris = $choRolXUris;
        $this->collChoRolXUrisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ChoRolXUri objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related ChoRolXUri objects.
     * @throws PropelException
     */
    public function countChoRolXUris(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collChoRolXUrisPartial && !$this->isNew();
        if (null === $this->collChoRolXUris || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChoRolXUris) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getChoRolXUris());
            }
            $query = ChoRolXUriQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChoUri($this)
                ->count($con);
        }

        return count($this->collChoRolXUris);
    }

    /**
     * Method called to associate a ChoRolXUri object to this object
     * through the ChoRolXUri foreign key attribute.
     *
     * @param    ChoRolXUri $l ChoRolXUri
     * @return ChoUri The current object (for fluent API support)
     */
    public function addChoRolXUri(ChoRolXUri $l)
    {
        if ($this->collChoRolXUris === null) {
            $this->initChoRolXUris();
            $this->collChoRolXUrisPartial = true;
        }
        if (!in_array($l, $this->collChoRolXUris->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddChoRolXUri($l);
        }

        return $this;
    }

    /**
     * @param	ChoRolXUri $choRolXUri The choRolXUri object to add.
     */
    protected function doAddChoRolXUri($choRolXUri)
    {
        $this->collChoRolXUris[]= $choRolXUri;
        $choRolXUri->setChoUri($this);
    }

    /**
     * @param	ChoRolXUri $choRolXUri The choRolXUri object to remove.
     * @return ChoUri The current object (for fluent API support)
     */
    public function removeChoRolXUri($choRolXUri)
    {
        if ($this->getChoRolXUris()->contains($choRolXUri)) {
            $this->collChoRolXUris->remove($this->collChoRolXUris->search($choRolXUri));
            if (null === $this->choRolXUrisScheduledForDeletion) {
                $this->choRolXUrisScheduledForDeletion = clone $this->collChoRolXUris;
                $this->choRolXUrisScheduledForDeletion->clear();
            }
            $this->choRolXUrisScheduledForDeletion[]= clone $choRolXUri;
            $choRolXUri->setChoUri(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChoUri is new, it will return
     * an empty collection; or if this ChoUri has previously
     * been saved, it will retrieve related ChoRolXUris from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChoUri.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|ChoRolXUri[] List of ChoRolXUri objects
     */
    public function getChoRolXUrisJoinChoRol($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ChoRolXUriQuery::create(null, $criteria);
        $query->joinWith('ChoRol', $join_behavior);

        return $this->getChoRolXUris($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->module_id = null;
        $this->uri = null;
        $this->title = null;
        $this->access = null;
        $this->type = null;
        $this->position = null;
        $this->description = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collChoRolXUris) {
                foreach ($this->collChoRolXUris as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collChoRolXUris instanceof PropelCollection) {
            $this->collChoRolXUris->clearIterator();
        }
        $this->collChoRolXUris = null;
        $this->aChoModule = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ChoUriPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
