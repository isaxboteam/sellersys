<?php


/**
 * Base class that represents a query for the 'sel_product_order' table.
 *
 * 
 *
 * @method SelProductOrderQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelProductOrderQuery orderByOrderId($order = Criteria::ASC) Order by the ORDER_ID column
 * @method SelProductOrderQuery orderByProductId($order = Criteria::ASC) Order by the PRODUCT_ID column
 * @method SelProductOrderQuery orderByProductAcquisitionId($order = Criteria::ASC) Order by the PRODUCT_ACQUISITION_ID column
 * @method SelProductOrderQuery orderByStatus($order = Criteria::ASC) Order by the STATUS column
 * @method SelProductOrderQuery orderByCode($order = Criteria::ASC) Order by the CODE column
 * @method SelProductOrderQuery orderByPrice($order = Criteria::ASC) Order by the PRICE column
 * @method SelProductOrderQuery orderByQuantity($order = Criteria::ASC) Order by the QUANTITY column
 * @method SelProductOrderQuery orderByObservations($order = Criteria::ASC) Order by the OBSERVATIONS column
 *
 * @method SelProductOrderQuery groupById() Group by the ID column
 * @method SelProductOrderQuery groupByOrderId() Group by the ORDER_ID column
 * @method SelProductOrderQuery groupByProductId() Group by the PRODUCT_ID column
 * @method SelProductOrderQuery groupByProductAcquisitionId() Group by the PRODUCT_ACQUISITION_ID column
 * @method SelProductOrderQuery groupByStatus() Group by the STATUS column
 * @method SelProductOrderQuery groupByCode() Group by the CODE column
 * @method SelProductOrderQuery groupByPrice() Group by the PRICE column
 * @method SelProductOrderQuery groupByQuantity() Group by the QUANTITY column
 * @method SelProductOrderQuery groupByObservations() Group by the OBSERVATIONS column
 *
 * @method SelProductOrderQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelProductOrderQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelProductOrderQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelProductOrderQuery leftJoinSelOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelOrder relation
 * @method SelProductOrderQuery rightJoinSelOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelOrder relation
 * @method SelProductOrderQuery innerJoinSelOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SelOrder relation
 *
 * @method SelProductOrderQuery leftJoinSelProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProduct relation
 * @method SelProductOrderQuery rightJoinSelProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProduct relation
 * @method SelProductOrderQuery innerJoinSelProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProduct relation
 *
 * @method SelProductOrder findOne(PropelPDO $con = null) Return the first SelProductOrder matching the query
 * @method SelProductOrder findOneOrCreate(PropelPDO $con = null) Return the first SelProductOrder matching the query, or a new SelProductOrder object populated from the query conditions when no match is found
 *
 * @method SelProductOrder findOneByOrderId(int $ORDER_ID) Return the first SelProductOrder filtered by the ORDER_ID column
 * @method SelProductOrder findOneByProductId(int $PRODUCT_ID) Return the first SelProductOrder filtered by the PRODUCT_ID column
 * @method SelProductOrder findOneByProductAcquisitionId(int $PRODUCT_ACQUISITION_ID) Return the first SelProductOrder filtered by the PRODUCT_ACQUISITION_ID column
 * @method SelProductOrder findOneByStatus(string $STATUS) Return the first SelProductOrder filtered by the STATUS column
 * @method SelProductOrder findOneByCode(int $CODE) Return the first SelProductOrder filtered by the CODE column
 * @method SelProductOrder findOneByPrice(double $PRICE) Return the first SelProductOrder filtered by the PRICE column
 * @method SelProductOrder findOneByQuantity(int $QUANTITY) Return the first SelProductOrder filtered by the QUANTITY column
 * @method SelProductOrder findOneByObservations(string $OBSERVATIONS) Return the first SelProductOrder filtered by the OBSERVATIONS column
 *
 * @method array findById(int $ID) Return SelProductOrder objects filtered by the ID column
 * @method array findByOrderId(int $ORDER_ID) Return SelProductOrder objects filtered by the ORDER_ID column
 * @method array findByProductId(int $PRODUCT_ID) Return SelProductOrder objects filtered by the PRODUCT_ID column
 * @method array findByProductAcquisitionId(int $PRODUCT_ACQUISITION_ID) Return SelProductOrder objects filtered by the PRODUCT_ACQUISITION_ID column
 * @method array findByStatus(string $STATUS) Return SelProductOrder objects filtered by the STATUS column
 * @method array findByCode(int $CODE) Return SelProductOrder objects filtered by the CODE column
 * @method array findByPrice(double $PRICE) Return SelProductOrder objects filtered by the PRICE column
 * @method array findByQuantity(int $QUANTITY) Return SelProductOrder objects filtered by the QUANTITY column
 * @method array findByObservations(string $OBSERVATIONS) Return SelProductOrder objects filtered by the OBSERVATIONS column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProductOrderQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelProductOrderQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelProductOrder', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelProductOrderQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelProductOrderQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelProductOrderQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelProductOrderQuery) {
            return $criteria;
        }
        $query = new SelProductOrderQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelProductOrder|SelProductOrder[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelProductOrderPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelProductOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductOrder A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProductOrder A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `ORDER_ID`, `PRODUCT_ID`, `PRODUCT_ACQUISITION_ID`, `STATUS`, `CODE`, `PRICE`, `QUANTITY`, `OBSERVATIONS` FROM `sel_product_order` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelProductOrder();
            $obj->hydrate($row);
            SelProductOrderPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelProductOrder|SelProductOrder[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelProductOrder[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelProductOrderPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelProductOrderPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelProductOrderPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the ORDER_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByOrderId(1234); // WHERE ORDER_ID = 1234
     * $query->filterByOrderId(array(12, 34)); // WHERE ORDER_ID IN (12, 34)
     * $query->filterByOrderId(array('min' => 12)); // WHERE ORDER_ID > 12
     * </code>
     *
     * @see       filterBySelOrder()
     *
     * @param     mixed $orderId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function filterByOrderId($orderId = null, $comparison = null)
    {
        if (is_array($orderId)) {
            $useMinMax = false;
            if (isset($orderId['min'])) {
                $this->addUsingAlias(SelProductOrderPeer::ORDER_ID, $orderId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($orderId['max'])) {
                $this->addUsingAlias(SelProductOrderPeer::ORDER_ID, $orderId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductOrderPeer::ORDER_ID, $orderId, $comparison);
    }

    /**
     * Filter the query on the PRODUCT_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByProductId(1234); // WHERE PRODUCT_ID = 1234
     * $query->filterByProductId(array(12, 34)); // WHERE PRODUCT_ID IN (12, 34)
     * $query->filterByProductId(array('min' => 12)); // WHERE PRODUCT_ID > 12
     * </code>
     *
     * @see       filterBySelProduct()
     *
     * @param     mixed $productId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function filterByProductId($productId = null, $comparison = null)
    {
        if (is_array($productId)) {
            $useMinMax = false;
            if (isset($productId['min'])) {
                $this->addUsingAlias(SelProductOrderPeer::PRODUCT_ID, $productId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productId['max'])) {
                $this->addUsingAlias(SelProductOrderPeer::PRODUCT_ID, $productId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductOrderPeer::PRODUCT_ID, $productId, $comparison);
    }

    /**
     * Filter the query on the PRODUCT_ACQUISITION_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByProductAcquisitionId(1234); // WHERE PRODUCT_ACQUISITION_ID = 1234
     * $query->filterByProductAcquisitionId(array(12, 34)); // WHERE PRODUCT_ACQUISITION_ID IN (12, 34)
     * $query->filterByProductAcquisitionId(array('min' => 12)); // WHERE PRODUCT_ACQUISITION_ID > 12
     * </code>
     *
     * @param     mixed $productAcquisitionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function filterByProductAcquisitionId($productAcquisitionId = null, $comparison = null)
    {
        if (is_array($productAcquisitionId)) {
            $useMinMax = false;
            if (isset($productAcquisitionId['min'])) {
                $this->addUsingAlias(SelProductOrderPeer::PRODUCT_ACQUISITION_ID, $productAcquisitionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productAcquisitionId['max'])) {
                $this->addUsingAlias(SelProductOrderPeer::PRODUCT_ACQUISITION_ID, $productAcquisitionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductOrderPeer::PRODUCT_ACQUISITION_ID, $productAcquisitionId, $comparison);
    }

    /**
     * Filter the query on the STATUS column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE STATUS = 'fooValue'
     * $query->filterByStatus('%fooValue%'); // WHERE STATUS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $status)) {
                $status = str_replace('*', '%', $status);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductOrderPeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the CODE column
     *
     * Example usage:
     * <code>
     * $query->filterByCode(1234); // WHERE CODE = 1234
     * $query->filterByCode(array(12, 34)); // WHERE CODE IN (12, 34)
     * $query->filterByCode(array('min' => 12)); // WHERE CODE > 12
     * </code>
     *
     * @param     mixed $code The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (is_array($code)) {
            $useMinMax = false;
            if (isset($code['min'])) {
                $this->addUsingAlias(SelProductOrderPeer::CODE, $code['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($code['max'])) {
                $this->addUsingAlias(SelProductOrderPeer::CODE, $code['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductOrderPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the PRICE column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE PRICE = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE PRICE IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE PRICE > 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(SelProductOrderPeer::PRICE, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(SelProductOrderPeer::PRICE, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductOrderPeer::PRICE, $price, $comparison);
    }

    /**
     * Filter the query on the QUANTITY column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantity(1234); // WHERE QUANTITY = 1234
     * $query->filterByQuantity(array(12, 34)); // WHERE QUANTITY IN (12, 34)
     * $query->filterByQuantity(array('min' => 12)); // WHERE QUANTITY > 12
     * </code>
     *
     * @param     mixed $quantity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function filterByQuantity($quantity = null, $comparison = null)
    {
        if (is_array($quantity)) {
            $useMinMax = false;
            if (isset($quantity['min'])) {
                $this->addUsingAlias(SelProductOrderPeer::QUANTITY, $quantity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantity['max'])) {
                $this->addUsingAlias(SelProductOrderPeer::QUANTITY, $quantity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductOrderPeer::QUANTITY, $quantity, $comparison);
    }

    /**
     * Filter the query on the OBSERVATIONS column
     *
     * Example usage:
     * <code>
     * $query->filterByObservations('fooValue');   // WHERE OBSERVATIONS = 'fooValue'
     * $query->filterByObservations('%fooValue%'); // WHERE OBSERVATIONS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observations The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function filterByObservations($observations = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observations)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $observations)) {
                $observations = str_replace('*', '%', $observations);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductOrderPeer::OBSERVATIONS, $observations, $comparison);
    }

    /**
     * Filter the query by a related SelOrder object
     *
     * @param   SelOrder|PropelObjectCollection $selOrder The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductOrderQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelOrder($selOrder, $comparison = null)
    {
        if ($selOrder instanceof SelOrder) {
            return $this
                ->addUsingAlias(SelProductOrderPeer::ORDER_ID, $selOrder->getId(), $comparison);
        } elseif ($selOrder instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductOrderPeer::ORDER_ID, $selOrder->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelOrder() only accepts arguments of type SelOrder or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function joinSelOrder($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelOrder');
        }

        return $this;
    }

    /**
     * Use the SelOrder relation SelOrder object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelOrderQuery A secondary query class using the current class as primary query
     */
    public function useSelOrderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelOrder', 'SelOrderQuery');
    }

    /**
     * Filter the query by a related SelProduct object
     *
     * @param   SelProduct|PropelObjectCollection $selProduct The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductOrderQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProduct($selProduct, $comparison = null)
    {
        if ($selProduct instanceof SelProduct) {
            return $this
                ->addUsingAlias(SelProductOrderPeer::PRODUCT_ID, $selProduct->getId(), $comparison);
        } elseif ($selProduct instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductOrderPeer::PRODUCT_ID, $selProduct->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelProduct() only accepts arguments of type SelProduct or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProduct relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function joinSelProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProduct');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProduct');
        }

        return $this;
    }

    /**
     * Use the SelProduct relation SelProduct object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductQuery A secondary query class using the current class as primary query
     */
    public function useSelProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProduct', 'SelProductQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelProductOrder $selProductOrder Object to remove from the list of results
     *
     * @return SelProductOrderQuery The current query, for fluid interface
     */
    public function prune($selProductOrder = null)
    {
        if ($selProductOrder) {
            $this->addUsingAlias(SelProductOrderPeer::ID, $selProductOrder->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
