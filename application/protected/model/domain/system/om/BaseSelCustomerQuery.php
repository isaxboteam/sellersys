<?php


/**
 * Base class that represents a query for the 'sel_customer' table.
 *
 * 
 *
 * @method SelCustomerQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelCustomerQuery orderByCustomerAccountId($order = Criteria::ASC) Order by the CUSTOMER_ACCOUNT_ID column
 * @method SelCustomerQuery orderByIdNumber($order = Criteria::ASC) Order by the ID_NUMBER column
 * @method SelCustomerQuery orderByName($order = Criteria::ASC) Order by the NAME column
 * @method SelCustomerQuery orderByLastname($order = Criteria::ASC) Order by the LASTNAME column
 * @method SelCustomerQuery orderByEmail($order = Criteria::ASC) Order by the EMAIL column
 * @method SelCustomerQuery orderByBirthday($order = Criteria::ASC) Order by the BIRTHDAY column
 * @method SelCustomerQuery orderByAddress($order = Criteria::ASC) Order by the ADDRESS column
 * @method SelCustomerQuery orderByPhone($order = Criteria::ASC) Order by the PHONE column
 * @method SelCustomerQuery orderByCellphone($order = Criteria::ASC) Order by the CELLPHONE column
 * @method SelCustomerQuery orderByStatus($order = Criteria::ASC) Order by the STATUS column
 * @method SelCustomerQuery orderByCode($order = Criteria::ASC) Order by the CODE column
 *
 * @method SelCustomerQuery groupById() Group by the ID column
 * @method SelCustomerQuery groupByCustomerAccountId() Group by the CUSTOMER_ACCOUNT_ID column
 * @method SelCustomerQuery groupByIdNumber() Group by the ID_NUMBER column
 * @method SelCustomerQuery groupByName() Group by the NAME column
 * @method SelCustomerQuery groupByLastname() Group by the LASTNAME column
 * @method SelCustomerQuery groupByEmail() Group by the EMAIL column
 * @method SelCustomerQuery groupByBirthday() Group by the BIRTHDAY column
 * @method SelCustomerQuery groupByAddress() Group by the ADDRESS column
 * @method SelCustomerQuery groupByPhone() Group by the PHONE column
 * @method SelCustomerQuery groupByCellphone() Group by the CELLPHONE column
 * @method SelCustomerQuery groupByStatus() Group by the STATUS column
 * @method SelCustomerQuery groupByCode() Group by the CODE column
 *
 * @method SelCustomerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelCustomerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelCustomerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelCustomerQuery leftJoinSelCustomerAccount($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelCustomerAccount relation
 * @method SelCustomerQuery rightJoinSelCustomerAccount($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelCustomerAccount relation
 * @method SelCustomerQuery innerJoinSelCustomerAccount($relationAlias = null) Adds a INNER JOIN clause to the query using the SelCustomerAccount relation
 *
 * @method SelCustomerQuery leftJoinSelOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelOrder relation
 * @method SelCustomerQuery rightJoinSelOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelOrder relation
 * @method SelCustomerQuery innerJoinSelOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SelOrder relation
 *
 * @method SelCustomerQuery leftJoinSelSale($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelSale relation
 * @method SelCustomerQuery rightJoinSelSale($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelSale relation
 * @method SelCustomerQuery innerJoinSelSale($relationAlias = null) Adds a INNER JOIN clause to the query using the SelSale relation
 *
 * @method SelCustomer findOne(PropelPDO $con = null) Return the first SelCustomer matching the query
 * @method SelCustomer findOneOrCreate(PropelPDO $con = null) Return the first SelCustomer matching the query, or a new SelCustomer object populated from the query conditions when no match is found
 *
 * @method SelCustomer findOneByCustomerAccountId(int $CUSTOMER_ACCOUNT_ID) Return the first SelCustomer filtered by the CUSTOMER_ACCOUNT_ID column
 * @method SelCustomer findOneByIdNumber(string $ID_NUMBER) Return the first SelCustomer filtered by the ID_NUMBER column
 * @method SelCustomer findOneByName(string $NAME) Return the first SelCustomer filtered by the NAME column
 * @method SelCustomer findOneByLastname(string $LASTNAME) Return the first SelCustomer filtered by the LASTNAME column
 * @method SelCustomer findOneByEmail(string $EMAIL) Return the first SelCustomer filtered by the EMAIL column
 * @method SelCustomer findOneByBirthday(string $BIRTHDAY) Return the first SelCustomer filtered by the BIRTHDAY column
 * @method SelCustomer findOneByAddress(string $ADDRESS) Return the first SelCustomer filtered by the ADDRESS column
 * @method SelCustomer findOneByPhone(string $PHONE) Return the first SelCustomer filtered by the PHONE column
 * @method SelCustomer findOneByCellphone(string $CELLPHONE) Return the first SelCustomer filtered by the CELLPHONE column
 * @method SelCustomer findOneByStatus(string $STATUS) Return the first SelCustomer filtered by the STATUS column
 * @method SelCustomer findOneByCode(string $CODE) Return the first SelCustomer filtered by the CODE column
 *
 * @method array findById(int $ID) Return SelCustomer objects filtered by the ID column
 * @method array findByCustomerAccountId(int $CUSTOMER_ACCOUNT_ID) Return SelCustomer objects filtered by the CUSTOMER_ACCOUNT_ID column
 * @method array findByIdNumber(string $ID_NUMBER) Return SelCustomer objects filtered by the ID_NUMBER column
 * @method array findByName(string $NAME) Return SelCustomer objects filtered by the NAME column
 * @method array findByLastname(string $LASTNAME) Return SelCustomer objects filtered by the LASTNAME column
 * @method array findByEmail(string $EMAIL) Return SelCustomer objects filtered by the EMAIL column
 * @method array findByBirthday(string $BIRTHDAY) Return SelCustomer objects filtered by the BIRTHDAY column
 * @method array findByAddress(string $ADDRESS) Return SelCustomer objects filtered by the ADDRESS column
 * @method array findByPhone(string $PHONE) Return SelCustomer objects filtered by the PHONE column
 * @method array findByCellphone(string $CELLPHONE) Return SelCustomer objects filtered by the CELLPHONE column
 * @method array findByStatus(string $STATUS) Return SelCustomer objects filtered by the STATUS column
 * @method array findByCode(string $CODE) Return SelCustomer objects filtered by the CODE column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelCustomerQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelCustomerQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelCustomer', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelCustomerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelCustomerQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelCustomerQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelCustomerQuery) {
            return $criteria;
        }
        $query = new SelCustomerQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelCustomer|SelCustomer[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelCustomerPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelCustomerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelCustomer A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelCustomer A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `CUSTOMER_ACCOUNT_ID`, `ID_NUMBER`, `NAME`, `LASTNAME`, `EMAIL`, `BIRTHDAY`, `ADDRESS`, `PHONE`, `CELLPHONE`, `STATUS`, `CODE` FROM `sel_customer` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelCustomer();
            $obj->hydrate($row);
            SelCustomerPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelCustomer|SelCustomer[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelCustomer[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelCustomerPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelCustomerPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelCustomerPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the CUSTOMER_ACCOUNT_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerAccountId(1234); // WHERE CUSTOMER_ACCOUNT_ID = 1234
     * $query->filterByCustomerAccountId(array(12, 34)); // WHERE CUSTOMER_ACCOUNT_ID IN (12, 34)
     * $query->filterByCustomerAccountId(array('min' => 12)); // WHERE CUSTOMER_ACCOUNT_ID > 12
     * </code>
     *
     * @see       filterBySelCustomerAccount()
     *
     * @param     mixed $customerAccountId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByCustomerAccountId($customerAccountId = null, $comparison = null)
    {
        if (is_array($customerAccountId)) {
            $useMinMax = false;
            if (isset($customerAccountId['min'])) {
                $this->addUsingAlias(SelCustomerPeer::CUSTOMER_ACCOUNT_ID, $customerAccountId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerAccountId['max'])) {
                $this->addUsingAlias(SelCustomerPeer::CUSTOMER_ACCOUNT_ID, $customerAccountId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelCustomerPeer::CUSTOMER_ACCOUNT_ID, $customerAccountId, $comparison);
    }

    /**
     * Filter the query on the ID_NUMBER column
     *
     * Example usage:
     * <code>
     * $query->filterByIdNumber('fooValue');   // WHERE ID_NUMBER = 'fooValue'
     * $query->filterByIdNumber('%fooValue%'); // WHERE ID_NUMBER LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idNumber The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByIdNumber($idNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idNumber)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idNumber)) {
                $idNumber = str_replace('*', '%', $idNumber);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCustomerPeer::ID_NUMBER, $idNumber, $comparison);
    }

    /**
     * Filter the query on the NAME column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE NAME = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE NAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCustomerPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the LASTNAME column
     *
     * Example usage:
     * <code>
     * $query->filterByLastname('fooValue');   // WHERE LASTNAME = 'fooValue'
     * $query->filterByLastname('%fooValue%'); // WHERE LASTNAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByLastname($lastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lastname)) {
                $lastname = str_replace('*', '%', $lastname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCustomerPeer::LASTNAME, $lastname, $comparison);
    }

    /**
     * Filter the query on the EMAIL column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE EMAIL = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE EMAIL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCustomerPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the BIRTHDAY column
     *
     * Example usage:
     * <code>
     * $query->filterByBirthday('2011-03-14'); // WHERE BIRTHDAY = '2011-03-14'
     * $query->filterByBirthday('now'); // WHERE BIRTHDAY = '2011-03-14'
     * $query->filterByBirthday(array('max' => 'yesterday')); // WHERE BIRTHDAY > '2011-03-13'
     * </code>
     *
     * @param     mixed $birthday The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByBirthday($birthday = null, $comparison = null)
    {
        if (is_array($birthday)) {
            $useMinMax = false;
            if (isset($birthday['min'])) {
                $this->addUsingAlias(SelCustomerPeer::BIRTHDAY, $birthday['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($birthday['max'])) {
                $this->addUsingAlias(SelCustomerPeer::BIRTHDAY, $birthday['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelCustomerPeer::BIRTHDAY, $birthday, $comparison);
    }

    /**
     * Filter the query on the ADDRESS column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE ADDRESS = 'fooValue'
     * $query->filterByAddress('%fooValue%'); // WHERE ADDRESS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $address)) {
                $address = str_replace('*', '%', $address);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCustomerPeer::ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the PHONE column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE PHONE = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE PHONE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCustomerPeer::PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the CELLPHONE column
     *
     * Example usage:
     * <code>
     * $query->filterByCellphone('fooValue');   // WHERE CELLPHONE = 'fooValue'
     * $query->filterByCellphone('%fooValue%'); // WHERE CELLPHONE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cellphone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByCellphone($cellphone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cellphone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cellphone)) {
                $cellphone = str_replace('*', '%', $cellphone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCustomerPeer::CELLPHONE, $cellphone, $comparison);
    }

    /**
     * Filter the query on the STATUS column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE STATUS = 'fooValue'
     * $query->filterByStatus('%fooValue%'); // WHERE STATUS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $status)) {
                $status = str_replace('*', '%', $status);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCustomerPeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the CODE column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE CODE = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE CODE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelCustomerPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query by a related SelCustomerAccount object
     *
     * @param   SelCustomerAccount|PropelObjectCollection $selCustomerAccount The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelCustomerQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelCustomerAccount($selCustomerAccount, $comparison = null)
    {
        if ($selCustomerAccount instanceof SelCustomerAccount) {
            return $this
                ->addUsingAlias(SelCustomerPeer::CUSTOMER_ACCOUNT_ID, $selCustomerAccount->getId(), $comparison);
        } elseif ($selCustomerAccount instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelCustomerPeer::CUSTOMER_ACCOUNT_ID, $selCustomerAccount->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelCustomerAccount() only accepts arguments of type SelCustomerAccount or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelCustomerAccount relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function joinSelCustomerAccount($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelCustomerAccount');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelCustomerAccount');
        }

        return $this;
    }

    /**
     * Use the SelCustomerAccount relation SelCustomerAccount object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelCustomerAccountQuery A secondary query class using the current class as primary query
     */
    public function useSelCustomerAccountQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelCustomerAccount($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelCustomerAccount', 'SelCustomerAccountQuery');
    }

    /**
     * Filter the query by a related SelOrder object
     *
     * @param   SelOrder|PropelObjectCollection $selOrder  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelCustomerQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelOrder($selOrder, $comparison = null)
    {
        if ($selOrder instanceof SelOrder) {
            return $this
                ->addUsingAlias(SelCustomerPeer::ID, $selOrder->getCustomerId(), $comparison);
        } elseif ($selOrder instanceof PropelObjectCollection) {
            return $this
                ->useSelOrderQuery()
                ->filterByPrimaryKeys($selOrder->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelOrder() only accepts arguments of type SelOrder or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function joinSelOrder($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelOrder');
        }

        return $this;
    }

    /**
     * Use the SelOrder relation SelOrder object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelOrderQuery A secondary query class using the current class as primary query
     */
    public function useSelOrderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelOrder', 'SelOrderQuery');
    }

    /**
     * Filter the query by a related SelSale object
     *
     * @param   SelSale|PropelObjectCollection $selSale  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelCustomerQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelSale($selSale, $comparison = null)
    {
        if ($selSale instanceof SelSale) {
            return $this
                ->addUsingAlias(SelCustomerPeer::ID, $selSale->getCustomerId(), $comparison);
        } elseif ($selSale instanceof PropelObjectCollection) {
            return $this
                ->useSelSaleQuery()
                ->filterByPrimaryKeys($selSale->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelSale() only accepts arguments of type SelSale or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelSale relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function joinSelSale($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelSale');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelSale');
        }

        return $this;
    }

    /**
     * Use the SelSale relation SelSale object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelSaleQuery A secondary query class using the current class as primary query
     */
    public function useSelSaleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelSale($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelSale', 'SelSaleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelCustomer $selCustomer Object to remove from the list of results
     *
     * @return SelCustomerQuery The current query, for fluid interface
     */
    public function prune($selCustomer = null)
    {
        if ($selCustomer) {
            $this->addUsingAlias(SelCustomerPeer::ID, $selCustomer->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
