<?php


/**
 * Base class that represents a query for the 'sel_order_product' table.
 *
 * 
 *
 * @method SelOrderProductQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelOrderProductQuery orderByDate($order = Criteria::ASC) Order by the DATE column
 * @method SelOrderProductQuery orderByState($order = Criteria::ASC) Order by the STATE column
 * @method SelOrderProductQuery orderByOrderDetail($order = Criteria::ASC) Order by the ORDER_DETAIL column
 *
 * @method SelOrderProductQuery groupById() Group by the ID column
 * @method SelOrderProductQuery groupByDate() Group by the DATE column
 * @method SelOrderProductQuery groupByState() Group by the STATE column
 * @method SelOrderProductQuery groupByOrderDetail() Group by the ORDER_DETAIL column
 *
 * @method SelOrderProductQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelOrderProductQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelOrderProductQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelOrderProduct findOne(PropelPDO $con = null) Return the first SelOrderProduct matching the query
 * @method SelOrderProduct findOneOrCreate(PropelPDO $con = null) Return the first SelOrderProduct matching the query, or a new SelOrderProduct object populated from the query conditions when no match is found
 *
 * @method SelOrderProduct findOneByDate(string $DATE) Return the first SelOrderProduct filtered by the DATE column
 * @method SelOrderProduct findOneByState(string $STATE) Return the first SelOrderProduct filtered by the STATE column
 * @method SelOrderProduct findOneByOrderDetail(int $ORDER_DETAIL) Return the first SelOrderProduct filtered by the ORDER_DETAIL column
 *
 * @method array findById(int $ID) Return SelOrderProduct objects filtered by the ID column
 * @method array findByDate(string $DATE) Return SelOrderProduct objects filtered by the DATE column
 * @method array findByState(string $STATE) Return SelOrderProduct objects filtered by the STATE column
 * @method array findByOrderDetail(int $ORDER_DETAIL) Return SelOrderProduct objects filtered by the ORDER_DETAIL column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelOrderProductQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelOrderProductQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelOrderProduct', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelOrderProductQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelOrderProductQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelOrderProductQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelOrderProductQuery) {
            return $criteria;
        }
        $query = new SelOrderProductQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelOrderProduct|SelOrderProduct[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelOrderProductPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelOrderProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelOrderProduct A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelOrderProduct A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `DATE`, `STATE`, `ORDER_DETAIL` FROM `sel_order_product` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelOrderProduct();
            $obj->hydrate($row);
            SelOrderProductPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelOrderProduct|SelOrderProduct[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelOrderProduct[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelOrderProductQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelOrderProductPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelOrderProductQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelOrderProductPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderProductQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelOrderProductPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE DATE = '2011-03-14'
     * $query->filterByDate('now'); // WHERE DATE = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderProductQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(SelOrderProductPeer::DATE, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(SelOrderProductPeer::DATE, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderProductPeer::DATE, $date, $comparison);
    }

    /**
     * Filter the query on the STATE column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE STATE = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE STATE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderProductQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelOrderProductPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the ORDER_DETAIL column
     *
     * Example usage:
     * <code>
     * $query->filterByOrderDetail(1234); // WHERE ORDER_DETAIL = 1234
     * $query->filterByOrderDetail(array(12, 34)); // WHERE ORDER_DETAIL IN (12, 34)
     * $query->filterByOrderDetail(array('min' => 12)); // WHERE ORDER_DETAIL > 12
     * </code>
     *
     * @param     mixed $orderDetail The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderProductQuery The current query, for fluid interface
     */
    public function filterByOrderDetail($orderDetail = null, $comparison = null)
    {
        if (is_array($orderDetail)) {
            $useMinMax = false;
            if (isset($orderDetail['min'])) {
                $this->addUsingAlias(SelOrderProductPeer::ORDER_DETAIL, $orderDetail['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($orderDetail['max'])) {
                $this->addUsingAlias(SelOrderProductPeer::ORDER_DETAIL, $orderDetail['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderProductPeer::ORDER_DETAIL, $orderDetail, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   SelOrderProduct $selOrderProduct Object to remove from the list of results
     *
     * @return SelOrderProductQuery The current query, for fluid interface
     */
    public function prune($selOrderProduct = null)
    {
        if ($selOrderProduct) {
            $this->addUsingAlias(SelOrderProductPeer::ID, $selOrderProduct->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
