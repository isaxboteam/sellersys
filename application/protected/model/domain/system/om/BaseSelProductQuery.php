<?php


/**
 * Base class that represents a query for the 'sel_product' table.
 *
 * 
 *
 * @method SelProductQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelProductQuery orderByProductGroupId($order = Criteria::ASC) Order by the PRODUCT_GROUP_ID column
 * @method SelProductQuery orderByBrandId($order = Criteria::ASC) Order by the BRAND_ID column
 * @method SelProductQuery orderByCategoryId($order = Criteria::ASC) Order by the CATEGORY_ID column
 * @method SelProductQuery orderByMainImageId($order = Criteria::ASC) Order by the MAIN_IMAGE_ID column
 * @method SelProductQuery orderByCode($order = Criteria::ASC) Order by the CODE column
 * @method SelProductQuery orderByName($order = Criteria::ASC) Order by the NAME column
 * @method SelProductQuery orderByBasePrice($order = Criteria::ASC) Order by the BASE_PRICE column
 * @method SelProductQuery orderByDescription($order = Criteria::ASC) Order by the DESCRIPTION column
 * @method SelProductQuery orderByModel($order = Criteria::ASC) Order by the MODEL column
 * @method SelProductQuery orderByNumberParts($order = Criteria::ASC) Order by the NUMBER_PARTS column
 * @method SelProductQuery orderByIsMain($order = Criteria::ASC) Order by the IS_MAIN column
 * @method SelProductQuery orderByIsNew($order = Criteria::ASC) Order by the IS_NEW column
 * @method SelProductQuery orderByIsOffer($order = Criteria::ASC) Order by the IS_OFFER column
 *
 * @method SelProductQuery groupById() Group by the ID column
 * @method SelProductQuery groupByProductGroupId() Group by the PRODUCT_GROUP_ID column
 * @method SelProductQuery groupByBrandId() Group by the BRAND_ID column
 * @method SelProductQuery groupByCategoryId() Group by the CATEGORY_ID column
 * @method SelProductQuery groupByMainImageId() Group by the MAIN_IMAGE_ID column
 * @method SelProductQuery groupByCode() Group by the CODE column
 * @method SelProductQuery groupByName() Group by the NAME column
 * @method SelProductQuery groupByBasePrice() Group by the BASE_PRICE column
 * @method SelProductQuery groupByDescription() Group by the DESCRIPTION column
 * @method SelProductQuery groupByModel() Group by the MODEL column
 * @method SelProductQuery groupByNumberParts() Group by the NUMBER_PARTS column
 * @method SelProductQuery groupByIsMain() Group by the IS_MAIN column
 * @method SelProductQuery groupByIsNew() Group by the IS_NEW column
 * @method SelProductQuery groupByIsOffer() Group by the IS_OFFER column
 *
 * @method SelProductQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelProductQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelProductQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelProductQuery leftJoinSelCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelCategory relation
 * @method SelProductQuery rightJoinSelCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelCategory relation
 * @method SelProductQuery innerJoinSelCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the SelCategory relation
 *
 * @method SelProductQuery leftJoinSelProductGroup($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductGroup relation
 * @method SelProductQuery rightJoinSelProductGroup($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductGroup relation
 * @method SelProductQuery innerJoinSelProductGroup($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductGroup relation
 *
 * @method SelProductQuery leftJoinSelBrand($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelBrand relation
 * @method SelProductQuery rightJoinSelBrand($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelBrand relation
 * @method SelProductQuery innerJoinSelBrand($relationAlias = null) Adds a INNER JOIN clause to the query using the SelBrand relation
 *
 * @method SelProductQuery leftJoinSelProductImageRelatedByMainImageId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductImageRelatedByMainImageId relation
 * @method SelProductQuery rightJoinSelProductImageRelatedByMainImageId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductImageRelatedByMainImageId relation
 * @method SelProductQuery innerJoinSelProductImageRelatedByMainImageId($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductImageRelatedByMainImageId relation
 *
 * @method SelProductQuery leftJoinSelProductAcquisition($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductAcquisition relation
 * @method SelProductQuery rightJoinSelProductAcquisition($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductAcquisition relation
 * @method SelProductQuery innerJoinSelProductAcquisition($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductAcquisition relation
 *
 * @method SelProductQuery leftJoinSelProductImageRelatedByProductId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductImageRelatedByProductId relation
 * @method SelProductQuery rightJoinSelProductImageRelatedByProductId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductImageRelatedByProductId relation
 * @method SelProductQuery innerJoinSelProductImageRelatedByProductId($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductImageRelatedByProductId relation
 *
 * @method SelProductQuery leftJoinSelProductOffer($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductOffer relation
 * @method SelProductQuery rightJoinSelProductOffer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductOffer relation
 * @method SelProductQuery innerJoinSelProductOffer($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductOffer relation
 *
 * @method SelProductQuery leftJoinSelProductOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductOrder relation
 * @method SelProductQuery rightJoinSelProductOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductOrder relation
 * @method SelProductQuery innerJoinSelProductOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductOrder relation
 *
 * @method SelProductQuery leftJoinSelProductSell($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductSell relation
 * @method SelProductQuery rightJoinSelProductSell($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductSell relation
 * @method SelProductQuery innerJoinSelProductSell($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductSell relation
 *
 * @method SelProduct findOne(PropelPDO $con = null) Return the first SelProduct matching the query
 * @method SelProduct findOneOrCreate(PropelPDO $con = null) Return the first SelProduct matching the query, or a new SelProduct object populated from the query conditions when no match is found
 *
 * @method SelProduct findOneByProductGroupId(int $PRODUCT_GROUP_ID) Return the first SelProduct filtered by the PRODUCT_GROUP_ID column
 * @method SelProduct findOneByBrandId(int $BRAND_ID) Return the first SelProduct filtered by the BRAND_ID column
 * @method SelProduct findOneByCategoryId(int $CATEGORY_ID) Return the first SelProduct filtered by the CATEGORY_ID column
 * @method SelProduct findOneByMainImageId(int $MAIN_IMAGE_ID) Return the first SelProduct filtered by the MAIN_IMAGE_ID column
 * @method SelProduct findOneByCode(string $CODE) Return the first SelProduct filtered by the CODE column
 * @method SelProduct findOneByName(string $NAME) Return the first SelProduct filtered by the NAME column
 * @method SelProduct findOneByBasePrice(double $BASE_PRICE) Return the first SelProduct filtered by the BASE_PRICE column
 * @method SelProduct findOneByDescription(string $DESCRIPTION) Return the first SelProduct filtered by the DESCRIPTION column
 * @method SelProduct findOneByModel(string $MODEL) Return the first SelProduct filtered by the MODEL column
 * @method SelProduct findOneByNumberParts(int $NUMBER_PARTS) Return the first SelProduct filtered by the NUMBER_PARTS column
 * @method SelProduct findOneByIsMain(boolean $IS_MAIN) Return the first SelProduct filtered by the IS_MAIN column
 * @method SelProduct findOneByIsNew(boolean $IS_NEW) Return the first SelProduct filtered by the IS_NEW column
 * @method SelProduct findOneByIsOffer(boolean $IS_OFFER) Return the first SelProduct filtered by the IS_OFFER column
 *
 * @method array findById(int $ID) Return SelProduct objects filtered by the ID column
 * @method array findByProductGroupId(int $PRODUCT_GROUP_ID) Return SelProduct objects filtered by the PRODUCT_GROUP_ID column
 * @method array findByBrandId(int $BRAND_ID) Return SelProduct objects filtered by the BRAND_ID column
 * @method array findByCategoryId(int $CATEGORY_ID) Return SelProduct objects filtered by the CATEGORY_ID column
 * @method array findByMainImageId(int $MAIN_IMAGE_ID) Return SelProduct objects filtered by the MAIN_IMAGE_ID column
 * @method array findByCode(string $CODE) Return SelProduct objects filtered by the CODE column
 * @method array findByName(string $NAME) Return SelProduct objects filtered by the NAME column
 * @method array findByBasePrice(double $BASE_PRICE) Return SelProduct objects filtered by the BASE_PRICE column
 * @method array findByDescription(string $DESCRIPTION) Return SelProduct objects filtered by the DESCRIPTION column
 * @method array findByModel(string $MODEL) Return SelProduct objects filtered by the MODEL column
 * @method array findByNumberParts(int $NUMBER_PARTS) Return SelProduct objects filtered by the NUMBER_PARTS column
 * @method array findByIsMain(boolean $IS_MAIN) Return SelProduct objects filtered by the IS_MAIN column
 * @method array findByIsNew(boolean $IS_NEW) Return SelProduct objects filtered by the IS_NEW column
 * @method array findByIsOffer(boolean $IS_OFFER) Return SelProduct objects filtered by the IS_OFFER column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelProductQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelProductQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelProduct', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelProductQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelProductQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelProductQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelProductQuery) {
            return $criteria;
        }
        $query = new SelProductQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelProduct|SelProduct[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelProductPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelProductPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProduct A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelProduct A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `PRODUCT_GROUP_ID`, `BRAND_ID`, `CATEGORY_ID`, `MAIN_IMAGE_ID`, `CODE`, `NAME`, `BASE_PRICE`, `DESCRIPTION`, `MODEL`, `NUMBER_PARTS`, `IS_MAIN`, `IS_NEW`, `IS_OFFER` FROM `sel_product` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelProduct();
            $obj->hydrate($row);
            SelProductPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelProduct|SelProduct[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelProduct[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelProductPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelProductPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelProductPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the PRODUCT_GROUP_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByProductGroupId(1234); // WHERE PRODUCT_GROUP_ID = 1234
     * $query->filterByProductGroupId(array(12, 34)); // WHERE PRODUCT_GROUP_ID IN (12, 34)
     * $query->filterByProductGroupId(array('min' => 12)); // WHERE PRODUCT_GROUP_ID > 12
     * </code>
     *
     * @see       filterBySelProductGroup()
     *
     * @param     mixed $productGroupId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByProductGroupId($productGroupId = null, $comparison = null)
    {
        if (is_array($productGroupId)) {
            $useMinMax = false;
            if (isset($productGroupId['min'])) {
                $this->addUsingAlias(SelProductPeer::PRODUCT_GROUP_ID, $productGroupId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productGroupId['max'])) {
                $this->addUsingAlias(SelProductPeer::PRODUCT_GROUP_ID, $productGroupId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductPeer::PRODUCT_GROUP_ID, $productGroupId, $comparison);
    }

    /**
     * Filter the query on the BRAND_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByBrandId(1234); // WHERE BRAND_ID = 1234
     * $query->filterByBrandId(array(12, 34)); // WHERE BRAND_ID IN (12, 34)
     * $query->filterByBrandId(array('min' => 12)); // WHERE BRAND_ID > 12
     * </code>
     *
     * @see       filterBySelBrand()
     *
     * @param     mixed $brandId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByBrandId($brandId = null, $comparison = null)
    {
        if (is_array($brandId)) {
            $useMinMax = false;
            if (isset($brandId['min'])) {
                $this->addUsingAlias(SelProductPeer::BRAND_ID, $brandId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($brandId['max'])) {
                $this->addUsingAlias(SelProductPeer::BRAND_ID, $brandId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductPeer::BRAND_ID, $brandId, $comparison);
    }

    /**
     * Filter the query on the CATEGORY_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryId(1234); // WHERE CATEGORY_ID = 1234
     * $query->filterByCategoryId(array(12, 34)); // WHERE CATEGORY_ID IN (12, 34)
     * $query->filterByCategoryId(array('min' => 12)); // WHERE CATEGORY_ID > 12
     * </code>
     *
     * @see       filterBySelCategory()
     *
     * @param     mixed $categoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByCategoryId($categoryId = null, $comparison = null)
    {
        if (is_array($categoryId)) {
            $useMinMax = false;
            if (isset($categoryId['min'])) {
                $this->addUsingAlias(SelProductPeer::CATEGORY_ID, $categoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoryId['max'])) {
                $this->addUsingAlias(SelProductPeer::CATEGORY_ID, $categoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductPeer::CATEGORY_ID, $categoryId, $comparison);
    }

    /**
     * Filter the query on the MAIN_IMAGE_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByMainImageId(1234); // WHERE MAIN_IMAGE_ID = 1234
     * $query->filterByMainImageId(array(12, 34)); // WHERE MAIN_IMAGE_ID IN (12, 34)
     * $query->filterByMainImageId(array('min' => 12)); // WHERE MAIN_IMAGE_ID > 12
     * </code>
     *
     * @see       filterBySelProductImageRelatedByMainImageId()
     *
     * @param     mixed $mainImageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByMainImageId($mainImageId = null, $comparison = null)
    {
        if (is_array($mainImageId)) {
            $useMinMax = false;
            if (isset($mainImageId['min'])) {
                $this->addUsingAlias(SelProductPeer::MAIN_IMAGE_ID, $mainImageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mainImageId['max'])) {
                $this->addUsingAlias(SelProductPeer::MAIN_IMAGE_ID, $mainImageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductPeer::MAIN_IMAGE_ID, $mainImageId, $comparison);
    }

    /**
     * Filter the query on the CODE column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE CODE = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE CODE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the NAME column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE NAME = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE NAME LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the BASE_PRICE column
     *
     * Example usage:
     * <code>
     * $query->filterByBasePrice(1234); // WHERE BASE_PRICE = 1234
     * $query->filterByBasePrice(array(12, 34)); // WHERE BASE_PRICE IN (12, 34)
     * $query->filterByBasePrice(array('min' => 12)); // WHERE BASE_PRICE > 12
     * </code>
     *
     * @param     mixed $basePrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByBasePrice($basePrice = null, $comparison = null)
    {
        if (is_array($basePrice)) {
            $useMinMax = false;
            if (isset($basePrice['min'])) {
                $this->addUsingAlias(SelProductPeer::BASE_PRICE, $basePrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($basePrice['max'])) {
                $this->addUsingAlias(SelProductPeer::BASE_PRICE, $basePrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductPeer::BASE_PRICE, $basePrice, $comparison);
    }

    /**
     * Filter the query on the DESCRIPTION column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE DESCRIPTION = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE DESCRIPTION LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the MODEL column
     *
     * Example usage:
     * <code>
     * $query->filterByModel('fooValue');   // WHERE MODEL = 'fooValue'
     * $query->filterByModel('%fooValue%'); // WHERE MODEL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $model The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByModel($model = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($model)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $model)) {
                $model = str_replace('*', '%', $model);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelProductPeer::MODEL, $model, $comparison);
    }

    /**
     * Filter the query on the NUMBER_PARTS column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberParts(1234); // WHERE NUMBER_PARTS = 1234
     * $query->filterByNumberParts(array(12, 34)); // WHERE NUMBER_PARTS IN (12, 34)
     * $query->filterByNumberParts(array('min' => 12)); // WHERE NUMBER_PARTS > 12
     * </code>
     *
     * @param     mixed $numberParts The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByNumberParts($numberParts = null, $comparison = null)
    {
        if (is_array($numberParts)) {
            $useMinMax = false;
            if (isset($numberParts['min'])) {
                $this->addUsingAlias(SelProductPeer::NUMBER_PARTS, $numberParts['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numberParts['max'])) {
                $this->addUsingAlias(SelProductPeer::NUMBER_PARTS, $numberParts['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelProductPeer::NUMBER_PARTS, $numberParts, $comparison);
    }

    /**
     * Filter the query on the IS_MAIN column
     *
     * Example usage:
     * <code>
     * $query->filterByIsMain(true); // WHERE IS_MAIN = true
     * $query->filterByIsMain('yes'); // WHERE IS_MAIN = true
     * </code>
     *
     * @param     boolean|string $isMain The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByIsMain($isMain = null, $comparison = null)
    {
        if (is_string($isMain)) {
            $IS_MAIN = in_array(strtolower($isMain), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SelProductPeer::IS_MAIN, $isMain, $comparison);
    }

    /**
     * Filter the query on the IS_NEW column
     *
     * Example usage:
     * <code>
     * $query->filterByIsNew(true); // WHERE IS_NEW = true
     * $query->filterByIsNew('yes'); // WHERE IS_NEW = true
     * </code>
     *
     * @param     boolean|string $isNew The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByIsNew($isNew = null, $comparison = null)
    {
        if (is_string($isNew)) {
            $IS_NEW = in_array(strtolower($isNew), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SelProductPeer::IS_NEW, $isNew, $comparison);
    }

    /**
     * Filter the query on the IS_OFFER column
     *
     * Example usage:
     * <code>
     * $query->filterByIsOffer(true); // WHERE IS_OFFER = true
     * $query->filterByIsOffer('yes'); // WHERE IS_OFFER = true
     * </code>
     *
     * @param     boolean|string $isOffer The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function filterByIsOffer($isOffer = null, $comparison = null)
    {
        if (is_string($isOffer)) {
            $IS_OFFER = in_array(strtolower($isOffer), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SelProductPeer::IS_OFFER, $isOffer, $comparison);
    }

    /**
     * Filter the query by a related SelCategory object
     *
     * @param   SelCategory|PropelObjectCollection $selCategory The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelCategory($selCategory, $comparison = null)
    {
        if ($selCategory instanceof SelCategory) {
            return $this
                ->addUsingAlias(SelProductPeer::CATEGORY_ID, $selCategory->getId(), $comparison);
        } elseif ($selCategory instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductPeer::CATEGORY_ID, $selCategory->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelCategory() only accepts arguments of type SelCategory or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelCategory relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function joinSelCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelCategory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelCategory');
        }

        return $this;
    }

    /**
     * Use the SelCategory relation SelCategory object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelCategoryQuery A secondary query class using the current class as primary query
     */
    public function useSelCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelCategory', 'SelCategoryQuery');
    }

    /**
     * Filter the query by a related SelProductGroup object
     *
     * @param   SelProductGroup|PropelObjectCollection $selProductGroup The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductGroup($selProductGroup, $comparison = null)
    {
        if ($selProductGroup instanceof SelProductGroup) {
            return $this
                ->addUsingAlias(SelProductPeer::PRODUCT_GROUP_ID, $selProductGroup->getId(), $comparison);
        } elseif ($selProductGroup instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductPeer::PRODUCT_GROUP_ID, $selProductGroup->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelProductGroup() only accepts arguments of type SelProductGroup or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductGroup relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function joinSelProductGroup($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductGroup');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductGroup');
        }

        return $this;
    }

    /**
     * Use the SelProductGroup relation SelProductGroup object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductGroupQuery A secondary query class using the current class as primary query
     */
    public function useSelProductGroupQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelProductGroup($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductGroup', 'SelProductGroupQuery');
    }

    /**
     * Filter the query by a related SelBrand object
     *
     * @param   SelBrand|PropelObjectCollection $selBrand The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelBrand($selBrand, $comparison = null)
    {
        if ($selBrand instanceof SelBrand) {
            return $this
                ->addUsingAlias(SelProductPeer::BRAND_ID, $selBrand->getId(), $comparison);
        } elseif ($selBrand instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductPeer::BRAND_ID, $selBrand->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelBrand() only accepts arguments of type SelBrand or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelBrand relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function joinSelBrand($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelBrand');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelBrand');
        }

        return $this;
    }

    /**
     * Use the SelBrand relation SelBrand object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelBrandQuery A secondary query class using the current class as primary query
     */
    public function useSelBrandQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelBrand($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelBrand', 'SelBrandQuery');
    }

    /**
     * Filter the query by a related SelProductImage object
     *
     * @param   SelProductImage|PropelObjectCollection $selProductImage The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductImageRelatedByMainImageId($selProductImage, $comparison = null)
    {
        if ($selProductImage instanceof SelProductImage) {
            return $this
                ->addUsingAlias(SelProductPeer::MAIN_IMAGE_ID, $selProductImage->getId(), $comparison);
        } elseif ($selProductImage instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelProductPeer::MAIN_IMAGE_ID, $selProductImage->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelProductImageRelatedByMainImageId() only accepts arguments of type SelProductImage or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductImageRelatedByMainImageId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function joinSelProductImageRelatedByMainImageId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductImageRelatedByMainImageId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductImageRelatedByMainImageId');
        }

        return $this;
    }

    /**
     * Use the SelProductImageRelatedByMainImageId relation SelProductImage object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductImageQuery A secondary query class using the current class as primary query
     */
    public function useSelProductImageRelatedByMainImageIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelProductImageRelatedByMainImageId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductImageRelatedByMainImageId', 'SelProductImageQuery');
    }

    /**
     * Filter the query by a related SelProductAcquisition object
     *
     * @param   SelProductAcquisition|PropelObjectCollection $selProductAcquisition  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductAcquisition($selProductAcquisition, $comparison = null)
    {
        if ($selProductAcquisition instanceof SelProductAcquisition) {
            return $this
                ->addUsingAlias(SelProductPeer::ID, $selProductAcquisition->getProductId(), $comparison);
        } elseif ($selProductAcquisition instanceof PropelObjectCollection) {
            return $this
                ->useSelProductAcquisitionQuery()
                ->filterByPrimaryKeys($selProductAcquisition->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductAcquisition() only accepts arguments of type SelProductAcquisition or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductAcquisition relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function joinSelProductAcquisition($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductAcquisition');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductAcquisition');
        }

        return $this;
    }

    /**
     * Use the SelProductAcquisition relation SelProductAcquisition object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductAcquisitionQuery A secondary query class using the current class as primary query
     */
    public function useSelProductAcquisitionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductAcquisition($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductAcquisition', 'SelProductAcquisitionQuery');
    }

    /**
     * Filter the query by a related SelProductImage object
     *
     * @param   SelProductImage|PropelObjectCollection $selProductImage  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductImageRelatedByProductId($selProductImage, $comparison = null)
    {
        if ($selProductImage instanceof SelProductImage) {
            return $this
                ->addUsingAlias(SelProductPeer::ID, $selProductImage->getProductId(), $comparison);
        } elseif ($selProductImage instanceof PropelObjectCollection) {
            return $this
                ->useSelProductImageRelatedByProductIdQuery()
                ->filterByPrimaryKeys($selProductImage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductImageRelatedByProductId() only accepts arguments of type SelProductImage or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductImageRelatedByProductId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function joinSelProductImageRelatedByProductId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductImageRelatedByProductId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductImageRelatedByProductId');
        }

        return $this;
    }

    /**
     * Use the SelProductImageRelatedByProductId relation SelProductImage object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductImageQuery A secondary query class using the current class as primary query
     */
    public function useSelProductImageRelatedByProductIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductImageRelatedByProductId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductImageRelatedByProductId', 'SelProductImageQuery');
    }

    /**
     * Filter the query by a related SelProductOffer object
     *
     * @param   SelProductOffer|PropelObjectCollection $selProductOffer  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductOffer($selProductOffer, $comparison = null)
    {
        if ($selProductOffer instanceof SelProductOffer) {
            return $this
                ->addUsingAlias(SelProductPeer::ID, $selProductOffer->getId(), $comparison);
        } elseif ($selProductOffer instanceof PropelObjectCollection) {
            return $this
                ->useSelProductOfferQuery()
                ->filterByPrimaryKeys($selProductOffer->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductOffer() only accepts arguments of type SelProductOffer or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductOffer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function joinSelProductOffer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductOffer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductOffer');
        }

        return $this;
    }

    /**
     * Use the SelProductOffer relation SelProductOffer object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductOfferQuery A secondary query class using the current class as primary query
     */
    public function useSelProductOfferQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductOffer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductOffer', 'SelProductOfferQuery');
    }

    /**
     * Filter the query by a related SelProductOrder object
     *
     * @param   SelProductOrder|PropelObjectCollection $selProductOrder  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductOrder($selProductOrder, $comparison = null)
    {
        if ($selProductOrder instanceof SelProductOrder) {
            return $this
                ->addUsingAlias(SelProductPeer::ID, $selProductOrder->getProductId(), $comparison);
        } elseif ($selProductOrder instanceof PropelObjectCollection) {
            return $this
                ->useSelProductOrderQuery()
                ->filterByPrimaryKeys($selProductOrder->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductOrder() only accepts arguments of type SelProductOrder or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function joinSelProductOrder($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductOrder');
        }

        return $this;
    }

    /**
     * Use the SelProductOrder relation SelProductOrder object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductOrderQuery A secondary query class using the current class as primary query
     */
    public function useSelProductOrderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductOrder', 'SelProductOrderQuery');
    }

    /**
     * Filter the query by a related SelProductSell object
     *
     * @param   SelProductSell|PropelObjectCollection $selProductSell  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelProductQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductSell($selProductSell, $comparison = null)
    {
        if ($selProductSell instanceof SelProductSell) {
            return $this
                ->addUsingAlias(SelProductPeer::ID, $selProductSell->getProductId(), $comparison);
        } elseif ($selProductSell instanceof PropelObjectCollection) {
            return $this
                ->useSelProductSellQuery()
                ->filterByPrimaryKeys($selProductSell->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductSell() only accepts arguments of type SelProductSell or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductSell relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function joinSelProductSell($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductSell');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductSell');
        }

        return $this;
    }

    /**
     * Use the SelProductSell relation SelProductSell object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductSellQuery A secondary query class using the current class as primary query
     */
    public function useSelProductSellQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductSell($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductSell', 'SelProductSellQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelProduct $selProduct Object to remove from the list of results
     *
     * @return SelProductQuery The current query, for fluid interface
     */
    public function prune($selProduct = null)
    {
        if ($selProduct) {
            $this->addUsingAlias(SelProductPeer::ID, $selProduct->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
