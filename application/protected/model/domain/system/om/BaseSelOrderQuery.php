<?php


/**
 * Base class that represents a query for the 'sel_order' table.
 *
 * 
 *
 * @method SelOrderQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelOrderQuery orderByCustomerId($order = Criteria::ASC) Order by the CUSTOMER_ID column
 * @method SelOrderQuery orderBySalesId($order = Criteria::ASC) Order by the SALES_ID column
 * @method SelOrderQuery orderByStatus($order = Criteria::ASC) Order by the STATUS column
 * @method SelOrderQuery orderByShippingAddress($order = Criteria::ASC) Order by the SHIPPING_ADDRESS column
 * @method SelOrderQuery orderByPhone($order = Criteria::ASC) Order by the PHONE column
 * @method SelOrderQuery orderByCustommerComment($order = Criteria::ASC) Order by the CUSTOMMER_COMMENT column
 * @method SelOrderQuery orderBySalesComment($order = Criteria::ASC) Order by the SALES_COMMENT column
 * @method SelOrderQuery orderByCreationDate($order = Criteria::ASC) Order by the CREATION_DATE column
 * @method SelOrderQuery orderByOrderDate($order = Criteria::ASC) Order by the ORDER_DATE column
 * @method SelOrderQuery orderByPaymentDate($order = Criteria::ASC) Order by the PAYMENT_DATE column
 * @method SelOrderQuery orderByPaymentCode($order = Criteria::ASC) Order by the PAYMENT_CODE column
 * @method SelOrderQuery orderByPaymentEntity($order = Criteria::ASC) Order by the PAYMENT_ENTITY column
 * @method SelOrderQuery orderByCheckDate($order = Criteria::ASC) Order by the CHECK_DATE column
 * @method SelOrderQuery orderByApprovedDate($order = Criteria::ASC) Order by the APPROVED_DATE column
 * @method SelOrderQuery orderByShippingDate($order = Criteria::ASC) Order by the SHIPPING_DATE column
 * @method SelOrderQuery orderByDeliveryDate($order = Criteria::ASC) Order by the DELIVERY_DATE column
 *
 * @method SelOrderQuery groupById() Group by the ID column
 * @method SelOrderQuery groupByCustomerId() Group by the CUSTOMER_ID column
 * @method SelOrderQuery groupBySalesId() Group by the SALES_ID column
 * @method SelOrderQuery groupByStatus() Group by the STATUS column
 * @method SelOrderQuery groupByShippingAddress() Group by the SHIPPING_ADDRESS column
 * @method SelOrderQuery groupByPhone() Group by the PHONE column
 * @method SelOrderQuery groupByCustommerComment() Group by the CUSTOMMER_COMMENT column
 * @method SelOrderQuery groupBySalesComment() Group by the SALES_COMMENT column
 * @method SelOrderQuery groupByCreationDate() Group by the CREATION_DATE column
 * @method SelOrderQuery groupByOrderDate() Group by the ORDER_DATE column
 * @method SelOrderQuery groupByPaymentDate() Group by the PAYMENT_DATE column
 * @method SelOrderQuery groupByPaymentCode() Group by the PAYMENT_CODE column
 * @method SelOrderQuery groupByPaymentEntity() Group by the PAYMENT_ENTITY column
 * @method SelOrderQuery groupByCheckDate() Group by the CHECK_DATE column
 * @method SelOrderQuery groupByApprovedDate() Group by the APPROVED_DATE column
 * @method SelOrderQuery groupByShippingDate() Group by the SHIPPING_DATE column
 * @method SelOrderQuery groupByDeliveryDate() Group by the DELIVERY_DATE column
 *
 * @method SelOrderQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelOrderQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelOrderQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelOrderQuery leftJoinSelSale($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelSale relation
 * @method SelOrderQuery rightJoinSelSale($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelSale relation
 * @method SelOrderQuery innerJoinSelSale($relationAlias = null) Adds a INNER JOIN clause to the query using the SelSale relation
 *
 * @method SelOrderQuery leftJoinSelCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelCustomer relation
 * @method SelOrderQuery rightJoinSelCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelCustomer relation
 * @method SelOrderQuery innerJoinSelCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the SelCustomer relation
 *
 * @method SelOrderQuery leftJoinSelProductOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelProductOrder relation
 * @method SelOrderQuery rightJoinSelProductOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelProductOrder relation
 * @method SelOrderQuery innerJoinSelProductOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SelProductOrder relation
 *
 * @method SelOrder findOne(PropelPDO $con = null) Return the first SelOrder matching the query
 * @method SelOrder findOneOrCreate(PropelPDO $con = null) Return the first SelOrder matching the query, or a new SelOrder object populated from the query conditions when no match is found
 *
 * @method SelOrder findOneByCustomerId(int $CUSTOMER_ID) Return the first SelOrder filtered by the CUSTOMER_ID column
 * @method SelOrder findOneBySalesId(int $SALES_ID) Return the first SelOrder filtered by the SALES_ID column
 * @method SelOrder findOneByStatus(string $STATUS) Return the first SelOrder filtered by the STATUS column
 * @method SelOrder findOneByShippingAddress(string $SHIPPING_ADDRESS) Return the first SelOrder filtered by the SHIPPING_ADDRESS column
 * @method SelOrder findOneByPhone(string $PHONE) Return the first SelOrder filtered by the PHONE column
 * @method SelOrder findOneByCustommerComment(string $CUSTOMMER_COMMENT) Return the first SelOrder filtered by the CUSTOMMER_COMMENT column
 * @method SelOrder findOneBySalesComment(int $SALES_COMMENT) Return the first SelOrder filtered by the SALES_COMMENT column
 * @method SelOrder findOneByCreationDate(string $CREATION_DATE) Return the first SelOrder filtered by the CREATION_DATE column
 * @method SelOrder findOneByOrderDate(string $ORDER_DATE) Return the first SelOrder filtered by the ORDER_DATE column
 * @method SelOrder findOneByPaymentDate(string $PAYMENT_DATE) Return the first SelOrder filtered by the PAYMENT_DATE column
 * @method SelOrder findOneByPaymentCode(string $PAYMENT_CODE) Return the first SelOrder filtered by the PAYMENT_CODE column
 * @method SelOrder findOneByPaymentEntity(string $PAYMENT_ENTITY) Return the first SelOrder filtered by the PAYMENT_ENTITY column
 * @method SelOrder findOneByCheckDate(string $CHECK_DATE) Return the first SelOrder filtered by the CHECK_DATE column
 * @method SelOrder findOneByApprovedDate(string $APPROVED_DATE) Return the first SelOrder filtered by the APPROVED_DATE column
 * @method SelOrder findOneByShippingDate(string $SHIPPING_DATE) Return the first SelOrder filtered by the SHIPPING_DATE column
 * @method SelOrder findOneByDeliveryDate(string $DELIVERY_DATE) Return the first SelOrder filtered by the DELIVERY_DATE column
 *
 * @method array findById(int $ID) Return SelOrder objects filtered by the ID column
 * @method array findByCustomerId(int $CUSTOMER_ID) Return SelOrder objects filtered by the CUSTOMER_ID column
 * @method array findBySalesId(int $SALES_ID) Return SelOrder objects filtered by the SALES_ID column
 * @method array findByStatus(string $STATUS) Return SelOrder objects filtered by the STATUS column
 * @method array findByShippingAddress(string $SHIPPING_ADDRESS) Return SelOrder objects filtered by the SHIPPING_ADDRESS column
 * @method array findByPhone(string $PHONE) Return SelOrder objects filtered by the PHONE column
 * @method array findByCustommerComment(string $CUSTOMMER_COMMENT) Return SelOrder objects filtered by the CUSTOMMER_COMMENT column
 * @method array findBySalesComment(int $SALES_COMMENT) Return SelOrder objects filtered by the SALES_COMMENT column
 * @method array findByCreationDate(string $CREATION_DATE) Return SelOrder objects filtered by the CREATION_DATE column
 * @method array findByOrderDate(string $ORDER_DATE) Return SelOrder objects filtered by the ORDER_DATE column
 * @method array findByPaymentDate(string $PAYMENT_DATE) Return SelOrder objects filtered by the PAYMENT_DATE column
 * @method array findByPaymentCode(string $PAYMENT_CODE) Return SelOrder objects filtered by the PAYMENT_CODE column
 * @method array findByPaymentEntity(string $PAYMENT_ENTITY) Return SelOrder objects filtered by the PAYMENT_ENTITY column
 * @method array findByCheckDate(string $CHECK_DATE) Return SelOrder objects filtered by the CHECK_DATE column
 * @method array findByApprovedDate(string $APPROVED_DATE) Return SelOrder objects filtered by the APPROVED_DATE column
 * @method array findByShippingDate(string $SHIPPING_DATE) Return SelOrder objects filtered by the SHIPPING_DATE column
 * @method array findByDeliveryDate(string $DELIVERY_DATE) Return SelOrder objects filtered by the DELIVERY_DATE column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelOrderQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelOrderQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelOrder', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelOrderQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelOrderQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelOrderQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelOrderQuery) {
            return $criteria;
        }
        $query = new SelOrderQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelOrder|SelOrder[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelOrderPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelOrderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelOrder A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelOrder A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `CUSTOMER_ID`, `SALES_ID`, `STATUS`, `SHIPPING_ADDRESS`, `PHONE`, `CUSTOMMER_COMMENT`, `SALES_COMMENT`, `CREATION_DATE`, `ORDER_DATE`, `PAYMENT_DATE`, `PAYMENT_CODE`, `PAYMENT_ENTITY`, `CHECK_DATE`, `APPROVED_DATE`, `SHIPPING_DATE`, `DELIVERY_DATE` FROM `sel_order` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelOrder();
            $obj->hydrate($row);
            SelOrderPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelOrder|SelOrder[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelOrder[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelOrderPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelOrderPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelOrderPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the CUSTOMER_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerId(1234); // WHERE CUSTOMER_ID = 1234
     * $query->filterByCustomerId(array(12, 34)); // WHERE CUSTOMER_ID IN (12, 34)
     * $query->filterByCustomerId(array('min' => 12)); // WHERE CUSTOMER_ID > 12
     * </code>
     *
     * @see       filterBySelCustomer()
     *
     * @param     mixed $customerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerId($customerId = null, $comparison = null)
    {
        if (is_array($customerId)) {
            $useMinMax = false;
            if (isset($customerId['min'])) {
                $this->addUsingAlias(SelOrderPeer::CUSTOMER_ID, $customerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerId['max'])) {
                $this->addUsingAlias(SelOrderPeer::CUSTOMER_ID, $customerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::CUSTOMER_ID, $customerId, $comparison);
    }

    /**
     * Filter the query on the SALES_ID column
     *
     * Example usage:
     * <code>
     * $query->filterBySalesId(1234); // WHERE SALES_ID = 1234
     * $query->filterBySalesId(array(12, 34)); // WHERE SALES_ID IN (12, 34)
     * $query->filterBySalesId(array('min' => 12)); // WHERE SALES_ID > 12
     * </code>
     *
     * @see       filterBySelSale()
     *
     * @param     mixed $salesId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterBySalesId($salesId = null, $comparison = null)
    {
        if (is_array($salesId)) {
            $useMinMax = false;
            if (isset($salesId['min'])) {
                $this->addUsingAlias(SelOrderPeer::SALES_ID, $salesId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($salesId['max'])) {
                $this->addUsingAlias(SelOrderPeer::SALES_ID, $salesId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::SALES_ID, $salesId, $comparison);
    }

    /**
     * Filter the query on the STATUS column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE STATUS = 'fooValue'
     * $query->filterByStatus('%fooValue%'); // WHERE STATUS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $status)) {
                $status = str_replace('*', '%', $status);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the SHIPPING_ADDRESS column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingAddress('fooValue');   // WHERE SHIPPING_ADDRESS = 'fooValue'
     * $query->filterByShippingAddress('%fooValue%'); // WHERE SHIPPING_ADDRESS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shippingAddress The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByShippingAddress($shippingAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shippingAddress)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $shippingAddress)) {
                $shippingAddress = str_replace('*', '%', $shippingAddress);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::SHIPPING_ADDRESS, $shippingAddress, $comparison);
    }

    /**
     * Filter the query on the PHONE column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE PHONE = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE PHONE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the CUSTOMMER_COMMENT column
     *
     * Example usage:
     * <code>
     * $query->filterByCustommerComment('fooValue');   // WHERE CUSTOMMER_COMMENT = 'fooValue'
     * $query->filterByCustommerComment('%fooValue%'); // WHERE CUSTOMMER_COMMENT LIKE '%fooValue%'
     * </code>
     *
     * @param     string $custommerComment The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByCustommerComment($custommerComment = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($custommerComment)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $custommerComment)) {
                $custommerComment = str_replace('*', '%', $custommerComment);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::CUSTOMMER_COMMENT, $custommerComment, $comparison);
    }

    /**
     * Filter the query on the SALES_COMMENT column
     *
     * Example usage:
     * <code>
     * $query->filterBySalesComment(1234); // WHERE SALES_COMMENT = 1234
     * $query->filterBySalesComment(array(12, 34)); // WHERE SALES_COMMENT IN (12, 34)
     * $query->filterBySalesComment(array('min' => 12)); // WHERE SALES_COMMENT > 12
     * </code>
     *
     * @param     mixed $salesComment The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterBySalesComment($salesComment = null, $comparison = null)
    {
        if (is_array($salesComment)) {
            $useMinMax = false;
            if (isset($salesComment['min'])) {
                $this->addUsingAlias(SelOrderPeer::SALES_COMMENT, $salesComment['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($salesComment['max'])) {
                $this->addUsingAlias(SelOrderPeer::SALES_COMMENT, $salesComment['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::SALES_COMMENT, $salesComment, $comparison);
    }

    /**
     * Filter the query on the CREATION_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByCreationDate('2011-03-14'); // WHERE CREATION_DATE = '2011-03-14'
     * $query->filterByCreationDate('now'); // WHERE CREATION_DATE = '2011-03-14'
     * $query->filterByCreationDate(array('max' => 'yesterday')); // WHERE CREATION_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $creationDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByCreationDate($creationDate = null, $comparison = null)
    {
        if (is_array($creationDate)) {
            $useMinMax = false;
            if (isset($creationDate['min'])) {
                $this->addUsingAlias(SelOrderPeer::CREATION_DATE, $creationDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($creationDate['max'])) {
                $this->addUsingAlias(SelOrderPeer::CREATION_DATE, $creationDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::CREATION_DATE, $creationDate, $comparison);
    }

    /**
     * Filter the query on the ORDER_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByOrderDate('2011-03-14'); // WHERE ORDER_DATE = '2011-03-14'
     * $query->filterByOrderDate('now'); // WHERE ORDER_DATE = '2011-03-14'
     * $query->filterByOrderDate(array('max' => 'yesterday')); // WHERE ORDER_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $orderDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByOrderDate($orderDate = null, $comparison = null)
    {
        if (is_array($orderDate)) {
            $useMinMax = false;
            if (isset($orderDate['min'])) {
                $this->addUsingAlias(SelOrderPeer::ORDER_DATE, $orderDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($orderDate['max'])) {
                $this->addUsingAlias(SelOrderPeer::ORDER_DATE, $orderDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::ORDER_DATE, $orderDate, $comparison);
    }

    /**
     * Filter the query on the PAYMENT_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentDate('2011-03-14'); // WHERE PAYMENT_DATE = '2011-03-14'
     * $query->filterByPaymentDate('now'); // WHERE PAYMENT_DATE = '2011-03-14'
     * $query->filterByPaymentDate(array('max' => 'yesterday')); // WHERE PAYMENT_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $paymentDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByPaymentDate($paymentDate = null, $comparison = null)
    {
        if (is_array($paymentDate)) {
            $useMinMax = false;
            if (isset($paymentDate['min'])) {
                $this->addUsingAlias(SelOrderPeer::PAYMENT_DATE, $paymentDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paymentDate['max'])) {
                $this->addUsingAlias(SelOrderPeer::PAYMENT_DATE, $paymentDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::PAYMENT_DATE, $paymentDate, $comparison);
    }

    /**
     * Filter the query on the PAYMENT_CODE column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentCode('fooValue');   // WHERE PAYMENT_CODE = 'fooValue'
     * $query->filterByPaymentCode('%fooValue%'); // WHERE PAYMENT_CODE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByPaymentCode($paymentCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentCode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paymentCode)) {
                $paymentCode = str_replace('*', '%', $paymentCode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::PAYMENT_CODE, $paymentCode, $comparison);
    }

    /**
     * Filter the query on the PAYMENT_ENTITY column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentEntity('fooValue');   // WHERE PAYMENT_ENTITY = 'fooValue'
     * $query->filterByPaymentEntity('%fooValue%'); // WHERE PAYMENT_ENTITY LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentEntity The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByPaymentEntity($paymentEntity = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentEntity)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paymentEntity)) {
                $paymentEntity = str_replace('*', '%', $paymentEntity);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::PAYMENT_ENTITY, $paymentEntity, $comparison);
    }

    /**
     * Filter the query on the CHECK_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByCheckDate('2011-03-14'); // WHERE CHECK_DATE = '2011-03-14'
     * $query->filterByCheckDate('now'); // WHERE CHECK_DATE = '2011-03-14'
     * $query->filterByCheckDate(array('max' => 'yesterday')); // WHERE CHECK_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $checkDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByCheckDate($checkDate = null, $comparison = null)
    {
        if (is_array($checkDate)) {
            $useMinMax = false;
            if (isset($checkDate['min'])) {
                $this->addUsingAlias(SelOrderPeer::CHECK_DATE, $checkDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($checkDate['max'])) {
                $this->addUsingAlias(SelOrderPeer::CHECK_DATE, $checkDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::CHECK_DATE, $checkDate, $comparison);
    }

    /**
     * Filter the query on the APPROVED_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByApprovedDate('2011-03-14'); // WHERE APPROVED_DATE = '2011-03-14'
     * $query->filterByApprovedDate('now'); // WHERE APPROVED_DATE = '2011-03-14'
     * $query->filterByApprovedDate(array('max' => 'yesterday')); // WHERE APPROVED_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $approvedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByApprovedDate($approvedDate = null, $comparison = null)
    {
        if (is_array($approvedDate)) {
            $useMinMax = false;
            if (isset($approvedDate['min'])) {
                $this->addUsingAlias(SelOrderPeer::APPROVED_DATE, $approvedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($approvedDate['max'])) {
                $this->addUsingAlias(SelOrderPeer::APPROVED_DATE, $approvedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::APPROVED_DATE, $approvedDate, $comparison);
    }

    /**
     * Filter the query on the SHIPPING_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingDate('2011-03-14'); // WHERE SHIPPING_DATE = '2011-03-14'
     * $query->filterByShippingDate('now'); // WHERE SHIPPING_DATE = '2011-03-14'
     * $query->filterByShippingDate(array('max' => 'yesterday')); // WHERE SHIPPING_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $shippingDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByShippingDate($shippingDate = null, $comparison = null)
    {
        if (is_array($shippingDate)) {
            $useMinMax = false;
            if (isset($shippingDate['min'])) {
                $this->addUsingAlias(SelOrderPeer::SHIPPING_DATE, $shippingDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shippingDate['max'])) {
                $this->addUsingAlias(SelOrderPeer::SHIPPING_DATE, $shippingDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::SHIPPING_DATE, $shippingDate, $comparison);
    }

    /**
     * Filter the query on the DELIVERY_DATE column
     *
     * Example usage:
     * <code>
     * $query->filterByDeliveryDate('2011-03-14'); // WHERE DELIVERY_DATE = '2011-03-14'
     * $query->filterByDeliveryDate('now'); // WHERE DELIVERY_DATE = '2011-03-14'
     * $query->filterByDeliveryDate(array('max' => 'yesterday')); // WHERE DELIVERY_DATE > '2011-03-13'
     * </code>
     *
     * @param     mixed $deliveryDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function filterByDeliveryDate($deliveryDate = null, $comparison = null)
    {
        if (is_array($deliveryDate)) {
            $useMinMax = false;
            if (isset($deliveryDate['min'])) {
                $this->addUsingAlias(SelOrderPeer::DELIVERY_DATE, $deliveryDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deliveryDate['max'])) {
                $this->addUsingAlias(SelOrderPeer::DELIVERY_DATE, $deliveryDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelOrderPeer::DELIVERY_DATE, $deliveryDate, $comparison);
    }

    /**
     * Filter the query by a related SelSale object
     *
     * @param   SelSale|PropelObjectCollection $selSale The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelOrderQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelSale($selSale, $comparison = null)
    {
        if ($selSale instanceof SelSale) {
            return $this
                ->addUsingAlias(SelOrderPeer::SALES_ID, $selSale->getId(), $comparison);
        } elseif ($selSale instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelOrderPeer::SALES_ID, $selSale->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelSale() only accepts arguments of type SelSale or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelSale relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function joinSelSale($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelSale');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelSale');
        }

        return $this;
    }

    /**
     * Use the SelSale relation SelSale object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelSaleQuery A secondary query class using the current class as primary query
     */
    public function useSelSaleQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSelSale($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelSale', 'SelSaleQuery');
    }

    /**
     * Filter the query by a related SelCustomer object
     *
     * @param   SelCustomer|PropelObjectCollection $selCustomer The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelOrderQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelCustomer($selCustomer, $comparison = null)
    {
        if ($selCustomer instanceof SelCustomer) {
            return $this
                ->addUsingAlias(SelOrderPeer::CUSTOMER_ID, $selCustomer->getId(), $comparison);
        } elseif ($selCustomer instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelOrderPeer::CUSTOMER_ID, $selCustomer->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelCustomer() only accepts arguments of type SelCustomer or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelCustomer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function joinSelCustomer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelCustomer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelCustomer');
        }

        return $this;
    }

    /**
     * Use the SelCustomer relation SelCustomer object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelCustomerQuery A secondary query class using the current class as primary query
     */
    public function useSelCustomerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelCustomer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelCustomer', 'SelCustomerQuery');
    }

    /**
     * Filter the query by a related SelProductOrder object
     *
     * @param   SelProductOrder|PropelObjectCollection $selProductOrder  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelOrderQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelProductOrder($selProductOrder, $comparison = null)
    {
        if ($selProductOrder instanceof SelProductOrder) {
            return $this
                ->addUsingAlias(SelOrderPeer::ID, $selProductOrder->getOrderId(), $comparison);
        } elseif ($selProductOrder instanceof PropelObjectCollection) {
            return $this
                ->useSelProductOrderQuery()
                ->filterByPrimaryKeys($selProductOrder->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySelProductOrder() only accepts arguments of type SelProductOrder or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelProductOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function joinSelProductOrder($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelProductOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelProductOrder');
        }

        return $this;
    }

    /**
     * Use the SelProductOrder relation SelProductOrder object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelProductOrderQuery A secondary query class using the current class as primary query
     */
    public function useSelProductOrderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelProductOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelProductOrder', 'SelProductOrderQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelOrder $selOrder Object to remove from the list of results
     *
     * @return SelOrderQuery The current query, for fluid interface
     */
    public function prune($selOrder = null)
    {
        if ($selOrder) {
            $this->addUsingAlias(SelOrderPeer::ID, $selOrder->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
