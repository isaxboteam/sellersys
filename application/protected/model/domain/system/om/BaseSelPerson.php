<?php


/**
 * Base class that represents a row from the 'sel_person' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelPerson extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelPersonPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  SelPersonPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the first_lastname field.
     * @var        string
     */
    protected $first_lastname;

    /**
     * The value for the second_lastname field.
     * @var        string
     */
    protected $second_lastname;

    /**
     * The value for the identity field.
     * @var        int
     */
    protected $identity;

    /**
     * The value for the identity_ext field.
     * @var        string
     */
    protected $identity_ext;

    /**
     * The value for the gender field.
     * @var        string
     */
    protected $gender;

    /**
     * The value for the birthday_date field.
     * @var        string
     */
    protected $birthday_date;

    /**
     * @var        PropelObjectCollection|SelProvider[] Collection to store aggregation of SelProvider objects.
     */
    protected $collSelProviders;
    protected $collSelProvidersPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $selProvidersScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [name] column value.
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [first_lastname] column value.
     * 
     * @return string
     */
    public function getFirstLastname()
    {
        return $this->first_lastname;
    }

    /**
     * Get the [second_lastname] column value.
     * 
     * @return string
     */
    public function getSecondLastname()
    {
        return $this->second_lastname;
    }

    /**
     * Get the [identity] column value.
     * 
     * @return int
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Get the [identity_ext] column value.
     * 
     * @return string
     */
    public function getIdentityExt()
    {
        return $this->identity_ext;
    }

    /**
     * Get the [gender] column value.
     * 
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Get the [optionally formatted] temporal [birthday_date] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getBirthdayDate($format = '%x')
    {
        if ($this->birthday_date === null) {
            return null;
        }

        if ($this->birthday_date === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->birthday_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->birthday_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return SelPerson The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = SelPersonPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [name] column.
     * 
     * @param string $v new value
     * @return SelPerson The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = SelPersonPeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [first_lastname] column.
     * 
     * @param string $v new value
     * @return SelPerson The current object (for fluent API support)
     */
    public function setFirstLastname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->first_lastname !== $v) {
            $this->first_lastname = $v;
            $this->modifiedColumns[] = SelPersonPeer::FIRST_LASTNAME;
        }


        return $this;
    } // setFirstLastname()

    /**
     * Set the value of [second_lastname] column.
     * 
     * @param string $v new value
     * @return SelPerson The current object (for fluent API support)
     */
    public function setSecondLastname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->second_lastname !== $v) {
            $this->second_lastname = $v;
            $this->modifiedColumns[] = SelPersonPeer::SECOND_LASTNAME;
        }


        return $this;
    } // setSecondLastname()

    /**
     * Set the value of [identity] column.
     * 
     * @param int $v new value
     * @return SelPerson The current object (for fluent API support)
     */
    public function setIdentity($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->identity !== $v) {
            $this->identity = $v;
            $this->modifiedColumns[] = SelPersonPeer::IDENTITY;
        }


        return $this;
    } // setIdentity()

    /**
     * Set the value of [identity_ext] column.
     * 
     * @param string $v new value
     * @return SelPerson The current object (for fluent API support)
     */
    public function setIdentityExt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->identity_ext !== $v) {
            $this->identity_ext = $v;
            $this->modifiedColumns[] = SelPersonPeer::IDENTITY_EXT;
        }


        return $this;
    } // setIdentityExt()

    /**
     * Set the value of [gender] column.
     * 
     * @param string $v new value
     * @return SelPerson The current object (for fluent API support)
     */
    public function setGender($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gender !== $v) {
            $this->gender = $v;
            $this->modifiedColumns[] = SelPersonPeer::GENDER;
        }


        return $this;
    } // setGender()

    /**
     * Sets the value of [birthday_date] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SelPerson The current object (for fluent API support)
     */
    public function setBirthdayDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->birthday_date !== null || $dt !== null) {
            $currentDateAsString = ($this->birthday_date !== null && $tmpDt = new DateTime($this->birthday_date)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->birthday_date = $newDateAsString;
                $this->modifiedColumns[] = SelPersonPeer::BIRTHDAY_DATE;
            }
        } // if either are not null


        return $this;
    } // setBirthdayDate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->name = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->first_lastname = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->second_lastname = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->identity = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->identity_ext = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->gender = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->birthday_date = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 8; // 8 = SelPersonPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SelPerson object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelPersonPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SelPersonPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collSelProviders = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelPersonPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SelPersonQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SelPersonPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SelPersonPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->selProvidersScheduledForDeletion !== null) {
                if (!$this->selProvidersScheduledForDeletion->isEmpty()) {
                    foreach ($this->selProvidersScheduledForDeletion as $selProvider) {
                        // need to save related object because we set the relation to null
                        $selProvider->save($con);
                    }
                    $this->selProvidersScheduledForDeletion = null;
                }
            }

            if ($this->collSelProviders !== null) {
                foreach ($this->collSelProviders as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SelPersonPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(SelPersonPeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`NAME`';
        }
        if ($this->isColumnModified(SelPersonPeer::FIRST_LASTNAME)) {
            $modifiedColumns[':p' . $index++]  = '`FIRST_LASTNAME`';
        }
        if ($this->isColumnModified(SelPersonPeer::SECOND_LASTNAME)) {
            $modifiedColumns[':p' . $index++]  = '`SECOND_LASTNAME`';
        }
        if ($this->isColumnModified(SelPersonPeer::IDENTITY)) {
            $modifiedColumns[':p' . $index++]  = '`IDENTITY`';
        }
        if ($this->isColumnModified(SelPersonPeer::IDENTITY_EXT)) {
            $modifiedColumns[':p' . $index++]  = '`IDENTITY_EXT`';
        }
        if ($this->isColumnModified(SelPersonPeer::GENDER)) {
            $modifiedColumns[':p' . $index++]  = '`GENDER`';
        }
        if ($this->isColumnModified(SelPersonPeer::BIRTHDAY_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`BIRTHDAY_DATE`';
        }

        $sql = sprintf(
            'INSERT INTO `sel_person` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`NAME`':						
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`FIRST_LASTNAME`':						
                        $stmt->bindValue($identifier, $this->first_lastname, PDO::PARAM_STR);
                        break;
                    case '`SECOND_LASTNAME`':						
                        $stmt->bindValue($identifier, $this->second_lastname, PDO::PARAM_STR);
                        break;
                    case '`IDENTITY`':						
                        $stmt->bindValue($identifier, $this->identity, PDO::PARAM_INT);
                        break;
                    case '`IDENTITY_EXT`':						
                        $stmt->bindValue($identifier, $this->identity_ext, PDO::PARAM_STR);
                        break;
                    case '`GENDER`':						
                        $stmt->bindValue($identifier, $this->gender, PDO::PARAM_STR);
                        break;
                    case '`BIRTHDAY_DATE`':						
                        $stmt->bindValue($identifier, $this->birthday_date, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();

            if (($retval = SelPersonPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collSelProviders !== null) {
                    foreach ($this->collSelProviders as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelPersonPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getFirstLastname();
                break;
            case 3:
                return $this->getSecondLastname();
                break;
            case 4:
                return $this->getIdentity();
                break;
            case 5:
                return $this->getIdentityExt();
                break;
            case 6:
                return $this->getGender();
                break;
            case 7:
                return $this->getBirthdayDate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SelPerson'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SelPerson'][$this->getPrimaryKey()] = true;
        $keys = SelPersonPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getFirstLastname(),
            $keys[3] => $this->getSecondLastname(),
            $keys[4] => $this->getIdentity(),
            $keys[5] => $this->getIdentityExt(),
            $keys[6] => $this->getGender(),
            $keys[7] => $this->getBirthdayDate(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collSelProviders) {
                $result['SelProviders'] = $this->collSelProviders->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SelPersonPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setFirstLastname($value);
                break;
            case 3:
                $this->setSecondLastname($value);
                break;
            case 4:
                $this->setIdentity($value);
                break;
            case 5:
                $this->setIdentityExt($value);
                break;
            case 6:
                $this->setGender($value);
                break;
            case 7:
                $this->setBirthdayDate($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SelPersonPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setFirstLastname($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setSecondLastname($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIdentity($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setIdentityExt($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setGender($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setBirthdayDate($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SelPersonPeer::DATABASE_NAME);

        if ($this->isColumnModified(SelPersonPeer::ID)) $criteria->add(SelPersonPeer::ID, $this->id);
        if ($this->isColumnModified(SelPersonPeer::NAME)) $criteria->add(SelPersonPeer::NAME, $this->name);
        if ($this->isColumnModified(SelPersonPeer::FIRST_LASTNAME)) $criteria->add(SelPersonPeer::FIRST_LASTNAME, $this->first_lastname);
        if ($this->isColumnModified(SelPersonPeer::SECOND_LASTNAME)) $criteria->add(SelPersonPeer::SECOND_LASTNAME, $this->second_lastname);
        if ($this->isColumnModified(SelPersonPeer::IDENTITY)) $criteria->add(SelPersonPeer::IDENTITY, $this->identity);
        if ($this->isColumnModified(SelPersonPeer::IDENTITY_EXT)) $criteria->add(SelPersonPeer::IDENTITY_EXT, $this->identity_ext);
        if ($this->isColumnModified(SelPersonPeer::GENDER)) $criteria->add(SelPersonPeer::GENDER, $this->gender);
        if ($this->isColumnModified(SelPersonPeer::BIRTHDAY_DATE)) $criteria->add(SelPersonPeer::BIRTHDAY_DATE, $this->birthday_date);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SelPersonPeer::DATABASE_NAME);
        $criteria->add(SelPersonPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SelPerson (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setFirstLastname($this->getFirstLastname());
        $copyObj->setSecondLastname($this->getSecondLastname());
        $copyObj->setIdentity($this->getIdentity());
        $copyObj->setIdentityExt($this->getIdentityExt());
        $copyObj->setGender($this->getGender());
        $copyObj->setBirthdayDate($this->getBirthdayDate());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getSelProviders() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSelProvider($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SelPerson Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SelPersonPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SelPersonPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SelProvider' == $relationName) {
            $this->initSelProviders();
        }
    }

    /**
     * Clears out the collSelProviders collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SelPerson The current object (for fluent API support)
     * @see        addSelProviders()
     */
    public function clearSelProviders()
    {
        $this->collSelProviders = null; // important to set this to null since that means it is uninitialized
        $this->collSelProvidersPartial = null;

        return $this;
    }

    /**
     * reset is the collSelProviders collection loaded partially
     *
     * @return void
     */
    public function resetPartialSelProviders($v = true)
    {
        $this->collSelProvidersPartial = $v;
    }

    /**
     * Initializes the collSelProviders collection.
     *
     * By default this just sets the collSelProviders collection to an empty array (like clearcollSelProviders());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSelProviders($overrideExisting = true)
    {
        if (null !== $this->collSelProviders && !$overrideExisting) {
            return;
        }
        $this->collSelProviders = new PropelObjectCollection();
        $this->collSelProviders->setModel('SelProvider');
    }

    /**
     * Gets an array of SelProvider objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SelPerson is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SelProvider[] List of SelProvider objects
     * @throws PropelException
     */
    public function getSelProviders($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSelProvidersPartial && !$this->isNew();
        if (null === $this->collSelProviders || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSelProviders) {
                // return empty collection
                $this->initSelProviders();
            } else {
                $collSelProviders = SelProviderQuery::create(null, $criteria)
                    ->filterBySelPerson($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSelProvidersPartial && count($collSelProviders)) {
                      $this->initSelProviders(false);

                      foreach($collSelProviders as $obj) {
                        if (false == $this->collSelProviders->contains($obj)) {
                          $this->collSelProviders->append($obj);
                        }
                      }

                      $this->collSelProvidersPartial = true;
                    }

                    return $collSelProviders;
                }

                if($partial && $this->collSelProviders) {
                    foreach($this->collSelProviders as $obj) {
                        if($obj->isNew()) {
                            $collSelProviders[] = $obj;
                        }
                    }
                }

                $this->collSelProviders = $collSelProviders;
                $this->collSelProvidersPartial = false;
            }
        }

        return $this->collSelProviders;
    }

    /**
     * Sets a collection of SelProvider objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $selProviders A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SelPerson The current object (for fluent API support)
     */
    public function setSelProviders(PropelCollection $selProviders, PropelPDO $con = null)
    {
        $selProvidersToDelete = $this->getSelProviders(new Criteria(), $con)->diff($selProviders);

        $this->selProvidersScheduledForDeletion = unserialize(serialize($selProvidersToDelete));

        foreach ($selProvidersToDelete as $selProviderRemoved) {
            $selProviderRemoved->setSelPerson(null);
        }

        $this->collSelProviders = null;
        foreach ($selProviders as $selProvider) {
            $this->addSelProvider($selProvider);
        }

        $this->collSelProviders = $selProviders;
        $this->collSelProvidersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SelProvider objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SelProvider objects.
     * @throws PropelException
     */
    public function countSelProviders(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSelProvidersPartial && !$this->isNew();
        if (null === $this->collSelProviders || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSelProviders) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSelProviders());
            }
            $query = SelProviderQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySelPerson($this)
                ->count($con);
        }

        return count($this->collSelProviders);
    }

    /**
     * Method called to associate a SelProvider object to this object
     * through the SelProvider foreign key attribute.
     *
     * @param    SelProvider $l SelProvider
     * @return SelPerson The current object (for fluent API support)
     */
    public function addSelProvider(SelProvider $l)
    {
        if ($this->collSelProviders === null) {
            $this->initSelProviders();
            $this->collSelProvidersPartial = true;
        }
        if (!in_array($l, $this->collSelProviders->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSelProvider($l);
        }

        return $this;
    }

    /**
     * @param	SelProvider $selProvider The selProvider object to add.
     */
    protected function doAddSelProvider($selProvider)
    {
        $this->collSelProviders[]= $selProvider;
        $selProvider->setSelPerson($this);
    }

    /**
     * @param	SelProvider $selProvider The selProvider object to remove.
     * @return SelPerson The current object (for fluent API support)
     */
    public function removeSelProvider($selProvider)
    {
        if ($this->getSelProviders()->contains($selProvider)) {
            $this->collSelProviders->remove($this->collSelProviders->search($selProvider));
            if (null === $this->selProvidersScheduledForDeletion) {
                $this->selProvidersScheduledForDeletion = clone $this->collSelProviders;
                $this->selProvidersScheduledForDeletion->clear();
            }
            $this->selProvidersScheduledForDeletion[]= $selProvider;
            $selProvider->setSelPerson(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->name = null;
        $this->first_lastname = null;
        $this->second_lastname = null;
        $this->identity = null;
        $this->identity_ext = null;
        $this->gender = null;
        $this->birthday_date = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSelProviders) {
                foreach ($this->collSelProviders as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collSelProviders instanceof PropelCollection) {
            $this->collSelProviders->clearIterator();
        }
        $this->collSelProviders = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SelPersonPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
