<?php


/**
 * Base class that represents a query for the 'cho_user_x_rol' table.
 *
 * 
 *
 * @method ChoUserXRolQuery orderByUserId($order = Criteria::ASC) Order by the USER_ID column
 * @method ChoUserXRolQuery orderByRolId($order = Criteria::ASC) Order by the ROL_ID column
 *
 * @method ChoUserXRolQuery groupByUserId() Group by the USER_ID column
 * @method ChoUserXRolQuery groupByRolId() Group by the ROL_ID column
 *
 * @method ChoUserXRolQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ChoUserXRolQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ChoUserXRolQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ChoUserXRolQuery leftJoinChoUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChoUser relation
 * @method ChoUserXRolQuery rightJoinChoUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChoUser relation
 * @method ChoUserXRolQuery innerJoinChoUser($relationAlias = null) Adds a INNER JOIN clause to the query using the ChoUser relation
 *
 * @method ChoUserXRolQuery leftJoinChoRol($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChoRol relation
 * @method ChoUserXRolQuery rightJoinChoRol($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChoRol relation
 * @method ChoUserXRolQuery innerJoinChoRol($relationAlias = null) Adds a INNER JOIN clause to the query using the ChoRol relation
 *
 * @method ChoUserXRol findOne(PropelPDO $con = null) Return the first ChoUserXRol matching the query
 * @method ChoUserXRol findOneOrCreate(PropelPDO $con = null) Return the first ChoUserXRol matching the query, or a new ChoUserXRol object populated from the query conditions when no match is found
 *
 * @method ChoUserXRol findOneByUserId(int $USER_ID) Return the first ChoUserXRol filtered by the USER_ID column
 * @method ChoUserXRol findOneByRolId(int $ROL_ID) Return the first ChoUserXRol filtered by the ROL_ID column
 *
 * @method array findByUserId(int $USER_ID) Return ChoUserXRol objects filtered by the USER_ID column
 * @method array findByRolId(int $ROL_ID) Return ChoUserXRol objects filtered by the ROL_ID column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseChoUserXRolQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseChoUserXRolQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'ChoUserXRol', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChoUserXRolQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     ChoUserXRolQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChoUserXRolQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ChoUserXRolQuery) {
            return $criteria;
        }
        $query = new ChoUserXRolQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query 
                         A Primary key composition: [$USER_ID, $ROL_ID]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ChoUserXRol|ChoUserXRol[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ChoUserXRolPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ChoUserXRolPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   ChoUserXRol A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `USER_ID`, `ROL_ID` FROM `cho_user_x_rol` WHERE `USER_ID` = :p0 AND `ROL_ID` = :p1';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);			
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ChoUserXRol();
            $obj->hydrate($row);
            ChoUserXRolPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ChoUserXRol|ChoUserXRol[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ChoUserXRol[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ChoUserXRolQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ChoUserXRolPeer::USER_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ChoUserXRolPeer::ROL_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ChoUserXRolQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ChoUserXRolPeer::USER_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ChoUserXRolPeer::ROL_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the USER_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE USER_ID = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE USER_ID IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE USER_ID > 12
     * </code>
     *
     * @see       filterByChoUser()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserXRolQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ChoUserXRolPeer::USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the ROL_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByRolId(1234); // WHERE ROL_ID = 1234
     * $query->filterByRolId(array(12, 34)); // WHERE ROL_ID IN (12, 34)
     * $query->filterByRolId(array('min' => 12)); // WHERE ROL_ID > 12
     * </code>
     *
     * @see       filterByChoRol()
     *
     * @param     mixed $rolId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChoUserXRolQuery The current query, for fluid interface
     */
    public function filterByRolId($rolId = null, $comparison = null)
    {
        if (is_array($rolId) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(ChoUserXRolPeer::ROL_ID, $rolId, $comparison);
    }

    /**
     * Filter the query by a related ChoUser object
     *
     * @param   ChoUser|PropelObjectCollection $choUser The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ChoUserXRolQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByChoUser($choUser, $comparison = null)
    {
        if ($choUser instanceof ChoUser) {
            return $this
                ->addUsingAlias(ChoUserXRolPeer::USER_ID, $choUser->getId(), $comparison);
        } elseif ($choUser instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChoUserXRolPeer::USER_ID, $choUser->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChoUser() only accepts arguments of type ChoUser or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChoUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ChoUserXRolQuery The current query, for fluid interface
     */
    public function joinChoUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChoUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChoUser');
        }

        return $this;
    }

    /**
     * Use the ChoUser relation ChoUser object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ChoUserQuery A secondary query class using the current class as primary query
     */
    public function useChoUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChoUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChoUser', 'ChoUserQuery');
    }

    /**
     * Filter the query by a related ChoRol object
     *
     * @param   ChoRol|PropelObjectCollection $choRol The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   ChoUserXRolQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByChoRol($choRol, $comparison = null)
    {
        if ($choRol instanceof ChoRol) {
            return $this
                ->addUsingAlias(ChoUserXRolPeer::ROL_ID, $choRol->getId(), $comparison);
        } elseif ($choRol instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChoUserXRolPeer::ROL_ID, $choRol->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChoRol() only accepts arguments of type ChoRol or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChoRol relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ChoUserXRolQuery The current query, for fluid interface
     */
    public function joinChoRol($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChoRol');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChoRol');
        }

        return $this;
    }

    /**
     * Use the ChoRol relation ChoRol object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ChoRolQuery A secondary query class using the current class as primary query
     */
    public function useChoRolQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChoRol($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChoRol', 'ChoRolQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChoUserXRol $choUserXRol Object to remove from the list of results
     *
     * @return ChoUserXRolQuery The current query, for fluid interface
     */
    public function prune($choUserXRol = null)
    {
        if ($choUserXRol) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ChoUserXRolPeer::USER_ID), $choUserXRol->getUserId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ChoUserXRolPeer::ROL_ID), $choUserXRol->getRolId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
