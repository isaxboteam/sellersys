<?php


/**
 * Base class that represents a query for the 'sel_branch_category' table.
 *
 * 
 *
 * @method SelBranchCategoryQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method SelBranchCategoryQuery orderByBranchId($order = Criteria::ASC) Order by the BRANCH_ID column
 * @method SelBranchCategoryQuery orderByCategoryId($order = Criteria::ASC) Order by the CATEGORY_ID column
 * @method SelBranchCategoryQuery orderByState($order = Criteria::ASC) Order by the STATE column
 * @method SelBranchCategoryQuery orderByObservation($order = Criteria::ASC) Order by the OBSERVATION column
 *
 * @method SelBranchCategoryQuery groupById() Group by the ID column
 * @method SelBranchCategoryQuery groupByBranchId() Group by the BRANCH_ID column
 * @method SelBranchCategoryQuery groupByCategoryId() Group by the CATEGORY_ID column
 * @method SelBranchCategoryQuery groupByState() Group by the STATE column
 * @method SelBranchCategoryQuery groupByObservation() Group by the OBSERVATION column
 *
 * @method SelBranchCategoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SelBranchCategoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SelBranchCategoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SelBranchCategoryQuery leftJoinSelBranch($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelBranch relation
 * @method SelBranchCategoryQuery rightJoinSelBranch($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelBranch relation
 * @method SelBranchCategoryQuery innerJoinSelBranch($relationAlias = null) Adds a INNER JOIN clause to the query using the SelBranch relation
 *
 * @method SelBranchCategoryQuery leftJoinSelCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the SelCategory relation
 * @method SelBranchCategoryQuery rightJoinSelCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SelCategory relation
 * @method SelBranchCategoryQuery innerJoinSelCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the SelCategory relation
 *
 * @method SelBranchCategory findOne(PropelPDO $con = null) Return the first SelBranchCategory matching the query
 * @method SelBranchCategory findOneOrCreate(PropelPDO $con = null) Return the first SelBranchCategory matching the query, or a new SelBranchCategory object populated from the query conditions when no match is found
 *
 * @method SelBranchCategory findOneByBranchId(int $BRANCH_ID) Return the first SelBranchCategory filtered by the BRANCH_ID column
 * @method SelBranchCategory findOneByCategoryId(int $CATEGORY_ID) Return the first SelBranchCategory filtered by the CATEGORY_ID column
 * @method SelBranchCategory findOneByState(string $STATE) Return the first SelBranchCategory filtered by the STATE column
 * @method SelBranchCategory findOneByObservation(string $OBSERVATION) Return the first SelBranchCategory filtered by the OBSERVATION column
 *
 * @method array findById(int $ID) Return SelBranchCategory objects filtered by the ID column
 * @method array findByBranchId(int $BRANCH_ID) Return SelBranchCategory objects filtered by the BRANCH_ID column
 * @method array findByCategoryId(int $CATEGORY_ID) Return SelBranchCategory objects filtered by the CATEGORY_ID column
 * @method array findByState(string $STATE) Return SelBranchCategory objects filtered by the STATE column
 * @method array findByObservation(string $OBSERVATION) Return SelBranchCategory objects filtered by the OBSERVATION column
 *
 * @package    propel.generator.system.om
 */
abstract class BaseSelBranchCategoryQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSelBranchCategoryQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'system', $modelName = 'SelBranchCategory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SelBranchCategoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     SelBranchCategoryQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SelBranchCategoryQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SelBranchCategoryQuery) {
            return $criteria;
        }
        $query = new SelBranchCategoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SelBranchCategory|SelBranchCategory[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SelBranchCategoryPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SelBranchCategoryPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelBranchCategory A model object, or null if the key is not found
     * @throws   PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   SelBranchCategory A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `BRANCH_ID`, `CATEGORY_ID`, `STATE`, `OBSERVATION` FROM `sel_branch_category` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SelBranchCategory();
            $obj->hydrate($row);
            SelBranchCategoryPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SelBranchCategory|SelBranchCategory[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SelBranchCategory[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SelBranchCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SelBranchCategoryPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SelBranchCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SelBranchCategoryPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchCategoryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(SelBranchCategoryPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the BRANCH_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByBranchId(1234); // WHERE BRANCH_ID = 1234
     * $query->filterByBranchId(array(12, 34)); // WHERE BRANCH_ID IN (12, 34)
     * $query->filterByBranchId(array('min' => 12)); // WHERE BRANCH_ID > 12
     * </code>
     *
     * @see       filterBySelBranch()
     *
     * @param     mixed $branchId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchCategoryQuery The current query, for fluid interface
     */
    public function filterByBranchId($branchId = null, $comparison = null)
    {
        if (is_array($branchId)) {
            $useMinMax = false;
            if (isset($branchId['min'])) {
                $this->addUsingAlias(SelBranchCategoryPeer::BRANCH_ID, $branchId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($branchId['max'])) {
                $this->addUsingAlias(SelBranchCategoryPeer::BRANCH_ID, $branchId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelBranchCategoryPeer::BRANCH_ID, $branchId, $comparison);
    }

    /**
     * Filter the query on the CATEGORY_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryId(1234); // WHERE CATEGORY_ID = 1234
     * $query->filterByCategoryId(array(12, 34)); // WHERE CATEGORY_ID IN (12, 34)
     * $query->filterByCategoryId(array('min' => 12)); // WHERE CATEGORY_ID > 12
     * </code>
     *
     * @see       filterBySelCategory()
     *
     * @param     mixed $categoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchCategoryQuery The current query, for fluid interface
     */
    public function filterByCategoryId($categoryId = null, $comparison = null)
    {
        if (is_array($categoryId)) {
            $useMinMax = false;
            if (isset($categoryId['min'])) {
                $this->addUsingAlias(SelBranchCategoryPeer::CATEGORY_ID, $categoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoryId['max'])) {
                $this->addUsingAlias(SelBranchCategoryPeer::CATEGORY_ID, $categoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SelBranchCategoryPeer::CATEGORY_ID, $categoryId, $comparison);
    }

    /**
     * Filter the query on the STATE column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE STATE = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE STATE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchCategoryQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelBranchCategoryPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the OBSERVATION column
     *
     * Example usage:
     * <code>
     * $query->filterByObservation('fooValue');   // WHERE OBSERVATION = 'fooValue'
     * $query->filterByObservation('%fooValue%'); // WHERE OBSERVATION LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SelBranchCategoryQuery The current query, for fluid interface
     */
    public function filterByObservation($observation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $observation)) {
                $observation = str_replace('*', '%', $observation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SelBranchCategoryPeer::OBSERVATION, $observation, $comparison);
    }

    /**
     * Filter the query by a related SelBranch object
     *
     * @param   SelBranch|PropelObjectCollection $selBranch The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelBranchCategoryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelBranch($selBranch, $comparison = null)
    {
        if ($selBranch instanceof SelBranch) {
            return $this
                ->addUsingAlias(SelBranchCategoryPeer::BRANCH_ID, $selBranch->getId(), $comparison);
        } elseif ($selBranch instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelBranchCategoryPeer::BRANCH_ID, $selBranch->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelBranch() only accepts arguments of type SelBranch or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelBranch relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelBranchCategoryQuery The current query, for fluid interface
     */
    public function joinSelBranch($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelBranch');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelBranch');
        }

        return $this;
    }

    /**
     * Use the SelBranch relation SelBranch object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelBranchQuery A secondary query class using the current class as primary query
     */
    public function useSelBranchQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelBranch($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelBranch', 'SelBranchQuery');
    }

    /**
     * Filter the query by a related SelCategory object
     *
     * @param   SelCategory|PropelObjectCollection $selCategory The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   SelBranchCategoryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBySelCategory($selCategory, $comparison = null)
    {
        if ($selCategory instanceof SelCategory) {
            return $this
                ->addUsingAlias(SelBranchCategoryPeer::CATEGORY_ID, $selCategory->getId(), $comparison);
        } elseif ($selCategory instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SelBranchCategoryPeer::CATEGORY_ID, $selCategory->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySelCategory() only accepts arguments of type SelCategory or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SelCategory relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SelBranchCategoryQuery The current query, for fluid interface
     */
    public function joinSelCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SelCategory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SelCategory');
        }

        return $this;
    }

    /**
     * Use the SelCategory relation SelCategory object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SelCategoryQuery A secondary query class using the current class as primary query
     */
    public function useSelCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSelCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SelCategory', 'SelCategoryQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SelBranchCategory $selBranchCategory Object to remove from the list of results
     *
     * @return SelBranchCategoryQuery The current query, for fluid interface
     */
    public function prune($selBranchCategory = null)
    {
        if ($selBranchCategory) {
            $this->addUsingAlias(SelBranchCategoryPeer::ID, $selBranchCategory->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
