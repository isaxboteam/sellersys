<?php


/**
 * Base class that represents a row from the 'cho_rol' table.
 *
 * 
 *
 * @package    propel.generator.system.om
 */
abstract class BaseChoRol extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'ChoRolPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var  ChoRolPeer
     */
    protected static $peer;

    /**
     * The array for validation rules
     * @var  array
     */
    protected static $validationRules = array();

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var  boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the code field.
     * @var        string
     */
    protected $code;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * @var        PropelObjectCollection|ChoRolXUri[] Collection to store aggregation of ChoRolXUri objects.
     */
    protected $collChoRolXUris;
    protected $collChoRolXUrisPartial;

    /**
     * @var        PropelObjectCollection|ChoUserXRol[] Collection to store aggregation of ChoUserXRol objects.
     */
    protected $collChoUserXRols;
    protected $collChoUserXRolsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $choRolXUrisScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $choUserXRolsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [code] column value.
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [name] column value.
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [description] column value.
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param int $v new value
     * @return ChoRol The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ChoRolPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [code] column.
     * 
     * @param string $v new value
     * @return ChoRol The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[] = ChoRolPeer::CODE;
        }


        return $this;
    } // setCode()

    /**
     * Set the value of [name] column.
     * 
     * @param string $v new value
     * @return ChoRol The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = ChoRolPeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [description] column.
     * 
     * @param string $v new value
     * @return ChoRol The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = ChoRolPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->code = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->name = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->description = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 4; // 4 = ChoRolPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating ChoRol object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoRolPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ChoRolPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collChoRolXUris = null;

            $this->collChoUserXRols = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoRolPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ChoRolQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ChoRolPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ChoRolPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->choRolXUrisScheduledForDeletion !== null) {
                if (!$this->choRolXUrisScheduledForDeletion->isEmpty()) {
                    ChoRolXUriQuery::create()
                        ->filterByPrimaryKeys($this->choRolXUrisScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->choRolXUrisScheduledForDeletion = null;
                }
            }

            if ($this->collChoRolXUris !== null) {
                foreach ($this->collChoRolXUris as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->choUserXRolsScheduledForDeletion !== null) {
                if (!$this->choUserXRolsScheduledForDeletion->isEmpty()) {
                    ChoUserXRolQuery::create()
                        ->filterByPrimaryKeys($this->choUserXRolsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->choUserXRolsScheduledForDeletion = null;
                }
            }

            if ($this->collChoUserXRols !== null) {
                foreach ($this->collChoUserXRols as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ChoRolPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ChoRolPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ChoRolPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`ID`';
        }
        if ($this->isColumnModified(ChoRolPeer::CODE)) {
            $modifiedColumns[':p' . $index++]  = '`CODE`';
        }
        if ($this->isColumnModified(ChoRolPeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`NAME`';
        }
        if ($this->isColumnModified(ChoRolPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`DESCRIPTION`';
        }

        $sql = sprintf(
            'INSERT INTO `cho_rol` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`ID`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`CODE`':						
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case '`NAME`':						
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`DESCRIPTION`':						
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        if($this->preValidate($columns)){
            $res =  $this->doValidate($columns);
        }
        if ($res === true) {
            $this->validationFailures = array();
            $this->postValidate($columns);

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();

            if (($retval = ChoRolPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collChoRolXUris !== null) {
                    foreach ($this->collChoRolXUris as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collChoUserXRols !== null) {
                    foreach ($this->collChoUserXRols as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

            $validationErrors = ValidationHelper::validateObject($this, static::$validationRules);
            $failureMap = ValidationHelper::mergeFailures($failureMap, $validationErrors);

            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Return the validation failures as an array for a easy data management.
     *
     * @return array Array of <code>ValidationFailed</code> objets.
     */
    public function getErrorsMap()
    {
        return ValidationHelper::failuresMap($this->validationFailures);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ChoRolPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getCode();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getDescription();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['ChoRol'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ChoRol'][$this->getPrimaryKey()] = true;
        $keys = ChoRolPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getCode(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getDescription(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collChoRolXUris) {
                $result['ChoRolXUris'] = $this->collChoRolXUris->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collChoUserXRols) {
                $result['ChoUserXRols'] = $this->collChoUserXRols->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ChoRolPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setCode($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setDescription($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ChoRolPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setCode($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setDescription($arr[$keys[3]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ChoRolPeer::DATABASE_NAME);

        if ($this->isColumnModified(ChoRolPeer::ID)) $criteria->add(ChoRolPeer::ID, $this->id);
        if ($this->isColumnModified(ChoRolPeer::CODE)) $criteria->add(ChoRolPeer::CODE, $this->code);
        if ($this->isColumnModified(ChoRolPeer::NAME)) $criteria->add(ChoRolPeer::NAME, $this->name);
        if ($this->isColumnModified(ChoRolPeer::DESCRIPTION)) $criteria->add(ChoRolPeer::DESCRIPTION, $this->description);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ChoRolPeer::DATABASE_NAME);
        $criteria->add(ChoRolPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of ChoRol (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setCode($this->getCode());
        $copyObj->setName($this->getName());
        $copyObj->setDescription($this->getDescription());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getChoRolXUris() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChoRolXUri($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getChoUserXRols() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChoUserXRol($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return ChoRol Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ChoRolPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ChoRolPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ChoRolXUri' == $relationName) {
            $this->initChoRolXUris();
        }
        if ('ChoUserXRol' == $relationName) {
            $this->initChoUserXRols();
        }
    }

    /**
     * Clears out the collChoRolXUris collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return ChoRol The current object (for fluent API support)
     * @see        addChoRolXUris()
     */
    public function clearChoRolXUris()
    {
        $this->collChoRolXUris = null; // important to set this to null since that means it is uninitialized
        $this->collChoRolXUrisPartial = null;

        return $this;
    }

    /**
     * reset is the collChoRolXUris collection loaded partially
     *
     * @return void
     */
    public function resetPartialChoRolXUris($v = true)
    {
        $this->collChoRolXUrisPartial = $v;
    }

    /**
     * Initializes the collChoRolXUris collection.
     *
     * By default this just sets the collChoRolXUris collection to an empty array (like clearcollChoRolXUris());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChoRolXUris($overrideExisting = true)
    {
        if (null !== $this->collChoRolXUris && !$overrideExisting) {
            return;
        }
        $this->collChoRolXUris = new PropelObjectCollection();
        $this->collChoRolXUris->setModel('ChoRolXUri');
    }

    /**
     * Gets an array of ChoRolXUri objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChoRol is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|ChoRolXUri[] List of ChoRolXUri objects
     * @throws PropelException
     */
    public function getChoRolXUris($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collChoRolXUrisPartial && !$this->isNew();
        if (null === $this->collChoRolXUris || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChoRolXUris) {
                // return empty collection
                $this->initChoRolXUris();
            } else {
                $collChoRolXUris = ChoRolXUriQuery::create(null, $criteria)
                    ->filterByChoRol($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collChoRolXUrisPartial && count($collChoRolXUris)) {
                      $this->initChoRolXUris(false);

                      foreach($collChoRolXUris as $obj) {
                        if (false == $this->collChoRolXUris->contains($obj)) {
                          $this->collChoRolXUris->append($obj);
                        }
                      }

                      $this->collChoRolXUrisPartial = true;
                    }

                    return $collChoRolXUris;
                }

                if($partial && $this->collChoRolXUris) {
                    foreach($this->collChoRolXUris as $obj) {
                        if($obj->isNew()) {
                            $collChoRolXUris[] = $obj;
                        }
                    }
                }

                $this->collChoRolXUris = $collChoRolXUris;
                $this->collChoRolXUrisPartial = false;
            }
        }

        return $this->collChoRolXUris;
    }

    /**
     * Sets a collection of ChoRolXUri objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $choRolXUris A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return ChoRol The current object (for fluent API support)
     */
    public function setChoRolXUris(PropelCollection $choRolXUris, PropelPDO $con = null)
    {
        $choRolXUrisToDelete = $this->getChoRolXUris(new Criteria(), $con)->diff($choRolXUris);

        $this->choRolXUrisScheduledForDeletion = unserialize(serialize($choRolXUrisToDelete));

        foreach ($choRolXUrisToDelete as $choRolXUriRemoved) {
            $choRolXUriRemoved->setChoRol(null);
        }

        $this->collChoRolXUris = null;
        foreach ($choRolXUris as $choRolXUri) {
            $this->addChoRolXUri($choRolXUri);
        }

        $this->collChoRolXUris = $choRolXUris;
        $this->collChoRolXUrisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ChoRolXUri objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related ChoRolXUri objects.
     * @throws PropelException
     */
    public function countChoRolXUris(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collChoRolXUrisPartial && !$this->isNew();
        if (null === $this->collChoRolXUris || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChoRolXUris) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getChoRolXUris());
            }
            $query = ChoRolXUriQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChoRol($this)
                ->count($con);
        }

        return count($this->collChoRolXUris);
    }

    /**
     * Method called to associate a ChoRolXUri object to this object
     * through the ChoRolXUri foreign key attribute.
     *
     * @param    ChoRolXUri $l ChoRolXUri
     * @return ChoRol The current object (for fluent API support)
     */
    public function addChoRolXUri(ChoRolXUri $l)
    {
        if ($this->collChoRolXUris === null) {
            $this->initChoRolXUris();
            $this->collChoRolXUrisPartial = true;
        }
        if (!in_array($l, $this->collChoRolXUris->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddChoRolXUri($l);
        }

        return $this;
    }

    /**
     * @param	ChoRolXUri $choRolXUri The choRolXUri object to add.
     */
    protected function doAddChoRolXUri($choRolXUri)
    {
        $this->collChoRolXUris[]= $choRolXUri;
        $choRolXUri->setChoRol($this);
    }

    /**
     * @param	ChoRolXUri $choRolXUri The choRolXUri object to remove.
     * @return ChoRol The current object (for fluent API support)
     */
    public function removeChoRolXUri($choRolXUri)
    {
        if ($this->getChoRolXUris()->contains($choRolXUri)) {
            $this->collChoRolXUris->remove($this->collChoRolXUris->search($choRolXUri));
            if (null === $this->choRolXUrisScheduledForDeletion) {
                $this->choRolXUrisScheduledForDeletion = clone $this->collChoRolXUris;
                $this->choRolXUrisScheduledForDeletion->clear();
            }
            $this->choRolXUrisScheduledForDeletion[]= clone $choRolXUri;
            $choRolXUri->setChoRol(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChoRol is new, it will return
     * an empty collection; or if this ChoRol has previously
     * been saved, it will retrieve related ChoRolXUris from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChoRol.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|ChoRolXUri[] List of ChoRolXUri objects
     */
    public function getChoRolXUrisJoinChoUri($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ChoRolXUriQuery::create(null, $criteria);
        $query->joinWith('ChoUri', $join_behavior);

        return $this->getChoRolXUris($query, $con);
    }

    /**
     * Clears out the collChoUserXRols collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return ChoRol The current object (for fluent API support)
     * @see        addChoUserXRols()
     */
    public function clearChoUserXRols()
    {
        $this->collChoUserXRols = null; // important to set this to null since that means it is uninitialized
        $this->collChoUserXRolsPartial = null;

        return $this;
    }

    /**
     * reset is the collChoUserXRols collection loaded partially
     *
     * @return void
     */
    public function resetPartialChoUserXRols($v = true)
    {
        $this->collChoUserXRolsPartial = $v;
    }

    /**
     * Initializes the collChoUserXRols collection.
     *
     * By default this just sets the collChoUserXRols collection to an empty array (like clearcollChoUserXRols());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChoUserXRols($overrideExisting = true)
    {
        if (null !== $this->collChoUserXRols && !$overrideExisting) {
            return;
        }
        $this->collChoUserXRols = new PropelObjectCollection();
        $this->collChoUserXRols->setModel('ChoUserXRol');
    }

    /**
     * Gets an array of ChoUserXRol objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChoRol is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|ChoUserXRol[] List of ChoUserXRol objects
     * @throws PropelException
     */
    public function getChoUserXRols($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collChoUserXRolsPartial && !$this->isNew();
        if (null === $this->collChoUserXRols || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChoUserXRols) {
                // return empty collection
                $this->initChoUserXRols();
            } else {
                $collChoUserXRols = ChoUserXRolQuery::create(null, $criteria)
                    ->filterByChoRol($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collChoUserXRolsPartial && count($collChoUserXRols)) {
                      $this->initChoUserXRols(false);

                      foreach($collChoUserXRols as $obj) {
                        if (false == $this->collChoUserXRols->contains($obj)) {
                          $this->collChoUserXRols->append($obj);
                        }
                      }

                      $this->collChoUserXRolsPartial = true;
                    }

                    return $collChoUserXRols;
                }

                if($partial && $this->collChoUserXRols) {
                    foreach($this->collChoUserXRols as $obj) {
                        if($obj->isNew()) {
                            $collChoUserXRols[] = $obj;
                        }
                    }
                }

                $this->collChoUserXRols = $collChoUserXRols;
                $this->collChoUserXRolsPartial = false;
            }
        }

        return $this->collChoUserXRols;
    }

    /**
     * Sets a collection of ChoUserXRol objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $choUserXRols A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return ChoRol The current object (for fluent API support)
     */
    public function setChoUserXRols(PropelCollection $choUserXRols, PropelPDO $con = null)
    {
        $choUserXRolsToDelete = $this->getChoUserXRols(new Criteria(), $con)->diff($choUserXRols);

        $this->choUserXRolsScheduledForDeletion = unserialize(serialize($choUserXRolsToDelete));

        foreach ($choUserXRolsToDelete as $choUserXRolRemoved) {
            $choUserXRolRemoved->setChoRol(null);
        }

        $this->collChoUserXRols = null;
        foreach ($choUserXRols as $choUserXRol) {
            $this->addChoUserXRol($choUserXRol);
        }

        $this->collChoUserXRols = $choUserXRols;
        $this->collChoUserXRolsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ChoUserXRol objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related ChoUserXRol objects.
     * @throws PropelException
     */
    public function countChoUserXRols(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collChoUserXRolsPartial && !$this->isNew();
        if (null === $this->collChoUserXRols || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChoUserXRols) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getChoUserXRols());
            }
            $query = ChoUserXRolQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChoRol($this)
                ->count($con);
        }

        return count($this->collChoUserXRols);
    }

    /**
     * Method called to associate a ChoUserXRol object to this object
     * through the ChoUserXRol foreign key attribute.
     *
     * @param    ChoUserXRol $l ChoUserXRol
     * @return ChoRol The current object (for fluent API support)
     */
    public function addChoUserXRol(ChoUserXRol $l)
    {
        if ($this->collChoUserXRols === null) {
            $this->initChoUserXRols();
            $this->collChoUserXRolsPartial = true;
        }
        if (!in_array($l, $this->collChoUserXRols->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddChoUserXRol($l);
        }

        return $this;
    }

    /**
     * @param	ChoUserXRol $choUserXRol The choUserXRol object to add.
     */
    protected function doAddChoUserXRol($choUserXRol)
    {
        $this->collChoUserXRols[]= $choUserXRol;
        $choUserXRol->setChoRol($this);
    }

    /**
     * @param	ChoUserXRol $choUserXRol The choUserXRol object to remove.
     * @return ChoRol The current object (for fluent API support)
     */
    public function removeChoUserXRol($choUserXRol)
    {
        if ($this->getChoUserXRols()->contains($choUserXRol)) {
            $this->collChoUserXRols->remove($this->collChoUserXRols->search($choUserXRol));
            if (null === $this->choUserXRolsScheduledForDeletion) {
                $this->choUserXRolsScheduledForDeletion = clone $this->collChoUserXRols;
                $this->choUserXRolsScheduledForDeletion->clear();
            }
            $this->choUserXRolsScheduledForDeletion[]= clone $choUserXRol;
            $choUserXRol->setChoRol(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ChoRol is new, it will return
     * an empty collection; or if this ChoRol has previously
     * been saved, it will retrieve related ChoUserXRols from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ChoRol.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|ChoUserXRol[] List of ChoUserXRol objects
     */
    public function getChoUserXRolsJoinChoUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ChoUserXRolQuery::create(null, $criteria);
        $query->joinWith('ChoUser', $join_behavior);

        return $this->getChoUserXRols($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->code = null;
        $this->name = null;
        $this->description = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collChoRolXUris) {
                foreach ($this->collChoRolXUris as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collChoUserXRols) {
                foreach ($this->collChoUserXRols as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collChoRolXUris instanceof PropelCollection) {
            $this->collChoRolXUris->clearIterator();
        }
        $this->collChoRolXUris = null;
        if ($this->collChoUserXRols instanceof PropelCollection) {
            $this->collChoUserXRols->clearIterator();
        }
        $this->collChoUserXRols = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ChoRolPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
