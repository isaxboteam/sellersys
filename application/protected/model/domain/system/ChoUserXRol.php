<?php
/**
 * Skeleton subclass for representing a row from the 'cho_user_x_rol' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class ChoUserXRol extends BaseChoUserXRol
{

    static $validationRules = array(
        'userId' => array(
            'null' => false, 'blank'=> false,
        ),
        'rolId' => array(
            'null' => false, 'blank'=> false,
        ),
    );

}