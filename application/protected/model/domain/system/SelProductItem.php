<?php
/**
 * Skeleton subclass for representing a row from the 'sel_product_item' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.system
 */
class SelProductItem extends BaseSelProductItem
{

    const IN_ORDER = 'INORDER';
    const STOCK = 'STOCK';
    const RESERVED = 'RESERVED';
    const SOLD = 'SOLD';

    public static function enum()
    {
        $statusTypes = array(
            SelProductItem::IN_ORDER => SelProductItem::IN_ORDER,
            SelProductItem::STOCK => SelProductItem::STOCK,
            SelProductItem::SOLD => SelProductItem::SOLD,
            SelProductItem::RESERVED => SelProductItem::RESERVED
        );
        return $statusTypes;
    }

    public function isNotAssigned()
    {
        return $this->status == self::IN_ORDER;
    }

}