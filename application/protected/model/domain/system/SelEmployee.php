<?php

class SelEmployee extends BaseSelEmployee
{

    static $validationRules = array(
        'InitDate' => array(
            'null' => false, 'blank'=> false,
        ),
        'EndDate' => array(
            'null' => false, 'blank'=> false,
        ),
        'UserId' => array(
            'null' => false,  'blank'=> false,
        ),
        'Details' => array(
            'null' => true, 'blank'=> false,
            'size'=> array('min' => 3, 'max' => 500),
        ),
    );

    public function preValidate($columns = null)
    {
        return $this->preSave();
    }

    public function preSave(PropelPDO $con = null)
    {
        $this->user_id = $this->user_id?: null;
        $this->details = trim($this->details)?: null;
        return parent::preSave($con);
    }

    /**
     * @param $date
     * @return array
     */
    public function branchesInDate($date)
    {
        $branchesEmployee = SelBranchEmployeeQuery::createInDateEmployee($date, $this)
            ->useSelBranchQuery()
            ->orderByName(Criteria::ASC)
            ->endUse()
            ->find();
//        return array_map(function($branchEmployee){
//                return $branchEmployee->getSelBranch();
//            }, $branchesEmployee->getArrayCopy());
        return SelBranchQuery::create()
            ->useSelBranchEmployeeQuery()
            ->filterBySelEmployee($this)
            ->endUse()
            ->find();
    }

}